<table cellspacing="0" cellpadding="0" border="0" style="margin:0 auto;border-collapse:collapse;width:600px">
    <tbody>
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" border="0" bgcolor="#00000" style="border-collapse:collapse;width:600px;margin:0 auto">
                    <tbody>
                       <tr style="line-height:17px" height="17" bgcolor="#00000">
                          <td valign="top" colspan="1" style="font-size:1px;line-height:1px">        
                             <img border="0" style="display:block;border:0;width:1px;height:17px" src="https://ci3.googleusercontent.com/proxy/UqmZ6wQxEWlb3II5jomx9MdDmbUg7LFpdWtTTF_qd6-INKrY8qXVjmZQYo-4iGCYc4ff4H7MjQih2X518_RfPcQo58GzdlZx=s0-d-e1-ft#https://static.chrono24.com/images/default/mail/s.gif" alt="" title="" width="1" height="17" class="CToWUd">
                          </td>
                       </tr>
                       <tr>
                          <td valign="top" align="center" bgcolor="#00000" width="600">
                             <a style="text-decoration:none!important" href="#" target="_blank">   <img border="0" style="display:block;border:0;width:150px;height:auto;" src="http://tangiblelisting.developersoptimal.com/assets/img/logo-header.svg" alt="Tangible- The World's Watch Market" title="Tangible- The World's Watch Market" width="150" height="36" class="CToWUd">
                             </a>      
                          </td>
                       </tr>
                       <tr style="line-height:17px" height="17" bgcolor="#00000">
                          <td valign="top" colspan="1" style="font-size:1px;line-height:1px">        
                             <img border="0" style="display:block;border:0;width:1px;height:17px" src="https://ci3.googleusercontent.com/proxy/UqmZ6wQxEWlb3II5jomx9MdDmbUg7LFpdWtTTF_qd6-INKrY8qXVjmZQYo-4iGCYc4ff4H7MjQih2X518_RfPcQo58GzdlZx=s0-d-e1-ft#https://static.chrono24.com/images/default/mail/s.gif" alt="" title="" width="1" height="17" class="CToWUd">
                          </td>
                       </tr>
                    </tbody>
                 </table> 
                <table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;width:600px">
                    <tbody>
                        <tr>
                            <td valign="top" align="left" bgcolor="#fcfcfc" width="25">
                                <img border="0" style="display:block;border:0;width:25px;height:1px" src="" alt=""
                                    title="" width="25" height="1" class="CToWUd">
                            </td>
                            <td valign="top" align="left" bgcolor="#fcfcfc" width="550">
                                <table cellspacing="0" cellpadding="0" border="0"
                                    style="border-collapse:collapse;width:550px">
                                    <tbody>
                                        <tr style="line-height:17px" height="17" bgcolor="#fcfcfc">
                                            <td valign="top" colspan="1" style="font-size:1px;line-height:1px">
                                                <img border="0" style="display:block;border:0;width:1px;height:17px"
                                                    src="" alt="" title="" width="1" height="17" class="CToWUd">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="1" valign="top" align="left" bgcolor="#fcfcfc" width="550">
                                                <font face="Arial, Helvetica, sans-serif" size="2"
                                                    style="font-size:16px;line-height:22px;color:#04293a">
                                                    Dear <strong>{{$user->first_name .' '. $user->last_name}}</strong>,<br><br>You have suggested a
                                                    price to <stong>{{$agent->first_name .' '. $agent->last_name}}</stong>
                                                </font>
                                            </td>
                                        </tr>
                                        <tr style="line-height:17px" height="17" bgcolor="#fcfcfc">
                                            <td valign="top" colspan="1" style="font-size:1px;line-height:1px">
                                                <img border="0" style="display:block;border:0;width:1px;height:17px"
                                                    alt="" title="" width="1" height="17" class="CToWUd">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="1" valign="top" align="left" width="550">
                                                <font face="Arial, Helvetica, sans-serif" size="2"
                                                    style="font-size:15px;line-height:22px;color:#04293a">
                                                    <b>Listing information <br></b>
                                                </font>
                                            </td>
                                        </tr>
                                        <tr style="line-height:17px" height="17" bgcolor="#fcfcfc">
                                            <td valign="top" colspan="1" style="font-size:1px;line-height:1px">
                                                <img border="0" style="display:block;border:0;width:1px;height:17px"
                                                    alt="" title="" width="1" height="17" class="CToWUd">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="1" valign="top" align="left" bgcolor="#fcfcfc" width="550">
                                                <table cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" border="0"
                                                    style="border-collapse:collapse;width:550px;border: 1px solid #efefef;">
                                                    <tbody>
                                                        <tr style="line-height:10px" height="10" bgcolor="#FFFFFF">
                                                            <td valign="top" colspan="5"
                                                                style="font-size:1px;line-height:1px">
                                                                <img border="0"
                                                                    style="display:block;border:0;width:1px;height:10px"
                                                                    src="https://ci3.googleusercontent.com/proxy/UqmZ6wQxEWlb3II5jomx9MdDmbUg7LFpdWtTTF_qd6-INKrY8qXVjmZQYo-4iGCYc4ff4H7MjQih2X518_RfPcQo58GzdlZx=s0-d-e1-ft#https://static.chrono24.com/images/default/mail/s.gif"
                                                                    alt="" title="" width="1" height="10"
                                                                    class="CToWUd">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="1" rowspan="2" valign="top" align="left"
                                                                bgcolor="#FFFFFF" width="10">
                                                                <img border="0"
                                                                    style="display:block;border:0;width:10px;height:1px"
                                                                    src="https://ci3.googleusercontent.com/proxy/UqmZ6wQxEWlb3II5jomx9MdDmbUg7LFpdWtTTF_qd6-INKrY8qXVjmZQYo-4iGCYc4ff4H7MjQih2X518_RfPcQo58GzdlZx=s0-d-e1-ft#https://static.chrono24.com/images/default/mail/s.gif"
                                                                    alt="" title="" width="10" height="1"
                                                                    class="CToWUd">
                                                            </td>
                                                            <td colspan="1" rowspan="2" valign="top" align="center"
                                                                bgcolor="#FFFFFF" width="85">
                                                                <img border="0"
                                                                    style="display:inline;border:0;width:70px;height:70px"
                                                                    src="https://ci6.googleusercontent.com/proxy/WxbgMJXU3U9xTIBePTt0a7GjroNKzfn62L1olY_cOBPg9FNITlZaafUfBf6H9rbdDVBaRxD3EQR76qpZdU5p-DTeo7Hk-noAPHWBkUaikhCkD8P4hbKXtUPx9e0P1RkRNtkhaLJVqwgX=s0-d-e1-ft#https://cdn2.chrono24.com/images/uhren/15298226-e9g39lrfl1z4zo4wqzfs61mh-Square210.jpg"
                                                                    alt="" title="" width="70" height="70"
                                                                    class="CToWUd">
                                                            </td>
                                                            <td colspan="1" rowspan="1" valign="top" align="left"
                                                                bgcolor="#FFFFFF" width="25">
                                                                <img border="0"
                                                                    style="display:block;border:0;width:25px;height:50px"
                                                                    src="https://ci3.googleusercontent.com/proxy/UqmZ6wQxEWlb3II5jomx9MdDmbUg7LFpdWtTTF_qd6-INKrY8qXVjmZQYo-4iGCYc4ff4H7MjQih2X518_RfPcQo58GzdlZx=s0-d-e1-ft#https://static.chrono24.com/images/default/mail/s.gif"
                                                                    alt="" title="" width="25" height="50"
                                                                    class="CToWUd">
                                                            </td>
                                                            <td colspan="1" rowspan="1" valign="top" align="left"
                                                                bgcolor="#FFFFFF" width="550">
                                                                <font face="Arial, Helvetica, sans-serif" size="2"
                                                                    style="font-size:16px;line-height:22px;color:#04293a">
                                                                    {{$product->brand_name .' '.$product->model_name}}
                                                                </font>
                                                            </td>
                                                            <td colspan="1" rowspan="2" valign="top" align="left"
                                                                bgcolor="#FFFFFF" width="10">
                                                                <img border="0"
                                                                    style="display:block;border:0;width:10px;height:1px"
                                                                    src="https://ci3.googleusercontent.com/proxy/UqmZ6wQxEWlb3II5jomx9MdDmbUg7LFpdWtTTF_qd6-INKrY8qXVjmZQYo-4iGCYc4ff4H7MjQih2X518_RfPcQo58GzdlZx=s0-d-e1-ft#https://static.chrono24.com/images/default/mail/s.gif"
                                                                    alt="" title="" width="10" height="1"
                                                                    class="CToWUd">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="1" rowspan="1" valign="top" align="left"
                                                                bgcolor="#FFFFFF" width="25">
                                                                <img border="0"
                                                                    style="display:block;border:0;width:25px;height:20px"
                                                                    src="https://ci3.googleusercontent.com/proxy/UqmZ6wQxEWlb3II5jomx9MdDmbUg7LFpdWtTTF_qd6-INKrY8qXVjmZQYo-4iGCYc4ff4H7MjQih2X518_RfPcQo58GzdlZx=s0-d-e1-ft#https://static.chrono24.com/images/default/mail/s.gif"
                                                                    alt="" title="" width="25" height="20"
                                                                    class="CToWUd">
                                                            </td>
                                                            <td colspan="1" rowspan="1" valign="top" align="left"
                                                                bgcolor="#FFFFFF" width="550">
                                                                <font face="Arial, Helvetica, sans-serif" size="2"
                                                                    style="font-size:16px;line-height:22px;color:#04293a">
                                                                    <b>{{currency()->convert(floatval($product->value), 'USD', currency()->getUserCurrency())}} </b>
                                                                </font>
                                                            </td>
                                                        </tr>
                                                        <tr style="line-height:10px" height="10" bgcolor="#FFFFFF">
                                                            <td valign="top" colspan="5"
                                                                style="font-size:1px;line-height:1px">
                                                                <img border="0"
                                                                    style="display:block;border:0;width:1px;height:10px"
                                                                    src="https://ci3.googleusercontent.com/proxy/UqmZ6wQxEWlb3II5jomx9MdDmbUg7LFpdWtTTF_qd6-INKrY8qXVjmZQYo-4iGCYc4ff4H7MjQih2X518_RfPcQo58GzdlZx=s0-d-e1-ft#https://static.chrono24.com/images/default/mail/s.gif"
                                                                    alt="" title="" width="1" height="10"
                                                                    class="CToWUd">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="line-height:17px" height="17" bgcolor="#fcfcfc">
                                            <td valign="top" colspan="1" style="font-size:1px;line-height:1px">
                                                <img border="0" style="display:block;border:0;width:1px;height:17px"
                                                    src="" alt="" title="" width="1" height="17" class="CToWUd">
                                            </td>
                                        </tr>
                                        <tr style="line-height:17px" height="17" bgcolor="#fcfcfc">
                                            <td valign="top" colspan="1" style="font-size:1px;line-height:1px">
                                                <img border="0" style="display:block;border:0;width:1px;height:17px"
                                                    alt="" title="" width="1" height="17" class="CToWUd">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="1" valign="top" align="left" width="550">
                                                <font face="Arial, Helvetica, sans-serif" size="2"
                                                    style="font-size:15px;line-height:22px;color:#04293a">
                                                    <b>Price Suggestion<br></b>
                                                </font>
                                            </td>
                                        </tr>

                                        <tr style="line-height:17px" height="17" bgcolor="#fcfcfc">
                                            <td valign="top" colspan="1" style="font-size:1px;line-height:1px">
                                                <img border="0" style="display:block;border:0;width:1px;height:17px"
                                                    src="" alt="" title="" width="1" height="17" class="CToWUd">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="1" valign="top" align="left" bgcolor="#fcfcfc" width="550">
                                                <table cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" border="0"
                                                    style="border-collapse:collapse;width:550px;border: 1px solid #efefef;">
                                                    <tbody>
                                                        <tr style="line-height:10px" height="10" bgcolor="#FFFFFF">
                                                            <td valign="top" colspan="5" style="padding: 10px;">
                                                                <font face="Arial, Helvetica, sans-serif" size="2"
                                                                    style="font-size:15px;line-height:22px;color:#04293a">
                                                                    Seller : </font>
                                                            </td>
                                                            <td valign="top" colspan="5" style="padding: 10px">
                                                                <font face="Arial, Helvetica, sans-serif" size="2"
                                                                    style="font-size:15px;line-height:22px;color:#04293a">
                                                                    {{$agent->first_name .' '.$agent->last_name}}</font>
                                                            </td>
                                                        </tr>
                                                        <tr style="line-height:10px" height="10" bgcolor="#FFFFFF">
                                                            <td valign="top" colspan="5" style="padding: 10px;">
                                                                <font face="Arial, Helvetica, sans-serif" size="2"
                                                                    style="font-size:15px;line-height:22px;color:#04293a">
                                                                    Your Price Suggestion : </font>
                                                            </td>
                                                            <td valign="top" colspan="5" style="padding: 10px">
                                                                <font face="Arial, Helvetica, sans-serif" size="2"
                                                                    style="font-size:15px;line-height:22px;color:#04293a">
                                                                    {{currency()->convert(floatval($suggestPrice), currency()->getUserCurrency(), currency()->getUserCurrency())}}</font>
                                                            </td>
                                                        </tr>
                                                        <tr style="line-height:10px" height="10" bgcolor="#FFFFFF">
                                                            <td valign="top" colspan="5" style="padding: 10px;">
                                                                <font face="Arial, Helvetica, sans-serif" size="2"
                                                                    style="font-size:15px;line-height:22px;color:#04293a">
                                                                    Status : </font>
                                                            </td>
                                                            <td valign="top" colspan="5" style="padding: 10px">
                                                                <font face="Arial, Helvetica, sans-serif" size="2"
                                                                    style="font-size:15px;line-height:22px;color:#04293a">
                                                                    Offer Requested</font>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="line-height:17px" height="17" bgcolor="#fcfcfc">
                                            <td valign="top" colspan="1" style="font-size:1px;line-height:1px">
                                                <img border="0" style="display:block;border:0;width:1px;height:17px"
                                                    src="" alt="" title="" width="1" height="17" class="CToWUd">
                                            </td>
                                        </tr>
                                        <tr style="line-height:17px" height="17" bgcolor="#fcfcfc">
                                            <td valign="top" colspan="1" style="font-size:1px;line-height:1px">
                                                <img border="0" style="display:block;border:0;width:1px;height:17px"
                                                    src="" alt="" title="" width="1" height="17" class="CToWUd">
                                            </td>
                                        </tr>
                                        <tr style="line-height:17px" height="17" bgcolor="#fcfcfc">
                                            <td valign="top" colspan="1" style="font-size:1px;line-height:1px">
                                                <img border="0" style="display:block;border:0;width:1px;height:17px"
                                                    src="" alt="" title="" width="1" height="17" class="CToWUd">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="1" valign="top" align="left" bgcolor="#fcfcfc" width="550">
                                                <font face="Arial, Helvetica, sans-serif" size="2"
                                                    style="font-size:16px;line-height:22px;color:#04293a">
                                                    Kind regards,<br>Your Tangible Team
                                                </font>
                                            </td>
                                        </tr>
                                        <tr style="line-height:17px" height="17" bgcolor="#fcfcfc">
                                            <td valign="top" colspan="1" style="font-size:1px;line-height:1px">
                                                <img border="0" style="display:block;border:0;width:1px;height:17px"
                                                    src="" alt="" title="" width="1" height="17" class="CToWUd">
                                            </td>
                                        </tr>
                                        <tr style="line-height:17px" height="17" bgcolor="#fcfcfc">
                                            <td valign="top" colspan="1" style="font-size:1px;line-height:1px">
                                                <img border="0" style="display:block;border:0;width:1px;height:17px"
                                                    src="" alt="" title="" width="1" height="17" class="CToWUd">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td valign="top" align="left" bgcolor="#fcfcfc" width="25">
                                <img border="0" style="display:block;border:0;width:25px;height:1px" src="" alt=""
                                    title="" width="25" height="1" class="CToWUd">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table cellspacing="0" cellpadding="0" border="0" bgcolor="#fcfcfc"
                    style="border-collapse:collapse;width:600px">
                    <tbody>
                        <tr style="line-height:17px" height="17" bgcolor="#fcfcfc">
                            <td valign="top" colspan="1" style="font-size:1px;line-height:1px">
                                <img border="0" style="display:block;border:0;width:1px;height:17px" src="" alt=""
                                    title="" width="1" height="17" class="CToWUd">
                            </td>
                        </tr>
                    </tbody>    
                </table>
               
                
            <div style="background-color:#EBEEEF;">
                <div class="block-grid" style="min-width: 320px; max-width: 635px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: #EBEEEF;">
                <div style="border-collapse: collapse;display: table;width: 100%;background-color:#EBEEEF;">               
                <div class="col num12" style="min-width: 320px; max-width: 635px; display: table-cell; vertical-align: top; width: 635px;">
                <div class="col_cont" style="width:100% !important;">
                
                <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:20px; padding-bottom:30px; padding-right: 0px; padding-left: 0px;">
                
                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" valign="top" width="100%">
                <tbody><tr style="vertical-align: top;" valign="top">
                <td align="center" style="word-break: break-word; vertical-align: top; padding-top: 15px; padding-bottom: 15px; padding-left: 0px; padding-right: 0px; text-align: center; font-size: 0px;" valign="top">
                <input class="menu-checkbox" id="menu3jc1dp" style="display:none !important;max-height:0;visibility:hidden;" type="checkbox">
                <div class="menu-trigger" style="display:none;max-height:0px;max-width:0px;font-size:0px;overflow:hidden;"> <label class="menu-label" for="menu3jc1dp" style="height:36px;width:36px;display:inline-block;cursor:pointer;mso-hide:all;user-select:none;align:center;text-align:center;color:#ffffff;text-decoration:none;background-color:#b11f2c;"><span class="menu-open" style="mso-hide:all;font-size:26px;line-height:36px;">☰</span><span class="menu-close" style="display:none;mso-hide:all;font-size:26px;line-height:36px;">✕</span></label></div>
                <div class="menu-links">
                <a href="https://www.example.com" style="padding-top:5px;padding-bottom:5px;padding-left:20px;padding-right:20px;display:inline;color:#515151;font-family:Lucida Sans Unicode, Lucida Grande, Lucida Sans, Geneva, Verdana, sans-serif;font-size:14px;text-decoration:none;letter-spacing:undefined;">Visit Our Website</a>
                <span class="sep" style="font-size:14px;font-family:Lucida Sans Unicode, Lucida Grande, Lucida Sans, Geneva, Verdana, sans-serif;color:#515151;">|</span>
                <a href="tel:+11234567890" style="padding-top:5px;padding-bottom:5px;padding-left:20px;padding-right:20px;display:inline;color:#515151;font-family:Lucida Sans Unicode, Lucida Grande, Lucida Sans, Geneva, Verdana, sans-serif;font-size:14px;text-decoration:none;letter-spacing:undefined;">Call Us</a>
                </div>
                </td>
                </tr>
                </tbody></table>
                <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
                <tbody>
                <tr style="vertical-align: top;" valign="top">
                <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;" valign="top">
                <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 1px solid #BBBBBB; width: 100%;" valign="top" width="100%">
                <tbody>
                <tr style="vertical-align: top;" valign="top">
                <td style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
                </tr>
                </tbody>
                </table>
                </td>
                </tr>
                </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
                <tbody>
                <tr style="vertical-align: top;" valign="top">
                <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px;" valign="top">
                <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="15" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 0px solid transparent; height: 15px; width: 100%;" valign="top" width="100%">
                <tbody>
                <tr style="vertical-align: top;" valign="top">
                <td height="15" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
                </tr>
                </tbody>
                </table>
                </td>
                </tr>
                </tbody>
                </table>
                <table cellpadding="0" cellspacing="0" class="social_icons" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" valign="top" width="100%">
                <tbody>
                <tr style="vertical-align: top;" valign="top">
                <td style="word-break: break-word; vertical-align: top; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;" valign="top">
                <table align="center" cellpadding="0" cellspacing="0" class="social_table" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-tspace: 0; mso-table-rspace: 0; mso-table-bspace: 0; mso-table-lspace: 0;" valign="top">
                <tbody>
                <tr align="center" style="vertical-align: top; display: inline-block; text-align: center;" valign="top">
                <td style="word-break: break-word; vertical-align: top; padding-bottom: 0; padding-right: 10px; padding-left: 10px;" valign="top"><a href="https://www.facebook.com/" target="_blank"><img alt="Facebook" height="32" src="https://img.icons8.com/color/64/000000/facebook-new.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; display: block;" title="facebook" width="32"></a></td>
                <td style="word-break: break-word; vertical-align: top; padding-bottom: 0; padding-right: 10px; padding-left: 10px;" valign="top"><a href="https://www.twitter.com/" target="_blank"><img alt="Twitter" height="32" src="https://img.icons8.com/color/64/000000/twitter--v1.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; display: block;" title="twitter" width="32"></a></td>
                <td style="word-break: break-word; vertical-align: top; padding-bottom: 0; padding-right: 10px; padding-left: 10px;" valign="top"><a href="https://www.linkedin.com/" target="_blank"><img alt="Linkedin" height="32" src="https://img.icons8.com/fluent/48/000000/linkedin.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; display: block;" title="linkedin" width="32"></a></td>
                <td style="word-break: break-word; vertical-align: top; padding-bottom: 0; padding-right: 10px; padding-left: 10px;" valign="top"><a href="https://www.instagram.com/" target="_blank"><img alt="Instagram" height="32" src="https://img.icons8.com/fluent/48/000000/instagram-new.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; display: block;" title="instagram" width="32"></a></td>
                </tr>
                </tbody>
                </table>
                </td>
                </tr>
                </tbody>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
                <tbody>
                <tr style="vertical-align: top;" valign="top">
                <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px;" valign="top">
                <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="15" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 0px solid transparent; height: 15px; width: 100%;" valign="top" width="100%">
                <tbody>
                <tr style="vertical-align: top;" valign="top">
                <td height="15" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
                </tr>
                </tbody>
                </table>
                </td>
                </tr>
                </tbody>
                </table>
                
                <div style="color:#888888;font-family:Lucida Sans Unicode, Lucida Grande, Lucida Sans, Geneva, Verdana, sans-serif;line-height:1.2;padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:10px;">
                <div class="txtTinyMce-wrapper" style="line-height: 1.2; font-size: 12px; font-family: Lucida Sans Unicode, Lucida Grande, Lucida Sans, Geneva, Verdana, sans-serif; color: #888888; mso-line-height-alt: 14px;">
                <p style="font-size: 12px; line-height: 1.2; word-break: break-word; text-align: center; font-family: Lucida Sans Unicode, Lucida Grande, Lucida Sans, Geneva, Verdana, sans-serif; mso-line-height-alt: 14px; margin: 0;">
                   Tangible Listings, Haid-und-Neu-Str. 18, D-76131 Karlsruhe,Germany
                </p>
                </div>
                </div>
                <!--[if mso]></td></tr></table><![endif]-->
                <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 5px; padding-bottom: 5px; font-family: Verdana, sans-serif"><![endif]-->
                <div style="color:#888888;font-family:Lucida Sans Unicode, Lucida Grande, Lucida Sans, Geneva, Verdana, sans-serif;line-height:1.2;padding-top:5px;padding-right:10px;padding-bottom:5px;padding-left:10px;">
                <div class="txtTinyMce-wrapper" style="line-height: 1.2; font-size: 12px; font-family: Lucida Sans Unicode, Lucida Grande, Lucida Sans, Geneva, Verdana, sans-serif; color: #888888; mso-line-height-alt: 14px;">
                <p style="font-size: 12px; line-height: 1.2; word-break: break-word; text-align: center; font-family: Lucida Sans Unicode, Lucida Grande, Lucida Sans, Geneva, Verdana, sans-serif; mso-line-height-alt: 14px;  margin-bottom: 10pxb;">
                   E-Mail: info@tangiblelistings.com                 
                </p>
                <p style="font-size: 12px; line-height: 1.2; word-break: break-word; text-align: center; font-family: Lucida Sans Unicode, Lucida Grande, Lucida Sans, Geneva, Verdana, sans-serif; mso-line-height-alt: 14px; margin-bottom: 10px;">
                   Managing directors: James, Jack                  
                </p>
                <p style="font-size: 12px; line-height: 1.2; word-break: break-word; text-align: center; font-family: Lucida Sans Unicode, Lucida Grande, Lucida Sans, Geneva, Verdana, sans-serif; mso-line-height-alt: 14px; margin: 0;">
                   VAT ID No.: DE269055220                 
                </p>              
                </div>
                </div>               
                </div>            
                </div>
                </div>
                
                </div>
                </div>
                </div>       
            </td>
        </tr>
    </tbody>
</table>