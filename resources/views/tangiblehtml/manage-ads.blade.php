<!DOCTYPE html>
<html lang="en">

    @include('tangiblehtml.headheader')
    <style>
        #chartdiv {
          width: 100%;
          height: 500px;
        }        
        </style>

<body>
    @include('tangiblehtml.innerheader')
   <div class="custom-top-home-wrapper pt-5 ">
        <div class="container">
            <div class="main-heading mt-5">
                <h3>Manage Ads</h3>
                <p><a href="{{ URL::to('/') }}/mylistings"> <i class="fa fa-arrow-left mr-2"></i>Back to other ads</a></p>
            </div>
            <div class="row d-flex align-items-center border p-3 mb-3">
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="border p-2">
                        <div class="ads-view position-relative">                          
                            <div class="home-list-item">
                                <a class="home-view" href="#"><img src="{{'/uploads/'.$images->firstWhere('listing_id','=', $listing->id)->filename}}" alt="" class="img-fluid"></a>
                            </div>                            
                        </div>                        
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="inner">
                        <div class="home-content mt-1" >                            
                            <h2 class="mb-1"><a href="#">{{$listing->ad_title}}</a></h2>                           
                            <div class="ads-details mt-2">
                                <!-- <span class="mr-5">ID  <strong>{{$listing->id}}</strong></span>                                -->
                                <span>Created <strong>{{date('D, d M, yy', strtotime($listing->created_at))}}</strong></span>  

                                <p>Adverisement Status: <strong><?php if($listing->status == 1){ echo "Active";}elseif($listing->status == 0){ echo "Inactive";}elseif($listing->status == 2){ echo "Sold";} ?></strong></p>  

                            </div>              
                        </div>                       
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="inner">

                        <!--                         
                        <div class="">
                        <button class="btn btn-success m-auto mb-3">Resume Ads</button>                           
                        </div>
                        <div class="mt-3">                          
                        <button class="btn btn-outline-dark m-auto">Remove Ads</button>
                        </div>

                        <div class="mt-3">   
                        <button class="btn btn-outline-dark m-auto">Sold</button>                           
                        </div> -->

                         @if($category == 'automobile')
                         <div class="mt-3">                          
                            <a href="/editautoads/{{$listing->id}}" class="btn btn-success m-auto mb-3">Edit Now</a>
                         </div>
                         @endif
                         @if($category == 'watch')
                         <div class="mt-3">                          
                            <a href="/editwatchads/{{$listing->id}}" class="btn btn-success m-auto mb-3">Edit Now</a>
                         </div>
                         @endif
                    </div>
                </div>
            </div>
            <div class="row">
            <div class="adsAnalytics col-md-12 border  p-3 mb-3">
                <div class="header pb-6">
                    <div class="heading p-3 main-heading stm-border-bottom-unit bg-color-border">
                        <h3 class="text-center">Check your ads Performance</h3>
                    </div>                    
                    <div class="container-fluid">
                      <div class="header-body">              
                        <div class="row">
                          <div class="col-xl-3 col-md-6">
                            <div class="card card-stats">
                              <!-- Card body -->
                              <div class="card-body p-0">
                                <div class="row">
                                  <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Total Views</h5>
                                    <span class="h2 font-weight-bold mb-0">350,897</span>
                                  </div>
                                  <div class="col-auto">
                                    <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                      <i class="ni ni-active-40"></i>
                                    </div>
                                  </div>
                                </div>
                                <!-- <p class="mt-3 mb-0 text-sm">
                                  <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                                  <span class="text-nowrap">Since last month</span>
                                </p> -->
                              </div>
                            </div>
                          </div>
                          <div class="col-xl-3 col-md-6">
                            <div class="card card-stats">
                              <!-- Card body -->
                              <div class="card-body p-0">
                                <div class="row">
                                  <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Saves</h5>
                                    <span class="h2 font-weight-bold mb-0">2,356</span>
                                  </div>
                                </div>
                                <!-- <p class="mt-3 mb-0 text-sm">
                                  <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                                  <span class="text-nowrap">Since last month</span>
                                </p> -->
                              </div>
                            </div>
                          </div>
                          <div class="col-xl-3 col-md-6">
                            <div class="card card-stats">
                              <!-- Card body -->
                              <div class="card-body p-0">
                                <div class="row">
                                  <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Enquiry</h5>
                                    <span class="h2 font-weight-bold mb-0">924</span>
                                  </div>
                                </div>
                                <!-- <p class="mt-3 mb-0 text-sm">
                                  <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                                  <span class="text-nowrap">Since last month</span>
                                </p> -->
                              </div>
                            </div>
                          </div>
                          <?php /* ?>
                          <div class="col-xl-3 col-md-6">
                            <div class="card card-stats">
                              <!-- Card body -->
                              <div class="card-body p-0">
                                <div class="row">
                                  <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Performance</h5>
                                    <span class="h2 font-weight-bold mb-0">49,65%</span>
                                  </div>
                                </div>
                                <p class="mt-3 mb-0 text-sm">
                                  <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                                  <span class="text-nowrap">Since last month</span>
                                </p>
                              </div>
                            </div>
                          </div> <?php */ ?>
                        </div>
                        <!-- <div class="row border-top pt-3">
                            <div class="Chart w-100">
                                <div id="chartdiv"></div>
                            </div>
                        </div> -->
                        <!-- <div class="row border-top pt-3">
                            <div class="boostAdd m-auto">
                                <button class="btn btn-outline-success p-3">Boost your Add Views </button>
                            </div>
                        </div> -->
                      </div>
                    </div>
                  </div>                  
            </div>
            </div>


        </div>
    </div>
    @include('tangiblehtml.innerfooter')
    
    <script src="/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/local.js"></script>
    <!-- Resources -->
    <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

    <script>
    $(document).on('click', '.search-filter-panel .dropdown-menu', function (e) {
      e.stopPropagation();   
    });
    </script>
    <script>
        am4core.ready(function() {
        
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end
        
        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart);
        
        // Add data
        chart.data = generateChartData();
        
        // Create axes
        var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.renderer.minGridDistance = 50;
        
        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        
        // Create series
        var series = chart.series.push(new am4charts.LineSeries());
        series.dataFields.valueY = "visits";
        series.dataFields.dateX = "date";
        series.strokeWidth = 2;
        series.minBulletDistance = 10;
        series.tooltipText = "{valueY}";
        series.tooltip.pointerOrientation = "vertical";
        series.tooltip.background.cornerRadius = 20;
        series.tooltip.background.fillOpacity = 0.5;
        series.tooltip.label.padding(12,12,12,12)
        
        // Add scrollbar
        chart.scrollbarX = new am4charts.XYChartScrollbar();
        chart.scrollbarX.series.push(series);
        
        // Add cursor
        chart.cursor = new am4charts.XYCursor();
        chart.cursor.xAxis = dateAxis;
        chart.cursor.snapToSeries = series;
        
        function generateChartData() {
            var chartData = [];
            var firstDate = new Date();
            firstDate.setDate(firstDate.getDate() - 1000);
            var visits = 1200;
            for (var i = 0; i < 500; i++) {
                // we create date objects here. In your data, you can have date strings
                // and then set format of your dates using chart.dataDateFormat property,
                // however when possible, use date objects, as this will speed up chart rendering.
                var newDate = new Date(firstDate);
                newDate.setDate(newDate.getDate() + i);
                
                visits += Math.round((Math.random()<0.5?1:-1)*Math.random()*10);
        
                chartData.push({
                    date: newDate,
                    visits: visits
                });
            }
            return chartData;
        }
        
        }); // end am4core.ready()
        </script>        
</body>

</html>