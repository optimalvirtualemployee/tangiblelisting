<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Make an offer</title>
    <link href="https://fonts.googleapis.com/css2?family=Muli:wght@200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https: //use.fontawesome.com/releases/v5.8.2/css/all.css">   
    <link rel="stylesheet" href="/assets/css/hover-min.css" />
    <link rel="stylesheet" href="/assets/css/slick.css" />
    <link rel="stylesheet" href="/assets/css/font-style.css">
    <link rel="stylesheet" href="/assets/css/local.css">
    <link rel="stylesheet" href="/assets/css/responsive.css">
</head>

<body>
    @include('tangiblehtml.innerheader')

    <section class="become-seller-from overlay-wrapper pt-0">
      
    </section>
    <div class="container mt-5">
      <div class="border-bottom">        
          <h3>Make an offer</h3>
        <p><strong>Jan 13, 2021</strong></p>  
    </div> 
      <div class="row m-0">        
        <div class="col-xl-3 order-xl-2 position-relative">
          <div class="card card-profile pt-3 position-sticky stickyView pb-1">          
            <div class="row justify-content-center">
              <div class="col-lg-12">
                <div class="card-profile-image">
                  <a href="#">
                    <img src="{{url('uploads/',$images->filename)}}" class="img-fluid">
                  </a>
                  <div class="text-center">
                    <div class="h5 mt-2">
                      <i class="ni business_briefcase-24 mr-2"></i>{{$enquiry->ad_title}}
                    </div>  
                    <p><div class="country-flag">
                        <img src="{{url('uploads/',$enquiry->countryflag)}}" alt="">
                        <p class="c-code"> {{$enquiry->agent_name}}</p>
                    </div></p>  
                    <div class="d-flex justify-content-between border p-2 font-weight-bold">
                        <div>List Price</div>
                        <div>{{currency()->convert(floatval($enquiry->value), 'USD', currency()->getUserCurrency())}}</div>
                    </div> 
                    <div class="d-flex justify-content-center p-2 font-weight-bold">
                        <a href="#" class="text-muted"> View List</i></a>
                    </div>               
                  </div>                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-9 order-xl-1">
            <div class="container-fluid p-0">
                <div class="row justify-content-center">
                    <div class="col-lg-12 p-0">                                                  
                            <form id="msform">
                                <!-- progressbar -->
                                <ul id="progressbar" class="mb-0">
                                    <li class="active" id="account"><strong>Make an offer</strong></li>
                                    <li id="personal"><strong>Review offer</strong></li>
                                    <li id="payment"><strong>Submit</strong></li>                                    
                                </ul> 
                                <!--Make an offer section starts-->                              
                                <fieldset>
                                    <div class="form-card">
                                        <div class="row m-0">                                            
                                           <!-- <div class="buyerSuggestedPrice mb-3 w-100">
                                               <h5>Buyer's Price Suggestion (incl. all costs)</h5>
                                               <div class="suggCn">
                                                 <strong>Buyer suggested Price :</strong> <span>$5030</span>.
                                              </div>
                                           </div>    -->
                                           <!-- Buyer Information Section --> 
                                           <input type="hidden" id="enquiryId" value="{{$enquiry->id}}" />
                                           <div class="buyerInfo mb-3 mt-3 pt-3 w-100">
                                                <h5> <i class="fa fa-user mr-2"></i> Buyer Details</h5>
                                                <table class="table border bg-light">
                                                    <tr><th>Name</th><td>{{$enquiry->user_name}}</td></tr>
                                                    @if($country->where('id', $enquiry->userCountryId)->first() != null)
                                                    <tr><th>Country</th><td>{{$country->where('id', $enquiry->userCountryId)->first()->country_name}}</td></tr>
                                                    @endif
                                                    <tr><th>Contact</th><td>{{$enquiry->userMobile}}</td></tr>
                                                    <tr><th>Email</th><td>{{$enquiry->userEmail}}</td></tr>                                                  
                                                    <!-- <tr><td colspan="2"><strong>Note : </strong>You will receive full shipping address once the buyer has accepted your offer and paid the purchase price</td></tr> -->
                                                </table>
                                           </div>
                                           <!-- Buyer Information Section ends -->  
                                           <!-- Price Section -->
                                           <div class="priceSetup mt-3 pt-3 border-top w-100">
                                            <h5> <i class="fa fa-dollar mr-2"></i>Price</h5>
                                            <div class="row">                                                
                                                <div class="col-lg-6">
                                                    <div class="inner">
                                                        <label class="">Item Price</label>
                                                        <div class="input-group mb-3">
                                                        <?php $submitPrice = currency()->convert(floatval($enquiry->submitprice), 'USD', currency()->getUserCurrency(), false);
                                                        $submitPrice = (double)$submitPrice;
                                                        ?>
                                                            <input type="text" class="form-control itemPrice" placeholder="price" value="{{round($submitPrice, 2)}}"  required="">
                                                            <div class="input-group-append">
                                                            <span class="input-group-text">{{currency()->getUserCurrency()}}</span>
                                                            </div>
                                                        </div>
                                                     </div>
                                                </div>  
                                            </div>          
                                            <div class="row">                                                
                                                <div class="col-lg-6">
                                                    <div class="inner">
                                                        <label class="">Commision Fee</label>
                                                        <div class="input-group mb-3"> 
                                                            <input type="number" class="form-control commisionFee" placeholder="price"  value="0"  disabled>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">{{currency()->getUserCurrency()}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">                                                
                                                <div class="col-lg-6">
                                                    <label class="">Revenue</label>
                                                    <div class="inner">
                                                        <div class="input-group mb-3">    
                                                            <input type="number" class="form-control revenueFee" placeholder="price"  value=""  disabled>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">{{currency()->getUserCurrency()}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                
                                           </div>   
                                           <div class="row">
                                               <div class="col-lg-12">
                                                   <div class="innerNote text-left">
                                                       Buyer price suggestion (incl all costs) : <strong>{{currency()->convert(floatval($enquiry->submitprice), 'USD', currency()->getUserCurrency())}}</strong>
                                                   </div>
                                               </div>
                                           </div>
                                        </div>    
                                        <!-- Price Section ends -->   
                                        <!-- Listing Section -->
                                        <div class="listingSetup mt-5 pt-5 border-top w-100">
                                            <h5><i class="fa fa-truck mr-2"></i>Delivery</h5>
                                            <div class="row">                                                
                                                <div class="col-lg-6">
                                                    <div class="inner">
                                                        <label class="">Delivery time</label>
                                                        <div class="input-group mb-3"> 
                                                            <select name="" id="deliveryTime" class="form-control">
                                                                <option value="3-5 days">3-5 days</option>
                                                                <option value="1- 2 weeks">1- 2 weeks</option>
                                                                <option value="2 - 3 weeks">2 - 3 weeks</option>
                                                            </select>
                                                        </div>
                                                     </div>
                                                </div>  
                                            </div>          
                                            <div class="row">                                              
                                                <div class="col-lg-6">
                                                    <div class="inner">
                                                        <label class="">Offer valide till</label>
                                                        <div class="input-group mb-3"> 
                                                            <select name="" id="offerValid" class="form-control">
                                                                <option value="3">3 days</option>
                                                                <option value="6">6 days</option>
                                                                <option value="10">10 days</option>
                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>            
                                        </div>    
                                        <!-- Listing Section ends -->   
                                        <!-- Message to buyer Section -->
                                        <div class="messageSetup mt-5 pt-5 border-top mb-3 border-bottom w-100">
                                            <h5> <i class="fa fa-envelope"></i> Message buyer</h5>                                            
                                            <div class="row">                                                
                                                <div class="col-lg-12">                                                    
                                                    <div class="inner">
                                                        <label class="">Message the buyer about any other further information’</label>
                                                        <div class="input-group mb-3"> 
                                                            <textarea class="form-control" name="" id="messageBuyer" cols="30" rows="3"></textarea>
                                                            
                                                        </div>
                                                        <p class="d-none text-danger" id="textAreaError">This field is required !!</p>
                                                     </div>
                                                </div>  
                                            </div>
                                        </div>    
                                        <!-- Message to buyer Section ends -->                                       
                                        </div>
                                    </div> 
                                    
                                    <input type="button" name="next" class="next action-button" value="Continue" id="continueThis"/>
                                    <button class="action-button-previous"><i class="fa fa-times mr-2"></i> Delete Request </button>
                                </fieldset>
                                <!--Make an offer section ends-->
                                <!--Check an offer section starts-->
                                <fieldset>
                                    <div class="form-card">
                                        <div class="row m-0"> 
                                           <!-- Buyer Information Section Review --> 
                                           <div class="buyerInfo mb-3 mt-3 pt-3 border-top w-100">
                                                <h5> <i class="fa fa-user mr-2"></i> Buyer Details</h5>
                                                <table class="table border bg-light">
                                                    <tr><th>Name</th><td>{{$enquiry->user_name}}</td></tr>
                                                    @if($country->where('id', $enquiry->userCountryId)->first() != null)
                                                    <tr><th>Country</th><td>{{$country->where('id', $enquiry->userCountryId)->first()->country_name}}</td></tr>
                                                    @endif
                                                    <tr><th>Contact</th><td>{{$enquiry->userMobile}}</td></tr>
                                                    <tr><th>Email</th><td>{{$enquiry->userEmail}}</td></tr>                                                   
                                                </table>
                                           </div>
                                           <!-- Buyer Information Section Review ends -->  
                                           <!-- Offer Information Section Review -->
                                           <div class="priceSetup mt-3 pt-3 border-top w-100">
                                            <h5> <i class="fa fa-info mr-2"></i>Offer Details</h5>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="inner table-responsive">
                                                        <table class="table border" id="offerDetails">
                                                            <tr><th>Total Price</th><td>5030</td></tr>
                                                            <tr><th>Offer Valid till</th><td>Monday Jan 27, 2019</td></tr>
                                                            <tr><th>Message to the Buyer</th><td>Hey, This is the least I could do for you.</td></tr>
                                                            <tr><th><label class="">Delivery time</label></th><td>3 -5 days</td></tr>
                                                        </table>
                                                    </div>
                                                </div>                                                 
                                            </div>                                             
                                        </div>    
                                        <!-- Offer Information Review Section ends -->
                                        <!-- Price Details Section Review-->
                                        <div class="priceDetailsReview mt-5 pt-5 border-top mb-3 w-100">
                                            <h5> <i class="fa fa-dollar mr-2"></i>Price Details</h5>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="inner table-responsive">
                                                        <table class="table border" id="priceDetails">
                                                            <tr><th>Item Price</th><td class="text-right">AU$ 5030</td></tr>
                                                            <tr><th>Commision Fee</th><td class="text-right">AU$ 250</td></tr>
                                                            <tr><th>Revenue</th><td class="text-right"> AU$ 5280</td></tr>                                                          
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>    
                                        <!-- Price Details to buyer Section Review ends -->
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="conditions border-top pt-3">
                                                    <div class="ifnotvalid">
                                                        <input type="checkbox" id="termsConditons" name="termsConditons" value="termsConditons" onclick ="validateIt()";>
                                                        <label for="termsConditons">I have read and accept the <a href="#">Terms and Conditions of Sale.</a></label> <br>
                                                        <p class="d-none text-danger">Please agree to the terms & conditions</p>
                                                    </div>
                                                    <div class="ifnotvalid">
                                                        <input type="checkbox" id="awareof" name="awareof" value="awareof" onclick ="validateIt()";>
                                                        <label for="awareof">I am aware that this purchase may incure <a href="#">Additional customs duties and import taxes</a></label>
                                                        <p class="d-none text-danger">Please agree to the additional customs duties and import taxes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div> 
                                    <input type="button" name="next" id="submitoffer" class="next finalsubmit action-button" value="Submit" /> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                                </fieldset>
                                <!--Check an offer section ends-->
                                <fieldset>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="inner border-bottom text-left mb-3 pb-2 mt-3">                                       
                                                <h5>Your offer has been sent to the buyer</h5>   
                                                <p>If the buyer accepts you offer you will receive confirmation. If the buyer counteroffers, you will receive a counteroffer from the buyer. </p>                                             
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="inner text-left mb-3 pb-2">
                                                <p>if you have any questions regarding the transaction process get in contact with our team.</p>
                                                <p>info@tangiblelistings.com.au </p>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>                        
                    </div>
                </div>
            </div> 
        </div>
      </div>      
    </div>
    
        <div class="support-box">
        <a href="#" class="icon-text hvr-float-shadow" id="icon-anchor">
            <p><i class="fa fa-envelope-o" aria-hidden="true"></i> Ask Us If You Any Queries</p>
        </a>
    </div> 

    <@include('tangiblehtml.innerfooter')


    <script src="/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/intlTelInput.min.js"></script>
    <script src="/assets/js/local.js"></script>
    <script>
$(document).ready(function(){
        var current_fs, next_fs, previous_fs; //fieldsets
        var opacity;
        var current = 1;
        var steps = $("fieldset").length;
        var nextStep = true;

        setProgressBar(current);

        $('#submitoffer').on('click',function(){
            var enquiryId = $('#enquiryId').val();
            var finalOfferValue = $('.itemPrice').val();
            var deliveryTime = $('#deliveryTime').val();
            var offerValid = $('#offerValid').val();
            var message =  $('#messageBuyer').val();           
            if($('#termsConditons').prop('checked') && $('#awareof').prop('checked')){
            nextStep = true;
            $.ajax({
            url: "/seller/submitfinaloffer",
            type: "POST",
            data: {enquiryId: enquiryId, finalOfferValue: finalOfferValue, deliveryTime: deliveryTime, offerValid: offerValid, message: message, _token: '{{csrf_token()}}' },
            dataType: 'json',
            success : function(data){

                if(data == "success"){

                    alert("inside success");
                   
                    }                    
                }
            }); 
            }
            else{
               
                if($('#termsConditons').prop('checked') != true){                    
                    $('#termsConditons').parents('.ifnotvalid').find('p').removeClass('d-none');
                }
                if($('#awareof').prop('checked') != true){                    
                    $('#awareof').parents('.ifnotvalid').find('p').removeClass('d-none');
                }               
                nextStep = false;
            }     
        }); 

        $(".next").click(function(){      
        if($('#messageBuyer').val()!='' && nextStep==true)
        {       
        getVariable();
        current_fs = $(this).parent();   
        next_fs = $(this).parent().next();

        //Add Class Active
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
        step: function(now) {
        // for making fielset appear animation
        opacity = 1 - now;

        current_fs.css({
        'display': 'none',
        'position': 'relative'
        });
        next_fs.css({'opacity': opacity});
        },
        duration: 500
        });
        setProgressBar(++current);
        }
        else{
            $('#textAreaError').removeClass('d-none');            
        }

        });

        $(".previous").click(function(){

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        //Remove class active
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset
        previous_fs.show();

        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
        step: function(now) {
        // for making fielset appear animation
        opacity = 1 - now;

        current_fs.css({
        'display': 'none',
        'position': 'relative'
        });
        previous_fs.css({'opacity': opacity});
        },
        duration: 500
        });
        setProgressBar(--current);
        });

        function setProgressBar(curStep){
        var percent = parseFloat(100 / steps) * curStep;
        percent = percent.toFixed();
        $(".progress-bar")
        .css("width",percent+"%")
        }

        $(".submit").click(function(){
            alert('lala');
        return false;
        })       

        $('#messageBuyer').on('change',function(){
            if($(this).text!='')
            {
                $("#textAreaError").addClass('d-none');
                nextStep = true;
            }
            else{
                nextStep = false;
            }
        })
});

function validateIt(){
                if($('#termsConditons').prop('checked') == true){                    
                    $('#termsConditons').parents('.ifnotvalid').find('p').addClass('d-none');                    
                }
                if($('#awareof').prop('checked') == true){                    
                    $('#awareof').parents('.ifnotvalid').find('p').addClass('d-none');                   
                }               
                
}

</script>


<script>
$('.itemPrice').on('change',function(){
const totalAmount = $(this).val();
console.log('total amount',totalAmount);
getTheCommision(totalAmount);
});

 function getTheCommision(totalAmount) {
 const percentToGet = 1.5;

//Calculate the percent.
var percent = (percentToGet / 100) * totalAmount;
const commisionFee = Math.round(percent);
$('.commisionFee').val(commisionFee);
const revenue = totalAmount - commisionFee;
$('.revenueFee').val(revenue)
}

 $("document").ready(function() {
	    setTimeout(function() {
	        $(".itemPrice").trigger('change');
	    },10);
	});

 function getVariable(){
var itemPrice = $('.itemPrice').val();
var commisionFee = $('.commisionFee').val();
var revenueFee = $('.revenueFee').val();

var message = $('#messageBuyer').val();
var deliveryTime = $('#deliveryTime').val();
var offerValid = $('#offerValid').val();


var date = new Date();
date.setDate(date.getDate() + parseInt(offerValid));

var futDate=date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();

var newDate = date.toString('dd-MM-yy');

var offerDetails = `<tr><th>Total Price</th><td>${itemPrice}</td></tr>
    <tr><th>Offer Valid till</th><td>${newDate}</td></tr>
    <tr><th>Message to the Buyer</th><td>${message}</td></tr>
    <tr><th><label class="">Delivery time</label></th><td>${deliveryTime}</td></tr>`;

var priceDetails = `<tr><th>Item Price</th><td id="itemPrice" class="text-right">${itemPrice}</td></tr>
    <tr><th>Commision Fee</th><td class="text-right">${commisionFee}</td></tr>
    <tr><th>Revenue</th><td class="text-right"> ${revenueFee}</td></tr>`;
    
 $('#offerDetails').html(offerDetails);
 $('#priceDetails').html(priceDetails);
	 }



</script>  
</body>

</html>