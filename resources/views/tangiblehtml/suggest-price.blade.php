<!DOCTYPE html>
<html lang="en">

@include('tangiblehtml.headheader')

<body>
    @include('tangiblehtml.innerheader')

    <section class="become-seller-from overlay-wrapper">
      
    </section>
    @if (Route::has('login'))
    @auth
    <div class="container mt-5">
        @if(session()->has('success_msg'))
                <div class="alert alert-success" role="alert">
                {{ session()->get('success_msg') }}
                </div>
                @endif
      <div class="main-heading stm-border-bottom-unit bg-color-border">
        <h3>Your price suggestion</h3>     
    </div> 
      <div class="row">
        <div class="col-xl-4 order-xl-2">
          <div class="card card-profile">          
            <div class="row justify-content-center">
              <div class="col-lg-12">
                <div class="card-profile-image">
                  <a href="#">
                    <img src="{{url('uploads/'.$images->firstWhere('listing_id', $listing->id)->filename)}}" class="rounded-circle">
                  </a>
                  <div class="text-center">
                    <div class="h5 mt-4">
                      <i class="ni business_briefcase-24 mr-2"></i>{{$listing->ad_title}}
                    </div>                   
                  </div>
                  <hr class="my-4">
                </div>
              </div>
            </div>            
            <div class="card-body pt-0 p-1">             
              
              <div class="accordion mt-5" id="accordionExample">
                <div class="">
                    <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <strong class="mb-0">Free Trusted Checkout</strong>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                        <p>The most secure approach to buy the watch you had always wanted. Pay safely by means of an escrow account. The vendor gets your installment simply after you've gotten the watch..</p>
                        </div>
                    </div>
              </div>
                <div class="">
                    <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <strong class="mb-0">Buyer Protection guaranteed</strong>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            <p>In the uncommon case that something doesn't go as arranged, you can depend on our Buyer Protection administration.</p>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <strong class="mb-0">Your data is secure</strong>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                            <p>Tangible carefully clings to all pertinent information assurance laws. 

                                All information that you enter on our site is encoded (SSL) and communicated over a safe Internet association (HTTPS). 
                                
                                Your own data will never be distributed and is possibly utilized during the enrollment cycle and when selling things on Tangible.</p>
                        </div>
                    </div>
                </div>
            </div>
            </div>
          </div>
        </div>
        <div class="col-xl-8 order-xl-1">
          <div class="card">           
            <div class="card-body">
              <form >              
                <div class="pl-lg-4">
                    <div class="row">
                        <div class="col-lg-12">
                          <div class="form-group">
                            <label class="form-control-label" for="input-first-name">Your price suggestion (incl. all costs)</label>
                            <input type="text" id="input-price-suggest" class="form-control" placeholder="List price (plus shipping costs):  {{currency()->convert(floatval($listing->value), 'USD', currency()->getUserCurrency())}}">
                            <p class="mt-1">List price (plus shipping costs):  {{currency()->convert(floatval($listing->value), 'USD', currency()->getUserCurrency())}}</p>
                          </div>
                        </div>                        
                      </div>
                </div>                
              </form>
            </div>
          </div>
          <div class="card">           
            <div class="card-body">
              <form id="submitPriceForm" method="POST" action="{{ url('/'.$category.'/submitsuggestprice') }}">   
              @csrf           
              <input type="hidden" id="productId" name="productId"  value="{{$listing->id}}">
              <input type="hidden" id="agentId" name="agentId"  value="{{$listing->agent_id}}">
              <input type="hidden" id="createdId" name="createdId"  value="{{$listing->createdID}}">
              <input type="hidden" id="userId" name="userId"  value="{{Auth::user()->id}}">
                <div class="pl-lg-4">
                    <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="form-control-label" for="input-first-name">First name</label>
                            <input type="text" id="input-first-name" name="firstName" class="form-control" placeholder="First name" value="{{Auth::user()->first_name}}" disabled>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="form-control-label" for="input-last-name">Last name</label>
                            <input type="text" id="input-last-name" name="lastName" class="form-control" placeholder="Last name" value="{{Auth::user()->last_name}}" disabled>
                          </div>
                        </div>
                      </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-number">Phone Number</label>
                        <input type="text" id="input-number" name="phoneNumber" class="form-control" placeholder="Mobile Num" value="{{($userDetails->firstWhere('user_id',Auth::user()->id) != null) ? $userDetails->firstWhere('user_id',Auth::user()->id)->user_mobile : ""}}">
                      </div>
                      @error('phoneNumber')
          				<div class="alert alert-danger">This Field is required</div>
       				 @enderror
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Email address</label>
                        <input type="email" id="input-email" name="email" class="form-control" placeholder="{{Auth::user()->email}}" disabled>
                      </div>
                    </div>
                  </div>
                  
                </div>
               <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Address</label>
                        <input id="input-address" class="form-control" name="address" placeholder="Home Address" value="{{($userDetails->firstWhere('user_id',Auth::user()->id) != null) ? $userDetails->firstWhere('user_id',Auth::user()->id)->user_address : "" }}" type="text">
                        @error('address')
          				<div class="alert alert-danger">This Field is required</div>
       				 @enderror
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Country</label>
                        <select id="countryId" class="form-control required"  name="country">
                         <?php $countryId = $userDetails->firstWhere('user_id',Auth::user()->id) != null ? $userDetails->firstWhere('user_id',Auth::user()->id)->country_id : ""; 
                         ?>
                                @if($countryId != null)
                                <option value="{{$countryId}}">{{$country_data->firstWhere('id',$countryId )->country_name}}</option>
                                @else
                                <option value="">Select Country</option>
                                @endif
                                @foreach ($country_data as $country)
                                 <option value="{{$country->id}}">{{$country->country_name}}</option>
                               @endforeach
                                </select>
                      </div>
                      @error('country')
          				<div class="alert alert-danger">This Field is required</div>
       				 @enderror
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-city">City</label>
                        <select id="city" name="city" class="form-control required">
                        <?php $cityId = $userDetails->firstWhere('user_id',Auth::user()->id) != null ? $userDetails->firstWhere('user_id',Auth::user()->id)->city_id : ""; 
                         ?>
                                @if($cityId != null)
                                <option value="{{$cityId}}">{{$city_data->firstWhere('id',$cityId )->city_name}}</option>
                                @else
                                <option value="">Select City</option>
                                @endif
                                </select>
                      </div>
                      @error('city')
          				<div class="alert alert-danger">This Field is required</div>
       				 @enderror
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Postal code</label>
                        <input type="number" id="input-postal-code" name="postalCode" class="form-control" placeholder="Postal code" value="{{$userDetails->firstWhere('user_id',Auth::user()->id) != null ? $userDetails->firstWhere('user_id',Auth::user()->id)->user_postalCode : ""}}">
                      </div>
                      @error('postalCode')
          				<div class="alert alert-danger">This Field is required</div>
       				 @enderror
                    </div>
                  </div>
                </div>
                <hr class="my-4">     
                <h6 class="heading-small text-muted mb-4">Offer Summary</h6>  
                <div class="pl-lg-4">
                    <div class="row">
                      <div class="col-md-6">
                       <div class="inner">
                        <ul class="p-0 d-inline">
                            <li><i class="fa fa-check"></i> No additional costs</li>
                            <li><i class="fa fa-check"></i> Screened &amp; approved dealers</li>
                            <li><i class="fa fa-check"></i> Payment via an escrow account</li>
                        </ul>
                       </div>
                      </div>
                      <div class="col-md-6">
                       <div class="inner bg-light p-3">
                           <div class="d-flex justify-content-between">
                               <div>Item Price</div>
                               <div id="show-price-suggest">{{currency()->convert(floatval($listing->value), 'USD', currency()->getUserCurrency())}}</div>
                               <input type="hidden" id="suggestedPrice" name="suggestedPrice" value="">
                           </div>
                           <div  class="d-flex justify-content-between">
                            <div>Shipping Cost</div>
                            <div>0$</div>
                            <input type="hidden" id="shippingPrice" name="shippingPrice" value="{{0}}">
                           </div>
                           <hr class="my-4"> 
                           <div  class="d-flex justify-content-between">
                            <div><strong>Your price suggestion (incl. all costs)</strong></div>
                            <div><strong>-</strong></div>
                           </div>
                       </div>
                    </div>
                    </div>                    
                </div>
                <hr class="my-4">   
                <div class="pl-lg-4 mt-5">
                  <div class="row">
                <button type="submit" id="submit-suggest" class="btn btn-primary mx-auto ">Submit Price Suggestion</button>
                 </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>      
    </div>
    
    
        <div class="support-box">
        <a href="#" class="icon-text hvr-float-shadow" id="icon-anchor">
            <p><i class="fa fa-envelope-o" aria-hidden="true"></i> Ask Us If You Any Queries</p>
        </a>
    </div>
    @else
    
    <div class="container mt-5">
      <div class="main-heading stm-border-bottom-unit bg-color-border">
        <h3>Your price suggestion</h3>     
    </div> 
      <div class="row">
        <div class="col-xl-4 order-xl-2">
          <div class="card card-profile">          
            <div class="row justify-content-center">
              <div class="col-lg-12">
                <div class="card-profile-image">
                  <a href="#">
                    <img src="{{url('uploads/'.$images->firstWhere('listing_id', $listing->id)->filename)}}" class="rounded-circle">
                  </a>
                  <div class="text-center">
                    <div class="h5 mt-4">
                      <i class="ni business_briefcase-24 mr-2"></i>{{$listing->ad_title}}
                    </div>                   
                  </div>
                  <hr class="my-4">
                </div>
              </div>
            </div>            
            <div class="card-body pt-0 p-1">             
              
              <div class="accordion mt-5" id="accordionExample">
                <div class="">
                    <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <strong class="mb-0">Free Trusted Checkout</strong>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                        <p>The most secure approach to buy the watch you had always wanted. Pay safely by means of an escrow account. The vendor gets your installment simply after you've gotten the watch..</p>
                        </div>
                    </div>
              </div>
                <div class="">
                    <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <strong class="mb-0">Buyer Protection guaranteed</strong>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            <p>In the uncommon case that something doesn't go as arranged, you can depend on our Buyer Protection administration.</p>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <strong class="mb-0">Your data is secure</strong>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                            <p>Tangible carefully clings to all pertinent information assurance laws. 

                                All information that you enter on our site is encoded (SSL) and communicated over a safe Internet association (HTTPS). 
                                
                                Your own data will never be distributed and is possibly utilized during the enrollment cycle and when selling things on Tangible.</p>
                        </div>
                    </div>
                </div>
            </div>
            </div>
          </div>
        </div>
        <div class="col-xl-8 order-xl-1">
          <div class="card">           
            <div class="card-body">
              <form>              
                <div class="pl-lg-4">
                    <div class="row">
                        <div class="col-lg-12">
                          <div class="form-group">
                            <label class="form-control-label" for="input-first-name">Your price suggestion (incl. all costs)</label>
                            <input type="text" id="input-price-suggest" class="form-control" placeholder="List price (plus shipping costs):  {{currency()->convert(floatval($listing->value), 'USD', currency()->getUserCurrency())}}">
                          </div>
                        </div>                        
                      </div>
                </div>                
              </form>
            </div>
          </div>
          <div class="card">           
            <div class="card-body">
              <form>              
                <div class="pl-lg-4">
                    <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="form-control-label" for="input-first-name">First name</label>
                            <input type="text" id="input-first-name" class="form-control" placeholder="First name" value="Lucky">
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="form-control-label" for="input-last-name">Last name</label>
                            <input type="text" id="input-last-name" class="form-control" placeholder="Last name" value="Jesse">
                          </div>
                        </div>
                      </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-number">Phone Number</label>
                        <input type="text" id="input-number" class="form-control" placeholder="Username" value="james">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Email address</label>
                        <input type="email" id="input-email" class="form-control" placeholder="james@example.com">
                      </div>
                    </div>
                  </div>
                  
                </div>
               <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Address</label>
                        <input id="input-address" class="form-control" placeholder="Home Address" value="Bld Mihail Kogalniceanu, nr. 8 Bl 1, Sc 1, Ap 09" type="text">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-city">City</label>
                        <input type="text" id="input-city" class="form-control" placeholder="City" value="New York">
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Country</label>
                        <input type="text" id="input-country" class="form-control" placeholder="Country" value="United States">
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Postal code</label>
                        <input type="number" id="input-postal-code" class="form-control" placeholder="Postal code">
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="my-4">     
                <h6 class="heading-small text-muted mb-4">Offer Summary</h6>  
                <div class="pl-lg-4">
                    <div class="row">
                      <div class="col-md-6">
                       <div class="inner">
                        <ul class="p-0 d-inline">
                            <li><i class="fa fa-check"></i> No additional costs</li>
                            <li><i class="fa fa-check"></i> Screened &amp; approved dealers</li>
                            <li><i class="fa fa-check"></i> Payment via an escrow account</li>
                        </ul>
                       </div>
                      </div>
                      <div class="col-md-6">
                       <div class="inner bg-light p-3">
                           <div class="d-flex justify-content-between">
                               <div>Item Price</div>
                               <div>-</div>
                           </div>
                           <div  class="d-flex justify-content-between">
                            <div>Shipping Cost</div>
                            <div>0$</div>
                           </div>
                           <hr class="my-4"> 
                           <div  class="d-flex justify-content-between">
                            <div><strong>Your price suggestion (incl. all costs)</strong></div>
                            <div><strong>-</strong></div>
                           </div>
                       </div>
                    </div>
                    </div>                    
                </div>
                <hr class="my-4">   
                <div class="pl-lg-4 mt-5">
                  <div class="row">
                <button type="submit" class="btn btn-primary mx-auto ">Submit Price Suggestion</button>
                 </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>      
    </div>
    
    
        <div class="support-box">
        <a href="#" class="icon-text hvr-float-shadow" id="icon-anchor">
            <p><i class="fa fa-envelope-o" aria-hidden="true"></i> Ask Us If You Any Queries</p>
        </a>
    </div>
        <!-- Modal HTML -->
        <div id="myModal1" class="modal fade">
            <div class="modal-dialog modal-login">
                <div class="modal-content">
                @if($errors->any())
                    @foreach($errors->all() as $error)
                      <div class="alert alert-danger">
                    {{$error}}
                      </div>
                    @endforeach
                  @endif
                    <form action="/user/logincheck" method="post" id="loginForm">
                    @csrf
                    <div class="modal-header">                       			
                        <h4 class="modal-title">Member Login</h4>	
                        <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
                    </div>
                    <div class="modal-body">                        
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" placeholder="Username or email" required="required" id="usernameEmail">		
                            </div>
                            <div class="login">
                                <div class="form-group">
                                    <input type="password" class="form-control d-none" name="password" placeholder="Password" required="required" id="rPassword"> 	
                                </div>      
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-btn">Login</button>
                            </div>
                            <div class="modal-footer">
                                <a href="#">Forgot Password?</a>
                            </div>         
                    </div>
                    </form>
                    
                    @if($errors->any())
                    @foreach($errors->all() as $error)
                      <div class="alert alert-danger">
                    {{$error}}
                      </div>
                    @endforeach
                  @endif
                    <form action="user/newUserRegister" method="post" id="registerForm" class="d-none" >
                    @csrf
                        <div class="modal-header">                       			
                            <h4 class="modal-title">Register</h4>	
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body"> 
                        <div class="register">
                            <div class="form-group">
                                <input type="text" class="form-control" name="first_name" placeholder="First Name" required="required">	
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="last_name" placeholder="Last Name" required="required">	
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-control" name="phone" placeholder="Phone No" required="required">	
                            </div> 
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="email" required="required">	
                            </div> 
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Password" required="required">	
                            </div>   
                            <div class="form-group">
                                <input type="password" class="form-control" name="confirmpassword" placeholder="Confirm Password" required="required">	
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-btn">Register</button>
                            </div>
                        </div>
                    </div>                        
                    </form>
                </div>
            </div>
        </div>     
   @endauth
   @endif

    @include('tangiblehtml.innerfooter')


    <script src="/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/intlTelInput.min.js"></script>
    <script src="/assets/js/local.js"></script>
  <script type="text/javascript">

  $(window).on('load',function(){
      $('#myModal1').modal({backdrop: 'static', keyboard: false});
  });


  $(document).ready(function () {

	  $("#countryId").change(function(){
	        var countryId = $(this).val();
	        $('#loading-image').show();
	        $.ajax({
				url: "/getCity",
				type: "POST",
				data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
				dataType: 'json',
				success : function(data){
					var html = `<option value="">Select City</option>`;
					for (var i = 0; i < data.length; i++) {
						  var id = data[i].id;
						  var name = data[i].city_name;
						  var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
						  html += option;
						}
					$('#loading-image').hide();
						$('#city').html(html);	
					}
				});
	      });

      function onchange() {
          //Since you have JQuery, why aren't you using it?
          console.log('Inside function');
          var box1 = $('#input-price-suggest').val();
          $('#show-price-suggest').text(box1);
          $('#suggestedPrice').val(box1);
          
      }
      $('#input-price-suggest').on('change', onchange);

      $("#submitPriceForm").validate({
    	    
          rules: {
          	firstName: "required",
          	lastName: "required",
          	suggestedPrice: "required",
          	phoneNumber: "required"
          },
      
          messages: {
          	currencyCode: "Currency Code can\'t be left blank",
          	currency: "Currency can\'t be left blank",
          	phoneNumber: "Phone Number  Can\'t be left blank "  	
          },
      
          submitHandler: function(form) {
            form.submit();
          }
        });
      
  });
  </script>
</body>

</html>