<script src="https://cdn.rawgit.com/PascaleBeier/bootstrap-validate/v2.2.0/dist/bootstrap-validate.js" ></script>
    <section>               
        <!-- Modal HTML -->
        <div id="myModal" class="modal fade ">
            <div class="modal-dialog modal-login">
                <div class="modal-content">
                <div id="error"></div>
                    <div id="loginForm">
                        
                    <div class="modal-header">                       			
                        <h4 class="modal-title">Member Login</h4>	
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">                        
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" placeholder="Username or email" required="required" id="usernameEmail"><span id="emailcheck"></span>		
                            </div>
                            <div class="login">
                                <div class="form-group">
                                    <input type="password" class="form-control d-none" name="password" placeholder="Password" required="required" id="rPassword"> 	
                                </div>      
                            </div>
                            <div class="form-group">
                                <button id="userLogin" class="btn btn-primary btn-lg btn-block login-btn">Login</button>
                            </div>
                            <div class="modal-footer">
                                <a href="#forgotPassword" class="forgetPasword">Forgot Password?</a>
                            </div>         
                    </div>
                    </div>
                    
                    @if($errors->any())
                    @foreach($errors->all() as $error)
                      <div class="alert alert-danger">
                    {{$error}}
                      </div>
                    @endforeach
                  @endif
                    <form class="needs-validation d-none" novalidate action="user/newUserRegister" method="post" id="registerForm">
                    @csrf
                        <div class="modal-header">                       			
                            <h4 class="modal-title">Register</h4>	
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body"> 
                        <div class="register">
                            <div class="form-group">
                                <input type="text" class="form-control" name="first_name" placeholder="First Name" required="required">	
                                <div class="invalid-feedback">This Field is required</div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="last_name" placeholder="Last Name" required="required">	
                                <div class="invalid-feedback">This Field is required</div>
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-control" name="phone" id="mobileNumber" placeholder="Phone No" minlength="10" maxlength="10" required="required">
                                <span id="message"></span>	
                                <div class="invalid-feedback">This Field is required</div>
                            </div> 
                            <div class="form-group">
                                <input type="email" class="form-control" id="email"  placeholder="email" required="required" disabled>
                                <input type="hidden" class="form-control" id="useremail" name="email" placeholder="email">	
                            </div> 
                            <div class="form-group">
                                <input type="password" class="form-control" id="UserPassword" name="password" placeholder="Password" required="required" minlength="8">	
                                <div class="invalid-feedback">Password should be 8 digits minimum</div>
                            </div>   
                            <div class="form-group">
                                <input type="password" class="form-control" id="confirmPassword" name="confirmpassword" placeholder="Confirm Password" required="required" minlength="8">
                                <div class="registrationFormAlert" style="color:red;" id="CheckPasswordMatch">
                                <div class="invalid-feedback">Password should be 8 digits minimum</div>	
                            </div>
                            <div class="form-group mt-4">
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-btn">Register</button>
                            </div>
                        </div>
                    </div>                        
                    </form>
                </div>
            </div>
        </div>  
        
        <div id="forgotPassword" class="modal fade show">
        <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-4">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
            
                <i class="fa fa-times closeWindow"></i>
              <div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
              <div class="offset-lg-2 col-lg-8">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-2">Forgot Your Password?</h1>
                    <p class="mb-4">We get it, stuff happens. Just enter your email address below and we'll send you a link to reset your password!</p>
                  </div>
                  <form method="POST" action="{{ route('password.email') }}" class="user">
                        @csrf
                    <div class="form-group">
                       <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                       <input id="email" type="email" class="form-control  form-control-user @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Enter Email Address..." autofocus>

                                @error('email')
                                 <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    
                    </div>
                   
                     <button type="submit" class="btn btn-primary btn-user btn-block">
                                    {{ __('Send Password Reset Link') }}
                      </button>
                  </form>
                  
                 
                 
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
</div>  
    </section>
    <!-- Modal -->  
  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="sellNowLabel">Sell Now</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="inner sell-cat">
                        <div class="uppersection mb-3 text-center">
                            <i class=" fa-5x fa fa-lock"></i>
                            <p>Choose between creating a watch or automobile listing</p>
                        </div>
                        <div class="linkto">
                            <a class="sell-btn" href="/privatesellercategoryselect">Create Private Listing</a>
                        </div>
                    </div>
                </div>           
                <div class="col-lg-6">
                    <div class="inner sell-cat">
                        <div class="uppersection mb-3 text-center">
                            <i class="fa fa-5x fa-user"></i>
                            <p>Sign up to showcase your inventory list globally.</p>
                        </div>
                        <div class="linkto">
                            <a class="sell-btn" href="/becomedealer">Become Dealer</a>
                        </div>
                    </div>
                </div>
            </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>             
            </div>
          </div>
    </div>
  </div>
    
<section>               
        <!-- Modal HTML -->
        <div id="currency" class="modal fade">
            <div class="modal-dialog modal-login">
                <div class="modal-content">                
                    <form action="#" method="post" id="currencyForm" >
                        <div class="modal-header">                       			
                            <h4 class="modal-title">Settings</h4>	
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body"> 
                        <div class="register">
                            <div class="form-group">
                                <select name="je_currency" id="" class="form-control">
                                    <option value="AUD">Australian dollar - AUD $</option>
                                    <option value="BRL">Brazilian real - BRL R$</option>
                                    <option value="CAD">Canadian dollar - CAD $</option>
                                    <option value="CZK">Czech korun - CZK Kč</option>
                                    <option value="DKK">Danish krone - DKK kr.</option>
                                    <option value="AED">Emirati dirham - AED AED</option>
                                    <option value="EUR">Euro - EUR €</option>
                                    <option value="HKD">Hong Kong dollar - HKD HK$</option>
                                    <option value="HUF">Hungarian forint - HUF Ft</option>
                                    <option selected="selected" value="INR">Indian rupee - INR ₹</option>
                                    <option value="JPY">Japanese yen - JPY ¥</option>
                                    <option value="MYR">Malaysian ringgit - MYR RM</option>
                                    <option value="MXN">Mexican peso - MXN $</option>
                                    <option value="NOK">Norwegian krone - NOK kr</option>
                                    <option value="PLN">Polish zloty - PLN zł</option>
                                    <option value="GBP">Pound sterling - GBP £</option>
                                    <option value="RUB">Russian ruble - RUB ₽</option>
                                    <option value="SAR">Saudi Arabian riyal - SAR ر.س</option>
                                    <option value="SGD">Singapore dollar - SGD $</option>
                                    <option value="ZAR">South African rand - ZAR R</option>
                                    <option value="KRW">South Korean won - KRW ₩</option>
                                    <option value="SEK">Swedish krona - SEK kr</option>
                                    <option value="CHF">Swiss franc - CHF CHF</option>
                                    <option value="TRY">Turkish lira - TRY ₺</option>
                                    <option value="USD">United States dollar - USD $</option></select>	
                            </div> 
                            <div class="form-group">
                                <select name="je_measurement_units" id="" class="form-control">
                                    <option selected="selected" value="sqft">Square Feet — ft²</option>
                                    <option value="sqm">Square Meter — m²</option></select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-btn">Save Choice</button>
                            </div>
                    </div>                        
                    </form>
                </div>
            </div>
        </div>     
    </section>
<footer>
        <div class="custom-top-footer pt-5 pb-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 mr-auto col-md-4 mb-4">
                        <div class="custom-footer-logo">
                            <a href="#"><img src="/assets/img/logo-header.png" class="img-fluid"></a>
                            <p>Tangible Listings is a global market place which offers you access to luxury real estate, automobiles and watches. Tangible Listings provides you with a platform to buy and sell luxury items.</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 mb-4">
                        <div class="custom-footer-links footer-social">
                            <h3>Contact Us</h3>
                            <ul>
                                <li>
                                    <a  href="https://www.facebook.com/tangiblelistings"><i class="fab fa-facebook-f"></i>Tangible Listings</a>
                                </li>
                                <li>
                                    <a  href="https://www.instagram.com/tangiblelistings/"><i class="fab fa-instagram"></i>Tangible Listings</a>
                                </li>
                                <li>
                                    <a href="mailto:email@email.com"><i class="far fa-envelope"></i> {{$contactUs->email}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="custom-footer-links footer-nav">
                            <h3>Important Links</h3>
                            <ul>
                               
                                <li>
                                    <a href="#">About</a>
                                </li>
                                <li>
                                    <a href="#">FAQ</a>
                                </li>
                                <li>
                                    <a href="#">Terms and Conditions</a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="modal" data-target=".bd-example-modal-lg">Sell with Us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="custom-bottom-footer pt-3 pb-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-12">
                        <div class="copyright-wrapper">
                            <p>© Tangible - All rights reserved</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-12">
                        <div class="footer-social-links">
                            <ul>
                                <li><a class="hvr-buzz-out" href="https://www.facebook.com/tangiblelistings"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a class="hvr-buzz-out" href="https://www.instagram.com/tangiblelistings/"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script>
    

// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();


</script>