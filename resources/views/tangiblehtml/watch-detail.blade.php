<!DOCTYPE html>
<html lang="en">

@include('tangiblehtml.headheader')

<body class="watch-lading">
    @include('tangiblehtml.innerheader')
    
    <div class="custom-home-listing-wrapper pb-3">
        <nav aria-label="breadcrumb ">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/tangiblewatches">Home</a></li>
                    <li class="breadcrumb-item"><a href="/watchlisting">Watch</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{$watchListing->ad_title}}
                    </li>
                </ol>
                @if(session()->has('success_msg'))
                <div class="alert alert-success" role="alert">
                {{ session()->get('success_msg') }}
                </div>
                @endif
            </div>
        </nav>
    </div>
    <div class="watch-single-product pb-5 pt-3">
        <div class="container">
            <div class="watch-product-image">
                <div class="row">
                    <div class="col-md-9">
                        <div class="stm-listing-single-price-title heading-font clearfix">
                            <div class="price">{{currency()->convert(floatval($watchListing->watch_price), 'USD', currency()->getUserCurrency())}}</div>

                            <div class="stm-single-title-wrap">
                                <h1 class="title">
                                    {{$watchListing->ad_title}}
                                    <div class="heading-down d-flex flex-wrap w-100">
                                        <p><i class="fa fa-flag-o" aria-hidden="true"></i> Model: <span>{{$watchListing->model_name}}</span>
                                        </p>
                                        <!-- <i class="fa fa-check-square-o" aria-hidden="true"></i> Varified Seller</p> <p>-->
                                        <p><i class="fa fa-bell-o" aria-hidden="true"></i> Condition: <span>{{$watchListing->watch_condition}}</span>
                                        </p>
                                    </div>
                                </h1>

                            </div>

                        </div>
                        <div class="single-product-images">
                            <div class="watch-slider mb-3" id="lightgallery-watch">

								@foreach($images as $image)
                                <div class="item" data-src="images/watch/watch2.jpg">
                                    <img src="{{url('uploads/'.$image->filename)}}">
                                </div>
								@endforeach
								
                                <!--<p class="save-listing" title="Save Listing">
                                    <a class="" href="#">
                                        <i class="fa fa-bookmark-o" aria-hidden="true"></i> Save
                                    </a>
                                </p>-->

                            </div>
                            <div class="watch-slider-nav-thumbnails">
                                @foreach($images as $image)
                                <div><img src="{{url('uploads/'.$image->filename)}}" alt="One"> </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="watch-desc">

                            <div class="stm-border-top-unit bg-color-border">
                                <h5><strong>Description</strong></h5>
                            </div>

                            <div class="mt-5">
                                <p>
                                    {{strip_tags($description->comment)}}</p>
                            </div>

                            <div class="stm-border-top-unit bg-color-border">
                                <h5><strong>Basic Details</strong></h5>
                            </div>
                            <div class="stm-single-listing-car-features watch-details">
                                <div class="lists-inline">
                                    <ul class="list-style-2">
                                        <li>Brand: 	{{$watchListing->brand_name}}</li>
                                        <li>Model: {{$watchListing->model_name}}</li>
                                    </ul>
                                    <ul class="list-style-2">
                                        <li>Condition: {{$watchListing->watch_condition}}</li>
                                        <li>Movement: {{$watchListing->movement}}</li>
                                    </ul>
                                    <ul class="list-style-2">
                                        <li>Case material: {{$watchListing->case_material}}</li>
                                        <li>Bracelet material: {{$watchListing->bracelet_material}}</li>
                                    </ul>
                                    <ul class="list-style-2">
                                        <li>Year of production: {{$watchListing->year_of_manufacture}}</li>
                                        <li>Gender: {{$watchListing->gender}}</li>
                                    </ul>
                                    <ul class="list-style-2">
                                        <li>Location: {{$watchListing->country_name}}, {{$watchListing->state_name}} {{$watchListing->city_name}}</li>
                                        <li>Dial Color: {{$watchListing->dial_color}}</li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Start of the report section -->
                            <div id="report">
                                <br>
                                <div class="report">
                                    
                                    @auth
                            @if($savedListing && $savedListing->listingId == $watchListing->id && $savedListing->userId == Auth::user()->id)
                            <button type="button" class="btn btn-success" >
                                <i class="fa fa-heart"  aria-hidden="true"></i> <span>Saved</span>
                            </button>
                            @else
                            <button type="button" class="btn btn-success" id="save" value="{{$watchListing->id}}">
                                <i class="fa fa-heart"  aria-hidden="true"></i> <span>Save</span>
                            </button>
                            @endif
                            @else
                            <button type="button" class="btn btn-success" href="#myModal" data-toggle="modal">
                                <i class="fa fa-heart"  aria-hidden="true"></i> <span>Save</span>
                            </button>
                            @endauth
                                </div>
                            </div>


                            <div class="modal fade" id="report-modal" tabindex="-1" role="dialog"
                                aria-labelledby="report-modal" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalCenterTitle">Report this listing
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form>
                                            <div class="modal-body">
                                                <p>Please select the appropriate option for your concern regarding this
                                                    listing. We will review your report and determine whether it
                                                    violates our Listing Criteria or isn't suitable for us.</p>
                                                <div class="form-row">
                                                    <div class="col-md-12 mb-3">
                                                        <select class="custom-select" required>
                                                            <option value="" selected disabled>Report this listing
                                                            </option>
                                                            <option value="Inappropriate content">Inappropriate content
                                                            </option>
                                                            <option value="Misleading content">Misleading content
                                                            </option>
                                                            <option value="Terms of use violation">Terms of use
                                                                violation</option>
                                                            <option value="Copyright infringement">Copyright
                                                                infringement</option>
                                                            <option value="Exclusive listing rights">Exclusive listing
                                                                rights</option>
                                                            <option value="Other">Other</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-12 mb-3">
                                                        <input type="text" class="form-control" id="name"
                                                            placeholder="name" value="" required>
                                                    </div>
                                                    <div class="col-md-12 mb-3">
                                                        <input type="email" class="form-control" id="email"
                                                            placeholder="email" value="" required>
                                                    </div>
                                                    <div class="col-md-12 mb-3">
                                                        <textarea class="form-control" rows="5" id="message"
                                                            placeholder="comments" value="" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                                <button class="btn btn-danger" type="submit">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="stm-single-listing-car-sidebar">
                            <div class="dealer-contacts">
                                <div class="dealer-contact-unit purchase">
                                <a href="checkout/{{$watchListing->id}}" class="buy-btn">Purchase</a>
                                    @if($watchListing->priceType == 'Negotiable')
                                <a href="suggestprice/{{$watchListing->id}}" class="suggest-btn">Make an Offer</a>
                                @endif
                                </div>
                                <!-- <div class="dealer-contact-unit address">
                                    <i class="fa fa-map-marker"></i>
                                    <div class="address">Démouville, France</div>
                                </div>
                                <div class="dealer-contact-unit phone">
                                    <i class="fa fa-phone"></i>
                                    <div class="phone heading-font">(88*******</div>
                                    <span class="stm-show-number" data-id="628">Show number</span>
                                </div> -->
                            </div>
                            <!-- <div class="map">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30710983.209769644!2d64.45235976587381!3d20.01273993518969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30635ff06b92b791%3A0xd78c4fa1854213a6!2sIndia!5e0!3m2!1sen!2sin!4v1597646766464!5m2!1sen!2sin"
                                    width="100%" height="250" frameborder="0" style="border:0;" allowfullscreen=""
                                    aria-hidden="false" tabindex="0"></iframe>
                            </div> -->
                            <div class="dealerDetails border">
                            <div class="brand">
                                @if($brandImg != null)
                                <img src="{{url(('uploads/'.$brandImg->filename))}}" alt="">
                                @endif 
                           </div>
                            <div class="agentsContainer">
                                <div class="agents">
                                    <div class="agentPhoto">
                                    @if($agentImage != null)
                                        <img src="{{url(('uploads/'.$agentImage->filename))}}" alt="">
                                    @endif    
                                    </div>
                                    <div class="agentDetails">
                                        <div class="agentName font-weight-bold">
                                            {{$watchListing->agentname}} 
                                        </div>
                                        <div class="agentNumber">
                                        {{$watchListing->agentMobile}}
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="propertyDetails p-3">
                                <div class="propertyAddress">
                                @if($city != null)
                                {{$city->city_name}}
                                @endif
                                @if($state != null)
                                {{$state->state_name}}
                                @endif
                                @if($country != null)
                                {{$country->country_name}}
                                @endif
                                </div>                                
                            </div>                        
                        </div>
                            <div>
                                <div class="stm-border-bottom-unit bg-color-border">
                                    <h5>Contact seller</h5>
                                </div>
                                <div class="contact-seller">
                                    <!-- <div class="d-flex">
                                        <div class="seller-profile">
                                            <img src="https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png"
                                                alt="seller" width="40" height="40">
                                        </div>
                                        <div class="seller-info">
                                            <h6>Varified Seller Since 2016</h6>
                                            <ul>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <span><a href="#"> Reviews: <span id="numbers">60</span></a></span>
                                            </ul>
                                        </div>
                                    </div>
                                    <p class="seller-city">This seller is from Démouville, France</p> -->
                                    @if(Route::has('login'))
                                @auth
                                <?php 
                                $checkMessage = null;
                                if($message->firstWhere('userID', '=', Auth::user()->id) != null)
                                $checkMessage = $message->firstWhere('userID', '=', Auth::user()->id)->firstWhere('productId', '=', $productId);;
                                ?>
                                    @if( $checkMessage != null)
                                <a class="contact-btn" href="/mymessages" role="button"
                                    >Contact Seller  <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                    @else
									 <a class="contact-btn" data-toggle="collapse" href="#contact-seller-form" role="button"
                                    aria-expanded="false" aria-controls="contact-seller-form">Contact Seller  <i class="fa fa-angle-down" aria-hidden="true"></i></a>                                   
                                    @endif
                                @else
                                <a href="#myModal" class="contact-btn" data-toggle="modal">Contact Seller</a>    
                                @endauth
                                @endif
                                </div>
                                <div class="collapse stm-single-car-contact bg-color" id="contact-seller-form">
                                    <div role="form">
                                    <form method="POST" id="enquiyFormWatch" action="{{ url('/watch/enquery') }}">
                                        @csrf
                                            <div class="form-group">
                                                <input type="text" placeholder="Name" id="nameForm" name="name" value="{{ old('name') }}" class="from-control" >
                                                    @error('name')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                            </div>
                                            <input type="hidden" name="productid" value="{{$productId}}" class="from-control" value="">
                                            <input type="hidden" name="agentId" value="{{$watchListing->agent_id}}" class="from-control" value="">
                                            @if(Route::has('login'))
                                            @auth
                                            <input type="hidden" name="userId" value="{{Auth::user()->id}}" class="from-control" value="">
                                            @endauth
                                            @endif
                                            <div class="form-group">
                                                <input type="number" placeholder="Phone" id="telephoneForm" name="mobile"  value="{{ old('phone') }}" class="from-control" ><span id="phone"></span>
                                                    @error('mobile')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                            </div>
                                            <div class="form-group">
                                                <!-- <input type="email" placeholder="Email" class="from-control" required> -->
                                            </div>
                                            <div class="form-group">
                                                <select class="from-control" name="country" id="countryForm"
                                                    data-role="country-selector" value="default"></select>
                                            </div>

                                            <div class="form-group">
                                                <textarea placeholder="Message"  value="{{ old('message') }}" name="message"  reqclass="from-control"
                                                    >Hi, I am interested in your listing</textarea>
                                                    @error('message')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                            </div>
                                            <div class="form-group">
                                                <input type="button" id="buttonsubmit" value="Send enquiry"
                                                    class="read-more bg-color-border">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="boredr-top bg-color-border">
                                <div class="seller-details">
                                    <h6 class="seller-status">For Sale by</h6>
                                    <h3 class="seller-name">COHERENTHK</h3>
                                    <h6 class="seller-location">Démouville, France</h6>
                                    <div class="member">
                                        <h5><i title="Our Member" class="fa fa-id-badge" aria-hidden="true"></i> <span>:
                                                Since 2015</span> </h5>
                                        <p><i class="fa fa-check-square-o" aria-hidden="true"></i> Trusted seller</p>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End single-product-images -->

        </div>
    </div>
    <!-- End single-product -->
    @include('tangiblehtml.innerfooter')
    
    <script src="/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/lightgallery@1.7.3/dist/js/lightgallery.js"></script>
    <!-- <script src="js/lightgallery.min.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
        </script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/local.js"></script>
    <script src="/assets/js/jquery.countrySelector.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            
            $('#buttonsubmit').on('click',function(){
			
		var isError = false;

		var nameForm = $('#nameForm').val();
		var mobile = $("#telephoneForm").val();
		var countryForm = $('#countryForm').val();
        
			
       	if(mobile.length !=10){
               phone.style.color = 'red';
               phone.innerHTML = "Required 10 digits!"
            	isError = true;	
                   
           }else {
           	phone.style.color = 'green';
           	phone.innerHTML = "";
           } 
       	          
        
       if(nameForm == ""){
			$('#nameForm').css('border','1px solid red');
			isError = true;
			}else{
				
				}


       if(countryForm == ""){
			$('#countryForm').css('border','1px solid red');
			isError = true;
			}else{
				
				}

       if(isError == false){
           
    	   document.getElementById("enquiyFormWatch").submit();
           }

    	});

$("#telephoneForm").keyup(checkphonenumber);
    
    function checkphonenumber() {

        var mobile = $("#telephoneForm").val();
		
        if(mobile.length !=10){
            
            phone.style.color = 'red';
            phone.innerHTML = "Required 10 digits!"
        }else {
        	phone.style.color = 'green';
        	phone.innerHTML = "";
        }
    }
            
            $("#lightgallery-watch").lightGallery({
                selector: '.item'
            });
            
            $("#save").click(function(){
            	  var listingId = $(this).val();
            	  $('#loading-image').show();
            	  $.ajax({
            	    url: "/user/savelisting",
            	    type: "POST",
            	    data: {listingId: listingId, category: 'Watch', _token: '{{csrf_token()}}' },
            	    success : function(data){
            	    	var html = `<i class="fa fa-heart"  aria-hidden="true"></i> <span>Saved</span>`;
            	    	$('#save').html(html);
            	      }
            	    });
            	  });
        });
    </script>

</body>

</html>