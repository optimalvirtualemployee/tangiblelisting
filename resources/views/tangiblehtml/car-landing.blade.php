<!DOCTYPE html>
<html lang="en">

@include('tangiblehtml.headheader')
<style>
    
    .article-para{
text-overflow: ellipsis;
overflow: hidden;
white-space: nowrap;
}
</style>
<body class="watch-lading">
    @include('tangiblehtml.innerheader')

    <!--banner area-->
    <div class="custom-landing-banner-wrapper">
        <div class="custom-landing-banner-slider">
            <div class="landing-banner-item">
                <div class="container">
                    <div class="caption">
                        <h3> Find Your Dream Car</h3>
                        <!-- <a href="#" class="read-more fill-read-more"> Explore More</a> -->
                    </div>
                    <div class="col p-0">
                        <div class="banner_filter slide-1">
                            <div class="banner___from">
                                <div class="row search-headtext align-items-center">
                                    <div class="col-sm-8 col-xs-12">
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <a href="#" class="reset-search" id="clear"><i class="fa fa-undo" aria-hidden="true" ></i>
                                            Reset Search</a>
                                    </div>
                                </div>
                                <form id="searchForm" method="GET" action="{{url('/carlisting/search')}}" class="form-inline">
                                    <div class="form-group arrow">
                                        <select name="make" id="brand" class="form-control"
                                            placeholder="All Makes">
                                            <option value="" selected disabled>All Makes</option>
                                            @foreach($makes as $make)
                                          	<option value="{{$make->id}}">{{$make->automobile_brand_name}}</option>
											@endforeach
                                        </select>
                                    </div>
                                    <div class="form-group arrow">
                                        <select name="model" id="model" class="form-control"
                                            placeholder="All Models">
                                            <option value="" selected disabled>All Models</option>
                                            @foreach($models as $model)
                                          	<option value="{{$model->id}}">{{$model->automobile_model_name}}</option>
											@endforeach
                                        </select>
                                    </div>
                                    <div class="form-group arrow">
                                        <select name="fromYear" id="year_from" class="form-control"
                                            placeholder="Year From">
                                            <option value="" selected disabled>From year</option>
                                            @foreach($from_year as $year)
                                          	<option value="{{$year->build_year}}">{{$year->build_year}}</option>
											@endforeach
                                        </select>
                                    </div>
                                    <div class="form-group arrow mr-0">
                                        <select name="toYear" id="year_to" class="form-control"
                                            placeholder="Year To">
                                            <option value="" selected disabled>To year</option>
                                            @foreach($to_year as $year)
                                          	<option value="{{$year->build_year}}">{{$year->build_year}}</option>
											@endforeach                                        </select>
                                    </div>

                                    <div class="form-group pos-bottom pr-0">
                                        <input type="submit" id="findvalue" class="read-more submit-btn" value="Search">
                                    </div>

                                </form>



                            </div>
                        </div>
                    </div>

                </div>
                <img src="/assets/images/car/budget-slider-2.jpg" alt="" class="img-fluid">
            </div>
            

        </div>
    </div>
    <!--banner area end-->

    <div class="custom-home-listing-wrapper pt-1">

        <div class="car-latest-collections pt-5 pb-5">
            <div class="container">
                <div class="main-heading text-center">
                    <h3> Latest Listings </h3>
                </div>
                <!--<div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="suv-tab" data-toggle="tab" href="#suv" role="tab"
                                aria-controls="suv" aria-selected="true">
                                SUV</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="sedan-tab" data-toggle="tab" href="#sedan" role="tab"
                                aria-controls="sedan" aria-selected="false">
                                Sedan</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="electric-tab" data-toggle="tab" href="#electric" role="tab"
                                aria-controls="electric" aria-selected="false">
                                Electric</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="hybrid-tab" data-toggle="tab" href="#hybrid" role="tab"
                                aria-controls="hybrid" aria-selected="false">
                                Hybrid</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="luxury-tab" data-toggle="tab" href="#luxury" role="tab" aria-controls="luxury" aria-selected="false">Luxury</a>
                        </li>
                    </ul>
                </div>-->
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade active show" id="suv" role="tabpanel" aria-labelledby="suv-tab">
                        <div class="row l_collection_inner car-listing-row perfect-car-slider">

							@if($latest_listing_top != null)
							@foreach($latest_listing_top as $automobile)
                            <div class="col-lg-3 col-md-6">
                                <div class="stm-directory-grid-loop">
                                    <div class="image">
                                        <div class="car-listing-slider">
                                        	<a href="{{url('/carlisting/'.Crypt::encrypt($automobile->id))}}">
                                            <img src="{{'uploads/'.$images->firstWhere('listing_id','=', $automobile->id)->filename}}" alt="">
                                            </a>
                                        </div>
                                        <div class="stm-badge-directory heading-font Special">Special</div>
                                    </div>
                                    <div class="listing-car-item-meta">
                                        <div class="car-meta-top heading-font clearfix">
                                            <div class="price">
                                                <div class="normal-price">{{currency()->convert(floatval($automobile->value), 'USD', currency()->getUserCurrency())}}</div>
                                            </div>
                                            <div class="car-title">
                                                <div class="labels">{{$automobile->neworused .' '. $automobile->build_year}}</div> {{$automobile->ad_title}}
                                            </div>
                                        </div>
                                        <div class="car-meta-bottom">
                                            <ul>
                                                <li>
                                                    <i class="fa fa-road"></i>
    
                                                    <span>{{$automobile->odometer}} </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-gear"></i>
                                                    <span>{{$automobile->transmission}}</span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-hand-paper-o"></i>
                                                    <span>{{$automobile->rhdorlhd}}</span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-calendar"></i>
                                                    <span>{{date('Y', strtotime($automobile->automobilecreationtime))}}</span>
                                                </li>
                                            </ul>
                                        </div>
    
                                        <div class="dealer-logo">
                                            <div>
                                                <ul>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                                <p><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>
                                                    {{ $automobile->agency_id === 0 ? "Private Seller" : "Dealer" }}</p>
                                            </div>
                                            <div class="country-flag">
                                                <img src="https://image.flaticon.com/icons/svg/197/197430.svg" alt="">
                                                <p class="c-code">{{$automobile->countrycode}}</p>
                                            </div>
                                        </div>
    
                                    </div>
                                    </a>
                                </div>
                            </div>
                            @endforeach
							@endif

                        </div>
                    </div>
                   <!-- <div class="tab-pane fade " id="sedan" role="tabpanel" aria-labelledby="suv-tab">
                        <div class="row l_collection_inner car-listing-row">
							
							@if($sedan_listing != null)
							@foreach($sedan_listing as $automobile)
                            <div class="col-lg-4 col-md-6">
                                <div class="stm-directory-grid-loop">
                                    <div class="image">
                                        <div class="car-listing-slider">
                                            <a href="{{url('/carlisting/'.Crypt::encrypt($automobile->id))}}">
                                            <img src="{{'uploads/'.$images->firstWhere('listing_id','=', $automobile->id)->filename}}" alt="">
                                            </a>
                                        </div>
                                        <div class="stm-badge-directory heading-font Special ">Special</div>
                                    </div>
                                    <div class="listing-car-item-meta">
                                        <div class="car-meta-top heading-font clearfix">
                                            <div class="price">
                                                <div class="normal-price">{{currency()->convert(floatval($automobile->value), 'USD', currency()->getUserCurrency())}}</div>
                                            </div>
                                            <div class="car-title">
                                                <div class="labels">{{$automobile->neworused .' '. $automobile->build_year}}</div> {{$automobile->ad_title}}
                                            </div>
                                        </div>
                                        <div class="car-meta-bottom">
                                            <ul>
                                                <li>
                                                    <i class="fa fa-road"></i>
    
                                                    <span>{{$automobile->odometer}} </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-gear"></i>
                                                    <span>{{$automobile->transmission}}</span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-hand-paper-o"></i>
                                                    <span>{{$automobile->rhdorlhd}}</span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-calendar"></i>
                                                    <span>{{date('Y', strtotime($automobile->automobilecreationtime))}}</span>
                                                </li>
                                            </ul>
                                        </div>
    
                                        <div class="dealer-logo">
                                            <div>
                                                <ul>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                                <p><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>
                                                    {{ $automobile->agency_id === 0 ? "Private Seller" : "Dealer" }}</p>
                                            </div>
                                            <div class="country-flag">
                                                <img src="https://image.flaticon.com/icons/svg/197/197430.svg" alt="">
                                                <p class="c-code">{{$automobile->countrycode}}</p>
                                            </div>
                                        </div>
    
                                    </div>
                                    </a>
                                </div>
                            </div>
                            @endforeach
							@endif	

                        </div>
                    </div>-->
                    <!--<div class="tab-pane fade " id="electric" role="tabpanel" aria-labelledby="suv-tab">
                        <div class="row l_collection_inner car-listing-row">

							@if($electric_listing != null)
							@foreach($electric_listing as $automobile)
                            <div class="col-lg-4 col-md-6">
                                <div class="stm-directory-grid-loop">
                                    <div class="image">
                                        <div class="car-listing-slider">
                                            <a href="{{url('/carlisting/'.Crypt::encrypt($automobile->id))}}">
                                            <img src="{{'uploads/'.$images->firstWhere('listing_id','=', $automobile->id)->filename}}" alt="">
                                            </a>
                                        </div>
                                        <div class="stm-badge-directory heading-font Special ">Special</div>
                                    </div>
                                    <div class="listing-car-item-meta">
                                        <div class="car-meta-top heading-font clearfix">
                                            <div class="price">
                                                <div class="normal-price">{{currency()->convert(floatval($automobile->value), 'USD', currency()->getUserCurrency())}}</div>
                                            </div>
                                            <div class="car-title">
                                                <div class="labels">{{$automobile->neworused .' '. $automobile->build_year}}</div> {{$automobile->ad_title}}
                                            </div>
                                        </div>
                                        <div class="car-meta-bottom">
                                            <ul>
                                                <li>
                                                    <i class="fa fa-road"></i>
    
                                                    <span>{{$automobile->odometer}} </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-gear"></i>
                                                    <span>{{$automobile->transmission}}</span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-hand-paper-o"></i>
                                                    <span>{{$automobile->rhdorlhd}}</span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-calendar"></i>
                                                    <span>{{date('Y', strtotime($automobile->automobilecreationtime))}}</span>
                                                </li>
                                            </ul>
                                        </div>
    
                                        <div class="dealer-logo">
                                            <div>
                                                <ul>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                                <p><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>
                                                    {{ $automobile->agency_id === 0 ? "Private Seller" : "Dealer" }}</p>
                                            </div>
                                            <div class="country-flag">
                                                <img src="https://image.flaticon.com/icons/svg/197/197430.svg" alt="">
                                                <p class="c-code">{{$automobile->countrycode}}</p>
                                            </div>
                                        </div>
    
                                    </div>
                                    </a>
                                </div>
                            </div>
                            @endforeach
							@endif


                        </div>
                    </div>-->
                    <!--<div class="tab-pane fade " id="hybrid" role="tabpanel" aria-labelledby="suv-tab">
                        <div class="row l_collection_inner car-listing-row">

 						    @if($hybrid_listing != null)
							@foreach($hybrid_listing as $automobile)
                            <div class="col-lg-4 col-md-6">
                                <div class="stm-directory-grid-loop">
                                    <div class="image">
                                        <div class="car-listing-slider">
                                            <a href="{{url('/carlisting/'.Crypt::encrypt($automobile->id))}}">
                                            <img src="{{'uploads/'.$images->firstWhere('listing_id','=', $automobile->id)->filename}}" alt="">
                                            </a>
                                        </div>
                                        <div class="stm-badge-directory heading-font Special ">Special</div>
                                    </div>
                                    <div class="listing-car-item-meta">
                                        <div class="car-meta-top heading-font clearfix">
                                            <div class="price">
                                                <div class="normal-price">{{currency()->convert(floatval($automobile->value), 'USD', currency()->getUserCurrency())}}</div>
                                            </div>
                                            <div class="car-title">
                                                <div class="labels">{{$automobile->neworused .' '. $automobile->build_year}}</div> {{$automobile->ad_title}}
                                            </div>
                                        </div>
                                        <div class="car-meta-bottom">
                                            <ul>
                                                <li>
                                                    <i class="fa fa-road"></i>
    
                                                    <span>{{$automobile->odometer}} </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-gear"></i>
                                                    <span>{{$automobile->transmission}}</span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-hand-paper-o"></i>
                                                    <span>{{$automobile->rhdorlhd}}</span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-calendar"></i>
                                                    <span>{{date('Y', strtotime($automobile->automobilecreationtime))}}</span>
                                                </li>
                                            </ul>
                                        </div>
    
                                        <div class="dealer-logo">
                                            <div>
                                                <ul>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                                <p><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>
                                                    {{ $automobile->agency_id === 0 ? "Private Seller" : "Dealer" }}</p>
                                            </div>
                                            <div class="country-flag">
                                                <img src="https://image.flaticon.com/icons/svg/197/197430.svg" alt="">
                                                <p class="c-code">{{$automobile->countrycode}}</p>
                                            </div>
                                        </div>
    
                                    </div>
                                    </a>
                                </div>
                            </div>
                            @endforeach
							@endif

                        </div>
                    </div>-->
                    <!--<div class="tab-pane fade " id="luxury" role="tabpanel" aria-labelledby="suv-tab">
                        <div class="row l_collection_inner car-listing-row">

						 	@if($luxury_listing != null)
							@foreach($luxury_listing as $automobile)
                            <div class="col-lg-4 col-md-6">
                                <div class="stm-directory-grid-loop">
                                    <div class="image">
                                        <div class="car-listing-slider">
                                            <a href="{{url('/carlisting/'.Crypt::encrypt($automobile->id))}}">
                                            <img src="{{'uploads/'.$images->firstWhere('listing_id','=', $automobile->id)->filename}}" alt="">
                                            </a>
                                        </div>
                                        <div class="stm-badge-directory heading-font Special ">Special</div>
                                    </div>
                                    <div class="listing-car-item-meta">
                                        <div class="car-meta-top heading-font clearfix">
                                            <div class="price">
                                                <div class="normal-price">{{currency()->convert(floatval($automobile->value), 'USD', currency()->getUserCurrency())}}</div>
                                            </div>
                                            <div class="car-title">
                                                <div class="labels">{{$automobile->neworused .' '. $automobile->build_year}}</div> {{$automobile->ad_title}}
                                            </div>
                                        </div>
                                        <div class="car-meta-bottom">
                                            <ul>
                                                <li>
                                                    <i class="fa fa-road"></i>
    
                                                    <span>{{$automobile->odometer}} </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-gear"></i>
                                                    <span>{{$automobile->transmission}}</span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-hand-paper-o"></i>
                                                    <span>{{$automobile->rhdorlhd}}</span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-calendar"></i>
                                                    <span>{{date('Y', strtotime($automobile->automobilecreationtime))}}</span>
                                                </li>
                                            </ul>
                                        </div>
    
                                        <div class="dealer-logo">
                                            <div>
                                                <ul>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                                <p><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>
                                                    {{ $automobile->agency_id === 0 ? "Private Seller" : "Dealer" }}</p>
                                            </div>
                                            <div class="country-flag">
                                                <img src="https://image.flaticon.com/icons/svg/197/197430.svg" alt="">
                                                <p class="c-code">{{$automobile->countrycode}}</p>
                                            </div>
                                        </div>
    
                                    </div>
                                    </a>
                                </div>
                            </div>
                            @endforeach
							@endif
                            
                        </div>

                    </div>-->
                </div>
            </div>
        </div>  
        <section class="car_browse_brand gray-arrow pb-5">
            <div class="container">
                <div class="main-heading text-center">
                    <h3>Browse by Brands </h3>
                </div>
                <div class="car_browse_slider wow animated fadeInUp" data-wow-delay="0.4s">
                    @if($makes != null)
                    @foreach($make_brand as $make)
                    <div class="item">
                        <div class="car_c_item">
                            <a href="{{url('/carlisting/search?make='.$make->id)}}">
                            @if($makes_images->firstWhere('listing_id', '=', $make->id) != null)
                            <img src="{{'uploads/'.$makes_images->firstWhere('listing_id', '=', $make->id)->filename}}" alt="Alt"></a>
                            @else
                            <img src="/assets/img/car/Acura-logo-189x125.png" alt="Alt"></a>
                            @endif
                            <a href="{{url('/carlisting/search?make='.$make->id)}}">
                                <h5>{{$make->automobile_brand_name}} </h5>
                            </a>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </section>
        <section class="stm_cars_on_top  pb-5">
            <div class="container">
                <div class="main-heading text-center">
                    <h3> Featured Listings
                    </h3>
                </div>
                <div class="row  car-listing-row carFeaturedListing">
                @if($latest_listing != null)
                @foreach($latest_listing as $automobile)
                    <div class="col">
                        <div class="stm-directory-grid-loop">
                            <div class="image">
                                <a href="{{url('/carlisting/'.Crypt::encrypt($automobile->id))}}">
                                            <img src="{{'uploads/'.$images->firstWhere('listing_id','=', $automobile->id)->filename}}" alt="">
                                            </a>
                                <div class="stm-badge-directory heading-font ">

                                    Special </div>
                            </div>
                            <div class="listing-car-item-meta">
                                        <div class="car-meta-top heading-font clearfix">
                                            <div class="price">
                                                <div class="normal-price">{{currency()->convert(floatval($automobile->value), 'USD', currency()->getUserCurrency())}}</div>
                                            </div>
                                            <div class="car-title">
                                                <div class="labels">{{$automobile->neworused .' '. $automobile->build_year}}</div> {{$automobile->ad_title}}
                                            </div>
                                        </div>
                                        <div class="car-meta-bottom">
                                            <ul>
                                                <li>
                                                    <i class="fa fa-road"></i>
    
                                                    <span>{{$automobile->odometer}} </span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-gear"></i>
                                                    <span>{{$automobile->transmission}}</span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-hand-paper-o"></i>
                                                    <span>{{$automobile->rhdorlhd}}</span>
                                                </li>
                                                <li>
                                                    <i class="fa fa-calendar"></i>
                                                    <span>{{date('Y', strtotime($automobile->automobilecreationtime))}}</span>
                                                </li>
                                            </ul>
                                        </div>
    
                                        <div class="dealer-logo">
                                            <div>
                                                <ul>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                                <p><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>
                                                    {{ $automobile->agency_id === 0 ? "Private Seller" : "Dealer" }}</p>
                                            </div>
                                            <div class="country-flag">
                                                <img src="https://image.flaticon.com/icons/svg/197/197430.svg" alt="">
                                                <p class="c-code">{{$automobile->countrycode}}</p>
                                            </div>
                                        </div>
    
                                    </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
					@endif

                </div>

                <div class="text-center pt-2    ">
                    <a href="/carlisting/search?sortBy=3" class="read-more fill-read-more">Show Latest Car Listings</a>
                </div>



            </div>
        </section>

        <section class="car_browse_area gray-arrow pb-5">
            <div class="container">
                <div class="main-heading text-center">
                    <h3> Browse by Type </h3>
                </div>
                <div class="car_browse_slider wow animated fadeInUp" data-wow-delay="0.4s">
                    @if($body_types != null)
                    @foreach($body_types as $model)
                    @if(count($total_listing->where('body_type_id', $model->id)))
                    <div class="item">
                        <div class="car_c_item">
                            <a href="{{url('/carlisting/search?body_type='.$model->id)}}">
                            @if($body_types_images->firstWhere('listing_id', '=', $model->id) != null)
                            <img src="{{'uploads/'.$body_types_images->firstWhere('listing_id', '=', $model->id)->filename}}" alt="Alt"></a>
                            @else
                            <img src="/assets/img/car/car-6.png"  class="img-fluid"></a>
                            @endif
                            <a href="{{url('/carlisting/search?body_type='.$model->id)}}">
                                <h5>{{$model->body_type}}<span>{{count($total_listing->where('body_type_id', $model->id))}}</span></h5>
                            </a>
                        </div>
                    </div>
                    @endif
                    @endforeach
                   @endif
                </div>
            </div>
        </section>



        <!-- <div class="custom-testimonial-wrapper grey-bg">
                <div class="container">
                    <div class="main-heading text-center">
                        <h3 class="cl-white">Our Testimonial</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
                    </div>

                    <div class="testimonial-slider">
                        <div class="testimonial-item">
                            <div class="testimonial-info">
                                <div class="testimonial-detail">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it
                                        to make a type specimen book. It has survived not</p>
                                    <h3>Anne Brady</h3>
                                    <h4>Villa Owner</h4>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial-item">
                            <div class="testimonial-info">
                                <div class="testimonial-detail">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it
                                        to make a type specimen book. It has survived not</p>
                                    <h3>Anne Brady</h3>
                                    <h4>Villa Owner</h4>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial-item">
                            <div class="testimonial-info">
                                <div class="testimonial-detail">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it
                                        to make a type specimen book. It has survived not</p>
                                    <h3>Anne Brady</h3>
                                    <h4>Villa Owner</h4>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial-item">
                            <div class="testimonial-info">
                                <div class="testimonial-detail">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it
                                        to make a type specimen book. It has survived not</p>
                                    <h3>Anne Brady</h3>
                                    <h4>Villa Owner</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->

        <div class="our-agent-wrapper   pt-5 pb-5">
            <div class="container">
                <div class="main-heading text-center">
                    <h3>Our Dealers</h3>
                </div>
                <div class="row">
                    
                    @if($dealer_listing)
            @foreach ($dealer_listing as $data)
                <div class="col-lg-3 col-md-6">
                    <div class="agent-detail-box">
                        <div class="agent-photo">
                            <a href="#">
                                <img src="{{url('uploads/'.$data->filename)}}" class="img-fluid">
                            </a>
                        </div>
                        <div class="agent-details">
                            <h4><a href="#">{{$data->first_name}}</a></h4>
                            <h5>400+</h5>
                            <a href="#" class="read-more d-block text-center">Read more</a>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="home-item">
					<div class="home-detail">
						<div class="home-list-box">
						<img src="{{url('/assets/images/NoImageFound.png')}}" alt="" class="img-fluid">
						</div>
						</div>
						</div>
                
                @endif
                    
                    
                </div>
            </div>
        </div>



        <div class="custom-our-blog  pt-5 pb-5">
            <div class="container">
                <div class="main-heading text-center">
                    <h3>Our Articles</h3>
                </div>
                <div class="row">
                    
                    @if($auto_blogs != null)	            
                <div class="col-sm-7">
                    <div class="blog-left">
                        <div class="blog__item">
                        @if($auto_blogs[0]->url != null )
                        <a class="home-view" href="{{url($auto_blogs[0]->url)}}"><img src="{{'uploads/'.$blog_images->firstWhere('listing_id', '=', $auto_blogs[0]->id)->filename}}" alt=""
                                    class="img-fluid"></a>
                        @else
                            <a class="home-view" href="{{url('carblog/'.$auto_blogs[0]->id)}}"><img src="{{'uploads/'.$blog_images->firstWhere('listing_id', '=', $auto_blogs[0]->id)->filename}}" alt=""
                                    class="img-fluid"></a>
                           @endif         
                            <div class="blog-detail-content">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i> <span>{{date_format($auto_blogs[0]->created_at, 'F d, Y')}}</span></span>
                                    <span><i class="fas fa-tag"></i> <a href="#"> {{$auto_blogs[0]->blogType}}</a></span>
                                </div>
                                @if($auto_blogs[0]->url != null )
                                <h4><a href="{{url($auto_blogs[0]->url)}}">{{$auto_blogs[0]->title}}</a></h4>
                                @else
                                <h4><a href="{{url('carblog/'.$auto_blogs[0]->id)}}">{{$auto_blogs[0]->title}}</a></h4>
                                @endif
                                <p class="article-para">{{$auto_blogs[0]->blogPost}}</p>

                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <div class="col-sm-5">
                    <div class="blog-right">
						@if(count($auto_blogs) >= 1)                    
						@for ($i = 1; $i < count($auto_blogs); $i++)                        
                        <div class="blog__right-item">
                            <div class="blog__right-item__left">
                            @if($auto_blogs[$i]->url != null)
                            <a class="home-view" href="{{url($auto_blogs[$i]->url)}}"><img src="{{'uploads/'.$blog_images->firstWhere('listing_id', '=', $auto_blogs[$i]->id)->filename}}" alt=""
                                        class="img-fluid"></a>
                            @else
                                <a class="home-view" href="{{url('carblog/'.$auto_blogs[$i]->id)}}"><img src="{{'uploads/'.$blog_images->firstWhere('listing_id', '=', $auto_blogs[$i]->id)->filename}}" alt=""
                                        class="img-fluid"></a>
                             @endif           
                            </div>
                            <div class="blog__right-item__right">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i> <span>{{date_format($auto_blogs[$i]->created_at, 'F d, Y')}}</span></span>

                                </div>
                                @if($auto_blogs[$i]->url != null)
                                <h4><a href="{{url($auto_blogs[$i]->url)}}">{{$auto_blogs[$i]->title}}</a></h4>
                                @else
                                <h4><a href="{{url('carblog/'.$auto_blogs[$i]->id)}}">{{$auto_blogs[$i]->title}}</a></h4>
                                @endif
                                <p class="article-para">{{$auto_blogs[$i]->blogPost}} </p>


                            </div>
                        </div>
						@endfor
						@endif
                    </div>
                </div>
                    
                </div>
            </div>
        </div>
        <section class="become-a-section">
            <div class="container">
                <h3>If you are a seller in automobiles, watches or real estate, sign up below to advertise your inventory globally.</h3>
                <a href="/becomedealer" class="read-more fill-read-more">Become a Seller</a>
            </div>

        </section>

        @include('tangiblehtml.innerfooter')


        <script src="/assets/js/jquery.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <script src="/assets/js/wow.min.js"></script>
        <script src="/assets/js/slick.min.js"></script>
        <script src="/assets/js/wow.min.js"></script>
        <script src="/assets/js/local.js"></script>
</body>
<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">

$(document).ready(function(){

	$("#brand").change(function(){
		 $('#findValue').prop('disabled', false);
		 var checked = [];
		 checked.push($(this).val());	
		 $.ajax({
	            url:"/carlisting/brand",
	            type: 'post',
	            dataType: "json",
	            data: {
	               _token: '{{csrf_token()}}',
	               brandfilter:checked ,
	               type: 'brand'
	            },
	            success: function( data ) {
					var listingHtml= '';
					if(data['model'] != null){
					var modelHtml = "";
					var modelData =data['model'];
					modelHtml = `<option value="" disabled selected>All Models</option>`;  
					for(var i=0; i < modelData.length; i++){
					    if(modelData[i].status == '1'){
						modelHtml += `
                      	<option value="${modelData[i].id}">${modelData[i].automobile_model_name}</option>`
					    }
						}
						$('#model').html(modelHtml);
						}	               
	            }
	          });
	});
	
	$("#year_from").change(function(){
	     $.ajax({
	            url:"/carlisting/getYear",
	            type: 'post',
	            dataType: "json",
	            data: {
	               _token: '{{csrf_token()}}',
	               year:$(this).val()
	            },
	            success: function( data ) {
					var to_year = '';
					var year = data['to_year'];
					to_year = `<option value="" disabled selected>To year</option>`;
					for(var i=0; i < year.length; i++){
						to_year += `
	                      	<option value="${year[i].build_year}">${year[i].build_year}</option>`
						}	
					$('#year_to').html(to_year);	          
	            }
	          });
		 
	});

	$('#clear').click(function (e) {
		$('#brand')[0].selectedIndex = 0;
		$('#model')[0].selectedIndex = 0;
		$('#year_from')[0].selectedIndex = 0;
		$('#year_to')[0].selectedIndex = 0;
		});

	
	$("#model").change(function(){
	     console.log('---------------------');
		 $('#findValue-0').prop('disabled', false);
	});
	$("#year_from").change(function(){
	     console.log('---------------------');
		 $('#findValue-0').prop('disabled', false);
	});
	$("#year_to").change(function(){
	     console.log('---------------------');
		 $('#findValue-0').prop('disabled', false);
	});
	
});

</script>
</html>