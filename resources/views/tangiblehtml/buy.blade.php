<!DOCTYPE html>
<html lang="en">
@include('tangiblehtml.headheader')
<body>
    @include('tangiblehtml.innerheader')
   <div class="custom-top-home-wrapper pt-5 ">
        <div class="container">
            <div class="main-heading mt-5">
                <h3>Buy</h3>                
            </div>
            @if($enquiry_property == null || $enquiry_automobile == null || $enquiry_watch == null)
            <div class="noproduct text-center p-5 border mb-5">
                <i class="fa-5x fa fa-shopping-cart p-3"></i>
                <h2>You haven't purchased any items yet</h2>
                <p>Here you'll find the current status of your price suggestions, purchase requests, and orders on Tangible.</p>
                <div class="d-flex justify-content-center align-items-center flex-column">Make Secure Purchases with Tangible Buyer Protection
                    <a href="#">Learn more</a>
                 </div>
            </div>
             @endif 
            @if($enquiry_automobile != null)
            @foreach ($enquiry_automobile as $data)
            <div class="row d-flex align-items-center border p-3 mb-3">
                <div class="col-lg-3 col-md-3 mb-4">
                    <div class="border p-2">
                        <div class="ads-view position-relative">                          
                            <div class="home-list-item">
                                <a class="home-view" href="#"><img src="{{'uploads/'.$automobile_images->firstWhere('listing_id','=', $data->productId)->filename}}" alt="" class="img-fluid"></a>
                            </div>                            
                        </div>                        
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="inner">
                        <div class="home-content mt-1 border-right" >  
                            <p class="muted">TD:{{$data->listing_id}}</p>                          
                            <h4 class="mb-1"><a href="#">{{$data->ad_title}}</a></h4>  
                            <p class="font-weight-bold">{{currency()->convert(floatval($data->value), 'USD', currency()->getUserCurrency())}}</p>                        
                            <div class="d-flex mt-2 flex-column">
                                <div class="paymentTrusted mb-2">
                                    <i class="fa fa-payment"></i>Payment via Trusted Checkout
                                </div>    
                                <div class="SellerName mb-2">
                                    <i class="fa fa-payment"></i>@if($data->company_name != null)
                            {{$data->company_name}}
                            @else
                            {{$data->agent_name}}
                            @endif<a href="#">(Contact)</a>
                                </div>    
                                <div class="country mb-2">
                                    <img src="{{'uploads/'.$data->countryflag}}" alt="" width="25px"> {{$data->country_name}}
                                   
                                </div>                 
                            </div>              
                        </div>                       
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="inner">
                        <div class="font-weight-bold">
                            Canceled on Dec 6, 2020                           
                         </div>                         
                    </div>
                </div>
            </div>
            @endforeach
            @endif
            @if($enquiry_watch != null)
            @foreach ($enquiry_watch as $data)
            <div class="row d-flex align-items-center border p-3 mb-3">
                <div class="col-lg-3 col-md-3 mb-4">
                    <div class="border p-2">
                        <div class="ads-view position-relative">                          
                            <div class="home-list-item">
                                <a class="home-view" href="#"><img src="{{'uploads/'.$watch_images->firstWhere('listing_id','=', $data->productId)->filename}}" alt="" class="img-fluid"></a>
                            </div>                            
                        </div>                        
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="inner">
                        <div class="home-content mt-1 border-right" >  
                            <p class="muted">TD:{{$data->listing_id}}</p>                          
                            <h4 class="mb-1"><a href="#">{{$data->ad_title}}</a></h4>  
                            <p class="font-weight-bold">{{currency()->convert(floatval($data->value), 'USD', currency()->getUserCurrency())}}</p>                        
                            <div class="d-flex mt-2 flex-column">
                                <div class="paymentTrusted mb-2">
                                    <i class="fa fa-payment"></i>Payment via Trusted Checkout
                                </div>    
                                <div class="SellerName mb-2">
                                    <i class="fa fa-payment"></i>@if($data->company_name != null)
                            {{$data->company_name}}
                            @else
                            {{$data->agent_name}}
                            @endif<a href="#">(Contact)</a>
                                </div>    
                                <div class="country mb-2">
                                    <img src="{{'uploads/'.$data->countryflag}}" alt="" width="25px"> {{$data->country_name}}
                                   
                                </div>                 
                            </div>              
                        </div>                       
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="inner">
                        <div class="font-weight-bold">
                            Canceled on Dec 6, 2020                           
                         </div>                         
                    </div>
                </div>
            </div>
            @endforeach
            @endif
            @if($enquiry_property != null)
            @foreach ($enquiry_property as $data)
            <div class="row d-flex align-items-center border p-3 mb-3">
                <div class="col-lg-3 col-md-3 mb-4">
                    <div class="border p-2">
                        <div class="ads-view position-relative">                          
                            <div class="home-list-item">
                                <a class="home-view" href="#"><img src="{{'uploads/'.$property_images->firstWhere('listing_id','=', $data->productId)->filename}}" alt="" class="img-fluid"></a>
                            </div>                            
                        </div>                        
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="inner">
                        <div class="home-content mt-1 border-right" >  
                            <p class="muted">TD:{{$data->listing_id}}</p>                          
                            <h4 class="mb-1"><a href="#">{{$data->ad_title}}</a></h4>  
                            <p class="font-weight-bold">{{currency()->convert(floatval($data->value), 'USD', currency()->getUserCurrency())}}</p>                        
                            <div class="d-flex mt-2 flex-column">
                                <div class="paymentTrusted mb-2">
                                    <i class="fa fa-payment"></i>Payment via Trusted Checkout
                                </div>    
                                <div class="SellerName mb-2">
                                    <i class="fa fa-payment"></i>@if($data->company_name != null)
                            {{$data->company_name}}
                            @else
                            {{$data->agent_name}}
                            @endif<a href="#">(Contact)</a>
                                </div>    
                                <div class="country mb-2">
                                    <img src="{{'uploads/'.$data->countryflag}}" alt="" width="25px"> {{$data->country_name}}
                                   
                                </div>                 
                            </div>              
                        </div>                       
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="inner">
                        <div class="font-weight-bold">
                            Canceled on Dec 6, 2020                           
                         </div>                         
                    </div>
                </div>
            </div>
            @endforeach
            @endif
            
            <!-- <div class="row d-flex align-items-center border p-3 mb-3">
                <div class="col-lg-3 col-md-3 mb-4">
                    <div class="border p-2">
                        <div class="ads-view position-relative">                          
                            <div class="home-list-item">
                                <a class="home-view" href="#"><img src="images/car-image4.jpg" alt="" class="img-fluid"></a>
                            </div>                            
                        </div>                        
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="inner">
                        <div class="home-content mt-1 border-right" >  
                            <p class="muted">TD:23424234</p>                          
                            <h4 class="mb-1"><a href="#">Luxury Car - Audi</a></h4>  
                            <p class="font-weight-bold">$80,000</p>                        
                            <div class="d-flex mt-2 flex-column">
                                <div class="paymentTrusted mb-2">
                                    <i class="fa fa-payment"></i>Payment via Trusted Checkout
                                </div>    
                                <div class="SellerName mb-2">
                                    <i class="fa fa-payment"></i>Sky Diamonds <a href="#">(Contact)</a>
                                </div>    
                                <div class="country mb-2">
                                    <img src="https://image.flaticon.com/icons/svg/197/197374.svg" alt="" width="25px"> United Stated of America
                                   
                                </div>                 
                            </div>              
                        </div>                       
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="inner">
                        <div class="font-weight-bold">
                            Canceled on Dec 6, 2020                           
                         </div>                         
                    </div>
                </div>
            </div> -->
            <!-- <div class="row d-flex align-items-center border p-3 mb-3">
                <div class="col-lg-3 col-md-3 mb-4">
                    <div class="border p-2">
                        <div class="ads-view position-relative">                          
                            <div class="home-list-item">
                                <a class="home-view" href="#"><img src="images/car-image3.jpg" class="img-fluid"></a>
                            </div>                            
                        </div>                        
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="inner">
                        <div class="home-content mt-1 border-right" >  
                            <p class="muted">TD:23424234</p>                          
                            <h4 class="mb-1"><a href="#">Luxury Car - Audi</a></h4>  
                            <p class="font-weight-bold">$80,000</p>                        
                            <div class="d-flex mt-2 flex-column">
                                <div class="paymentTrusted mb-2">
                                    <i class="fa fa-payment"></i>Payment via Trusted Checkout
                                </div>    
                                <div class="SellerName mb-2">
                                    <i class="fa fa-payment"></i>Sky Diamonds <a href="#">(Contact)</a>
                                </div>    
                                <div class="country mb-2">
                                    <img src="https://image.flaticon.com/icons/svg/197/197374.svg" alt="" width="25px"> United Stated of America
                                   
                                </div>                 
                            </div>              
                        </div>                       
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="inner">
                        <div class="font-weight-bold">
                            Canceled on Dec 6, 2020                           
                         </div>                         
                    </div>
                </div>
            </div> -->
        </div>
    </div>
    @include('tangiblehtml.innerfooter')
    <script src="/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/local.js"></script>

    <script>
    $(document).on('click', '.search-filter-panel .dropdown-menu', function (e) {
      e.stopPropagation();   
    });
    </script>
</body>

</html>