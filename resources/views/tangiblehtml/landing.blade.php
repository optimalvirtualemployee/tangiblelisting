<!DOCTYPE html>
<html lang="en">@include('tangiblehtml.headheader')
<style>
.home-nav {
    background-color: transparent !important;
}

.article-para {
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
}
</style>

<body>@include('tangiblehtml.innerheader')
    <!--banner area-->
    <div class="custom-landing-banner-wrapper main-landing">
        <div class="custom-landing-banner-slider">
            <div class="landing-banner-item ">
                <div class="container">
                    <div class="caption">
                        <h2>The Global </h2>
                        <h3>Luxury Marketplace </h3>
                        <p>
                        </p> <a href="/carlisting" class="read-more fill-read-more"> Explore More</a>
                    </div>
                </div>
                <img src="/assets/img/car-image3.jpg" alt="" class="img-fluid">
            </div>
            <div class="landing-banner-item">
                <div class="container">
                    <div class="caption">
                        <h2>The Global </h2>
                        <h3>Luxury Marketplace </h3>
                        <p>
                        </p> <a href="/watchlisting" class="read-more fill-read-more"> Explore More</a>
                    </div>
                </div>
                <img src="/assets/img/watch3.jpg" alt="" class="img-fluid">
            </div>
            <div class="landing-banner-item">
                <div class="container">
                    <div class="caption">
                        <h2>The Global </h2>
                        <h3>Luxury Marketplace </h3>
                        <p>
                        </p> <a href="/propertylisting" class="read-more fill-read-more"> Explore More</a>
                    </div>
                </div>
                <img src="/assets/img/banner-villa2.jpg" alt="" class="img-fluid">
            </div>
        </div>
    </div>
    <!--banner area end-->
    <div class="custom-find-home grey-bg pt-5 pb-5">
        <div class="container">
            <div class="main-heading">
                <h3>Latest Listings
                </h3>
            </div>
            <div class="perfect-home-slider">@if($propertyListing) @foreach ($propertyListing as $data)
                <div class="home-item">
                    <div class="home-detail">
                        <div class="home-list-box"> <a class="home-view"
                                href="{{route('tangiblerealestate.show',Crypt::encrypt($data->id))}}">
                                <img src="{{url('uploads/'.$images->firstWhere('listing_id',$data->id)->filename)}}"
                                    alt="" class="img-fluid">

                            </a>
                            <div class="home-content listing-home-content">
                                <div class="propertyListing d-flex justify-content-between align-items-center"> <span
                                        class="price order-2">{{currency()->convert(floatval($data->property_price), 'USD', currency()->getUserCurrency())}}</span>
                                    <h4 class="mb-0"><a
                                            href="{{route('tangiblerealestate.show',$data->id)}}">{{$data->ad_title}}</a>
                                    </h4>
                                </div>
                                <div class="fasility-item"> <span><i
                                            class="fas fa-bed"></i>{{$data->number_of_bedrooms}}</span>
                                    <span><i class="fas fa-chart-area"></i>
                                        {{number_format($data->land_size).' '.$data->metric}}</span>
                                    <span><i class="fas fa-shower"></i> {{$data->number_of_bathrooms}}</span>
                                </div>
                                <a class=""
                                    href="{{url('/propertylisting/search?home-'.$data->property_type. '=' .$data->propertyId)}}">
                                    <div class="apartment_wrap">{{$data->property_type}}</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>@endforeach @else
                <div class="home-item">
                    <div class="home-detail">
                        <div class="home-list-box">
                            <img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>@endif
            </div>
        </div>
    </div>
    <div class="custom-latestoffer-wrapper pt-5 pb-5">
        <div class="container">
            <div class="main-heading">
                <h3>Featured Listings </h3>
            </div>
            <div class="row">
                <!-- <div class="col-lg-3">@if($latest_automobile)
                    <div class="latestCar  border p-3">
                        <div class="listingImage">
                            <div class="imgList">
                                <img src="{{url('uploads/'.$latest_automobile->filename)}}" alt="" class="img-fluid">
                            </div>
                            <div class="listingStatus">New</div>
                        </div>
                        <div class="listingBottom">
                            <div class="price">
                                {{currency()->convert(floatval($latest_automobile->value), 'USD', currency()->getUserCurrency())}}
                            </div>
                            <div class="description"> <a
                                    href="{{url('/carlisting/'.Crypt::encrypt($latest_automobile->id))}}">{{$latest_automobile->automobile_brand_name .' '. $latest_automobile->automobile_model_name}}</a>
                            </div>
                        </div>
                    </div>@else
                    <div class="home-item">
                        <div class="home-detail">
                            <div class="home-list-box">
                                <img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>@endif
                </div>
                <div class="col-lg-6">
                    <div class="inner">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="inner border p-3">@if(isset($latest_watches[0]))
                                    <div class="listingImage">
                                        <div class="imgList">
                                            <img src="{{url('uploads/'.$latest_watches[0]->filename)}}" alt=""
                                                class="img-fluid">
                                        </div>
                                        <div class="listingStatus">New</div>
                                    </div>
                                    <div class="listingBottom">
                                        <div class="price">
                                            {{currency()->convert(floatval($latest_watches[0]->watch_price), 'USD', currency()->getUserCurrency())}}
                                        </div>
                                        <div class="description"> <a
                                                href="{{url('/watchlisting/'.Crypt::encrypt($latest_watches[0]->id))}}">{{$latest_watches[0]->brand_name}}</a>
                                        </div>
                                    </div>@else
                                    <div class="home-item">
                                        <div class="home-detail">
                                            <div class="home-list-box">
                                                <img src="{{url('/assets/img/NoImageFound.png')}}" alt=""
                                                    class="img-fluid">
                                            </div>
                                        </div>
                                    </div>@endif
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="inner border p-3">@if(isset($latest_watches[1]))
                                    <div class="listingImage  ">
                                        <div class="imgList">
                                            <img src="{{url('uploads/'.$latest_watches[0]->filename)}}" alt=""
                                                class="img-fluid">
                                        </div>
                                        <div class="listingStatus">New</div>
                                    </div>
                                    <div class="listingBottom">
                                        <div class="price">
                                            {{currency()->convert(floatval($latest_watches[1]->watch_price), 'USD', currency()->getUserCurrency())}}
                                        </div>
                                        <div class="description"> <a
                                                href="{{url('/watchlisting/'.Crypt::encrypt($latest_watches[0]->id))}}">{{$latest_watches[1]->brand_name}}</a>
                                        </div>
                                    </div>@else
                                    <div class="home-item">
                                        <div class="home-detail">
                                            <div class="home-list-box">
                                                <img src="{{url('/assets/img/NoImageFound.png')}}" alt=""
                                                    class="img-fluid">
                                            </div>
                                        </div>
                                    </div>@endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">@if($latest_property)
                    <div class="latestproperty  border p-3">
                        <div class="listingImage">
                            <div class="imgList"> <a class="home-view"
                                    href="{{route('tangiblerealestate.show',$data->id)}}">
                                    <img src="{{url('uploads/'.$images->firstWhere('listing_id',$latest_property->id)->filename)}}"
                                        alt="" class="img-fluid">
                                </a>
                            </div>
                            <div class="listingStatus">New</div>
                        </div>
                        <div class="listingBottom">
                            <div class="price">
                                {{currency()->convert(floatval($latest_property->property_price), 'USD', currency()->getUserCurrency())}}
                            </div>
                            <div class="description"> <a
                                    href="{{url('/tangiblerealestate/'.Crypt::encrypt($latest_property->id))}}">{{$latest_property->property_type}}</a>
                            </div>
                        </div>
                    </div>@else
                    <div class="home-item">
                        <div class="home-detail">
                            <div class="home-list-box">
                                <img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>@endif
                </div> -->
                <div class="col-lg-7 col-md-12">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 home-content">
                            @if($latest_automobile)
                            <div class="offer-category-list mb-2">
                                <div class="offer-category-bg-box cat-1-bg"> <a class="home-view" href="#">
                                        <img src="{{url('uploads/'.$latest_automobile->filename)}}" alt=""
                                            class="img-fluid">
                                    </a>
                                    <div class="offer-category-overlay">
                                        <div class="offer-category-content">
                                            <div class="new-offer">New</div>                                            
                                        </div>
                                    </div>
                                </div>                               
                            </div>
                            <div class="propertyListing d-flex justify-content-between align-items-center"> 
                                <span class="price order-2">{{currency()->convert(floatval($latest_automobile->value), 'USD', currency()->getUserCurrency())}}</span>
                                <h4 class="mb-0"><a href="{{url('/carlisting/'.Crypt::encrypt($latest_automobile->id))}}">{{$latest_automobile->automobile_brand_name .' '. $latest_automobile->automobile_model_name}}</a>
                                </h4>
                            </div>                            
                            @else
                            <div class="home-item">
                                    <div class="home-detail">
                                        <div class="home-list-box">
                                            <img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 col-lg-6 home-content">
                            @if(isset($latest_watches[0]))
                            <div class="offer-category-list mb-2">                                
                                <div class="offer-category-bg-box cat-2-bg">
                                    <a class="home-view" href="#"><img src="{{url('uploads/'.$latest_watches[0]->filename)}}" alt="" class="img-fluid"></a>
                                    <div class="offer-category-overlay">
                                        <div class="offer-category-content">
                                            <div class="new-offer">New</div>                                            
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <div class="propertyListing d-flex justify-content-between align-items-center"> 
                                <span class="price order-2">{{currency()->convert(floatval($latest_watches[0]->watch_price), 'USD', currency()->getUserCurrency())}}</span>
                                <h4 class="mb-0"><a href="{{url('/watchlisting/'.Crypt::encrypt($latest_watches[0]->id))}}">{{$latest_watches[0]->brand_name}}</a></h4>
                            </div>
                                @else
                                <div class="home-item">
                                    <div class="home-detail">
                                        <div class="home-list-box">
                                            <img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                @endif
                        </div>
                        <div class="col-md-12 col-lg-6 home-content">
                            @if(isset($latest_watches[1]))
                            <div class="offer-category-list mb-2">                                
                                <div class="offer-category-bg-box cat-2-bg">
                                    <a class="home-view" href="#"><img src="{{url('uploads/'.$latest_watches[0]->filename)}}" alt="" class="img-fluid"></a>
                                    <div class="offer-category-overlay">
                                        <div class="offer-category-content">
                                            <div class="new-offer">New</div>                                            
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <div class="propertyListing d-flex justify-content-between align-items-center"> 
                                <span class="price order-2">{{currency()->convert(floatval($latest_watches[1]->watch_price), 'USD', currency()->getUserCurrency())}}</span>
                                <h4 class="mb-0"><a href="{{url('/watchlisting/'.Crypt::encrypt($latest_watches[0]->id))}}">{{$latest_watches[1]->brand_name}}</a></h4>
                            </div>
                                @else
                                <div class="home-item">
                                    <div class="home-detail">
                                        <div class="home-list-box">
                                            <img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                @endif
                        </div>
                    </div>
                </div> 
                 <div class="col-lg-5 col-md-12 home-content">
                     @if($latest_property)
                    <div class="offer-category-list mb-2">                      
                        <div class="offer-category-bg-box offer-category_long_bg">
                            <a class="home-view" href="{{route('tangiblerealestate.show',$data->id)}}">
                                <img src="{{url('uploads/'.$images->firstWhere('listing_id',$latest_property->id)->filename)}}"
                                    alt="" class="img-fluid  offer-category_long_bg">
                            </a>
                            <div class="offer-category-overlay">
                                <div class="offer-category-content">
                                    <div class="new-offer">New</div>                                    
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <div class="propertyListing d-flex justify-content-between align-items-center"> 
                        <span class="price order-2">{{currency()->convert(floatval($latest_property->property_price), 'USD', currency()->getUserCurrency())}}</span>
                        <h4 class="mb-0"> <a href="{{url('/tangiblerealestate/'.Crypt::encrypt($latest_property->id))}}">{{$latest_property->property_type}}</a></h4>
                    </div>
                    @else
                        <div class="home-item">
                            <div class="home-detail">
                                <div class="home-list-box">
                                    <img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>

                        @endif
                </div>
            </div>
        </div>
    </div>
    <div class="custom-service-section pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 align-self-center mb-4 mb-lg-auto">
                    <div class="main-title">
                        <h1>We Are The Best</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
                        <a href="#" class="read-more">Read more</a>
                    </div>
                </div>
                <div class="col-lg-7 offset-lg-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="custom-service-info">
                                <div class="service-icon">
                                    <i class="fas fa-shield-alt"></i>
                                </div>
                                <div class="our-service-detail">
                                    <h3>Highly secure payments:</h3>
                                    <p>Utilise our escrow payment service to ensure secure and safe transactions
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="custom-service-info">
                                <div class="service-icon">
                                    <i class="far fa-handshake"></i>
                                </div>
                                <div class="our-service-detail">
                                    <h3>Trusted Agents</h3>
                                    <p>Our agents are individually verified by tangible Listings to ensure legitimacy 
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="custom-service-info">
                                <div class="service-icon">
                                    <i class="fas fa-dollar-sign"></i>
                                </div>
                                <div class="our-service-detail">
                                    <h3>Get an Offer</h3>
                                    <p>Negotiate with the seller to get the best price
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="custom-service-info">
                                <div class="service-icon">
                                    <i class="fas fa-headset"></i>
                                </div>
                                <div class="our-service-detail">
                                    <h3>Free Support</h3>
                                    <p>Get in contact with our support service if you have any further queries. 
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="our-agent-wrapper   pt-5 pb-5">
        <div class="container">
            <div class="main-heading text-center">
                <h3>Featured Dealers</h3>
            </div>
            <div class="row">
                @if($dealer_listing)
                @foreach ($dealer_listing as $data)
                <div class="col-lg-3 col-md-6">
                    <div class="agent-detail-box">
                        <div class="agent-photo">
                            <a href="#">
                                <img src="{{url('uploads/'.$data->filename)}}" class="img-fluid">
                            </a>
                        </div>
                        <div class="agent-details">
                            <h4><a href="#">{{$data->first_name}}</a></h4>
                            <h5>400+</h5>
                            <a href="#" class="read-more d-block text-center">Read more</a>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="home-item">
                    <div class="home-detail">
                        <div class="home-list-box">
                            <img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>

                @endif
            </div>
        </div>
    </div>

    <div class="custom-testimonial-wrapper grey-bg">
        <div class="container">
            <div class="main-heading text-center">
                <h3 class="cl-white">Testimonials</h3>
            </div>

            <div class="testimonial-slider">
                <div class="testimonial-item">
                    <div class="testimonial-info">
                        <div class="testimonial-detail">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make
                                a type specimen book. It has survived not</p>
                            <h3>Anne Brady</h3>
                            <h4>Villa Owner</h4>
                        </div>
                    </div>
                </div>
                <div class="testimonial-item">
                    <div class="testimonial-info">
                        <div class="testimonial-detail">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make
                                a type specimen book. It has survived not</p>
                            <h3>Anne Brady</h3>
                            <h4>Villa Owner</h4>
                        </div>
                    </div>
                </div>
                <div class="testimonial-item">
                    <div class="testimonial-info">
                        <div class="testimonial-detail">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make
                                a type specimen book. It has survived not</p>
                            <h3>Anne Brady</h3>
                            <h4>Villa Owner</h4>
                        </div>
                    </div>
                </div>
                <div class="testimonial-item">
                    <div class="testimonial-info">
                        <div class="testimonial-detail">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make
                                a type specimen book. It has survived not</p>
                            <h3>Anne Brady</h3>
                            <h4>Villa Owner</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="custom-our-blog  pt-5 pb-5">
        <div class="container">
            <div class="main-heading text-center">
                <h3>Our Articles</h3>
            </div>
            <div class="row">
                <div class="col-sm-7">
                    <div class="blog-left">
                        @if($property_blog)
                        <div class="blog__item">
                            @if($property_blog->url != null)
                            <a class="home-view" href="{{url($property_blog->url)}}"><img
                                    src="{{'uploads/'.$property_blog->filename}}" alt="" class="img-fluid"></a>
                            @else
                            <a class="home-view" href="{{url('propertyblog/'.$property_blog->id)}}"><img
                                    src="{{'uploads/'.$property_blog->filename}}" alt="" class="img-fluid"></a>
                            @endif
                            <div class="blog-detail-content">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i>
                                        <span>{{date_format($property_blog->created_at, 'F d, Y')}}</span></span>
                                    <span><i class="fas fa-tag"></i> <a href="#">
                                            {{$property_blog->blogType}}</a></span>
                                </div>
                                @if($property_blog->url != null)
                                <h4><a href="{{url($property_blog->url)}}">{{$property_blog->title}}</a></h4>
                                @else
                                <h4><a href="{{url('propertyblog/'.$property_blog->id)}}">{{$property_blog->title}}</a>
                                </h4>
                                @endif
                                <p class="article-para">{{$property_blog->blogPost}}</p>

                            </div>
                        </div>
                        @else
                        <div class="home-item">
                            <div class="home-detail">
                                <div class="home-list-box">
                                    <img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>

                        @endif
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="blog-right">
                        @if($automobile_blog)
                        <div class="blog__right-item">
                            <div class="blog__right-item__left">
                                @if($automobile_blog->url != null)
                                <a class="home-view" href="{{url($automobile_blog->url)}}"><img
                                        src="{{'uploads/'.$automobile_blog->filename}}" alt="" class="img-fluid"></a>
                                @else
                                <a class="home-view" href="{{url('carblog/'.$automobile_blog->id)}}"><img
                                        src="{{'uploads/'.$automobile_blog->filename}}" alt="" class="img-fluid"></a>
                                @endif
                            </div>
                            <div class="blog__right-item__right">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i>
                                        <span>{{date_format($automobile_blog->created_at, 'F d, Y')}}</span></span>

                                </div>
                                @if($automobile_blog->url != null)
                                <h4><a href="{{url($automobile_blog->url)}}">{{$automobile_blog->title}}</a></h4>
                                @else
                                <h4><a href="{{url('carblog/'.$automobile_blog->id)}}">{{$automobile_blog->title}}</a>
                                </h4>
                                @endif
                                <p class="article-para">{{$automobile_blog->blogPost}}</p>


                            </div>
                            @else
                            <div class="home-item">
                                <div class="home-detail">
                                    <div class="home-list-box">
                                        <img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>

                            @endif
                        </div>
                        @if($watch_blog)
                        @foreach($watch_blog as $data)
                        <div class="blog__right-item">
                            <div class="blog__right-item__left">
                                @if($data->url != null)
                                <a class="home-view" href="{{url($data->url)}}"><img
                                        src="{{'uploads/'.$data->filename}}" alt="" class="img-fluid"></a>
                                @else
                                <a class="home-view" href="{{url('watchblog/'.$data->id)}}"><img
                                        src="{{'uploads/'.$data->filename}}" alt="" class="img-fluid"></a>
                                @endif
                            </div>
                            <div class="blog__right-item__right">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i>
                                        <span>{{date_format($data->created_at, 'F d, Y')}}</span></span>
                                </div>
                                @if($data->url != null)
                                <h4><a href="{{url($data->url)}}">{{$data->title}}</a></h4>
                                @else
                                <h4><a href="{{url('watchblog/'.$data->id)}}">{{$data->title}}</a></h4>
                                @endif
                                <p class="article-para">{{$data->blogPost}}</p>


                            </div>
                        </div>
                        @endforeach
                        @else
                        <div class="home-item">
                            <div class="home-detail">
                                <div class="home-list-box">
                                    <img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="become-a-section">
        <div class="container">
            <h3>If you are a seller in automobiles, watches or real estate, sign up below to advertise your inventory globally.</h3>
            <a href="/becomedealer" class="read-more fill-read-more">Become a Seller</a>
        </div>

    </section>
    @include('tangiblehtml.innerfooter')
    <script src="/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/local.js"></script>
</body>

</html>