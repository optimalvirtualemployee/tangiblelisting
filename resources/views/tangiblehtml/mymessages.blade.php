<!DOCTYPE html>
<html lang="en">

@include('tangiblehtml.headheader')
<head>
<link rel="stylesheet" href="/assets/css/chat-box.css">
</head>
<body>
    @include('tangiblehtml.innerheader')

    <section class="become-seller-from overlay-wrapper">
      
    </section>
    <div class="container mt-5">  
        <div class="main-heading stm-border-bottom-unit bg-color-border">
            <h3>My Messages</h3>         
        </div>    
        <div class="messaging">
              <div class="inbox_msg">
                <div class="inbox_people">
                  <div class="headind_srch">                    
                    <div class="srch_bar w-100">
                      <div class="stylish-input-group">
                        <input type="text" class="search-bar"  placeholder="Search" >
                        <span class="input-group-addon">
                        <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                        </span> </div>
                    </div>
                  </div>
                  <div class="inbox_chat">
                  
                  @if($enquiry_automobile != null)
                    @foreach ($enquiry_automobile as $data)
                    <div class="chat_list ">
                      <div class="chat_people">
                        <div class="chat_img"> <img src="{{'uploads/'.$automobile_images->firstWhere('listing_id','=', $data->productId)->filename}}" alt=""> </div>
                        <div class="chat_ib">
                          <div class="d-flex justify-content-between">
                          <input type="hidden" name="category" id="category" value="Automobile">
                          <input type="hidden" name="enquiryId" id="enquiryId" value="{{$data->id}}">
                          <input type="hidden" name="queryType" id="queryType" value="enquiry">
                          
                            <div class="dealerTitle">
                            <img src="{{'uploads/'.$data->countryflag}}" width="15px" height="15px">
                            @if($data->company_name != null)
                            {{$data->company_name}}
                            @else
                            {{$data->agent_name}}
                            @endif
                          </div>
                            <span class="message-date">{{date('m-d-yy', strtotime($data->created_at))}}</span>
                          </div>
                          <h5 class="mt-2">{{$data->ad_title}}<span><i class="fa fa-check-circle mr-1 text-success "></i>{{currency()->convert(floatval($data->value), 'USD', currency()->getUserCurrency())}}</span></h5>
                          <h6></h6>
                        </div>
                      </div>
                    </div>
                    @endforeach
                    @endif
                   
                    @if($enquiry_watch != null)
                    @foreach ($enquiry_watch as $data)
                    <div class="chat_list ">
                      <div class="chat_people">
                        <div class="chat_img"> <img src="{{'uploads/'.$watch_images->firstWhere('listing_id','=', $data->productId)->filename}}" alt=""> </div>
                        <div class="chat_ib">
                          <div class="d-flex justify-content-between">
                          <input type="hidden" name="category" id="category" value="Watch">
                          <input type="hidden" name="enquiryId" id="enquiryId" value="{{$data->id}}">
                          <input type="hidden" name="queryType" id="queryType" value="enquiry">
                            <div class="dealerTitle">
                            <img src="{{'uploads/'.$data->watchDetail->country->filename}}" width="15px" height="15px">
                            <?php /* ?>
                            @if(isset($data->agentTable->agency['company_name']) ? count(array($data->agentTable->agency['company_name'])) : 0)
                            {{$data->agentTable->agency['company_name']}}
                            @else
                            {{$data->agentTable->first_name}} {{$data->agentTable->last_name}}
                            @endif

                               <?php */ ?>

                            @if(isset($data->createBy->first_name) ? count(array($data->createBy->first_name)) : 0)
                            {{$data->createBy->first_name}} {{$data->createBy->last_name}}
                            @endif


                          </div>
                            <span class="message-date">{{date('m-d-yy', strtotime($data->created_at))}}</span>
                          </div>
                          <h5 class="mt-2">{{$data->ad_title}}<span><i class="fa fa-check-circle mr-1 text-success "></i>{{currency()->convert(floatval($data->value), 'USD', currency()->getUserCurrency())}}</span></h5>
                          <h6></h6>
                        </div>
                      </div>
                    </div>
                    @endforeach
                    @endif
                    @if($enquiry_property != null)
                    @foreach ($enquiry_property as $data)
                    <div class="chat_list">
                      <div class="chat_people">
                        <div class="chat_img"> <img src="{{'uploads/'.$property_images->firstWhere('listing_id','=', $data->productId)->filename}}" alt=""> </div>
                        <div class="chat_ib">
                          <div class="d-flex justify-content-between">
                          <input type="hidden" name="category" id="category" value="RealEstate">
                          <input type="hidden" name="enquiryId" id="enquiryId" value="{{$data->id}}">
                          <input type="hidden" name="queryType" id="queryType" value="enquiry">
                            <div class="dealerTitle">
                            <img src="{{'uploads/'.$data->countryflag}}" width="15px" height="15px">
                            @if($data->company_name != null)
                            {{$data->company_name}}
                            @else
                            {{$data->agent_name}}
                            @endif
                          </div>
                            <span class="message-date">{{date('m-d-yy', strtotime($data->created_at))}}</span>
                          </div>
                          <h5 class="mt-2">{{$data->ad_title}}<span><i class="fa fa-check-circle mr-1 text-success "></i>{{currency()->convert(floatval($data->value), 'USD', currency()->getUserCurrency())}}</span></h5>
                          <h6></h6>
                        </div>
                      </div>
                    </div>
                    @endforeach
                    @endif
                    <!-- <div class="chat_list">
                      <div class="chat_people">
                        <div class="chat_img"> <img src="images/watch/watch1.jpg" alt="sunil"> </div>
                        <div class="chat_ib">
                          <div class="d-flex justify-content-between">
                            <div class="dealerTitle">
                            <img src="https://image.flaticon.com/icons/svg/197/197374.svg" width="15px" height="15px">
                            Tangible barcelona
                          </div>
                            <span class="message-date">14.11.20205</span>
                          </div>
                          <h5 class="mt-2">Rolex GMT Master 2 <span><i class="fa fa-check-circle mr-1 text-success "></i>$20,000</span></h5>
                          <h6></h6>
                        </div>
                      </div>
                    </div> -->
                    <!-- <div class="chat_list active_chat">
                      <div class="chat_people">
                        <div class="chat_img"> <img src="images/watch/watch1.jpg" alt="sunil"> </div>
                        <div class="chat_ib">
                          <div class="d-flex justify-content-between">
                            <div class="dealerTitle">
                            <img src="https://image.flaticon.com/icons/svg/197/197374.svg" width="15px" height="15px">
                            Tangible barcelona
                          </div>
                            <span class="message-date">14.11.20205</span>
                          </div>
                          <h5 class="mt-2">Rolex GMT Master 2 <span><i class="fa fa-check-circle mr-1 text-success "></i>$20,000</span></h5>
                          <h6></h6>
                        </div>
                      </div>
                    </div> -->
                    <!-- <div class="chat_list active_chat">
                      <div class="chat_people">
                        <div class="chat_img"> <img src="images/watch/watch1.jpg" alt="sunil"> </div>
                        <div class="chat_ib">
                          <div class="d-flex justify-content-between">
                            <div class="dealerTitle">
                            <img src="https://image.flaticon.com/icons/svg/197/197374.svg" width="15px" height="15px">
                            Tangible barcelona
                          </div>
                            <span class="message-date">14.11.20205</span>
                          </div>
                          <h5 class="mt-2">Rolex GMT Master 2 <span><i class="fa fa-check-circle mr-1 text-success "></i>$20,000</span></h5>
                          <h6></h6>
                        </div>
                      </div>
                    </div> -->
                  
                  </div>
                </div>
                <div class="noChat">
                  <div class="inner">
                    <div class="noChatImg mt-5">
                      <img src="/assets/img/no-chat.png" alt="">
                    </div>
                    <div class="noChatMessage">
                      <h2 class="text-center">No Chat selected</h2>
                      <p class="text-center">select chat from left to see the messages.</p>
                    </div>
                  </div>
                </div>
                
                <div class="mesgs d-none">
                  <div class="productDetails" id="productDetails">
                  
                    <div class="dealerName">
                      <!-- <div class="dealerimg mr-1">
                        <img src="https://image.flaticon.com/icons/svg/197/197374.svg" width="40px" height="40px" alt="">
                      </div> -->
                      <div class="delerTitle">
                        <p class="mb-0">Tangible Barcelona</p>
                        <p>Trusted Seller since 2019, 197 trusted checkout</p>
                      </div>
                    </div>
                    </div>
                    <div class="product" id="product">
                      <!-- <div><img src="images/watch/watch1.jpg" width="40px" height="40px"> </div> -->
                      <div class="pl-2">
                        <p class="mb-0"> <strong>GMT2 | Pepsi Jublie 1260BLRO, New Unborn October 2</strong> </p>
                        <p>TC - 2876839</p>
                      </div>
                      <div class="pl-2">
                        <h5 id="productPrice"></h5>
                      </div>
                    </div>
                  <div class="msg_history" id="msgdata">
                   <div class="outgoing_msg">
                      <div class="sent_msg">
                        <div class="yourOffer">
                          <table class="w-100">
                            <tr><td colspan="2">You have ordered the following item:</td></tr>
                            <tr><td>Item Price</td><td>AU$ - 20000</td></tr>
                            <tr><td>Shipping Cost</td><td> 00.00</td></tr>
                            <tr><td>Total Price</td><td>AU$ - 20000</td></tr>
                          </table>
                        </div>                        
                        <span class="time_date"> 11:01 AM    |    June 9</span> </div>
                    </div>                
                   
                    <div class="incoming_msg">
                      <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                      <div class="received_msg">
                        <div class="received_withd_msg">
                          <p>We work directly with our designers and suppliers,
                            and sell direct to you, which means quality, exclusive
                            products, at a price anyone can afford.</p>
                          <span class="time_date"> 11:01 AM    |    Today</span></div>
                      </div>
                    </div>
                  </div>
                  <div class="type_msg" id="bottom_line">                           
                  <div class="input_msg_write">
                        <input type="text" class="write_msg" placeholder="Type a message"/>
                        <button class="msg_send_btn" type="button" id="message"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                      </div>
                    
                  </div>
                </div>
              </div>              
            </div>
          </div>
 
    <div class="support-box">
        <a href="#" class="icon-text hvr-float-shadow" id="icon-anchor">
            <p><i class="fa fa-envelope-o" aria-hidden="true"></i> Ask Us If You Any Queries</p>
        </a>
    </div>
    @include('tangiblehtml.innerfooter')


    <script src="/assets/js/jquery.js"></script>
    <script>
      $(document).ready(function(){
        
    	  // $('#message').on('click',function(){
      $(document).on('click','#message',function(){
        var message = $('.write_msg').val();
        var category = $('#categoryMessage').val();

 
				
				var queryType = $('#queryTypeMessage').val();
				var enquiryId = $('#enquiryIdMessage').val();
				var productId = $('#productIdMessage').val();
				var agentUserId = $('#agentUserIdMessage').val();
        var listingCreatedById = $('#listingCreatedByIdMessage').val();
				var userId = $('#userIdMessage').val();
				var agentId = $('#agentIdMessage').val();



				$.ajax({
		  			url: "saveMessages",
		  			type: "POST",
		  			data: {
              enquiryId: enquiryId, 
              category: category,
              queryType: queryType,
              productId: productId,
              agentUserId:agentUserId,
              listingCreatedById:listingCreatedById,
              userId:userId,
		  				agentId: agentId, 
              message: message,
              _token: '{{csrf_token()}}' },
		  			  dataType: 'json',
		  			success : function(data){
						console.log('Data ',data);

		  				var message = data.message;
		  				var images = data.images;

		  				console.log('message', message);

		  				var d = new Date(message[0].agencycreate).getFullYear();
		  				
						var productHtml = `
							<input type="hidden" name="category" id="categoryMessage" value="${message[0].category}">
							<input type="hidden" name="queryType" id="queryTypeMessage" value="${queryType}">
		                    <input type="hidden" name="enquiryId" id="enquiryIdMessage" value="${message[0].enquiryId}">
		                    <input type="hidden" name="productId" id="productIdMessage" value="${message[0].productId}">
		                    <input type="hidden" name="agentUserId" id="agentUserIdMessage" value="${message[0].agentUserId}">
                        <input type="hidden" name="listingCreatedById" id="listingCreatedByIdMessage" value="${message[0].listingCreatedById}">
		                    <input type="hidden" name="userId" id="userIdMessage" value="${data.userId}">
		                    <input type="hidden" name="agentId" id="agentIdMessage" value="${message[0].agent_id}">
							<div class="dealerName">
		                <div class="dealerimg mr-1">
		                <img src="uploads/${message[0].countryflag}" width="40px" height="40px" alt="">
		              </div>
		              <div class="delerTitle">
		                <p class="mb-0">${message[0].company_name}</p>
		                <p>Trusted Seller since ${d}</p>
		              </div>
		            </div>`;

		            var product= ``;
		            for(var i=0; i<images.length; i++){
		                
		                if(message[0].productId == images[i].listing_id ){
					product = `<div><img src="uploads/${images[i].filename}" width="40px" height="40px"> </div>
		            <div class="pl-2">
		            <p class="mb-0"> <strong>${message[0].ad_title}</strong></p>
		            <p></p>
		          		</div>
		          	<div class="pl-2">
		          		<h5>${data.productPrice}</h5>
		          		
		          	</div>`;
		                }
		            }

		           
		            
		             var outgoingHtml =``;
		            if(data.outgoingEnquiryMessage != null){
		                for(var i=0; i < data.outgoingEnquiryMessage.length; i++){
		                    console.log('outgoing', data.outgoingEnquiryMessage[i]);
		                if(data.outgoingEnquiryMessage[i].messagefrom == data.loggedInUser){
		                	if(data.outgoingEnquiryMessage[i].queryType == 'enquiry'){    
						outgoingHtml += `<div class="outgoing_msg" >
							<div class="sent_msg">
							<div class="yourOffer">
		                    <p>${data.outgoingEnquiryMessage[i].message}</p></div>
		                  <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span></div></div>`;}
		                	else if(data.outgoingEnquiryMessage[i].queryType == 'submitprice'){
		                		outgoingHtml +=	`<div class="outgoing_msg">
		                      <div class="sent_msg">
		                        <div class="yourOffer">
		                          <table class="w-100">
		                            <tr><td colspan="2">You have ordered the following item:</td></tr>
		                            <tr><td>Item Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
		                            <tr><td>Shipping Cost</td><td> 00.00</td></tr>
		                            <tr><td>Total Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
		                          </table>
		                        </div>                        
		                        <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span> </div>
		                    </div>`;
		                		
		                    	}
		                }else{
		                	if(data.outgoingEnquiryMessage[i].queryType == 'enquiry'){
		                	outgoingHtml += `<div class="incoming_msg">
		                    	<div class="received_msg">
		                        <div class="received_withd_msg">
		                          <p>${data.outgoingEnquiryMessage[i].message}</p>
		                          <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span></div>
		                      </div>
		                      </div>`;
		                    } else if(data.outgoingEnquiryMessage[i].queryType == 'submitprice'){
		                    	outgoingHtml +=	`<div class="incoming_msg">
		                            <div class="received_msg">
		                              <div class="received_withd_msg">
		                                <table class="w-100">
		                                  <tr><td colspan="2">You have submit price request for the following item:</td></tr>
		                                  <tr><td>Item Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
		                                  <tr><td>Shipping Cost</td><td> 00.00</td></tr>
		                                  <tr><td>Total Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
		                                </table>
		                              </div>                        
		                              <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span> </div>
		                          </div>`;
		                        }
		                    }
		                }
		                }
		            
					console.log(outgoingHtml);
					$('.write_msg').val("");
		            $('#productDetails').html(productHtml);
		            $('#product').html(product);
		            $('#msgdata').html(outgoingHtml);
						
			  			}
				});
				
      });

       $('.chat_list').on('click',function(){

          //alert(JSON.stringify(this));
          
           var category = $('#category',this).val();
           var enquiryId = $('#enquiryId',this).val();
           var queryType = $('#queryType',this).val();

           console.log(queryType);

           // No use of this
			     $('#enquiryIdHidden').html('enquiryId');
			     $('#queryTypeHidden').val(queryType); 

           // Removing Active class from all chat div
           $('.chat_list').removeClass('active_chat');

           // Adding Active class on selected chat div
           $(this).addClass('active_chat');

          if($('.chat_list').hasClass('active_chat')){
          $('.noChat').addClass('d-none');
          if(category == 'Automobile'){
          $('.mesgs').removeClass('d-none');

          $.ajax({
  			url: "getMessages",
  			type: "POST",
  			data: {enquiryId: enquiryId,queryType:queryType ,category: 'Automobile' ,_token: '{{csrf_token()}}' },
  			dataType: 'json',
  			success : function(data){
				console.log('data', data);
  				var message = data.message;
  				var images = data.images;

  				console.log('message', message);

  				var d = new Date(message[0].agencycreate).getFullYear();
  				
				var productHtml = `
					<input type="hidden" name="category" id="categoryMessage" value="${message[0].category}">
					<input type="hidden" name="queryType" id="queryTypeMessage" value="${queryType}">
                    <input type="hidden" name="enquiryId" id="enquiryIdMessage" value="${message[0].enquiryId}">
                    <input type="hidden" name="productId" id="productIdMessage" value="${message[0].productId}">
                    <input type="hidden" name="agentUserId" id="agentUserIdMessage" value="${message[0].agentUserId}">
                    <input type="hidden" name="userId" id="userIdMessage" value="${data.userId}">
                    <input type="hidden" name="agentId" id="agentIdMessage" value="${message[0].agent_id}">
					<div class="dealerName">
                <div class="dealerimg mr-1">
                <img src="uploads/${message[0].countryflag}" width="40px" height="40px" alt="">
              </div>
              <div class="delerTitle">
                <p class="mb-0">${message[0].company_name}</p>
                <p>Trusted Seller since ${d}</p>
              </div>
            </div>`;

            var product= ``;
            for(var i=0; i<images.length; i++){
                
                if(message[0].productId == images[i].listing_id ){
			product = `<div><img src="uploads/${images[i].filename}" width="40px" height="40px"> </div>
            <div class="pl-2">
            <p class="mb-0"> <strong>${message[0].ad_title}</strong></p>
            <p></p>
          		</div>
          	<div class="pl-2">
          		<h5>${data.productPrice}</h5>
          		
          	</div>`;
                }
            }

           
            
             var outgoingHtml =``;
            if(data.outgoingEnquiryMessage != null){
                for(var i=0; i < data.outgoingEnquiryMessage.length; i++){
                    console.log('outgoing', data.outgoingEnquiryMessage[i]);
                if(data.outgoingEnquiryMessage[i].messagefrom == data.loggedInUser){
                	if(data.outgoingEnquiryMessage[i].queryType == 'enquiry'){    
				outgoingHtml += `<div class="outgoing_msg" >
					<div class="sent_msg">
					<div class="yourOffer">
                    <p>${data.outgoingEnquiryMessage[i].message}</p></div>
                  <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span></div></div>`;}
                	else if(data.outgoingEnquiryMessage[i].queryType == 'submitprice'){
                		outgoingHtml +=	`<div class="outgoing_msg">
                      <div class="sent_msg">
                        <div class="yourOffer">
                          <table class="w-100">
                            <tr><td colspan="2">You have ordered the following item:</td></tr>
                            <tr><td>Item Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                            <tr><td>Shipping Cost</td><td> 00.00</td></tr>
                            <tr><td>Total Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                          </table>
                        </div>                        
                        <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span> </div>
                    </div>`;
                		
                    	}
                }else{
                	if(data.outgoingEnquiryMessage[i].queryType == 'enquiry'){
                	outgoingHtml += `<div class="incoming_msg">
                    	<div class="received_msg">
                        <div class="received_withd_msg">
                          <p>${data.outgoingEnquiryMessage[i].message}</p>
                          <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span></div>
                      </div>
                      </div>`;
                    } else if(data.outgoingEnquiryMessage[i].queryType == 'submitprice'){
                    	outgoingHtml +=	`<div class="incoming_msg">
                            <div class="received_msg">
                              <div class="received_withd_msg">
                                <table class="w-100">
                                  <tr><td colspan="2">You have submit price request for the following item:</td></tr>
                                  <tr><td>Item Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                                  <tr><td>Shipping Cost</td><td> 00.00</td></tr>
                                  <tr><td>Total Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                                </table>
                              </div>                        
                              <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span> </div>
                          </div>`;
                        }
                    }

			
                var bottom_line = `<div class="input_msg_write">
                    <input type="text" class="write_msg" placeholder="Type a message"/>
                    <button class="msg_send_btn" type="button" id="message"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                  </div>
                  <div class="takeAction">
                    <div class="addToWatch w-50">
                      <button class="btn btn-outline-primary w-100" value="">Add to Auto collection</button>
                    </div>
                    <div class="viewOrder w-50">
                    <form id="acceptoffer" method="POST" action="{{ url('/buyer/checkout') }}">
						@csrf
						<input type="hidden" name="enquiryId" value=${enquiryId} />
						<input type="hidden" name="queryType" value=${queryType} />
						<input type="hidden" name="category" value=${category} />
						<input type="hidden" name="messageId" value=${data.outgoingEnquiryMessage[i].messageId} />
                      <button class="btn btn-success w-100" value="">View Order</button>
                      </form>
                    </div>
                    
                  </div> `;
                }
                }


			console.log(outgoingHtml);
            $('#productDetails').html(productHtml);
            $('#product').html(product);
            $('#msgdata').html(outgoingHtml);
            /* $('#bottom_line').html(bottom_line); */
	  		 		
  				}
  			}); 
          
          }else if(category == 'Watch'){
        	  $('.mesgs').removeClass('d-none');
				    console.log("Inside watch");
        	  $.ajax({
        			url: "getMessages",
        			type: "POST",
        			data: {enquiryId: enquiryId,queryType:queryType ,category: 'Watch' ,_token: '{{csrf_token()}}' },
        			dataType: 'json',
        			success : function(data){
      				console.log('success', data);
        				var message = data.message;
        				var images = data.images;

                		

        			var d = new Date(message[0].agencycreate).getFullYear();

        				
      				var productHtml = `
      					<input type="hidden" name="category" id="categoryMessage" value="${message[0].category}">
      					<input type="hidden" name="queryType" id="queryTypeMessage" value="${queryType}">
                          <input type="hidden" name="enquiryId" id="enquiryIdMessage" value="${message[0].enquiryId}">
                          <input type="hidden" name="productId" id="productIdMessage" value="${message[0].productId}">
                          <input type="hidden" name="agentUserId" id="agentUserIdMessage" value="${message[0].agentUserId}">
                          <input type="hidden" name="userId" id="userIdMessage" value="${message[0].userId}">
                          <input type="hidden" name="agentId" id="agentIdMessage" value="${message[0].agent_id}">
                          <input type="hidden" name="listingCreatedById" id="listingCreatedByIdMessage" value="${message[0].listingCreatedBy}">

                          <div class="dealerName">
                      <div class="dealerimg mr-1">
                      <img src="uploads/${message[0].countryflag}" width="40px" height="40px" alt="">
                    </div>
                    <div class="delerTitle">
                      <p class="mb-0">${message[0].company_name}</p>
                      <p>Trusted Seller since ${d}</p>
                    </div>
                  </div>`;

                  var product= ``;
                  for(var i=0; i<images.length; i++){
                      
                      if(message[0].productId == images[i].listing_id ){
      			product = `<div><img src="uploads/${images[i].filename}" width="40px" height="40px"> </div>
                  <div class="pl-2">
                  <p class="mb-0"> <strong>${message[0].ad_title}</strong></p>
                  <p></p>
                		</div>
                	<div class="pl-2">
                		<h5>${data.productPrice}</h5>
                		
                	</div>`;
                      }
                  }

                 
                  
                   var outgoingHtml =``;
                  if(data.outgoingEnquiryMessage != null){
                      console.log("inside first if");
                      for(var i=0; i <data.outgoingEnquiryMessage.length; i++){
                          
                      if(data.outgoingEnquiryMessage[i].messagefrom == data.loggedInUser){
                              
                          if(data.outgoingEnquiryMessage[i].queryType == 'enquiry' ){
      				outgoingHtml += `<div class="outgoing_msg" >
      					<div class="sent_msg">
      					<div class="yourOffer">
                          <p>${data.outgoingEnquiryMessage[i].message}</p></div>
                        <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span></div></div>`;
                          }else if(data.outgoingEnquiryMessage[i].queryType == 'submitprice'){
                        	  outgoingHtml +=	`<div class="outgoing_msg">
                                  <div class="sent_msg">
                                    <div class="yourOffer">
                                      <table class="w-100">
                                        <tr><td colspan="2">You have ordered the following item:</td></tr>
                                        <tr><td>Item Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                                        <tr><td>Shipping Cost</td><td> 0.00</td></tr>
                                        <tr><td>Total Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                                      </table>
                                    </div>                        
                                    <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span> </div>
                                </div>`;
                              }else if(data.outgoingEnquiryMessage[i].queryType == 'counterOffer'){
                            	  outgoingHtml +=	`<div class="outgoing_msg">
                                      <div class="sent_msg">
                                        <div class="yourOffer">
                                          <table class="w-100">
                                            <tr><td colspan="2">You have ordered the following item:</td></tr>
                                            <tr><td>Item Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                                            <tr><td>Shipping Cost</td><td> ${data.outgoingEnquiryMessage[i].shippingCost}</td></tr>
                                            <tr><td>Total Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                                          </table>
                                        </div>                        
                                        <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span> </div>
                                    </div>`;
                                  }
                      }else{
                          console.log("inside else ------- ",data.outgoingEnquiryMessage[i].queryType);
                    	  if(data.outgoingEnquiryMessage[i].queryType == 'enquiry'){
                      	outgoingHtml += `<div class="incoming_msg">
                          	<div class="received_msg">
                              <div class="received_withd_msg">
                                <p>${data.outgoingEnquiryMessage[i].message}</p>
                                <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span></div>
                            </div>
                            </div>`;
                          }else if(data.outgoingEnquiryMessage[i].queryType == 'submitprice'){
                        	  $('#productDetails').html(productHtml);
                        	  outgoingHtml +=	`<div class="outgoing_msg">
                                  <div class="received_msg">
                                    <div class="received_withd_msg">
                                      <table class="w-100">
                                        <tr><td colspan="2">You have ordered the following item:</td></tr>
                                        <tr><td>Item Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                                        <tr><td>Shipping Cost</td><td> 0.00</td></tr>
                                        <tr><td>Total Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                                      </table>
                                    </div>                        
                                    <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span> </div>
									<form id="acceptoffer" method="POST" action="{{ url('/seller/acceptoffer') }}">
									@csrf
									<input type="hidden" name="enquiryId" value=${enquiryId} />
									<input type="hidden" name="queryType" value= 'submitprice' />
									<input type="hidden" name="category" value=${category} />
									<input type="hidden" name="messageId" value=${data.outgoingEnquiryMessage[i].messageId} />
                                    <div class="viewOrder w-50">
                                    <button class="btn btn-success w-100"  type="submit" value="">Accept Offer</button>
                                  </div>
                                  </form>
                                </div>`;
                              }else if(data.outgoingEnquiryMessage[i].queryType == 'counterOffer' && data.outgoingEnquiryMessage[i].messageFinalFrom == 'seller'){
                            	  $('#productDetails').html(productHtml);
                            	  outgoingHtml +=	`<div class="outgoing_msg">
                                      <div class="received_msg">
                                        <div class="received_withd_msg">
                                          <table class="w-100">
                                            <tr><td colspan="2">You have ordered the following item:</td></tr>
                                            <tr><td>Item Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                                            <tr><td>Shipping Cost</td><td> ${data.outgoingEnquiryMessage[i].shippingCost}</td></tr>
                                            <tr><td>Total Price</td><td>${data.outgoingEnquiryMessage[i].totalPrice}</td></tr>
                                          </table>
                                        </div>                        
                                        <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span> </div>
    									<form id="acceptoffer" method="POST" action="{{ url('/buyer/checkout') }}">
    									@csrf
    									<input type="hidden" name="enquiryId" value=${enquiryId} />
    									<input type="hidden" name="queryType" value='counterOffer' />
    									<input type="hidden" name="category" value=${category} />
    									<input type="hidden" name="messageId" value=${data.outgoingEnquiryMessage[i].messageId} />
                                        <div class="viewOrder w-50">
                                        <button class="btn btn-success w-100"  type="submit" value="">Accept Offer</button>
                                      </div>
                                      </form>
                                    </div>`;
                                  }else if(data.outgoingEnquiryMessage[i].queryType == 'counterOffer' && data.outgoingEnquiryMessage[i].messageFinalFrom == 'buyer'){
										console.log('------------------------------------',data.outgoingEnquiryMessage[i].shippingCost);
                                 	  $('#productDetails').html(productHtml);
                                	  outgoingHtml +=	`<div class="outgoing_msg">
                                          <div class="received_msg">
                                            <div class="received_withd_msg">
                                              <table class="w-100">
                                                <tr><td colspan="2">You have ordered the following item:</td></tr>
                                                <tr><td>Item Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                                                <tr><td>Shipping Cost</td><td> ${data.outgoingEnquiryMessage[i].shippingCost}</td></tr>
                                                <tr><td>Total Price</td><td>${data.outgoingEnquiryMessage[i].totalPrice}</td></tr>
                                              </table>
                                            </div>                        
                                            <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span> </div>
        									<form id="acceptoffer" method="POST" action="{{ url('/seller/acceptoffer') }}">
        									@csrf
        									<input type="hidden" name="enquiryId" value=${enquiryId} />
        									<input type="hidden" name="queryType" value='counterOffer' />
        									<input type="hidden" name="category" value=${category} />
        									<input type="hidden" name="messageId" value=${data.outgoingEnquiryMessage[i].messageId} />
                                            <div class="viewOrder w-50">
                                            <button class="btn btn-success w-100"  type="submit" value="">Accept Offer</button>
                                          </div>
                                          </form>
                                        </div>`;
                                      }
                    	  
                          }
                      if(message[0].agentUserId == data.loggedInUser){

                    	  var bottom_line = `<div class="input_msg_write">
                              <input type="text" class="write_msg" placeholder="Type a message"/>
                              <button class="msg_send_btn" type="button" id="message"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                            </div>
                            <div class="takeAction">
                              <div class="addToWatch w-50">
                                <button class="btn btn-outline-primary w-100" value="">Add to Auto collection</button>
                              </div>
                              <div class="viewOrder w-50">
                              <form id="acceptoffer" method="POST" action="{{ url('/seller/acceptoffer') }}">
    							@csrf
    							<input type="hidden" name="enquiryId" value=${enquiryId} />
    							<input type="hidden" name="queryType" value=${queryType} />
    							<input type="hidden" name="category" value=${category} />
    							<input type="hidden" name="messageId" value=${data.outgoingEnquiryMessage[i].messageId} />
                                <button class="btn btn-success w-100" value="">View Order</button>
                                </form>
                              </div>
                              
                            </div>`;
    						
                          }else{
                        	  var bottom_line = `<div class="input_msg_write">
                                  <input type="text" class="write_msg" placeholder="Type a message"/>
                                  <button class="msg_send_btn" type="button" id="message"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                                </div>
                                <div class="takeAction">
                                  <div class="addToWatch w-50">
                                    <button class="btn btn-outline-primary w-100" value="">Add to Auto collection</button>
                                  </div>
                                  <div class="viewOrder w-50">
                                  <form id="acceptoffer" method="POST" action="{{ url('/buyer/checkout') }}">
        							@csrf
        							<input type="hidden" name="enquiryId" value=${enquiryId} />
        							<input type="hidden" name="queryType" value=${queryType} />
        							<input type="hidden" name="category" value=${category} />
        							<input type="hidden" name="messageId" value=${data.outgoingEnquiryMessage[i].messageId} />
                                    <button class="btn btn-success w-100" value="">View Order</button>
                                    </form>
                                  </div>
                                  
                                </div>`;
                              }
                      }
                      
                      }

				    console.log("----------------");
      			console.log(bottom_line);
                  $('#productDetails').html(productHtml);
                  $('#product').html(product);
                  $('#msgdata').html(outgoingHtml);
                  $('#bottom_line').html(bottom_line);
      	  		 		
        				}
        			});
          }else if(category == 'RealEstate'){
        	  $('.mesgs').removeClass('d-none');

        	  $.ajax({
      			url: "getMessages",
      			type: "POST",
      			data: {enquiryId: enquiryId,queryType:queryType ,category: 'RealEstate' ,_token: '{{csrf_token()}}' },
      			dataType: 'json',
      			success : function(data){
    				console.log('data', data);
      				var message = data.message;
      				var images = data.images;

      				console.log('message', message);

      				var d = new Date(message[0].agencycreate).getFullYear();
      				
    				var productHtml = `
    					<input type="hidden" name="category" id="categoryMessage" value="${message[0].category}">
    					<input type="hidden" name="queryType" id="queryTypeMessage" value="${queryType}">
                        <input type="hidden" name="enquiryId" id="enquiryIdMessage" value="${message[0].enquiryId}">
                        <input type="hidden" name="productId" id="productIdMessage" value="${message[0].productId}">
                        <input type="hidden" name="agentUserId" id="agentUserIdMessage" value="${message[0].agentUserId}">
                        <input type="hidden" name="userId" id="userIdMessage" value="${message[0].userId}">
                        <input type="hidden" name="agentId" id="agentIdMessage" value="${message[0].agent_id}">
                        <input type="hidden" name="messageId" id="messageId" value="${message[0].messageId}">
    					<div class="dealerName">
                    <div class="dealerimg mr-1">
                    <img src="uploads/${message[0].countryflag}" width="40px" height="40px" alt="">
                  </div>
                  <div class="delerTitle">
                    <p class="mb-0">${message[0].company_name}</p>
                    <p>Trusted Seller since ${d}</p>
                  </div>
                </div>`;

                var product= ``;
                for(var i=0; i<images.length; i++){
                    
                    if(message[0].productId == images[i].listing_id ){
    			product = `<div><img src="uploads/${images[i].filename}" width="40px" height="40px"> </div>
                <div class="pl-2">
                <p class="mb-0"> <strong>${message[0].ad_title}</strong></p>
                <p></p>
              		</div>
              	<div class="pl-2">
              		<h5>${data.productPrice}</h5>
              		
              	</div>`;
                    }
                }

               
                
                 var outgoingHtml =``;
                if(data.outgoingEnquiryMessage != null){
                    for(var i=0; i <data.outgoingEnquiryMessage.length; i++){
                    if(data.outgoingEnquiryMessage[i].messagefrom == data.loggedInUser){    
    				outgoingHtml += `<div class="outgoing_msg" >
    					<div class="sent_msg">
    					<div class="yourOffer">
                        <p>${data.outgoingEnquiryMessage[i].message}</p></div>
                      <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span></div></div>`;
                    }else{
                    	outgoingHtml += `<div class="incoming_msg">
                        	<div class="received_msg">
                            <div class="received_withd_msg">
                              <p>${data.outgoingEnquiryMessage[i].message}</p>
                              <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span></div>
                          </div>
                          </div>`;
                        }
                    var bottom_line = `<div class="input_msg_write">
                        <input type="text" class="write_msg" placeholder="Type a message"/>
                        <button class="msg_send_btn" type="button" id="message"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                      </div>
                       <div class="takeAction">
                        <div class="addToWatch w-50">
                          <button class="btn btn-outline-primary w-100" value="">Add to Auto collection</button>
                        </div>
                        <div class="viewOrder w-50">
                        <form id="acceptoffer" method="POST" action="{{ url('/buyer/checkout') }}">
							@csrf
							<input type="hidden" name="enquiryId" value=${enquiryId} />
							<input type="hidden" name="queryType" value=${queryType} />
							<input type="hidden" name="category" value=${category} />
							<input type="hidden" name="messageId" value=${data.outgoingEnquiryMessage[i].messageId} />
                          <button class="btn btn-success w-100" value="">View Order</button>
                          </form>
                        </div>
                        
                      </div> `;
                    
                    }
                    }


    			      console.log(outgoingHtml);
                $('#productDetails').html(productHtml);
                $('#product').html(product);
                $('#msgdata').html(outgoingHtml);
                /* $('#bottom_line').html(bottom_line); */ 		
      				}
      			});
          }
        }
       });
      });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/intlTelInput.min.js"></script>
    <script src="/assets/js/local.js"></script>
  
</body>

</html>