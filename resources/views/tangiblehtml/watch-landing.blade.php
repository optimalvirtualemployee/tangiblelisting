<!DOCTYPE html>
<html lang="en">

@include('tangiblehtml.headheader')

<style>
    .article-para {
        text-overflow: ellipsis;
        overflow: hidden;
        white-space: nowrap;
    }
</style>

<body class="watch-lading">
    @include('tangiblehtml.innerheader')

    <!--banner area-->
    <div class="custom-landing-banner-wrapper">
        <div class="custom-landing-banner-slider">

            <div class="landing-banner-item">
                <div class="container">
                    <div class="caption">
                        <h3>Find Your Dream Watch</h3>
                        <!-- <a href="#" class="read-more fill-read-more"> Explore More</a> -->
                    </div>
                    <div class="col p-0">
                        <div class="banner_filter slide-2">
                            <div class="banner___from">
                                <div class="row search-headtext align-items-center">
                                    <div class="col-sm-8 col-xs-12">
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <a href="#" class="reset-search" id="clear"><i class="fa fa-undo" aria-hidden="true"></i>
                                            Reset Search</a>
                                    </div>
                                </div>
                                <form id="searchForm" method="GET" action="{{url('/watchlisting/search')}}" class="form-inline">
                                    <div class="form-group">
                                        <select name="brand" id="brand" class="form-control ro-select" placeholder="All Makes">
                                            <option value="" disabled selected>All Brands</option>
                                            @foreach($makes as $make)
                                            <option value="{{$make->id}}">{{$make->watch_brand_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select name="model" id="model" class="form-control ro-select" placeholder="All Models">
                                            <option value="" disabled selected>All Models</option>
                                            @foreach($models as $model)
                                            <option value="{{$model->id}}">{{$model->watch_model_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select name="fromYear" id="year_from" class="form-control ro-select" placeholder="Year From">
                                            <option value="" disabled selected>From year</option>
                                            @foreach($from_year as $year)
                                            <option value="{{$year->year_of_manufacture}}">{{$year->year_of_manufacture}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group pr-0">
                                        <select name="toYear" id="year_to" class="form-control ro-select" placeholder="Year To">
                                            <option value="" disabled selected>To year</option>
                                            @foreach($to_year as $year)
                                            <option value="{{$year->year_of_manufacture}}">{{$year->year_of_manufacture}}</option>
                                            @endforeach


                                        </select>
                                    </div>

                                    <div class="form-group pos-bottom pr-0">
                                        <input type="submit" class="read-more submit-btn" id="findValue" value="Search">
                                    </div>

                                </form>



                            </div>
                        </div>
                    </div>

                </div>
                <img src="/assets/img/watch-landing/slide_02.jpg" alt="" class="img-fluid">
            </div>


        </div>
    </div>

    <!--banner area end-->

    <div class="custom-latestoffer-wrapper pt-5 pb-5">
        <div class="container">
            <div class="main-heading text-center">
                <h3>latest listings </h3>
            </div>
            <div class="latest-collection-watch">
                @foreach($latest_listing as $watch)
                <div class="watch-list">
                    <div class="watch-product-item">
                        <div class="product-thumb">
                            <a href="{{url('/watchlisting/'.Crypt::encrypt($watch->id))}}"><img src="{{'uploads/'.$listing_images->firstWhere('listing_id','=', $watch->id)->filename}}" alt=""></a>
                            <div class="d-none">
                                <ul>
                                    <li><a href="#"><i class="fas fa-search-plus"></i></a></li>
                                    <li><a href="#"><i class="far fa-heart"></i></a></li>
                                    <li><a href="#"><i class="fas fa-plus"></i></a></li>
                                </ul>
                            </div>
                            <div class="inside">
                                <div class="contents">
                                    <table class="w-100">
                                        <tr>
                                            <td>Model</td>
                                            <td><span>{{$watch->model_name}}</span></td>
                                        </tr>
                                        <tr>
                                            <td>Year</td>
                                            <td><span>{{$watch->year_of_manufacture}}</span></td>
                                        </tr>
                                        <tr>
                                            <td>Make</td>
                                            <td><span>{{$watch->brand_name}}</span></td>
                                        </tr>
                                        <tr>
                                            <td>Case Diameter</td>
                                            <td><span>{{$watch->case_diameter}}</span></td>
                                        </tr>
                                        <tr>
                                            <td>Location</td>
                                            <td><span>{{$watch->city_name}}, {{$watch->country_name}}</span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="product-details">
                            <div class="watchListing d-flex justify-content-between align-items-center">
                                <h5 class="mb-0"><a href="{{url('/watchlisting/'.Crypt::encrypt($watch->id))}}">{{$watch->ad_title}}</a></h5>
                                <span class="price">{{currency()->convert(floatval($watch->watch_price), 'USD', currency()->getUserCurrency())}}</span>

                            </div>
                            <div class="bottom-detais">
                                <div class="dealer">
                                    <ul>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>
                                    <p class="seller-type"><i class="fa fa-check-square-o" aria-hidden="true"></i> {{ $watch->agency_id === 0 ? "Private Seller" : "Dealer" }}</p>
                                </div>
                                <div class="country-flag">
                                    <img src="{{url('uploads/'.$watch->filename)}}" alt="">
                                    <p class="c-code">{{$watch->countrycode}}</p>
                                </div>
                            </div>
                        </div>

                        <a href="javascript:void(0);" title="click here" class="icon"><i class="fa fa-info-circle" aria-hidden="true"></i></a>

                    </div>
                </div>
                @endforeach
            </div>

        </div>
    </div>
    <section class="car_browse_brand gray-arrow pb-5">
        <div class="container">
            <div class="main-heading text-center">
                <h3>Browse by Brands </h3>
            </div>
            <div class="car_browse_slider wow animated fadeInUp" data-wow-delay="0.4s">
                @if($makes != null)
                @foreach($makes_brand as $make)
                <div class="item">
                    <div class="car_c_item">
                        <a href="{{url('/watchlisting/search?make='.$make->id)}}">
                            @if($makes_images->firstWhere('listing_id', '=', $make->id) != null)
                            <img src="{{'uploads/'.$makes_images->firstWhere('listing_id', '=', $make->id)->filename}}" alt="Alt"></a>
                        @else
                        <img src="" alt="Alt"></a>
                        @endif
                        <a href="{{url('/watchlisting/search?make='.$make->id)}}">
                            <h5>{{$make->watch_brand_name}}</h5>
                        </a>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </section>
    <!-- End custom-service-section2 -->

    <section class="stm_cars_on_top  pb-5">
        <div class="container">
            <div class="main-heading text-center">
                <h3>Featured listings</h3>
            </div>
            <div class="latest-premium-watch">
                @foreach($premium_listing as $watch)
                <div class="watch-list">
                    <div class="watch-product-item">
                        <div class="product-thumb">
                            <a href="{{url('/watchlisting/'.Crypt::encrypt($watch->id))}}"><img src="{{'uploads/'.$listing_images->firstWhere('listing_id','=', $watch->id)->filename}}" alt=""></a>
                            <div class="d-none">
                                <ul>
                                    <li><a href="#"><i class="fas fa-search-plus"></i></a></li>
                                    <li><a href="#"><i class="far fa-heart"></i></a></li>
                                    <li><a href="#"><i class="fas fa-plus"></i></a></li>
                                </ul>
                            </div>
                            <div class="inside">
                                <div class="contents">
                                    <table class="w-100">
                                        <tr>
                                            <td>Model</td>
                                            <td><span>{{$watch->model_name}}</span></td>
                                        </tr>
                                        <tr>
                                            <td>Year</td>
                                            <td><span>{{$watch->year_of_manufacture}}</span></td>
                                        </tr>
                                        <tr>
                                            <td>Make</td>
                                            <td><span>{{$watch->brand_name}}</span></td>
                                        </tr>
                                        <tr>
                                            <td>Case Diameter</td>
                                            <td><span>{{$watch->case_diameter}}</span></td>
                                        </tr>
                                        <tr>
                                            <td>Location</td>
                                            <td><span>{{$watch->city_name}}, {{$watch->country_name}}</span></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="product-details">
                            <div class="watchListing d-flex justify-content-between align-items-center">
                                <h5 class="mb-0"><a href="{{url('/watchlisting/'.Crypt::encrypt($watch->id))}}">{{$watch->ad_title}}</a></h5>
                                <span class="price">{{currency()->convert(floatval($watch->watch_price), 'USD', currency()->getUserCurrency())}}</span>

                            </div>
                            <div class="bottom-detais">
                                <div class="dealer">
                                    <ul>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>
                                    <p class="seller-type"><i class="fa fa-check-square-o" aria-hidden="true"></i> {{ $watch->agency_id === 0 ? "Private Seller" : "Dealer" }}</p>
                                </div>
                                <div class="country-flag">
                                    <img src="{{url('uploads/'.$watch->filename)}}" alt="">
                                    <p class="c-code">{{$watch->countrycode}}</p>
                                </div>
                            </div>
                        </div>

                        <a href="javascript:void(0);" title="click here" class="icon"><i class="fa fa-info-circle" aria-hidden="true"></i></a>

                    </div>
                </div>
                @endforeach

            </div>

            <div class="text-center pt-2    ">
                <a href="/watchlisting/search?sortBy=3" class="read-more fill-read-more">Show all premium watches</a>
            </div>
        </div>
    </section>

    <section class="car_browse_area gray-arrow pb-5">
        <div class="container">
            <div class="main-heading text-center">
                <h3> Browse by Type </h3>
            </div>
            <div class="car_browse_slider wow animated fadeInUp" data-wow-delay="0.4s">
                @foreach($watchTypes as $type)
                <div class="item">
                    <div class="car_c_item">
                        @if($type_images->firstWhere('listing_id', '=', $type->id) != null)
                        <img src="{{'uploads/'.$type_images->firstWhere('listing_id', '=', $type->id)->filename}}" alt="Alt"></a>
                        @else
                        <img src="" alt=""></a>
                        @endif
                        <a href="#">
                            <h5>{{$type->watch_type}}<span>{{count($total_listing->where('type_id', $type->id))}}</span></h5>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    </div>

    <!-- <div class="custom-testimonial-wrapper grey-bg">
        <div class="container">
            <div class="main-heading text-center">
                <h3 style="color:#fff;">Our Testimonial</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
            </div>

            <div class="testimonial-slider">
                <div class="testimonial-item">
                    <div class="testimonial-info">
                        <div class="testimonial-detail">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                                a type specimen book. It has survived not</p>
                            <h3>Anne Brady</h3>
                            <h4>Villa Owner</h4>
                        </div>
                    </div>
                </div>
                <div class="testimonial-item">
                    <div class="testimonial-info">
                        <div class="testimonial-detail">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                                a type specimen book. It has survived not</p>
                            <h3>Anne Brady</h3>
                            <h4>Villa Owner</h4>
                        </div>
                    </div>
                </div>
                <div class="testimonial-item">
                    <div class="testimonial-info">
                        <div class="testimonial-detail">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                                a type specimen book. It has survived not</p>
                            <h3>Anne Brady</h3>
                            <h4>Villa Owner</h4>
                        </div>
                    </div>
                </div>
                <div class="testimonial-item">
                    <div class="testimonial-info">
                        <div class="testimonial-detail">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                                a type specimen book. It has survived not</p>
                            <h3>Anne Brady</h3>
                            <h4>Villa Owner</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <div class="our-agent-wrapper   pt-5 pb-5">
        <div class="container">
            <div class="main-heading text-center">
                <h3>Our Dealers</h3>
            </div>
            <div class="row">
                @if($dealer_listing)
                @foreach ($dealer_listing as $data)
                <div class="col-lg-3 col-md-6">
                    <div class="agent-detail-box">
                        <div class="agent-photo">
                            <a href="#">
                                <img src="{{url('uploads/'.$data->filename)}}" class="img-fluid">
                            </a>
                        </div>
                        <div class="agent-details">
                            <h4><a href="#">{{$data->first_name}}</a></h4>
                            <h5>400+</h5>
                            <a href="#" class="read-more d-block text-center">Read more</a>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="home-item">
                    <div class="home-detail">
                        <div class="home-list-box">
                            <img src="{{url('/assets/images/NoImageFound.png')}}" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>

                @endif

            </div>
        </div>
    </div>


    <div class="custom-our-blog  pt-5 pb-5">
        <div class="container">
            <div class="main-heading text-center">
                <h3>Our Articles</h3>
            </div>
            <div class="row">
                @if($watch_blogs != null)
                <div class="col-sm-7">
                    <div class="blog-left">
                        <div class="blog__item">
                            @if($watch_blogs[0]->url != null)
                            <a class="home-view" href="{{url($watch_blogs[0]->url)}}"><img src="{{'uploads/'.$blog_images->firstWhere('listing_id', '=', $watch_blogs[0]->id)->filename}}" alt="" class="img-fluid"></a>
                            @else
                            <a class="home-view" href="{{url('watchblog/'.$watch_blogs[0]->id)}}"><img src="{{'uploads/'.$blog_images->firstWhere('listing_id', '=', $watch_blogs[0]->id)->filename}}" alt="" class="img-fluid"></a>
                            @endif
                            <div class="blog-detail-content">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i> <span>{{date_format($watch_blogs[0]->created_at, 'F d, Y')}}</span></span>
                                    <span><i class="fas fa-tag"></i> <a href="#"> {{$watch_blogs[0]->blogType}}</a></span>
                                </div>
                                @if($watch_blogs[0]->url != null)
                                <h4><a href="{{url($watch_blogs[0]->url)}}">{{$watch_blogs[0]->title}}</a></h4>
                                @else
                                <h4><a href="{{url('watchblog/'.$watch_blogs[0]->id)}}">{{$watch_blogs[0]->title}}</a></h4>
                                @endif
                                <p class="article-para">{{$watch_blogs[0]->blogPost}}</p>

                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <div class="col-sm-5">
                    <div class="blog-right">
                        @if(count($watch_blogs) >= 1)
                        @for ($i = 1; $i < count($watch_blogs); $i++) <div class="blog__right-item">
                            <div class="blog__right-item__left">
                                @if($watch_blogs[$i]->url != null)
                                <a class="home-view" href="{{url($watch_blogs[$i]->url)}}"><img src="{{'uploads/'.$blog_images->firstWhere('listing_id', '=', $watch_blogs[$i]->id)->filename}}" alt="" class="img-fluid"></a>
                                @else
                                <a class="home-view" href="{{url('watchblog/'.$watch_blogs[$i]->id)}}"><img src="{{'uploads/'.$blog_images->firstWhere('listing_id', '=', $watch_blogs[$i]->id)->filename}}" alt="" class="img-fluid"></a>
                                @endif
                            </div>
                            <div class="blog__right-item__right">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i> <span>{{date_format($watch_blogs[$i]->created_at, 'F d, Y')}}</span></span>

                                </div>
                                @if($watch_blogs[$i]->url != null)
                                <h4><a href="{{url($watch_blogs[$i]->url)}}">{{$watch_blogs[$i]->title}}</a></h4>
                                @else
                                <h4><a href="{{url('watchblog/'.$watch_blogs[$i]->id)}}">{{$watch_blogs[$i]->title}}</a></h4>
                                @endif
                                <p class="article-para">{{$watch_blogs[$i]->blogPost}} </p>


                            </div>
                    </div>
                    @endfor
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>


    <section class="become-a-section">
        <div class="container">
            <h3>If you are a seller in automobiles, watches or real estate, sign up below to advertise your inventory globally.</h3>
            <a href="/becomedealer" class="read-more fill-read-more">Become a Seller</a>
        </div>

    </section>

    @include('tangiblehtml.innerfooter')



    <script src="/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/local.js"></script>
</body>
<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function() {

        $("#brand").change(function() {
            console.log('---------------------', $(this).val());
            $('#findValue').prop('disabled', false);
            var checked = [];
            checked.push($(this).val());
            $.ajax({
                url: "/watchlisting/brand",
                type: 'post',
                dataType: "json",
                data: {
                    _token: '{{csrf_token()}}',
                    brandfilter: checked,
                    type: 'brand'
                },
                success: function(data) {
                    var listingHtml = '';
                    console.log(data);
                    if (data['model'] != null) {
                        var modelHtml = "";
                        var modelData = data['model'];
                        modelHtml = `<option value="" disabled selected>All Models</option>`;
                        for (var i = 0; i < modelData.length; i++) {
                            if (modelData[i].status == '1') {
                                modelHtml += `
                      	<option value="${modelData[i].id}">${modelData[i].watch_model_name}</option>`
                            }
                        }
                        $('#model').html(modelHtml);
                    }
                }
            });
        });

        $("#year_from").change(function() {
            $.ajax({
                url: "/watchlisting/getYear",
                type: 'post',
                dataType: "json",
                data: {
                    _token: '{{csrf_token()}}',
                    year: $(this).val()
                },
                success: function(data) {
                    var to_year = '';
                    var year = data['to_year'];
                    to_year = `<option value="" disabled selected>To year</option>`;
                    for (var i = 0; i < year.length; i++) {
                        console.log('inside');
                        to_year += `
	                      	<option value="${year[i].year_of_manufacture}">${year[i].year_of_manufacture}</option>`
                    }
                    $('#year_to').html(to_year);
                }
            });

        });

        $('#clear').click(function(e) {
            $('#brand')[0].selectedIndex = 0;
            $('#model')[0].selectedIndex = 0;
            $('#year_from')[0].selectedIndex = 0;
            $('#year_to')[0].selectedIndex = 0;
        });

        $("#model").change(function() {
            console.log('---------------------');
            $('#findValue').prop('disabled', false);
        });
        $("#year_from").change(function() {
            console.log('---------------------');
            $('#findValue').prop('disabled', false);
        });
        $("#year_to").change(function() {
            console.log('---------------------');
            $('#findValue').prop('disabled', false);
        });

    });
</script>

</html>