<!DOCTYPE html>
<html lang="en">

<!-- <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit Profile</title>
    <link href="https://fonts.googleapis.com/css2?family=Muli:wght@200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https: //use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="css/intlTelInput.min.css">
    <link rel="stylesheet" href="css/hover-min.css" />
    <link rel="stylesheet" href="css/slick.css" />
    <link rel="stylesheet" href="css/font-style.css">
    <link rel="stylesheet" href="css/local.css">
    <link rel="stylesheet" href="css/responsive.css">
</head> -->
@include('tangiblehtml.headheader')
<body>
    @include('tangiblehtml.innerheader')

    <section class="become-seller-from overlay-wrapper">
      
    </section>
    <div class="container mt-5">
    @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
    @if(session()->has('success_msg'))
                <div class="alert alert-success" role="alert">
                {{ session()->get('success_msg') }}
                </div>
                @endif
    <form id="msform" action="{{ route('myprofile.update',Auth::user()->id ) }}" method="post" enctype="multipart/form-data">
     @method('PATCH')
     @csrf
      <div class="main-heading stm-border-bottom-unit bg-color-border">
        <h3>My Profile</h3>     
    </div> 
      <div class="row">
        <div class="col-xl-4 order-xl-2">
          <div class="card card-profile">          
            <div class="row justify-content-center">
              <div class="col-lg-3 order-lg-2">
                <div class="card-profile-image">
                  <a href="#">
                    <img src="https://dummyimage.com/300/000/fff.png" class="rounded-circle">
                  </a>
                  <hr class="my-4">
                </div>
              </div>
            </div>            
            <div class="card-body pt-0">             
              <div class="text-center">
                <h5 class="h3">
                  {{Auth::user()->first_name}} {{Auth::user()->last_name}}<!-- <span class="font-weight-light">, 27</span> -->
                </h5>
                <div class="h5 font-weight-300">
                  <i class="ni location_pin mr-2"></i>
                  @if($userDetails !='')           
                  {{$userDetails->cityName->city_name}}
                  @endif
                </div>
                <div class="h5 mt-4">
                  @if($userDetails !='') 
                  <i class="ni business_briefcase-24 mr-2"></i>{{$userDetails->occupation}}
                  @endif
                </div>
                <!-- <div>
                  <i class="ni education_hat mr-2"></i>University of Computer Science
                </div> -->
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-8 order-xl-1">
          <div class="card">
           
            <div class="card-body">
              <form>
                <h6 class="heading-small text-muted mb-4">User information</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Username</label>
                        <input type="text" id="input-username" class="form-control" placeholder="Username" value="{{Auth::user()->first_name}} {{Auth::user()->last_name}}">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Email address</label>
                        <input type="email" id="input-email"  class="form-control" placeholder="{{Auth::user()->email}}" value="{{Auth::user()->email}}" disabled>
                        <input type="hidden" name="email" value="{{Auth::user()->email}}" />
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-first-name">First name</label>
                        <input type="text" id="input-first-name" name="first_name" class="form-control" placeholder="First name" value="{{Auth::user()->first_name}}">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-last-name">Last name</label>
                        <input type="text" id="input-last-name"  name="last_name" class="form-control" placeholder="Last name" value="{{Auth::user()->last_name}}">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                  <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-last-name">Phone Number</label>
                        <input type="number" id="input-last-name"  name="phoneNumber" class="form-control" placeholder="Phone Number" value="{{Auth::user()->phone}}">
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="my-4">
                <h6 class="heading-small text-muted mb-4">Login information</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Email address</label>
                        <input type="email" id="inputUserEmail" name="email" class="form-control" placeholder="{{Auth::user()->email}}" disabled>
                        <!-- <a href="#">Change email address</a> -->
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Password</label>
                        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="password">
                        
                      </div>
                    </div>                    
                  </div>                 
                </div>
                <hr class="my-4">
                <!-- Address -->
                <h6 class="heading-small text-muted mb-4">Contact information</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Address</label>
                        <input id="input-address" class="form-control" name="address" placeholder="Home Address" value="@if($userDetails !='') {{$userDetails->user_address }}@endif" type="text">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Country</label>
                        @if($userDetails !='') 
                        <select id="countryId" class="form-control required"  name="country">
                          
                         <?php $countryId = $userDetails->firstWhere('user_id',Auth::user()->id) != null ? $userDetails->firstWhere('user_id',Auth::user()->id)->country_id : ""; 
                         ?>
                         
                                @if($countryId != null)
                                <option value="{{$countryId}}">{{$country_data->firstWhere('id',$countryId )->country_name}}</option>
                                @else
                                <option value="">Select Country</option>
                                @endif
                                @foreach ($country_data as $country)
                                <option value="{{$country->id}}">{{$country->country_name}}</option>
                                @endforeach
                                </select>
                                @else
                                <select id="countryId" class="form-control required"  name="country">
                                <option value="">Select Country</option>
                                </select>
                                @endif
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-city">City</label>
                          @if($userDetails !='') 
                        <select id="city" name="city" class="form-control required">
                        <?php $cityId = $userDetails->city_id;?>
                        
                                @if($cityId != null)
                                <option value="{{$cityId}}">{{$city_data->firstWhere('id',$cityId )->city_name}}</option>
                                @else
                                <option value="">Select City</option>
                                @endif
                                </select>
                                @else
                                <select id="city" name="city" class="form-control required">
                                <option value="">Select City</option>
                                </select>
                                @endif
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-country">Postal code</label>
                        <input type="number" id="input-postal-code" name="postalCode" class="form-control" placeholder="Postal code" value="@if($userDetails !='') {{$userDetails->user_postalCode}}@endif">
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="my-4">
                <!-- Description -->
                <h6 class="heading-small text-muted mb-4">About me</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Date of birth</label>
                        <input type="date" id="input-dob" name="date_of_birth" class="form-control" placeholder="DOB" value="@if($userDetails !='') {{$userDetails->dob}}@endif">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Occupation</label>
                        <input type="text" id="input-occupation" name="occupation" class="form-control" placeholder="Occupation" value="@if($userDetails !='') {{$userDetails->occupation}}@endif">
                      </div>
                    </div>
                  </div>                
                  <div class="form-group">
                    <label class="form-control-label">About Me</label>
                    <textarea rows="4" class="form-control" name="aboutMe" placeholder="@if($userDetails !='') {{$userDetails->about_me}}@endif" value="@if($userDetails !='') {{$userDetails->about_me}}@endif"></textarea>
                  </div>
                </div>
                <div class="pl-lg-4">
                  <div class="row">
                <button type="submit" class="btn btn-primary mx-auto ">Save Changes</button>
                 </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div> 
      </form>     
    </div>
        <div class="support-box">
        <a href="#" class="icon-text hvr-float-shadow" id="icon-anchor">
            <p><i class="fa fa-envelope-o" aria-hidden="true"></i> Ask Us If You Any Queries</p>
        </a>
    </div>
   

    @include('tangiblehtml.innerfooter')


    <script src="js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/slick.min.js"></script>
    <script src="js/intlTelInput.min.js"></script>
    <script src="js/local.js"></script>
    
    <script type="text/javascript">

    $(document).ready(function(){
        $("#msform").validate({
      
          rules: {
          	email: "required",
          	phoneNumber: "required",
          	
          },
      
          messages: {
        	  email: "email can\'t be left blank",
        	  phoneNumber: "Phone Number can\'t be left blank"
          },
      
          submitHandler: function(form) {
            form.submit();
          }
        });
      });
    </script>
  
</body>

</html>