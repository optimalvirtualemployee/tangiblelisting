<!DOCTYPE html>
<html lang="en">
    <style>
        .sendEnqBtn{
                background-color: #eb5322 !important;
                border-radius: 55px  !important;
                font-weight: 700  !important;
                width: 100%  !important;
                padding: 10px 16px  !important;
                display: inline-block  !important;
                transition: 0.3s ease  !important;
                text-transform: uppercase  !important;
                font-size: 14px  !important;
                border: 1px solid #eb5322  !important;
                text-align: center  !important;
                color: #000  !important;
        }
    </style>
   @include('tangiblehtml.headheader')
   <body>
      @include('tangiblehtml.innerheader')

<?php /*?><div class="custom-home-listing-wrapper pb-3">
        <nav aria-label="breadcrumb ">
            <div class="container">
                <ol class="breadcrumb bg-color">
                    <li class="breadcrumb-item"><a href="/tangiblerealestate">Home</a></li>
                    <li class="breadcrumb-item"><a href="/propertylisting">Property</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{$property->property_type}}</li>
                </ol>
            </div>
        </nav>
    </div> <?php */ ?>
        <!-- Single Property Section Begin -->
      <div class="single-property-wrapper">
          <div class="container">
              <div class="row spad-p">
                  <div class="col-lg-12">
                                                           @if(session()->has('success_msg'))
                <div class="alert alert-success" role="alert">
                {{ session()->get('success_msg') }}
                </div>
                @endif
                      <div class="property-title">

                          <h3>Title:- @if($property->ad_title ?? ''){{$property->ad_title}}@endif</h3>
                          <a href="#"><i class="fa flaticon-placeholder"></i>Address:- @if($property->address ?? ''){{$property->address}},@endif @if(!empty($property->postal_code)){{$property->postal_code}}@endif</a>
                      </div>
                      <div class="property-price">
                          <p>For Sale</p>
                          <span>Price:- @if(!empty($property->property_price)){{currency()->convert(floatval($property->property_price), 'USD', currency()->getUserCurrency())}}@endif</span>
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-lg-12">
                      <div class="property-img">
                        <div class="property-detail-slider">
                        @if($images ?? '')
                          @foreach($images as $img)
                            <div class="property-detail-item">
                              <img src="{{'/uploads/'.$img->filename}}" class="img-fluid">
                            </div>
                          @endforeach
                        @endif
                        
                        </div>
                        
                      </div>
                      
                  </div>
              </div>
              <div class="property-detail-wrapper pt-4">
                  <div class="row">
                        <div class="col-lg-8">
                            <div class="p-ins">
                                <div class="row details-top">
                                    <div class="col-lg-12">
                                        <div class="t-details">
                                            <div class="register-id">
                                                <p>Registered ID: <span>@if(!empty($property->id)){{$property->id}}@endif</span></p>
                                            </div>
                                            <div class="popular-room-features single-property">
                                                <div class="size">
                                                    <p>Land Size</p>
                                                    <i class="fas fa-chart-area"></i>
                                                    <span>@if(!empty($property->land_size)){{$property->land_size .' '. 'sqft'}}@endif</span>
                                                </div>
                                                <div class="beds">
                                                    <p>Beds</p>
                                                    <i class="fas fa-bed"></i>
                                                    <span>@if(!empty($property->number_of_bedrooms)){{$property->number_of_bedrooms}}@endif</span>
                                                </div>
                                                <div class="baths">
                                                    <p>Baths</p>
                                                    <i class="fas fa-bath"></i>
                                                    <span>@if(!empty($property->number_of_bathrooms)){{$property->number_of_bathrooms}}@endif</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="property-description">
                                            <h4>Description</h4>
                                            <p>@if(!empty($property->comment)){{strip_tags($property->comment)}}@endif </p>
                                        </div>
                                        <div class="property-features">
                                            <h4>Property Features</h4>
                                            <div class="property-table">
                                                <table>
                                                <?php echo $html ?>    
                                                </table>
                                            </div>
                                        </div>
                                        <div class="location-map">
                                            <h4>Location</h4>
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d107002.020096289!2d-96.80666618302782!3d33.06138629992991!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x864c21da13c59513%3A0x62aa036489cd602b!2sPlano%2C+TX%2C+USA!5e0!3m2!1sen!2sbd!4v1558246953339!5m2!1sen!2sbd" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Start of the report section -->
                    <div id="report">
                        <br>
                        <div class="report">
                            @auth
                            @if($savedListing && $savedListing->listingId == $property->id && $savedListing->userId == Auth::user()->id)
                            <button type="button" class="btn btn-success" >
                                <i class="fa fa-heart"  aria-hidden="true"></i> <span>Saved</span>
                            </button>
                            @else
                            <button type="button" class="btn btn-success" id="save" value="{{$property->id}}">
                                <i class="fa fa-heart"  aria-hidden="true"></i> <span>Save</span>
                            </button>
                            @endif
                            @else
                            <button type="button" class="btn btn-success" href="#myModal" data-toggle="modal">
                                <i class="fa fa-heart"  aria-hidden="true"></i> <span>Save</span>
                            </button>
                            @endauth
                        </div>
                    </div>
                        </div>
                        <div class="col-md-4">
                    <div class="stm-single-listing-car-sidebar">
                        <div class="dealer-contacts">
                            <?php /*?><div class="dealer-contact-unit purchase">
                                <a href="#" class="buy-btn">Purchase</a>
                                 @if($property->priceType == 'Negotiable')
                                <a href="suggestprice/{{$property->id}}" class="suggest-btn">Make an Offer</a>
                                @endif
                            </div>-->
                            <!-- <div class="dealer-contact-unit address">
                                <i class="fa fa-map-marker"></i>
                                <div class="address">Démouville, France</div>
                            </div>
                            <div class="dealer-contact-unit phone">
                                <i class="fa fa-phone"></i>
                                <div class="phone heading-font">(88*******</div>
                                <span class="stm-show-number" data-id="628">Show number</span>
                            </div> <?php */?>
                        </div>
                        <div class="dealerDetails border">
                            <div class="brand">
                                @if($brand != null)
                                        <img src="{{url(('uploads/'.$brand->brandImg))}}" alt="">
                                @endif  
                            </div>
                            <div class="agentsContainer">
                                
                                    <div class="agents">
                                    <div class="agentPhoto">
                                    @if($agentImage != null)
                                        <img src="{{url(('uploads/'.$agentImage->filename))}}" alt="">
                                    @endif    
                                    </div>
                                    <div class="agentDetails">
                                        <div class="agentName font-weight-bold">
                                        @if(!empty($property->agentname)){{$property->agentname}}@endif
                                        </div>
                                        <div class="agentNumber">
                                        @if(!empty($property->agentMobile)){{$property->agentMobile}}@endif
                                        </div>
                                    </div>
                                </div>
                            </div>   
                            <div class="propertyDetails p-3">
                                <div class="propertyAddress">
                                @if($city != null)
                                {{$city->city_name}}
                                @endif
                                @if($state != null)
                                {{$state->state_name}}
                                @endif
                                @if($country != null)
                                {{$country->country_name}}
                                @endif
                                </div>                                
                            </div>                        
                        </div>

                        <div>
                            <div class="stm-border-bottom-unit bg-color-border">
                                <h5>Contact seller</h5>
                            </div>
                            <div class="contact-seller">
                                
                                @if(Route::has('login'))
                                @auth
                                <?php 
                                $checkMessage = null;
                                if($message->firstWhere('userID', '=', Auth::user()->id) != null)
                                $checkMessage = $message->firstWhere('userID', '=', Auth::user()->id)->firstWhere('productId', '=', $productId);;
                                ?>
                                    @if( $checkMessage != null)
                                <a class="contact-btn d-none" href="/mymessages" role="button"
                                    >Contact Seller  <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                    @else
									 <!--<a class="contact-btn" data-toggle="collapse" role="button"-->
          <!--                          aria-expanded="false" >Contact Seller  <i class="fa fa-angle-down" aria-hidden="true"></i></a>                                   -->
                                    @endif
                                @else
                                <a href="#myModal" class="contact-btn d-none" data-toggle="modal">Contact Seller</a>    
                                @endauth
                                @endif
                            </div>
                            <div class="collapse stm-single-car-contact bg-color show" id="contact-seller-form">
                                <div role="form">
                                    <form method="POST" id="enquiyFormRealEstate" action="{{ url('/RealEstate/enquery') }}">
                                        @csrf
                                            <div class="form-group">
                                                <input type="text" placeholder="Name" id="nameForm" name="name" value="{{ old('name') }}" class="from-control" >
                                                    @error('name')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                            </div>
                                            <input type="hidden" name="productid" value="{{$id}}" class="from-control" value="">
                                            <input type="hidden" name="agentId" value="@if(!empty($property->agent_id)){{$property->agent_id}}@endif" class="from-control" value="">
                                            @if(Route::has('login'))
                                            @auth
                                            <input type="hidden" name="userId" value="{{Auth::user()->id}}" class="from-control" value="">
                                            @endauth
                                            @endif
                                            <div class="form-group">
                                                <input type="number" placeholder="Phone" id="telephoneForm" name="mobile"  value="{{ old('phone') }}" class="from-control" ><span id="phone"></span>
                                                    @error('mobile')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                            </div>
                                            <div class="form-group">
                                                <!-- <input type="email" placeholder="Email" class="from-control" required> -->
                                            </div>
                                            <div class="form-group">
                                                <select class="from-control" name="country" id="countryForm"
                                                    data-role="country-selector" value="default"></select>
                                            </div>

                                            <div class="form-group">
                                                <textarea placeholder="Message"  value="{{ old('message') }}" name="message"   class="form-control"
                                                    >Hi, I am interested in your listing</textarea>
                                                    @error('message')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                            </div>
                                            <div class="form-group">
                                                        @if(Route::has('login'))
                                                        @auth
                                                        <?php 
                                                        $checkMessage = null;
                                                        if($message->firstWhere('userID', '=', Auth::user()->id) != null)
                                                        $checkMessage = $message->firstWhere('userID', '=', Auth::user()->id)->firstWhere('productId', '=', $productId);;
                                                        ?>
                                                            @if( $checkMessage != null)
                                                                <div>
                                                                    Thank your for your enquiry, your information has been sent to the seller
                                                                </div>
                                                            @else
                        									 <input type="button" id="buttonsubmit" value="Send enquiry" class="read-more bg-color-border">
                                                            @endif
                                                        @else
                                                        <a href="#myModal" class="sendEnqBtn" data-toggle="modal">Send Enquiry</a>    
                                                        @endauth
                                                        @endif
                                            </div>
                                        </form>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="boredr-top bg-color-border">
                            <div class="seller-details">
                                <h6 class="seller-status">For Sale by</h6>
                                <h3 class="seller-name">Rama </h3>
                                <h6 class="seller-location">New Delhi, India</h6>
                                <div class="member">
                                    <h5><i title="Our Member" class="fa fa-id-badge" aria-hidden="true"></i> <span>: Since 2015</span> </h5>
                                    <p><i class="fa fa-check-square-o" aria-hidden="true"></i> Trusted seller</p>
                                </div>
                            </div>
                        </div> -->
                    </div>

                </div>
                    </div>
              </div>
          </div>
      </div>
      
    <!-- Single Property End -->
      <!-- <div class="custom-single-listing-wrapper">
        <div class="container">
          
        </div>
      </div> -->
      @include('tangiblehtml.innerfooter')


      <script src="/assets/js/jquery.js"></script>
      <script src="/assets/js/bootstrap.min.js"></script>
      <script src="/assets/js/wow.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
      <script src="/assets/js/local.js"></script>
      <script src="/assets/js/jquery.countrySelector.js"></script>
   </body>
   
<script type="text/javascript">
$(document).ready(function(){
	
	$('#buttonsubmit').on('click',function(){
			
		var isError = false;

		var nameForm = $('#nameForm').val();
		var mobile = $("#telephoneForm").val();
		var countryForm = $('#countryForm').val();
        
			
       	if(mobile.length !=10){
               phone.style.color = 'red';
               phone.innerHTML = "Required 10 digits!"
            	isError = true;	
                   
           }else {
           	phone.style.color = 'green';
           	phone.innerHTML = "";
           } 
       	          
        
       if(nameForm == ""){
			$('#nameForm').css('border','1px solid red');
			isError = true;
			}else{
				
				}


       if(countryForm == ""){
			$('#countryForm').css('border','1px solid red');
			isError = true;
			}else{
				
				}

       if(isError == false){
           
    	   document.getElementById("enquiyFormRealEstate").submit();
           }

    	});

$("#telephoneForm").keyup(checkphonenumber);
    
    function checkphonenumber() {

        var mobile = $("#telephoneForm").val();
		
        if(mobile.length !=10){
            
            phone.style.color = 'red';
            phone.innerHTML = "Required 10 digits!"
        }else {
        	phone.style.color = 'green';
        	phone.innerHTML = "";
        }
    }
	
	$("#save").click(function(){
  	  var listingId = $(this).val();
  	  $('#loading-image').show();
  	  $.ajax({
  	    url: "/user/savelisting",
  	    type: "POST",
  	    data: {listingId: listingId, category: 'Real Estate', _token: '{{csrf_token()}}' },
  	    success : function(data){
  	    	var html = `<i class="fa fa-heart"  aria-hidden="true"></i> <span>Saved</span>`;
  	    	$('#save').html(html);
  	      }
  	    });
  	  });
});
  
</script>
</html>