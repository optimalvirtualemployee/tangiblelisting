<!DOCTYPE html>
<html lang="en">

@include('tangiblehtml.headheader')

<body>
    @include('tangiblehtml.innerheader')


    <div class="custom-home-listing-wrapper ">
        <nav aria-label="breadcrumb ">
            <div class="container-fluid">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/tangiblerealestate">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Realstate</li>
                </ol>
            </div>
        </nav>
        <div class="container-fluid pt-4">
            <div class="row">
                <div class="col-lg-3 order-1">
                    <div class="product-filter" id="filter-form">
                    <form id="searchForm" method="GET" action="{{url('/propertylisting/search')}}">
                        <div class="product-filter-inner">
                            <div class="filter">
                                <p><i class="fa fa-filter" aria-hidden="true"></i> Refine Search</p>
                                <a href="javascript:;"><i class="fa fa-undo" aria-hidden="true" id="clear"> Clear</a></i>
                            </div>
                            <div class="filter-area">
								 <div class="search-box mb-2 mt-2">
						            <div class="finder">
                                        <div class="finder__outer">
                                            <div class="finder__inner">
                                                <div class="finder__icon" ref="icon"></div>
                                                @if(isset($searchTerm))
                                                <input class="finder__input" id="autocomplete2" type="text" name="search" placeholder="City, Region or Country" value="{{$searchTerm}}" />
                                                @else
                                                <input class="finder__input" id= "autocomplete2" type="text" name="" placeholder="City, Region or Country" value=""/>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#hometype-filter"
                                        role="button" aria-expanded="false" aria-controls="hometype-filter">
                                        <div class="icon-heading"><i class="fa fa-home" aria-hidden="true"></i> Home
                                            Type</div> <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="hometype-filter">
                                        <div class="inner-text">
                                            <div class="pt-2 pb-1">
                                                @foreach($propertyType_data as $propertyType)
                                                <div class="custom-control">
                                                   @if(isset($homeSelected))
                                                    <input type="checkbox" class="" id="{{$propertyType->property_type}}" name="home-{{$propertyType->property_type}}" value="{{$propertyType->id}}" {{in_array($propertyType->id,$homeSelected) ? "Checked" : ""}}>
                                                   @else 
                                                   <input type="checkbox" class="" id="{{$propertyType->property_type}}" name="home-{{$propertyType->property_type}}" value="{{$propertyType->id}}">
                                                   @endif
                                                    <label class="custom-control-label" for="{{$propertyType->property_type}}">{{$propertyType->property_type}}</label>
                                                   <!-- <div class="value">(867)</div> --> 
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#condition-filter"
                                        role="button" aria-expanded="false" aria-controls="condition-filter">
                                        <div class="icon-heading"><i class="fa fa-flag" aria-hidden="true"></i>
                                            Condition</div> <span><i class="fa fa-caret-right"
                                                aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="condition-filter">
                                        <div class="inner-text">
                                            <div class="pt-2 pb-1">
                                                <div class="custom-control custom-radio">
                                                   @if(isset($condition))
                                                    <input type="radio" id="condition1" name="condition1"
                                                        class="custom-control-input" value="0" {{$condition == '0' ? "Checked" : ""}}>
                                                    @else
                                                    <input type="radio" id="condition1" name="condition1"
                                                        class="custom-control-input" value="0">
                                                    @endif        
                                                    <label class="custom-control-label" for="condition1">New</label>
                                                    <!-- <div class="value">(867)</div> --> 
                                                </div>
                                                <div class="custom-control custom-radio">
                                                    @if(isset($condition))
                                                    <input type="radio" id="condition2" name="condition1"
                                                        class="custom-control-input" value="1" {{$condition == '1' ? "Checked" : ""}}>
                                                    @else
                                                    <input type="radio" id="condition2" name="condition1"
                                                        class="custom-control-input" value="1">
                                                     @endif       
                                                    <label class="custom-control-label"
                                                        for="condition2">Established</label>
                                                   <!-- <div class="value">(867)</div> --> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#bed-filter"
                                        role="button" aria-expanded="false" aria-controls="bed-filter">
                                        <div class="icon-heading"><i class="fa fa-bed" aria-hidden="true"></i>
                                            Bedrooms</div> <span><i class="fa fa-caret-right"
                                                aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="bed-filter">
                                        <div class="inner-text mt-2">
                                            <div class="select-box">
                                                <div class="title">Min :</div>
                                                <select class="form-control custom-select" name="minbeds"
                                                    id="min-beds">
                                                    <option value="" selected disabled>Choose...</option>
                                                    @foreach($bedroom_data as $bedroom)
                                                    @if(isset($bedMin))
                                                    <option value="{{$bedroom->number_of_bedrooms}}" {{$bedMin == $bedroom->number_of_bedrooms ? "Selected" : ""}}>{{$bedroom->number_of_bedrooms}}</option>
                                                    @else
                                                    <option value="{{$bedroom->number_of_bedrooms}}">{{$bedroom->number_of_bedrooms}}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="select-box">
                                                <div class="title">Max :</div>
                                                <select class="form-control custom-select" name="maxbeds"
                                                    id="max-beds">
                                                    <option value="" selected disabled>Choose...</option>
                                                    @foreach($bedroom_data as $bedroom)
                                                    @if(isset($bedMax))
                                                    <option value="{{$bedroom->number_of_bedrooms}}" {{$bedMax == $bedroom->number_of_bedrooms ? "Selected" : ""}}>{{$bedroom->number_of_bedrooms}}</option>
                                                    @else
                                                    <option value="{{$bedroom->number_of_bedrooms}}">{{$bedroom->number_of_bedrooms}}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#bath-filter"
                                        role="button" aria-expanded="false" aria-controls="bath-filter">
                                        <div class="icon-heading"><i class="fa fa-bath" aria-hidden="true"></i>
                                            Bathrooms</div> <span><i class="fa fa-caret-right"
                                                aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="bath-filter">
                                        <div class="inner-text mt-2">
                                            <div class="select-box">
                                                <div class="title">Min :</div>
                                                <select class="form-control custom-select" name="minbath"
                                                    id="min-bath">
                                                    <option value="" selected disabled>Choose...</option>
                                                    @foreach($bathroom_data as $bathroom)
                                                    @if(isset($bathMin))
                                                    <option value="{{$bathroom->number_of_bathrooms}}" {{$bathMin == $bathroom->number_of_bathrooms ? "Selected" : ""}}>{{$bathroom->number_of_bathrooms}}</option>
                                                    @else
                                                    <option value="{{$bathroom->number_of_bathrooms}}">{{$bathroom->number_of_bathrooms}}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="select-box">
                                                <div class="title">Max :</div>
                                                <select class="form-control custom-select" name="maxbath"
                                                    id="max-bath">
                                                    <option value="" selected disabled>Choose...</option>
                                                    @foreach($bathroom_data as $bathroom)
                                                    @if(isset($bathMax))
                                                    <option value="{{$bathroom->number_of_bathrooms}}" {{$bathMax == $bathroom->number_of_bathrooms ? "Selected" : ""}}>{{$bathroom->number_of_bathrooms}}</option>
                                                    @else
                                                    <option value="{{$bathroom->number_of_bathrooms}}">{{$bathroom->number_of_bathrooms}}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="filter-box icon">
                                    <a class="filter-text" data-toggle="collapse" href="#price-filter" role="button"
                                        aria-expanded="false" aria-controls="price-filter">
                                        <div class="icon-heading"><i class="fa fa-money" aria-hidden="true"></i> Price
                                        </div> <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse show" id="price-filter">
                                        <div class="inner-text">
                                            <div class="values">
                                                <!-- <div class="min-price"></div>
                                    <p class="max-price mt-3"></p> -->
                                                <div id="slider-range" class="price-filter-range" name="rangeInput">
                                                </div>
                                                <div
                                                    style="margin: 20px auto 5px;display: flex;justify-content: space-between;align-items: center;text-align: center;">
                                                    <div class="div">
                            <label style="line-height: 1;" class="d-block" for="min_price">Min</label>
                            @if(isset($priceMin))
                            <input type="number" min=0 max={{$price_max-10}} value = {{$priceMin}} oninput="validity.valid||(value='0');" id="min_price" name="minPrice"
                              class="price-range-field" />
                             @else
                             <input type="number" min=0 max={{$price_max-10}} value = 0 oninput="validity.valid||(value='0');" id="min_price" name="minPrice"
                              class="price-range-field" /> 
                             @endif 
                          </div>
                          <div class="div">
                            <label style="line-height: 1;" class="d-block" for="max_price">Max</label>
                            @if(isset($priceMax) && $priceMax > 0)
                            <input type="number" min=0 max={{$price_max}} value = {{$priceMax}} oninput="validity.valid||(value='{{$priceMax}}');" name="maxPrice"
                              id="max_price" class="price-range-field" />
                              @else
                              <input type="number" min=0 max={{$price_max}} value = {{$price_max}} oninput="validity.valid||(value='{{$price_max}}');" name="maxPrice"
                              id="max_price" class="price-range-field" />
                              @endif
                          </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#landsize-filter"
                                        role="button" aria-expanded="false" aria-controls="landsize-filter">
                                        <div class="icon-heading"><i class="fa fa-map-o" aria-hidden="true"></i> Land
                                            Size</div>
                                        <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="landsize-filter">
                                        <div class="inner-text">
                                            <div class="form-row mb-2 mt-2">
                                                <label for="landsize">Min Land Size :</label>
                                                <!-- <input class="form-control" list="browsers" name="landsize" id="landsize"> -->
                                                <input list="browsers" class="form-control" id="yearfrom" value=""
                                                     placeholder="any">
                                                <datalist id="browsers">
                                                    <option value="200 sq/ft">
                                                    <option value="300 sq/ft">
                                                    <option value="400 sq/ft">
                                                    <option value="500 sq/ft">
                                                    <option value="600 sq/ft">
                                                    <option value="700 sq/ft">
                                                    <option value="800 sq/ft">
                                                    <option value="900 sq/ft">
                                                    <option value="1000 sq/ft">
                                                    <option value="1100 sq/ft">
                                                    <option value="1200 sq/ft">
                                                </datalist>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#country-filter"
                                        role="button" aria-expanded="false" aria-controls="country-filter">
                                        <div class="icon-heading"><i class="fa fa-globe" aria-hidden="true"></i> Country
                                        </div>
                                        <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="country-filter">
                                        <div class="inner-text">
                                            <ul>
                                            @foreach($country_data as $country)
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                        @if(isset($countrySelected))
                                                            <input type="checkbox" class="" id="{{$country->id}}" name="country-{{$country-> country_name}}" value="{{$country-> id}}" {{in_array($country-> id,$countrySelected) ? "Checked" : ""}}>
                                                        @else
                                                        <input type="checkbox" class="" id="{{$country->id}}" name="country-{{$country-> country_name}}" value="{{$country-> id}}">
                                                        @endif    
                                                            <label class="custom-control-label" for="{{$country->id}}">{{$country-> country_name}}</label>
                                                        </div>
                                                        <!-- <div class="value">(867)</div> -->
                                                    </a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
								<button type= "submit" id= "searchButton" class="site-btn">Search</button>
                            </div>
                        </div>
                        </form>
                    </div>

                </div>
                
                
                <div class="col-lg-9 mb-3 order-2">
                    <div class="main-heading mb-0">
                        <h3>Luxury Homes for Sale Worldwide</h3>
                        <p>Classic and contemporary design</p>
                    </div>
                    <div class="sorting-panel">
                        <form action="#" class="needs-validation form-inline justify-content-end mb-3" novalidate>
                            <label for="sortby" class="mr-2">Sort by</label>
                            <select id="sortBy" class="form-control custom-select " name="sortBy">
                                 @if(!isset($sortBy))
                                <option value="0" selected>Popularity</option>
                                @else
                                <option value="0">Popularity</option>
                                @endif
                                @if(isset($sortBy) && $sortBy == "1")
                                <option value="1" selected>High to low</option>
                                @else
                                <option value="1">High to low</option>
                                @endif
                                @if(isset($sortBy) && $sortBy == "2")
                                <option value="2" selected>Low to high</option>
                                @else
                                <option value="2">Low to high</option>
                                @endif
                                @if(isset($sortBy) && $sortBy == "3")
                                <option value="3" selected>Recent</option>
                                @else
                                <option value="3">Recent</option>
                                @endif
                            </select>
                        </form>
                    </div>

                    <div class="row">
                    @foreach($propertyListing as $property)
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="home-list-box listing-page-slider-wrapper">
                                <div class="home-list-slider">
									@foreach($images->where('listing_id',$property->id) as $image)
                                    <div class="home-list-item">
                                        <a class="home-view" href="{{route('tangiblerealestate.show',Crypt::encrypt($property->id))}}"><img src="{{url('uploads/'.$image->filename)}}" alt=""
                                                class="img-fluid"></a>
                                    </div>
                                    @endforeach
                                </div>

                                <div class="product-detail-btn">
                                    <ul>
                                        <li><a href="#"><i class="fas fa-search-plus"></i></a></li>
                                        <li><a href="#"><i class="far fa-heart"></i></a></li>
                                        <li><a href="#"><i class="fas fa-plus"></i></a></li>
                                    </ul>
                                </div>
                                <div class="home-content">
                                    <span class="price pl-0">{{currency()->convert(floatval($property->value), 'USD', currency()->getUserCurrency())}}</span>
                                    
                                    <h4><a href="#">{{$property->ad_title}}</a></h4>
                                    <div class="fasility-item">
                                    <span class="icon"><i class="fas fa-bed"></i>{{$property->bedrooms}} </span>
                                    <span class="icon"><i class="fas fa-chart-area"></i> {{$property->size  .' '. ($property->metric == 'sqft' ? 'Sq Ft' : 'Sq Mtr')}}</span>
                                    </div>
                                </div>
                                <div class="bottom-detais">
                                    <div class="dealer">
                                        <ul>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>
                                        <p class="seller-type sellerType">
                                            
                                            @if($property->agency_id === 0)
                                            <img src="https://dummyimage.com/32x32/000/fff" />
                                            {{ "Private Seller" }}
                                            @else
                                            @if($agenciesImages->firstWhere('listing_id',$property->agency_id) != null)
                                            <img src="{{url('uploads/'.$agenciesImages->firstWhere('listing_id',$property->agency_id)->filename)}}" />
                                            @else
                                            <img src="https://dummyimage.com/32x32/000/fff" />
                                            @endif
                                            {{$property->agency_name}}
                                            @endif
                                            </p>
                                    </div>
                                    <div class="country-flag">
                                        <img src="{{url('uploads/'.$property->filename)}}" alt="">
                                        <p class="c-code">{{$property->countrycode}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @if($propertyListing->hasPages())
          <div class="custom-pagination mt-4 d-flex justify-content-end">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
              @if ($propertyListing->onFirstPage())
            <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
        @else
        	<li class="page-item"><a class="page-link" href="{{ $propertyListing->withQueryString()->previousPageUrl() }}">Previous</a></li>
        @endif
        
        @for($i = 1 ; $i <= $propertyListing->lastPage() ; $i++)
        	@if($propertyListing->currentPage() == $i)
			<li class="page-item active"><a class="page-link" href="{{$propertyListing->withQueryString()->url($i)}}">{{$i}}</a></li>
			@else
			<li class="page-item"><a class="page-link" href="{{$propertyListing->withQueryString()->url($i)}}">{{$i}}</a></li>
			@endif
        @endfor        
          @if ($propertyListing->hasMorePages())
            <li class="page-item"><a class="page-link" href="{{ $propertyListing->withQueryString()->nextPageUrl() }}">Next</a></li>
        @else
            <li class="page-item disabled"><a class="page-link" href="#">Next</a></li>
        @endif    
              </ul>
            </nav>
          </div>
          @endif
                </div>
            </div>
        </div>
    </div>
    
    @include('tangiblehtml.innerfooter')



    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script src="/assets/js/jquery-ui.min.js"></script>
    <script src="/assets/js/local.js"></script>
</body>

<script type="text/javascript">

$(document).ready(function(){ 

	$("#sortBy").change(function(){
		var urlSortBy = window.location.href;
		
		if(urlSortBy.includes("/propertylisting?")){
			var finalUrl = urlSortBy + '&sortBy=' +$(this).val();
			}else  if(urlSortBy.includes("/propertylisting")){
				var finalUrl = urlSortBy + '/search?sortBy=' +$(this).val();
		}
		if(urlSortBy.includes("search")){
			if(urlSortBy.includes("sortBy")){
				finalUrl = urlSortBy.replace(/sortBy=\d/,'sortBy=' + +$(this).val());
				}else{
			finalUrl = window.location.href + '&sortBy=' + +$(this).val();
				}
			}
		window.location.assign(finalUrl);
	});
	
	$( "#autocomplete2" ).autocomplete({
        source: function( request, response ) {
          // Fetch data
          $.ajax({
            url:"/propertylisting/autosearch",
            type: 'post',
            dataType: "json",
            data: {
               _token: '{{csrf_token()}}',
               search: request.term
            },
            success: function( data ) {
               response( data );
            }
          });
        },
        select: function (event, ui) {
           $('#autocomplete2').val(ui.item.finalLabel); // display the selected text
           return false;
        }
      });	

	
	$(function () {
        $("#slider-range").slider({
            range: true,
            orientation: "horizontal",
            min: 0,
            max: {{$price_max}},
            values: [$("#min_price").val(), $("#max_price").val()],
            step: 1000,

            slide: function (event, ui) {
                if (ui.values[0] == ui.values[1]) {
                    return false;
                }

               $("#min_price").val(ui.values[0]);
                $("#max_price").val(ui.values[1]);

                
            }
        });

        $("#min_price").val($("#slider-range").slider("values", 0));
        $("#max_price").val($("#slider-range").slider("values", 1));

    });

	$('#clear').click(function (e) {
	  $('#filter-form').find(':input').each(function() {
	    if(this.type == 'submit'){
	          //do nothing
	      }
	      else if(this.type == 'checkbox' || this.type == 'radio') {
	        this.checked = false;
	      }else if( this.type == 'text'){
	    	  $(this). val("") 
		      }
	   })
	   $('#min-bath')[0].selectedIndex = 0;
	  $('#max-bath')[0].selectedIndex = 0;

	  $('#min-beds')[0].selectedIndex = 0;
	  $('#max-beds')[0].selectedIndex = 0;
	  $(function () {
	        $("#slider-range").slider({
	            range: true,
	            orientation: "horizontal",
	            min: 0,
	            max: {{$price_max}},
	            values: [0, {{$price_max}}],
	            step: 100,

	            slide: function (event, ui) {
	                if (ui.values[0] == ui.values[1]) {
	                    return false;
	                }

	               $("#min_price").val(ui.values[0]);
	                $("#max_price").val(ui.values[1]);
	            }
	        });

	        $("#min_price").val($("#slider-range").slider("values", 0));
	        $("#max_price").val($("#slider-range").slider("values", 1));

	    });
	});
	
});
</script>
</html>