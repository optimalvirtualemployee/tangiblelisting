@extends('admin.common.innercommondefault')
@section('title', 'User Management')
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-5">
            <div class="row">
                <div class="col-lg-12 margin-tb">
                  <div class="pull-left">
                    <h3> Show Role</h3>
                  </div>
                  <div class="pull-right">
                    <a class="btn btn-primary btn-sm" href="{{ route('roles.index') }}"> Back</a>
                  </div>
                </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                  <strong>Name:</strong>
                    {{ $role->name }}
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                  <strong>Permissions:</strong>
                  @if(!empty($rolePermissions))
                    @foreach($rolePermissions as $v)
                      <label class="label label-success">{{ $v->name }},</label>
                    @endforeach
                  @endif
                </div>
              </div>
            </div><hr>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection