<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>@yield('title')</title>
  
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.min.js"></script>

  <!-- Custom fonts for this template-->
  <link rel="stylesheet" href="{{url('assets/vendor/fontawesome-free/css/all.min.css')}}">
   <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

  <!-- Custom styles for this template-->
  
  <link rel="stylesheet" href="{{url('assets/css/sb-admin-2.min.css')}}">
   <link rel="stylesheet" href="{{url('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}">
   
   <!-- CK editor validate script-->
   <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
   
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/admin/home">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3"></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="{{url('/admin/watch/dashboard')}}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>

      
      
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Create Watch categories</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            
            <a class="collapse-item" href="{{url('/admin/watch/displaybrands')}}">Create Brands</a>
            <a class="collapse-item" href="{{url('/admin/watch/displaybrandsmodel')}}">Create Models</a>
            
            <!-- FACTORY SETTINGS -->
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtil" aria-expanded="true" aria-controls="collapseUtil" style="color:black;text-decoration: none;">
                <i class=""></i>
                <span>Factory settings</span>
            </a>
            
            <div id="collapseUtil" class="collapse">
            	<!-- Dial collapse -->
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseDial" aria-expanded="true" aria-controls="collapseDial" style="color:black;text-decoration: none;">
                <i class=""></i>
                <span>- Dial</span>
            </a>
            <div id="collapseDial" class="collapse">
            	<a class="collapse-item" href="{{ url('admin/dialPowerReserve') }}">Power Reserve</a>
                <a class="collapse-item" href="{{ url('admin/dialColor') }}">Color</a>
            </div>	
            
            <!-- Bracelet collapse -->
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBracelet" aria-expanded="true" aria-controls="collapseBracelet" style="color:black;text-decoration: none;">
                <i class=""></i>
                <span>- Bracelet</span>
            </a>
            <div id="collapseBracelet" class="collapse">
            	<a class="collapse-item" href="{{ url('admin/braceletMaterial') }}">- Material</a>
            	<a class="collapse-item" href="{{ url('admin/braceletColor') }}">- Color</a>
            	<a class="collapse-item" href="{{ url('admin/braceletClaspType') }}">- Clasp Type</a>
            	<a class="collapse-item" href="{{ url('admin/braceletClaspMaterial') }}">- Clasp Material</a>
            </div>
            
            <!-- Case collapse -->
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCase" aria-expanded="true" aria-controls="collapseCase" style="color:black;text-decoration: none;">
                <i class=""></i>
                <span>- Case</span>
            </a>
            <div id="collapseCase" class="collapse">
            	<a class="collapse-item" href="{{ url('admin/caseMaterial') }}">- Material</a>
            	<a class="collapse-item" href="{{ url('admin/caseMM') }}">- MM</a>
            	<a class="collapse-item" href="{{ url('admin/caseWRD') }}">- Water RD</a>
            	<a class="collapse-item" href="{{ url('admin/caseGlassType') }}">- Glass Type</a>
            </div>
            
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFunction" aria-expanded="true" aria-controls="collapseFunction" style="color:black;text-decoration: none;">
                <i class=""></i>
                <span>- Funtions</span>
            </a>
            <div id="collapseFunction" class="collapse">
            	<a class="collapse-item" href="{{ url('admin/functionChronograph') }}">- Chronograph</a>
            	<a class="collapse-item" href="{{ url('admin/functionTourbillion') }}">- Tourbillion</a>
            	<a class="collapse-item" href="{{ url('admin/functionGMT') }}">- GMT</a>
            	<a class="collapse-item" href="{{ url('admin/functionAnnualCalender') }}">- Annual Calender</a>
            	<a class="collapse-item" href="{{ url('admin/functionMinuteRepeater') }}">- Minute Repeater</a>
            	<a class="collapse-item" href="{{ url('admin/functionDoubleChronograph') }}">- Double Chronograph</a>
            	<a class="collapse-item" href="{{ url('admin/functionPanormaDate') }}">- Panorma Date</a>
            	<a class="collapse-item" href="{{ url('admin/functionJumpingHour') }}">- Jumping Hour</a>
            	<a class="collapse-item" href="{{ url('admin/functionAlarm') }}">- Alarm</a>
            	<a class="collapse-item" href="{{ url('admin/functionYear') }}">- Year</a>
            	<a class="collapse-item" href="{{ url('admin/functionDay') }}">- Day</a>
            </div>
            
                
            </div>
            
            
            <!-- FACTORY SETTINGS -->
          </div>
        </div>
      </li>


      <!---------------------------- WATCH MASTER SECTION STARTS HERE -------------------------------------->

        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseWatchMaster" aria-expanded="true" aria-controls="collapseWatchMaster">
            <i class="fas fa-clock"></i>
            <span>Watch Master</span>
          </a>
          <div id="collapseWatchMaster" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="{{ url('admin/gender') }}">Gender</a>
              <a class="collapse-item" href="{{ url('admin/type') }}">Type</a>
              <a class="collapse-item" href="{{ url('admin/movement') }}">Movement</a>
              <a class="collapse-item" href="{{ url('admin/case_diameter') }}">Case Diameter</a>
              <a class="collapse-item" href="{{ url('admin/price') }}">Price</a>
              <a class="collapse-item" href="{{ url('admin/bezel') }}">Bezel Material</a>
             
             <!-------- Y
            	<a class="collapse-item" href="{{ url('admin/braceletMaterial') }}">- Bracelet Material</a>
            	<a class="collapse-item" href="{{ url('admin/braceletColor') }}">- Bracelet Color</a>
            	<a class="collapse-item" href="{{ url('admin/braceletClaspType') }}">- Bracelet Clasp Type</a>
            	<a class="collapse-item" href="{{ url('admin/braceletClaspMaterial') }}">- Bracelet Clasp Material</a>
            </div>
            hImageController');hImageController');hImageController');ear of manufacture, Conditions and Inclusion Starts here ------------->
              <hr>
              <a class="collapse-item" href="{{ url('admin/year_of_manufacture') }}">Year Of Manufacture</a>
              <a class="collapse-item" href="{{ url('admin/conditions') }}">Conditions</a>
              <a class="collapse-item" href="{{ url('admin/inclusion') }}">Inclusion</a>
              <a class="collapse-item" href="{{ url('admin/watchfeaturelisting') }}">Features</a>

              <!-------- Year of manufacture, Conditions and Inclusion Ends here ------------->
            </div>
          </div>
        </li>

      <!---------------------------- WATCH MASTER SECTION ENDS HERE ---------------------------------------->
      
      <!---------------------------- WATCH DATA LISTING SECTION STARTS HERE -------------------------------------->

        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseWatchData" aria-expanded="true" aria-controls="collapseWatchData">
            <i class="fas fa-clock"></i>
            <span>Create Watch Data</span>
          </a>
          <div id="collapseWatchData" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="{{ url('admin/createwatchdata') }}">Watch Data</a>
        </li>

      <!---------------------------- WATCH DATA LISTING SECTION ENDS HERE ---------------------------------------->
      
      <!---------------------------- WATCH BANNER SECTION STARTS HERE -------------------------------------->

        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseWatchBanner" aria-expanded="true" aria-controls="collapseWatchBanner">
            <i class="fas fa-clock"></i>
            <span>Create Watch Banner</span>
          </a>
          <div id="collapseWatchBanner" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="{{ url('admin/watchbanner') }}">Watch Banner</a>
        </li>

      <!---------------------------- WATCH BANNER SECTION ENDS HERE ---------------------------------------->
      
      <!------------------------------ Featured  DATA STARTS HERE ------------------------------------------>
	
	<li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse_featuredata" aria-expanded="true" aria-controls="collapse_featuredata">
          <i class="fa fa-gavel"></i>
          <span>Featured Watches</span>
        </a>
        <div id="collapse_featuredata" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ url('admin/featuredwatch') }}">Show Featured Watches</a>
          </div>
        </div>
      </li>
	
<!------------------------------ FEATURE DATA ENDS HERE -------------------------------------------->
		
	  <!---------------------------- WATCH BLOG SECTION STARTS HERE -------------------------------------->

        <li class="nav-item">
          <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseWatchBlog" aria-expanded="true" aria-controls="collapseWatchBlog">
            <i class="fas fa-clock"></i>
            <span>Blogs</span>
          </a>
          <div id="collapseWatchBlog" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="{{ url('admin/watchblog') }}">Show Blogs</a>
        </li>

      <!---------------------------- WATCH BLOG SECTION ENDS HERE ---------------------------------------->	
      
      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <!-- Nav Item - Alerts -->

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Welcome {{ Auth::user()->first_name . ' '. Auth::user()->last_name  }}</span>
               
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="{{url('admin/profile',Auth::user()->id)}}">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                @canany(['superAdmin-create', 'userManagement-create'])
                <a class="dropdown-item" href="{{ url('admin/contactus') }}">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Contact Us
                </a>
                @endcan
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->