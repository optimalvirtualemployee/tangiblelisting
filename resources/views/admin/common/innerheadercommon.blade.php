<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>@yield('title')</title>
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.min.js"></script>

  <!-- Custom fonts for this template-->
  <link rel="stylesheet" href="{{url('assets/vendor/fontawesome-free/css/all.min.css')}}">
   <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
   <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

  <!-- Custom styles for this template-->
  
  <link rel="stylesheet" href="{{url('assets/css/sb-admin-2.min.css')}}">
   <link rel="stylesheet" href="{{url('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}">
   
   <!-- CK editor validate script-->
   <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/admin/home">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3"></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="{{url('/admin/admincommon/dashboard')}}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
     

      <!-- Nav Item - Utilities Collapse Menu -->
      
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Create common entries</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ route('displaycountries') }}">Create Country</a>
            <a class="collapse-item" href="{{ route('displaystates') }}">Create State</a>
            <a class="collapse-item" href="{{ route('displaycity') }}">Create City</a>
            <a class="collapse-item" href="{{ url('admin/currencyData') }}">Create Currency</a>
            <a class="collapse-item" href="{{ url('admin/language') }}">Create Language</a>
          </div>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtil" aria-expanded="true" aria-controls="collapseUtil">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Create Users & Roles</span>
        </a>
        <div id="collapseUtil" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ url('admin/roles') }}">Create Roles</a>
            <a class="collapse-item" href="{{ url('admin/users') }}">Create Users</a>
            <a class="collapse-item" href="{{ url('admin/categories') }}">Categories Permission</a>
          </div>
        </div>
      </li>
      
      
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseDealer" aria-expanded="true" aria-controls="collapseUtil">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Create Dealers & Agents</span>
        </a>
        <div id="collapseDealer" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ url('admin/agencies') }}">Create Dealers</a>
            <a class="collapse-item" href="{{ url('admin/agentdata') }}">Create Agents</a>
            <a class="collapse-item" href="{{ url('admin/agentjobposition') }}">Agents Job</a>
          </div>
        </div>
      </li>

<!--------------------- PACKAGE & PRICING MENU STARTS HERE --------------------------->
      
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePackages" aria-expanded="true" aria-controls="collapsePackages">
          <i class="fas fa-fw fa-cube"></i>
          <span>Packages & Pricing</span>
        </a>
        <div id="collapsePackages" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ url('admin/package_for_dealers') }}">Dealers</a>
            <a class="collapse-item" href="{{ url('admin/package_for_private_sellers') }}">Private Sellers Automobile</a>
            <a class="collapse-item" href="{{ url('admin/package_for_ps_watch') }}">Private Sellers Watch</a>
          </div>
        </div>
      </li>

<!--------------------- PACKAGE & PRICING MENU ENDS HERE ----------------------------->

<!--------------------- SELECT CURRENCY MENU STARTS HERE --------------------------->

	<li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCurrency" aria-expanded="true" aria-controls="collapseCurrency">
          <i class="fas fa-fw fa-cube"></i>
          <span>Currency</span>
        </a>
        <div id="collapseCurrency" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ url('admin/currencySelect/select') }}">Select Currency</a>
            <a class="collapse-item" href="{{ url('admin/countrycurrency') }}">Currency For Country </a>
            <a class="collapse-item" href="{{ url('admin/currencyconverter') }}">Currency Converter </a>
          </div>
        </div>
      </li>
<!--------------------- SELECT CURRENCY MENU ENDS HERE --------------------------->

<!--------------------- CONTRACTED PRODUCTS MENU STARTS HERE --------------------------->

	<li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseContractedProduct" aria-expanded="true" aria-controls="collapseContractedProduct">
          <i class="fas fa-fw fa-cube"></i>
          <span>Contracted Product</span>
        </a>
        <div id="collapseContractedProduct" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ url('admin/propertycontractedproduct') }}">Contracted Properties</a>
            <a class="collapse-item" href="{{ url('admin/watchcontractedproduct') }}">Contracted Watches</a>
            <a class="collapse-item" href="{{ url('admin/automobilecontractedproduct') }}">Contracted Automobiles</a>
          </div>
        </div>
      </li>
<!--------------------- CONTRACTED PRODUCTS MENU ENDS HERE --------------------------->

      <!-- Divider -->
      <hr class="sidebar-divider">

      

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              
            </li>

            <!-- Nav Item - Alerts -->


            <!-- Nav Item - Messages -->
            

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Welcome {{ Auth::user()->first_name . ' '. Auth::user()->last_name  }}</span>
               
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="{{url('admin/profile',Auth::user()->id)}}">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                @canany(['superAdmin-create', 'userManagement-create'])
                <a class="dropdown-item" href="{{ url('admin/contactus') }}">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Contact Us
                </a>
                @endcan
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->