<!DOCTYPE html>
<html>
  <head>
   @include('admin.common.header')
  </head>
  <body>
   
     <!-- Counts Section --> 
     @yield('content')
     @include('admin.common.footer')
    </body>
</html>   