<!DOCTYPE html>
<html>
  <head>
   @include('admin.common.innerheadercommon')
  </head>
  <body>
   
     <!-- Counts Section --> 
     @yield('content')
     @include('admin.common.innerfooter')
    </body>
</html>   