@extends('admin.common.innerdefaultreal')
@section('title', 'Create Contact Us')
@section('content')

<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Add Contact Us</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="createcontactus" method="POST" action="{{ route('contactus.store') }}">
                  @csrf
                  <div class="form-group">
                    <input id="address" class="form-control form-control-user " type="text"  name="address" placeholder="Enter Address" value="">
                   </div>
                   <div class="form-group">
                    <input id="mobile" class="form-control form-control-user " type="text"  name="mobile" placeholder="Enter Mobile" value="">
                   </div>
                   <div class="form-group">
                    <input id="whatsapp" class="form-control form-control-user " type="text"  name="whatsapp" placeholder="Enter Whatsapp" value="">
                  </div>
                  <div class="form-group">
                    <input id="email" class="form-control form-control-user " type="email"  name="email" placeholder="Enter Email" value="">
                  </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">Add New</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#createcontactus").validate({
    
        rules: {
          address: {required: true},
          mobile: {required: true},
          whatsapp: {required: true},
          email: {required: true}
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop
