@extends('admin.common.innercommondefault')
@section('title', 'Create Agent')
@section('content')

<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Create Agent</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="createagent" method="POST" action="{{ route('agentdata.store') }}">
                  @csrf
                  <div class="form-group">
                    <label>First Name:</label><input id="first_name" class="form-control form-control-user " type="text"  name="first_name" placeholder="Enter First Name" value="">
                   </div>
                   <div class="form-group">
                    <label>Last Name:</label><input id="last_name" class="form-control form-control-user " type="text"  name="last_name" placeholder="Enter Last Name" value="">
                  </div>
                  <div class="form-group">
                    <label>Whatsapp:</label><input id="whatsapp" class="form-control form-control-user " type="text"  name="whatsapp" placeholder="Enter Whatsapp No" value="">
                  </div>
                  <div class="form-group">
                    <label>Mobile:</label><input id="mobile" class="form-control form-control-user " type="text"  name="mobile" placeholder="Enter Mobile No" value="">
                  </div>
                  <div class="form-group">
                    <label>Fax:</label><input id="fax" class="form-control form-control-user " type="text"  name="fax" placeholder="Enter Fax" value="">
                  </div>
                  <div class="form-group">
                    <label>Office:</label><input id="office" class="form-control form-control-user " type="text"  name="office" placeholder="Enter Office No" value="">
                  </div>
                  <div class="form-group">
                    <label>Email:</label><input id="email" class="form-control form-control-user " type="email"  name="email" placeholder="Enter Email Address" value="">
                  </div>
                  <div class="form-group">
                    <label>Address:</label><input id="address" class="form-control form-control-user " type="text"  name="address" placeholder="Enter Address" value="">
                  </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">Add</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#createagent").validate({
    
        rules: {
        	first_name: "required",
        	last_name: "required",
        	whatsapp: {required: true, number:true},
        	mobile: {required: true, number:true},
        	fax: {required: true, number:true},
        	office: {required: true, number:true},
        	email: {required: true},
        	address: "required"
        },
    
        
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop
