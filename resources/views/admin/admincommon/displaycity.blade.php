@extends('admin.common.innercommondefault')
@section('title', 'Display Cities')
@section('content')

  <!-- Page Wrapper -->
  <div id="wrapper">
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- End of Topbar -->
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">City</h1>
          @can('superAdmin-create')
          <a href="{{url('/admin/admincommon/createcity')}}" class="btn btn-primary btn-icon-split">
            <span class="text">Add New City</span>
          </a>
          @endcan
          @if(Session::has('success_msg'))
            <div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">{{ Session::get('success_msg') }}</div>
          @endif
          <div class="card shadow mb-4" style="margin-top: 10px;">
            <div class="card-body" >
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>City Name</th>
                      <th>State Name</th>
                      <th>Country Name</th>
                      <th>Status</th>
                      @can('superAdmin-create')
                      <th>Action</th>
                      @endcan
                    </tr>
                  </thead>
                 
                  <tbody>
                    @foreach ($cities as $city)
                      <tr>
                        <td>{{$city->city_name}} </td>
                        <td>{{$city->state_name}} </td>
                        <td>{{$city->country_name}} </td>
                        <td>@if($city->status =='1') Active  @else Inactive @endif  </td>
                        @can('superAdmin-create')
                        <td>
                          <a href="{{url('/admin/admincommon/updatecity/'.$city->id)}}">
                            <i class="fas fa-edit"></i>
                          </a> &nbsp; &nbsp;
                          <a href="#" class="deletecity" id="del_{{$city->id}}">
                            <i class="fas fa-trash-alt"></i>
                          </a>
                        </td>
                        @endcan
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      
<!----------------------------------------------------- JS SCRIPT SECTION --------------------------------->
<script type="text/javascript">
    $(document).on("click", ".deletecity", function() { 
      var url = "{{ route('deletecity') }}";
      var id = this.id;
      var splitid = id.split("_");
      var deleteid = splitid[1];
      if ( confirm("Do you really want to delete record?")) {
        $.ajax({
          url: url,
          type: 'POST',
          cache: false,
          data:{
             id:deleteid,
            _token:'{{ csrf_token() }}'
          },
          success: function(data){ 

              $('#main').html('<div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">' + data.success_msg + '</div>')
               location.reload();
          }
        });
      }
    });

    $(document).ready(function(){
      setTimeout(function(){
        $("div.alert").remove();
      }, 3000 ); 
    });
</script>
<!----------------------------------------------------- JS SCRIPT SECTION --------------------------------->
@stop
