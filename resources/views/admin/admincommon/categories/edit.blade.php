@extends('admin.common.innercommondefault')
@section('title', 'Category Management')
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg">
            <div class="card-body p-5">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h3>Edit {{$category->website_category}} Permissions</h3>
                        </div>
                        
                    </div>
                </div>

                @if (count($errors) > 0)
                  <br><div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                       @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
                       @endforeach
                    </ul>
                  </div>
                @endif


                {!! Form::model($category, ['method' => 'PATCH','route' => ['categories.update', $category->id]]) !!}
                @foreach($permission as $key => $value)
                        @if($key == 0)
                            <strong>Role Management :</strong>
                            <br/>
                          @elseif(( $key & 3 ) == 0)
                            <strong><?php $hd = explode('-',$value->name); ?>{{ucfirst($hd[0]) }} :</strong>
                            <br/>
                          @endif
                            <label>{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $categoryPermissions) ? true : false, array('class' => 'name')) }}
                            {{ $value->name }}</label>
                        <br/>
                        @endforeach
                   <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>     
                        
                {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection