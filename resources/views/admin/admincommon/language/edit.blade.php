@extends('admin.common.innercommondefault')
@section('title', 'Update Language')
@section('content')
<style>
.error {
      color: red;
   }   
</style>
<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update Language</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="updatelanguage" method="POST" action="{{ route('language.update',$language->id) }}">
                  @method('PATCH')
                  @csrf
                  <div class="form-group">
                    <div class="form-group">
                    <label>Language*</label><input id="language" class="form-control form-control-user required" type="text"  name="language" placeholder="Enter Language" value="{{$language->language}}">
                  </div> 
                       <div class="form-group">
                    <label>Status</label>
                    <select id ="status" name="status" class="form-control">
                      <option value="1" {{$language->status == 1 ? 'selected' : ''}}>Active</option>
                      <option value="0" {{$language->status == 0 ? 'selected' : ''}}>Inactive</option>
                    </select>
                  </div>
                      </div> 
                  <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
    	$("#updateCurrencyData").validate({
    	    
            rules: {
            	currencyCode: "required",
            	currency: "required"
            },
        
            messages: {
            	currencyCode: "Currency Code can\'t be left blank",
            	currency: "Currency can\'t be left blank"
            },
        
            submitHandler: function(form) {
              form.submit();
            }
          });
        });
        
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop
