@extends('admin.common.innercommondefault')
@section('title', 'Update City')
@section('content')
<style>
.error {
      color: red;
   }
</style>
<div class="container">
    <!-- Outer Row -->
    <div class="row justify-content-center">
      <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-8">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Update City</h1>
                  </div>
                   @if($errors->any())
                    @foreach($errors->all() as $error)
                      <div class="alert alert-danger">
                    {{$error}}
                      </div>
                    @endforeach
                  @endif
                  <form class="user" id="city" method="POST" action="{{ route('editcity') }}">
                  @csrf
                   @foreach ($cities as $city)
                   <div class="form-group">
                      <label>Country</label>
                      <input id="statename" value="{{$city->country_name}}" class="form-control form-control-user " type="text"  name="countryname" required  placeholder="Enter Country" disabled="disbaled">
                    </div>
                    <div class="form-group">
                      <label>State</label>
                      <input id="statename" value="{{$city->state_name}}" class="form-control form-control-user " type="text"  name="statename" required  placeholder="Enter State" disabled="disbaled">
                    </div>
                    <div class="form-group">
                      <input id="cityname" value="{{$city->city_name}}" class="form-control form-control-user " type="text"  maxlength="50" name="cityname" required  placeholder="Enter City">
                      <input type="hidden" name="stateid" value="{{$city->id}}"/>
                    </div>
                    <div class="form-group">
                      <label>Status</label>
                      <select id ="citystatus" name="citystatus" class="form-control" required>
                        <option value="1" {{$city->status == "1" ? 'selected' : ''}}>Active</option>
                        <option value="0" {{$city->status == "0" ? 'selected' : ''}}>Inactive</option>
                      </select>
                    </div> 
                    @endforeach
                      <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



<!---------------------------------------- JS SCRIPT SECTION -------------------------------------------->
<script type="text/javascript">
    $(document).ready(function(){
     $("#cityname").keypress(function (e) {
            var keyCode = e.keyCode || e.which;

            $("#lblError").html("");

            //Regex for Valid Characters i.e. Alphabets.
            var regex = /^[A-Za-z]+$/;

            //Validate TextBox value against the Regex.
            var isValid = regex.test(String.fromCharCode(keyCode));
            if (!isValid) {
                $("#lblError").html("Only Alphabets allowed.");
            }

            return isValid;
        });    
        
      $("#city").validate({
        rules: {
          cityname: "required",
        },
    
        messages: {
          cityname: "City can\'t be empty",
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>
<!---------------------------------------- JS SCRIPT SECTION -------------------------------------------->
@stop
