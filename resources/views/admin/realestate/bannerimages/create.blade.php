@extends('admin.common.innerdefaultreal') 
@section('title', 'Create Property Banner') 
@section('content')
<style>
.error {
      color: red;
   }
</style>
<div class="container">
	<!-- Outer Row -->
	<div class="row justify-content-center">
		<div class="col-xl-10 col-lg-12 col-md-9">
			<div class="card o-hidden border-0 shadow-lg my-5">
				<div class="card-body p-0">
					<!-- Nested Row within Card Body -->
					<div class="row">
						<div class="col-lg-8">
							<div class="p-5">
								<div class="text-center">
									<h1 class="h4 text-gray-900 mb-4">Create Property Banner</h1>
								</div>
								@if($errors->any()) @foreach($errors->all() as $error)
								<div class="alert alert-danger">{{$error}}</div>
								@endforeach @endif
								<form class="user" id="createimage" method="POST"
									action="{{ route('propertybanner.store') }}"
									enctype="multipart/form-data">
									@csrf
									<div class="form-group">
										<div class="input-group control-group increment">
											<div class="form-group">
											<input type="file" style="width:500px;" name="photos[]" class="form-control required">
											</div>
											<div class="form-group">
											<label>Image Display Order:</label><input class="form-control col-md-16 required" style="width:500px;" type="number"  name="imagePriority" placeholder="Enter Image Display Order" value="">
											</div>
										</div>
										<div class="form-group">
                      <label>Status</label>
                      <select id ="status" name="status" class="form-control required">
                       <option value="">Select Status</option>
                        <option value="1" >Active</option>
                        <option value="0" >Inactive</option>
                      </select>
                    </div>
									</div>
									<button type="submit"
										class="btn btn-primary btn-user btn-block" value="Upload">Add</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
	  $(document).ready(function() {
		   $("#createimage").validate({
			    
		        rules: {
		        	photos: {required: true},
		        	imagePriority: {required: true, number:true}
       	
		        },
		    
		        messages: {
		        	photos: {required:"Photos can\'t be left blank"}
		        },
		    
		        submitHandler: function(form) {
		          form.submit();
		        }
		      });
		      
	      $(".btn-success").click(function(){ 
	          var html = $(".clone").html();
	          $(".increment").after(html);
	      });

	      $("body").on("click",".btn-danger",function(){ 
	          $(this).parents(".control-group").remove();
	      });

	    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop
