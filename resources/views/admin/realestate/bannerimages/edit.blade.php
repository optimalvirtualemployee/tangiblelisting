@extends('admin.common.innerdefaultauto')
@section('title', 'Update Banner')
@section('content')

<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update Banner</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="createimage" method="POST" action="{{ route('propertybanner.update', $data->id) }}">
									@method('PATCH')
									@csrf
									<div class="form-group">
										<div class="form-group">
											<label>Image Display Order:</label>
											<input class="form-control" type="number"  name="imagePriority" placeholder="Enter Image Display Order" value="{{$data->priority}}" required>
										</div>
									<div class="form-group">
                      <label>Status</label>
                      <select id ="status" name="status" class="form-control" required>
                        <option value="1" {{$data->status == "1" ? 'selected' : ''}}>Active</option>
                        <option value="0" {{$data->status == "0" ? 'selected' : ''}}>Inactive</option>
                      </select>
                    </div>
									</div>
									<button type="submit"
										class="btn btn-primary btn-user btn-block" value="Upload">Add</button>
								</form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#editcolour").validate({
    
        rules: {
        	imagePriority: {required: true, number:true}
        }
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop
