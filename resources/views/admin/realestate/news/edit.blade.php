@extends('admin.common.innerdefaultreal')
@section('title', 'Update News')
@section('content')

<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update News</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="editnews" method="POST" action="{{ route('propertynews.update', $news->id) }}">
                  @method('PATCH')
                  @csrf
                  <div class="form-group">
                    <input id="title" class="form-control form-control-user " type="text"  name="title" placeholder="Enter Title" value="{{$news->title}}">
                   </div>
                   <div class="form-group">
                    <input id="content" class="form-control form-control-user " type="text"  name="content" placeholder="Enter Content" value="{{$news->content}}">
                   </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#editno_of_bathrooms").validate({
    
        rules: {
          number_of_bathrooms: {required: true, number: true}
        },
    
        messages: {
          number_of_bathrooms: {required:"No. of Bathrooms can\'t be left blank",number:" Please enter numbers only"}
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop
