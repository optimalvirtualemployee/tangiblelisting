@extends('admin.common.innerdefaultauto')
@section('title', 'Update Comment')
@section('content')

<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update Comment</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="editcomment" method="POST" action="{{ route('automobilecomment.update', $comment_data->id) }}">
                  @method('PATCH')
                  @csrf
                  <div class="form-group">
                    <input id="comment" class="form-control form-control-user " type="text"  name="comment" placeholder="Enter Comment" value="{{ $comment_data->comment}}">
                    <input type ="hidden" id="listing_id" class="form-control form-control-user " type="text"  name="listing_id" value="{{ $comment_data->listing_id}}">
                  </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#editcomment").validate({
    
        rules: {
          comment: "required",
        },
    
        messages: {
          comment: "Comment can\'t be left blank",
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop
