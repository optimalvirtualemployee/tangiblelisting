@extends('admin.common.innerdefaultauto')
@section('title', 'Update No of Seats')
@section('content')
<style>
.error {
      color: red;
   }
</style>
<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update No of Seats</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="editno_of_seats" method="POST" action="{{ route('no_of_seats.update', $no_of_seats_data->id) }}">
                  @method('PATCH')
                  @csrf
                  <div class="form-group">
                    <input id="no_of_seats" class="form-control form-control-user " type="number" min="1" max="1000"  name="no_of_seats" placeholder="Enter No of Seats" value="{{ $no_of_seats_data->no_of_seats}}">
                  </div>
                  <div class="form-group">
                      <label>Status</label>
                      <select id ="status" name="status" class="form-control" required>
                        <option value="1" {{$no_of_seats_data->status == "1" ? 'selected' : ''}}>Active</option>
                        <option value="0" {{$no_of_seats_data->status == "0" ? 'selected' : ''}}>Inactive</option>
                      </select>
                    </div> 
                  <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
    	console.log('---------------------------');
      $("#editno_of_seats").validate({
        rules: {
        	no_of_seats: {required: true, number: true}
        },
    
        messages: {
        	no_of_seats: {required:"No. of Seats can\'t be left blank",number:" Please enter numbers only"}
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop
