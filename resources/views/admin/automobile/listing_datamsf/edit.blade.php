@extends('admin.common.innerdefaultauto')
@section('title', 'Update Automobile')
@section('content')
<style>
add css for multi step form
.wizard a,
.tabcontrol a
{
outline: 0;
}

.wizard ul,
.tabcontrol ul
{
list-style: none !important;
padding: 0;
margin: 0;
}

.wizard ul > li,
.tabcontrol ul > li
{
display: block;
padding: 0;
}

/ Accessibility /
.wizard > .steps .current-info,
.tabcontrol > .steps .current-info
{
position: absolute;
left: -999em;
}

.wizard > .content > .title,
.tabcontrol > .content > .title
{
position: absolute;
left: -999em;
}



/*
Wizard
*/

.wizard > .steps
{
position: relative;
display: block;
width: 100%;
}

.wizard.vertical > .steps
{
display: inline;
float: left;
width: 30%;
}

.wizard > .steps .number
{
background-color: #FFF;
color: #106db2;
border-radius: 100%;
padding-right: 3px;
padding-left: 5px;

}

.wizard > .steps > ul > li
{
width: 25%;
}

.wizard > .steps > ul > li,
.wizard > .actions > ul > li
{
float: left;
}

.wizard.vertical > .steps > ul > li
{
float: none;
width: 100%;
}

.wizard > .steps a,
.wizard > .steps a:hover,
.wizard > .steps a:active
{
display: block;
width: auto;
margin: 0 0.5em 0.5em;
padding: 1em 1em;
text-decoration: none;

-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
}

.wizard > .steps .disabled a,
.wizard > .steps .disabled a:hover,
.wizard > .steps .disabled a:active
{
background: #eee;
color: #aaa;
cursor: default;
}

.wizard > .steps .current a,
.wizard > .steps .current a:hover,
.wizard > .steps .current a:active
{
background: #fcfcfc;
color: #111;
cursor: default;
}

.wizard > .steps .done a,
.wizard > .steps .done a:hover,
.wizard > .steps .done a:active
{
background: #eeeeee;
color: #000;
}

.wizard > .steps .error a,
.wizard > .steps .error a:hover,
.wizard > .steps .error a:active
{
background: #eeeeee;
color: #000;
}

.wizard > .content
{
background: #eee;
display: block;
margin: 0.5em;
min-height: 25em;
overflow: scroll;
position: relative;
width: auto;

-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
}

.wizard.vertical > .content
{
display: inline;
float: left;
margin: 0 2.5% 0.5em 2.5%;
width: 65%;
}

.wizard > .content > .body
{
float: left;
position: absolute;
width: 95%;
height: 95%;
padding: 2.5%;
}

.wizard > .content > .body ul
{
list-style: disc !important;
}

.wizard > .content > .body ul > li
{
display: list-item;
}

.wizard > .content > .body > iframe
{
border: 0 none;
width: 100%;
height: 100%;
}

.wizard > .content > .body input
{
display: block;
border: 1px solid #ccc;
}

.wizard > .content > .body input[type="checkbox"]
{
display: inline-block;
}

.wizard > .content > .body input.error
{
border: 1px solid #fbc2c4;
color: #8a1f11;
}

.wizard > .content > .body label
{
display: inline;
margin-bottom: 0.5em;
}

.wizard > .content > .body label.error
{
color: #ff1e00;
display: inline;
font-size: 12px;
}

.wizard > .actions
{
position: relative;
display: block;
text-align: right;
width: 100%;
}

.wizard.vertical > .actions
{
display: inline;
float: right;
margin: 0 2.5%;
width: 95%;
}

.wizard > .actions > ul
{
display: inline-block;
text-align: right;
}

.wizard > .actions > ul > li
{
margin: 0 0.5em;
}

.wizard.vertical > .actions > ul > li
{
margin: 0 0 0 1em;
}

.wizard > .actions a,
.wizard > .actions a:hover,
.wizard > .actions a:active
{
background: #2184be;
color: #fff;
display: block;
padding: 0.5em 1em;
text-decoration: none;

-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
}

.wizard > .actions .disabled a,
.wizard > .actions .disabled a:hover,
.wizard > .actions .disabled a:active
{
background: #eee;
color: #aaa;
}

.wizard > .loading
{
}

.wizard > .loading .spinner
{
}

.wizard > .steps .current a, .wizard > .steps .current a:hover, .wizard > .steps .current a:active {
background: #106db2;
color: #fff;
cursor: default;
font-size: 16px;
font-weight: 700;
/ padding: 20px 10px; /
}

/*
Tabcontrol
*/

.tabcontrol > .steps
{
position: relative;
display: block;
width: 100%;
}

.tabcontrol > .steps > ul
{
position: relative;
margin: 6px 0 0 0;
top: 1px;
z-index: 1;
}

.tabcontrol > .steps > ul > li
{
float: left;
margin: 5px 2px 0 0;
padding: 1px;

-webkit-border-top-left-radius: 5px;
-webkit-border-top-right-radius: 5px;
-moz-border-radius-topleft: 5px;
-moz-border-radius-topright: 5px;
border-top-left-radius: 5px;
border-top-right-radius: 5px;
}

.tabcontrol > .steps > ul > li:hover
{
background: #edecec;
border: 1px solid #bbb;
padding: 0;
}

.tabcontrol > .steps > ul > li.current
{
background: #fff;
border: 1px solid #bbb;
border-bottom: 0 none;
padding: 0 0 1px 0;
margin-top: 0;
}

.tabcontrol > .steps > ul > li > a
{
color: #5f5f5f;
display: inline-block;
border: 0 none;
margin: 0;
padding: 10px 30px;
text-decoration: none;
}

.tabcontrol > .steps > ul > li > a:hover
{
text-decoration: none;
}

.tabcontrol > .steps > ul > li.current > a
{
padding: 15px 30px 10px 30px;
}

.tabcontrol > .content
{
position: relative;
display: inline-block;
width: 100%;
height: 35em;
overflow: hidden;
border-top: 1px solid #bbb;
padding-top: 20px;
}

.tabcontrol > .content > .body
{
float: left;
position: absolute;
width: 95%;
height: 95%;
padding: 2.5%;
}

.tabcontrol > .content > .body ul
{
list-style: disc !important;
}

.tabcontrol > .content > .body ul > li
{
display: list-item;
}

#contact input[type="text"],
#contact input[type="email"],
#contact input[type="tel"],
#contact input[type="url"],
#contact textarea,
#contact button[type="submit"] {
font: 400 12px/16px "Titillium Web", Helvetica, Arial, sans-serif;
}

#contact {
background: #F9F9F9;
padding: 25px;

}

#contact h3 {
display: block;
font-size: 30px;
font-weight: 300;
margin-bottom: 10px;
}

#contact h4 {
margin: 5px 0 15px;
display: block;
font-size: 13px;
font-weight: 400;
}

fieldset {
border: medium none !important;
margin: 0 0 10px;
min-width: 100%;
padding: 0;
width: 100%;
}

#contact input[type="text"],
#contact input[type="email"],
#contact input[type="tel"],
#contact input[type="url"],
#contact textarea {
width: 100%;
border: 1px solid #ccc;
background: #FFF;
margin: 0 0 5px;
padding: 10px;
}

#contact input[type="text"]:hover,
#contact input[type="email"]:hover,
#contact input[type="tel"]:hover,
#contact input[type="url"]:hover,
#contact textarea:hover {
-webkit-transition: border-color 0.3s ease-in-out;
-moz-transition: border-color 0.3s ease-in-out;
transition: border-color 0.3s ease-in-out;
border: 1px solid #aaa;
}

#contact textarea {
height: 100px;
max-width: 100%;
resize: none;
}

#contact button[type="submit"] {
cursor: pointer;
border: none;
background: #4CAF50;
color: #FFF;
margin: 0 0 5px;
padding: 10px;
font-size: 15px;
}

#contact button[type="submit"]:hover {
background: #43A047;
-webkit-transition: background 0.3s ease-in-out;
-moz-transition: background 0.3s ease-in-out;
transition: background-color 0.3s ease-in-out;
}

#contact button[type="submit"]:active {
box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.5);
}

.copyright {
text-align: center;
}

#contact input:focus,
#contact textarea:focus {
outline: 0;
border: 1px solid #aaa;
}



.steps > ul > li > a,
.actions li a {
padding: 10px;
text-decoration: none;
margin: 1px;
display: block;
color: #777;
}
.steps > ul > li,
.actions li {
list-style:none;
}
#loading-image{
  position:fixed;
  top:0px;
  right:0px;
  width:100%;
  height:100%;
  background-color:#666;
  background-image:url('/assets/img/Spinner-loader.gif');
  background-repeat:no-repeat;
  background-position:center;
  z-index:10000000;
  opacity:0.6;
}
span.current-info.audible {
  display: none;
}
</style>
<div class="container">
    <div class="row justify-content-center mt-5">
      <div class="" style="width: 100%;">
        <div class=""> 
         <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-12">
              <div class="">
                       <div id='loading-image' style='display:none'>
				</div>
                <div class="container mt-3">
@if (count($errors) > 0)
              <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                   @foreach ($errors->all() as $error)
                     <li>{{ $error }}</li>
                   @endforeach
                </ul>
              </div>
            @endif
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div id="standalone" class="container tab-pane active"><br>
                      <div class="container">
                        
                           <form id="updateproperty" action="{{ url('admin/automobileData/update',$automobile->id) }}"  method="POST" enctype="multipart/form-data">
                          @csrf
                          <div>
                            <h3>Automobile Data</h3>
                            <section>
                              <div class="form-group row mx-auto">
     
                                <!-- <div class="col-md-12 col-lg-4">
                                  <label>AD Title*</label>
                                  <input id="adTitle" class="form-control form-control-user required" type="text"  name="adTitle" placeholder="Enter AD Title" value="{{$automobile->ad_title}}">
                                    
                                </div> -->
                                <div class="col-md-12 col-lg-4">
                              <label>Status*</label>
                          <select id ="status" name="status" class="form-control required" >
                          <option value="1" {{$automobile->status == "1" ? 'selected' : ''}}>Active</option>
                        <option value="0" {{$automobile->status == "0" ? 'selected' : ''}}>Inactive</option>
                        </select>
                              </div>
                                <div class="col-md-12 col-lg-4">
                                  <label>Currency*</label>
                                  <select id="currency" class="form-control required"  name="currency">
                                      @if($currency_data ?? '')
                                    @foreach ($currency_data as $currency)
                                      <option @if($currencySelected->id ?? '') @if($currencySelected->id==$currency->id) selected @endif @endif value="{{$currency->id}}">{{$currency->currency_code}}</option>
                                    @endforeach
                                    @endif
                                    </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Country*</label>
                                <select id="countryId" class="form-control required"  name="countryId">
                                @foreach ($country_data as $country)
                                       <option @if($countrySelected->id ?? '') @if($countrySelected->id==$country->id) selected @endif @endif value="{{$country->id}}">{{$country->country_name}}</option>
                                     @endforeach
                                </select>
                              </div>
                            </div>
                            <div class="form-group row mx-auto">
                              <div class="col-md-12 col-lg-4">
                              <label>State:</label><select id="stateId"  name="stateId" class="form-control">
                                @foreach ($state_data as $state)
                                  <option @if($stateSelected->id ?? '') @if($stateSelected->id==$state->id) selected @endif @endif value="{{$state->id}}">{{$state->state_name != '' ? $state->state_name : 'Please Select State'}}</option>
                                @endforeach
                                </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                              <label>City*</label><select id="cityId" name="cityId" class="form-control">
                              <option value="{{$citySelected->id}}">{{$citySelected->city_name}}</option>
                             </select>
                              </div>
                               <div class="col-md-12 col-lg-4">
					<label>Make*</label><select id="make" class="form-control required"   name="make">
						@foreach ($makes as $make)
                           <option @if($makeSelected->id ?? '') @if($makeSelected->id==$make->id) selected @endif @endif value="{{$make->id}}">{{$make->automobile_brand_name}}</option>
                         @endforeach
                      </select>
                      </div>
                            </div>
                            <div class="form-group row mx-auto">
                              <div class="col-md-12 col-lg-4">
                                <label>Model*</label><select id="model" class="form-control required"   name="model">
						<option value="{{$modelSelected->id}}">{{$modelSelected->automobile_model_name}}</option>
                      </select>
                              </div>
                              <!-- <div class="col-md-12 col-lg-4">
                                <label>Price On Request</label>
					<select id="price_on_request" class="form-control required" name="price_on_request">
                        <option @if($automobile->price_on_request ?? '' ) @if($automobile->price_on_request==0) selected @endif @endif value="0">No</option>
                                  <option @if($automobile->price_on_request ?? '' ) @if($automobile->price_on_request==1) selected @endif @endif value="1">Yes</option>
                      </select>
                              </div> -->
                              
                              <div class="col-md-12 col-lg-4">
                         <label>price*</label><select id="price" class="form-control required"  name="price">
                         @foreach ($prices as $price)
                           <option @if($priceSelected->id ?? '') @if($priceSelected->id==$price->id) selected @endif @endif value="{{$price->id}}">{{$price->price}}</option>
                         @endforeach
                               
						</select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                              	@if($priceSelected->id == 3)
                                <label>Car Value*</label><input id="value" class="form-control form-control-user required" type="number" min="0" name="value" placeholder="Enter Car Value" value="{{$automobile->value}}" disabled>
                                @else
                                <label>Car Value*</label><input id="value" class="form-control form-control-user required" type="number" min="0" name="value" placeholder="Enter Car Value" value="{{$automobile->value}}">
                                @endif
                              </div>
                               
                            </div>
                            <div class="form-group row mx-auto">
                            <!-- <div class="col-md-12 col-lg-4">
                            @if($automobile->price_on_request == 0)
                               <label>price*</label><select id="price" class="form-control required"  name="price" >
                                @if($priceSelected)
						<option value="{{$priceSelected->id}}">{{$priceSelected->price}}</option>
						@else
						<option value="">Select Price</option>
						@endif
						@foreach ($prices as $price)
                           <option value="{{$price->id}}">{{$price->price}}</option>
                         @endforeach
                         @else
                         <label>price*</label><select id="price" class="form-control required"  name="price" disabled>
                                @if($priceSelected)
						<option value="{{$priceSelected->id}}">{{$priceSelected->price}}</option>
						@else
						<option value="">Select Price</option>
						@endif
						@foreach ($prices as $price)
                           <option value="{{$price->id}}">{{$price->price}}</option>
                         @endforeach                        
                         @endif
                      </select> 
                              </div> -->
                              <div class="col-md-12 col-lg-4">
                                <label>Fuel Type*</label><select id="fuel_type" class="form-control required"  name="fuel_type">
						@foreach ($fuel_types as $fuel_type)
                           <option @if($fuelTypeSelected->id ?? '') @if($fuelTypeSelected->id==$fuel_type->id) selected @endif @endif value="{{$fuel_type->id}}">{{$fuel_type->fuel_type}}</option>
                         @endforeach
                      </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>No Of Doors</label><select id="no_of_doors" class="form-control"  name="no_of_doors">
                               @if($noOfDoorsSelected)       
						<option value="{{$noOfDoorsSelected->id}}">{{$noOfDoorsSelected->no_of_doors}}</option>
						@else
						<option value="">Select No Of Doors</option>
						@endif
						@foreach ($no_of_doors as $no_of_door)
                           <option value="{{$no_of_door->id}}">{{$no_of_door->no_of_doors}}</option>
                         @endforeach
                      </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                               <label>Agent*</label>
                                <select id="agent" class="form-control required" name="agent">
                                  @foreach ($agents as $agent)
                                     <option @if($agentSelected->id ?? '') @if($agentSelected->id==$agent->id) selected @endif @endif value="{{$agent->id}}">{{$agent->id .'-'. $agent-> first_name .' '. $agent->last_name}}</option>
                                   @endforeach
                                </select>    
                              </div>
                            </div>
                            <div class="form-group row mx-auto">
                              <div class="col-md-12 col-lg-4">
                                <label>Body Type*</label><select id="body_type" class="form-control required"  name="body_type">
						@foreach ($body_types as $body_type)
                           <option @if($bodyTypeSelected->id ?? '') @if($bodyTypeSelected->id==$body_type->id) selected @endif @endif value="{{$body_type->id}}">{{$body_type->body_type}}</option>
                         @endforeach
                         </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Transmissioin*</label><select id="transmission" class="form-control required"  name="transmission">
						@foreach ($transmissions as $transmission)
                           <option @if($transmissionSelected->id ?? '') @if($transmissionSelected->id==$transmission->id) selected @endif @endif value="{{$transmission->id}}">{{$transmission->transmission}}</option>
                         @endforeach
                         </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Registration Place</label><input id="registrationPlace" class="form-control form-control-user" type="text"  name="registrationPlace" placeholder="Enter Registration Place" value="{{$automobile->registration_place}}">
                              </div>
                            </div>
                            <div class="form-group row mx-auto">
                              <div class="col-md-12 col-lg-4">
                                 <label>RHD/LHD*</label>
                      <select id="rhdorlhd" class="form-control required" name="rhdorlhd">
                        <option value="rhd" {{ $automobile->rhdorlhd == 'rhd'  ? 'selected' : ''}}>RHD</option>
                        <option value="lhd" {{ $automobile->rhdorlhd == 'lhd'  ? 'selected' : ''}}>LHD</option>
                      </select>
                              </div>
                               <div class="col-md-12 col-lg-4">
                                <label>Year*</label>
                                <select id="year" class="form-control required"  name="year" >
                                  @foreach ($year as $buildYear)
                                  <option @if($yearSelected->id ?? '') @if($yearSelected->id==$buildYear->id) selected @endif @endif value="{{$buildYear->id}}">{{$buildYear->build_year}}</option>
                                  @endforeach
                                </select>
                              </div>
                               <div class="col-md-12 col-lg-4">
                                <label>Condition*</label>
                      <select id="neworused" class="form-control required" name="neworused">
                        <option value="new" {{ $automobile->neworused == 'new'  ? 'selected' : ''}}>New</option>
                        <option value="used" {{ $automobile->neworused == 'used'  ? 'selected' : ''}}>Used</option>
                      </select>
                              </div>
                            </div>
                            <div class="form-group row mx-auto">
                            <div class="col-md-12 col-lg-4">
                                <label>Odometer*</label><input id="odometer" class="form-control form-control-user required" type="number"  name="odometer" placeholder="Enter Odometer Value" value="{{$automobile->odometer}}">
                              </div>
                              <div class="col-md-12 col-lg-4">
                              <label>Odometer Metric*</label>
                      <select id="metric" class="form-control required" name="metric">
                        <option value="km" {{ $automobile->metric == 'km'  ? 'selected' : ''}}>KM</option>
                        <option value="mi" {{ $automobile->metric == 'mi'  ? 'selected' : ''}}>Miles</option>
                      </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                              <label>Engine</label><input id="engine" class="form-control form-control-user" type="text"  name="engine" placeholder="Enter Engine" value="{{$automobile->engine}}">
                              </div>
                             </div> 
                             <div class="form-group row mx-auto">
                             <div class="col-md-12 col-lg-4">
                              <label>VIN Number*</label><input id="vin" class="form-control form-control-user required" type="text"  name="vin" placeholder="Enter VIN Number" value="{{$automobile->vin_number}}">
                              </div>
                              <div class="col-md-12 col-lg-4">
                              <label>Registrstion Number*</label><input id="regNum" class="form-control form-control-user required" type="text"  name="regNum" placeholder="Enter Registration Plate" value="{{$automobile->registration_number}}">
                              </div>
                             <div class="col-md-12 col-lg-4">
                                <label>Colour*</label><select id="colour" class="form-control required"  name="colour">
						@foreach ($colours as $colour)
                           <option @if($colourSelected->id ?? '') @if($colourSelected->id==$colour->id) selected @endif @endif value="{{$colour->id}}">{{$colour->colour}}</option>
                         @endforeach
                         </select>
                              </div>
                             </div>
                             <div class="form-group row mx-auto">
                             <div class="col-md-12 col-lg-4">
					<label>Interior Colour</label><select id="interior_colour" class="form-control"   name="interior_colour">
					@if($interiorColourSelected)       
						<option value="{{$interiorColourSelected->id}}">{{$interiorColourSelected->interior_colour}}</option>
						@else
						<option value="">Select Interior Color</option>
						@endif
						@foreach ($interior_colours as $interior_colour)
                           <option value="{{$interior_colour->id}}">{{$interior_colour->interior_colour}}</option>
                         @endforeach
                      </select>
                         </div>
                             <div class="col-md-12 col-lg-4">
                          <label class="form-control-label" for="timezone">Timezone</label>
                          <select id="timezone" class="form-control" required name="timezone">
                                @foreach ($timezone as $zone)
                                 <option @if($timezoneSelected->id ?? '') @if($timezoneSelected->id==$zone->id) selected @endif @endif value="{{$zone->id}}">{{$zone->name .' '. $zone->offset}}</option>
                               @endforeach
                          </select>
                      </div>
                             
                              </div>
                              <div class="form-group row mx-auto">
                              
                              </div>
                            </section>
                            <h3>Features</h3>
                            <section>
                              <div class="form-group row mx-auto">
                                <div class="table-responsive">
                                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                  <tbody id="features">
                                  <?php echo $html; ?>
                                  </tbody>
                                  </table>
                                </div>
                              </div>
                            </section>
                            <h3>Additional Features</h3>
                            <section>
                              <div class="form-group row mx-auto">
                                <div class="table-responsive">
                                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                  <tbody id="additionalFeatures">
                                  @if($additionalHtml)
                                  <?php echo $additionalHtml; ?>
                                  @endif
                                  <tr class = "increment">
                                  <td>
                                  <label>Additional Feature</label>
                                <input class="form-control form-control-user " type="text" maxLength="100" name="additional_feature[]" placeholder="Enter Feature" value="">
                                </td>
                                <td>
												<button class="btn btn-primary btn-success-feature" type="button">
													<i class="glyphicon glyphicon-plus"></i>Add
												</button>
                                </td>
                                  </tr>
                                  </tbody>
                                  </table>
                                </div>
                              </div>
                            </section>
                            <h3>Description</h3>
                            <section>
                                <div class="form-group row mx-auto">
                              <textarea cols="200" rows="10" id="comment" name="comment" required> 
                           @if($comment_data->comment ?? '') {{$comment_data->comment}} @endif
                         </textarea>
                                </div>     
                            </section>
                            <h3>Automobile Images</h3>
                            <section>
                              <div class="form-group row mx-auto">
                                 @foreach ($images_data as $data)
                                    <tr >
                                      <td>
                                        <img src="{{url('uploads/'.$data->filename)}}" id="img_{{$data->id}}" class="img-thumbnail" width="75" />
                                      </td>
                                      <td>
                                        <button type="button" class="imgDelete" id="imgDelete_{{$data->id}}">
                                          <i class="fas fa-trash-alt"></i>
                                        </button> &nbsp; &nbsp;
                                    </td>
                                    </tr>
                                  @endforeach
                                
                              </div>
                              <div class="form-group row mx-auto">
                                <div class="input-group control-group increment">
                                  <input type="file" name="photos[]" class="form-control" value="fileupload">
                                </div>
                                
                                <div class="clone hide" hidden>
                                  <div class="control-group input-group"
                                    style="margin-top: 10px">
                                    <input type="file" name="photos[]" class="form-control">
                                    <div class="input-group-btn">
                                    <button class="btn btn-primary  btn-danger" type="button">
                                        <i class="glyphicon glyphicon-remove"></i> Remove
                                      </button>
                                      </div>
                                  </div>
                                </div>
                                <div class="input-group-btn">
                                    <button class="btn btn-primary  btn-success" type="button">
                                      <i class="glyphicon glyphicon-plus"></i>Add
                                    </button>
                                  </div>
                                
                              </div>
                             
                              </br>
                            
                            </section>
                            
                            <h3>Damage Images</h3>
                            <section>
                              <div class="form-group row mx-auto">
                                 @foreach ($images_damage as $data)
                                    <tr >
                                      <td>
                                        <img src="{{url('uploads/'.$data->filename)}}" id="img_{{$data->id}}" class="w3-circle" />
                                      </td>
                                    </tr>
                                  @endforeach
                              </div>
                            </section>
                            
                            <h3>Security Images</h3>
                            <section>
                              <div class="form-group row mx-auto">
                                 @foreach ($security_data as $data)
                                    <tr >
                                      <td>
                                        <img src="{{url('uploads/'.$data->filename)}}" id="img_{{$data->id}}" class="w3-circle" alt="Alps" />
                                      </td>
                                    </tr>
                                  @endforeach
                                
                              </div>
                            
                            </section>
                              <h3>Listing Approval</h3>
                              <section>
                              <div class="form-group row mx-auto">
                                    <div class="col-md-12 col-lg-4">
                          <label>Listing Approval</label>
					<select id="listingApproval" class="form-control required" name="listingApproval">
                        <option @if($automobile->approval_status ?? '' ) @if($automobile->approval_status=='pending') selected @endif @endif value="pending">Pending</option>
                                  <option @if($automobile->approval_status ?? '' ) @if($automobile->approval_status=='approved') selected @endif @endif value="approved">Approved</option>
                      </select>
                                 
                          </select>
                      </div>
                                
                              </div>
                              </br>
                            
                            </section>
                          </div>
                        </form>
                      
                      </div>
                    </div>
<!--                     <div id="chain" class="container tab-pane fade col-md-6"><br>
                      <form id="contact" action="" autocomplete="on" method="POST" enctype="multipart/form-data">
                        @CSRF
                        <div class="col-md-12">
                          <input type="hidden" name="venue_type" value="1">
                          <label for="confirm">Venue Upload *</label>
                          <input type="file" class="form-control required"  name="venue_file" value="{{ old('venue_file') }}">
                        </div>
                        <div class="form-group download_sample">
                               <label for="name" class="mr-sm-2">Download Sample File</label><br>
                           <a href="{{url('assets/file/venue1.xlsx')}}" download><button class="btn btn-primary btn-sm"    type="button" id="dropdownMenuButton">Download Sample File</button></a>
                        </div>

                        <a href="#" style="margin-left:10px">
                          <button type="" class="btn btn-primary mt-4">Submit</button>
                        </a>
                        </form>
                      
                      </div>
                    </div>
<!--                     <div id="chain" class="container tab-pane fade col-md-6"><br>
                      <form id="contact" action="" autocomplete="on" method="POST" enctype="multipart/form-data">
                        @CSRF
                        <div class="col-md-12">
                          <input type="hidden" name="venue_type" value="1">
                          <label for="confirm">Venue Upload *</label>
                          <input type="file" class="form-control required"  name="venue_file" value="{{ old('venue_file') }}">
                        </div>
                        <div class="form-group download_sample">
                               <label for="name" class="mr-sm-2">Download Sample File</label><br>
                           <a href="{{url('assets/file/venue1.xlsx')}}" download><button class="btn btn-primary btn-sm"    type="button" id="dropdownMenuButton">Download Sample File</button></a>
                        </div>

                        <a href="#" style="margin-left:10px">
                          <button type="" class="btn btn-primary mt-4">Submit</button>
                        </a>
                      </form>
                    </div>  -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->

 
 
<script type="text/javascript">

  $(document).ready(function(){

var form = $("#updateproperty");

form.validate({
errorPlacement: function errorPlacement(error, element) { element.before(error); },

});

form.children("div").steps({
headerTag: "h3",
bodyTag: "section",
transitionEffect: "slideLeft",
autoFocus: true,
/*saveState: true,*/

onStepChanging: function (event, currentIndex, newIndex)
{
form.validate().settings.ignore = ":disabled,:hidden";
return form.valid();

},
onFinishing: function (event, currentIndex)
{

form.validate().settings.ignore = ":disabled";
return form.valid();
},
onFinished: function (event, currentIndex)
{

/*alert('are you sure you want to save venue.!!');*/
$('#updateproperty').submit();
}

});

  });

</script>
<script type="text/javascript">
  $(document).ready(function() {

	  $("#price").change(function(){ 
			var htmlcarPrice = '';
			var htmlPrice = '';
      if($(this).val() == 3){
      	htmlcarPrice += '@if($automobile->value != null)<input id="value" class="form-control form-control-user required" type="number"  min="0" name="value" placeholder="Enter Car Value" value="{{$automobile->value}}" disabled> @else <input id="value" class="form-control form-control-user required" type="number"  min="0" name="value" placeholder="Enter Car Value" value="{{$automobile->value}}" disabled> @endif';
  	  	/*htmlPrice = '@if($priceSelected != null)<select id="price" class="form-control required"  name="price" disabled> <option value="{{$priceSelected->id}}">{{$priceSelected->price}}</option> @else <select id="price" class="form-control required"  name="price" disabled> <option value="">Select Price</option> @endif';*/
			$("#value").replaceWith(htmlcarPrice);
			//$("#price").replaceWith(htmlPrice);	
         }else{
        	 htmlcarPrice += '@if($automobile->value != null)<input id="value" class="form-control form-control-user required" type="number"  min="0" name="value" placeholder="Enter Car Value" value="{{$automobile->value}}"> @else <input id="value" class="form-control form-control-user required" type="number"  min="0" name="value" placeholder="Enter Car Value" value="{{$automobile->value}}"> @endif';
        	 /*htmlPrice += '@if($priceSelected != null)<select id="price" class="form-control required"  name="price"> <option value="{{$priceSelected->id}}">{{$priceSelected->price}}</option> @else <select id="price" class="form-control required"  name="price"> <option value="">Select Price</option>@endif';*/
				$("#value").replaceWith(htmlcarPrice);
				//$("#price").replaceWith(htmlPrice);	
                 } 
      
  });
	  
      $(".imgDelete").click(function() {
        var ids = $(this).attr("id");
        var id = ids.split("_").pop();
        $('#loading-image').show();
        $.ajax({
          url: "/admin/deleteAutomobileImage/"+id,
          type: "GET",
          success : function(data){
                 $('#img_'+id).remove();     
                 $('#imgDelete_'+id).remove();
                 $('#loading-image').hide();               
            }
          });
        
      });
  });

   
    $(document).ready(function() {

    	$(".btn-success-feature").click(function(){ 
            var html = `<tr class = "control-group-feature">
                <td>
                <label>Additional Feature</label>
              <input class="form-control form-control-user " type="text" maxLength="100" name="additional_feature[]" placeholder="Enter Feature" value="">
              </td>
              <td>
              <button class="btn btn-primary  btn-danger btn-danger-feature" type="button">
				<i class="glyphicon glyphicon-remove"></i> Remove
			</button>
              </td>
                </tr>`;
			
                tableBody = $("table tbody");     
            	tableBody.append(html);
        });

        $("body").on("click",".btn-danger-feature",function(){ 
            $(this).parents(".control-group-feature").remove();
        });

    	
        $(".btn-success").click(function(){ 
            var html = $(".clone").html();
            $(".increment").after(html);
        });

        $("body").on("click",".btn-danger",function(){ 
            $(this).parents(".control-group").remove();
        });

        $("#countryId").change(function(){
        var countryId = $(this).val();
        $('#loading-image').show();
        $.ajax({
          url: "/admin/admincommon/getState",
          type: "POST",
          data: {countryId: countryId, _token: '{{csrf_token()}}' },
          dataType: 'json',
          success : function(data){
            var html = `<option value="">Select State</option>`;
            for (var i = 0; i < data.length; i++) {
                var id = data[i].id;
                var name = data[i].state_name;
                var option = `<option value="${data[i].id}">${data[i].state_name}</option>`;
                html += option;
              }
            $('#loading-image').hide();
              $('#stateId').html(html); 
            }
          });
        $('#loading-image').show();
        $.ajax({
			url: "/admin/admincommon/getCity",
			type: "POST",
			data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
			dataType: 'json',
			success : function(data){
				var html = `<option value="">Select City</option>`;
				for (var i = 0; i < data.length; i++) {
					  var id = data[i].id;
					  var name = data[i].city_name;
					  var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
					  html += option;
					}
				$('#loading-image').hide();
					$('#cityId').html(html);	
				}
			});
      });
        $("#stateId").change(function(){
        	console.log('inside state id');
        var stateId = $(this).val();
        $('#loading-image').show();
        $.ajax({
          url: "/admin/admincommon/getCity",
          type: "POST",
          data: {stateId: stateId, _token: '{{csrf_token()}}' },
          dataType: 'json',
          success : function(data){
            var html = `<option value="">Select City</option>`;
            for (var i = 0; i < data.length; i++) {
            	console.log('data-----------');
                var id = data[i].id;
                var name = data[i].city_name;
                var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
                html += option;
              }
            $('#loading-image').hide();
              $('#cityId').html(html);  
            }
          });
      });

        $("#make").change(function(){
    		var brandId = $(this).val();
    		$('#loading-image').show();
    		$.ajax({
    			url: "/admin/createautomobiledata/getModel",
    			type: "POST",
    			data: {brandId: brandId, _token: '{{csrf_token()}}' },
    			dataType: 'json',
    			success : function(data){
    				var html = `<option value="">Select Model</option>`;
    				for (var i = 0; i < data.length; i++) {
    					  var id = data[i].id;
    					  var name = data[i].city_name;
    					  var option = `<option value="${data[i].id}">${data[i].automobile_model_name}</option>`;
    					  html += option;
    					}
    				$('#loading-image').hide();
    					$('#model').html(html);	
    				}
    			});
    	});  

      });

</script>
<script src="https://cdn.ckeditor.com/4.5.6/standard/ckeditor.js"/>
@stop