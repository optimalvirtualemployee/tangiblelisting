@extends('admin.common.innerdefault')
@section('title', 'Update Function Panorma Date')
@section('content')
<style>
.error {
      color: red;
   }
</style>
<div class="container">
    <!-- Outer Row -->
    <div class="row justify-content-center">
      <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-8">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Update Function Panorma Date</h1>
                  </div>
                  @if($errors->any())
                    @foreach($errors->all() as $error)
                      <div class="alert alert-danger">
                    {{$error}}
                      </div>
                    @endforeach
                  @endif
                  <form class="user" id="update_Function" method="POST" action="{{ route('functionPanormaDate.update',$functions_data->id) }}">
                  @method('PATCH')  
                  @csrf
                  <div class="form-group">
                   <label>Panorma Date</label>
                          <select id ="panorma_date" name="panorma_date" class="form-control">
                     
                           <option value="1" {{$functions_data->panorma_date == "1" ? 'selected' : ''}}>Yes</option>
                           <option value="0" {{$functions_data->panorma_date == "0" ? 'selected' : ''}}>No</option>
                       
                        </select>
                    </div>
                    <div class="form-group">
                          <label>Status</label>
                          <select id ="status" name="status" class="form-control">
                     
                           <option value="1" {{$functions_data->status == "1" ? 'selected' : ''}}>Active</option>
                           <option value="0" {{$functions_data->status == "0" ? 'selected' : ''}}>Inactive</option>
                       
                        </select>
                        
                       </div>
                      <button type="submit" class="btn btn-primary btn-user btn-block">Add New</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------------ JS SCRIPT SECTION --------------------------------------->
  
<script type="text/javascript">
    $(document).ready(function(){
     
      $("#update_Function").validate({
        
        rules: {
         
          panorma_date: "required"
          },
        
        messages: {
          panorma_date: "Panorama Date can\'t be left blank"
        },
        
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>
<!------------------------------------------------ JS SCRIPT SECTION --------------------------------------->
@stop
