@extends('admin.common.innerdefault')
@section('title', 'Update Watch')
@section('content')
<style>
add css for multi step form
.wizard a,
.tabcontrol a
{
outline: 0;
}

.wizard ul,
.tabcontrol ul
{
list-style: none !important;
padding: 0;
margin: 0;
}

.wizard ul > li,
.tabcontrol ul > li
{
display: block;
padding: 0;
}

/ Accessibility /
.wizard > .steps .current-info,
.tabcontrol > .steps .current-info
{
position: absolute;
left: -999em;
}

.wizard > .content > .title,
.tabcontrol > .content > .title
{
position: absolute;
left: -999em;
}



/*
Wizard
*/

.wizard > .steps
{
position: relative;
display: block;
width: 100%;
}

.wizard.vertical > .steps
{
display: inline;
float: left;
width: 30%;
}

.wizard > .steps .number
{
background-color: #FFF;
color: #106db2;
border-radius: 100%;
padding-right: 3px;
padding-left: 5px;

}

.wizard > .steps > ul > li
{
width: 25%;
}

.wizard > .steps > ul > li,
.wizard > .actions > ul > li
{
float: left;
}

.wizard.vertical > .steps > ul > li
{
float: none;
width: 100%;
}

.wizard > .steps a,
.wizard > .steps a:hover,
.wizard > .steps a:active
{
display: block;
width: auto;
margin: 0 0.5em 0.5em;
padding: 1em 1em;
text-decoration: none;

-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
}

.wizard > .steps .disabled a,
.wizard > .steps .disabled a:hover,
.wizard > .steps .disabled a:active
{
background: #eee;
color: #aaa;
cursor: default;
}

.wizard > .steps .current a,
.wizard > .steps .current a:hover,
.wizard > .steps .current a:active
{
background: #fcfcfc;
color: #111;
cursor: default;
}

.wizard > .steps .done a,
.wizard > .steps .done a:hover,
.wizard > .steps .done a:active
{
background: #eeeeee;
color: #000;
}

.wizard > .steps .error a,
.wizard > .steps .error a:hover,
.wizard > .steps .error a:active
{
background: #eeeeee;
color: #000;
}

.wizard > .content
{
background: #eee;
display: block;
margin: 0.5em;
min-height: 25em;
overflow: scroll;
position: relative;
width: auto;

-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
}

.wizard.vertical > .content
{
display: inline;
float: left;
margin: 0 2.5% 0.5em 2.5%;
width: 65%;
}

.wizard > .content > .body
{
float: left;
position: absolute;
width: 95%;
height: 95%;
padding: 2.5%;
}

.wizard > .content > .body ul
{
list-style: disc !important;
}

.wizard > .content > .body ul > li
{
display: list-item;
}

.wizard > .content > .body > iframe
{
border: 0 none;
width: 100%;
height: 100%;
}

.wizard > .content > .body input
{
display: block;
border: 1px solid #ccc;
}

.wizard > .content > .body input[type="checkbox"]
{
display: inline-block;
}

.wizard > .content > .body input.error
{
background: rgb(251, 227, 228);
border: 1px solid #fbc2c4;
color: #8a1f11;
}

.wizard > .content > .body label
{
display: inline;
margin-bottom: 0.5em;
}

.wizard > .content > .body label.error
{
color: #ff1e00;
display: inline;
font-size: 12px;
}

.wizard > .actions
{
position: relative;
display: block;
text-align: right;
width: 100%;
}

.wizard.vertical > .actions
{
display: inline;
float: right;
margin: 0 2.5%;
width: 95%;
}

.wizard > .actions > ul
{
display: inline-block;
text-align: right;
}

.wizard > .actions > ul > li
{
margin: 0 0.5em;
}

.wizard.vertical > .actions > ul > li
{
margin: 0 0 0 1em;
}

.wizard > .actions a,
.wizard > .actions a:hover,
.wizard > .actions a:active
{
background: #2184be;
color: #fff;
display: block;
padding: 0.5em 1em;
text-decoration: none;

-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
}

.wizard > .actions .disabled a,
.wizard > .actions .disabled a:hover,
.wizard > .actions .disabled a:active
{
background: #eee;
color: #aaa;
}

.wizard > .loading
{
}

.wizard > .loading .spinner
{
}

.wizard > .steps .current a, .wizard > .steps .current a:hover, .wizard > .steps .current a:active {
background: #106db2;
color: #fff;
cursor: default;
/*font-size: 16px;
font-weight: 700;*/
/ padding: 20px 10px; /
}

/*
Tabcontrol
*/

.tabcontrol > .steps
{
position: relative;
display: block;
width: 100%;
}

.tabcontrol > .steps > ul
{
position: relative;
margin: 6px 0 0 0;
top: 1px;
z-index: 1;
}

.tabcontrol > .steps > ul > li
{
float: left;
margin: 5px 2px 0 0;
padding: 1px;

-webkit-border-top-left-radius: 5px;
-webkit-border-top-right-radius: 5px;
-moz-border-radius-topleft: 5px;
-moz-border-radius-topright: 5px;
border-top-left-radius: 5px;
border-top-right-radius: 5px;
}

.tabcontrol > .steps > ul > li:hover
{
background: #edecec;
border: 1px solid #bbb;
padding: 0;
}

.tabcontrol > .steps > ul > li.current
{
background: #fff;
border: 1px solid #bbb;
border-bottom: 0 none;
padding: 0 0 1px 0;
margin-top: 0;
}

.tabcontrol > .steps > ul > li > a
{
color: #5f5f5f;
display: inline-block;
border: 0 none;
margin: 0;
padding: 10px 30px;
text-decoration: none;
}

.tabcontrol > .steps > ul > li > a:hover
{
text-decoration: none;
}

.tabcontrol > .steps > ul > li.current > a
{
padding: 15px 30px 10px 30px;
}

.tabcontrol > .content
{
position: relative;
display: inline-block;
width: 100%;
height: 35em;
overflow: hidden;
border-top: 1px solid #bbb;
padding-top: 20px;
}

.tabcontrol > .content > .body
{
float: left;
position: absolute;
width: 95%;
height: 95%;
padding: 2.5%;
}

.tabcontrol > .content > .body ul
{
list-style: disc !important;
}

.tabcontrol > .content > .body ul > li
{
display: list-item;
}

#contact input[type="text"],
#contact input[type="email"],
#contact input[type="tel"],
#contact input[type="url"],
#contact textarea,
#contact button[type="submit"] {
font: 400 12px/16px "Titillium Web", Helvetica, Arial, sans-serif;
}

#contact {
background: #F9F9F9;
padding: 25px;

}

#contact h3 {
display: block;
font-size: 30px;
font-weight: 300;
margin-bottom: 10px;
}

#contact h4 {
margin: 5px 0 15px;
display: block;
font-size: 13px;
font-weight: 400;
}

fieldset {
border: medium none !important;
margin: 0 0 10px;
min-width: 100%;
padding: 0;
width: 100%;
}

#contact input[type="text"],
#contact input[type="email"],
#contact input[type="tel"],
#contact input[type="url"],
#contact textarea {
width: 100%;
border: 1px solid #ccc;
background: #FFF;
margin: 0 0 5px;
padding: 10px;
}

#contact input[type="text"]:hover,
#contact input[type="email"]:hover,
#contact input[type="tel"]:hover,
#contact input[type="url"]:hover,
#contact textarea:hover {
-webkit-transition: border-color 0.3s ease-in-out;
-moz-transition: border-color 0.3s ease-in-out;
transition: border-color 0.3s ease-in-out;
border: 1px solid #aaa;
}

#contact textarea {
height: 100px;
max-width: 100%;
resize: none;
}

#contact button[type="submit"] {
cursor: pointer;
border: none;
background: #4CAF50;
color: #FFF;
margin: 0 0 5px;
padding: 10px;
font-size: 15px;
}

#contact button[type="submit"]:hover {
background: #43A047;
-webkit-transition: background 0.3s ease-in-out;
-moz-transition: background 0.3s ease-in-out;
transition: background-color 0.3s ease-in-out;
}

#contact button[type="submit"]:active {
box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.5);
}

.copyright {
text-align: center;
}

#contact input:focus,
#contact textarea:focus {
outline: 0;
border: 1px solid #aaa;
}



.steps > ul > li > a,
.actions li a {
padding: 10px;
text-decoration: none;
margin: 1px;
display: block;
color: #777;
}
.steps > ul > li,
.actions li {
list-style:none;
}
#loading-image{
  position:fixed;
  top:0px;
  right:0px;
  width:100%;
  height:100%;
  background-color:#666;
  background-image:url('/assets/img/Spinner-loader.gif');
  background-repeat:no-repeat;
  background-position:center;
  z-index:10000000;
  opacity:0.6;
}
span.current-info.audible {
  display: none;
}
</style>
<div class="container-fluid">
    <div class="row justify-content-center mt-5">
      <div class="" style="width: 100%;">
        <div class=""> 
         <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-12">
              <div class="">
                        <div id='loading-image' style='display:none'>
				</div>
                <div class="container mt-3">
@if (count($errors) > 0)
              <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                   @foreach ($errors->all() as $error)
                     <li>{{ $error }}</li>
                   @endforeach
                </ul>
              </div>
            @endif
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div id="standalone" class="tab-pane active"><br>
                      <div class="container-tab">
                        
                           <form id="updateproperty" action="{{ url('admin/watchData/update',$watch->id) }}"  method="POST" enctype="multipart/form-data">
                          @csrf
                          <div>
                            <h3>Watch Data</h3>
                            <section>
                              <div class="form-group row mx-auto">
     
                                <!-- <div class="col-md-12 col-lg-4">
                                  <label>AD Title*</label><input id="adTitle" class="form-control form-control-user required" type="text"  name="adTitle" placeholder="Enter AD Title" value="{{ $watch->ad_title}}">
                                    
                                </div> -->
                                <div class="col-md-12 col-lg-4">
                               <label>Agent*</label>
                                <select id="agent" class="form-control required" name="agent">
                                  @foreach ($agents as $agent)
                                     <option @if($agentSelected->id ?? '') @if($agentSelected->id==$agent->id) selected @endif @endif value="{{$agent->id}}">{{$agent->id .'-'. $agent-> first_name .' '. $agent->last_name}}</option>
                                   @endforeach
                                </select>    
                              </div>
                                <div class="col-md-12 col-lg-4">
                                  <label>Currency*</label>
                                  <select id="currency" class="form-control required" name="currency">
                                    <option value="">--Select Currency--</option>
                                    @if($currency_data ?? '')
                                    @foreach ($currency_data as $currency)
                                      <option @if($currencySelected->id ?? '') @if($currencySelected->id==$currency->id) selected @endif @endif value="{{$currency->id}}">{{$currency->currency_code}}</option>
                                    @endforeach
                                    @endif
                                    </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Country*</label>
                                <select id="countryId" class="form-control required" name="countryId">
                                    @foreach ($country_data as $country)
                                       <option @if($countrySelected->id ?? '') @if($countrySelected->id==$country->id) selected @endif @endif value="{{$country->id}}">{{$country->country_name}}</option>
                                     @endforeach
                                </select>
                              </div>
                            </div>
                            <div class="form-group row mx-auto">
                              <div class="col-md-12 col-lg-4">
                                <label>State:</label>
                                <select id="stateId"  name="stateId" class="form-control required" >
                                @foreach ($state_data as $state)
                                  <option @if($stateSelected->id ?? '') @if($stateSelected->id==$state->id) selected @endif @endif value="{{$state->id}}">{{$state->state_name != '' ? $state->state_name : 'Please Select State'}}</option>
                                @endforeach
                                </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>City*</label>
                                <select id="cityId" name="cityId" class="form-control required">
                                   @foreach ($city_data as $city)
                                  <option @if($citySelected->id ?? '') @if($citySelected->id==$city->id) selected @endif @endif value="{{$city->id}}">{{$city->city_name}}</option>
                                @endforeach
                                </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Brand*</label><select id="brand" class="form-control required" type="text"  name="brand" placeholder="Enter Brand" value="" >
					<option value="{{$brandSelected->id}}">{{$brandSelected->watch_brand_name}}</option>
						@foreach ($brands as $brand)
                           <option value="{{$brand->id}}">{{$brand->watch_brand_name}}</option>
                         @endforeach
                      </select>
                              </div>
                            </div>
                            <div class="form-group row mx-auto">
                            <div class="col-md-12 col-lg-4">
                                <label>Model*</label><select id="model" class="form-control required"   name="model" >
						<option value="{{$modelSelected->id}}">{{$modelSelected->watch_model_name}}</option>
                      </select>  
                              </div>
                              
                              <!-- <div class="col-md-12 col-lg-4">
                                <label>Price On Request</label>
					<select id="price_on_request" class="form-control required" name="price_on_request">
                        <option @if($watch->price_on_request ?? '' ) @if($watch->price_on_request==0) selected @endif @endif value="0">No</option>
                                  <option @if($watch->price_on_request ?? '' ) @if($watch->price_on_request==1) selected @endif @endif value="1">Yes</option>
                      </select>
                              </div> -->
                              <div class="col-md-12 col-lg-4">
                         <label>price*</label><select id="price" class="form-control required"  name="price">
                         @foreach ($prices as $price)
                           <option @if($priceSelected->id ?? '') @if($priceSelected->id==$price->id) selected @endif @endif value="{{$price->id}}">{{$price->price}}</option>
                         @endforeach
                               
						</select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                              	@if($priceSelected->id == '3')
                                <label>Watch Price*</label><input id="watch_price" class="form-control form-control-user required" type="number" min="0"  name="watch_price" placeholder="Enter Watch Price" value="{{$watch->watch_price}}" disabled>
                                @else
                                <label>Watch Price*</label><input id="watch_price" class="form-control form-control-user required" type="number" min="0"  name="watch_price" placeholder="Enter Watch Price" value="{{$watch->watch_price}}" >
                                @endif
                              </div>
                                
                            </div>
                            <div class="form-group row mx-auto">
                              
                              <div class="col-md-12 col-lg-4">
                                <label>Case Diameter</label><select id="case_diameter" class="form-control"  name="case_diameter" >
                         @if($caseDiameterSelected)       
						<option value="{{$caseDiameterSelected->id}}">{{$caseDiameterSelected->case_diameter}}</option>
						@else
						<option value="">Select Case Diameter</option>
						@endif
						@foreach ($case_diameters as $case_diameter)
                           <option value="{{$case_diameter->id}}">{{$case_diameter->case_diameter}}</option>
                         @endforeach
                      </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Movement</label><select id="movement" class="form-control"  name="movement" >
                                @if($movementSelected)
						<option value="{{$movementSelected->id}}">{{$movementSelected->movement}}</option>
						@else
						<option value="">Select Movement</option>
						@endif
						@foreach ($movements as $movement)
                           <option value="{{$movement->id}}">{{$movement->movement}}</option>
                         @endforeach
                         </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Inclusions*</label><select id="inclusions" class="form-control"  name="inclusions" >
						@if($inclusionSelected)
						<option value="{{$inclusionSelected->id}}">{{$inclusionSelected->inclusion}}</option>
						@else
						<option value="">Select Type</option>
						@endif
						@foreach ($inclusions_data as $inclusion)
                           <option value="{{$inclusion->id}}">{{$inclusion->inclusion}}</option>
                         @endforeach
                         </select>
                              </div>
                            </div>
                            <div class="form-group row mx-auto">
                              
                              <div class="col-md-12 col-lg-4">
                                <label>Type</label><select id="type" class="form-control"  name="type" >
						@if($typeSelected)
						<option value="{{$typeSelected->id}}">{{$typeSelected->watch_type}}</option>
						@else
						<option value="">Select Type</option>
						@endif
						@foreach ($types as $type)
                           <option value="{{$type->id}}">{{$type->watch_type}}</option>
                         @endforeach
                         </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Year Of Manufature</label><select id="yom" class="form-control"  name="yom" >
                        @if($year_of_manufactureSelected)        
						<option value="{{$year_of_manufactureSelected->id}}">{{$year_of_manufactureSelected->year_of_manufacture}}</option>
						@else
						<option value="">Select Year Of Manufacture</option>
						@endif
						@foreach ($year_of_manufactures as $year_of_manufacture)
                           <option value="{{$year_of_manufacture->id}}">{{$year_of_manufacture->year_of_manufacture}}</option>
                         @endforeach
                         </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                 <label>Gender*</label><select id="gender" class="form-control required"  name="gender" >
						<option value="{{$genderSelected->id}}">{{$genderSelected->gender}}</option>
						@foreach ($genders as $gender)
                           <option value="{{$gender->id}}">{{$gender->gender}}</option>
                         @endforeach
                         </select>
                              </div>
                            </div>
                            <div class="form-group row mx-auto">
                              
                               
                               <div class="col-md-12 col-lg-4">
                                <label>Reference Number*</label><input id="referenceNo" class="form-control form-control-user required" type="text"  name="referenceNo" placeholder="Enter Reference Number" value="{{$watch->reference_no}}" >
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Watch Condition*</label>
                                <select id ="watchCondition" name="watchCondition" class="form-control required" >
                          			<option value="1" {{$watch->watch_condition == "New" ? 'selected' : ''}}>New</option>
                        			<option value="0" {{$watch->watch_condition == "Used" ? 'selected' : ''}}>Used</option>
                        		</select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                          <label class="form-control-label" for="timezone">Timezone</label>
                          <select id="timezone" class="form-control" required name="timezone">
                                @foreach ($timezone as $zone)
                                 <option @if($timezoneSelected->id ?? '') @if($timezoneSelected->id==$zone->id) selected @endif @endif value="{{$zone->id}}">{{$zone->name .' '. $zone->offset}}</option>
                               @endforeach
                          </select>
                      </div> 
                            </div>
                            <div class="form-group row mx-auto">
                                
                              <div class="col-md-12 col-lg-4">
                              <label>Status*</label>
                          <select id ="status" name="status" class="form-control required" >
                          <option value="1" {{$watch->status == "1" ? 'selected' : ''}}>Active</option>
                        <option value="0" {{$watch->status == "0" ? 'selected' : ''}}>Inactive</option>
                        </select>
                              </div>
                              
                             </div>
                            </section>

                            <h3>Additional Info</h3>
                            <section>
                            <div class="form-group row mx-auto">
                              <div class="col-md-12 col-lg-4">
                                <label>Power Reserve*</label><select id="powerReserve" class="form-control required"  name="powerReserve" >
					<?php /*	<option value="{{$powerReserveSelected->id}}">{{$powerReserveSelected->power_reserve}}</option> */?>
						@foreach ($power_reserve as $power_reserve)

            
                           <option value="{{$power_reserve->id}}"  <?php if (isset($powerReserveSelected->id)) {
                                      if ($powerReserveSelected->id ==$power_reserve->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$power_reserve->power_reserve}}</option>
                         @endforeach
                      </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Dial Color*</label><select id="dialColor" class="form-control required"  name="dialColor" >
                                <?php /* ?>		<option value="{{$dialColorSelected->id}}">{{$dialColorSelected->dial_color}}</option> */ ?>
						@foreach ($dial_color as $dial_color)
                           <option value="{{$dial_color->id}}" <?php if (isset($dialColorSelected->id)) {
                                      if ($dialColorSelected->id == $dial_color->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$dial_color->dial_color}}</option>
                         @endforeach
                      </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Bracelet Material*</label><select id="braceletMaterial" class="form-control required"  name="braceletMaterial" >
                                <?php /* ?>	<option value="{{$braceletMaterialSelected->id}}" >{{$braceletMaterialSelected->bracelet_material}}</option> */ ?>
						@foreach ($bracelet_material as $bracelet_material)
                           <option value="{{$bracelet_material->id}}" <?php if (isset($braceletMaterialSelected->id)) {
                                      if ($braceletMaterialSelected->idd == $bracelet_material->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$bracelet_material->bracelet_material}}</option>
                         @endforeach
                      </select>
                              </div>
                            </div>
                            <div class="form-group row mx-auto">
                              <div class="col-md-12 col-lg-4">
                                <label>Bezel Material*</label><select id="bezelMaterial" class="form-control required"  name="bezelMaterial" >
                                <?php /* ?>	<option value="{{$bezelMaterialSelected->id}}">{{$bezelMaterialSelected->bezel_material}}</option> */ ?>
						@foreach ($bezel_material as $bezel_material)
                           <option value="{{$bezel_material->id}}" <?php if (isset($$bezelMaterialSelected->id)) {
                                      if ($bezelMaterialSelected->id == $bezel_material->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$bezel_material->bezel_material}}</option>
                         @endforeach
                      </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Bracelet Color*</label><select id="braceletColor" class="form-control required"  name="braceletColor" >
                                <?php /* ?>	<option value="{{$braceletColorSelected->id}}">{{$braceletColorSelected->bracelet_color}}</option> */ ?>
						@foreach ($bracelet_color as $bracelet_color)
                           <option value="{{$bracelet_color->id}}" <?php if (isset($braceletColorSelected->id)) {
                                      if ($braceletColorSelected->id == $bracelet_color->i) {
                                      echo "selected";
                                      }
                                      }?>>{{$bracelet_color->bracelet_color}}</option>
                         @endforeach
                      </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Type Of Clasp*</label><select id="typeOfClasp" class="form-control required"  name="typeOfClasp" >
                                <?php /* ?>		<option value="{{$typeOfClaspSelected->id}}">{{$typeOfClaspSelected->type_of_clasp}}</option> */ ?>
						@foreach ($type_of_clasp as $type_of_clasp)
                           <option value="{{$type_of_clasp->id}}" <?php if (isset($typeOfClaspSelected->id)) {
                                      if ($typeOfClaspSelected->id == $type_of_clasp->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$type_of_clasp->type_of_clasp}}</option>
                         @endforeach
                      </select>
                              </div>
                            </div>
                            <div class="form-group row mx-auto">
                              <div class="col-md-12 col-lg-4">
                                <label>Clasp Material*</label><select id="claspMaterial" class="form-control required"  name="claspMaterial" >
                                <?php /* ?>	<option value="{{$claspMaterialSelected->id}}">{{$claspMaterialSelected->clasp_material}}</option> */ ?>
						@foreach ($clasp_material as $clasp_material)
                           <option value="{{$clasp_material->id}}" <?php if (isset($claspMaterialSelected->id)) {
                                      if ($claspMaterialSelected->id == $clasp_material->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$clasp_material->clasp_material}}</option>
                         @endforeach
                      </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Case Material*</label><select id="caseMaterial" class="form-control required"  name="caseMaterial" >
                                <?php /* ?>	<option value="{{$caseMaterialSelected->id}}">{{$caseMaterialSelected->case_material}}</option> */ ?>
						@foreach ($case_material as $case_material)
                           <option value="{{$case_material->id}}" <?php if (isset($caseMaterialSelected->id)) {
                                      if ($caseMaterialSelected->id == $case_material->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$case_material->case_material}}</option>
                         @endforeach
                      </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Case MM*</label><select id="caseMM" class="form-control required"  name="caseMM" >
                                <?php /* ?>			<option value="{{$caseMMSelected->id}}">{{$caseMMSelected->case_mm}}</option> */ ?>
						@foreach ($case_mm as $case_mm)
                           <option value="{{$case_mm->id}}" <?php if (isset($caseMMSelected->id)) {
                                      if ($caseMMSelected->id == $case_mm->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$case_mm->case_mm}}</option>
                         @endforeach
                      </select>
                              </div>
                            </div>
                            <div class="form-group row mx-auto">
                              <div class="col-md-12 col-lg-4">
                                <label>Water Resistance Depth*</label><select id="wrd" class="form-control required"  name="wrd" >
                                <?php /* ?>		<option value="{{$wrdSelected->id}}">{{$wrdSelected->water_resistant_depth}}</option> */ ?>
						@foreach ($wrd as $wrd)
                           <option value="{{$wrd->id}}" <?php if (isset($wrdSelected->id)) {
                                      if ($wrdSelected->id == $wrd->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$wrd->water_resistant_depth}}</option>
                         @endforeach
                      </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Glass Type*</label><select id="glassType" class="form-control required"  name="glassType" >
					<?php /* ?>	<option value="{{$glassTypeSelected->id}}">{{$glassTypeSelected->glass_type}}</option> */?>
						@foreach ($glass_type as $glass_type)
                           <option value="{{$glass_type->id}}" <?php if (isset($glassTypeSelected->id)) {
                                      if ($glassTypeSelected->id == $glass_type->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$glass_type->glass_type}}</option>
                         @endforeach
                      </select>
                              </div>
                            </div>
                            </section>
                            <h3>Features</h3>
                            <section>
                              <div class="form-group row mx-auto">
                                <div class="table-responsive">
                                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                  <tbody id="features">
                                  <?php echo $html; ?>
                                  </tbody>
                                  </table>
                                </div>
                              </div>
                            </section>
                            <h3>Additional Features</h3>
                            <section>
                              <div class="form-group row mx-auto">
                                <div class="table-responsive">
                                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                  <tbody id="additionalFeatures">
                                  @if($additionalHtml)
                                  <?php echo $additionalHtml; ?>
                                  @endif
                                  <tr class = "increment">
                                  <td>
                                  <label>Additional Feature</label>
                                <input class="form-control form-control-user " type="text" maxLength="100" name="additional_feature[]" placeholder="Enter Feature" value="">
                                </td>
                                <td>
												<button class="btn btn-primary btn-success-feature" type="button">
													<i class="glyphicon glyphicon-plus"></i>Add
												</button>
                                </td>
                                  </tr>
                                  </tbody>
                                  </table>
                                </div>
                              </div>
                            </section>
                            <h3>Description</h3>
                            <section>
                                <div class="form-group row mx-auto">
                              <textarea cols="200" rows="10" id="comment" name="comment" required> 
                           @if($comment_data->comment ?? '') {{$comment_data->comment}} @endif
                         </textarea>
                                </div>     
                            </section>
                            <h3>Watch Images</h3>
                            <section>
                              <div class="form-group row mx-auto">
                                 @foreach ($images_data as $data)
                                    <tr >
                                      <td>
                                        <img src="{{url('uploads/'.$data->filename)}}" id="img_{{$data->id}}" class="img-thumbnail" width="75" />
                                      </td>
                                      <td>
                                        <button type="button" class="imgDelete" id="imgDelete_{{$data->id}}">
                                          <i class="fas fa-trash-alt"></i>
                                        </button> &nbsp; &nbsp;
                                    </td>
                                    </tr>
                                  @endforeach
                                
                              </div>
                              <div class="form-group row mx-auto">
                                <div class="input-group control-group increment">
                                  <input type="file" name="photos[]" class="form-control" value="fileupload">
                                </div>
                                
                                <div class="clone hide" hidden>
                                  <div class="control-group input-group"
                                    style="margin-top: 10px">
                                    <input type="file" name="photos[]" class="form-control">
                                    <div class="input-group-btn">
                                    <button class="btn btn-primary  btn-danger" type="button">
                                        <i class="glyphicon glyphicon-remove"></i> Remove
                                      </button>
                                      </div>
                                  </div>
                                </div>
                                <div class="input-group-btn">
                                    <button class="btn btn-primary  btn-success" type="button">
                                      <i class="glyphicon glyphicon-plus"></i>Add
                                    </button>
                                  </div>
                                
                              </div>
                             
                              </br>
                            
                            </section>
                            
                            <h3>Damage Images</h3>
                              <section>
                              <div class="form-group row mx-auto">
                                 @foreach ($images_damages as $data)
                                    <tr >
                                      <br>
                                      <td>
                                        <img src="{{url('uploads/'.$data->filename)}}" id="img_{{$data->id}}" class="w3-circle" alt="Alps"  />
                                      </td>
                                    </tr>
                                  @endforeach
                                
                              </div>
                              </br>
                            
                            </section>
                            
                              <h3>Security Images</h3>
                              <section>
                              <div class="form-group row mx-auto">
                                 @foreach ($security_images as $data)
                                    <tr >
                                      <td>
                                      <p>Security-Time <time> {{ $data->security_time}}</time></p>
                                      </td>
                                      <br>
                                      <td>
                                        <img src="{{url('uploads/'.$data->filename)}}" id="img_{{$data->id}}" class="w3-circle" alt="Alps"  />
                                      </td>
                                    </tr>
                                  @endforeach
                                
                              </div>
                              </br>
                            
                            </section>
                              <h3>Document</h3>
                              <section>
                              <div class="form-group row mx-auto">

                                <p>Document</p2>

                                <ul style="margin-top: 49px;">
                                @foreach ($document as $data)
                                <li><a href="{{url('uploads/'.$data->filename)}}" target="_blank">View document </a></li>

                                <?php /* ?> 
                                <tr >
                                <td>
                                <?php
                                $ext = pathinfo($data->filename, PATHINFO_EXTENSION); 
                                if($ext == 'pdf' || $ext == 'docx'){
                                ?>                                      
                                <iframe src="{{url('uploads/'.$data->filename)}}" width="100%" height="500px"></iframe> 
                                <?php }else{ ?>
                                <img src="{{url('uploads/'.$data->filename)}}" id="img_{{$data->id}}" class="w3-circle" alt="Alps"  />
                                <?php } 
                                ?> 
                                </td>
                                </tr>
                                <?php */ ?>
                                  @endforeach
                                </ul>  
                              </div>
                              </br>
                            
                            </section>

                            <h3>Listing Approval</h3>
                              <section>
                              <div class="form-group row mx-auto">
                                    <div class="col-md-12 col-lg-4">
                          <label>Listing Approval</label>
					<select id="listingApproval" class="form-control required" name="listingApproval">
                        <option @if($watch->approval_status ?? '' ) @if($watch->approval_status=='pending') selected @endif @endif value="pending">Pending</option>
                                  <option @if($watch->approval_status ?? '' ) @if($watch->approval_status=='approved') selected @endif @endif value="approved">Approved</option>
                      </select>
                                 
                          </select>
                      </div>
                                
                              </div>
                              </br>
                            
                            </section>
                          </div>
                        </form>
                      
                      </div>
                    </div>
<!--                     <div id="chain" class="container tab-pane fade col-md-6"><br>
                      <form id="contact" action="" autocomplete="on" method="POST" enctype="multipart/form-data">
                        @CSRF
                        <div class="col-md-12">
                          <input type="hidden" name="venue_type" value="1">
                          <label for="confirm">Venue Upload *</label>
                          <input type="file" class="form-control required"  name="venue_file" value="{{ old('venue_file') }}">
                        </div>
                        <div class="form-group download_sample">
                               <label for="name" class="mr-sm-2">Download Sample File</label><br>
                           <a href="{{url('assets/file/venue1.xlsx')}}" download><button class="btn btn-primary btn-sm"    type="button" id="dropdownMenuButton">Download Sample File</button></a>
                        </div>

                        <a href="#" style="margin-left:10px">
                          <button type="" class="btn btn-primary mt-4">Submit</button>
                        </a>
                      </form>
                    </div>  -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->

 
 
<script type="text/javascript">

  $(document).ready(function(){

var form = $("#updateproperty");

form.validate({
errorPlacement: function errorPlacement(error, element) { element.before(error); },

});

form.children("div").steps({
headerTag: "h3",
bodyTag: "section",
transitionEffect: "slideLeft",
autoFocus: true,
/*saveState: true,*/

onStepChanging: function (event, currentIndex, newIndex)
{

form.validate().settings.ignore = ":disabled,:hidden";
return form.valid();

},
onFinishing: function (event, currentIndex)
{

form.validate().settings.ignore = ":disabled";
return form.valid();
},
onFinished: function (event, currentIndex)
{

/*alert('are you sure you want to save venue.!!');*/
$('#updateproperty').submit();
}

});

  });

</script>
<script type="text/javascript">
  $(document).ready(function() {

	  $("#price").change(function(){ 
			var htmlPropertyPrice = '';
			var htmlPrice = '';
    if($(this).val() == 3){
  	  htmlPropertyPrice += '<input id="watch_price" class="form-control form-control-user required" type="number" min="0"  name="watch_price" placeholder="Enter Watch Price" value="{{$watch->watch_price}}" disabled>';
  	  /* htmlPrice = '<select id="price" class="form-control required"  name="price" disabled> <option value="">Select Price</option>'; */
			$("#watch_price").replaceWith(htmlPropertyPrice);
			//$("#price").replaceWith(htmlPrice);	
       }else{
      	 htmlPropertyPrice += '<input id="watch_price" class="form-control form-control-user required" type="number" min="0"  name="watch_price" placeholder="Enter Watch Price" value="{{$watch->watch_price}}">';
      	 //htmlPrice = '<select id="price" class="form-control required"  name="price"> <option value="">Select Price</option> @foreach ($prices as $price) <option value="{{$price->id}}">{{$price->price}}</option> @endforeach </select>';
					$("#watch_price").replaceWith(htmlPropertyPrice);
					//$("#price").replaceWith(htmlPrice);	
	                 } 
    
});
	  
      $(".imgDelete").click(function() {
        var ids = $(this).attr("id");
        var id = ids.split("_").pop();
        $('#loading-image').show();
        $.ajax({
          url: "/admin/deleteWatchImage/"+id,
          type: "GET",
          success : function(data){
                 $('#img_'+id).remove();     
                 $('#imgDelete_'+id).remove();  
                 $('#loading-image').hide();             
            }
          });
        
      });
  });

   
    $(document).ready(function() {

    	$(".btn-success-feature").click(function(){ 
            var html = `<tr class = "control-group-feature">
                <td>
                <label>Additional Feature</label>
              <input class="form-control form-control-user " type="text" maxLength="100" name="additional_feature[]" placeholder="Enter Feature" value="">
              </td>
              <td>
              <button class="btn btn-primary  btn-danger btn-danger-feature" type="button">
				<i class="glyphicon glyphicon-remove"></i> Remove
			</button>
              </td>
                </tr>`;
			
                tableBody = $("table tbody");     
            	tableBody.append(html);
        });

        $("body").on("click",".btn-danger-feature",function(){ 
            $(this).parents(".control-group-feature").remove();
        });

        $(".btn-success").click(function(){ 
            var html = $(".clone").html();
            $(".increment").after(html);
        });

        $("body").on("click",".btn-danger",function(){ 
            $(this).parents(".control-group").remove();
        });

        $("#countryId").change(function(){
        var countryId = $(this).val();
        $('#loading-image').show();
        $.ajax({
          url: "/admin/admincommon/getState",
          type: "POST",
          data: {countryId: countryId, _token: '{{csrf_token()}}' },
          dataType: 'json',
          success : function(data){
            var html = `<option value="">Select State</option>`;
            for (var i = 0; i < data.length; i++) {
                var id = data[i].id;
                var name = data[i].state_name;
                var option = `<option value="${data[i].id}">${data[i].state_name}</option>`;
                html += option;
              }
            $('#loading-image').hide();
              $('#stateId').html(html); 
            }
          });
        $('#loading-image').show();
        $.ajax({
			url: "/admin/admincommon/getCity",
			type: "POST",
			data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
			dataType: 'json',
			success : function(data){
				var html = `<option value="">Select City</option>`;
				for (var i = 0; i < data.length; i++) {
					  var id = data[i].id;
					  var name = data[i].city_name;
					  var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
					  html += option;
					}
				$('#loading-image').hide();
					$('#cityId').html(html);	
				}
			});
      });
        $("#stateId").change(function(){
        	console.log('inside state id');
        var stateId = $(this).val();
        $('#loading-image').show();
        $.ajax({
          url: "/admin/admincommon/getCity",
          type: "POST",
          data: {stateId: stateId, _token: '{{csrf_token()}}' },
          dataType: 'json',
          success : function(data){
            var html = `<option value="">Select City</option>`;
            for (var i = 0; i < data.length; i++) {
            	console.log('data-----------');
                var id = data[i].id;
                var name = data[i].city_name;
                var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
                html += option;
              }
            $('#loading-image').hide();
              $('#cityId').html(html);  
            }
          });
      });

        $("#brand").change(function(){
    		var brandId = $(this).val();
    		$('#loading-image').show();
    		$.ajax({
    			url: "/admin/createwatchdata/getModel",
    			type: "POST",
    			data: {brandId: brandId, _token: '{{csrf_token()}}' },
    			dataType: 'json',
    			success : function(data){
    				var html = `<option value="">Select Model</option>`;
    				for (var i = 0; i < data.length; i++) {
    					  var option = `<option value="${data[i].id}">${data[i].watch_model_name}</option>`;
    					  html += option;
    					}
    				$('#loading-image').hide();
    					$('#model').html(html);	
    				}
    			});
    	});

      });

</script>
<script src="https://cdn.ckeditor.com/4.5.6/standard/ckeditor.js"/>
@stop