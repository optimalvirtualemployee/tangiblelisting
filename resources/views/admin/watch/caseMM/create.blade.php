@extends('admin.common.innerdefault')
@section('title', 'Create Case MM')
@section('content')
<style>
.error {
      color: red;
   }
</style>
<div class="container">
    <!-- Outer Row -->
    <div class="row justify-content-center">
      <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-8">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Create New Case MM</h1>
                  </div>
                  @if($errors->any())
                    @foreach($errors->all() as $error)
                      <div class="alert alert-danger">
                    {{$error}}
                      </div>
                    @endforeach
                  @endif
                  <form class="user" id="create_case" method="POST" action="{{ route('caseMM.store') }}">
                  @csrf
                    <div class="form-group">
                      <input id="case_mm" class="form-control form-control-user" type="text"  maxlength="100"  name="case_mm" placeholder="Enter Case MM" value="{{ old('case_mm') }}"><br>
                    </div>
                    <div class="form-group">
                      <label>Status</label>
                      <select id ="status" name="status" class="form-control required">
                       <option value="">Select Status</option>
                        <option value="1" >Active</option>
                        <option value="0" >Inactive</option>
                      </select>
                    </div>
                      <button type="submit" class="btn btn-primary btn-user btn-block">Add New</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------------ JS SCRIPT SECTION --------------------------------------->
  
<script type="text/javascript">
    $(document).ready(function(){
     
      $("#create_case").validate({
        
        rules: {
         
          case_mm: {required: true,number:true}
        
          },
        
        messages: {
          case_mm: {required: "Case MM can\'t be left blank",number:"Please enter numbers Only"}
        },
        
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>
<!------------------------------------------------ JS SCRIPT SECTION --------------------------------------->
@stop
