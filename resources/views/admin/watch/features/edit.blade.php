@extends('admin.common.innerdefault')
@section('title', 'Update Feature')
@section('content')

<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update Feature</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="editfeature" method="POST" action="{{ route('createwatchfeature.update', $feature_data->id) }}">
                  @method('PATCH')
                  @csrf
                  <div class="form-group">
                  <div class="form-group">
                  	<input type ="hidden" id="listing_id" class="form-control form-control-user " type="text"  name="listing_id" value="{{$feature_data->listing_id}}">
                    </div>
                    <div class="form-group">
                   	<label>Power Reserve:</label><select id="powerReserve" class="form-control"  name="powerReserve" required>
						<option value="{{$powerReserveSelected->id}}">{{$powerReserveSelected->power_reserve}}</option>
						@foreach ($power_reserve as $power_reserve)
                           <option value="{{$power_reserve->id}}">{{$power_reserve->power_reserve}}</option>
                         @endforeach
                      </select>
                      </div>
                      <div class="form-group">
                   	<label>Dial Color:</label><select id="dialColor" class="form-control"  name="dialColor" required>
						<option value="{{$dialColorSelected->id}}">{{$dialColorSelected->dial_color}}</option>
						@foreach ($dial_color as $dial_color)
                           <option value="{{$dial_color->id}}">{{$dial_color->dial_color}}</option>
                         @endforeach
                      </select>
                      </div>
                      <div class="form-group">
                   	<label>Bracelet Material:</label><select id="braceletMaterial" class="form-control"  name="braceletMaterial" required>
						<option value="{{$braceletMaterialSelected->id}}">{{$braceletMaterialSelected->bracelet_material}}</option>
						@foreach ($bracelet_material as $bracelet_material)
                           <option value="{{$bracelet_material->id}}">{{$bracelet_material->bracelet_material}}</option>
                         @endforeach
                      </select>
                      </div>
                      <div class="form-group">
                   	<label>Bezel Material:</label><select id="bezelMaterial" class="form-control"  name="bezelMaterial" required>
						<option value="{{$bezelMaterialSelected->id}}">{{$bezelMaterialSelected->bezel_material}}</option>
						@foreach ($bezel_material as $bezel_material)
                           <option value="{{$bezel_material->id}}">{{$bezel_material->bezel_material}}</option>
                         @endforeach
                      </select>
                      </div>
                        <div class="form-group">
                   	<label>Bracelet Color:</label><select id="braceletColor" class="form-control"  name="braceletColor" required>
						<option value="{{$braceletColorSelected->id}}">{{$braceletColorSelected->bracelet_color}}</option>
						@foreach ($bracelet_color as $bracelet_color)
                           <option value="{{$bracelet_color->id}}">{{$bracelet_color->bracelet_color}}</option>
                         @endforeach
                      </select>
                      </div>
                      <div class="form-group">
                   	<label>Type Of Clasp:</label><select id="typeOfClasp" class="form-control"  name="typeOfClasp" required>
						<option value="{{$typeOfClaspSelected->id}}">{{$typeOfClaspSelected->type_of_clasp}}</option>
						@foreach ($type_of_clasp as $type_of_clasp)
                           <option value="{{$type_of_clasp->id}}">{{$type_of_clasp->type_of_clasp}}</option>
                         @endforeach
                      </select>
                      </div>
                      <div class="form-group">
                   	<label>Clasp Material:</label><select id="claspMaterial" class="form-control"  name="claspMaterial" required>
						<option value="{{$claspMaterialSelected->id}}">{{$claspMaterialSelected->clasp_material}}</option>
						@foreach ($clasp_material as $clasp_material)
                           <option value="{{$clasp_material->id}}">{{$clasp_material->clasp_material}}</option>
                         @endforeach
                      </select>
                      </div>
                      <div class="form-group">
                   	<label>Case Material:</label><select id="caseMaterial" class="form-control"  name="caseMaterial" required>
						<option value="{{$caseMaterialSelected->id}}">{{$caseMaterialSelected->case_material}}</option>
						@foreach ($case_material as $case_material)
                           <option value="{{$case_material->id}}">{{$case_material->case_material}}</option>
                         @endforeach
                      </select>
                      </div>
                      <div class="form-group">
                   	<label>Case MM:</label><select id="caseMM" class="form-control"  name="caseMM" required>
						<option value="{{$caseMMSelected->id}}">{{$caseMMSelected->case_mm}}</option>
						@foreach ($case_mm as $case_mm)
                           <option value="{{$case_mm->id}}">{{$case_mm->case_mm}}</option>
                         @endforeach
                      </select>
                      </div>
                      <div class="form-group">
                   	<label>Water Resistance Depth:</label><select id="wrd" class="form-control"  name="wrd" required>
						<option value="{{$wrdSelected->id}}">{{$wrdSelected->water_resistant_depth}}</option>
						@foreach ($wrd as $wrd)
                           <option value="{{$wrd->id}}">{{$wrd->water_resistant_depth}}</option>
                         @endforeach
                      </select>
                      </div>
                        <div class="form-group">
                   	<label>Glass Type:</label><select id="glassType" class="form-control"  name="glassType" required>
						<option value="{{$glassTypeSelected->id}}">{{$glassTypeSelected->glass_type}}</option>
						@foreach ($glass_type as $glass_type)
                           <option value="{{$glass_type->id}}">{{$glass_type->glass_type}}</option>
                         @endforeach
                      </select>
                      </div>
                      <div class="form-group">
                   	<label>Chronograph:</label><select id="chronograph" class="form-control"  name="chronograph" required>
						<option value="{{$chronographSelected->id}}">{{$chronographSelected->chronograph == 1 ? 'Yes' : 'No'}}</option>
                        @foreach ($chronograph as $chronograph)
                           <option value="{{$chronograph->id}}">{{$chronograph->chronograph == 1 ? 'Yes' : 'No'}}</option>
                         @endforeach
                      </select>
                      </div>
                      <div class="form-group">
                   	<label>Tourbillion:</label><select id="tourbillion" class="form-control"  name="tourbillion" required>
						<option value="{{$tourbillionSelected->id}}">{{$tourbillionSelected->tourbillion == 1 ? 'Yes' : 'No'}}</option>
						@foreach ($tourbillion as $tourbillion)
                           <option value="{{$tourbillion->id}}">{{$tourbillion->tourbillion == 1 ? 'Yes' : 'No'}}</option>
                         @endforeach
                      </select>
                      </div>
                      <div class="form-group">
                   	<label>GMT:</label><select id="gmt" class="form-control"  name="gmt" required>
						<option value="{{$gmtSelected->id}}">{{$gmtSelected->gmt == 1 ? 'Yes' : 'No'}}</option>
						@foreach ($gmt as $gmt)
                           <option value="{{$gmt->id}}">{{$gmt->gmt == 1 ? 'Yes' : 'No'}}</option>
                         @endforeach
                      </select>
                      </div>
                      <div class="form-group">
                   	<label>Annual Calender:</label><select id="annualCalender" class="form-control"  name="annualCalender" required>
						<option value="{{$annualCalenderSelected->id}}">{{$annualCalenderSelected->annual_calender == 1 ? 'Yes' : 'No'}}</option>
						@foreach ($annual_calender as $annual_calender)
                           <option value="{{$annual_calender->id}}">{{$annual_calender->annual_calender == 1 ? 'Yes' : 'No'}}</option>
                         @endforeach
                      </select>
                      </div>
                      <div class="form-group">
                   	<label>Minute Repeater:</label><select id="minuteRepeater" class="form-control"  name="minuteRepeater" required>
						<option value="{{$minuteRepeaterSelected->id}}">{{$minuteRepeaterSelected->minute_repeater == 1 ? 'Yes' : 'No'}}</option>
						@foreach ($minute_repeater as $minute_repeater)
                           <option value="{{$minute_repeater->id}}">{{$minute_repeater->minute_repeater == 1 ? 'Yes' : 'No'}}</option>
                         @endforeach
                      </select>
                      </div>
                      <div class="form-group">
                   	<label>Double Chronograph:</label><select id="doubleChronograph" class="form-control"  name="doubleChronograph" required>
						<option value="{{$doubleChronographSelected->id}}">{{$doubleChronographSelected->double_chronograph == 1 ? 'Yes' : 'No'}}</option>
						@foreach ($double_chronograph as $double_chronograph)
                           <option value="{{$double_chronograph->id}}">{{$double_chronograph->double_chronograph == 1 ? 'Yes' :'No'}}</option>
                         @endforeach
                      </select>
                      </div>
                      <div class="form-group">
                   	<label>Panorma Date:</label><select id="panormaDate" class="form-control"  name="panormaDate" required>
						<option value="{{$panormaDateSelected->id}}">{{$panormaDateSelected->panorma_date == 1 ? 'Yes' : 'No'}}</option>
						@foreach ($panorma_date as $panorma_date)
                           <option value="{{$panorma_date->id}}">{{$panorma_date->panorma_date == 1 ? 'Yes' :'No'}}</option>
                         @endforeach
                      </select>
                      </div>
                      <div class="form-group">
                   	<label>Jumping Hour:</label><select id="jumpingHour" class="form-control"  name="jumpingHour" required>
						<option value="{{$jumpingHourSelected->id}}">{{$jumpingHourSelected->jumping_hour == 1 ? 'Yes' : 'No'}}</option>
						@foreach ($jumping_hour as $jumping_hour)
                           <option value="{{$jumping_hour->id}}">{{$jumping_hour-> jumping_hour == 1 ? 'Yes' :'No'}}</option>
                         @endforeach
                      </select>
                      </div>
                      <div class="form-group">
                   	<label>Alarm:</label><select id="alarm" class="form-control"  name="alarm" required>
						<option value="{{$alarmSelected->id}}">{{$alarmSelected->alarm == 1 ? 'Yes' : 'No'}}</option>
						@foreach ($alarm as $alarm)
                           <option value="{{$alarm->id}}">{{$alarm->alarm == 1 ? 'Yes' :'No'}}</option>
                         @endforeach
                      </select>
                      </div>
                      <div class="form-group">
                   	<label>Year:</label><select id="year" class="form-control"  name="year" required>
						<option value="{{$yearSelected->id}}">{{$yearSelected->year == 1 ? 'Yes' : 'No'}}</option>
						@foreach ($year as $year)
                           <option value="{{$year->id}}">{{$year->year == 1 ? 'Yes' :'No'}}</option>
                         @endforeach
                      </select>
                      </div>
                      <div class="form-group">
                   	<label>Day:</label><select id="day" class="form-control"  name="day" required>
						<option value="{{$daySelected->id}}">{{$daySelected->day == 1 ? 'Yes' : 'No'}}</option>
						@foreach ($day as $day)
                           <option value="{{$day->id}}">{{$day->day == 1 ? 'Yes' :'No'}}</option>
                         @endforeach
                      </select>
                      </div>
                      </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#editfeature").validate({
    
        rules: {
        	listing_id: "required",
        },
    
        messages: {
        	listing_id: "Listing Id can\'t be left blank",
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop
