@extends('admin.common.innerdefault')
@section('title', 'Display Brands Models')
@section('content')

  <!-- Page Wrapper -->
  <div id="wrapper">

   
  
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

       
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Watch Brands Models</h1>
         <a href="{{url('/admin/watch/createbrandsmodel')}}" class="btn btn-primary btn-icon-split">
                  
                    <span class="text">Add New Models</span>
                  </a>
                  @if(Session::has('success_msg'))
                  <div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">{{ Session::get('success_msg') }}</div>
                  @endif

                  
          <div class="card shadow mb-4" style="margin-top: 10px;">
           
            <div class="card-body" >
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Model Name</th>
                      <th>Brand Name</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                 
                  <tbody>
                    @foreach ($brands as $brand)
                      <tr>
                        <td>  {{$brand->watch_model_name}} </td>
                        <td>  {{$brand->watch_brand_name}} </td>
                        <td>@if($brand->status =='1') Active  @else Inactive @endif  </td>
                        <td><a href="{{url('/admin/watch/updatebrandsmodel/'.$brand->id)}}"><i class="fas fa-edit"></i></a> &nbsp; &nbsp;<a href="#" class="deletebrandsmodel" id="del_{{$brand->id}}"><i class="fas fa-trash-alt"></td>
                      
                      </tr>
                    @endforeach
                    
                     
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      
<script type="text/javascript">
   $(document).on("click", ".deletebrandsmodel", function() { 
      var url = "{{ route('deletebrandsmodel') }}";
      var id = this.id;
      var splitid = id.split("_");
      var deleteid = splitid[1];
     if ( confirm("Do you really want to delete record?")) {
        
        $.ajax({
          url: url,
           type: 'POST',
          cache: false,
          data:{
             id:deleteid,
            _token:'{{ csrf_token() }}'
          },
          success: function(data){ 

              $('#main').html('<div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">' + data.success_msg + '</div>')
               location.reload();
           
          }
        });
      }
      
  });
</script>
     @stop
