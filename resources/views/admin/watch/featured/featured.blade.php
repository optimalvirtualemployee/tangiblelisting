@extends('admin.common.innerdefault')
@section('title', 'Display Featured Watches')
@section('content')

  <!-- Page Wrapper -->
  <div id="wrapper">
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- End of Topbar -->
        <!-- Begin Page Content -->
        <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Featured Watches</h1>
        @if(Session::has('success_msg'))
          <div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">{{ Session::get('success_msg') }}</div>
            @endif
            <div id="main"></div>
            <div class="card shadow mb-4" style="margin-top: 10px;">
            <div class="card-body" >
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Watch Title</th>
                      <th>Model</th>
                      <th>Brand</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  
                  <tbody>
                    @foreach ($watches as $data)
                      <tr>
                        <td>{{ $data->ad_title}}</td>
                        <td>{{ $data->model_name}}</td>
                        <td>{{ $data->brand_name}}</td>
                        <td>
                          <a href="{{url('admin/watchData/editFeatured', $data->id)}}"><i class="fas fa-edit"></i></a> &nbsp; &nbsp;
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->

      <!-------------------------- JS SCRIPT SECTION --------------------------------------->
      <script type="text/javascript">
          $(document).ready(function(){
            setTimeout(function(){
                $("div.alert").remove();
            }, 3000 ); 
          });
      </script>
      <!--------------------------- JS SCRIPT SECTION ------------------------------------->
@stop

