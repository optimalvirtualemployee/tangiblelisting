@extends('dealer.common.default')
@section('title', 'Enquiry')
@section('content')
    <!-- Header -->
    <div class="header pb-6 d-flex align-items-center" style="min-height: 100px;">
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">

        <div class="col-xl-12">
          <form>
            <div class="card">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col-4">
                    <h3 class="mb-0">Enquiry </h3>
                  </div>
                  <div class="col-8 text-right">
                    <div id="searchwrap" class="mr-4">
                        <form action="{{url('dealer/enquiryList')}}" method="get">
                            <input id="search" name="search" type="text" class="form-control" placeholder="What're we looking for ?">
                            <button type="submit" class="btn btn-icon btn-primary">
                                <span class="btn-inner--icon"><i class="fa fa-search" aria-hidden="true"></i></span>
                            </button>
                        </form>
                      </div>
                    <a class="btn btn-primary" href="#">
                        <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>
                        <span class="btn-inner--text">Add Prospect</span> 
                    </a>
                    <a class="btn" data-toggle="collapse" href="#filterCollapse" role="button" aria-expanded="false" aria-controls="filterCollapse">
                        <span class="btn-inner--icon"><i class="ni ni-ui-04"></i></span>
                        <span class="btn-inner--text">Filter</span> 
                    </a>
                  </div>
                </div>

              </div>

              <div class="collapse" id="filterCollapse">
                <div class="card-body">
                    Filter section coming soon...
                </div>
            </div>
              
              <div class="card-body">
                <div class="table-responsive">
                    <div>
                       <table class="table align-items-center">
                          <thead class="thead-light">
                             <tr>
                                <th scope="col" class="sort" data-sort="name">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheckall">
                                        <label class="custom-control-label" for="customCheckall">All</label>
                                      </div>
                                </th>
                                <th scope="col" class="sort" data-sort="item">Date</th>
                                <th scope="col" class="sort" data-sort="price">Customer Name</th>
                                <th scope="col" class="sort" data-sort="price">Customer Mobile Number</th>
                                <th scope="col" class="sort" data-sort="price">Country</th>
                                <th scope="col" class="sort" data-sort="status">Allocated To</th>                                                        
                                <th scope="col" class="sort" data-sort="action">Messages</th>
                                <th scope="col" class="sort" data-sort="action">Enquired On</th>
                             </tr>
                          </thead>
                          <tbody class="list">
                            @if($watchEnquary ?? '')
                            <?php $pagees = $watchEnquary;?>
                            @foreach($watchEnquary as $key=>$watchDetail)
                             <tr>
                                <th scope="row">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck2">
                                        <label class="custom-control-label" for="customCheck2"></label>
                                      </div>
                                </th>
                                <th scope="row">
                                  {{$watchDetail->created_at}}
                                </th>
                                <td class="budget">
                                {{$watchDetail->name}}
                                </td>
                                <td class="budget">
                                {{$watchDetail->phone}}
                                </td>
                                <td class="budget">
                                {{$watchDetail->country}}
                                </td>
                                <td class="text-left">
                                  {{$watchDetail->first_name}} {{$watchDetail->last_name}}
                                </td>                                                        
                                <td>
                                  {{$watchDetail->message}}
                                </td>
                                <td class="text-left">
                                  <!-- <img src="{{url('assets/img/icons/common/google.svg')}}"> -->
                                  <strong>{{$watchDetail->model_name}}</strong>
                                  <br>
                              <span>${{number_format($watchDetail->watch_price,2)}}</span>
                              <br>
                              <span class="topdeal">TOPDEAL</span>
                              </td>
                             </tr>
                             @endforeach
                             @endif
                             @if($carEnquary ?? '')
                              @foreach($carEnquary as $key=>$automobileDetail)
                             <tr>
                                <th scope="row">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck2">
                                        <label class="custom-control-label" for="customCheck2"></label>
                                      </div>
                                </th>
                                <th scope="row">
                                  {{$automobileDetail->created_at}}
                                </th>
                                <td class="budget">
                                {{$automobileDetail->name}}
                                </td>
                                <td class="budget">
                                {{$automobileDetail->phone}}
                                </td>
                                <td class="budget">
                                {{$automobileDetail->country}}
                                </td>
                                <td class="text-left">
                                  {{$automobileDetail->first_name}} {{$automobileDetail->last_name}}
                                </td>                                                        
                                <td>
                                  {{$automobileDetail->message}}
                                </td>
                                <td class="text-left">
                                  <!-- <img src="{{url('assets/img/icons/common/google.svg')}}"> -->
                                  <strong>{{$automobileDetail->ad_title}}</strong>
                              <br>
                              <span class="topdeal">TOPDEAL</span>
                              </td>
                             </tr>
                             @endforeach
                             @endif
                             @if($realEstate ?? '')
                                @foreach($realEstate as $key=>$realEstateDetail)
                               <tr>
                                  <th scope="row">
                                      <div class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input" id="customCheck2">
                                          <label class="custom-control-label" for="customCheck2"></label>
                                        </div>
                                  </th>
                                  <th scope="row">
                                    {{$realEstateDetail->created_at}}
                                  </th>
                                  <td class="budget">
                                  {{$realEstateDetail->name}}
                                  </td>
                                  <td class="budget">
                                  {{$realEstateDetail->phone}}
                                  </td>
                                  <td class="budget">
                                  {{$realEstateDetail->country}}
                                  </td>
                                  <td class="text-left">
                                    {{$realEstateDetail->first_name}} {{$realEstateDetail->last_name}}
                                  </td>                                                        
                                  <td>
                                    {{$realEstateDetail->message}}
                                  </td>
                                  <td class="text-left">
                                    <!-- <img src="{{url('assets/img/icons/common/google.svg')}}"> -->
                                    <strong>{{$realEstateDetail->ad_title}}</strong>
                                <br>
                                <span class="topdeal">TOPDEAL</span>
                                </td>
                               </tr>
                               @endforeach
                              
                            @endif
                          </tbody>
                       </table>
                      {{$pages->withQueryString()->links()}}
                    </div>
                 </div>
              </div>
            </div>
          </form>
        </div>
      </div>
@stop