<!DOCTYPE html>
<html lang="en">
  
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Update Watch | Tangible Dealer Portal</title>
    <!-- Favicon -->
    <link rel="icon" href="{{url('assets/img/brand/favicon.png')}}" type="image/png">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="{{url('assets/vendor/nucleo/css/nucleo.css')}}" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Page plugins -->
    <link rel="stylesheet" href="{{url('assets/css/main.css')}}" type="text/css">   
    <link rel="stylesheet" href="{{url('assets/css/image-uploader.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/multistep-form.css')}}">
  </head>
  <style>
    .slick-slide img {
    max-height: 300px;
    width: auto;
    }
    #imgGallery div, #dmgGallery div {
    position: relative; 
    width: 120px;
    height: 120px;
    margin: 5px;
    border: 1px solid #e7e7e7;
    }
    #imgGallery div img, #dmgGallery div img {
    max-height: 150px;
    width: 100%;
    height: 100%;
    object-fit: cover;
    position: absolute;
    }
    .avatar-upload {
  position: relative;
  max-width: 205px;
  border: 2px dotted #bfc8d9;
}
.avatar-upload .avatar-edit {
  position: absolute;
  right: 12px;
  z-index: 1;
  top: 10px;
}
.avatar-upload .avatar-edit input {
  display: none;
}
.avatar-upload .avatar-edit input + label {
  display: inline-block;
  width: 34px;
  height: 34px;
  margin-bottom: 0;
  border-radius: 100%;
  background: #FFFFFF;
  border: 1px solid transparent;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
  cursor: pointer;
  font-weight: normal;
  transition: all 0.2s ease-in-out;
}
.avatar-upload .avatar-edit input + label:hover {
  background: #f1f1f1;
  border-color: #d6d6d6;
}
.avatar-upload .avatar-edit input + label:after {
  content: "\f040";
  font-family: 'FontAwesome';
  color: #757575;
  position: absolute;
  top: 10px;
  left: 0;
  right: 0;
  text-align: center;
  margin: auto;
}
.avatar-upload .avatar-preview {
  width: 192px;
  height: 192px;
  position: relative;
  border-radius: 100%;
  border: 6px solid #F8F8F8;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
  margin: 0 auto;
}
.avatar-upload .avatar-preview > div {
  width: 100%;
  height: 100%;
  border-radius: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
}
.security-photos{
  background-color: #fcfcfc;
    padding: 10px;
}
.previewContainer{
  background-color: #fff;
    box-shadow: 1px 1px 1px 1px #80808040;
    padding: 25px;
}
#loading-image{
  position:fixed;
  top:0px;
  right:0px;
  width:100%;
  height:100%;
  background-color:#666;
  background-image:url('/assets/img/Spinner-loader.gif');
  background-repeat:no-repeat;
  background-position:center;
  z-index:10000000;
  opacity:0.6;
}
  </style>
  <body>
    <@include('agent.common.sidebar')
    
    
    <section class="become-seller-from overlay-wrapper">
    
    @if (Route::has('login'))
    @auth
      <div class="container">
      <div class="row">
      <div class="col-lg-12">
      <div class="">
                        <div id='loading-image' style='display:none'>
				</div>
        <!-- MultiStep Form -->
        <div class="container-fluid" id="grad1">
          <div class="row justify-content-center mt-0">
            <div class="col-11  col-lg-12 text-center p-0 mt-3 mb-2">
              <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                <!-- <h2><strong>Sign Up Your User Account</strong></h2>
                  <p>Fill all form field to go to next step</p> -->
                <div class="row">
                  <div class="col-md-12 mx-0">
                   <form id="msform" action="{{ url('dealer/storewatch') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="agentId" id="agentId" value="{{$watch->agent_id}}">
                    <input type="hidden" name="productId" id="productId" value="{{$watch->id}}">
                      <!-- progressbar -->
                      <ul id="progressbar">
                        <li class="active" id="account"><strong>Select Watch</strong></li>
                        <li id="personal"><strong>Add Details</strong></li>
                        <li id="payment"><strong>Attach Photos</strong></li>
                        <li id="personalInfo"><strong>Personal Information</strong></li>
                        <li id="confirm"><strong>Summary</strong></li>
                      </ul>
                      <!-- fieldsets -->

                      <fieldset>
                        <div class="form-card">
                          <p><b>What type of Watch you Selling?</b></p>
                          <div class="row">
                            <div class="col-6">
                              <select class=" form-control" id="brand"
                                name="brand">
                                <option value="{{$brandSelected->id}}">{{$brandSelected->watch_brand_name}}</option>
                          @foreach ($brands as $brand)
                           <option value="{{$brand->id}}">{{$brand->watch_brand_name}}</option>
                         @endforeach
                              </select>
                            </div>
                            <div class="col-6">
                              <select class=" form-control" id="model"
                                name="model">
                                <option value="{{$modelSelected->id}}">{{$modelSelected->watch_model_name}}</option>
                                                             
                              </select>
                            </div>                            
                          </div>
                        </div>
                        <input type="button" name="next" class="next action-button" id ="next1" value="Next Step" />
                      </fieldset>
                      <fieldset>
                        <div class="form-card">
                          <div class="">
                            <h2 class="fs-title">Watch Details</h2>

                            <hr class="my-2">
                            <div class="row">
                              <div class="col-lg-12">
                                <div class="row m-0">
                                
                                  
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Country*</label>
                                      <select class="form-control" data-trigger name="countryId" id="countryId">
                                                    @foreach ($country_data as $country)
                                       <option @if($countrySelected->id ?? '') @if($countrySelected->id==$country->id) selected @endif @endif value="{{$country->id}}">{{$country->country_name}}</option>
                                     @endforeach
                                                  </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">State</label>
                                      <select class="form-control" data-trigger name="stateId" id="stateId">
                                                    @foreach ($state_data as $state)
                                  <option @if($stateSelected->id ?? '') @if($stateSelected->id==$state->id) selected @endif @endif value="{{$state->id}}">{{$state->state_name != '' ? $state->state_name : 'Please Select State'}}</option>
                                @endforeach
                                                  </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">City*</label>
                                      <select class="form-control" data-trigger name="city-id" id="cityId">
                                                    @foreach ($city_data as $city)
                                  <option @if($citySelected->id ?? '') @if($citySelected->id==$city->id) selected @endif @endif value="{{$city->id}}">{{$city->city_name}}</option>
                                @endforeach
                                                  </select>
                                    </div>
                                  </div>
                                   <!-- 
                                  <div class="col-lg-4">
                                   <div class="form-group">
                                      <label>Price On Request</label>
                                      <select id="price_on_request" class="form-control required" name="price_on_request">
                                  <option @if($watch->price_on_request ?? '' ) @if($watch->price_on_request==0) selected @endif @endif value="0">No</option>
                                  <option @if($watch->price_on_request ?? '' ) @if($watch->price_on_request==1) selected @endif @endif value="1">Yes</option>
                                  </select>
                                    </div>
                                  </div>
                                   -->
                                
                                  
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Case Diameter</label>
                                      <select id="case_diameter" class="form-control"  name="case_diameter">
                                        @if($caseDiameterSelected)       
                                        <option value="{{$caseDiameterSelected->id}}">{{$caseDiameterSelected->case_diameter}}</option>
                                        @else
                                        <option value="">Select Case Diameter</option>
                                        @endif
                                        @foreach ($case_diameters as $case_diameter)
                                                       <option value="{{$case_diameter->id}}">{{$case_diameter->case_diameter}}</option>
                                                     @endforeach
                                      </select>
                                    </div>
                                  </div>
                                 
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Movement</label>
                                      <select id="movement" class="form-control"  name="movement">
                                        @if($movementSelected ?? '')
                                        <option value="{{$movementSelected->id}}">{{$movementSelected->movement}}</option>
                                        @else
                                        <option value="">Select Movement</option>
                                        @endif
                                        @foreach ($movements as $movement)
                                         <option value="{{$movement->id}}">{{$movement->movement}}</option>
                                       @endforeach
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Watch Type</label>
                                      <select id="type" class="form-control"  name="type">
                                        @if($typeSelected)
                                        <option value="{{$typeSelected->id}}">{{$typeSelected->watch_type}}</option>
                                        @else
                                        <option value="">Select Type</option>
                                        @endif
                                        @foreach ($types as $type)
                                         <option value="{{$type->id}}">{{$type->watch_type}}</option>
                                       @endforeach
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Year of Manufacturer</label>
                                      <select id="yom" class="form-control"  name="yom">
                                        @if($year_of_manufactureSelected)        
                                        <option value="{{$year_of_manufactureSelected->id}}">{{$year_of_manufactureSelected->year_of_manufacture}}</option>
                                        @else
                                        <option value="">Select Year Of Manufacture</option>
                                        @endif
                                        @foreach ($year_of_manufactures as $year_of_manufacture)
                                          <option value="{{$year_of_manufacture->id}}">{{$year_of_manufacture->year_of_manufacture}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Gender*</label>
                                      <select id="gender" class="form-control required"  name="gender">
                                        <option value="{{$genderSelected->id}}">{{$genderSelected->gender}}</option>
                                        @foreach ($genders as $gender)
                                        <option value="{{$gender->id}}">{{$gender->gender}}</option>
                                                     @endforeach
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Inclusions*</label>
                                      <select class="form-control" data-trigger name="inclusions" id="inclusions">
                                        <option value="{{$inclusionSelected->id}}">{{$inclusionSelected->inclusion}}</option>
                                           @foreach ($inclusions_data as $inclusion)
                                            <option value="{{$inclusion->id}}">{{$inclusion->inclusion}}</option>
                                           @endforeach                                        
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <div class="form-group">
                                        <label class="form-control-label"
                                          for="input-last-name">Reference Number*</label>
                                        <div class="input-group mb-3">                                        
                                          <input type="text" class="form-control" id="reference_num" name="referenceNo" value="{{$watch->reference_no}}" placeholder="Reference Number">
                                        </div>
                                      </div>
                                  </div>
                                </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Watch Condition*</label>
                                      <select class="form-control" data-trigger name="watchCondition" id="watchCondition">
                                        <option value="1" {{$watch->watch_condition == "New" ? 'selected' : ''}}>New</option>
                              <option value="0" {{$watch->watch_condition == "Used" ? 'selected' : ''}}>Used</option>                                        
                                      </select>
                                    </div>
                                  </div>                                 
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                     <label class="form-control-label" for="input-last-name">Status Of Listing*</label>
                                                        <select id ="status" name="status" class="form-control" >
                                          <option value="1" {{$watch->status == "1" ? 'selected' : ''}}>Active</option>
                                          <option value="0" {{$watch->status == "0" ? 'selected' : ''}}>Inactive</option>
                                          <option value="2" {{$watch->status == "2" ? 'selected' : ''}}>Sold</option>
                                        </select>
                                        
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-last-name">Timezone*</label>
                                                        <select class="form-control" data-trigger name="timezone" id="timezone">
                                                            <option value="">Select Timezone</option>
                                                            @foreach ($timezone as $zone)
                                 <option @if($timezoneSelected->id ?? '') @if($timezoneSelected->id==$zone->id) selected @endif @endif value="{{$zone->id}}">{{$zone->name .' '. $zone->offset}}</option>
                               @endforeach
                                                          </select>
                                                    </div>
                                  </div>
                                </div>
                                <h2 class="fs-title mt-5">Additional Info</h2>
                                <hr class="my-2">
                                <div class="row m-0">
                                                                 
                                </div>
                                <div class="row m-0">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Power Reserve </label>
                                      
                                      <select id="powerReserve" class="form-control required"  name="powerReserve">

                                     
                                      <option value="">Select Power Reserve</option>
                                      <?php /* <option value="{{$powerReserveSelected->id}}">{{$powerReserveSelected->power_reserve}}</option> */ ?>

                                      @foreach ($power_reserve as $power_reserve)
                                      <option value="{{$power_reserve->id}}"  <?php if (isset($powerReserveSelected->id)) {
                                      if ($powerReserveSelected->id == $power_reserve->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$power_reserve->power_reserve}}</option>
                                      @endforeach
                                      </select>

                                    </div>
                                  </div>
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Dial Color</label>
                                   
                                      <select id="dialColor" class="form-control required"  name="dialColor" >
                                      <option value="">Select Dial Color</option>
                                      <?php /* ?> <option value="{{$dialColorSelected->id}}">{{$dialColorSelected->dial_color}}</option> <?php */ ?>
                                      @foreach ($dial_color as $dial_color)
                                      <option value="{{$dial_color->id}}" <?php if (isset($dialColorSelected->id)) {
                                      if ($dialColorSelected->id == $dial_color->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$dial_color->dial_color}}</option>
                                      @endforeach
                                      </select>                                       
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Bracelet Material</label>
                                   
                                      <select id="braceletMaterial" class="form-control required"  name="braceletMaterial">
                                      <option value="">Select Bracelet Material</option>
                                      <?php /* ?> <option value="{{$braceletMaterialSelected->id}}">{{$braceletMaterialSelected->bracelet_material}}</option> <?php */ ?>

                                      @foreach ($bracelet_material as $bracelet_material)
                                      <option value="{{$bracelet_material->id}}"  <?php if (isset($braceletMaterialSelected->id)) {
                                      if ($braceletMaterialSelected->id == $bracelet_material->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$bracelet_material->bracelet_material}}</option>
                                      @endforeach
                                      </select>

                                    </div>
                                  </div>

                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Bezel Material</label>
                                      <select id="bezelMaterial" class="form-control required"  name="bezelMaterial" >
                                      <option value="">Select Bezel Material</option>
          <?php /* ?> <option value="{{$bezelMaterialSelected->id}}">{{$bezelMaterialSelected->bezel_material}}</option> <?php */ ?>
            @foreach ($bezel_material as $bezel_material)
                           <option value="{{$bezel_material->id}}" <?php if (isset($bezelMaterialSelected->id)) {
                                      if ($bezelMaterialSelected->id == $bezel_material->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$bezel_material->bezel_material}}</option>
                         @endforeach
                      </select>
                                    </div>
                                  </div>                                  
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Bracelet Color</label>
                                        <select id="braceletColor" class="form-control required"  name="braceletColor">
                                        <option value="">Select Bracelet Color</option>
                                        <?php /*
            <option value="{{$braceletColorSelected->id}}">{{$braceletColorSelected->bracelet_color}}</option> <?php */ ?>
            @foreach ($bracelet_color as $bracelet_color)
                           <option value="{{$bracelet_color->id}}" <?php if (isset($braceletColorSelected->id)) {
                                      if ($braceletColorSelected->id == $bracelet_color->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$bracelet_color->bracelet_color}}</option>
                         @endforeach
                      </select>                                      
                                    </div>
                                  </div>    
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Type of Clasp</label>
                                      <select id="typeOfClasp" class="form-control required"  name="typeOfClasp">
                                      <option value="">Select Type of Clasp</option>
          <?php /* ?> <option value="{{$typeOfClaspSelected->id}}">{{$typeOfClaspSelected->type_of_clasp}}</option> <?php */ ?>
            @foreach ($type_of_clasp as $type_of_clasp)
                           <option value="{{$type_of_clasp->id}}" <?php if (isset($typeOfClaspSelected->id)) {
                                      if ($typeOfClaspSelected->id == $type_of_clasp->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$type_of_clasp->type_of_clasp}}</option>
                         @endforeach
                      </select>
                                    </div>
                                  </div>                           
                                </div>
                                <div class="row m-0">                                
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Clasp Material</label>
                                        <select id="claspMaterial" class="form-control required"  name="claspMaterial">
                                        <option value="">Select Clasp Material</option>
            <?php /* ?><option value="{{$claspMaterialSelected->id}}">{{$claspMaterialSelected->clasp_material}}</option>
            <?php */ ?>
            @foreach ($clasp_material as $clasp_material)
                           <option value="{{$clasp_material->id}}" <?php if (isset($claspMaterialSelected->id)) {
                                      if ($claspMaterialSelected->id == $clasp_material->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$clasp_material->clasp_material}}</option>
                         @endforeach
                      </select>                                      
                                    </div>
                                  </div>
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Case Material</label>
                                        <select id="caseMaterial" class="form-control required"  name="caseMaterial">
                                        <option value="">Select Case Material</option>
        <?php /* ?>   <option value="{{$caseMaterialSelected->id}}">{{$caseMaterialSelected->case_material}}</option> <?php */ ?>
            @foreach ($case_material as $case_material)
                           <option value="{{$case_material->id}}"  <?php if (isset($caseMaterialSelected->id)) {
                                      if ($claspMaterialSelected->id == $clasp_material->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$case_material->case_material}}</option>
                         @endforeach
                      </select>                                      
                                    </div>
                                  </div> 

                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Case MM</label>
                                        <select id="caseMM" class="form-control required"  name="caseMM">
                                        <option value="">Select Case MM</option>
        <?php /* ?>   <option value="{{$caseMMSelected->id}}">{{$caseMMSelected->case_mm}}</option> <?php */ ?>
            @foreach ($case_mm as $case_mm)
                           <option value="{{$case_mm->id}}" <?php if (isset($caseMMSelected->id)) {
                                      if ($caseMMSelected->id == $case_mm->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$case_mm->case_mm}}</option>
                         @endforeach
                      </select>                                       
                                    </div>
                                  </div>   
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Water Resistance Depth</label>
                                        <select id="wrd" class="form-control required"  name="wrd">
                                        <option value="">Select Water Resistance Depth</option>
        <?php /* ?>   <option value="{{$wrdSelected->id}}">{{$wrdSelected->water_resistant_depth}}</option> <?php */ ?>
            @foreach ($wrd as $wrd)
                           <option value="{{$wrd->id}}" <?php if (isset($wrdSelected->id)) {
                                      if ($wrdSelected->id == $wrd->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$wrd->water_resistant_depth}}</option>
                         @endforeach
                      </select>                                      
                                    </div>
                                  </div>  
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Glass Type</label>
                                        <select id="glassType" class="form-control required"  name="glassType">
                                        <option value="">Select Glass Type</option>
          <?php /* ?> <option value="{{$glassTypeSelected->id}}">{{$glassTypeSelected->glass_type}}</option> <?php */ ?>
            @foreach ($glass_type as $glass_type)
                           <option value="{{$glass_type->id}}" <?php if (isset($glassTypeSelected->id)) {
                                      if ($glassTypeSelected->id == $glass_type->id) {
                                      echo "selected";
                                      }
                                      }?>>{{$glass_type->glass_type}}</option>
                         @endforeach
                      </select>                                     
                                    </div>
                                  </div>                         
                                </div>
                                <h2 class="fs-title mt-5">Price Details</h2>
                                <hr class="my-2">
                                <div class="row m-0">
                                <div class="col-lg-4">
                                  <div class="form-group">
                                    <label class="form-control-label" for="input-last-name">Price Type*</label>
                                    <select class="form-control required"  name="pricetType" id="pricetType" >
                                      @foreach ($prices as $price)
                                      <option @if($priceSelected->id ?? '') @if($priceSelected->id==$price->id) selected @endif @endif value="{{$price->id}}">{{$price->price}}</option>
                                      @endforeach 
                                    </select>
                                  </div>
                                </div>

                              <div class="col-lg-4">
                                <div class="form-group">
                                  <label class="form-control-label"
                                  for="input-last-name">Currency*</label>
                                  <select id="currency" class="form-control required"  name="currency">
                                    @if($currency_data ?? '')
                                    @foreach ($currency_data as $currency)
                                    <option @if($currencySelected->id ?? '') @if($currencySelected->id==$currency->id) selected @endif @endif value="{{$currency->id}}">{{$currency->currency_code}}</option>
                                    @endforeach
                                    @endif
                                  </select>
                                </div>
                              </div>




                          
                          <div class="col-lg-4">                                   
                          <div class="form-group">
                          <label class="form-control-label"
                          for="input-last-name">Watch Price*</label>
                          @if($priceSelected->id == '3')
                          <input id="watch_price" class="form-control form-control-user required" type="number" min="0"  name="watch_price" placeholder="Enter Watch Price" value="{{$watch->watch_price}}" disabled>
                          @else
                          <input id="watch_price" class="form-control form-control-user required" type="number" min="0"  name="watch_price" placeholder="Enter Watch Price" value="{{$watch->watch_price}}" >
                          @endif                                   
                          </div>
                          </div>   




                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label" for="input-last-name">Commision</label>
                                      <div class="input-group mb-3">
                                        <input type="text" class="form-control" id="commision" name="commision" value="{{$watch->commision}}">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="form-control-label" for="input-last-name">Revenue</label>
                                      <div class="input-group mb-3">
                                        <input type="text" class="form-control" id="revenue" name="revenue" value="{{$watch->revenue}}">
                                      </div>
                                    </div>
                                  </div>
                                </div>  
                                <h2 class="fs-title mt-5"> Additional information (optional) </h2>
                                <hr class="my-2">                                                    
                                <div class="accordion-1">
                                  <div class="container">
                                    <div class="row">
                                      <div class="col-md-12 ml-auto">
                                        <div class="accordion my-3"
                                          id="accordionExample">
                                          <div class="card p-0 mb-1">
                                            <div class="card-header" id="headingOne">
                                              <h5 class="mb-0">
                                                <button class="btn btn-link w-100 text-primary text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" 
                                                aria-controls="collapseOne"> Features <i class="ni ni-bold-down float-right"></i>
                                              </button>
                                              </h5>
                                            </div>
                                            <div id="collapseOne"
                                              class="collapse show"
                                              aria-labelledby="headingOne"
                                              data-parent="#accordionExample">
                                              <div class="mt-3">
                                                <div class="row">
                                                  <div class="col-lg-6">
                                                    <div class="inner">
                                                    <?php echo $html1;?>
                                                    </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                    <div class="inner">
                                                    <?php echo $html2;?>
                                                    </div>
                                                    </div>
                                                    </div>
                                                                            
                                              </div>    
                                            </div>
                                          </div>
                                          <div class="card p-0 mb-1">
                                            <div class="card-header"
                                              id="headingTwo">
                                              <h5 class="mb-0">
                                                <button
                                                  class="btn btn-link w-100 text-primary text-left collapsed"
                                                  type="button"
                                                  data-toggle="collapse"
                                                  data-target="#collapseTwo"
                                                  aria-expanded="false"
                                                  aria-controls="collapseTwo">
                                                Additional Features
                                                <i
                                                  class="ni ni-bold-down float-right"></i>
                                                </button>
                                              </h5>
                                            </div>
                                            <div id="collapseTwo" class="collapse"
                                              aria-labelledby="headingTwo"
                                              data-parent="#accordionExample">
                                            
                                             <div class="row">                                             
                                              <div class="col-lg-12">
                                                <div class="additionalFeature">
                                                  <table class="table table-bordered">
                                                    <thead>
                                                    @if($additionalHtml)
                                                      <?php echo $additionalHtml; ?>
                                                      @endif
                                                      <tr>
                                                        <th>Àdd features
                                                        </th>
                                                        <th>Action</th>
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                      <tr>
                                                        <td><input type="text" id="additional-feature" name="additional_feature[]" class="form-control">
                                                        </td>
                                                        <td><button class="btn btn-success" type="button" id="addRow"><i class="fa fa-plus"></i></button>
                                                        </td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                </div>
                                              </div>
                                            </div> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            
                          </div>
                         
                        </div>
                        <div>
                        </div>
                        <input type="button" name="previous"
                          class="previous action-button-previous" value="Previous" />
                        <input type="button" name="next" id="next2" class="next action-button"
                          value="Next Step" />
                      </fieldset>
                      <fieldset>
                        <div class="form-card">
                          <h2 class="fs-title">Attach Photos</h2>
                          <p>Attach your watch photos here, you can drag the photos to the main </p>
                          <div class="row" >
                            <div class="col-lg-12">
                              <div class="inner">
                              
                                <div class="input-field">      
                                  <div class="input-images-1" id="input-images-1" style="padding-top: .5rem;"></div>
                                </div>
                                <div class="editNormalImages d-none">
                                @foreach($preloadImages as $images)
                                 <img id="{{$images->id}}" src="{{$images->src}}" alt="">
                                @endforeach
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row mt-5">
                            <hr class="my-2">
                            <div class="col-lg-12">
                              <div class="inner">
                                <h2 class="fs-title">Damage Photos</h2>
                                <p class="muted">Lorem ipsum is a dummy text, Lorem ipsum is a dummy text</p>
                                <div class="input-field">
                                  <div class="input-images-2" id="damagePhotos" style="padding-top: .5rem;"></div>
                                </div>
                                <div class="editDamageImages d-none">
                                @foreach($loadedDamageImages as $dImages)
                                 <img id="{{$dImages->id}}" src="{{$dImages->src}}" alt="">
                                @endforeach
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row mt-5">   
                            <div class="col-lg-12">
                              <h2 class="fs-title">Security Photos</h2>  
                            </div>      

                            <div class="col-lg-6">                             
                              <div class="inner security-photos">
                                <div class="row">
                                  <div class="col-lg-6">
                                    <div class="secutrity1">
                                      <img src="/assets/img/security-1.png" alt="">
                                    </div>
                                    <input type="hidden" class="digitalTime1" id="security-time1" name="security-time1"/>
                                    <div class="digitalTime digitalTime1" id="security-time1" name="security-time1">
                                      10:32
                                    </div>
                                  </div>
                                  <div class="col-lg-6">
                                    <div class="uploadImage">
                                      <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input type='file' id="security1" name="security1[]" class="imageUploadCls" accept=".png, .jpg, .jpeg" />
                                            <label for="security1"></label>
                                        </div>
                                        <div class="avatar-preview" id="security1-image">
                                        @if($security_images ?? '')
                                        @if(count($security_images)>0)
                                            <div class="imagePreview" @if($security_images[1]->id ?? '') id="{{$security_images[1]->id}}"@endif @if($security_images[0]->filename ?? '') style="background-image: url({{'/uploads/'.$security_images[0]->filename}});" @endif>
                                        @else
                                        <div class="imagePreview" style="background-image: url(img/watch.png);"> 
                                        @endif   
                                        @endif
                                            </div>
                                        </div>
                                    </div>  
                                    </div>
                                  </div>
                                </div>                     
                                <hr class="my-2">
                              </div>
                            </div>
                            <div class="col-lg-6">                           
                              <div class="inner security-photos">
                                <div class="row">
                                  <div class="col-lg-6">
                                    <div class="secutrity2">
                                      <img src="/assets/img/security-2.png" alt="">
                                    </div>
                                    <input type="hidden" class="digitalTime2" id="security-time2" name="security-time2"/>
                                    <div class="digitalTime digitalTime2" >
                                      10:32
                                    </div>
                                  </div>
                                  <div class="col-lg-6">
                                    <div class="uploadImage">
                                      <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input type='file' id="security2" name="security2[]" class="imageUploadCls" accept=".png, .jpg, .jpeg" />
                                            <label for="security2"></label>
                                        </div>
                                        <div class="avatar-preview" id="security2-image" >
                                          @if(count($security_images) >1)
                                            <div class="imagePreview" id="$security_images[1]->id" style="background-image: url({{'/uploads/'.$security_images[1]->filename}});">
                                        @else
                                        <div class="imagePreview" style="background-image: url(img/watch.png);"> 
                                        @endif
                                            </div>
                                        </div>
                                    </div>  
                                    </div>
                                  </div>
                                </div>                     
                                <hr class="my-2">
                              </div>
                            </div>
                          </div>
                          <div class="row mt-5">
                            <hr class="my-2">
                            <div class="col-lg-12">
                              <div class="inner">
                                <h2 class="fs-title">Comments</h2>
                                <hr class="my-2">
                                <label>
                                Setting aside the effort to portray special and
                                recognizing highlights about your vehicle can
                                have the effect in catching a purchaser's
                                consideration.
                                </label>
                                <div class="form-group">
    
                                  <textarea rows="4" class="form-control" id="comment" name="comment"
                                    placeholder="Imagine driving on the rough terrain in this 2012 ACE Cycle-Car. 34 km on the clock only. It is exceptional value at $24. 
                                    Only travelled 34 km. Don't let this go at this price!.">
                                    @if($comment_data->comment ?? '') {{strip_tags($comment_data->comment)}} @endif
                                    </textarea>
                                </div>
                              </div>
                            </div>
                          </div>                         
                        </div>
                        <input type="button" name="previous"
                          class="previous action-button-previous" value="Previous" />
                        <input type="button" name="next3" id="next3" class="next action-button"
                          value="Next Step" />
                      </fieldset>   
                      <!--Personal Details-->
                      <fieldset>
                        <div class="form-card">
                          <div class="">
                            <h2 class="fs-title">Personal Information</h2>
                            <p class="muted">Please review your personal information.</p>
                            <hr class="my-2">
                            <div class="row">
                              <div class="col-lg-12">
                                <div class="row m-0">
                                  <div class="col-lg-7">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">First Name*</label>
                                        <?php if($PersonalInformation->name ?? ''){ $name = explode(' ', $PersonalInformation->name); } ?>
                                      <div class="input-group mb-3">                                        
                                        <input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name" value="@if($name['0'] ?? ''){{$name['0']}}@endif">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-lg-7">
                                    <div class="form-group">
                                      <label class="form-control-label" for="input-country">Last Name*</label>
                                      <input type="text" class="form-control"  id="lastName" name="lastName" placeholder="Last Name" value="@if($name['1'] ?? ''){{$name['1']}}@endif">
                                    </div>
                                  </div>
                                  <div class="col-lg-7">
                                    <div class="form-group">
                                      <label class="form-control-label" for="input-email">Email address</label>
                                      <input type="email" id="inputemail" name="email" class="form-control" placeholder="Email Address" value="@if($PersonalInformation->email ?? ''){{$PersonalInformation->email}}@endif">
                                    </div>
                                  </div>
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="form-control-label" for="input-number">Phone Number</label>
                                      <input type="text" id="inputphone" name="phoneNumber" class="form-control" placeholder="Mobile Num" value="@if($PersonalInformation->phone ?? ''){{$PersonalInformation->phone}}@endif">
                                    </div>
                                    @error('phoneNumber')
                                        <div class="alert alert-danger">This Field is required</div>
                                     @enderror
                                  </div>                     
                                </div>

<!--                                 <h2 class="fs-title mt-5">Bank Details</h2>
                                <hr class="my-2">
                                <div class="row m-0">
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="bankName">Bank Name*</label>
                                        <input type="text" class="form-control" id="bankName" name="bankName"  placeholder="Bank Name" value="@if($PersonalInformation->bankName ?? ''){{$PersonalInformation->bankName}}@endif">
                                    </div>
                                  </div> 
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="beneficiaryName">Beneficiary Name*</label>
                                        <input type="text" class="form-control" id="beneficiaryName" name="beneficiaryName"  placeholder="Beneficiary Name" value="@if($PersonalInformation->beneficiaryName ?? ''){{$PersonalInformation->beneficiaryName}}@endif">
                                    </div>
                                  </div> 


                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="bankAccount">Bank Account*</label>
                                        <input type="text" class="form-control" id="bankAccount" name="bankAccount"  placeholder="Bank Account" value="@if($PersonalInformation->bankAccount ?? ''){{$PersonalInformation->bankAccount}}@endif">
                                    </div>
                                  </div> 

                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="ifsc">IFSC Code*</label>
                                        <input type="text" class="form-control" id="ifsc" name="ifsc"  placeholder="IFSC Code" value="@if($PersonalInformation->ifsc ?? ''){{$PersonalInformation->ifsc}}@endif">
                                    </div>
                                  </div> 
                                  </div> -->
                                <h2 class="fs-title mt-5">Address</h2>
                                <hr class="my-2">
                                <div class="row m-0">
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Address*</label>
                                        <input type="text" class="form-control" id="address" name="address"  placeholder="Address" value="@if($PersonalInformation->address1 ?? ''){{$PersonalInformation->address1}}@endif">
                                    </div>
                                  </div> 
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Street Line 2</label>
                                        <input type="text" class="form-control"  id="street_2" name="street_2"  placeholder="Streen Line 2" value="@if($PersonalInformation->address2 ?? ''){{$PersonalInformation->address2}}@endif">
                                    </div>
                                  </div>   
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Zip Code*</label>
                                        <input type="text" class="form-control"  id="zipcode" name="zipcode"  placeholder="Zip Code" value="@if($PersonalInformation->zip ?? ''){{$PersonalInformation->zip}}@endif">
                                    </div>
                                  </div> 
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-last-name">Select Country*</label>
                                        <select class="form-control" data-trigger name="country_pi" id="country_pi">
                                        <option value="">Select Country</option>
                                        @if($country_data ?? '')
                                        @foreach ($country_data as $country)
                                         <option @if($PersonalInformation->country ?? '')@if($country->id==$PersonalInformation->country) selected @endif @endif value="{{$country->id}}">{{$country->country_name}}</option>
                                       @endforeach
                                       @endif
                                        </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                    <label class="form-control-label" for="input-last-name">Select State</label>
                                    <select class="form-control" data-trigger name="state_pi" id="state_pi">
                                        <option  value="">Select State</option>
                                    </select>
                                    <input type="hidden" name="hidestate" id="hidestate" value="@if($PersonalInformation->state ?? ''){{$PersonalInformation->state}}@endif">
                                    </div>
                                  </div>
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="form-control-label" for="input-last-name">Select City*</label>
                                                <select class="form-control" data-trigger name="city_pi" id="city_pi">
                                                    
                                @if($cityId ?? '')
                                <option value="{{$cityId}}">{{$city_data->firstWhere('id',$cityId )->city_name}}</option>
                                @else
                                <option value="">Select City</option>
                                @endif
                                </select>
                                    <input type="hidden" name="hidecity" id="hidecity" value="@if($PersonalInformation->city ?? ''){{$PersonalInformation->city}}@endif">
                                    </div>
                                  </div>
                   
                                </div>
                                <h2 class="fs-title mt-5">Upload Document</h2>
                                <hr class="my-2">
                                <div class="row m-0">
                                  <div class="col-lg-12">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Upload Your License</label>
                                        <div class='file-input'>
                                          <input class="custom-file-input" type='file' name="license" id="license">
                                          <?php foreach($watchDOC as $doc){
                                              if($doc->type=='license'){
                                                $license_name = $doc->filename;
                                              }
                                              if($doc->type=='drivingLicense'){
                                                $drivingLicense_name = $doc->filename;
                                              }
                                              if($doc->type=='utilityBill'){
                                                $utilityBill_name = $doc->filename;
                                              }
                                              if($doc->type=='passport'){
                                                $passport_name = $doc->filename;
                                              }
                                          }?>
                                          @if($license_name ?? '')
                                          <span class='choose-btn'>{{$license_name}}</span>
                                          @else
                                          <span class='choose-btn'>Choose</span>
                                          @endif
                                          <span class="clear-btn" data-clear-input>&times;</span>
                                        </div>
                                    </div>
                                  </div>
                                  <div class="col-lg-12">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Upload Your Driving License</label>
                                        <div class='file-input'>
                                          <input class="custom-file-input" type='file' name="drivingLicense" id="drivingLicense">
                                          @if($drivingLicense_name ?? '')
                                          <span class='choose-btn'>{{$drivingLicense_name}}</span>
                                          @else
                                          <span class='choose-btn'>Choose</span>
                                          @endif
                                          <span class="clear-btn" data-clear-input>&times;</span>
                                        </div>
                                    </div>
                                  </div>
                                  <div class="col-lg-12">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Upload Your Utility Bill</label>
                                        <div class='file-input'>
                                          <input class="custom-file-input" type='file' name="utilityBill" id="utilityBill">
                                          @if($utilityBill_name ?? '')
                                          <span class='choose-btn'>{{$utilityBill_name}}</span>
                                          @else
                                          <span class='choose-btn'>Choose</span>
                                          @endif
                                          <span class="clear-btn" data-clear-input>&times;</span>
                                        </div>
                                    </div>
                                  </div>

                                  <div class="col-lg-12">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Upload Your Passport*</label>
                                        <div class='file-input'>
                                          <input class="custom-file-input" type='file' id="passport" name="passport">
                                          @if($passport_name ?? '')
                                          <span class='choose-btn'>{{$passport_name}}</span>
                                          @else
                                          <span class='choose-btn'>Choose</span>
                                          @endif
                                          <span class="clear-btn" data-clear-input>&times;</span>
                                        </div>
                                        <!--Uploded file is here-->
<!-- 
                                        <label class="form-control-label"
                                        for="input-last-name">Upload File Name : </label> -->
                                    </div>
                                  </div>  
                                  <span id="documentError" style="color:red;"></span>
                                  <?php if($license_name ?? '') { $val=1; }

                                  elseif($drivingLicense_name ?? '') { $val=1; } 
                                   elseif($passport_name ?? '') { $val=1; } 
                                  ?>
                                 <input type="hidden" id="anyOneDocumentUploaded" name="anyOneDocumentUploaded" @if($val ?? '') value="1" @endif>
                                </div>
                              </div>                             
                            </div>                            
                          </div>                         
                        </div>
                        <div>
                        </div>
                        <input type="button" name="previous"
                          class="previous action-button-previous" value="Previous" />
                        <input type="button" name="next" class="next action-button" id="next4" value="Next Step" />
                      </fieldset> 
                      
                      <fieldset>
                        <div class="form-card">
                        <div class="col-lg-12">
                        <div class="search-result">
                            <div class="details-box row mt-4 m-0">
                                <div class="col-lg-12 text-left">
                                    <h5>Ads Information</h5>
                                </div>
                                <div class="col-md-12">
                                    <div
                                        class="stm-listing-single-price-title heading-font clearfix text-left">
                                        <div class="price" id="priceShow">$18,000</div>
                                        <div class="stm-single-title-wrap">
                                            <h1 class="title" id="title">
                                                Rolex GT Matrix 2
                                            </h1>
                                        </div>
                                    </div>
                                    <div class="row">                                       
                                        <div class="col-lg-6">
                                            <p>Normal Images</p>
                                            <div id="imgGallery" class="d-flex flex-wrap">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                          <p>Damage Images</p>
                                          <div id="dmgGallery" class="d-flex flex-wrap">
                                          </div>
                                      </div>
                                    </div>
                                    <!-- End single-product-images -->                                   
                                    <div class="watch-desc">

                                      <div class="stm-border-top-unit bg-color-border" id="commentShow">
                                          <h5><strong>Description</strong></h5>
                                      </div>
          
                                      <div class="stm-border-top-unit bg-color-border">
                                          <h5><strong>Basic Details</strong></h5>
                                      </div>
                                      <div class="stm-single-listing-car-features watch-details">
                                          <div class="lists-inline">
                                              <ul class="list-style-2">
                                                  <li id="brandShow">Brand:   Rolex</li>
                                                  <li id="modelShow">Model: GMT-Master II</li>
                                              </ul>
                                              <ul class="list-style-2">
                                                  <li id="watchConditionShow">Condition: New</li>
                                                  <li id="movementShow">Movement: </li>
                                              </ul>
                                              <ul class="list-style-2">
                                                  <li id="caseMaterialShow">Case material: Steel</li>
                                                  <li id="braceletMaterialShow">Bracelet material: Stainless-Steel</li>
                                              </ul>
                                              <ul class="list-style-2">
                                                  <li id="yomShow">Year of production: </li>
                                                  <li id="genderShow">Gender: Women's</li>
                                              </ul>
                                              <ul class="list-style-2">
                                                  <li id="locationShow">Location: India,  Delhi</li>
                                                  <li id="dialColorShow">Dial Color: Gold</li>
                                              </ul>
                                          </div>
                                      </div>
                                      <!-- Start of the report section -->          
          
                                  </div>
                                </div>
                            </div>
                            <!-- <div class="row mt-5">
                              <div class="col-lg-12">
                                <div class="choosePaymmentoption">
                                  <div class="form-group"> 
                                    <label class="radio-inline"> 
                                      <input type="radio" name="postAds" checked="" value="free">Post free ads </label> 
                                      <label class="radio-inline"> <input type="radio" name="postAds" value="paid" class="ml-5">Upgrade Ads for 3 months 
                                    </label>
                                </div>
                                </div>
                              </div>
                            </div> -->
                        </div>
                        </div>
                        </div>
                        <div class="row form-group">
                    <div class="col-md-12">
                        <div class="text-danger font-italic generic-errors-top"></div>
                    </div>
                </div>
                        <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                        <input type="submit"  class="next action-button"  value="Submit" />
                        <!-- <input id="postNow" type="button" name="postnow" class="btn btn-success action-button" value="Post ads Now" /> -->
                    </fieldset>  
                    
                                                  
                    </form>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    
        @endauth
   @endif 
   
    @include('agent.common.footer')
     <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/image-uploader.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/local.js"></script>
    <script src="https://js.stripe.com/v3/"></script>


    <script>
    $(window).on('load',function(){
        $('#myModal2').modal({backdrop: 'static', keyboard: false});
    });
      // steps JS
      $(document).ready(function () {

        $(".custom-file-input").on("change", function(){
          var fileName = $(this)[0].files[0].name;
          $(this).next(".choose-btn").html(fileName);
        });

        $(".clear-btn").click(function(){
          $(this).siblings(".custom-file-input").val('');
          $(this).siblings(".choose-btn").html('Choose');
        });



                $('#loading-image').show();
                  var countryId = $('#country_pi').val();
                  var hidestate = $('#hidestate').val();
                  var hidecity = $('#hidecity').val();
                  $.ajax({
                    url: "/getState",
                    type: "POST",
                    data: {countryId: countryId, _token: '{{csrf_token()}}' },
                    dataType: 'json',
                    success : function(data){

                      var sele ='';
                      var html = `<option value="">Select State</option>`;
                      for (var i = 0; i < data.length; i++) {
                          var id = data[i].id;
                          var name = data[i].state_name;
                          if(hidestate==data[i].id){
                            sele = 'selected';
                          }
                          else{
                              sele = '';
                          }
                          var option = `<option ${sele} value="${data[i].id}">${data[i].state_name}</option>`;
                          html += option;
                        }
                        $('#state_pi').html(html); 
                      }
                    });
                  $.ajax({
                url: "/getCity",
                type: "POST",
                data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
                dataType: 'json',
                success : function(data){
                  var cit = '';
                  var html = `<option value="">Select City</option>`;
                  for (var i = 0; i < data.length; i++) {
                      var id = data[i].id;
                      var name = data[i].city_name;
                      if(hidecity == data[i].id){
                        cit ='selected';
                      }
                      else{
                        cit = '';
                      }
                      var option = `<option ${cit} value="${data[i].id}">${data[i].city_name}</option>`;
                      html += option;
                    }
                  $('#loading-image').hide();
                    $('#city_pi').html(html); 
                  }
                });


      $('#watch_price').keyup(function() {
      var price = this.value;
      var discount = (1.5/100) * price;
      //console.log(discount);
      $('#commision').val(discount);

      var revenue = price-discount;
      $('#revenue').val(revenue);

      });
    $('#passport, #utilityBill, #license, #drivingLicense').on('change', function() { 
    // select the form and submit
    $("#anyOneDocumentUploaded").val(1);
    });
        $("#countryId").change(function(){
            $('#loading-image').show();
              var countryId = $(this).val();
              $.ajax({
                url: "/getState",
                type: "POST",
                data: {countryId: countryId, _token: '{{csrf_token()}}' },
                dataType: 'json',
                success : function(data){
                  var html = `<option value="">Select State</option>`;
                  for (var i = 0; i < data.length; i++) {
                      var id = data[i].id;
                      var name = data[i].state_name;
                      var option = `<option value="${data[i].id}">${data[i].state_name}</option>`;
                      html += option;
                    }
                    $('#stateId').html(html); 
                  }
                });
              $.ajax({
            url: "/getCity",
            type: "POST",
            data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
            dataType: 'json',
            success : function(data){
              var html = `<option value="">Select City</option>`;
              for (var i = 0; i < data.length; i++) {
                  var id = data[i].id;
                  var name = data[i].city_name;
                  var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
                  html += option;
                }
              $('#loading-image').hide();
                $('#cityId').html(html);  
              }
            });
            });
              $("#stateId").change(function(){
              var stateId = $(this).val();
              $('#loading-image').show();
              $.ajax({
                url: "/getCity",
                type: "POST",
                data: {stateId: stateId, _token: '{{csrf_token()}}' },
                dataType: 'json',
                success : function(data){
                  var html = `<option value="">Select City</option>`;
                  for (var i = 0; i < data.length; i++) {
                      var id = data[i].id;
                      var name = data[i].city_name;
                      var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
                      html += option;
                    }
                  $('#loading-image').hide();
                    $('#cityId').html(html);  
                  }
                });
            });

              $("#country_pi").change(function(){
                $('#loading-image').show();
                  var countryId = $(this).val();
                  $.ajax({
                    url: "/getState",
                    type: "POST",
                    data: {countryId: countryId, _token: '{{csrf_token()}}' },
                    dataType: 'json',
                    success : function(data){
                      var html = `<option value="">Select State</option>`;
                      for (var i = 0; i < data.length; i++) {
                          var id = data[i].id;
                          var name = data[i].state_name;
                          var option = `<option value="${data[i].id}">${data[i].state_name}</option>`;
                          html += option;
                        }
                        $('#state_pi').html(html); 
                      }
                    });
                  $.ajax({
                url: "/getCity",
                type: "POST",
                data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
                dataType: 'json',
                success : function(data){
                  var html = `<option value="">Select City</option>`;
                  for (var i = 0; i < data.length; i++) {
                      var id = data[i].id;
                      var name = data[i].city_name;
                      var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
                      html += option;
                    }
                  $('#loading-image').hide();
                    $('#city_pi').html(html); 
                  }
                });
                });
                  $("#state_pi").change(function(){
                  var stateId = $(this).val();
                  $('#loading-image').show();
                  $.ajax({
                    url: "/getCity",
                    type: "POST",
                    data: {stateId: stateId, _token: '{{csrf_token()}}' },
                    dataType: 'json',
                    success : function(data){
                      var html = `<option value="">Select City</option>`;
                      for (var i = 0; i < data.length; i++) {
                          var id = data[i].id;
                          var name = data[i].city_name;
                          var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
                          html += option;
                        }
                      $('#loading-image').hide();
                        $('#city_pi').html(html);  
                      }
                    });
                });

        $("#brand").change(function(){
            console.log('------------------------');
            var brandId = $(this).val();
            
            $('#loading-image').show();
            $.ajax({
              url: "/getWatchModel",
              type: "POST",
              data: {brandId: brandId, _token: '{{csrf_token()}}' },
              dataType: 'json',
              success : function(data){
                var html = `<option value="">Select Model</option>`;
                for (var i = 0; i < data.length; i++) {
                    var option = `<option value="${data[i].id}">${data[i].watch_model_name}</option>`;
                    html += option;
                  }
                $('#loading-image').hide();
                  $('#model').html(html); 
                }
              });
          });

          
          var current_fs, next_fs, previous_fs; //fieldsets
          var opacity;
          var ImgArray = [];
          var ImgArrayDamage = []; 
          const imageArrayfn = function () {
              ImgArray = [];
              ImgArrayDamage = [];
              $('.uploaded img').each(function (index, value) {
                if($(this).parents('#damagePhotos').length > 0){
                  ImgArrayDamage.push($(this).attr('src'));
                }
                else{
                  ImgArray.push($(this).attr('src'));
                }
              });
              imgUploadedfn();
          }
      
          const imgUploadedfn = function () {
              let imgCount = ImgArray.length;
              let imgCountDmg = ImgArrayDamage.length;
              let html = '';
              let imgGallery = '';
              let dmgGallery= '';
              
              if (imgCount > 0) {
                  for (let j = 0; j < imgCount; j++) {                   
                      // html += ' <div class="item" data-src="' + ImgArray[j] + '"><img src="' + ImgArray[j] + '"></div>';
                      imgGallery += '<div><img src="' + ImgArray[j] + '" /></div>';
                  }
                  $('#imgGallery').empty();   
                  $('#imgGallery').append(imgGallery);      
                 
              }  
              if (imgCountDmg > 0) {
                  for (let i = 0; i < imgCountDmg; i++) { 
                    dmgGallery += '<div><img src="' + ImgArrayDamage[i] + '" /></div>';
                  }   
                  $('#dmgGallery').empty();               
                  $('#dmgGallery').append(dmgGallery);      
                  
              } 
                console.log('Images Array - > ', ImgArray)  ;  
                console.log('Damage Images Array - > ', ImgArrayDamage)  ;  
          }
          /* $(".next").click(function () {
              imageArrayfn();
              current_fs = $(this).parent();
              next_fs = $(this).parent().next();      
              //Add Class Active
              $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");      
              //show the next fieldset
              next_fs.show();
              //hide the current fieldset with style
              current_fs.animate({ opacity: 0 }, {
                  step: function (now) {
                      // for making fielset appear animation
                      opacity = 1 - now;
      
                      current_fs.css({
                          'display': 'none',
                          'position': 'relative'
                      });
                      next_fs.css({ 'opacity': opacity });
                  },
                  duration: 600
              });
          }); */      
          $(".previous").click(function () {
              current_fs = $(this).parent();
              previous_fs = $(this).parent().prev();
              //Remove class active
              $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");      
              //show the previous fieldset
              previous_fs.show();
              //hide the current fieldset with style
              current_fs.animate({ opacity: 0 }, {
                  step: function (now) {
                      // for making fielset appear animation
                      opacity = 1 - now;
      
                      current_fs.css({
                          'display': 'none',
                          'position': 'relative'
                      });
                      previous_fs.css({ 'opacity': opacity });
                  },
                  duration: 600
              });
          });
          $('.radio-group .radio').click(function () {
              $(this).parent().find('.radio').removeClass('selected');
              $(this).addClass('selected');
          });
          $(".submit").click(function () {
              return false;
          })
          var preloadImages = [];
          $(".editNormalImages img").each(function(){
            let imgObj={id:$(this).attr('id'),src:`http://${location.hostname}/${$(this).attr('src')}`};
            preloadImages.push(imgObj);
          });
          
          // console.log('preloadImages'+ JSON.stringify(preloadImages));
          $('.input-images-1').imageUploader({
            preloaded: preloadImages,
            imagesInputName: 'normalImages',
            preloadedInputName: 'preloaded',
            maxSize: 2 * 1024 * 1024,
            maxFiles: 10
          });
          var preloadImagesDamage = [];
          $(".editDamageImages img").each(function(){
            let imgObjDamage={id:$(this).attr('id'),src:`http://${location.hostname}/${$(this).attr('src')}`};
            preloadImagesDamage.push(imgObjDamage);
          });
         
          $('.input-images-2').imageUploader2({
            preloaded: preloadImagesDamage,
            imagesInputName: 'damageImages2',
            preloadedInputName: 'damageImages2',
            maxSize: 2 * 1024 * 1024,
            maxFiles: 10
          });
          $( ".uploaded" ).sortable();
          $( ".uploaded" ).disableSelection();

          var imgType ='';
          var imageId
          $('.delete-image').on('click',function(){
            imgType = 'normalPhoto'
            imageId = $(this).parents('.ui-sortable-handle').find('input').val();    
            callAjax(imgType,imageId);       
          });
          $('.delete-image2').on('click',function(){
            imgType = 'damagePhotos'
            imageId = $(this).parents('.ui-sortable-handle').find('input').val();     
            callAjax(imgType,imageId);         
          });
          function callAjax(imgType,imageId){
           //Call you ajax here 
        console.log('Image type----- ',imgType);
              alert('we are in ajax call method');

              $.ajax({
                url: "/deletewatchadsimage/" +imageId+ "/" +imgType,
                type: "get",
                success : function(data){
                  console.log('response', data);
                  }
                });
          }

        //Timer Array
          const timerArray = [
            ['security-1.png','10:10'],
            ['security-2.png','12:16'],
            ['security-3.png','01:22'],
            ['security-4.png','01:30'],
            ['security-5.png','01:45'],
            ['security-6.png','02:34'],
            ['security-7.png','03:42'],
            ['security-8.png','04:50'],
            ['security-9.png','06:08'],
            ['security-10.png','05:14'],
            ['security-11.png','07:14'],
            ['security-12.png','08:24'],
            ['security-13.png','09:36'],
            ['security-14.png','10:42'],
            ['security-15.png','11:52'],
            ['security-16.png','12:46'],
            ['security-17.png','01:51'],
            ['security-18.png','02:50'],
            ['security-19.png','05:04'],
            ['security-20.png','08:15']            
            ]     
            //Setting First clock Time            
            let RandomIndexNumber = Math.floor(Math.random() * 20);            
            let RandomImageSecurity1 = timerArray[RandomIndexNumber][0];
            let RandomTimeSecurity1 = timerArray[RandomIndexNumber][1];
            $('.secutrity1 img').attr('src','/assets/img/security-images/'+RandomImageSecurity1+'');
            $('.digitalTime1').text(RandomTimeSecurity1);
            $('.digitalTime1').val(RandomTimeSecurity1);

            //Setting second clock Time
            let RandomIndexNumber2 = Math.floor(Math.random() * 20);
            let RandomImageSecurity2 = timerArray[RandomIndexNumber2][0];
            let RandomTimeSecurity2 = timerArray[RandomIndexNumber2][1];
            $('.secutrity2 img').attr('src','/assets/img/security-images/'+RandomImageSecurity2+'');
            $('.digitalTime2').text(RandomTimeSecurity2);
            $('.digitalTime2').val(RandomTimeSecurity2);  
          
          var isError = false;
       $("#next1").click(function(){
        var make = $('#brand').val();
        var model = $('#model').val();

        console.log('Make ',$('#brand').val());
        console.log('Model ',$('#model').val());
        
         /* if(make == ""){
          console.log('lalalalaalal');
          $('#brand').css('border','1px solid red');
          isError = true;
          }else{
            isError = false;
            }
        if(model == ""){
          $('#model').css('border','1px solid red');
          isError = true;
          }else{
            isError = false;
            }  */
        

           if(isError == false){
            current_fs = $('#next1').parent();
      next_fs = $('#next1').parent().next();

      //Add Class Active
      $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

      //show the next fieldset
      next_fs.show();
      //hide the current fieldset with style
      current_fs.animate({opacity: 0}, {
      step: function(now) {
      // for making fielset appear animation
      opacity = 1 - now;

      current_fs.css({
      'display': 'none',
      'position': 'relative'
      });
      next_fs.css({'opacity': opacity});
      },
      duration: 600
      });
               }
       });

      $("#pricetType").change(function(){ 
        var htmlcarPrice = '';
        var htmlPrice = '';
         if($(this).val() == 3){
          htmlcarPrice += '<input id="watch_price" class="form-control form-control-user " type="number"  min="0" watch_price="price" placeholder="Enter Watch Value" value="" disabled>';
          /* htmlPrice = '<select id="pricetType" class="form-control "  name="pricetType" disabled> <option value="">Select Price</option>'; */
        $("#watch_price").replaceWith(htmlcarPrice);
        /* $("#pricetType").replaceWith(htmlPrice); */  
            }else{
             htmlcarPrice += '<input id="watch_price" class="form-control form-control-user" type="number"  min="0" name="watch_price" placeholder="Enter Watch Value" value="">';
             /* htmlPrice = '<select id="pricetType" class="form-control "  name="pricetType"> <option value="">Select Price</option> @foreach ($prices as $price) <option value="{{$price->id}}">{{$price->price}}</option> @endforeach </select>'; */
          $("#watch_price").replaceWith(htmlcarPrice);
          /* $("#pricetType").replaceWith(htmlPrice); */  
                   } 
         
     });
       
      $("#next2").click(function(){
    
       var currency = $('#currency').val();
       var country = $('#countryId :selected').val();
        var city = $('#cityId :selected').val();
        var priceType = $('#pricetType :selected').val();
        var price = $('#price').val();
        var caseDiameter = $('#case_diameter :selected').val();
        var movement = $('#movement :selected').val();
        var type = $('#type :selected').val();
        var gender = $('#gender :selected').val();
        var inclusions = $('#inclusions').val();
        var reference_num = $('#reference_num').val();
        var watchCondition = $('#watchCondition :selected').val();
        var status = $('#status :selected').val();

        var powerReserve = $('#powerReserve :selected').val();
        var dialColor = $('#dialColor :selected').val();
        var braceletMaterial = $('#braceletMaterial :selected').val();
        var bezelMaterial = $('#bezelMaterial :selected').val();
        var braceletColor = $('#braceletColor :selected').val();
        var typeOfClasp = $('#typeOfClasp :selected').val();
        var claspMaterial = $('#claspMaterial :selected').val();
        var caseMaterial = $('#caseMaterial :selected').val();
        var caseMM = $('#caseMM :selected').val();
        var wrd = $('#wrd :selected').val();
        var glassType = $('#glassType :selected').val();
        var timezone = $('#timezone').val();


       
        /* if(ad_title == ""){
          $('#adtitle').css('border','1px solid red');
          isError = true;
          }else{
            isError = false;
            }
        if(currency == ""){
          $('#currency').css('border','1px solid red');
          isError = true;
          }else{
            isError = false;
            }
        if(country == ""){
            $('#countryId').css('border','1px solid red');
            isError = true;
            }else{
              isError = false;
              }
           if(city == ""){
            $('#cityId').css('border','1px solid red');
            isError = true;
            }else{
              isError = false;
              }
           if(caseDiameter == ""){
            $('#case_diameter').css('border','1px solid red');
            isError = true;
            }else{
              isError = false;
              } 
           if(price == ""){
            $('#price').css('border','1px solid red');
            isError = true;
            }else{
              isError = false;
              }
           if(priceType == ""){
            $('#pricetType').css('border','1px solid red');
            isError = true;
            }else{
              isError = false;
              }
           if(gender == ""){
          $('#gender').css('border','1px solid red');
          isError = true;
          }else{
            isError = false;
            }
           if(inclusions == ""){
            $('#inclusions').css('border','1px solid red');
            isError = true;
            }else{
              isError = false;
              }   
           if(reference_num == ""){
            $('#reference_num').css('border','1px solid red');
            isError = true;
            }else{
              isError = false;
              } 
           if(watchCondition == ""){
            $('#watchCondition').css('border','1px solid red');
            isError = true;
            }else{
              isError = false;
              } 
           if(status == ""){
            $('#status').css('border','1px solid red');
            isError = true;
            }else{
              isError = false;
              }
           if(timezone == ""){
          $('#timezone').css('border','1px solid red');
          isError = true;
          }else{
            isError = false;
            }
             if(powerReserve == ""){
          $('#powerReserve').css('border','1px solid red');
          isError = true;
          }else{
            isError = false;
            }
        if(dialColor == ""){
              $('#dialColor').css('border','1px solid red');
              isError = true;
              }else{
                isError = false;
                }
        if(braceletMaterial == ""){
                  $('#braceletMaterial').css('border','1px solid red');
                  isError = true;
                  }else{
                    isError = false;
                    }
        if(bezelMaterial == ""){
                      $('#bezelMaterial').css('border','1px solid red');
                      isError = true;
                      }else{
                        isError = false;
                        }
        if(braceletColor == ""){
                          $('#braceletColor').css('border','1px solid red');
                          isError = true;
                          }else{
                            isError = false;
                            }
        if(typeOfClasp == ""){
                              $('#typeOfClasp').css('border','1px solid red');
                              isError = true;
                              }else{
                                isError = false;
                                }
        if(claspMaterial == ""){
                                  $('#claspMaterial').css('border','1px solid red');
                                  isError = true;
                                  }else{
                                    isError = false;
                                    }
        if(caseMaterial == ""){
                                      $('#caseMaterial').css('border','1px solid red');
                                      isError = true;
                                      }else{
                                        isError = false;
                                        }
        if(caseMM == ""){
                                          $('#caseMM').css('border','1px solid red');
                                          isError = true;
                                          }else{
                                            isError = false;
                                            }
          
           if(wrd == ""){
            $('#wrd').css('border','1px solid red');
              isError = true;
              }else{
                isError = false;
                }
      if(glassType == ""){
        $('#glassType').css('border','1px solid red');
          isError = true;
          }else{
            isError = false;
            } */

       if(isError == false){
        current_fs = $('#next2').parent();
      next_fs = $('#next2').parent().next();

      //Add Class Active
      $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

      //show the next fieldset
      next_fs.show();
      //hide the current fieldset with style
      current_fs.animate({opacity: 0}, {
      step: function(now) {
      // for making fielset appear animation
      opacity = 1 - now;

      current_fs.css({
      'display': 'none',
      'position': 'relative'
      });
      next_fs.css({'opacity': opacity});
      },
      duration: 600
      });
           }
   });

      $("#next3").click(function(){
        var comment = $('#comment').val();

          /* if(comment == ""){
        $('#comment').css('border','1px solid red');
        isError = true;
        }else{
          isError = false;
              }
        if($('.uploaded-image').length == 0){
        $('#input-images-1').css('border','1px solid red');
        isError = true;
        }else{
          isError = false;
          }

        if($('.uploaded-image').length == 0){
        $('#damagePhotos').css('border','1px solid red');
        isError = true;
        }else{
          isError = false;
          }
         if($('.uploaded-image').length == 0){
        $('#security1-image').css('border','1px solid red');
        isError = true;
        }else{
          isError = false;
          }
        if($('.uploaded-image').length == 0){
        $('#security2-image').css('border','1px solid red');
        isError = true;
        }else{
          isError = false;
          }  */

       if(isError == false){
        current_fs = $('#next3').parent();
      next_fs = $('#next3').parent().next();

      //Add Class Active
      $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

      //show the next fieldset
      next_fs.show();
      //hide the current fieldset with style
      current_fs.animate({opacity: 0}, {
      step: function(now) {
      // for making fielset appear animation
      opacity = 1 - now;

      current_fs.css({
      'display': 'none',
      'position': 'relative'
      });
      next_fs.css({'opacity': opacity});
      },
      duration: 600
      });
           }
   });

      $('#next4').click(function() {
      var isError = false;
      var firstName = $('#firstName').val();
      var lastName = $('#lastName').val();
      var email = $('#inputemail').val();
      var phone = $('#inputphone').val();
      var bankName = $('#bankName').val();
      var beneficiaryName = $('#beneficiaryName').val();
      var bankAccount = $('#bankAccount').val();
      var ifsc = $('#ifsc').val();
      var address = $('#address').val();
      var street_2 = $('#street_2').val();
      var zipcode = $('#zipcode').val();
      var country_pi = $('#country_pi').val();
      var state_pi = $('#state_pi').val();
      var city_pi = $('#city_pi').val();


      if(firstName == ""){
        $('#firstName').css('border','1px solid red');
        isError = true;
      }
      if(lastName == ""){
        $('#lastName').css('border','1px solid red');
        isError = true;
      }
      if(email == ""){
        $('#inputemail').css('border','1px solid red');
        isError = true;
      }
      if(phone == ""){
        $('#inputphone').css('border','1px solid red');
        isError = true;
      }
      // if(bankName == ""){
      //   $('#bankName').css('border','1px solid red');
      // isError = true;
      // }
      // if(beneficiaryName == ""){
      //   $('#beneficiaryName').css('border','1px solid red');
      // isError = true;
      // }
      // if(bankAccount == ""){
      //   $('#bankAccount').css('border','1px solid red');
      //   isError = true;
      // }
      // if(ifsc == ""){
      //   $('#ifsc').css('border','1px solid red');
      //   isError = true;
      // }
      if(address == ""){
        $('#address').css('border','1px solid red');
        isError = true;
      }
      if(street_2 == ""){
        $('#street_2').css('border','1px solid red');
        isError = true;
      }
      if(zipcode == ""){
        $('#zipcode').css('border','1px solid red');
        isError = true;
      }
      if(country_pi == ""){
        $('#country_pi').css('border','1px solid red');
        isError = true;
      }
      if(state_pi == ""){
        $('#state_pi').css('border','1px solid red');
        isError = true;
      }
      if(city_pi == ""){
        $('#city_pi').css('border','1px solid red');
        isError = true;
      }
      var anyDocument = $("#anyOneDocumentUploaded").val();

      if(anyDocument != 1){
      $('#documentError').html('Please select at least one document');
      isError = true;
      }else{
        $('#documentError').html('');
      }

       if(isError == false){
          // imageArrayfn();
          
          current_fs = $('#next4').parent();
          next_fs = $('#next4').parent().next();

          //Add Class Active
          $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

          //show the next fieldset
          next_fs.show();
          //hide the current fieldset with style
          current_fs.animate({opacity: 0}, {
          step: function(now) {
          // for making fielset appear animation
          opacity = 1 - now;

          current_fs.css({
          'display': 'none',
          'position': 'relative'
          });
          next_fs.css({'opacity': opacity});
          },
          duration: 600
          });
          }
       
         });
      $('#next5').click(function() {
       
        if(!$('#agb').prop('checked')){
         $('.generic-errors-top').html('The terms conditions must be accepted.');
         isError = true;
      }else{
        isError = false;
        } 
       
       if(isError == false){
          //imageArrayfn();
           current_fs = $('#next5').parent();
            next_fs = $('#next5').parent().next();

            //Add Class Active
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
            step: function(now) {
            // for making fielset appear animation
            opacity = 1 - now;

            current_fs.css({
            'display': 'none',
            'position': 'relative'
            });
            next_fs.css({'opacity': opacity});
            },
            duration: 600
            });

         }
       
         });
      });   
// Security Upload Image Script
$(".imageUploadCls").change(function() {  
  const security = $(this).attr('id');
    const imgeID = $(this).parents('.avatar-upload').find('.imagePreview').attr('id');  
    deleteSecurityImage(imgeID);
    if (this.files && this.files[0]) {
          let reader = new FileReader();
          reader.onload = function(e) {                 
              $('#'+security).parents('.avatar-upload').find('.imagePreview').css('background-image', 'url('+e.target.result +')');             
              $('#'+security).parents('.avatar-upload').find('.imagePreview').hide();
              $('#'+security).parents('.avatar-upload').find('.imagePreview').fadeIn(650);
          }
          reader.readAsDataURL(this.files[0]);
        }  
});

function deleteSecurityImage(imgeID){
    alert(imgeID);

    $.ajax({
          url: "/deletewatchadsimage/" +imgeID+ "/securityPhotos",
          type: "get",
          success : function(data){
            console.log('response', data);
            }
          });
  }

//activate the premium ads

var initialPrice = $('.ads-fee').text();
$('.activatethis').on('click',function(){           
  if($(this).parents('.thispackage').hasClass('activatedPackage')){
    $(this).parents('.thispackage').removeClass('activatedPackage');
    $(this).text('Activate');
    $('.packageName').text('Listing Fee');
    $('.ads-fee').text(initialPrice);
  }   
  else{
  $('.thispackage').removeClass('activatedPackage');
  $('.activatethis').text('Activate');
  $(this).text('Deactivate')
  $(this).parents('.thispackage').addClass('activatedPackage');        
  $('.packageName').text($('.activatedPackage h2').text());
  $('.ads-fee').text($('.activatedPackage .amount').text());

  }
})

//Create an instance of the Stripe object
  // Set your publishable API key
  var stripe = Stripe('{{ env("STRIPE_PUBLISH_KEY") }}');

  // Create an instance of elements
  var elements = stripe.elements();

  var style = {
      base: {
          fontWeight: 400,
          fontFamily: '"DM Sans", Roboto, Open Sans, Segoe UI, sans-serif',
          fontSize: '16px',
          lineHeight: '1.4',
          color: '#1b1642',
          padding: '.75rem 1.25rem',
          '::placeholder': {
              color: '#ccc',
          },
      },
      invalid: {
          color: '#dc3545',
      }
  };

  var cardElement = elements.create('cardNumber', {
      style: style
  });
  cardElement.mount('#card_number');

  var exp = elements.create('cardExpiry', {
      'style': style
  });
  exp.mount('#card_expiry');

  var cvc = elements.create('cardCvc', {
      'style': style
  });
  cvc.mount('#card_cvc');

  // Validate input of the card elements
  var resultContainer = document.getElementById('paymentResponse');
  cardElement.addEventListener('change', function (event) {
      if (event.error) {
          resultContainer.innerHTML = '<p>' + event.error.message + '</p>';
      } else {
          resultContainer.innerHTML = '';
      }
  });

  // Get payment form element
  var form = document.getElementById('msform');

  // Create a token when the form is submitted.
  form.addEventListener('submit', function (e) {
      e.preventDefault();
      createToken();
  });

  // Create single-use token to charge the user
  function createToken() {
      stripe.createToken(cardElement).then(function (result) {
          if (result.error) {
              // Inform the user if there was an error
              resultContainer.innerHTML = '<p>' + result.error.message + '</p>';
          } else {
              // Send the token to your server
              stripeTokenHandler(result.token);
          }
      });
  }

  
  // Callback to handle the response from stripe
  function stripeTokenHandler(token) {
      
      // Insert the token ID into the form so it gets submitted to the server
      var hiddenInput = document.createElement('input');
      hiddenInput.setAttribute('type', 'hidden');
      hiddenInput.setAttribute('name', 'stripeToken');
      hiddenInput.setAttribute('value', token.id);
      form.appendChild(hiddenInput);

      // Submit the form
      form.submit();
  } 
  
  $('.pay-via-stripe-btn').on('click', function () {
      var payButton   = $(this);
      var name        = $('#name').val();
      var email       = $('#email').val();

      if (name == '' || name == 'undefined') {
          $('.generic-errors').html('Name field required.');
          return false;
      }
      if (email == '' || email == 'undefined') {
          $('.generic-errors').html('Email field required.');
          return false;
      }

      if(!$('#terms_conditions').prop('checked')){
          $('.generic-errors').html('The terms conditions must be accepted.');
          return false;
      }
  });


    </script>
  </body>
</html>