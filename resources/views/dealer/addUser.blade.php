@extends('dealer.common.default')
@section('title', 'Dashboard')
@section('content')
  <!-- Main content -->
 
    <!-- Header -->
    <div class="header pb-6 d-flex align-items-center" style="min-height: 100px;">
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
    @if($errors->any()) @foreach($errors->all() as $error)
								<div class="alert alert-danger">{{$error}}</div>
								@endforeach @endif
	<form class="user" id="addAgent" method="POST" action="@if(isset($agent)) {{ url('/dealer/updateUserData') }} @else {{ url('/dealer/addUserData') }} @endif" enctype="multipart/form-data">
          @csrf
          
          @if(isset($editStatus))
                            @method('PUT')
                            @endif
      <div class="row">
        <div class="col-xl-4 order-xl-2">
          <div class="card card-profile bg-dark">
            <div class="image-area mt-4" style=" width: 90%; margin: auto; min-height: 200px; ">
            @if(isset($agentimages))
                  <img id="imageResult" src="{{'/uploads/'.$agentimages->filename}}" alt="" class="img-fluid rounded shadow-sm mx-auto d-block"></div>
            @else
            <img id="imageResult" src="" alt="" class="img-fluid rounded shadow-sm mx-auto d-block"></div>
             @endif  
            <img src="/assets/img/theme/img-1-1000x600.jpg" alt="Image placeholder" class="card-img-top" style=" width: 90%; margin: auto; min-height: 200px; display: none; ">
            <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4 bg-dark" style=" border-radius: 10px; ">
              <div class="input-group mb-3 px-2 py-2 rounded-pill bg-white shadow-sm overflow-hidden">
                <input id="upload" type="file" name="photos[]" onchange="readURL(this);" class="form-control border-0" >
                <label id="upload-label" for="upload" class="font-weight-light text-muted">Choose file...</label>
                <div class="input-group-append">
                    <label for="upload" class="btn btn-primary m-0 rounded-pill"><small class="text-uppercase font-weight-bold">Choose file</small></label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-8 order-xl-1">
            <div class="card">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col-8">
                    @if(isset($agent))
                    <h3 class="mb-0">Update Agent </h3>
                    @else
                    <h3 class="mb-0">Add Agent </h3>
                    @endif
                  </div>
                  @if(isset($agent))
                  <input type="hidden" name="id" value="{{$agent->id}}" />
                  <input type="hidden" name="userId" value="{{$agent->userId}}" />
                  @endif
                  @if(isset($agentimages))
                  <input type="hidden" name="imageUrl" id="imageUrl" value="{{url('uploads/'.$agentimages->filename)}}" />
                  @endif
                  <div class="col-4 text-right">
                    <i class="ni-"></i>
                    <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <form>
                  <h6 class="heading-small text-muted mb-4">Agent information</h6>
                  <div class="pl-lg-4">
                    <div class="row">
                    <div class="col-lg-2">
                        <div class="form-group">
                          <label class="form-control-label" for="title-prefix">Title</label>
                          <select id="title-prefix"  name="title" class="form-control" required >
                            <option value="" selected disabled>Choose one</option>
                            <option value="Mr"> MR. </option>
                            <option value="MRS"> MRS. </option>
                          </select>

                        </div>
                      </div>
                      <div class="col-lg-5">
                        <div class="form-group">
                          <label class="form-control-label" for="input-first-name" >First name</label>
                          <input type="text" id="first_name" class="form-control" name="first_name" placeholder="First name" value="{{old('first_name',  isset($agent->first_name) ? $agent->first_name : NULL)}}">
                        </div>
                      </div>
                      <div class="col-lg-5">
                        <div class="form-group">
                          <label class="form-control-label" for="input-last-name" >Last name</label>
                          <input type="text" id="last_name" class="form-control" name="last_name" placeholder="Last name" value="{{old('last_name',  isset($agent->last_name) ? $agent->last_name : NULL)}}">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label class="form-control-label" for="input-email" >Email address</label>
                          <input type="email" id="email" class="form-control" name="email" placeholder="jesse@example.com" value="{{old('email',  isset($agent->email) ? $agent->email : NULL)}}">
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label class="form-control-label" for="position">Position</label>
                          <select id="position" class="form-control" required name="position">
                            <option value="">Select Position</option>
                                @foreach ($jobposition_list as $position)
                                 <option value="{{$position->id}}">{{$position->position_name}}</option>
                               @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                          <label class="form-control-label" for="timezone">Timezone</label>
                          <select id="timezone" class="form-control" required name="timezone">
                            <option value="">Select Timezone</option>
                                @foreach ($timezone as $zone)
                                <option @if($agent->timezone_id ?? '') @if($agent->timezone_id==$zone->id) selected @endif @endif value="{{$zone->id}}">{{$zone->name .' '. $zone->offset}}</option>
                                @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr class="my-4" />
                  <!-- Address -->
                  <h6 class="heading-small text-muted mb-4">Contact information</h6>
                  <div class="pl-lg-4">
                    <div class="row">
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label" for="input-mobile" >Mobile</label>
                          <input type="text" id="mobile" class="form-control" name="mobile" placeholder="Mobile" value="{{old('last_name',  isset($agent->mobile) ? $agent->mobile : NULL)}}">
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label" for="input-phone" >Phone</label>
                          <input type="text" id="phone" class="form-control" name="phone" placeholder="Phone" value="{{old('phone',  isset($agent->office) ? $agent->office : NULL)}}">
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label" for="input-fax" >Fax</label>
                          <input type="text" id="fax" class="form-control" name="fax" placeholder="Fax" value="{{old('phone',  isset($agent->fax) ? $agent->fax : NULL)}}">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label" for="input-country">Country</label>
                          <select id="countryId" class="form-control required"  name="countryId">
                                <option value="">Select Country</option>
                                @foreach ($country_data as $country)
                                <option @if($agent->country_id ?? '') @if($agent->country_id==$country->id) selected @endif @endif value="{{$country->id}}">{{$country->country_name}}</option>
                               @endforeach
                                </select>
                        </div>
                      </div>
                          <div class="col-lg-4">
                                              <div class="form-group">
                                                <label class="form-control-label" for="input-last-name">Select State</label>
                                                <select class="form-control" data-trigger name="stateId" id="stateId">
                                                    <option  value="">Select State</option>
                                                  </select>
                                              </div>
                                            </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label" for="input-city">City</label>
                          <select id="cityId" name="cityId" class="form-control required">
                                <option value="">Select City</option>
                                
                               
                                </select>
                        </div>
                      </div>
                      
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label" for="input-street-1" >Street 1</label>
                          <input type="text" id="street_1" class="form-control" name="street_1" placeholder="Street 1" value="{{old('street_1',  isset($agent->street_1) ? $agent->street_1 : NULL)}}">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label" for="input-street-2" >Street 2</label>
                          <input type="text" id="street_2" class="form-control" name="street_2" placeholder="street 2" value="{{old('street_2',  isset($agent->street_2) ? $agent->street_2 : NULL)}}" >
                        </div>
                      </div>
                     <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label" for="input-country" >Postal code</label>
                          <input type="number" id="postal_code" class="form-control" name="postal_code" placeholder="Postal code" value="{{old('postal_code',  isset($agent->postal_code) ? $agent->postal_code : NULL)}}">
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr class="my-4" />
                  <!-- Description -->
                  <h6 class="heading-small text-muted mb-4">Permissions/ Security</h6>
                  <div class="pl-lg-4">
                    <div class="row">
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label" for="input-country">Can this user add or edit other users?</label>
                          <br>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="customRadioInline1" value="1" class="custom-control-input">
                            <label class="custom-control-label" for="customRadioInline1" >Yes</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="customRadioInline1" value="0" class="custom-control-input">
                            <label class="custom-control-label" for="customRadioInline2" >No</label>
                          </div>
                        </div>

                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label" for="position">Primary Market </label>
                          <select id="position" class="form-control" required name="primary_market">
                            <option value="" selected disabled>Select a vertical...</option>
                            <option value="3"> Primary Market </option>
                            <option value="3f"> Primary Market </option>
                            <option value="4"> Primary Market </option>
                            <option value="4f"> Primary Market </option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label class="form-control-label" for="position">Create password</label>
                              <div class="password-i">
                                <input type="password" name="adminPassword" id="adminPassword" class="form-control" onfocus="" placeholder="Enter the password">
                                <i class="far fa-eye" id="togglePasswordEye" style="display: none;"></i> 
                              </div>
                              <div class="custom-control mt-2 custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="askPassword" name="login" value="1">
                                <label class="custom-control-label" for="askPassword"  >Ask for login</label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12">
                        <div class="form-group">
                          <label class="form-control-label" for="input-country">User Permissions</label>
                          <div class="checkboxes d-flex flex-wrap">
                            
                            @foreach($rolePermissions as $permission)
                            <div class="custom-control m-3 custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="customCheck1-{{$permission->id}}" name= "permission[]" value="{{$permission->id}}">
                              <label class="custom-control-label" for="customCheck1-{{$permission->id}}" >{{$permission->name}}</label>
                            </div>
                            @endforeach
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
        </div>
      </div>
      </form>



  @stop
