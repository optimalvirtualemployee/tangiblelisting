@extends('dealer.common.default')
@section('title', 'Enquiry')
@section('content')
  <!-- Main content -->
  <div class="header pb-6 d-flex align-items-center" style="min-height: 100px;">
    </div>
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col-xl-12">
            <div class="card">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col-12">
                    <h3 class="mb-0">Dealership Management</h3>
                  </div>
                 
                </div>
              </div>
              <div class="card-body">
                  <div class="row">
                      <div class="col-lg-6">
                        <div class="infoView ">
                          <div class="d-flex justify-content-between align-items-center mb-4">
                            <h6 class="heading-small text-muted">Address information</h6>
                            <div class="editDetails">
                              <button class="btn btn-primary add-info-edit" id="edit_form_dm" type="button"><i class="fa fa-edit"> </i> Edit</button>                              
                            </div>
                          </div>
                            
                            <div class="d-flex justify-content-between">
                                <div>Name</div><div>{{$agency->company_name}}</div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>Address Line 1 </div><div>{{$agency->street_1}}</div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>Address Line 2</div>---<div>{{$agency->street_2}}</div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>State</div><div>{{$stateSelected->state_name}}</div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>Country</div><div>{{$countrySelected->country_name}}</div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>Post Code</div><div>{{$agency->postal_code}}</div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>Country</div><div>{{$countrySelected->country_name}}</div>
                            </div>
                            <h6 class="heading-small text-muted mb-2 mt-4">Contact information</h6>                           
                            <div class="d-flex justify-content-between">
                                <div>Primary Phone</div><div>{{$agency->phone_no}}</div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>Email</div><div>{{$agency->email}}</div>
                            </div>
                            <!-- <div class="d-flex justify-content-between">
                                <div>Secondary Phone</div><div>-</div>
                            </div> -->                   
                          </div>
                          <div class="inner edit-info-form d-none">
                            <form method="POST" action="{{ url('dealer/dealerManagement/update') }}">
                            @method('PATCH')
                            @csrf 
                            <input type="hidden" name="agencyId" value="{{$agency->id}}">
                                <h6 class="heading-small text-muted mb-2">Agent information</h6>
                                <div class="pl-lg-4">
                                  <div class="row">                      
                                    <div class="col-lg-12">
                                      <div class="form-group">
                                        <label class="form-control-label" for="input-name">Name</label>
                                        <input type="text" id="input-name" class="form-control" name="company_name" placeholder="Name" value="{{$agency->company_name}}">
                                      </div>
                                    </div>                     
                                  </div>
              
                                  <div class="row">
                                    <div class="col-lg-12">
                                      <div class="form-group">
                                        <label class="form-control-label" for="input-email">Address Line 1</label>
                                        <input type="text" id="input-address" class="form-control" name="street_1" placeholder="Flat / Hno./ Aprtments" value="{{$agency->street_1}}">
                                      </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                          <label class="form-control-label" for="input-email">Address Line 2</label>
                                          <input type="text" id="input-address" class="form-control" name="street_2" placeholder="Landmark" value="{{$agency->street_2}}">
                                        </div>
                                      </div>                                   
                                  </div>
                                </div>                             
                                <div class="pl-lg-4">
                                  <div class="row"> 
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                          <label class="form-control-label">Country*</label>
                                <select id="countryId" class="form-control required"  name="countryId">
                                @foreach ($countryData as $country)
                                       <option @if($countrySelected->id ?? '') @if($countrySelected->id==$country->id) selected @endif @endif value="{{$country->id}}">{{$country->country_name}}</option>
                                @endforeach
                                </select>
                                        </div>
                                      </div>
                                      <div class="col-lg-12">
                                          <div class="form-group">
                                            <label class="form-control-label" for="input-country">Postal code</label>
                                            <input type="number" id="input-postal-code" class="form-control" name="postal_code" placeholder="Postal code" value="{{$agency->postal_code}}">
                                          </div>
                                        </div>
                                      <div class="col-lg-12">
                                        <div class="form-group">
                                          <label class="form-control-label">State:</label><select id="stateId"  name="stateId" class="form-control">
                                @foreach ($stateData as $state)
                                  <option @if($stateSelected->id ?? '') @if($stateSelected->id==$state->id) selected @endif @endif value="{{$state->id}}">{{$state->state_name != '' ? $state->state_name : 'Please Select State'}}</option>
                                @endforeach
                                </select>
                                        </div>
                                      </div>                                
                                  </div>
                                </div>
                                <hr class="my-4" />
                                <!-- Description -->
                                <h6 class="heading-small text-muted mb-4">Permissions/ Security</h6>
                                <div class="pl-lg-4">
                                    <div class="row">                                  
                                        <div class="col-lg-12">
                                          <div class="form-group">
                                            <label class="form-control-label" for="input-phone">Primary Phone</label>
                                            <input type="text" id="input-phone" class="form-control" name="mobile" placeholder="Phone" value="{{$agency->phone_no}}">
                                          </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                              <label class="form-control-label" for="input-email">Email</label>
                                              <input type="email" id="input-email" class="form-control" name="email" value="{{$agency->email}}">
                                            </div>
                                          </div>   
                                          <!-- <div class="col-lg-12">
                                              <div class="form-group">
                                                <label class="form-control-label" for="input-country">Secondary Phone code</label>
                                                <input type="number" id="input-postal-code" class="form-control" placeholder="Postal code">
                                              </div>
                                            </div> -->   
                                            <div class="col-lg-12">
                                              <div class="form-group">
                                                <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                    	                          </div>
                                            </div>                                                                      
                                      </div>
                                </div>
                              </form>
                        </div>
                      </div>                     
                      <div class="col-lg-4 offset-1 ">
                          <div class="inner business-hours-view">
                            <div class="d-flex justify-content-between align-items-center mb-4">
                              <h6 class="heading-small text-muted">Business Hours</h6>
                              <div class="editDetails">
                                <button class="btn btn-primary add-business-hours"><i class="fa fa-edit"> </i> Edit</button>                              
                              </div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>Monday</div><div>09:21</div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>Tuesday</div><div>09:21</div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>Wednesday</div><div>09:21</div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>Thursday</div><div>09:21</div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>Friday</div><div>09:21</div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>Saturday</div><div>09:21</div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div>Sunday</div><div>09:17</div>
                            </div>
                          </div>
                          <div class="inner business-hours-form d-none">
                            <form class=""> 
                              <h6 class="heading-small text-muted mb-2">Business Hours</h6>
                              <div class="">
                                <div class="row">                      
                                  <div class="col-lg-12">
                                    <div class="form-group">                                      
                                      <input type="text" id="input-monday" class="form-control" placeholder="Monday" value="">
                                    </div>
                                  </div> 
                                  <div class="col-lg-12">
                                    <div class="form-group">                                      
                                      <input type="text" id="input-tuesday" class="form-control" placeholder="Tuesday">
                                    </div>
                                  </div>
                                  <div class="col-lg-12">
                                      <div class="form-group">                                       
                                        <input type="text" id="input-wednesday" class="form-control" placeholder="Wednesday">
                                      </div>
                                  </div> 
                                  <div class="col-lg-12">
                                    <div class="form-group">                                       
                                      <input type="text" id="input-thursday" class="form-control" placeholder="Thursday">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                  <div class="form-group">                                       
                                    <input type="text" id="input-friday" class="form-control" placeholder="Friday">
                                  </div>
                              </div>
                              <div class="col-lg-12">
                                <div class="form-group">                                       
                                  <input type="text" id="input-saturday" class="form-control" placeholder="Saturday">
                                </div>
                              </div>     
                              <div class="col-lg-12">
                                <div class="form-group">                                       
                                  <input type="text" id="input-sunday" class="form-control" placeholder="Sunday">
                                </div>
                              </div>  
                              <div class="col-lg-12">
                                <div class="form-group">
                                  <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                </div>
                              </div>                
                                </div> 
                              </div>
                            </form>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
        </div>
      </div>
      <!-- Footer -->
    </div>
@stop
  
  
