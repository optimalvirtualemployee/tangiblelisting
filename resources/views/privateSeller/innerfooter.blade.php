
    <section>               
        <!-- Modal HTML -->
        <div id="myModal" class="modal fade">
            <div class="modal-dialog modal-login">
                <div class="modal-content">
                @if($errors->any())
                    @foreach($errors->all() as $error)
                      <div class="alert alert-danger">
                    {{$error}}
                      </div>
                    @endforeach
                  @endif
                    <form action="/user/logincheck" method="post" id="loginForm">
                    @csrf
                    <div class="modal-header">                       			
                        <h4 class="modal-title">Member Login</h4>	
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">                        
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" placeholder="Username or email" required="required" id="usernameEmail">		
                            </div>
                            <div class="login">
                                <div class="form-group">
                                    <input type="password" class="form-control d-none" name="password" placeholder="Password" required="required" id="rPassword"> 	
                                </div>      
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-btn">Login</button>
                            </div>
                            <div class="modal-footer">
                                <a href="#">Forgot Password?</a>
                            </div>         
                    </div>
                    </form>
                    
                    @if($errors->any())
                    @foreach($errors->all() as $error)
                      <div class="alert alert-danger">
                    {{$error}}
                      </div>
                    @endforeach
                  @endif
                    <form action="user/newUserRegister" method="post" id="registerForm" class="d-none" >
                    @csrf
                        <div class="modal-header">                       			
                            <h4 class="modal-title">Register</h4>	
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body"> 
                        <div class="register">
                            <div class="form-group">
                                <input type="text" class="form-control" name="first_name" placeholder="First Name" required="required">	
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="last_name" placeholder="Last Name" required="required">	
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-control" name="phone" placeholder="Phone No" required="required">	
                            </div> 
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="email" required="required">	
                            </div> 
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Password" required="required">	
                            </div>   
                            <div class="form-group">
                                <input type="password" class="form-control" name="confirmpassword" placeholder="Confirm Password" required="required">	
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-btn">Register</button>
                            </div>
                        </div>
                    </div>                        
                    </form>
                </div>
            </div>
        </div>     
    </section>
    <!-- Modal -->  
  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="sellNowLabel">Sell Now</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="inner sell-cat">
                        <div class="uppersection mb-3 text-center">
                            <i class=" fa-5x fa fa-lock"></i>
                            <p>Over 2,500 successful private sellers each month.</p>
                        </div>
                        <div class="linkto">
                            <a class="sell-btn" href="/privatesellercategoryselect">Create Private Listing</a>
                        </div>
                    </div>
                </div>           
                <div class="col-lg-6">
                    <div class="inner sell-cat">
                        <div class="uppersection mb-3 text-center">
                            <i class="fa fa-5x fa-user"></i>
                            <p>Open your door to 500,000 watch enthusiasts every day</p>
                        </div>
                        <div class="linkto">
                            <a class="sell-btn" href="create-private-listing.html">Become Dealer</a>
                        </div>
                    </div>
                </div>
            </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>             
            </div>
          </div>
    </div>
  </div>
    
<section>               
        <!-- Modal HTML -->
        <div id="currency" class="modal fade">
            <div class="modal-dialog modal-login">
                <div class="modal-content">                
                    <form action="#" method="post" id="currencyForm" >
                        <div class="modal-header">                       			
                            <h4 class="modal-title">Settings</h4>	
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body"> 
                        <div class="register">
                            <div class="form-group">
                                <select name="je_currency" id="" class="form-control">
                                    <option value="AUD">Australian dollar - AUD $</option>
                                    <option value="BRL">Brazilian real - BRL R$</option>
                                    <option value="CAD">Canadian dollar - CAD $</option>
                                    <option value="CZK">Czech korun - CZK Kč</option>
                                    <option value="DKK">Danish krone - DKK kr.</option>
                                    <option value="AED">Emirati dirham - AED AED</option>
                                    <option value="EUR">Euro - EUR €</option>
                                    <option value="HKD">Hong Kong dollar - HKD HK$</option>
                                    <option value="HUF">Hungarian forint - HUF Ft</option>
                                    <option selected="selected" value="INR">Indian rupee - INR ₹</option>
                                    <option value="JPY">Japanese yen - JPY ¥</option>
                                    <option value="MYR">Malaysian ringgit - MYR RM</option>
                                    <option value="MXN">Mexican peso - MXN $</option>
                                    <option value="NOK">Norwegian krone - NOK kr</option>
                                    <option value="PLN">Polish zloty - PLN zł</option>
                                    <option value="GBP">Pound sterling - GBP £</option>
                                    <option value="RUB">Russian ruble - RUB ₽</option>
                                    <option value="SAR">Saudi Arabian riyal - SAR ر.س</option>
                                    <option value="SGD">Singapore dollar - SGD $</option>
                                    <option value="ZAR">South African rand - ZAR R</option>
                                    <option value="KRW">South Korean won - KRW ₩</option>
                                    <option value="SEK">Swedish krona - SEK kr</option>
                                    <option value="CHF">Swiss franc - CHF CHF</option>
                                    <option value="TRY">Turkish lira - TRY ₺</option>
                                    <option value="USD">United States dollar - USD $</option></select>	
                            </div> 
                            <div class="form-group">
                                <select name="je_measurement_units" id="" class="form-control">
                                    <option selected="selected" value="sqft">Square Feet — ft²</option>
                                    <option value="sqm">Square Meter — m²</option></select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-btn">Save Choice</button>
                            </div>
                    </div>                        
                    </form>
                </div>
            </div>
        </div>     
    </section>
<footer>
        <div class="custom-top-footer pt-5 pb-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 mr-auto col-md-4 mb-4">
                        <div class="custom-footer-logo">
                            <a href="#"><img src="/assets/img/logo-header.png" class="img-fluid"></a>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                                a type specimen book.</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 mb-4">
                        <div class="custom-footer-links footer-social">
                            <h3>Contact Us</h3>
                            <ul>
                                <li>
                                    <i class="fas fa-map-marker-alt"></i>{{$contactUs->address}}
                                </li>
                                <li>
                                    Call us FREE<a href="#"><i class="fas fa-mobile-alt"></i> {{$contactUs->mobile}}</a>
                                </li>
                                <li>
                                    <a href="mailto:email@email.com"><i class="far fa-envelope"></i> {{$contactUs->email}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="custom-footer-links footer-nav">
                            <h3>Important Links</h3>
                            <ul>
                                @if($propertyType->firstWhere('property_type', 'Residential'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Residential='. $propertyType->firstWhere('property_type', 'Residential')->id)}}">Residential</a>
                                </li>
                                @endif
                                @if($propertyType->firstWhere('property_type', 'Apartment'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Apartment='. $propertyType->firstWhere('property_type', 'Apartment')->id)}}">Apartment</a>
                                </li>
                                @endif
                                @if($propertyType->firstWhere('property_type', 'Single Family Home'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Single Family Home='. $propertyType->firstWhere('property_type', 'Single Family Home')->id)}}">Single Family Home</a>
                                </li>
                                @endif
                                @if($propertyType->firstWhere('property_type', 'Villa'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Villa='. $propertyType->firstWhere('property_type', 'Villa')->id)}}">Villa</a>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="custom-bottom-footer pt-3 pb-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-12">
                        <div class="copyright-wrapper">
                            <p>© Tangible - All rights reserved</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-12">
                        <div class="footer-social-links">
                            <ul>
                                <li><a class="hvr-buzz-out" href="https://www.facebook.com/tangiblelistings"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a class="hvr-buzz-out" href="https://www.instagram.com/tangiblelistings/"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>