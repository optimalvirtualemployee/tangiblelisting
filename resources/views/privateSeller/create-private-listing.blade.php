<!DOCTYPE html>
<html lang="en">

@include('tangiblehtml.headheader')

<body>
    @include('tangiblehtml.innerheader')

    <section class="become-seller-from overlay-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 d-flex align-items-center">
                    <div class="seller__content">
                        <h3 class="text-center mb-3 pb-2">Create a private listing<span> by selecting your choice</span></h3>                     
                    </div>

                </div>
                <div class="col-lg-4 offset-lg-2">
                    <div class="card">
                        <img src="https://dummyimage.com/300x200/000000/ffffff.png" class="card-img-top" alt="...">
                        <div class="card-body text-center">
                          <h5 class="card-title">Automobiles</h5>
                          <p class="card-text">Create an automobile listing.</p>
                          <a href="/privatesellerselectpackage" class="btn btn-success">Sell Now</a>
                        </div>
                      </div>                     
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <img src="https://dummyimage.com/300x200/000000/ffffff.png" class="card-img-top" alt="...">
                        <div class="card-body text-center">
                          <h5 class="card-title">Watches</h5>
                          <p class="card-text">Create a watch listing.</p>
                          <a href="/privatesellerselectwatchpackage" class="btn btn-info">Sell Now</a>
                        </div>
                      </div>                     
                </div>
                <!-- <div class="col-lg-6">

                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="professional-tab" data-toggle="tab" href="#professional"
                                role="tab" aria-controls="professional-tab" aria-selected="true"> I'm a professional
                                dealer</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="private-tab" data-toggle="tab" href="#private" role="tab"
                                aria-controls="private-tab" aria-selected="false"> I'm a private seller</a>
                        </li>
                    </ul>

                    <div class="seller__from tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="professional" role="tabpanel"
                            aria-labelledby="professional-tab">
                            <form action="#">
                                <div class="form-row">
                                    <div class="form-group col-12">
                                        <input type="text" placeholder="Name" class="from-control">
                                    </div>
                                    <div class="form-group col-12">
                                        <input type="tel" id="telephone" name="telephone" placeholder="Phone number"
                                            class="from-control">
                                    </div>
                                    <div class="form-group col-12">
                                        <input type="email" placeholder="Email" class="from-control">
                                    </div>
                                    <div class="form-group col-12">
                                        <input type="text" placeholder="Company name" class="from-control">
                                    </div>
                                    <div class="form-group what-you-sell col-12">
                                        <label>
                                            What do you sell?
                                        </label>

                                        <div class="sell-item-row mt-2">
                                            <div class="sell-item">
                                                <input type="radio" name="what-we-sell" value="car" id="car">
                                                <label for="car">
                                                    <i class="fa fa-car" aria-hidden="true"></i>
                                                    <span>car</span>
                                                </label>
                                            </div>
                                            <div class="sell-item">
                                                <input type="radio" name="what-we-sell" value="car" id="watch">
                                                <label for="watch">
                                                    <i class="fa fa-clock" aria-hidden="true"></i>
                                                    <span>watch</span>
                                                </label>
                                            </div>
                                            <div class="sell-item">
                                                <input type="radio" name="what-we-sell" value="car" id="realstate">
                                                <label for="realstate">
                                                    <i class="fa fa-home" aria-hidden="true"></i>
                                                    <span>Realstate</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="submit" class="read-more submit-btn" value="Send Message">
                            </form>
                        </div>

                        <div class="tab-pane fade" id="private" role="tabpanel" aria-labelledby="private-tab">
                            <div class="form-group what-you-sell text text-center">
                                <label>
                                    To register as a private seller please use this link:

                                </label>
                                <a href="#" class="read-more fill-read-more">Proceed </a>
                            </div>

                        </div>

                    </div>



                </div> -->
            </div>
        </div>
    </section>
    @include('tangiblehtml.innerfooter')
    
     <script src="/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/intlTelInput.min.js"></script>
    <script src="/assets/js/local.js"></script>
    <script>
        // Show the first tab and hide the rest
        // jQuery('.row_choser .tb_link').addClass('active');
        jQuery('.tab-content').hide();
        jQuery('.tab-content:first').show();

        // Click function
        jQuery('.tb_link').click(function () {
            jQuery('.tb_link').removeClass('active');
            jQuery(this).addClass('active');
            jQuery('.tab-content').hide();

            var activeTab = jQuery(this).attr('href');
            jQuery(activeTab).fadeIn();
            return false;
        });
        var input = document.querySelector("#telephone");
        window.intlTelInput(input, ({
            separateDialCode:true
        }));
    </script>
</body>

</html>