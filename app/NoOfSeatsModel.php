<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoOfSeatsModel extends Model
{
    protected $table = 'au_no_of_seats';
    protected $fillable = ['no_of_seats', 'status'];
}
