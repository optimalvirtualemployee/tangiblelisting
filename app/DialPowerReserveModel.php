<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DialPowerReserveModel extends Model
{
    protected $table = 'wa_factory_features_dial_powerreserve';
    protected $fillable = ['power_reserve', 'status'];
}
