<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyMessageModel extends Model
{
    protected $table = 'tbl_message_details';
}
