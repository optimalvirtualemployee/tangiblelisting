<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobilePayment extends Model
{
    protected $table = 'tbl_paymentRealEstate';


    protected $fillable = [
        'name','email','amount','currency','transaction_id','payment_status','receipt_url','transaction_complete_details'
    ];
}
