<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgenciesModel extends Model
{
    protected $table = 'tbl_agencies';

    public function category()
    {
        return $this->belongsTo('App\CategoriesModel', 'category_id', 'id');
    }
}
