<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchFeaturesListingModel extends Model
{
    protected $table = 'wa_features_listing';
}
