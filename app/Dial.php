<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dial extends Model
{
    protected $table = 'wa_factory_features_dial';
    protected $fillable = ['power_reserve','dial_color'];
}
