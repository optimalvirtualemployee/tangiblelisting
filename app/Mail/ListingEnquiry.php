<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ListingEnquiry extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    
    protected $user;
    protected $agent;
    protected $enqury;
    
    public function __construct($user ,$agent, $enqury)
    {
        $this->user = $user;
        $this->agent = $agent;
        $this->enqury = $enqury;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.contact-seller')
        ->with([
            'user' => $this->user,
            'agent' => $this->agent,
            'enqury' => $this->enqury,
        ]);
    }
}
