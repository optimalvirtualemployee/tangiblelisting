<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SuggestPriceRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    
    protected $suggestPrice;
    protected $user;
    protected $agent;
    protected $product;
    protected $productImage;
    
    public function __construct($suggestPrice, $user ,$agent, $product,$productImage)
    {
        $this->suggestPrice = $suggestPrice;
        $this->user = $user;
        $this->agent = $agent;
        $this->product = $product;
        $this->productImage = $productImage;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.sending-suggesting-price')
        ->with([
            'suggestPrice' => $this->suggestPrice,
            'user' => $this->user,
            'agent' => $this->agent,
            'product' => $this->product,
            'productImage' => $this->productImage,
        ]);
    }
}
