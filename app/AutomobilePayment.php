<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobilePayment extends Model
{
    protected $table = 'tbl_paymentAutomobile';


    protected $fillable = [
        'name','email','amount','currency','transaction_id','payment_status','receipt_url','transaction_complete_details'
    ];
}
