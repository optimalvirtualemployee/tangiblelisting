<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobileContractedDetailsModel extends Model
{
    protected $table = 'au_customer_product-details';
}
