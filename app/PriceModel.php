<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceModel extends Model
{
    protected $table = 'wa_price';
    protected $fillable = ['price', 'status'];
}
