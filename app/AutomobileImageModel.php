<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobileImageModel extends Model
{
    protected $table = 'au_automobile_images';
}
