<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobileContactusModel extends Model
{
    protected $table = 'au_contactus';
}
