<?php

namespace App\Http\Controllers;

use App\ContactusModel;
use App\PropertyDataModel;
use App\PropertyImageModel;
use Illuminate\Http\Request;
use App\CurrencyModel;
use App\WatchBlogModel;
use App\WatchDataListingModel;
use App\AutomobileDataListingModel;
use App\AgenciesModel;
use App\PropertyBlogModel;
use App\AutomobileBlogModel;
use App\TypeModel;
use App\PropertyTypeModel;

class LandingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $contactUs = ContactusModel::get()->first();
        $images = PropertyImageModel::get();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        
        $propertyType = PropertyTypeModel::get();
        
        $propertyListing = PropertyDataModel::select('re_property_type.id as propertyId','currency_code','metric','re_property_details.id as id', 'ad_title', 'property_price', 'land_size', 'number_of_bedrooms', 'number_of_bathrooms', 'property_type')->join('re_beds', 're_beds.id', '=', 're_property_details.bed_id')
        ->join('re_bathrooms', 're_bathrooms.id', '=', 're_property_details.bathroom_id')
        ->join('tbl_currency', 'tbl_currency.id', '=', 're_property_details.currency_id')
        ->join('re_property_type', 're_property_type.id', '=', 're_property_details.property_type_id')
        ->orderBy('re_property_details.created_at', 'desc')
        ->whereIN('re_property_details.id', function($query){
            $query->select('listing_id')
            ->from('re_property_images');
        })
        ->whereIN('re_property_details.id', function($query){
            $query->select('listing_id')
            ->from('re_property_features');
        })
        ->whereIN('re_property_details.id', function($query){
            $query->select('listing_id')
            ->from('re_property_comment');
        })->where('re_property_details.status','=', '1')
        ->get();
        
        $latest_property = PropertyDataModel::select('currency_code','re_property_details.id as id',  'property_price', 'property_type')
        ->join('tbl_currency', 'tbl_currency.id', '=', 're_property_details.currency_id')
        ->join('re_property_type', 're_property_type.id', '=', 're_property_details.property_type_id')
        ->orderBy('re_property_details.created_at', 'desc')
        ->whereIN('re_property_details.id', function($query){
            $query->select('listing_id')
            ->from('re_property_images');
        })
        ->whereIN('re_property_details.id', function($query){
            $query->select('listing_id')
            ->from('re_property_features');
        })
        ->whereIN('re_property_details.id', function($query){
            $query->select('listing_id')
            ->from('re_property_comment');
        })->where('re_property_details.status','=', '1')
        ->limit(1)
        ->first();
        
        //dd($latest_property->currency_code);
        //dd(currency()->convert($latest_property->property_price, $latest_property->currency_code, 'USD'));
        
        $latest_automobile = AutomobileDataListingModel::select('currency_code','au_automobile_detail.id as id', 'value' ,'automobile_brand_name', 'automobile_model_name', 'filename', 'make_id')
        ->join('tbl_currency', 'tbl_currency.id', '=', 'au_automobile_detail.currency_id')
        ->join('au_automobile_images', 'au_automobile_images.listing_id', '=', 'au_automobile_detail.id')
        ->join('au_model', 'au_model.automobile_brand_id', '=', 'au_automobile_detail.make_id')
        ->join('au_brands', 'au_brands.id', '=', 'au_automobile_detail.make_id')
        ->orderBy('au_automobile_detail.created_at', 'desc')
        ->whereIN('au_automobile_detail.id', function($query){
            $query->select('listing_id')
            ->from('au_automobile_images');
        })
        /* ->whereIN('au_automobile_detail.id', function($query){
            $query->select('listing_id')
            ->from('au_automobile_features');
        }) */
        ->whereIN('au_automobile_detail.id', function($query){
            $query->select('listing_id')
            ->from('au_automobile_comments');
        })->where('au_automobile_detail.status','=', '1')
        ->limit(1)
        ->first();
        
        $latest_watches = WatchDataListingModel::select('currency_code','wa_watch_detail.id as id', 'brand_name', 'watch_price', 'filename')
        ->join('tbl_currency', 'tbl_currency.id', '=', 'wa_watch_detail.currency_id')
        ->join('wa_watch_images', 'wa_watch_images.listing_id', '=', 'wa_watch_detail.id')
        ->orderBy('wa_watch_detail.created_at', 'desc')
        ->whereIN('wa_watch_detail.id', function($query){
            $query->select('listing_id')
            ->from('wa_watch_images');
        })
        ->whereIN('wa_watch_detail.id', function($query){
            $query->select('listing_id')
            ->from('wa_watch_features');
        })
        ->whereIN('wa_watch_detail.id', function($query){
            $query->select('listing_id')
            ->from('wa_watch_comment');
        })->where('wa_watch_detail.status','=', '1')
        ->limit(2)
        ->get();
        
        //print_r($latest_watches);
        //dd($latest_watches);
        $dealer_listing = AgenciesModel::select('tbl_agencies.id as id', 'first_name', 'filename')
        ->join('tbl_agencies_images', 'tbl_agencies_images.listing_id', '=', 'tbl_agencies.id')
        ->orderBy('tbl_agencies.created_at', 'desc')
        ->limit('4')
        ->get();
        
        $property_blog = PropertyBlogModel::select('re_blog.id as id','filename','title', 'blogPost', 're_blog.created_at as created_at', 'blogType','url')
        ->join('re_property_blog_images', 're_property_blog_images.listing_id', '=', 're_blog.id')
        ->where('status', '=', '1')->orderBy('re_blog.created_at','desc')->first();
        
        $automobile_blog = AutomobileBlogModel::select('au_blog.id as id','filename','title', 'blogPost', 'au_blog.created_at as created_at', 'blogType','url')
        ->join('au_blog_images', 'au_blog_images.listing_id', '=', 'au_blog.id')
        ->where('status', '=', '1')->orderBy('au_blog.created_at','desc')->first();
        
        $watch_blog = WatchBlogModel::select('wa_blog.id as id','filename','title', 'blogPost', 'wa_blog.created_at as created_at', 'blogType','url')
        ->join('wa_blog_images', 'wa_blog_images.listing_id', '=', 'wa_blog.id')
        ->where('status', '=', '1')->orderBy('wa_blog.created_at','desc')->limit('2')->get();
        
        return view('tangiblehtml.landing', compact('contactUs', 'propertyListing', 'images', 'latest_property', 'latest_watches', 'latest_automobile', 'dealer_listing'
                    ,'property_blog', 'automobile_blog', 'watch_blog', 'propertyType'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
