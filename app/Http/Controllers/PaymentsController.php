<?php
namespace App\Http\Controllers;

use Exception;
use App\Payment;
use App\AutomobilePayment;
use App\WatchPayment;
use App\RealEstatePayment;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Stripe\Charge;
use Stripe\Stripe;
use Stripe\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use App\AgentModel;
use App\AutomobileDataListingModel;
use App\AutomobileFeatureListingModel;
use App\AutomobileFeaturesModel;
use App\AutomobileAdditionalFeaturesModel;
use App\AutomobileImageModel;
use App\AutomobileCommentModel;
use App\CurrencyModel;
use App\UserDetailsModel;
use App\AutomobileDamageImageModel;
use App\AutomobileSecurityImageModel;
use App\WatchDataListingModel;
use App\WatchFeaturesModel;
use App\DialColorModel;
use App\BraceletMaterialModel;
use App\WatchAdditionalFeaturesModel;
use App\WatchDetailsFeaturesModel;
use App\WatchCommentModel;
use App\WatchImageModel;
use App\WatchModel;
use App\Watchbrandmodel;
use App\WatchDamageImageModel;
use App\WatchSecurityImageModel;
use App\WatchDocumentImageModel;
use App\UserPermissionModel;
use App\User;
use Auth;

class PaymentsController extends Controller
{

    public function create()
    {
        $userDetails = UserDetailsModel::get();
        return view('payments.create');
    }

    public function store()
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'terms_conditions' => 'accepted'
        ]);

        /**
         * I have hard coded amount.
         * You may fetch the amount based on customers order or anything
         */
        $amount = 1 * 100;
        $currency = 'usd';

        if (empty(request()->get('stripeToken'))) {
            session()->flash('error', 'Some error while making the payment. Please try again');
            return back()->withInput();
        }
        Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
        try {
            /**
             * Add customer to stripe, Stripe customer
             */
            $customer = Customer::create([
                'name' => request('name'),
                'email' => request('email'),
                'source' => request('stripeToken')
            ]);
        } catch (Exception $e) {
            $apiError = $e->getMessage();
        }

        if (empty($apiError) && $customer) {
            /**
             * Charge a credit or a debit card
             */
            try {
                /**
                 * Stripe charge class
                 */
                $charge = Charge::create(array(
                    'customer' => $customer->id,
                    'amount' => $amount,
                    'currency' => $currency,
                    'description' => 'Some testing description'
                ));
            } catch (Exception $e) {
                $apiError = $e->getMessage();
            }

            if (empty($apiError) && $charge) {
                // Retrieve charge details
                $paymentDetails = $charge->jsonSerialize();
                if ($paymentDetails['amount_refunded'] == 0 && empty($paymentDetails['failure_code']) && $paymentDetails['paid'] == 1 && $paymentDetails['captured'] == 1) {
                    /**
                     * You need to create model and other implementations
                     */

                    Payment::create([
                        'name' => request('name'),
                        'email' => request('email'),
                        'amount' => $paymentDetails['amount'] / 100,
                        'currency' => $paymentDetails['currency'],
                        'transaction_id' => $paymentDetails['balance_transaction'],
                        'payment_status' => $paymentDetails['status'],
                        'receipt_url' => $paymentDetails['receipt_url']
                        // 'transaction_complete_details' => json_encode($paymentDetails)
                    ]);

                    return redirect('/thankyou/?receipt_url=' . $paymentDetails['receipt_url']);
                } else {
                    session()->flash('error', 'Transaction failed');
                    return back()->withInput();
                }
            } else {
                session()->flash('error', 'Error in capturing amount: ' . $apiError);
                return back()->withInput();
            }
        } else {
            session()->flash('error', 'Invalid card details: ' . $apiError);
            return back()->withInput();
        }
    }

    public function watchStore()
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'terms_conditions' => 'accepted'
        ]);

        /**
         * I have hard coded amount.
         * You may fetch the amount based on customers order or anything
         */
        $amount = 1 * 100;
        $currency = 'usd';

        if (empty(request()->get('stripeToken'))) {
            session()->flash('error', 'Some error while making the payment. Please try again');
            return back()->withInput();
        }
        Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
        try {
            /**
             * Add customer to stripe, Stripe customer
             */
            $customer = Customer::create([
                'name' => request('name'),
                'email' => request('email'),
                'source' => request('stripeToken')
            ]);
        } catch (Exception $e) {
            $apiError = $e->getMessage();
        }

        if (empty($apiError) && $customer) {
            /**
             * Charge a credit or a debit card
             */
            try {
                /**
                 * Stripe charge class
                 */
                $charge = Charge::create(array(
                    'customer' => $customer->id,
                    'amount' => $amount,
                    'currency' => $currency,
                    'description' => 'Some testing description'
                ));
            } catch (Exception $e) {
                $apiError = $e->getMessage();
            }

            if (empty($apiError) && $charge) {
                // Retrieve charge details
                // dd($charge);
                $paymentDetails = $charge->jsonSerialize();
                if ($paymentDetails['amount_refunded'] == 0 && empty($paymentDetails['failure_code']) && $paymentDetails['paid'] == 1 && $paymentDetails['captured'] == 1) {
                    /**
                     * You need to create model and other implementations
                     */

                    WatchPayment::create([
                        'name' => request('name'),
                        'email' => request('email'),
                        'amount' => $paymentDetails['amount'] / 100,
                        'currency' => $paymentDetails['currency'],
                        'transaction_id' => $paymentDetails['balance_transaction'],
                        'payment_status' => $paymentDetails['status'],
                        'receipt_url' => $paymentDetails['receipt_url']
                        // 'transaction_complete_details' => json_encode($paymentDetails)
                    ]);

                    return redirect('/thankyou/?receipt_url=' . $paymentDetails['receipt_url']);
                } else {
                    session()->flash('error', 'Transaction failed');
                    return back()->withInput();
                }
            } else {
                session()->flash('error', 'Error in capturing amount: ' . $apiError);
                return back()->withInput();
            }
        } else {
            session()->flash('error', 'Invalid card details: ' . $apiError);
            return back()->withInput();
        }
    }

    public function automobileStore()
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'terms_conditions' => 'accepted'
        ]);

        /**
         * I have hard coded amount.
         * You may fetch the amount based on customers order or anything
         */
        $amount = 1 * 100;
        $currency = 'usd';

        if (empty(request()->get('stripeToken'))) {
            session()->flash('error', 'Some error while making the payment. Please try again');
            return back()->withInput();
        }
        Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
        try {
            /**
             * Add customer to stripe, Stripe customer
             */
            $customer = Customer::create([
                'name' => request('name'),
                'email' => request('email'),
                'source' => request('stripeToken')
            ]);
        } catch (Exception $e) {
            $apiError = $e->getMessage();
        }

        if (empty($apiError) && $customer) {
            /**
             * Charge a credit or a debit card
             */
            try {
                /**
                 * Stripe charge class
                 */
                $charge = Charge::create(array(
                    'customer' => $customer->id,
                    'amount' => $amount,
                    'currency' => $currency,
                    'description' => 'Some testing description'
                ));
            } catch (Exception $e) {
                $apiError = $e->getMessage();
            }

            if (empty($apiError) && $charge) {
                // Retrieve charge details
                $paymentDetails = $charge->jsonSerialize();
                if ($paymentDetails['amount_refunded'] == 0 && empty($paymentDetails['failure_code']) && $paymentDetails['paid'] == 1 && $paymentDetails['captured'] == 1) {
                    /**
                     * You need to create model and other implementations
                     */

                    AutomobilePayment::create([
                        'name' => request('name'),
                        'email' => request('email'),
                        'amount' => $paymentDetails['amount'] / 100,
                        'currency' => $paymentDetails['currency'],
                        'transaction_id' => $paymentDetails['balance_transaction'],
                        'payment_status' => $paymentDetails['status'],
                        'receipt_url' => $paymentDetails['receipt_url']
                        // 'transaction_complete_details' => json_encode($paymentDetails)
                    ]);

                    return redirect('/thankyou/?receipt_url=' . $paymentDetails['receipt_url']);
                } else {
                    session()->flash('error', 'Transaction failed');
                    return back()->withInput();
                }
            } else {
                session()->flash('error', 'Error in capturing amount: ' . $apiError);
                return back()->withInput();
            }
        } else {
            session()->flash('error', 'Invalid card details: ' . $apiError);
            return back()->withInput();
        }
    }

    public function realEstateStore()
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'terms_conditions' => 'accepted'
        ]);

        /**
         * I have hard coded amount.
         * You may fetch the amount based on customers order or anything
         */
        $amount = 1 * 100;
        $currency = 'usd';

        if (empty(request()->get('stripeToken'))) {
            session()->flash('error', 'Some error while making the payment. Please try again');
            return back()->withInput();
        }
        Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
        try {
            /**
             * Add customer to stripe, Stripe customer
             */
            $customer = Customer::create([
                'name' => request('name'),
                'email' => request('email'),
                'source' => request('stripeToken')
            ]);
        } catch (Exception $e) {
            $apiError = $e->getMessage();
        }

        if (empty($apiError) && $customer) {
            /**
             * Charge a credit or a debit card
             */
            try {
                /**
                 * Stripe charge class
                 */
                $charge = Charge::create(array(
                    'customer' => $customer->id,
                    'amount' => $amount,
                    'currency' => $currency,
                    'description' => 'Some testing description'
                ));
            } catch (Exception $e) {
                $apiError = $e->getMessage();
            }

            if (empty($apiError) && $charge) {
                // Retrieve charge details
                $paymentDetails = $charge->jsonSerialize();
                if ($paymentDetails['amount_refunded'] == 0 && empty($paymentDetails['failure_code']) && $paymentDetails['paid'] == 1 && $paymentDetails['captured'] == 1) {
                    /**
                     * You need to create model and other implementations
                     */

                    RealEstatePayment::create([
                        'name' => request('name'),
                        'email' => request('email'),
                        'amount' => $paymentDetails['amount'] / 100,
                        'currency' => $paymentDetails['currency'],
                        'transaction_id' => $paymentDetails['balance_transaction'],
                        'payment_status' => $paymentDetails['status'],
                        'receipt_url' => $paymentDetails['receipt_url']
                        // 'transaction_complete_details' => json_encode($paymentDetails)
                    ]);

                    return redirect('/thankyou/?receipt_url=' . $paymentDetails['receipt_url']);
                } else {
                    session()->flash('error', 'Transaction failed');
                    return back()->withInput();
                }
            } else {
                session()->flash('error', 'Error in capturing amount: ' . $apiError);
                return back()->withInput();
            }
        } else {
            session()->flash('error', 'Invalid card details: ' . $apiError);
            return back()->withInput();
        }
    }

    public function privateSellerPaymentStore(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'terms_conditions' => 'accepted'
        ]);

        /**
         * I have hard coded amount.
         * You may fetch the amount based on customers order or anything
         */
        $amount = 1 * 100;
        $currency = 'usd';

        if (empty(request()->get('stripeToken'))) {
            session()->flash('error', 'Some error while making the payment. Please try again');
            return back()->withInput();
        }
        Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
        try {
            /**
             * Add customer to stripe, Stripe customer
             */
            $customer = Customer::create([
                'name' => request('name'),
                'email' => request('email'),
                'source' => request('stripeToken')
            ]);
        } catch (Exception $e) {
            $apiError = $e->getMessage();
        }

        if (empty($apiError) && $customer) {
            /**
             * Charge a credit or a debit card
             */
            try {
                /**
                 * Stripe charge class
                 */
                $charge = Charge::create(array(
                    'customer' => $customer->id,
                    'amount' => $amount,
                    'currency' => $currency,
                    'description' => $request->get('adtitle')
                ));
            } catch (Exception $e) {
                $apiError = $e->getMessage();
            }

            if (empty($apiError) && $charge) {
                // Retrieve charge details
                // dd($charge);
                $paymentDetails = $charge->jsonSerialize();
                if ($paymentDetails['amount_refunded'] == 0 && empty($paymentDetails['failure_code']) && $paymentDetails['paid'] == 1 && $paymentDetails['captured'] == 1) {
                    /**
                     * You need to create model and other implementations
                     */

                    WatchPayment::create([
                        'name' => request('name'),
                        'email' => request('email'),
                        'amount' => $paymentDetails['amount'] / 100,
                        'currency' => $paymentDetails['currency'],
                        'transaction_id' => $paymentDetails['balance_transaction'],
                        'payment_status' => $paymentDetails['status'],
                        'receipt_url' => $paymentDetails['receipt_url']
                        // 'transaction_complete_details' => json_encode($paymentDetails)
                    ]);

                    $agent = AgentModel::where('userId', '=', $request->agentId)->first();
                    
                    if($agent == null){
                        
                        $user = User::find($request->agentId);
                        
                        $createAgent = new AgentModel();
                        
                            $createAgent->agency_id = "Private Seller";
                            
                            $createAgent->first_name = $request->firstName;
                            $createAgent->last_name = $request->lastName;
                            $createAgent->country_id = $request->country_pi;
                            if($request->input('stateId') != '')
                                $createAgent->state_id = $request->input('stateId');
                                $createAgent->city_id = $request->city_pi;
                                $createAgent->street_1 = $request->address;
                                $createAgent->street_2 = $request->input('street_2');
                                $createAgent->postal_code = $request->zipcode;
                                $createAgent->whatsapp = $request->phoneNumber;
                                $createAgent->mobile = $request->phoneNumber;
                                $createAgent->fax = $request->input('fax');
                                $createAgent->office = $request->input('office');
                                $createAgent->email = $user->email;
                                $createAgent->timezone_id = $request->input('timezone');
                                $createAgent->status = $request->input('status');
                                
                                
                                $role = Role::findByName('Agent');
                                $permission = Permission::get();
                                $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
                                ->where("role_has_permissions.role_id",$role->id)
                                ->get();
                                
                                if($rolePermissions !=null){
                                    foreach ($rolePermissions as $permission){
                                        
                                        $userPermission = new UserPermissionModel();
                                        
                                        $userPermission->userId = $request->agentId;
                                        $userPermission->permissionId = $permission->id;
                                        
                                        $userPermission->save();
                                    }
                                }
                                
                                $createAgent->userId = $request->agentId;
                                
                                $createAgent->save();
                                
                                $agent = $createAgent;
                    }
                    
                    $updateUser = UserDetailsModel::where('user_id', '=', $request->agentId)->first();

                    if ($updateUser != null) {
                        $updateUser->user_id = $request->agentId;
                        $updateUser->user_postalCode = $request->zipcode;
                        $updateUser->user_mobile = $request->phoneNumber;
                        $updateUser->city_id = $request->city_pi;
                        $updateUser->country_id = $request->country_pi;
                        $updateUser->user_address = $request->address;

                        $updateUser->save();
                    } else {
                        $createUser = new UserDetailsModel();

                        $createUser->user_id = $request->agentId;
                        $createUser->user_postalCode = $request->zipcode;
                        $createUser->user_mobile = $request->phoneNumber;
                        $createUser->city_id = $request->city_pi;
                        $createUser->country_id = $request->country_pi;
                        $createUser->user_address = $request->address;

                        $createUser->save();
                    }

                    $converted_amount=NULL;
                    if($request->input('price') !=''){
                        $currency_code = CurrencyModel::find($request->input('currency'));
                        
                        $converted_amount = currency()->convert(floatval($request->input('price')), $currency_code->currency_code, "USD",false);
                    }
                    
                    $createAutomobile = new AutomobileDataListingModel();

                    $createAutomobile->ad_title = $request->get('adtitle');
                    $createAutomobile->currency_id = $request->get('currency');
                    if ($request->get('stateId') != '')
                        $createAutomobile->state_id = $request->get('stateId');
                    $createAutomobile->country_id = $request->get('countryId');
                    $createAutomobile->city_id = $request->get('cityId');
                    $createAutomobile->make_id = $request->get('make1');
                    $createAutomobile->model_id = $request->get('model');
                    if ($request->get('price_on_request') != '')
                        $createAutomobile->price_on_request = $request->get('price_on_request');
                        $createAutomobile->value = $converted_amount;
                    $createAutomobile->price_id = $request->get('pricetType');
                    $createAutomobile->fuel_type_id = $request->get('fuelType');
                    $createAutomobile->colour_id = $request->get('color');
                    if ($request->get('no_of_doors') != '')
                        $createAutomobile->no_of_doors_id = $request->get('no_of_doors');
                    $createAutomobile->body_type_id = $request->get('body_type');
                    $createAutomobile->transmission = $request->get('transmission');
                    if ($request->get('registrationPlace') != '')
                        $createAutomobile->registration_place = $request->get('registrationPlace');
                    $createAutomobile->rhdorlhd = $request->get('rhdorlhd');
                    $createAutomobile->year_id = $request->get('year');
                    $createAutomobile->neworused = $request->get('condition');
                    $createAutomobile->odometer = $request->get('odometer');
                    $createAutomobile->metric = $request->get('odometermetric');
                    if ($request->get('engine') != '')
                        $createAutomobile->engine = $request->get('engine');
                    if ($request->get('interior_color') != '')
                        $createAutomobile->interior_colour_id = $request->get('interior_color');
                    $createAutomobile->transmission_id = $request->get('transmission');
                    $createAutomobile->timezone_id = $request->get('timezone');
                    $createAutomobile->agent_id = $agent->id;
                    $createAutomobile->user_id = $request->get('agentId');
                    $createAutomobile->vin_number = $request->get('VIN');
                    $createAutomobile->registration_number = $request->get('registrationPlate');

                    $createAutomobile->save();

                    $listing_id = $createAutomobile->id;

                    if ($request->input('additional_feature') ?? '') {

                        foreach ($request->input('additional_feature') as $features) {

                            if ($features != null) {
                                $uploadFeature = new AutomobileAdditionalFeaturesModel();

                                $uploadFeature->feature = $features;
                                $uploadFeature->listing_id = $listing_id;
                                $uploadFeature->save();
                            }
                        }
                    }

                    foreach ($request->request as $key => $value) {
                        if ($key != '_token' && $key != 'listing_id' && gettype($key) != 'string') {
                            $createFeature = new AutomobileFeaturesModel();
                            $createFeature->listing_id = $listing_id;
                            $createFeature->feature_id = $key;
                            $createFeature->feature_available = $value;
                            $createFeature->save();
                        }
                    }

                    if ($request->comment ?? '') {
                        $createComment = new AutomobileCommentModel();
                        $createComment->comment = $request->input('comment');
                        $createComment->listing_id = $listing_id;
                        $createComment->save();
                    }
                    if ($request->file('normalImages') ?? '') {
                        foreach ($request->file('normalImages') as $image) {
                            echo "<br>";
                            $uploadImage = new AutomobileImageModel();

                            $extension = $image->getClientOriginalExtension();
                            Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                            $uploadImage->images = $image->getClientMimeType();
                            $uploadImage->filename = $image->getFilename() . '.' . $extension;
                            $uploadImage->listing_id = $listing_id;

                            $uploadImage->save();
                        }
                    }

                    if ($request->file('damageImages2') ?? '') {
                        foreach ($request->file('damageImages2') as $image) {
                            $uploadImage = new AutomobileDamageImageModel();

                            $extension = $image->getClientOriginalExtension();
                            Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                            $uploadImage->images = $image->getClientMimeType();
                            $uploadImage->filename = $image->getFilename() . '.' . $extension;
                            $uploadImage->listing_id = $listing_id;

                            $uploadImage->save();
                        }
                    }

                    if ($request->file('security1') ?? '') {
                        foreach ($request->file('security1') as $image) {
                            $uploadImage = new AutomobileSecurityImageModel();

                            $extension = $image->getClientOriginalExtension();
                            Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                            $uploadImage->images = $image->getClientMimeType();
                            $uploadImage->filename = $image->getFilename() . '.' . $extension;
                            $uploadImage->listing_id = $listing_id;

                            $uploadImage->save();
                        }
                    }

                    if ($request->file('security2') ?? '') {
                        foreach ($request->file('security2') as $image) {
                            $uploadImage = new AutomobileSecurityImageModel();

                            $extension = $image->getClientOriginalExtension();
                            Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                            $uploadImage->images = $image->getClientMimeType();
                            $uploadImage->filename = $image->getFilename() . '.' . $extension;
                            $uploadImage->listing_id = $listing_id;

                            $uploadImage->save();
                        }
                    }
                    
                    /*$meeting = 'Test';
                    
                    Mail::send(['text'=>'mail'], ['meeting' =>  $meeting], function($message) {
                        $message->to('pashu.sharma@gmail.com', 'Tutorials Point')->subject
                        ('Laravel Basic Testing Mail');
                        $message->from('xyz@gmail.com','Virat Gandhi');
                    });*/

                    return redirect('/thankyou/?receipt_url=' . $paymentDetails['receipt_url']);
                } else {
                    session()->flash('error', 'Transaction failed');
                    dd('Transaction failed');
                    return back()->withInput();
                }
            } else {
                session()->flash('error', 'Error in capturing amount: ' . $apiError);
                dd('Error in capturing amount: ' . $apiError);
                return back()->withInput();
            }
        } else {
            session()->flash('error', 'Invalid card details: ' . $apiError);
            dd('Invalid card details ' . $apiError);
            return back()->withInput();
        }
    }

    public function privateSellerWatchPaymentStore(Request $request)
    {    
            $this->validate($request, [
                'countryId' => 'required',
                'stateId' => 'required',
                'cityId' => 'required',
                'referenceNo' => 'required',
                'watchCondition' => 'required',
                'gender' => 'required',
                'inclusions' => 'required',
                'timezone' => 'required',
                'email' => 'required|email',
                'privateSellerAgb' => 'accepted'
            ]);
    
        //dd($request);

        // Getting agent detials from the authenticated id

        $agent = AgentModel::where('userId', '=', $request->agentId)->first();

           
            
                // Inserteing watch details

                $dealer_id = "";

                if ($agent != null )
                $dealer_id = 'Private Seller';

                $watch_price = NULL;
                if($request->input('watch_price') != ''){

                $currency_code = CurrencyModel::find($request->input('currency'));

                $watch_price = currency()->convert(floatval($request->input('watch_price')), $currency_code->currency_code, "USD",false);
                //$priceSqft = $request->input('priceSqft');
                }

                $userId = Auth::id();
                $createWatch = new WatchDataListingModel();

                if($userId){
                $createWatch->createdID = $userId;
                }
                $createWatch->ad_title = $request->input('adtitle');
                $createWatch->currency_id = $request->input('currency');
                if ($request->input('stateId') != '')
                $createWatch->state_id = $request->input('stateId');
                $createWatch->country_id = $request->input('countryId');
                $createWatch->city_id = $request->input('cityId');
                $createWatch->brand_id = $request->input('brand');
                $createWatch->price_on_request = $request->input('price_on_request');
                $createWatch->model_id = $request->input('model');
                $createWatch->watch_price = $watch_price;
                $createWatch->commision =  $request->input('commision');
                $createWatch->revenue = $request->input('revenue');
                if ($request->input('pricetType') != '')
                $createWatch->price_id = $request->input('pricetType');
                if ($request->input('case_diameter') != '')
                $createWatch->case_diameter_id = $request->input('case_diameter');
                $createWatch->movement_id = $request->input('movement');
                $createWatch->type_id = $request->input('type');
                $createWatch->year_of_manufacture_id = $request->input('yom');
                $createWatch->gender_id = $request->input('gender');
                $createWatch->inclusions = $request->input('inclusions');
                $createWatch->reference_no = $request->input('referenceNo');
                $createWatch->watch_condition = $request->input('watchCondition');
                $createWatch->status = 0;
                $createWatch->brand_name = WatchModel::find($request->input('brand'))->watch_brand_name;
                $createWatch->model_name = Watchbrandmodel::find($request->input('model'))->watch_model_name;
                if ($agent != "")
                $createWatch->agent_id = $agent->id;
                if ($dealer_id != "")
                $createWatch->agencies_id = $dealer_id;
                $createWatch->timezone_id = $request->input('timezone');

                $createWatch->save();


                $listing_id = $createWatch->id;

                $createFeature = new WatchFeaturesModel();

                $createFeature->listing_id = $listing_id;
                $createFeature->bezel_material_id = $request->input('bezelMaterial');
                $createFeature->power_reserve_id = $request->input('powerReserve');
                $createFeature->dial_color_id = $request->input('dialColor');
                $createFeature->bracelet_material_id = $request->input('braceletMaterial');
                $createFeature->bracelet_color_id = $request->input('braceletColor');
                $createFeature->type_of_clasp_id = $request->input('typeOfClasp');
                $createFeature->clasp_material_id = $request->input('claspMaterial');
                $createFeature->case_material_id = $request->input('caseMaterial');
                $createFeature->case_mm_id = $request->input('caseMM');
                $createFeature->water_resisitance_depth_id = $request->input('wrd');
                $createFeature->glass_type_id = $request->input('glassType');

                if ($request->has('dialColor') && !empty($request->input('dialColor'))) {
                  $createFeature->dial_color = DialColorModel::find($request->input('dialColor'))->dial_color;
                }   

                if ($request->has('braceletMaterial') && !empty($request->input('braceletMaterial'))) {

                    $createFeature->bracelet_material = BraceletMaterialModel::find($request->input('braceletMaterial'))->bracelet_material;

                }
           
                $createFeature->save();

                /// Inserting Images

                if ($request->input('additional_feature') ?? '') {

                    foreach ($request->input('additional_feature') as $features) {

                        if ($features != null) {
                            $uploadFeature = new WatchAdditionalFeaturesModel();

                            $uploadFeature->feature = $features;
                            $uploadFeature->listing_id = $listing_id;
                            $uploadFeature->save();
                        }
                    }
                }

                foreach ($request->request as $key => $value) {
                    if ($key != '_token' && $key != 'listing_id' && gettype($key) != 'string') {
                        $createDetailFeature = new WatchDetailsFeaturesModel();
                        $createDetailFeature->listing_id = $listing_id;
                        $createDetailFeature->feature_id = $key;
                        $createDetailFeature->feature_available = $value;
                        $createDetailFeature->save();
                    }
                }

                if ($request->comment ?? '') {
                    $createComment = new WatchCommentModel();
                    $createComment->comment = $request->input('comment');
                    $createComment->listing_id = $listing_id;
                    $createComment->save();
                }

                if ($request->file('normalImages') ?? '') {

                    foreach ($request->file('normalImages') as $image) {
                        echo "<br>";
                        $uploadImage = new WatchImageModel();

                        $extension = $image->getClientOriginalExtension();
                        Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                        $uploadImage->images = $image->getClientMimeType();
                        $uploadImage->filename = $image->getFilename() . '.' . $extension;
                        $uploadImage->listing_id = $listing_id;

                        $uploadImage->save();
                    }
                }

                if ($request->file('damageImages2') ?? '') {
                    foreach ($request->file('damageImages2') as $image) {
                        $uploadImage = new WatchDamageImageModel();

                        $extension = $image->getClientOriginalExtension();
                        Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                        $uploadImage->images = $image->getClientMimeType();
                        $uploadImage->filename = $image->getFilename() . '.' . $extension;
                        $uploadImage->listing_id = $listing_id;

                        $uploadImage->save();
                    }
                }

                if ($request->file('security1') ?? '') {
                    foreach ($request->file('security1') as $image) {
                        $uploadImage = new WatchSecurityImageModel();
                        
                        $extension = $image->getClientOriginalExtension();
                        Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));
                        
                        $uploadImage->images = $image->getClientMimeType();
                        $uploadImage->filename = $image->getFilename() . '.' . $extension;
                        $uploadImage->security_time = $request->get('security-time1');
                        $uploadImage->listing_id = $listing_id;
                        
                        //print_r($uploadImage);
                        
                        $uploadImage->save();
                    }
                }
                
                if ($request->file('security2') ?? '') {
                    foreach ($request->file('security2') as $image) {
                        $uploadImage = new WatchSecurityImageModel();
                        
                        $extension = $image->getClientOriginalExtension();
                        Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));
                        
                        $uploadImage->images = $image->getClientMimeType();
                        $uploadImage->filename = $image->getFilename() . '.' . $extension;
                        $uploadImage->security_time = $request->get('security-time2');
                        $uploadImage->listing_id = $listing_id;
                        
                       // print_r($uploadImage);
                        
                        $uploadImage->save();
                    }
                }

                 // Uploading Documents Logic

                 if ($request->hasFile('license')) {  // Check if file input is set

                        $image = $request->license;
                        $extension = $image->getClientOriginalExtension();
                        $uploadImage = new WatchDocumentImageModel();
                        
                        Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));
                        
                       // $uploadImage->images = $image->getClientMimeType();
                       $uploadImage->filename = $image->getFilename() . '.' . $extension;
                       $uploadImage->type = 'license';
                       $uploadImage->listing_id = $listing_id;       
                       $uploadImage->save();
                  
                }

                if ($request->hasFile('drivingLicense')) {  // Check if file input is set
                   
                         $image = $request->drivingLicense;
                         $extension = $image->getClientOriginalExtension();
                         $uploadImage = new WatchDocumentImageModel();
                        
                        
                        Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));
                        
                       // $uploadImage->images = $image->getClientMimeType();
                       $uploadImage->filename = $image->getFilename() . '.' . $extension;
                       $uploadImage->type = 'drivingLicense';
                       $uploadImage->listing_id = $listing_id;       
                    $uploadImage->save();
                    
                }

                if ($request->hasFile('utilityBill')) {  // Check if file input is set
                
                        $image = $request->utilityBill;
                        $extension = $image->getClientOriginalExtension();
                        $uploadImage = new WatchDocumentImageModel();
                        
                       
                        Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));
                        
                       // $uploadImage->images = $image->getClientMimeType();
                       $uploadImage->filename = $image->getFilename() . '.' . $extension;
                       $uploadImage->type = 'utilityBill';
                       $uploadImage->listing_id = $listing_id;       
                    $uploadImage->save();
                    
                }

                if ($request->hasFile('passport')) {  // Check if file input is set
                
                        $image = $request->passport;
                        $extension = $image->getClientOriginalExtension();
                        $uploadImage = new WatchDocumentImageModel();
                        
                        
                        Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));
                        
                       // $uploadImage->images = $image->getClientMimeType();
                       $uploadImage->filename = $image->getFilename() . '.' . $extension;
                       $uploadImage->type = 'passport';
                       $uploadImage->listing_id = $listing_id;       
                       $uploadImage->save();
                    
                }

                // Uploading Documents Logic


                    if ($createWatch) {
                    // Retrieve charge details
                    $detal = new WatchPayment;
                    $detal->listingId = $listing_id;   
                    $detal->name = $request->firstName." ".$request->lastName;
                    $detal->email = $request->email;
                    $detal->phone = $request->phoneNumber;
                    $detal->amount = $request->paymentAmount;
                    $detal->currency = $request->currency;
                    //'transaction_id' => $paymentDetails['balance_transaction']
                    $detal->bankName = $request->bankName;
                    $detal->beneficiaryName = $request->beneficiaryName;
                    $detal->bankAccount = $request->bankAccount;
                    $detal->ifsc = $request->ifsc;
                    $detal->payment_status = "pending";//$paymentDetails['status'],
                    $detal->address1 = $request->address;
                    $detal->address2 = $request->street_2;
                    $detal->zip = $request->zipcode;
                    $detal->country = $request->country_pi;
                    $detal->state = $request->state_pi;
                    $detal->city = $request->city_pi;
                    $detal->save();
                    //'receipt_url' => $paymentDetails['receipt_url']
                    // 'transaction_complete_details' => json_encode($paymentDetails)
                    
                }

                $paymentAmount = request('paymentAmount');

                if($paymentAmount == 0){

                    return redirect('/thankyou/');

                }else{

                    $sessionArray = array("listingId"=>$listing_id, "amount" => request('paymentAmount'), "type" => "tbl_paymentWatch");

                    $request->session()->put('paymentData', $sessionArray);
    
                    return redirect('/stripe-payment/');


                }

             

        
     

        /**
         * I have hard coded amount.
         * You may fetch the amount based on customers order or anything
         */
        // $amount = 1 * 100;
        // $currency = 'usd';

        // if (empty(request()->get('stripeToken'))) {
        //     session()->flash('error', 'Some error while making the payment. Please try again');
        //     return back()->withInput();
        // }
        // Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
        // try {
        //     /**
        //      * Add customer to stripe, Stripe customer
        //      */
        //     $customer = Customer::create([
        //         'name' => request('name'),
        //         'email' => request('email'),
        //         'source' => request('stripeToken')
        //     ]);
        // } catch (Exception $e) {
        //     $apiError = $e->getMessage();
        // }

        //if (empty($apiError) && $customer) {
            /**
             * Charge a credit or a debit card
             */
            // try {
            //     /**
            //      * Stripe charge class
            //      */
            //     $charge = Charge::create(array(
            //         'customer' => $customer->id,
            //         'amount' => $amount,
            //         'currency' => $currency,
            //         'description' => $request->get('adtitle')
            //     ));
            // } catch (Exception $e) {
            //     $apiError = $e->getMessage();
            // }

           //if (empty($apiError) && $charge) {
                // Retrieve charge details
                // dd($charge);
                // $paymentDetails = $charge->jsonSerialize();
                // if ($paymentDetails['amount_refunded'] == 0 && empty($paymentDetails['failure_code']) && $paymentDetails['paid'] == 1 && $paymentDetails['captured'] == 1) {
                    /**
                     * You need to create model and other implementations
                     */

                    // WatchPayment::create([
                    //     'name' => request('name'),
                    //     'email' => request('email'),
                    //     'amount' => $paymentDetails['amount'] / 100,
                    //     'currency' => $paymentDetails['currency'],
                    //     'transaction_id' => $paymentDetails['balance_transaction'],
                    //     'payment_status' => $paymentDetails['status'],
                    //     'receipt_url' => $paymentDetails['receipt_url']
                    //     // 'transaction_complete_details' => json_encode($paymentDetails)
                    // ]);

                   // return redirect('/thankyou/');

                 //   return redirect('/thankyou/?receipt_url=' . $paymentDetails['receipt_url']);
                // } else {
                //     session()->flash('error', 'Transaction failed');
                //     dd('Transaction failed');
                //     return back()->withInput();
                // }
            // } else {
            //     session()->flash('error', 'Error in capturing amount: ' . $apiError);
            //     dd('Error in capturing amount: ' . $apiError);
            //     return back()->withInput();
            // }
        // } else {
        //     session()->flash('error', 'Invalid card details: ' . $apiError);
        //     dd('Invalid card details ' . $apiError);
        //     return back()->withInput();
        // }
    }

    public function thankyou()
    {
        return view('payments.thankyou');
    }
}