<?php

namespace App\Http\Controllers;

use App\CaseGlassTypeModel;
use Illuminate\Http\Request;

class CaseGlassTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $case = CaseGlassTypeModel::get();
        return view('admin.watch.caseGlassType.index',compact('case'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.caseGlassType.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validate($request, [
            'glass_type' => "required|unique:wa_factory_features_case_glassType",
            'status' => "required"
        ]);

          $case = CaseGlassTypeModel::create($validator);
          $case->save();
       
      return redirect('/admin/caseGlassType')->with('success_msg','Case Glass Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $case_data = CaseGlassTypeModel::find($id);
        return view('admin.watch.caseGlassType.edit', compact('case_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            'glass_type' => "required",
            'status' => "required"
        ]);

        $case = CaseGlassTypeModel::find($id);

        $case->glass_type = $validator['glass_type'];
        $case->status = $validator['status'];
        $case->save();
        return redirect('/admin/caseGlassType')->with('success_msg','Case Glass Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = CaseGlassTypeModel::find($id)->delete();
        return redirect('/admin/caseGlassType')->with('success_msg','Case Glass Deleted successfully!');
    }
}
