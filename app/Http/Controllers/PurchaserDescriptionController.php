<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PurchaserDescriptionModel;

class PurchaserDescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchaser_description = PurchaserDescriptionModel::get();
        return view('admin.realestate.purchaser_description.index',compact('purchaser_description'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.realestate.purchaser_description.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'purchaser_description' => 'required|regex:/^[\pL\s_]+$/u|unique:re_purchaser_description',
            'status' => 'required'
        ]);

        $purchaser_description = PurchaserDescriptionModel::create($validate);
        $purchaser_description->save();
        return redirect('/admin/purchaser_description')->with('success_msg','Purchaser Description Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $purchaser_description_data = PurchaserDescriptionModel::find($id);
        return view('admin.realestate.purchaser_description.edit', compact('purchaser_description_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
           $validate = $this->validate($request, [
            'purchaser_description' => 'required|regex:/^[\pL\s_]+$/u'
        ]);

        $purchaser_descriptionUpdate = PurchaserDescriptionModel::find($id);

        if ($purchaser_descriptionUpdate->purchaser_description == $validate['purchaser_description']) {
            $purchaser_descriptionUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['purchaser_description' => 'unique:re_purchaser_description']);
            $purchaser_descriptionUpdate->purchaser_description = $validate['purchaser_description'];
            $purchaser_descriptionUpdate->status = $request->input('status');
        }

        $purchaser_descriptionUpdate->save();

        return redirect('/admin/purchaser_description')->with('success_msg','Purchaser Description Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = PurchaserDescriptionModel::find($id)->delete();
        return redirect('/admin/purchaser_description')->with('success_msg','Purchaser Description deleted successfully!');
    }
}
