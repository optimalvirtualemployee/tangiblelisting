<?php

namespace App\Http\Controllers;

use App\AutomobileCylindersModel;
use Illuminate\Http\Request;

class AutomobileCylindersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cylinders = AutomobileCylindersModel::get();
        return view('admin.automobile.cylinders.index',compact('cylinders'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.automobile.cylinders.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'cylinders' => 'required|unique:au_cylinders',
            'status' => 'required'
        ]);
        
        $cylinders = AutomobileCylindersModel::create($validate);
        $cylinders->save();
        return redirect('/admin/cylinders')->with('success_msg','Cylinders Created successfully!');
    }
    
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cylinders_data = AutomobileCylindersModel::find($id);
        return view('admin.automobile.cylinders.edit', compact('cylinders_data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'cylinders' => 'required'
        ]);
        
        $cylindersUpdate = AutomobileCylindersModel::find($id);
        
        
        $cylindersUpdate->cylinders = $validate['cylinders'];
        $cylindersUpdate->status = $request->input('status');
        
        $cylindersUpdate->save();
        
        return redirect('/admin/cylinders')->with('success_msg','Cylinders Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = AutomobileCylindersModel::find($id)->delete();
        return redirect('/admin/cylinders')->with('success_msg','Cylinders deleted successfully!');
    }
}
