<?php

namespace App\Http\Controllers;

use App\PropertyDataModel;
use Illuminate\Http\Request;

class PropertyTopHomesController extends Controller
{
    public function showTopHomesProperties(Request $request){
        
        $properties = PropertyDataModel::select('re_property_details.id', 'ad_title', 'land_size', 're_property_details.state_id', 're_property_details.city_id', 'city_name', 'state_name')->join('tbl_city', 'tbl_city.id', '=', 're_property_details.city_id')
        ->join('tbl_state', 'tbl_state.id', '=', 're_property_details.state_id')
        ->where('topHomes', '=','1')
        ->get();
        
        return view('admin.realestate.tophomes.tophomes')->with(array(
            'properties' => $properties
        ));
        
    }
    
    public function editTopHomes($id){
        
        $data = PropertyDataModel::find($id);
        
        return view('admin.realestate.tophomes.edittophomes', compact('data'));
    }
    
    public function updateTopHomes(Request $request, $id){
        
        $data = $request->validate([
            'topHomes' => ['required']
        ]);
        
        $updateFeatured = PropertyDataModel::find($id);
        
        $updateFeatured->topHomes = $request->input('topHomes');
        
        $updateFeatured->save();
        
        return redirect('/admin/tophomesproperty')->with('success_msg', 'Property Feature Updated successfully!');
    }
}
