<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\PropertyDataModel;
use App\PropertyFeaturesListingModel;
use App\StateModel;
use App\CityModel;
use App\CountryModel;
use App\NoOfBedroomsModel;
use App\NoOfBathroomsModel;
use App\PropertyTypeModel;
use App\BuildYearModel;
use App\CurrencyModel;
use App\PropertyPriceModel;
use App\AgentModel;
use App\PropertyCommentModel;
use App\PropertyImageModel;
use App\PropertyFeaturesModel;
use App\PropertyAdditionalFeaturesModel;
use App\NoOfBalconiesModel;
use App\Timezone;
use App\TopPropertyTypeModel;

class PropertyDataController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        /* $ipaddress = $_SERVER['REMOTE_ADDR'];
        $page = "http://".$_SERVER['HTTP_HOST']."".$_SERVER['PHP_SELF'];
        $datetime = date("F j, Y, g:i a");
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        
        echo "<p>IP Address : ".$ipaddress."</p>";
        echo "<p>Current Page : ".$page."</p>";
        echo "<p>Current Time : ".$datetime."</p>";
        echo "<p>Browser : ".$useragent."</p>";
        
        echo "<br>";
        dd($_server);
 */        
        $properties = PropertyDataModel::select('re_property_details.id','re_property_details.status as status' ,'latestOffer','ad_title', 'land_size', 're_property_details.state_id', 're_property_details.city_id', 'city_name', 'state_name')->join('tbl_city', 'tbl_city.id', '=', 're_property_details.city_id')
        ->leftjoin('tbl_state', function($join){
            $join->on('tbl_state.id','=','re_property_details.state_id');
            $join->orOn('tbl_state.id','=','re_property_details.state_id', '=', 0);
        })->get();
        
        return view('admin.realestate.property_datamsf.index')->with(array(
            'properties' => $properties
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $city_data = CityModel::get();
        $state_data = StateModel::get();
        $country_data = CountryModel::get();
        $bedroom_data = NoOfBedroomsModel::get();
        $bathroom_data = NoOfBathroomsModel::get();
        $parking_data = NoOfBalconiesModel::get();
        $propertyType_data = PropertyTypeModel::get();
        $buildYear_data = BuildYearModel::get();
        $currency_data = CurrencyModel::get();
        $prices = PropertyPriceModel::get();
        $agents = AgentModel::get();
        $toppropertyType_data = TopPropertyTypeModel::get();
        $timezone = Timezone::Orderby('offset')->get();
        
        $feature_listing = PropertyFeaturesListingModel::get();
        
        $html ='';
        foreach ($feature_listing as $values){
            $id = $values->id;
            $propertyName = $values->property_name;
            
            $html .= '<div class="form-group">
                    <tr>
                   <td><label style= margin-right:10px!important; ><Strong>'.$propertyName.':</Strong></label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$propertyName.'" name="'.$id.'" value="1"><label style= margin-right:5px!important; for="yes"> Yes </label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$propertyName.'" name="'.$id.'" value="0" checked><label style= margin-right:5px!important; for="no"> No</label></td>
                    </tr>
                      </div>';
            
        }

        return view('admin.realestate.property_datamsf.create', compact('timezone','parking_data','html','city_data', 'state_data', 'country_data', 'bedroom_data', 'bathroom_data', 'propertyType_data',
                    'buildYear_data','currency_data', 'prices', 'agents', 'toppropertyType_data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'currency' => 'required',
            'cityId' => 'required',
            'countryId' => 'required',
            'establishmentType' => 'required',
            'bedRoom' => 'required',
            'bathRoom' => 'required',
            'toppropertyType' => 'required',
            'propertyType' => 'required',
            'metric' => 'required',
            'sol' => 'required',
            'buildingSize' => 'required',
            'propertySL' => 'required',
            'postalCode' => 'required',
            'builtId' => 'required',
            'street_1' => 'required',
            'street_2' => 'required',
            'agent' => 'required',
            'status' => 'required',
            'comment' => 'required',
            'photos' => 'required',
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
            
        ]);
        
        $converted_amount = NULL;

        if($request->input('priceSqft') != ''){
        $currency_code = CurrencyModel::find($request->input('currency'));
        
        $converted_amount = currency()->convert(floatval($request->input('priceSqft')), $currency_code->currency_code, "USD",false);
        }
        
        $agent = AgentModel::find($request->input('agent'));
        $dealer_id = $agent->agency_id;
        
        $createProperty = new PropertyDataModel();
        $createProperty->ad_title = $request->input('adTitle');
        $createProperty->currency_id = $request->input('currency');
        $createProperty->city_id = $request->input('cityId');
        if($request->input('stateId') != '')
        $createProperty->state_id = $request->input('stateId');
        $createProperty->country_id = $request->input('countryId');
        /* $createProperty->featured = $request->input('featured');
        $createProperty->topHomes = $request->input('topHomes');
        $createProperty->dream_location = $request->input('dreamLocation'); */
        $createProperty->establishment_type = $request->input('establishmentType');
        $createProperty->bed_id = $request->input('bedRoom');
        $createProperty->bathroom_id = $request->input('bathRoom');
        $createProperty->top_property_type_id = $request->input('toppropertyType');
        $createProperty->property_type_id = $request->input('propertyType');
        $createProperty->metric = $request->input('metric');
        $createProperty->land_size = $request->input('sol');
        $createProperty->building_size = $request->input('buildingSize');
        $createProperty->property_sale_lease = $request->input('propertySL');
        if($request->input('unitNumber') != '')
        $createProperty->unit_number = $request->input('unitNumber');
        $createProperty->postal_code = $request->input('postalCode');
        $createProperty->price_id = $request->input('price');
        $createProperty->price_on_request = $request->input('price_on_request');
        $createProperty->property_price = $converted_amount;
        $createProperty->year_built_id = $request->input('builtId');
        $createProperty->street_1 = $request->input('street_1');
        $createProperty->street_2 = $request->input('street_2');
        $createProperty->agent_id = $request->input('agent');
        $createProperty->agencies_id = $dealer_id;
        $createProperty->status = $request->input('status');
        $createProperty->parking_id = $request->input('parking');
        $createProperty->timezone_id = $request->input('timezone');

        $createProperty->save();

        $listing_id = $createProperty->id;
        
        if($request->input('additional_feature') ?? ''){
            
            foreach ($request->input('additional_feature') as $features){

                if($features != null){
                $uploadFeature = new PropertyAdditionalFeaturesModel();
                
                $uploadFeature->feature = $features;
                $uploadFeature->listing_id = $listing_id;
                $uploadFeature->save();
                }
            }
        }
        
        
        foreach ($request->request as $key => $value){
            if($key != '_token' && $key != 'listing_id' && gettype($key) != 'string'){
                $createFeature = new PropertyFeaturesModel();
                $createFeature->listing_id = $listing_id;
                $createFeature->feature_id = $key;
                $createFeature->feature_available = $value;
                $createFeature->save();
            }
            
        }
        if($request->comment ?? ''){
            $createComment = new PropertyCommentModel();
            $createComment->comment = $request->input('comment');
            $createComment->listing_id = $listing_id;
            $createComment->save();
        }
        if($request->file('photos') ?? ''){
        
            foreach ($request->file('photos') as $image){
                $uploadImage = new PropertyImageModel();
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                
                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename().'.'.$extension;
                $uploadImage->listing_id = $listing_id;
                $uploadImage->save();
            }
        }
        
        return redirect('/admin/propertyData')->with('success_msg', 'Property Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $property = PropertyDataModel::find($id);
        $currency_data = CurrencyModel::get();
        $city_data = CityModel::get();
        $state_data = StateModel::get();
        $country_data = CountryModel::get();
        $bedroom_data = NoOfBedroomsModel::get();
        $bathroom_data = NoOfBathroomsModel::get();
        $parking_data = NoOfBalconiesModel::get();
        $propertyType_data = PropertyTypeModel::where('property_type', '=',$property->top_property_type_id)->get();
        $buildYear_data = BuildYearModel::get();
        $prices = PropertyPriceModel::get();
        $agents = AgentModel::get();
        $toppropertyType_data = TopPropertyTypeModel::get();
        $timezone = Timezone::Orderby('offset')->get();
        
        $priceSelected = PropertyPriceModel::find($property->price_id);
        $currencySelected = CurrencyModel::find($property->currency_id);
        $citySelected = CityModel::find($property->city_id);
        $stateId = 0;
        if($property->state_id != '')
            $stateId = $property->state_id;
            
        $stateSelected = StateModel::find($stateId);
        $countrySelected = CountryModel::find($property->country_id);
        $bathroomSelected = NoOfBathroomsModel::find($property->bathroom_id);
        $parkingSelected = NoOfBalconiesModel::find($property->parking_id);
        $bedroomSelected = NoOfBedroomsModel::find($property->bed_id);
        $typeSelected = PropertyTypeModel::find($property->property_type_id);
        $yearSelected = BuildYearModel::find($property->year_built_id); 
        $agentSelected = AgentModel::find($property->agent_id);
        $toppropertySelected = TopPropertyTypeModel::find($property->top_property_type_id);
        $timezoneSelected = Timezone::find($property->timezone_id);
        
        $currency_code = $currencySelected->currency_code;
        
        $converted_amount = currency()->convert(floatval($property->property_price), "USD", $currency_code ,false);
        
        $property->property_price = $converted_amount;
        
        $additionlFeatures = PropertyAdditionalFeaturesModel::select('re_property_additional_features.id as id', 'listing_id' , 'feature')
        ->where('listing_id', '=', $id)
        ->get();
            
        $additionalHtml = '';
        foreach($additionlFeatures as $features){
            
            $additionalHtml .= '<tr class = "control-group-feature">
                    <td><label  >Additional Feature</label>
                    <input  class="form-control form-control-user " type="text" maxLength="100"  name="additional_feature[]" value="'.$features->feature.'"></td>
                    <td><button class="btn btn-primary  btn-danger btn-danger-feature" type="button">
				<i class="glyphicon glyphicon-remove"></i> Remove
			</button></td>
                    </tr>';
        }
        
        $feature_data =PropertyFeaturesModel::select('re_property_features.id as id', 'listing_id' ,'property_name', 'feature_available', 'feature_id')
        ->join('re_properties_features_listing', 're_properties_features_listing.id', '=', 're_property_features.feature_id')
        ->where('listing_id', '=', $id)
        ->get();
        
        $html = '';
        foreach ($feature_data as $values){
            
            $yesChecked = '';
            $noChecked = '';
            if($values->feature_available == '1'){
                $yesChecked = 'checked';
            }else{
                $noChecked = 'checked';
            }
            
            $html .= '<div class="form-group">
                    <tr>
                    <td><label style= margin-right:10px!important; ><Strong>'.$values->property_name.':</Strong></label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$values->propertyName.'" name="'.$values->feature_id.'" value="1"'.$yesChecked.'><label style= margin-right:5px!important; for="yes"> Yes </label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$values->propertyName.'" name="'.$values->feature_id.'" value="0" '.$noChecked.'><label style= margin-right:5px!important; for="no"> No</label></td>
                    </tr>
                    </div>';
        }
        
        $comment_data = PropertyCommentModel::where('listing_id',$id)->first();
        
        $images_data = PropertyImageModel::where('listing_id', '=', $id)->get();
        
        return view('admin.realestate.property_datamsf.edit', compact('timezone','timezoneSelected','parkingSelected','parking_data','additionalHtml','images_data',
            'comment_data','html','agents', 'property','prices','currency_data' ,'city_data', 'state_data', 'country_data', 
            'bedroom_data', 'bathroom_data', 'propertyType_data', 'buildYear_data', 'citySelected', 'stateSelected', 'countrySelected', 
            'bathroomSelected', 'bedroomSelected', 'yearSelected', 'typeSelected', 'currencySelected','priceSelected', 'agentSelected',
             'toppropertyType_data', 'toppropertySelected'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'currency' => 'required',
            'cityId' => 'required',
            'countryId' => 'required',
            'establishmentType' => 'required',
            'bedRoom' => 'required',
            'bathRoom' => 'required',
            'toppropertyType' => 'required',
            'propertyType' => 'required',
            'metric' => 'required',
            'sol' => 'required',
            'buildingSize' => 'required',
            'propertySL' => 'required',
            'postalCode' => 'required',
            'builtId' => 'required',
            'street_1' => 'required',
            'street_2' => 'required',
            'agent' => 'required',
            'status' => 'required',
            'comment' => 'required',
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
            
        ]);
        
        $unitNumber = NULL;
        if($request->input('unitNumber') != ''){
            $unitNumber = $request->input('unitNumber');
        }
        
        $priceSqft = NULL;
        if($request->input('priceSqft') != ''){
            
            $currency_code = CurrencyModel::find($request->input('currency'));
            
            $priceSqft = currency()->convert(floatval($request->input('priceSqft')), $currency_code->currency_code, "USD",false);
            //$priceSqft = $request->input('priceSqft');
        }
        
        $price = NULL;
        if($request->input('price') != ''){
            $price = $request->input('price');
        }
        
        $agent = AgentModel::find($request->input('agent'));
        $dealer_id = $agent->agency_id;
        
        $updateProperty = PropertyDataModel::find($id);
        
        $updateProperty->ad_title = $request->input('adTitle');
        $updateProperty->currency_id = $request->input('currency');
        $updateProperty->city_id = $request->input('cityId');
        if($request->input('stateId') != '')
        $updateProperty->state_id = $request->input('stateId');
        $updateProperty->country_id = $request->input('countryId');
        $updateProperty->establishment_type = $request->input('establishmentType');
        $updateProperty->bed_id = $request->input('bedRoom');
        $updateProperty->bathroom_id = $request->input('bathRoom');
        $updateProperty->top_property_type_id = $request->input('toppropertyType');
        $updateProperty->property_type_id = $request->input('propertyType');
        $updateProperty->metric = $request->input('metric');
        $updateProperty->land_size = $request->input('sol');
        $updateProperty->building_size = $request->input('buildingSize');
        $updateProperty->property_sale_lease = $request->input('propertySL');
        $updateProperty->unit_number = $unitNumber;
        $updateProperty->postal_code = $request->input('postalCode');
        $updateProperty->price_id = $price;
        $updateProperty->price_on_request = $request->input('price_on_request');
        $updateProperty->property_price = $priceSqft;
        $updateProperty->year_built_id = $request->input('builtId');
        $updateProperty->street_1 = $request->input('street_1');
        $updateProperty->street_2 = $request->input('street_2');
        $updateProperty->agent_id = $request->input('agent');
        $updateProperty->agencies_id = $dealer_id;
        $updateProperty->status = $request->input('status');
        $updateProperty->parking_id = $request->input('parking');
        $updateProperty->timezone_id = $request->input('timezone');

        $updateProperty->save();
        
        $deleteRecords = PropertyAdditionalFeaturesModel::where('listing_id', '=', $id)->delete();
        
        if($request->input('additional_feature') ?? ''){
            
            foreach ($request->input('additional_feature') as $features){
                
                if($features != null){
                    $uploadFeature = new PropertyAdditionalFeaturesModel();
                    
                    $uploadFeature->feature = $features;
                    $uploadFeature->listing_id = $id;
                    $uploadFeature->save();
                }
            }
        }
        
        $deleteRecords = PropertyFeaturesModel::where('listing_id', '=', $id)->delete();
        
        foreach ($request->request as $key => $value){

            if($key != '_token' && $key != 'listing_id' && gettype($key) != 'string'){
                $createFeature = new PropertyFeaturesModel();
                $createFeature->listing_id = $id;
                $createFeature->feature_id = $key;
                $createFeature->feature_available = $value;
                $createFeature->save();
            }
            
        }
        if($request->comment ?? ''){
            $updateComment = PropertyCommentModel::where('listing_id',$id)->first();
            $updateComment->comment = $request->comment;
            $updateComment->listing_id = $id;
            $updateComment->save();
        }
        if($request->file('photos') ?? ''){
            foreach ($request->file('photos') as $image){
                $uploadImage = new PropertyImageModel();
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                
                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename().'.'.$extension;
                $uploadImage->listing_id = $id;
                $uploadImage->save();
            }
        }
        
        return redirect('/admin/propertyData')->with('success_msg', 'Property Updated successfully!');
    }
    
    public function updateLatestOffer($id){
        
        $property = PropertyDataModel::find($id);
        
        if($property->latestOffer == '0'){
            
            $property->latestOffer = '1';
            
        }else{
            $property->latestOffer = '0';
        }
        
        $property->save();
        
        return redirect('/admin/propertyData')->with('success_msg', 'Latest Offer Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = PropertyDataModel::find($id)->delete();
        return redirect('/admin/propertyData')->with('success_msg', 'Property Deleted successfully!');
        
    }
}