<?php

namespace App\Http\Controllers;

use App\AgenciesModel;
use App\ContactusModel;
use App\TypeModel;
use App\WatchBlogModel;
use App\WatchDataListingModel;
use Illuminate\Http\Request;
use App\PropertyTypeModel;
use App\Watchbrandmodel;
use App\WatchModel;
use App\YearOfManufactureModel;
use App\MovementModel;
use App\WatchImageModel;
use App\WatchBlogImageModel;
use App\WatchModelImage;
use App\TypeImageModel;
use App\CurrencyModel;

class TangibleListingWatches extends Controller
{
    public function index(){
        
        $contactUs = ContactusModel::get()->first();
        $propertyType = PropertyTypeModel::get();
        
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        
       $total_listing = WatchDataListingModel::where('status', '=' , '1')->get();
        
        $latest_listing = WatchDataListingModel::select('ad_title', 'wa_watch_detail.id as id' ,'currency_code', 'brand_name', 'model_name','watch_price', 'year_of_manufacture', 'case_diameter', 'city_name',
            'state_name','country_name' ,'watch_price', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode', 'tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id')
            ->join('tbl_currency', 'tbl_currency.id', '=', 'wa_watch_detail.currency_id')
            ->leftJoin('wa_year_of_manufacture', 'wa_year_of_manufacture.id', '=', 'wa_watch_detail.year_of_manufacture_id')
            ->leftJoin('wa_case_diameter','wa_case_diameter.id', '=', 'wa_watch_detail.case_diameter_id' )
            ->join('tbl_state', 'tbl_state.id', '=', 'wa_watch_detail.state_id')
            ->join('tbl_city', 'tbl_city.id', '=', 'wa_watch_detail.city_id')
            ->join('tbl_country', 'tbl_country.id', '=', 'wa_watch_detail.country_id')
            ->join('tbl_agent', 'tbl_agent.id', '=', 'wa_watch_detail.agent_id')
            ->whereIN('wa_watch_detail.id', function($query){
                $query->select('listing_id')
                ->from('wa_watch_images');
            })
            ->whereIN('wa_watch_detail.id', function($query){
                $query->select('listing_id')
                ->from('wa_watch_features');
            })
            ->whereIN('wa_watch_detail.id', function($query){
                $query->select('listing_id')
                ->from('wa_watch_comment');
            })->where('wa_watch_detail.status','=', '1')
            ->orWhere('wa_watch_detail.case_diameter_id','=', 'NULL')
            ->limit('8')
            ->get();
            
            
            $premium_listing = WatchDataListingModel::select('ad_title', 'wa_watch_detail.id as id' ,'currency_code', 'brand_name', 'model_name','watch_price', 'year_of_manufacture', 'case_diameter', 'city_name',
                'state_name','country_name' ,'watch_price', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode', 'tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id')
                ->join('tbl_currency', 'tbl_currency.id', '=', 'wa_watch_detail.currency_id')
                ->leftJoin('wa_year_of_manufacture', 'wa_year_of_manufacture.id', '=', 'wa_watch_detail.year_of_manufacture_id')
                ->leftJoin('wa_case_diameter','wa_case_diameter.id', '=', 'wa_watch_detail.case_diameter_id' )
                ->join('tbl_state', 'tbl_state.id', '=', 'wa_watch_detail.state_id')
                ->join('tbl_city', 'tbl_city.id', '=', 'wa_watch_detail.city_id')
                ->join('tbl_country', 'tbl_country.id', '=', 'wa_watch_detail.country_id')
                ->join('tbl_agent', 'tbl_agent.id', '=', 'wa_watch_detail.agent_id')
                ->whereIN('wa_watch_detail.id', function($query){
                    $query->select('listing_id')
                    ->from('wa_watch_images');
                })
                ->whereIN('wa_watch_detail.id', function($query){
                    $query->select('listing_id')
                    ->from('wa_watch_features');
                })
                ->whereIN('wa_watch_detail.id', function($query){
                    $query->select('listing_id')
                    ->from('wa_watch_comment');
                })->where('wa_watch_detail.status','=', '1')
                ->orWhere('wa_watch_detail.case_diameter_id','=', 'NULL')
                ->limit('8')
                ->get();
                
                $listing_images = WatchImageModel::get();
        
        $movement = MovementModel::get();
        $watchTypes = TypeModel::where('status','=', '1')->get();
        $type_images = TypeImageModel::get();
        
        $makes = WatchModel::where('status','=', '1')->orderBy('watch_brand_name', 'ASC')->get();

        $makes_images = WatchModelImage::get();

        $models = Watchbrandmodel::where('status','=', '1')->orderBy('watch_model_name', 'ASC')->get();
        
        $from_year = YearOfManufactureModel::orderBy('year_of_manufacture', 'ASC')->get();
        $to_year = YearOfManufactureModel::orderBy('year_of_manufacture', 'Desc')->get();
        
        $dealer_listing = AgenciesModel::select('tbl_agencies.id as id', 'first_name', 'filename')
        ->join('tbl_agencies_images', 'tbl_agencies_images.listing_id', '=', 'tbl_agencies.id')
        ->orderBy('tbl_agencies.created_at', 'desc')
        ->limit('4')
        ->get();
        
        $watch_blogs = WatchBlogModel::select('wa_blog.id as id', 'title', 'blogPost', 'wa_blog.created_at as created_at', 'blogType','url')
        ->where('status', '=', '1')->orderBy('wa_blog.created_at','asc')->limit('4')->get();
        
        $blog_images = WatchBlogImageModel::get();
        
        $makes_brand = WatchModel::where('show_under_brand', '=', '1')->get();
        
        return view('tangiblehtml.watch-landing', compact('total_listing','premium_listing','propertyType','listing_images','watchTypes','movement','dealer_listing','contactUs','watch_blogs','latest_listing', 'makes',
                    'models', 'from_year', 'to_year', 'blog_images','makes_images', 'type_images', 'makes_brand'));
    }
}
