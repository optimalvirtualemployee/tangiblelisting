<?php

namespace App\Http\Controllers;

use App\AutomobileDataListingModel;
use Illuminate\Http\Request;

class AutomobileFeaturedController extends Controller
{
    public function showFeaturedProperties(Request $request){
        
        $automobiles = AutomobileDataListingModel::where('featured', '=','1')->get();
        
        return view('admin.automobile.featured.featured')->with(array(
            'automobiles' => $automobiles
        ));
        
    }
    
    public function editFeatured($id){
        
        $featured = AutomobileDataListingModel::find($id);
        
        return view('admin.automobile.featured.editfeatured', compact('featured'));
    }
    
    public function updateFeatured(Request $request, $id){
        
        $data = $request->validate([
            'featured' => ['required']
        ]);
        
        $updateFeatured = AutomobileDataListingModel::find($id);
        
        $updateFeatured->featured = $request->input('featured');
        
        $updateFeatured->save();
        
        return redirect('/admin/featuredautomobile')->with('success_msg', 'Automobile Feature Updated successfully!');
    }
}
