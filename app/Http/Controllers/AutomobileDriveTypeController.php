<?php

namespace App\Http\Controllers;

use App\AutomobileDriveTypeModel;
use Illuminate\Http\Request;

class AutomobileDriveTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $drive_type = AutomobileDriveTypeModel::get();
        return view('admin.automobile.drive_type.index',compact('drive_type'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.automobile.drive_type.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'drive_type' => 'required|unique:au_drive_type',
            'status' => 'required'
        ]);
        
        $drive_type = AutomobileDriveTypeModel::create($validate);
        $drive_type->save();
        return redirect('/admin/drive_type')->with('success_msg','Drive Type Created successfully!');
    }
    
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $drive_type_data = AutomobileDriveTypeModel::find($id);
        return view('admin.automobile.drive_type.edit', compact('drive_type_data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'drive_type' => 'required'
        ]);
        
        $drive_typeUpdate = AutomobileDriveTypeModel::find($id);
        
        
        $drive_typeUpdate->drive_type = $validate['drive_type'];
        $drive_typeUpdate->status = $request->input('status');
        
        $drive_typeUpdate->save();
        
        return redirect('/admin/drive_type')->with('success_msg','Drive Type Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = AutomobileDriveTypeModel::find($id)->delete();
        return redirect('/admin/drive_type')->with('success_msg','Drive Type deleted successfully!');
    }
}
