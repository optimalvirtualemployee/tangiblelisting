<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PropertyTypeModel;
use App\TopPropertyTypeModel;
use App\CommercialPropertyTypeModel;

class TopPropertyTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $property_type = TopPropertyTypeModel::get();
        
        return view('admin.realestate.top_property_type.index',compact('property_type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.realestate.top_property_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'property_type' => 'required|unique:re_top_property_type',
            'status' => 'required'
        ]);
        
        $property_type = TopPropertyTypeModel::create($validate);
        $property_type->save();
        return redirect('/admin/top_property_type')->with('success_msg','Top Property Type Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $property_type_data = TopPropertyTypeModel::find($id);
        return view('admin.realestate.top_property_type.edit', compact('property_type_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'property_type' => 'required'
        ]);
        
        $property_typeUpdate = TopPropertyTypeModel::find($id);
        
        if ($property_typeUpdate->property_type == $validate['property_type']) {
            $property_typeUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['property_type' => 'unique:re_top_property_type']);
            $property_typeUpdate->property_type = $validate['property_type'];
            $property_typeUpdate->status = $request->input('status');
        }
        
        $property_typeUpdate->save();
        
        return redirect('/admin/top_property_type')->with('success_msg','Top Property Type Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = TopPropertyTypeModel::find($id)->delete();
        return redirect('/admin/top_property_type')->with('success_msg','Top Property Type deleted successfully!');
    }
    
    public function getSubPropertyType(Request $request){
        
        $propertyId = $request->input('propertyId');
        $propertyType = '';

        if($propertyId == '1')
        $propertyType = CommercialPropertyTypeModel::get();
        else
        $propertyType = PropertyTypeModel::get();
        
        return $propertyType;
    }
}
