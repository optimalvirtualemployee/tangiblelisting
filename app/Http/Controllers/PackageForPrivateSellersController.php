<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PackageForPrivateSellersModel;
use App\PackageSettingsModelForPrivateSellers;
use App\PackageListingRangeManagementModel;

class PackageForPrivateSellersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $package_for_private_sellers = PackageForPrivateSellersModel::get();
        return view('admin.admincommon.package_for_private_sellers.index',compact('package_for_private_sellers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admincommon.package_for_private_sellers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'package_for_private_sellers' => 'required|regex:/^[\pL\s_]+$/u|unique:tbl_package_for_private_sellers',
            'from.*' => "required",
            'to.*' => "required",
            'pricing.*' => "required",
            'status' => ['required']
        ]);

        $package_for_private_sellers = PackageForPrivateSellersModel::create($validate);
        $package_for_private_sellers->save();
        
        $package_id = $package_for_private_sellers->id;
        
        $fromRange = $request->input('from');
        $toRange = $request->input('to');
        $pricing = $request->input('pricing');
        
        
        for ($i = 0; $i < sizeof($fromRange); $i++) {
            $createRange = new PackageListingRangeManagementModel();
            if($fromRange[$i] != null){
                $createRange->package_id = $package_id;
                $createRange->rangeFrom = $fromRange[$i];
                $createRange->rangeTo = $toRange[$i];
                $createRange->pricing = $pricing[$i];
                
                $createRange->save();
            }
        }
        
        return redirect('/admin/package_for_private_sellers')->with('success_msg','Package for Private Sellers Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package_for_private_sellers_data = PackageForPrivateSellersModel::find($id);
        
        $pricing = PackageListingRangeManagementModel::select('tbl_package_pricing_range_private_seller.id as id','package_for_private_sellers as package_name', 'rangeFrom', 'rangeTo','pricing')
        ->join('tbl_package_for_private_sellers', 'tbl_package_for_private_sellers.id', '=', 'tbl_package_pricing_range_private_seller.package_id')
        ->where('package_id', '=', $id)
        ->get();
        
        
        $html = '';
        
        for($i = 0; $i < sizeof($pricing); $i++){
            
            if($i == 0){
                $html .= '<div class="dtbl">
                 <div class="column"><input id="from" class="form-control form-control-user from" type="number"  name="from[]" placeholder="Enter From Range" value="'.$pricing[$i]->rangeFrom.'"></div>
                 <div class="column"><input id="to" class="form-control form-control-user to" type="number"  name="to[]" placeholder="Enter To Range" value="'.$pricing[$i]->rangeTo.'"></div>
                 <div class="column"><input id="pricing" class="form-control form-control-user " type="number"  name="pricing[]" placeholder="Enter Pricing" value="'.$pricing[$i]->pricing.'"></div>
                 <div class="column addbutton"><button class="btn btn-primary btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add +</button></div> 
                  </div>';
            }else{
               $html .= '<div class="dtbl"><div class="column"><input id="from" class="form-control form-control-user from" type="number"  name="from[]" placeholder="Enter From Range" value="'.$pricing[$i]->rangeFrom.'"></div>
                 <div class="column"><input id="to" class="form-control form-control-user to" type="number"  name="to[]" placeholder="Enter To Range" value="'.$pricing[$i]->rangeTo.'"></div>
                <div class="column"><input id="pricing" class="form-control form-control-user " type="number"  name="pricing[]" placeholder="Enter Pricing" value="'.$pricing[$i]->pricing.'"></div>
                <div class="column addbutton"><button class="btn btn-primary btn-danger" type="button"><i class="glyphicon glyphicon-plus"></i>Delete</button></div></div>';
            }
        }
        
        return view('admin.admincommon.package_for_private_sellers.edit', compact('package_for_private_sellers_data', 'html'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'package_for_private_sellers' => 'required|regex:/^[\pL\s_]+$/u',
           'from.*' => "required",
            'to.*' => "required",
            'pricing.*' => "required"
        ]);

        
        
        $package_for_private_sellersUpdate = PackageForPrivateSellersModel::find($id);

        $fromRange = $request->input('from');
        $toRange = $request->input('to');
        $pricing = $request->input('pricing');
        
        PackageListingRangeManagementModel::where('package_id','=',$id)->delete();
        
        if ($package_for_private_sellersUpdate->package_for_private_sellers == $validate['package_for_private_sellers']) {
            $package_for_private_sellersUpdate->status = $request->input('status');
            
            for ($i = 0; $i < sizeof($fromRange); $i++) {
                $createRange = new PackageListingRangeManagementModel();
                if($fromRange[$i] != null){
                    $createRange->package_id = $id;
                    $createRange->rangeFrom = $fromRange[$i];
                    $createRange->rangeTo = $toRange[$i];
                    $createRange->pricing = $pricing[$i];
                    
                    $createRange->save();
                }
            }
            
        } else {
            $validate = $this->validate($request, ['package_for_private_sellers' => 'unique:tbl_package_for_private_sellers']);
            $package_for_private_sellersUpdate->package_for_private_sellers = $validate['package_for_private_sellers'];
            $package_for_private_sellersUpdate->status = $request->input('status');
            
            for ($i = 0; $i < sizeof($fromRange); $i++) {
                $createRange = new PackageListingRangeManagementModel();
                if($fromRange[$i] != null){
                    $createRange->package_id = $id;
                    $createRange->rangeFrom = $fromRange[$i];
                    $createRange->rangeTo = $toRange[$i];
                    $createRange->pricing = $pricing[$i];
                    
                    $createRange->save();
                }
            }
        }

        $package_for_private_sellersUpdate->save();

        return redirect('/admin/package_for_private_sellers')->with('success_msg','Package for Private Sellers Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = PackageForPrivateSellersModel::find($id)->delete();
        return redirect('/admin/package_for_private_sellers')->with('success_msg','Package for Private Sellers deleted successfully!');
    }


    //------------------- PACKAGE FOR PRIVATE SELLERS SETTING OPTIONS STARTS HERE ------------------------------//

    /** 
    *  This is a custom method for redirecting on setting page from where Admin can enable package options.
    *  @param  int  $id
    *  @return \Illuminate\Http\Response  
    */
    public function package_setting($id)
    {
        $package_settings = PackageSettingsModelForPrivateSellers::where('package_id',$id)->get()->first();
        return view('admin.admincommon.package_for_private_sellers.package_settings.edit', compact('package_settings'));
    }

    /** 
    *  This is a custom method for setup custom package option, 
    *  In which Admin / Moderator can select or update options for various packages for its dealers.
    *  @param  int  $id
    *  @return \Illuminate\Http\Response  
    */
    public function package_setting_update(Request $request, $id)
    {
        // dd($request->all());
        $validate = $this->validate($request, [
            'no_of_images_per_listing' => 'required|numeric',
        ]);

          $if_package = PackageSettingsModelForPrivateSellers::where('package_id',$id)->first();  

        if($if_package){
            $package_setting = $if_package;
        }else{
            $package_setting = new PackageSettingsModelForPrivateSellers;
        }

        $package_setting->package_id                       = $request->input('package_id');
        $package_setting->no_of_images_per_listing         = $validate['no_of_images_per_listing'];
        $package_setting->normal_search_result             = $request->input('normal_search_result') ? '1' : '0' ;
        $package_setting->weekly_statistics                = $request->input('weekly_statistics'   ) ? '1' : '0';
        $package_setting->basic_analytics                  = $request->input('basic_analytics') ? '1' : '0';
        $package_setting->prioritized_search_result        = $request->input('prioritized_search_result') ? '1' : '0';
        $package_setting->escrow_platform                  = $request->input('escrow_platform') ? '1' : '0';
        $package_setting->top_search_result                = $request->input('top_search_result') ? '1' : '0';
        $package_setting->choice_listing_on_home_page      = $request->input('choice_listing_on_home_page') ? '1' : '0';
        $package_setting->tangible_listing_on_social_media = $request->input('tangible_listing_on_social_media') ? '1' : '0';
        
        $package_setting->save();

        return redirect('/admin/package_for_private_sellers')->with('success_msg','Package Setting Updated successfully!');
    }

//------------------- PACKAGE FOR PRIVATE SELLERS SETTING OPTIONS ENDS HERE ------------------------------//
}
