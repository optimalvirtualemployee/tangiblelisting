<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\TypeModel;
use App\TypeImageModel;


class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type = TypeModel::get();
        
        $images = TypeImageModel::get();
        return view('admin.watch.type.index',compact('type', 'images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.type.create_msf');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'watch_type' => 'required|regex:/^([a-zA-Z_]+)(\s[a-zA-Z_]+)*$/|unique:wa_type',
            'status' => 'required'
        ]);

        $type = TypeModel::create($validate);
        $type->save();
        
        $listing_id = $type->id;
        
        if($request->file('photos') ?? ''){
            foreach ($request->file('photos') as $image){
                $uploadImage = new TypeImageModel();
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                
                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename().'.'.$extension;
                $uploadImage->listing_id = $listing_id;
                $uploadImage->save();
            }
        }
        
        return redirect('/admin/type')->with('success_msg','Type Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type_data = TypeModel::find($id);
        
        $images_data = TypeImageModel::where('listing_id', '=', $id)->get();
        
        return view('admin.watch.type.edit_msf', compact('type_data', 'images_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        //dd($request);
        $validate = $this->validate($request, [
            'watch_type' => 'required|regex:/^([a-zA-Z_]+)(\s[a-zA-Z_]+)*$/'
        ]);

        $typeUpdate = TypeModel::find($id);

        
            /* $validate = $this->validate($request, ['watch_type' => 'unique:wa_type']); */
            $typeUpdate->watch_type = $validate['watch_type'];
            $typeUpdate->status = $request->input('status');
            
            
            if($request->file('photos') ?? ''){
                foreach ($request->file('photos') as $image){
                    $uploadImage = new TypeImageModel();
                    
                    $extension = $image->getClientOriginalExtension();
                    Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                    
                    $uploadImage->images = $image->getClientMimeType();
                    $uploadImage->filename = $image->getFilename().'.'.$extension;
                    $uploadImage->listing_id = $id;
                    $uploadImage->save();
                }
            }

        $typeUpdate->save();

        return redirect('/admin/type')->with('success_msg','Type Updated successfully!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = TypeModel::find($id)->delete();
        return redirect('/admin/type')->with('success_msg','Type deleted successfully!');
    }
}
