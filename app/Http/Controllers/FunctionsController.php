<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FunctionsModel;

class FunctionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $functions = FunctionsModel::get();
        return view('admin.watch.functions.index',compact('functions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.watch.functions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
         $validator = $this->validate($request, [
            "chronograph" => 'required|unique:wa_factory_features_functions',
            "tourbillion"  => "required",
            'GMT' => "required",
            'annual_calendar' => "required",
            'minute_repeater' => "required",
            'double_chronograph' => "required",
            'panorma_date' => "required",
            'jumping_hour' => "required",
            'alarm' => "required",
            'year' => "required",
            'day' => "required"
        ]);

          $case = FunctionsModel::create($validator);
          $case->save();
       
      return redirect('/admin/functions')->with('success_msg','Function Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $functions_data = FunctionsModel::find($id);
        return view('admin.watch.functions.edit', compact('functions_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $deleteRecords = FunctionsModel::find($id)->delete();
        if($deleteRecords){
            $validator = $this->validate($request, [
            "chronograph" => 'required|unique:wa_factory_features_functions',
            "tourbillion"  => "required",
            'GMT' => "required",
            'annual_calendar' => "required",
            'minute_repeater' => "required",
            'double_chronograph' => "required",
            'panorma_date' => "required",
            'jumping_hour' => "required",
            'alarm' => "required",
            'year' => "required",
            'day' => "required"
        ]);

          $case = FunctionsModel::create($validator);
          $case->save();
        }
        return redirect('/admin/functions')->with('success_msg','Function Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = FunctionsModel::find($id)->delete();
        return redirect('/admin/functions')->with('success_msg','Function Deleted successfully!');
    }
}
