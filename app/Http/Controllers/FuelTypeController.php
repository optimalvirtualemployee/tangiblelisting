<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FuelTypeModel;

class FuelTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fuel_type = FuelTypeModel::get();
        return view('admin.automobile.fuel_type.index',compact('fuel_type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.automobile.fuel_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'fuel_type' => 'required|regex:/^([a-zA-Z_]+)(\s[a-zA-Z_]+)*$/|unique:au_fuel_type',
            'status' => 'required'
        ]);

        $fuel_type = FuelTypeModel::create($validate);
        $fuel_type->save();
        return redirect('/admin/fuel_type')->with('success_msg','Fuel Type Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fuel_type_data = FuelTypeModel::find($id);
        return view('admin.automobile.fuel_type.edit', compact('fuel_type_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'fuel_type' => 'required|regex:/^([a-zA-Z_]+)(\s[a-zA-Z_]+)*$/'
        ]);

        $fuel_typeUpdate = FuelTypeModel::find($id);

        if ($fuel_typeUpdate->fuel_type == $validate['fuel_type']) {
            $fuel_typeUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['fuel_type' => 'unique:au_fuel_type']);
            $fuel_typeUpdate->fuel_type = $validate['fuel_type'];
            $fuel_typeUpdate->status = $request->input('status');
        }

        $fuel_typeUpdate->save();

        return redirect('/admin/fuel_type')->with('success_msg','Fuel Type Updated successfully!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = FuelTypeModel::find($id)->delete();
        return redirect('/admin/fuel_type')->with('success_msg','Fuel Type deleted successfully!');
    }
}
