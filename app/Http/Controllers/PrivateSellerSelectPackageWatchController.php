<?php

namespace App\Http\Controllers;

use App\ContactusModel;
use App\CurrencyModel;
use App\PackageListingRangeWatchManagementModel;
use App\PropertyTypeModel;
use Illuminate\Http\Request;

class PrivateSellerSelectPackageWatchController extends Controller
{
    public function index(){
        
        $contactUs = ContactusModel::get()->first();
        $propertyType = PropertyTypeModel::get();
        
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        currency()->setUserCurrency($selectedCurrency->code);
        
        $pricing_data = PackageListingRangeWatchManagementModel::Orderby('rangeFrom')->distinct()->get(['rangeFrom', 'rangeTo']);
        
        $value_data = PackageListingRangeWatchManagementModel::select('tbl_package_pricing_range_private_seller_watch.id as id','package_id','package_for_private_sellers as package', 'rangeFrom', 'rangeTo', 'pricing')
        ->join('tbl_package_for_private_sellers', 'tbl_package_for_private_sellers.id', '=', 'tbl_package_pricing_range_private_seller_watch.package_id')
        ->get();
        
        //dd($value_data);
        
        return view('privateSeller.select-package-watches' ,compact('pricing_data', 'value_data', 'contactUs', 'propertyType'));
    }
}
