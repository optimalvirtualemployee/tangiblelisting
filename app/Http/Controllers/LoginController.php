<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class LoginController extends Controller
{
    public function home() {
        return view('auth.login');
    }
    function checklogin(Request $request) {

			$this->validate($request, [
				'email'   => 'required|email',
				'password'  => 'required|alphaNum|min:3'
			]);
            $uid =$request->get('uid');
			$user_data = array(
				'email'  => $request->get('email'),
				'password' => $request->get('password'),
                // 'usertype' => $request->get('uid')
			);
			
			$remember_me = $request->has('remember') ? true : false;
			
		if(Auth::attempt($user_data, $remember_me)){
            if($uid == 1){
            
            if(Auth::user()->usertype==1){
                return redirect('/admin/home');
            }
            elseif(Auth::user()->usertype==3){
                return redirect('/BigcityOperations/dashboard');
            }
            elseif(Auth::user()->usertype==4){
                return redirect('/BigcityAccounts/dashboard');
            }
            elseif(Auth::user()->usertype==5) {
                return redirect('/DataAdmin/dashboard');
            }
            elseif(Auth::user()->usertype==6) {
                return redirect('/admin/dashboard');
            }
            else{
                // return redirect('/logout')->back()->withErrors(['error' => ['Wrong Login Details.']]);
                return redirect('/')->withErrors(['error' => ['Wrong Login Details.']]);
            }
        }
        elseif($uid == 2){
            //for outlet super admin
        if(Auth::user()->usertype==2 && Auth::user()->role == 1){
           return redirect('/vendor/dashboard');
        }
         //for outlet admin
        elseif (Auth::user()->usertype==2 && Auth::user()->role == 2) {
               return redirect('/vendor/dashboard');
            }
            //For Vendor Branch Role
            elseif (Auth::user()->usertype==2 && Auth::user()->role == 3) {
               return redirect('/VendorBranch/dashboard');
            
            //For Vendor Accounts Role
            }elseif (Auth::user()->usertype==2 && Auth::user()->role == 4) {
               return redirect('/VendorAccounts/dashboard');

            }
        }
        else{
            return back()->withErrors(['error' => ['Wrong Login Details.']]);
        }
        // $uid = Auth::user()->id;
        // $otp = DB::table('users')
        //             ->select("*")
        //             ->where("id",$uid)
        //             ->get();
        //  $phone = $otp[0]->phone; 
         
        //  $otp = new otp();
        //  //$pin = rand(100000,999999);
        //  $pin = 123456;
        //  $otp->phone  = $phone;
        //  $otp->otp        = $pin;
        //  $otp->save();
        // return view('auth.otp');
				// return redirect('main/successlogin');
		} else {
            return back()->withErrors(['error' => ['Wrong Login Details.']]);
		}

    }

    function successlogin()  {
     	return view('dashbord');
    }

    function logout()  {
	    Auth::logout();
	    return redirect('main');
    }
}
