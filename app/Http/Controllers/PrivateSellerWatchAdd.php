<?php

namespace App\Http\Controllers;

use App\AgentModel;
use App\BraceletClaspMaterialModel;
use App\BraceletClaspTypeModel;
use App\BraceletColorModel;
use App\BraceletMaterialModel;
use App\CaseDiameterModel;
use App\CaseGlassTypeModel;
use App\CaseMMModel;
use App\CaseMaterialModel;
use App\CaseWaterRDModel;
use App\CityModel;
use App\ContactusModel;
use App\CountryModel;
use App\CurrencyModel;
use App\DialColorModel;
use App\DialPowerReserveModel;
use App\FunctionAlarmModel;
use App\FunctionAnnualCalenderModel;
use App\FunctionChronographModel;
use App\FunctionDayModel;
use App\FunctionDoubleChronographModel;
use App\FunctionGMTModel;
use App\FunctionJumpingHourModel;
use App\FunctionMinuteRepeaterModel;
use App\FunctionPanormaDateModel;
use App\FunctionTourbillionModel;
use App\FunctionYearModel;
use App\GenderModel;
use App\MovementModel;
use App\PriceModel;
use App\PropertyTypeModel;
use App\StateModel;
use App\Timezone;
use App\TypeModel;
use App\UserDetailsModel;
use App\WatchBezelModel;
use App\WatchFeaturesListingModel;
use App\WatchModel;
use App\Watchbrandmodel;
use App\YearOfManufactureModel;
use Illuminate\Http\Request;
use App\PackageForPrivateSellersWatchModel;
use App\PackageListingRangeWatchManagementModel;
use App\InclusionModel;

class PrivateSellerWatchAdd extends Controller
{
    public function index(){
    
        $contactUs = ContactusModel::get()->first();
        $propertyType = PropertyTypeModel::get();
        
        $models = Watchbrandmodel::orderBy('watch_model_name','ASC')->get();
        $brands = WatchModel::orderBy('watch_brand_name','ASC')->get();
        $country_data = CountryModel::get();
        $case_diameters = CaseDiameterModel::orderBy('case_diameter','ASC')->get();
        $movements = MovementModel::get();
        $types = TypeModel::get();
        $year_of_manufactures = YearOfManufactureModel::orderBy('year_of_manufacture','ASC')->get();
        $genders = GenderModel::get();
        $currency_data = CurrencyModel::get();
        $prices = PriceModel::get();
        $inclusions_data = InclusionModel::get();
        
        $power_reserve = DialPowerReserveModel::get();
        $bezel_material = WatchBezelModel::get();
        $dial_color = DialColorModel::get();
        $bracelet_material = BraceletMaterialModel::get();
        $bracelet_color = BraceletColorModel::get();
        $type_of_clasp = BraceletClaspTypeModel::get();
        $clasp_material = BraceletClaspMaterialModel::get();
        $case_material = CaseMaterialModel::get();
        $case_mm = CaseMMModel::get();
        $wrd = CaseWaterRDModel::get();
        $glass_type = CaseGlassTypeModel::get();
        $chronograph = FunctionChronographModel::get();
        $tourbillion = FunctionTourbillionModel::get();
        $gmt = FunctionGMTModel::get();
        $annual_calender = FunctionAnnualCalenderModel::get();
        $minute_repeater = FunctionMinuteRepeaterModel::get();
        $double_chronograph = FunctionDoubleChronographModel::get();
        $panorma_date = FunctionPanormaDateModel::get();
        $jumping_hour = FunctionJumpingHourModel::get();
        $alarm = FunctionAlarmModel::get();
        $year = FunctionYearModel::get();
        $day = FunctionDayModel::get();
        $agents = AgentModel::get();
        $timezone = Timezone::Orderby('offset')->get();
        $userDetails = UserDetailsModel::get();
        
        $feature_listing = WatchFeaturesListingModel::get();
        
        $html ='';
        foreach ($feature_listing as $values){
            $id = $values->id;
            $propertyName = $values->property_name;
            
            $html .= '<div class="form-group">
                    <tr>
                   <td><label style= margin-right:10px!important; ><Strong>'.$propertyName.':</Strong></label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$propertyName.'" name="'.$id.'" value="1"><label style= margin-right:5px!important; for="yes"> Yes </label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$propertyName.'" name="'.$id.'" value="0" checked><label style= margin-right:5px!important; for="no"> No</label></td>
                    </tr>
                      </div>';
            
        }
        
        return view('privateSeller.create-watch', compact('inclusions_data','contactUs', 'propertyType','models','brands','country_data','case_diameters','movements','types', 'year_of_manufactures'
                ,'genders', 'power_reserve', 'bezel_material', 'dial_color', 'bracelet_material', 'bracelet_color', 'type_of_clasp', 'clasp_material',
                'case_material','case_mm', 'wrd', 'glass_type', 'chronograph', 'tourbillion', 'gmt', 'annual_calender', 'minute_repeater', 'double_chronograph',
            'panorma_date','jumping_hour', 'alarm', 'year', 'day', 'agents', 'timezone', 'html', 'currency_data', 'prices', 'userDetails'));
    
    }
    
    //this function is used to get the models based on selected brand
    public function getWatchModel(Request $request){
        
        $brandId = $request->input('brandId');
        $model = WatchBrandModel::select('id', 'watch_model_name')->where('watch_brand_id', $brandId)->get();
        return $model;
    }
    
    public function price($package, $pricerange, $packageprice){
        
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        currency()->setUserCurrency($selectedCurrency->code);
        
        $priceRangeArray = explode("-",$pricerange);
        
        $packageId = PackageForPrivateSellersWatchModel::select('id')->where('package_for_private_sellers','=' , $package)->get()->first();
        
        $packageList = PackageForPrivateSellersWatchModel::get();
        
        $upgradepackage = PackageListingRangeWatchManagementModel::where('rangeFrom', '=', $priceRangeArray[0])->
        where('rangeTo', '=', $priceRangeArray[1])->where('package_id', '!=', $packageId->id)->get();
        
        $contactUs = ContactusModel::get()->first();
        $propertyType = PropertyTypeModel::get();
        
        $models = Watchbrandmodel::orderBy('watch_model_name','ASC')->get();
        $brands = WatchModel::orderBy('watch_brand_name','ASC')->get();
        $country_data = CountryModel::get();
        $case_diameters = CaseDiameterModel::orderBy('case_diameter','ASC')->get();
        $movements = MovementModel::get();
        $types = TypeModel::get();
        $year_of_manufactures = YearOfManufactureModel::orderBy('year_of_manufacture','ASC')->get();
        $genders = GenderModel::get();
        $currency_data = CurrencyModel::get();
        $prices = PriceModel::get();
        $inclusions_data = InclusionModel::get();
        
        $power_reserve = DialPowerReserveModel::get();
        $bezel_material = WatchBezelModel::get();
        $dial_color = DialColorModel::get();
        $bracelet_material = BraceletMaterialModel::get();
        $bracelet_color = BraceletColorModel::get();
        $type_of_clasp = BraceletClaspTypeModel::get();
        $clasp_material = BraceletClaspMaterialModel::get();
        $case_material = CaseMaterialModel::get();
        $case_mm = CaseMMModel::get();
        $wrd = CaseWaterRDModel::get();
        $glass_type = CaseGlassTypeModel::get();
        $chronograph = FunctionChronographModel::get();
        $tourbillion = FunctionTourbillionModel::get();
        $gmt = FunctionGMTModel::get();
        $annual_calender = FunctionAnnualCalenderModel::get();
        $minute_repeater = FunctionMinuteRepeaterModel::get();
        $double_chronograph = FunctionDoubleChronographModel::get();
        $panorma_date = FunctionPanormaDateModel::get();
        $jumping_hour = FunctionJumpingHourModel::get();
        $alarm = FunctionAlarmModel::get();
        $year = FunctionYearModel::get();
        $day = FunctionDayModel::get();
        $agents = AgentModel::get();
        $timezone = Timezone::Orderby('offset')->get();
        $userDetails = UserDetailsModel::get();
        $country_data = CountryModel::get();
        $state_data = StateModel::get();
        $city_data = CityModel::get();
        
        $feature_listing = WatchFeaturesListingModel::get();
        
        $i =0;
        $html1 = '';
        $html2 = '';
        foreach ($feature_listing as $values){
            $id = $values->id;
            $propertyName = $values->property_name;

            if($i % 2 == 0){
            
            
            
            $html1 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="'.$propertyName.'"></label>
                    <input   type="checkbox" class="form-check-input" id="'.$propertyName.'" name="'.$id.'" value="1">'.$propertyName.'
                      </div>';
            }else {
                
                $html2 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="'.$propertyName.'"></label>
                    <input   type="checkbox" class="form-check-input" id="'.$propertyName.'" name="'.$id.'" value="1">'.$propertyName.'
                      </div>';
                
                
            }
            
            $i++;
        }
        
        return view('privateSeller.create-watch', compact('inclusions_data','contactUs', 'propertyType','models','brands','country_data','case_diameters','movements','types', 'year_of_manufactures'
            ,'genders', 'power_reserve', 'bezel_material', 'dial_color', 'bracelet_material', 'bracelet_color', 'type_of_clasp', 'clasp_material',
            'case_material','case_mm', 'wrd', 'glass_type', 'chronograph', 'tourbillion', 'gmt', 'annual_calender', 'minute_repeater', 'double_chronograph',
            'panorma_date','jumping_hour', 'alarm', 'year', 'day', 'agents', 'timezone', 'html1', 'html2', 'currency_data', 'prices'
            , 'package', 'packageprice', 'packageList', 'upgradepackage', 'userDetails', 'country_data', 'state_data', 'city_data'));
        
    }
}
