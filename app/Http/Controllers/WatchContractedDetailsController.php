<?php

namespace App\Http\Controllers;

use App\WatchContractedDetailsModel;
use Illuminate\Http\Request;

class WatchContractedDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contractedWatch = WatchContractedDetailsModel::select('ad_title', 'tbl_agent.first_name as agent_first_name',
            'tbl_agent.last_name as agent_last_name' , 'tbl_customer.first_name as customer_first_name', 'tbl_customer.last_name as customer_last_name')
            ->join('tbl_agent', 'tbl_agent.id', '=', 'wa_customer_product-details.dealer_id')
            ->join('tbl_customer', 'tbl_customer.id', '=', 'wa_customer_product-details.customer_id')
            ->join('wa_watch_detail', 'wa_watch_detail.id', '=', 'wa_customer_product-details.product_id')
            ->get();
            
            
           // dd($contractedWatch);
            
            return view('admin.admincommon.contractedwatch.index', compact('contractedWatch'));
    }
   
}
