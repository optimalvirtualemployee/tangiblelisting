<?php

namespace App\Http\Controllers;

use App\FunctionAnnualCalenderModel;
use Illuminate\Http\Request;

class FunctionAnnualCalenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $functions = FunctionAnnualCalenderModel::get();
        return view('admin.watch.functionAnnualCalender.index',compact('functions'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.functionAnnualCalender.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = $this->validate($request, [
            "annual_calendar" => 'required|unique:wa_factory_features_function_annualCalender,annual_calender',
            "status" => 'required'
        ]);
        
        $case = new FunctionAnnualCalenderModel;
        $case->annual_calender = $request->input('annual_calendar');
        $case->status = $request->input('status');
        $case->save();
        
        return redirect('/admin/functionAnnualCalender')->with('success_msg','Annual Calender Created successfully!');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $functions_data = FunctionAnnualCalenderModel::find($id);
        return view('admin.watch.functionAnnualCalender.edit', compact('functions_data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            "annual_calendar" => 'required|unique:wa_factory_features_function_annualCalender,annual_calender, '.$id,
            "status" => 'required'
            
        ]);
        
            $case = FunctionAnnualCalenderModel::find($id);
            $case->annual_calender = $request->input('annual_calendar');
            $case->status = $request->input('status');
            $case->save();
        
        return redirect('/admin/functionAnnualCalender')->with('success_msg','Annual Calender Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = FunctionAnnualCalenderModel::find($id)->delete();
        return redirect('/admin/functionAnnualCalender')->with('success_msg','Annual Calender Deleted successfully!');
    }
}
