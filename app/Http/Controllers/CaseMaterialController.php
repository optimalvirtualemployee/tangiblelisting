<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CaseMaterialModel;

class CaseMaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $case = CaseMaterialModel::get();
        return view('admin.watch.caseMaterial.index',compact('case'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.caseMaterial.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validate($request, [
            "case_material" => 'required|unique:wa_factory_features_case_material',
            "status" => 'required'
            
        ]);

          $case = CaseMaterialModel::create($validator);
          $case->save();
       
      return redirect('/admin/caseMaterial')->with('success_msg','Case Material Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $case_data = CaseMaterialModel::find($id);
        return view('admin.watch.caseMaterial.edit', compact('case_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            "case_material" => 'required',
            "status" => 'required'
            
        ]);

        $case = CaseMaterialModel::find($id);

        
        $case->case_material = $validator['case_material'];
        $case->status = $validator['status'];
            
        $case->save();
        return redirect('/admin/caseMaterial')->with('success_msg','Case Material Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = CaseMaterialModel::find($id)->delete();
        return redirect('/admin/caseMaterial')->with('success_msg','Case Material Deleted successfully!');
    }
}
