<?php

namespace App\Http\Controllers;

use App\AutomobileImageModel;
use App\AutomobileSubmitSuggestPriceModel;
use App\ContactusModel;
use App\CurrencyModel;
use App\EnquiryModel;
use App\MyMessageModel;
use App\PropertyImageModel;
use App\PropertyTypeModel;
use App\SubmitSuggestPriceModel;
use App\WatchImageModel;
use App\WatchSubmitSuggestPriceModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BuyController extends Controller
{
    public function index()
    {
        
        if (Auth::user() == null){
        return redirect('/');
        }
        
       $contactUs = ContactusModel::get()->first();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        $propertyType = PropertyTypeModel::get();
        
        $id = Auth::user()->id;
        
        $enquiry_automobile = EnquiryModel::select('au_automobile_detail.id as listing_id','tbl_enquiry.agent_id','tbl_enquiry.id as id','productId','tbl_enquiry.created_at', 'ad_title', 'value', 'tbl_country.country_name as country_name', 'tbl_country.filename as countryflag',
            'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
            ->where(function ($query) use ($id) {
                
                $query->where('tbl_enquiry.userId', '=', $id);
            })->where('category', '=', 'Automobile')
            ->join('au_automobile_detail','au_automobile_detail.id', 'tbl_enquiry.productId')
            ->join('tbl_country','tbl_country.id', 'au_automobile_detail.country_id')
            ->join('tbl_agent','tbl_agent.id', 'tbl_enquiry.agent_id')
            ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
            ->get();
            
            //dd(DB::getQueryLog());
            $enquiry_watch = EnquiryModel::select('wa_watch_detail.id as listing_id','tbl_enquiry.agent_id','tbl_enquiry.id as id','productId','tbl_enquiry.created_at', 'ad_title', 'watch_price as value', 'tbl_country.country_name as country_name', 'tbl_country.filename as countryflag',
                'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
                ->where(function ($query) use ($id) {
                    
                    $query->where('tbl_enquiry.userId', '=', $id);
                })->where('category', '=', 'watch')
                ->join('wa_watch_detail','wa_watch_detail.id', 'tbl_enquiry.productId')
                ->join('tbl_country','tbl_country.id', 'wa_watch_detail.country_id')
                ->join('tbl_agent','tbl_agent.id', 'tbl_enquiry.agent_id')
                ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
                ->get();
                
                $enquiry_property = EnquiryModel::select('re_property_details.id as listing_id','tbl_enquiry.agent_id','tbl_enquiry.id as id','productId','tbl_enquiry.created_at', 'ad_title', 'property_price as value', 'tbl_country.country_name as country_name', 'tbl_country.filename as countryflag',
                    'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
                    ->where(function ($query) use ($id) {
                        
                        $query->where('tbl_enquiry.userId', '=', $id);
                    })->where('category', '=', 'RealEstate')
                    ->join('re_property_details','re_property_details.id', 'tbl_enquiry.productId')
                    ->join('tbl_country','tbl_country.id', 're_property_details.country_id')
                    ->join('tbl_agent','tbl_agent.id', 'tbl_enquiry.agent_id')
                    ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
                    ->get();
                    
                    $automobile_productids = array();
                    
                    foreach ($enquiry_automobile as $auto){
                        
                        $automobile_productids[] = $auto->productId;
                    }
                    
                    /* $automobile_submitprice = SubmitSuggestPriceModel::select('au_automobile_detail.id as listing_id','automobile_submitpricerequest.agent_id','automobile_submitpricerequest.id as id','automobile_submitpricerequest.product_id as productId','automobile_submitpricerequest.created_at', 'ad_title', 'value', 'tbl_country.country_name as country_name','tbl_country.filename as countryflag',
                        'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
                        ->where('user_id', '=', $id)->whereNotIn('product_id', $automobile_productids)
                        ->join('au_automobile_detail','au_automobile_detail.id', 'automobile_submitpricerequest.product_id')
                        ->join('tbl_country','tbl_country.id', 'au_automobile_detail.country_id')
                        ->join('tbl_agent','tbl_agent.id', 'automobile_submitpricerequest.agent_id')
                        ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
                        ->get();
                    
                        $watch_productids = array();
                        $watch_agentsids = array();
                        
                        foreach ($enquiry_watch as $watch){
                            
                            $watch_productids[] = $watch->productId;
                        } */
                        
                        /* $watch_submitprice = WatchSubmitSuggestPriceModel::select('wa_watch_detail.id as listing_id','watch_submitpricerequest.agent_id','watch_submitpricerequest.id as id','watch_submitpricerequest.product_id as productId','watch_submitpricerequest.created_at', 'ad_title', 'wa_watch_detail.watch_price as value', 'tbl_country.country_name as country_name','tbl_country.filename as countryflag',
                            'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
                            ->where('user_id', '=', $id)->whereNotIn('product_id', $watch_productids)
                            ->join('wa_watch_detail','wa_watch_detail.id', 'watch_submitpricerequest.product_id')
                            ->join('tbl_country','tbl_country.id', 'wa_watch_detail.country_id')
                            ->join('tbl_agent','tbl_agent.id', 'watch_submitpricerequest.agent_id')
                            ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
                            ->get(); */
                            
                            /* $watch_submitprice = WatchSubmitSuggestPriceModel::where('user_id', '=', $id)->whereNotIn('product_id', $automobile_productids)
                             ->join('wa_watch_detail','wa_watch_detail.id', 'watch_submitpricerequest.product_id')
                             ->get(); */
                        
                            
                            $message = MyMessageModel::get();
                            
                            
                            $automobile_images = AutomobileImageModel::get();
                            $watch_images = WatchImageModel::get();
                            $property_images = PropertyImageModel::get();
        
                            return view('tangiblehtml.buy', compact('contactUs', 'propertyType', 'message','enquiry_automobile','enquiry_watch', 'enquiry_property','automobile_images', 'watch_images', 'property_images'));
    }
}
