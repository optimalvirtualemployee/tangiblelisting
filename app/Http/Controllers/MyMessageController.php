<?php

namespace App\Http\Controllers;

use App\ContactusModel;
use App\CurrencyModel;
use App\PropertyTypeModel;
use App\WatchImageModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\AgentModel;
use App\MyMessageModel;
use App\AutomobileImageModel;
use App\PropertyImageModel;
use App\AutomobileSubmitSuggestPriceModel;
use App\EnquiryModel;
use App\WatchSubmitSuggestPriceModel;
use Torann\Currency\Currency;

class MyMessageController extends Controller
{
    public function index()
    { 
        $data = array();

        $data['contactUs'] = ContactusModel::get()->first();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        $propertyType = PropertyTypeModel::get();
        
        if (Auth::user() == null){
            return redirect('/');
        }
        
        $id = Auth::user()->id;
        $auto = 'Automobiles';
        
        //dd($id);
        
                    DB::enableQueryLog();

                   
        
                    $data['enquiry_automobile'] = EnquiryModel::select('tbl_enquiry.agent_id','tbl_enquiry.id as id','productId','tbl_enquiry.created_at', 'ad_title', 'value', 'tbl_country.filename as countryflag',
                    'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
                    ->where(function ($query) use ($id) {

                    $query->where('tbl_enquiry.userId', '=', $id)
                    ->orWhere('tbl_enquiry.agent_userid', '=', $id);
                    })->where('category', '=', 'Automobile')
                    ->join('au_automobile_detail','au_automobile_detail.id', 'tbl_enquiry.productId')
                    ->join('tbl_country','tbl_country.id', 'au_automobile_detail.country_id')
                    ->join('tbl_agent','tbl_agent.id', 'tbl_enquiry.agent_id')
                    ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
                    ->get();

                    //dd(DB::getQueryLog());


                    /*
                    $enquiry_watch = EnquiryModel::select('tbl_enquiry.agent_id','tbl_enquiry.id as id','productId','tbl_enquiry.created_at', 'ad_title', 'watch_price as value', 'tbl_country.filename as countryflag',
                    'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
                    ->where(function ($query) use ($id) {
                    $query->where('tbl_enquiry.userId', '=', $id)
                    ->orWhere('tbl_enquiry.agent_userid', '=', $id);
                    })->where('category', '=', 'watch')
                    ->join('wa_watch_detail','wa_watch_detail.id', 'tbl_enquiry.productId')
                    ->join('tbl_country','tbl_country.id', 'wa_watch_detail.country_id')
                    ->join('tbl_agent','tbl_agent.id', 'tbl_enquiry.agent_id')
                    ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
                    ->get();
                    */


                    // Watch Enquiry Code Modified

                    $data['enquiry_watch'] = EnquiryModel::where('category', '=', 'watch')

                    ->where(function ($query) use ($id) {
                    $query->where('userId', '=', $id)
                    ->orWhere('agent_userid', '=', $id);
                    })

                    ->with(['watchDetail' => function($query){
                        $query->with('country');
                    }])

                    ->with(['createBy' => function($query){
                     //   $query->with('country');
                    }])

                    // ->with(['agentTable' => function($query){
                    //     $query->with('agency');
                    // }])

                    ->get();

                    // Watch Enquiry Code Modified

                  //  dd($data['enquiry_watch']);

                    $data['enquiry_property'] = EnquiryModel::select('tbl_enquiry.agent_id','tbl_enquiry.id as id','productId','tbl_enquiry.created_at', 'ad_title', 'property_price as value', 'tbl_country.filename as countryflag',
                    'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
                    ->where(function ($query) use ($id) {

                    $query->where('tbl_enquiry.userId', '=', $id)
                    ->orWhere('tbl_enquiry.agent_userid', '=', $id);
                    })->where('category', '=', 'RealEstate')
                    ->join('re_property_details','re_property_details.id', 'tbl_enquiry.productId')
                    ->join('tbl_country','tbl_country.id', 're_property_details.country_id')
                    ->join('tbl_agent','tbl_agent.id', 'tbl_enquiry.agent_id')
                    ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
                    ->get();

                  
                    /* $automobile_productids = array();
                    
                    foreach ($enquiry_automobile as $auto){
                    
                    $automobile_productids[] = $auto->productId;
                    }
                    
                    $automobile_submitprice = AutomobileSubmitSuggestPriceModel::select('automobile_submitpricerequest.agent_id','automobile_submitpricerequest.id as id','automobile_submitpricerequest.product_id as productId','automobile_submitpricerequest.created_at', 'ad_title', 'value', 'tbl_country.filename as countryflag',
                    'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
                    ->where('user_id', '=', $id)->whereNotIn('product_id', $automobile_productids)
                    ->join('au_automobile_detail','au_automobile_detail.id', 'automobile_submitpricerequest.product_id')
                    ->join('tbl_country','tbl_country.id', 'au_automobile_detail.country_id')
                    ->join('tbl_agent','tbl_agent.id', 'automobile_submitpricerequest.agent_id')
                    ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
                    ->get();
                    
                    $watch_productids = array();
                    $watch_agentsids = array();
                    
                    foreach ($enquiry_watch as $watch){
                    
                    $watch_productids[] = $watch->productId;
                    }
                    
                    $watch_submitprice = WatchSubmitSuggestPriceModel::select('watch_submitpricerequest.agent_id','watch_submitpricerequest.id as id','watch_submitpricerequest.product_id as productId','watch_submitpricerequest.created_at', 'ad_title', 'wa_watch_detail.watch_price as value', 'tbl_country.filename as countryflag',
                    'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
                    ->where('user_id', '=', $id)->whereNotIn('product_id', $watch_productids)
                    ->join('wa_watch_detail','wa_watch_detail.id', 'watch_submitpricerequest.product_id')
                    ->join('tbl_country','tbl_country.id', 'wa_watch_detail.country_id')
                    ->join('tbl_agent','tbl_agent.id', 'watch_submitpricerequest.agent_id')
                    ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
                    ->get(); */
                    
                    /* $watch_submitprice = WatchSubmitSuggestPriceModel::where('user_id', '=', $id)->whereNotIn('product_id', $automobile_productids)
                     ->join('wa_watch_detail','wa_watch_detail.id', 'watch_submitpricerequest.product_id')
                     ->get(); */
                    
                    $data['message'] = MyMessageModel::get();
                    $data['automobile_images'] = AutomobileImageModel::get();
                    $data['watch_images'] = WatchImageModel::get();
                    $data['property_images'] = PropertyImageModel::get();
                    
                    
                    //$message_watch = MyMessageModel::where('userId',$id)->where('listing_category', 'watch')->get();
                    
                    //$message_watch = MyMessageModel::where('userId',$id)->where('listing_category', 'automobile')->get();
                    
                    //dd($enquiry_automobile);
                    
                    // return view('tangiblehtml.mymessages', compact('contactUs', 'propertyType', 'message','enquiry_automobile','enquiry_watch', 'enquiry_property','automobile_images', 'watch_images',  'property_images'));

                    return view('tangiblehtml.mymessages')->with($data);;

                    
    }
    
    public function getMessage(Request $request){
        
        $id = Auth::user()->id;
        $userId = Auth::user()->id;
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        currency()->setUserCurrency($selectedCurrency->code);
        
        $message = array();
        if($request->input('category') == 'Automobile' ){
            $message = MyMessageModel::select('tbl_enquiry.agent_userid as agentUserId','submitpricerequest.submit_price','submitpricerequest.id as submitpriceId','tbl_message_details.category','tbl_message_details.agent_id','tbl_enquiry.id as enquiryId','tbl_message_details.messageTo','tbl_message_details.messageFrom','tbl_enquiry.userId as userId', 'tbl_message_details.created_at as message_time','tbl_agent.userId as agentUserId','au_automobile_detail.id as productId','tbl_country.filename as countryflag','tbl_message_details.queryType as messageQueryType','tbl_message_details.message as message' ,'ad_title', 'value','tbl_message_details.category as messagecategory','tbl_message_details.id as messageId','tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
            ->where('message_id', '=', $request->input('enquiryId'))
            ->where(function ($query) {
                
                $query->where('tbl_message_details.queryType', '=', 'enquiry')
                ->orWhere('tbl_message_details.queryType', '=', 'submitprice');
            })
            ->where('tbl_message_details.category', '=', $request->input('category'))
            ->join('au_automobile_detail','au_automobile_detail.id', 'tbl_message_details.product_id')
            ->join('submitpricerequest','submitpricerequest.id', 'tbl_message_details.message_id')
            ->Join('tbl_enquiry','tbl_enquiry.id', 'tbl_message_details.message_id')
            ->join('tbl_agent','tbl_agent.id', 'tbl_message_details.agent_id')
            ->join('tbl_country','tbl_country.id', 'au_automobile_detail.country_id')
            ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
            ->get()
            ->sortBy('tbl_message_details.created_at');
            
            
            $images = AutomobileImageModel::get();
        }
        
        DB::enableQueryLog();
        
        if($request->input('category') == 'Watch' ){
            
            
            $message = MyMessageModel::select('tbl_enquiry.agent_userid
             as agentUserId','submitpricerequest.submit_price','submitpricerequest.id as submitpriceId','tbl_message_details.category','tbl_message_details.agent_id','tbl_enquiry.id as enquiryId','tbl_message_details.messageTo','tbl_message_details.messageFrom','tbl_enquiry.userId as userId', 'tbl_message_details.created_at as message_time','wa_watch_detail.id as productId','tbl_message_details.queryType as messageQueryType','tbl_message_details.message as message' ,'wa_watch_detail.brand_name as ad_title', 'wa_watch_detail.watch_price as value','tbl_message_details.category as messagecategory', 'tbl_message_details.id as messageId','tbl_final_offer.finalOfferValue as counterOffer', 'users.created_at as agencycreate', 'tbl_final_offer.shippingCost as shippingCost','tbl_enquiry.listingCreatedBy as listingCreatedBy', 'tbl_final_offer.messageFrom as messageFinalFrom')
                ->where('message_id', '=', $request->input('enquiryId'))
                ->where(function ($query) {
                    
                    $query->where('tbl_message_details.queryType', '=', 'enquiry')
                    ->orWhere('tbl_message_details.queryType', '=', 'counterOffer')
                    ->orWhere('tbl_message_details.queryType', '=', 'submitprice');
                })
                ->where('tbl_message_details.category', '=', $request->input('category'))
                ->join('wa_watch_detail','wa_watch_detail.id', 'tbl_message_details.product_id')
                ->join('submitpricerequest','submitpricerequest.id', 'tbl_message_details.message_id')
                ->leftJoin( 'tbl_final_offer', 'tbl_final_offer.messageId','tbl_message_details.id')
                ->join('tbl_enquiry','tbl_enquiry.id', 'tbl_message_details.message_id')
                ->join('users','users.id', 'tbl_enquiry.listingCreatedBy')
                //->join('tbl_agent','tbl_agent.id', 'tbl_message_details.agent_id')
                //->join('tbl_country','tbl_country.id', 'wa_watch_detail.country_id')
                //->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
                ->orderBy('tbl_message_details.created_at','ASC')
                ->get();
                
                // dd(sizeof($message));
                $images = WatchImageModel::get();
        }
        
        if($request->input('category') == 'RealEstate'){
            $message = MyMessageModel::select('tbl_enquiry.agent_userid as agentUserId','tbl_message_details.category','tbl_message_details.agent_id','tbl_enquiry.id as enquiryId','tbl_message_details.messageTo','tbl_message_details.messageFrom','tbl_enquiry.userId as userId', 'tbl_message_details.created_at as message_time','tbl_agent.userId as agentUserId','re_property_details.id as productId','tbl_country.filename as countryflag','tbl_message_details.queryType as messageQueryType','tbl_message_details.message as message' ,'ad_title', 're_property_details.property_price as value','tbl_message_details.category as messagecategory','tbl_message_details.id as messageId','tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
            ->where('message_id', '=', $request->input('enquiryId'))->where('tbl_message_details.category', '=', $request->input('category'))
            ->join('re_property_details','re_property_details.id', 'tbl_message_details.product_id')
            ->leftJoin('tbl_enquiry','tbl_enquiry.id', 'tbl_message_details.message_id')
            ->join('tbl_agent','tbl_agent.id', 'tbl_message_details.agent_id')
            ->join('tbl_country','tbl_country.id', 're_property_details.country_id')
            ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
            ->get()
            ->sortBy('tbl_message_details.created_at');
            
            
            $images = WatchImageModel::get();
        }
        
        $message_submit = array();
        
        /* if($request->input('category') == 'Automobile' ){
         $message_submit = MyMessageModel::select('submitpricerequest.submit_price','tbl_message_details.category','tbl_message_details.agent_id','submitpricerequest.id as enquiryId','tbl_message_details.messageTo','tbl_message_details.messageFrom','submitpricerequest.user_id as userId','submitpricerequest.agent_userid as agentUserId' ,'tbl_message_details.created_at as message_time','au_automobile_detail.id as productId','tbl_country.filename as countryflag','tbl_message_details.queryType as messageQueryType','tbl_message_details.message as message' ,'ad_title', 'value','tbl_message_details.category as messagecategory','tbl_message_details.id as messageId','tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
         ->where('message_id', '=', $request->input('enquiryId'))->where('tbl_message_details.queryType', '=', 'submitprice')->where('tbl_message_details.category', '=', $request->input('category'))
         ->join('au_automobile_detail','au_automobile_detail.id', 'tbl_message_details.product_id')
         ->join('submitpricerequest','submitpricerequest.id', 'tbl_message_details.message_id')
         ->join('tbl_agent','tbl_agent.id', 'tbl_message_details.agent_id')
         ->join('tbl_country','tbl_country.id', 'au_automobile_detail.country_id')
         ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
         ->get()
         ->sortBy('tbl_message_details.created_at');
         
         $images = AutomobileImageModel::get();
         } */
        //dd($message_submit);
        /* if($request->input('category') == 'Watch' ){
         $message_submit = MyMessageModel::select('watch_submitpricerequest.submit_price','tbl_message_details.category','tbl_message_details.agent_id','watch_submitpricerequest.id as enquiryId','tbl_message_details.messageTo','tbl_message_details.messageFrom','watch_submitpricerequest.user_id as userId','watch_submitpricerequest.agent_userid as agentUserId' ,'tbl_message_details.created_at as message_time','wa_watch_detail.id as productId','tbl_country.filename as countryflag','tbl_message_details.queryType as messageQueryType','tbl_message_details.message as message' ,'ad_title', 'wa_watch_detail.watch_price as value','tbl_message_details.category as messagecategory','tbl_message_details.id as messageId','tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
         ->where('message_id', '=', $request->input('enquiryId'))->where('tbl_message_details.category', '=', $request->input('category'))
         ->join('wa_watch_detail','wa_watch_detail.id', 'tbl_message_details.product_id')
         ->join('watch_submitpricerequest','watch_submitpricerequest.id', 'tbl_message_details.message_id')
         ->join('tbl_agent','tbl_agent.id', 'tbl_message_details.agent_id')
         ->join('tbl_country','tbl_country.id', 'wa_watch_detail.country_id')
         ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
         ->get()
         ->sortBy('tbl_message_details.created_at');
         
         $images = WatchImageModel::get();
         } */
        
        
        $outgoingEnquiryMessage = array();
        $receivingEnquiryMessage = array();
        $i = 0;
        $j= 0;
        
        foreach ($message as $value) {
            $array = array();
            
            if ($value->messageFrom == $id && $value->messageQueryType == 'enquiry'){
                $array['queryType'] = 'enquiry';
                $array['messagefrom'] = $value->messageFrom;
                $array['message'] = $value->message;
                $array['messageId'] = $value->messageId;
                $array['time'] = date_format(date_create($value->message_time), 'g:i A | m-d-yy');
                $outgoingEnquiryMessage[$i++] = $array;
            } else if($value->messageQueryType == 'enquiry'){
                $array['queryType'] = $request->input('queryType');
                $array['messagefrom'] = $value->messageFrom;
                $array['message'] = $value->message;
                $array['messageId'] = $value->messageId;
                $array['time'] = date_format(date_create($value->message_time), 'g:i A | m-d-yy');
                $outgoingEnquiryMessage[$i++] = $array;
                //$receivingEnquiryMessage[$j++] = $array;
            }else if($value->messageFrom == $id && $value->messageQueryType == 'submitprice'){
                
                $array['queryType'] = 'submitprice';
                $array['messagefrom'] = $value->messageFrom;
                $array['message'] = currency()->convert(floatval($value->submit_price), "USD", $selectedCurrency->code);
                $array['messageId'] = $value->messageId;
                $array['time'] = date_format(date_create($value->message_time), 'g:i A | m-d-yy');
                $outgoingEnquiryMessage[$i++] = $array;
            }else if($value->messageQueryType == 'submitprice'){
                
                $array['queryType'] = 'submitprice';
                $array['messagefrom'] = $value->messageFrom;
                $array['message'] = currency()->convert(floatval($value->submit_price), "USD", $selectedCurrency->code);
                $array['messageId'] = $value->messageId;
                $array['time'] = date_format(date_create($value->message_time), 'g:i A | m-d-yy');
                $outgoingEnquiryMessage[$i++] = $array;
            }else if($value->messageFrom == $id && $value->messageQueryType == 'counterOffer'){
                $array['queryType'] = 'counterOffer';
                $array['messagefrom'] = $value->messageFrom;
                $array['messageFinalFrom'] = $value->messageFinalFrom;
                $array['messageId'] = $value->messageId;
                $array['message'] = currency()->convert(floatval($value->counterOffer), "USD", $selectedCurrency->code);
                $array['shippingCost'] = currency()->convert(floatval($value->shippingCost), "USD", $selectedCurrency->code);
                $array['totalPrice'] = currency()->convert(floatval($value->counterOffer + $value->shippingCost), "USD", $selectedCurrency->code);
                $array['time'] = date_format(date_create($value->message_time), 'g:i A | m-d-yy');
                $outgoingEnquiryMessage[$i++] = $array;
            }else if($value->messageQueryType == 'counterOffer'){
                $array['queryType'] = 'counterOffer';
                $array['messagefrom'] = $value->messageFrom;
                $array['messageFinalFrom'] = $value->messageFinalFrom;
                $array['messageId'] = $value->messageId;
                $array['message'] = currency()->convert(floatval($value->counterOffer), "USD", $selectedCurrency->code);
                $array['shippingCost'] = currency()->convert(floatval($value->shippingCost), "USD", $selectedCurrency->code);
                $array['totalPrice'] = currency()->convert(floatval($value->counterOffer + $value->shippingCost), "USD", $selectedCurrency->code);
                $array['time'] = date_format(date_create($value->message_time), 'g:i A | m-d-yy');
                $outgoingEnquiryMessage[$i++] = $array;
            }
            
        }
        
        $result =array();
        
        $result['message'] = $message;
        $result['images'] = $images;
        $result['outgoingEnquiryMessage'] = $outgoingEnquiryMessage;
        $result['receivingEnquiryMessage'] = $receivingEnquiryMessage;
        if(sizeof($message) != 0){
            $result['productPrice'] = currency(floatval($message[0]->value), 'USD', currency()->getUserCurrency());
        }
        else if(sizeof($message_submit) != 0){
            $result['productPrice'] = currency(floatval($message_submit[0]->value), 'USD', currency()->getUserCurrency());
        }
        $result['loggedInUser'] = $id;
        $result['userId'] = $userId;
        
        return $result;
    }
    
    public function saveMessage(Request $request){
        
        $id = Auth::user()->id;
        
        $enquiryId = $request->input('enquiryId');
        
        $messageFrom = '';
        $messageTo = '';
        $messageType = '';
        if($request->input('userId') == $id){
            $messageFrom = $request->input('userId');
            $messageTo = $request->input('listingCreatedById');
            $messageType = 'outgoing';
        }else if ($request->input('listingCreatedBy') == $id){
            $messageTo = $request->input('userId');
            $messageFrom = $request->input('listingCreatedById');
            $messageType = 'receiving';
        }
        
        //$agent = AgentModel::find($request->input('agentId'));
        
        $myMessage = new MyMessageModel();
        $myMessage->message = $request->input('message');
        $myMessage->message_id = $request->input('enquiryId');
        $myMessage->queryType = $request->input('queryType');
        $myMessage->category = $request->input('category');
        //$myMessage->agent_id = $request->input('agentId');
        $myMessage->product_id = $request->input('productId');
        $myMessage->messageFrom = $messageFrom;
        $myMessage->messageTo = $messageTo;
        
        $myMessage->save();
        
        /* $result =array();
        
        $result['message'] = $myMessage->message;
        $result['time'] = $myMessage->created_at;
        $result['messageType'] = $messageType; */
        
        $userId = Auth::user()->id;
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        currency()->setUserCurrency($selectedCurrency->code);
        
        $message = array();
        if($request->input('category') == 'Automobile' ){
            $message = MyMessageModel::select('submitpricerequest.submit_price','submitpricerequest.id as submitpriceId','tbl_message_details.category','tbl_message_details.agent_id','tbl_enquiry.id as enquiryId','tbl_message_details.messageTo','tbl_message_details.messageFrom','tbl_enquiry.userId as userId', 'tbl_message_details.created_at as message_time','tbl_agent.userId as agentUserId','au_automobile_detail.id as productId','tbl_country.filename as countryflag','tbl_message_details.queryType as messageQueryType','tbl_message_details.message as message' ,'ad_title', 'value','tbl_message_details.category as messagecategory','tbl_message_details.id as messageId','tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
            ->where('message_id', '=', $request->input('enquiryId'))
            ->where(function ($query) {
                
                $query->where('tbl_message_details.queryType', '=', 'enquiry')
                ->orWhere('tbl_message_details.queryType', '=', 'submitprice');
            })
            ->where('tbl_message_details.category', '=', $request->input('category'))
            ->join('au_automobile_detail','au_automobile_detail.id', 'tbl_message_details.product_id')
            ->join('submitpricerequest','submitpricerequest.id', 'tbl_message_details.message_id')
            ->Join('tbl_enquiry','tbl_enquiry.id', 'tbl_message_details.message_id')
            ->join('tbl_agent','tbl_agent.id', 'tbl_message_details.agent_id')
            ->join('tbl_country','tbl_country.id', 'au_automobile_detail.country_id')
            ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
            ->get()
            ->sortBy('tbl_message_details.created_at');
            
            
            $images = AutomobileImageModel::get();
        }
        
        
        
        if($request->input('category') == 'Watch' ){
            $message = MyMessageModel::select('tbl_enquiry.agent_userid
            as agentUserId','submitpricerequest.submit_price','submitpricerequest.id as submitpriceId','tbl_message_details.category','tbl_message_details.agent_id','tbl_enquiry.id as enquiryId','tbl_message_details.messageTo','tbl_message_details.messageFrom','tbl_enquiry.userId as userId', 'tbl_message_details.created_at as message_time','wa_watch_detail.id as productId','tbl_message_details.queryType as messageQueryType','tbl_message_details.message as message' ,'wa_watch_detail.brand_name as ad_title', 'wa_watch_detail.watch_price as value','tbl_message_details.category as messagecategory', 'tbl_message_details.id as messageId','tbl_final_offer.finalOfferValue as counterOffer', 'users.created_at as agencycreate', 'tbl_final_offer.shippingCost as shippingCost','tbl_enquiry.listingCreatedBy as listingCreatedById', 'tbl_final_offer.messageFrom as messageFinalFrom')
               ->where('message_id', '=', $request->input('enquiryId'))
               ->where(function ($query) {
                   
                   $query->where('tbl_message_details.queryType', '=', 'enquiry')
                   ->orWhere('tbl_message_details.queryType', '=', 'counterOffer')
                   ->orWhere('tbl_message_details.queryType', '=', 'submitprice');
               })
               ->where('tbl_message_details.category', '=', $request->input('category'))
               ->join('wa_watch_detail','wa_watch_detail.id', 'tbl_message_details.product_id')
               ->join('submitpricerequest','submitpricerequest.id', 'tbl_message_details.message_id')
               ->leftJoin( 'tbl_final_offer', 'tbl_final_offer.messageId','tbl_message_details.id')
               ->join('tbl_enquiry','tbl_enquiry.id', 'tbl_message_details.message_id')
               ->join('users','users.id', 'tbl_enquiry.listingCreatedBy')
               //->join('tbl_agent','tbl_agent.id', 'tbl_message_details.agent_id')
               //->join('tbl_country','tbl_country.id', 'wa_watch_detail.country_id')
               //->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
               ->orderBy('tbl_message_details.created_at','ASC')
               ->get();
            
            
            $images = WatchImageModel::get();
        }
        
        if($request->input('category') == 'RealEstate'){
            $message = MyMessageModel::select('tbl_message_details.category','tbl_message_details.agent_id','tbl_enquiry.id as enquiryId','tbl_message_details.messageTo','tbl_message_details.messageFrom','tbl_enquiry.userId as userId', 'tbl_message_details.created_at as message_time','tbl_agent.userId as agentUserId','re_property_details.id as productId','tbl_country.filename as countryflag','tbl_message_details.queryType as messageQueryType','tbl_message_details.message as message' ,'ad_title', 're_property_details.property_price as value','tbl_message_details.category as messagecategory','tbl_message_details.id as messageId','tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
            ->where('message_id', '=', $request->input('enquiryId'))->where('tbl_message_details.category', '=', $request->input('category'))
            ->join('re_property_details','re_property_details.id', 'tbl_message_details.product_id')
            ->leftJoin('tbl_enquiry','tbl_enquiry.id', 'tbl_message_details.message_id')
            ->join('tbl_agent','tbl_agent.id', 'tbl_message_details.agent_id')
            ->join('tbl_country','tbl_country.id', 're_property_details.country_id')
            ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
            ->get()
            ->sortBy('tbl_message_details.created_at');
            
            
            $images = WatchImageModel::get();
        }
        
        $message_submit = array();
        
        $outgoingEnquiryMessage = array();
        $receivingEnquiryMessage = array();
        $i = 0;
        $j= 0;
        
        
        foreach ($message as $value) {
            $array = array();
            
            if ($value->messageFrom == $id && $value->messageQueryType == 'enquiry') {
                $array['queryType'] = 'enquiry';
                $array['messagefrom'] = $value->messageFrom;
                $array['message'] = $value->message;
                $array['time'] = date_format(date_create($value->message_time), 'g:i A | m-d-yy');
                $outgoingEnquiryMessage[$i++] = $array;
            } else if($value->messageQueryType == 'enquiry'){
                $array['queryType'] = $request->input('queryType');
                $array['messagefrom'] = $value->messageFrom;
                $array['message'] = $value->message;
                $array['time'] = date_format(date_create($value->message_time), 'g:i A | m-d-yy');
                $outgoingEnquiryMessage[$i++] = $array;
                //$receivingEnquiryMessage[$j++] = $array;
            }else if($value->messageFrom == $id && $value->messageQueryType == 'submitprice'){
                $array['queryType'] = 'submitprice';
                $array['messagefrom'] = $value->messageFrom;
                $array['message'] = $value->submit_price;
                $array['time'] = date_format(date_create($value->message_time), 'g:i A | m-d-yy');
                $outgoingEnquiryMessage[$i++] = $array;
            }else if($value->messageQueryType == 'submitprice'){
                
                $array['queryType'] = 'submitprice';
                $array['messagefrom'] = $value->messageFrom;
                $array['message'] = $value->submit_price;
                $array['time'] = date_format(date_create($value->message_time), 'g:i A | m-d-yy');
                $outgoingEnquiryMessage[$i++] = $array;
            }
            
        }
        
        //$result =array();
        
        $result['message'] = $message;
        $result['images'] = $images;
        $result['outgoingEnquiryMessage'] = $outgoingEnquiryMessage;
        $result['receivingEnquiryMessage'] = $receivingEnquiryMessage;
        if(sizeof($message) != 0){
            $result['productPrice'] = currency(floatval($message[0]->value), 'USD', currency()->getUserCurrency());
        }
        else if(sizeof($message_submit) != 0){
            $result['productPrice'] = currency(floatval($message_submit[0]->value), 'USD', currency()->getUserCurrency());
        }
        $result['loggedInUser'] = $id;
        $result['userId'] = $userId;
        
        
        return $result;
    }
    
}
