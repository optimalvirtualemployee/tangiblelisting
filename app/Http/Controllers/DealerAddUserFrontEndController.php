<?php

namespace App\Http\Controllers;

use App\CountryModel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\AgenciesImageModel;
use App\AgenciesModel;
use App\AgentModel;
use App\AgentImageModel;
use App\UserPermissionModel;
use App\AgentJobPositionModel;
use App\Timezone;
use App\CityModel;

class DealerAddUserFrontEndController extends Controller
{
    public function index()
    {
        return view('dealer.addUser');
    }
    
    
    public function listAgents(){
        
        $dealerId = Auth::user()->id;
        $agency = DB::table('tbl_agencies')->where('userId',$dealerId)->first();
        
        $dealer = AgenciesModel::select('tbl_category.website_category as category')
        ->where('userId', '=', Auth::user()->id)
        ->join('tbl_category', 'tbl_category.id', 'tbl_agencies.category_id')
        ->first();
        
        $agentList = AgentModel::select('tbl_agent.id as agentid',DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 
        DB::raw('CONCAT(tbl_agencies.first_name, " ", tbl_agencies.last_name) AS agency_name'), 'tbl_country.country_name as countryName',
        'tbl_city.city_name as cityName', 'tbl_agent.street_1 as line1', 'tbl_agent.street_2 as line2', 'tbl_agent.postal_code as line3',
        'tbl_agent.email as agentemail', 'tbl_agent.mobile as agentmobile')
        ->join('tbl_agencies', 'tbl_agencies.id', 'tbl_agent.agency_id')
        ->join('tbl_country', 'tbl_country.id', 'tbl_agent.country_id')
        ->join('tbl_city', 'tbl_city.id', 'tbl_agent.city_id')
        ->where('agency_id', '=', $agency->id)
        ->get();
        
        $images = AgentImageModel::get();
        
        return view('dealer.agentlist', compact('agentList', 'agency', 'images', 'dealer'));
    }
    
    public function create(){
        
        $country_data = CountryModel::get();
        $roles = Role::select('id')->where('name', '=', 'Agent')->first();
        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
        ->where("role_has_permissions.role_id",$roles->id)
        ->get();
        //dd($rolePermissions);
        $dealer;
        $dealer_images;
        $dealer = AgenciesModel::where('userId', '=', Auth::User()->id)->first();
        if(!empty($dealer->id)){
            $dealer_images = AgenciesImageModel::select('filename')->where('listing_id', '=', $dealer->id)->first();
        }
        
        $jobposition_list = AgentJobPositionModel::get();
        $timezone = Timezone::Orderby('offset')->get();
        
        
        return view('dealer.addUser', compact('country_data', 'rolePermissions', 'dealer_images', 'jobposition_list', 'timezone'));
    }
    
    public function store(Request $request){
              
            
            $this->validate($request, [
                'title' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'countryId' => 'required',
                'cityId' => 'required',
                'fax' => 'required',
                'phone' => 'required',
                'mobile' => 'required',
                'email' => 'required|email|unique:users,email',
                'photos' => 'required',
                'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]);
            
            $createAgent = new AgentModel();
            
            $userId = Auth::user()->id;
            
            $agencyId = AgenciesModel::where('userId', '=', $userId)->pluck('id')->first();
            
                $createAgent->agency_id = $agencyId;
                if($userId !=''){
                $createAgent->createdID = $userId;
                }
                $createAgent->title = $request->input('title');
                $createAgent->first_name = $request->input('first_name');
                $createAgent->last_name = $request->input('last_name');
                $createAgent->country_id = $request->input('countryId');
                if($request->input('stateId') != '')
                    $createAgent->state_id = $request->input('stateId');
                    
                $createAgent->city_id = $request->input('cityId');
                $createAgent->street_1 = $request->input('street_1');
                $createAgent->street_2 = $request->input('street_2');
                $createAgent->postal_code = $request->input('postal_code');
                $createAgent->whatsapp = $request->input('mobile');
                $createAgent->mobile = $request->input('mobile');
                $createAgent->fax = $request->input('fax');
                $createAgent->office = $request->input('phone');
                $createAgent->email = $request->input('email');
                $createAgent->timezone_id = $request->input('timezone');
                $createAgent->status = '1';
                    
                $input['first_name'] = $request->input('first_name');
                $input['last_name'] = $request->input('last_name');
                $input['email'] = $request->input('email');
                $input['password'] = Hash::make($request->input('password'));
                    
                $user = User::create($input);
                $user->assignRole('Agent');
                    
                if($request->permission ?? ''){
                    foreach ($request->permission as $permission){
                        $userPermission = new UserPermissionModel();
                        
                        $userPermission->userId = $user->id;
                        $userPermission->permissionId = $permission;
                        
                        $userPermission->save();
                    }
                }

                $createAgent->userId = $user->id;
                    
                $createAgent->save();
                    
                $listing_id = $createAgent->id;
                    
                if($request->file('photos') ?? ''){
                     foreach ($request->file('photos') as $image){
                     $uploadImage = new AgentImageModel();
                            
                     $extension = $image->getClientOriginalExtension();
                     Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                            
                     $uploadImage->images = $image->getClientMimeType();
                     $uploadImage->filename = $image->getFilename().'.'.$extension;
                     $uploadImage->listing_id = $listing_id;
                     $uploadImage->save();
                        }
                    }
                    
                    return redirect('/admin/agentdata')->with('success_msg', 'Agent Created successfully!');
    }
    
    public function edit($agentId)
    {
        $id = Crypt::decrypt($agentId);

        $agent = AgentModel::find($id);
        $country_data = CountryModel::get();
        $city_data = CityModel::get();
        $roles = Role::select('id')->where('name', '=', 'Agent')->first();
        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
        ->where("role_has_permissions.role_id",$roles->id)
        ->get();
        //dd($rolePermissions);
        $dealer;
        $dealer_images;
        $dealer = AgenciesModel::where('userId', '=', Auth::User()->id)->first();
        if(!empty($dealer->id)){
            $dealer_images = AgenciesImageModel::select('filename')->where('listing_id', '=', $dealer->id)->first();
        }
        
        $jobposition_list = AgentJobPositionModel::get();
        $timezone = Timezone::Orderby('offset')->get();
        
        $agentimages = AgentImageModel::select('filename')->where('listing_id', '=', $agent->id)->first();
        
        return view('dealer.addUser', compact('agent','country_data', 'city_data' ,'rolePermissions', 'dealer_images', 'jobposition_list', 'timezone', 'agentimages'));
    }
    
    public function update(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'countryId' => 'required',
            'cityId' => 'required',
            'fax' => 'required',
            'phone' => 'required',
            'mobile' => 'required',
            'email' => 'required|email|unique:users,email,'.$request->userId,
            'photos' => 'required',
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        
        $updateAgent = AgentModel::find($request->id);
        
        $userId = Auth::user()->id;
        
        $agencyId = AgenciesModel::where('userId', '=', $userId)->pluck('id')->first();
        
        $updateAgent->agency_id = $agencyId;
        
        
        $updateAgent->title = $request->input('title');
        $updateAgent->first_name = $request->input('first_name');
        $updateAgent->last_name = $request->input('last_name');
        $updateAgent->country_id = $request->input('countryId');
        if($request->input('stateId') != '')
            $updateAgent->state_id = $request->input('stateId');
            
            $updateAgent->city_id = $request->input('cityId');
            $updateAgent->street_1 = $request->input('street_1');
            $updateAgent->street_2 = $request->input('street_2');
            $updateAgent->postal_code = $request->input('postal_code');
            $updateAgent->whatsapp = $request->input('mobile');
            $updateAgent->mobile = $request->input('mobile');
            $updateAgent->fax = $request->input('fax');
            $updateAgent->office = $request->input('phone');
            $updateAgent->email = $request->input('email');
            $updateAgent->timezone_id = $request->input('timezone');
            $updateAgent->status = '1';
            
            $input['first_name'] = $request->input('first_name');
            $input['last_name'] = $request->input('last_name');
            $input['email'] = $request->input('email');
            if(!empty($request->input('password'))){
                $input['password'] = Hash::make($request->input('password'));
            }
            
            $user = User::find($updateAgent->userId);
            $user->update($input);
            $user->assignRole('Agent');
            
            if($request->permission ?? ''){
                foreach ($request->permission as $permission){
                    $userPermission = new UserPermissionModel();
                    
                    $userPermission->userId = $user->id;
                    $userPermission->permissionId = $permission;
                    
                    $userPermission->save();
                }
            }
            
            $updateAgent->userId = $user->id;
            
            $updateAgent->update();
            
            $listing_id = $updateAgent->id;
            
            $delete = AgentImageModel::where('listing_id', '=', $listing_id)->delete();
            
            if($request->file('photos') ?? ''){
                foreach ($request->file('photos') as $image){
                    $uploadImage = new AgentImageModel();
                    
                    $extension = $image->getClientOriginalExtension();
                    Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                    
                    $uploadImage->images = $image->getClientMimeType();
                    $uploadImage->filename = $image->getFilename().'.'.$extension;
                    $uploadImage->listing_id = $listing_id;
                    $uploadImage->save();
                }
            }
            
            return redirect('/dealer/listagents')->with('success_msg', 'Agent Created successfully!');
    }
}
