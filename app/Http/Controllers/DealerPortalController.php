<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\AgenciesModel;
use App\AgenciesImageModel;
use App\CityModel;

class DealerPortalController extends Controller
{
    public function index(){
        
        //dd(Auth::User()->id);
        
        $dealer = AgenciesModel::where('userId', '=', Auth::User()->id)->first();
        $dealer_images='';
        if(!empty($dealer->id)){
        $dealer_images = AgenciesImageModel::select('filename')->where('listing_id', '=', $dealer->id)->first();
        }
        
        $dealer = AgenciesModel::select('tbl_category.website_category as category')
        ->where('userId', '=', Auth::user()->id)
        ->join('tbl_category', 'tbl_category.id', 'tbl_agencies.category_id')
        ->first();
        
        return view('dealer.index', compact('dealer_images', 'dealer'));
    }
    
    public function getCityByState(Request $request){
        
        $stateId = $request->input('stateId');
        $countryId = $request->input('countryId');
        
        if($stateId == '')
            $city = CityModel::select('id', 'city_name')->where('country_id', $countryId)->get();
            else
                $city = CityModel::select('id', 'city_name')->where('state_id', $stateId)->get();
                
                
                return $city;
    }
}
