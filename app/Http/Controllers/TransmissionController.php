<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TransmissionModel;

class TransmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transmission = TransmissionModel::get();
        return view('admin.automobile.transmission.index',compact('transmission'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.automobile.transmission.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'transmission' => 'required|regex:/^([a-zA-Z_]+)(\s[a-zA-Z_]+)*$/|unique:au_transmission'
        ]);

        $transmission = TransmissionModel::create($validate);
        $transmission->save();
        return redirect('/admin/transmission')->with('success_msg','Transmission Created successfully!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transmission = TransmissionModel::find($id);
        return view('admin.automobile.transmission.edit', compact('transmission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'transmission' => 'required|regex:/^([a-zA-Z_]+)(\s[a-zA-Z_]+)*$/'
        ]);

        $transmission = TransmissionModel::find($id);

        if ($transmission->transmission == $validate['transmission']) {
            $transmission->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['transmission' => 'unique:au_transmission']);
            $transmission->transmission = $validate['transmission'];
            $transmission->status = $request->input('status');
        }

        $transmission->save();

        return redirect('/admin/transmission')->with('success_msg','Transmission Updated successfully!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = TransmissionModel::find($id)->delete();
        return redirect('/admin/transmission')->with('success_msg','Transmission deleted successfully!');
    }
}
