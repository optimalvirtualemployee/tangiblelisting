<?php

namespace App\Http\Controllers;

use App\ContactusModel;
use App\CurrencyModel;
use App\PropertyTypeModel;
use Illuminate\Http\Request;
use App\AutomobileBlogModel;
use App\AutomobileBlogImageModel;

class CarBlogFrontEndController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contactUs = ContactusModel::get()->first();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        $propertyType = PropertyTypeModel::get();
        
        $blog = AutomobileBlogModel::find($id);
        
        $blog_images = AutomobileBlogImageModel::get();
        
        
        $type_blog = AutomobileBlogModel::select('au_blog.id as id','filename','title', 'blogPost', 'au_blog.created_at as created_at', 'blogType', 'url')
        ->join('au_blog_images', 'au_blog_images.listing_id', '=', 'au_blog.id')
        ->where('status', '=', '1')->where('au_blog.id', '!=', $id)->orderBy('au_blog.created_at','desc')->limit('4')->get();
        
        return view('tangiblehtml.car-blog', compact('contactUs', 'propertyType', 'blog', 'blog_images', 'type_blog'));
    }
}
