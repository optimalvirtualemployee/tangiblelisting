<?php

namespace App\Http\Controllers;

use App\CityModel;
use App\ContactusModel;
use App\CountryModel;
use App\CurrencyModel;
use App\MyMessageModel;
use App\ProductOrderModel;
use App\StateModel;
use App\WatchImageModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\OrderStatusHierarchyModel;

class ProductPaymentController extends Controller
{
    public function payment(Request $request){

        if (Auth::user() == null){
            return redirect('/');
        }
        
        $id = Auth::user()->id;
        
        
        $orderProduct =  ProductOrderModel::find($request->input('enquiryId'));
        
        $orderProduct->order_status = 'payment done';
        
        $orderProduct->update();
        
        $orderStatus = new OrderStatusHierarchyModel();
        
        $orderStatus->enquiryId = $request->enquiryId;
        $orderStatus->status = 'payment done';
        
        $orderStatus->save();
        
        $contactUs = ContactusModel::get()->first();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        
        if($request->category == 'Watch'){
            $enquiry = MyMessageModel::select('submitpricerequest.submit_price as submitprice','submitpricerequest.id as submitpriceId','tbl_enquiry.agent_id','tbl_enquiry.id as id','productId','tbl_enquiry.created_at', 'ad_title', 'watch_price as value', 'tbl_country.filename as countryflag',
                'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate', 'tbl_enquiry.productId as productId', 'tbl_user_details.user_mobile as userMobile', 'tbl_user_details.user_address as userAddress', 'tbl_user_details.user_postalCode as userPostalCode',
                'tbl_user_details.country_id as userCountryId','users.email as userEmail',DB::raw('CONCAT(users.first_name, " ", users.last_name) AS user_name'),  'users.first_name as userFirstName', 'users.last_name as userLastName', 'tbl_user_details.city_id as userCityId','tbl_user_details.country_id as userCountryId',
                'tbl_enquiry.userID as userID', 'tbl_final_offer.finalOfferValue as finalOfferValue', 'tbl_final_offer.shippingCost as shippingCost', 'tbl_final_offer.deliveryTime as deliveryTime', 'tbl_final_offer.offerValid as offerValid', 'tbl_final_offer.message as finalMessage',  'tbl_final_offer.created_at as requestSent' ,'tbl_country.countrycode as countrycode', 'wa_watch_detail.agencies_id as agencyId', 'tbl_message_details.id as messageId','tbl_message_details.category as category',  'tbl_message_details.queryType as queryType')
                ->where('tbl_message_details.id', '=', $request->input('messageId'))
                /* ->where('tbl_message_details.queryType', '=', 'counterOffer') */
            ->where('tbl_message_details.category', '=', 'watch')
            ->join('submitpricerequest','submitpricerequest.id', 'tbl_message_details.message_id')
            ->leftJoin( 'tbl_final_offer', 'tbl_final_offer.messageId','tbl_message_details.id')
            ->join('tbl_enquiry','tbl_enquiry.id','tbl_message_details.message_id')
            ->join('tbl_user_details', 'tbl_user_details.user_id','tbl_enquiry.userID')
            ->join('users', 'users.id','tbl_enquiry.userID')
            ->join('wa_watch_detail','wa_watch_detail.id', 'tbl_enquiry.productId')
            ->join('tbl_country','tbl_country.id', 'wa_watch_detail.country_id')
            ->join('tbl_agent','tbl_agent.id', 'tbl_enquiry.agent_id')
            ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
            ->first();
            
            $images = WatchImageModel::where('listing_id',$enquiry->productId)->first();
        }
        
        if($enquiry->userCityId != null)
            $user_city = CityModel::find($enquiry->userCityId);
            
        if($enquiry->userCountryId != null)
            $user_country = CountryModel::find($enquiry->userCountryId);
                
        if($user_city != null)
            $user_state = StateModel::find($user_city->state_id);
                    
            $currentStep = ProductOrderModel::select('order_status')->where('id', '=', $request->enquiryId)->where('category', '=', $request->category)->where('userID', '=', $id)->first();
            
       $converted_shipping = currency()->convert(floatval($enquiry->shippingCost), "USD",$selectedCurrency->code, false);
        
       return view('tangiblehtml.initialOrderStatusBuyer', compact('currentStep','contactUs', 'enquiry', 'images', 'user_state','user_country','user_city', 'converted_shipping'));
    }
}
