<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InteriorColourModel;

class InteriorColourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interior_colour = InteriorColourModel::get();
        return view('admin.automobile.interior_colour.index',compact('interior_colour'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.automobile.interior_colour.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'interior_colour' => 'required|regex:/^([a-zA-Z_]+)(\s[a-zA-Z_]+)*$/|unique:au_interior_colour',
            'status' => 'required'
        ]);

        $interior_colour = InteriorColourModel::create($validate);
        $interior_colour->save();
        return redirect('/admin/interior_colour')->with('success_msg','Interior Colour Created successfully!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $interior_colour_data = InteriorColourModel::find($id);
        return view('admin.automobile.interior_colour.edit', compact('interior_colour_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'interior_colour' => 'required|regex:/^([a-zA-Z_]+)(\s[a-zA-Z_]+)*$/'
        ]);

        $interior_colourUpdate = InteriorColourModel::find($id);

        if ($interior_colourUpdate->interior_colour == $validate['interior_colour']) {
            $interior_colourUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['interior_colour' => 'unique:au_interior_colour']);
            $interior_colourUpdate->interior_colour = $validate['interior_colour'];
            $interior_colourUpdate->status = $request->input('status');
        }

        $interior_colourUpdate->save();

        return redirect('/admin/interior_colour')->with('success_msg','Interior Colour Updated successfully!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = InteriorColourModel::find($id)->delete();
        return redirect('/admin/interior_colour')->with('success_msg','Interior Colour deleted successfully!');
    }
}
