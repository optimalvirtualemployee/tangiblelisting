<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Session;
use App\User;
class UserLoginController extends Controller
{
    public function login() {
        return view('user.login');
    }
    public function register(){
        return view('user.register');
    }
    function checklogin(Request $request) {
			$this->validate($request, [
				'email'   => 'required|email',
				'password'  => 'required|min:3'
			]);
			$user_data = array(
				'email'  => $request->post('email'),
				'password' => $request->post('password'),
                'usertype' =>'3'
			);
			
			$response = '';
		if(Auth::attempt($user_data)){

            if(Auth::user()->status=='1'){
                $response = "logged-In";
                
                return $response;
            }
            else{
                $response = "User is not active";
                
                return $response;
            }

		} else {
            $response = "Invalid user name or password";
		    
		    return $response;
		}
    }
    public function newUserRegister(Request $request){
        $this->validate($request, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'min:10', 'max:12'],
            'password' => ['required', 'string', 'min:8']
        ]);

        $regUser = new User;
        $regUser->usertype  =  3;
        $regUser->first_name  =  $request->first_name;
        $regUser->last_name  =   $request->last_name;
        $regUser->email  =   $request->email;
        $regUser->phone  =  $request->phone;
        $regUser->password  =   Hash::make($request->password);
        $regUser->save();
            $user_data = array(
                'email'  => $request->post('email'),
                'password' => $request->post('password'),
                'usertype' =>'3'
            );
        Auth::attempt($user_data);
        session(['url.intended' => url()->previous()]);
                return redirect(session()->get('url.intended'))->with('success_msg', 'User Register Successfully!');
    }

    function successlogin()  {
     	return view('dashbord');
    }

    function logout()  {
	    Auth::logout();
	    return redirect('main');
    }
    
    public function checkRegisteredUser(Request $request){
        
        $email = $request->email;
        
        $user = User::where('email', '=', $email)->exists();
        
        $converted_res = $user ? 'true' : 'false';
        
        return $converted_res;
        
    }
}
