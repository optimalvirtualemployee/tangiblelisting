<?php

namespace App\Http\Controllers;

use App\AutomobileImageModel;
use App\ContactusModel;
use App\CurrencyModel;
use App\EnquiryModel;
use App\PropertyImageModel;
use App\WatchImageModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\AgentModel;

class SellController extends Controller
{
    public function index(){
        
        $contactUs = ContactusModel::get()->first();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        
        $id = Auth::user()->id;
        
        $enquiry_automobile = EnquiryModel::select('au_automobile_detail.id as listing_id','tbl_enquiry.agent_id','tbl_enquiry.id as id','productId','tbl_enquiry.created_at', 'ad_title', 'value', 'tbl_country.country_name as country_name', 'tbl_country.filename as countryflag',
            'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
            ->where(function ($query) use ($id) {
                
                $query->where('tbl_enquiry.agent_userid', '=', $id);
            })->where('category', '=', 'Automobile')
            ->join('au_automobile_detail','au_automobile_detail.id', 'tbl_enquiry.productId')
            ->join('tbl_country','tbl_country.id', 'au_automobile_detail.country_id')
            ->join('tbl_agent','tbl_agent.id', 'tbl_enquiry.agent_id')
            ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
            ->get();
            
            //dd(DB::getQueryLog());
            $enquiry_watch = EnquiryModel::select('wa_watch_detail.id as listing_id','tbl_enquiry.agent_id','tbl_enquiry.id as id','productId','tbl_enquiry.created_at', 'ad_title', 'watch_price as value', 'tbl_country.country_name as country_name', 'tbl_country.filename as countryflag',
                'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
                ->where(function ($query) use ($id) {
                    
                    $query->where('tbl_enquiry.agent_userid', '=', $id);
                })->where('category', '=', 'watch')
                ->join('wa_watch_detail','wa_watch_detail.id', 'tbl_enquiry.productId')
                ->join('tbl_country','tbl_country.id', 'wa_watch_detail.country_id')
                ->join('tbl_agent','tbl_agent.id', 'tbl_enquiry.agent_id')
                ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
                ->get();
                
                $enquiry_property = EnquiryModel::select('re_property_details.id as listing_id','tbl_enquiry.agent_id','tbl_enquiry.id as id','productId','tbl_enquiry.created_at', 'ad_title', 'property_price as value', 'tbl_country.country_name as country_name', 'tbl_country.filename as countryflag',
                    'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
                    ->where(function ($query) use ($id) {
                        
                        $query->where('tbl_enquiry.agent_userid', '=', $id);
                    })->where('category', '=', 'RealEstate')
                    ->join('re_property_details','re_property_details.id', 'tbl_enquiry.productId')
                    ->join('tbl_country','tbl_country.id', 're_property_details.country_id')
                    ->join('tbl_agent','tbl_agent.id', 'tbl_enquiry.agent_id')
                    ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
                    ->get();
                    
                    $agent = AgentModel::where('userId', '=',$id)->first();
                    
                    $my_listing = array();
                    
                    $automobile_listing = array();
                    $watch_listing = array();
                    $property_listing = array();
                    
                    if ($agent != null) {
                        $id = $agent->id;
                        $automobile_listing = AutomobileDataListingModel::select('currency_code', 'au_automobile_detail.id as id', 'ad_title', 'build_year', 'neworused', 'value', 'odometer', 'au_transmission.transmission as transmission', 'rhdorlhd', 'au_automobile_detail.created_at as automobilecreationtime', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode', 'tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id')->join('tbl_currency', 'tbl_currency.id', '=', 'au_automobile_detail.currency_id')
                        ->join('tbl_state', 'tbl_state.id', '=', 'au_automobile_detail.state_id')
                        ->join('tbl_city', 'tbl_city.id', '=', 'au_automobile_detail.city_id')
                        ->join('tbl_country', 'tbl_country.id', '=', 'au_automobile_detail.country_id')
                        ->join('tbl_agent', 'tbl_agent.id', '=', 'au_automobile_detail.agent_id')
                        ->join('re_build_year', 're_build_year.id', '=', 'au_automobile_detail.year_id')
                        ->join('au_transmission', 'au_transmission.id', '=', 'au_automobile_detail.transmission_id')
                        ->join('au_body_type', 'au_body_type.id', '=', 'au_automobile_detail.body_type_id')
                        ->where('agent_id', '=', $id)
                        ->whereIN('au_automobile_detail.id', function ($query) {
                            $query->select('listing_id')
                            ->from('au_automobile_images');
                        })
                        ->whereIN('au_automobile_detail.id', function ($query) {
                            $query->select('listing_id')
                            ->from('au_automobile_features');
                        })
                        ->whereIN('au_automobile_detail.id', function ($query) {
                            $query->select('listing_id')
                            ->from('au_automobile_comments');
                        })
                        ->where('au_automobile_detail.status', '=', '1')
                        ->orderBy('au_automobile_detail.created_at', 'DESC')
                        ->get();
                    }
                    
                    if ($agent != null) {
                        $id = $agent->id;
                        $watch_listing = WatchDataListingModel::select('ad_title', 'wa_watch_detail.id as id' ,'currency_code', 'brand_name', 'model_name','watch_price as value', 'year_of_manufacture', 'case_diameter', 'city_name',
                            'state_name', 'watch_price', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode', 'tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id')
                            ->join('tbl_currency', 'tbl_currency.id', '=', 'wa_watch_detail.currency_id')
                            ->leftJoin('wa_year_of_manufacture', 'wa_year_of_manufacture.id', '=', 'wa_watch_detail.year_of_manufacture_id')
                            ->leftJoin('wa_case_diameter','wa_case_diameter.id', '=', 'wa_watch_detail.case_diameter_id' )
                            ->join('tbl_state', 'tbl_state.id', '=', 'wa_watch_detail.state_id')
                            ->join('tbl_city', 'tbl_city.id', '=', 'wa_watch_detail.city_id')
                            ->join('tbl_country', 'tbl_country.id', '=', 'wa_watch_detail.country_id')
                            ->join('tbl_agent', 'tbl_agent.id', '=', 'wa_watch_detail.agent_id')
                            ->whereIN('wa_watch_detail.id', function($query){
                                $query->select('listing_id')
                                ->from('wa_watch_images');
                            })
                            ->whereIN('wa_watch_detail.id', function($query){
                                $query->select('listing_id')
                                ->from('wa_watch_features');
                            })
                            ->whereIN('wa_watch_detail.id', function($query){
                                $query->select('listing_id')
                                ->from('wa_watch_comment');
                            })->where('agent_id', '=', $id)
                            ->where('wa_watch_detail.status','=', '1')
                            ->orWhere('wa_watch_detail.case_diameter_id','=', 'NULL')
                            ->orderBy('wa_watch_detail.created_at', 'DESC')
                            ->get();
                    }
                    
                    if ($agent != null) {
                        $id = $agent->id;
                        $property_listing = PropertyDataModel::select('re_property_details.id as id', 'currency_code' ,'ad_title', 'land_size as size','property_price as value', 'number_of_bedrooms as bedrooms',
                            'metric', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode','tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id', 're_property_details.created_at as creationtime')
                            ->join('tbl_currency', 'tbl_currency.id', '=', 're_property_details.currency_id')
                            ->join('tbl_city', 'tbl_city.id', '=', 're_property_details.city_id')
                            ->join('tbl_country', 'tbl_country.id', '=', 're_property_details.country_id')
                            ->join('tbl_agent', 'tbl_agent.id', '=', 're_property_details.agent_id')
                            ->join('re_build_year', 're_build_year.id', '=', 're_property_details.year_built_id')
                            ->join('re_beds', 're_beds.id', '=', 're_property_details.bed_id')
                            ->whereIN('re_property_details.id', function($query){
                                $query->select('listing_id')
                                ->from('re_property_images');
                            })
                            ->whereIN('re_property_details.id', function($query){
                                $query->select('listing_id')
                                ->from('re_property_features');
                            })
                            ->whereIN('re_property_details.id', function($query){
                                $query->select('listing_id')
                                ->from('re_property_comment');
                            })->where('agent_id', '=', $id)
                            ->where('re_property_details.status','=', '1')
                            ->orderBy('re_property_details.created_at', 'DESC')
                            ->get();
                    }
                    
                    $automobile_images = AutomobileImageModel::get();
                    $watch_images = WatchImageModel::get();
                    $property_images = PropertyImageModel::get();
        
                    return view('tangiblehtml.sell',compact('contactUs','enquiry_automobile','enquiry_watch', 'enquiry_property','automobile_images', 'watch_images', 'property_listing', 'watch_listing', 'automobile_listing'));
    }
}
