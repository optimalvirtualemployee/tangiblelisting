<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PropertyContractedDetailsModel;

class PropertyContractedDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
        $contractedProperty = PropertyContractedDetailsModel::select('ad_title', 'tbl_agent.first_name as agent_first_name', 
            'tbl_agent.last_name as agent_last_name' , 'tbl_customer.first_name as customer_first_name', 'tbl_customer.last_name as customer_last_name')
        ->join('tbl_agent', 'tbl_agent.id', '=', 're_customer_product-details.dealer_id')
        ->join('tbl_customer', 'tbl_customer.id', '=', 're_customer_product-details.customer_id')
        ->join('re_property_details', 're_property_details.id', '=', 're_customer_product-details.product_id')
        ->get();
        
        //dd($contractedProperty);
        
        return view('admin.admincommon.contractedproperties.index', compact('contractedProperty'));
    }
}
