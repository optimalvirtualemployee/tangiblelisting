<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LanguageModel;

class LanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $languages = LanguageModel::get();
        
        return view('admin.admincommon.language.display', compact('languages'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admincommon.language.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            
            
            'language' => ['required'],
            'status' => ['required']
        ]);
        
        $createLanguage = new LanguageModel();
        
        $createLanguage->language = $request->language;
        $createLanguage->status = $request->status;
        
        $createLanguage->save();
        
        return redirect('/admin/language')->with('success_msg', 'Language Created successfully!');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $language = LanguageModel::find($id);
        
        return view('admin.admincommon.language.edit',compact('language'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            
            
            'language' => ['required'],
            'status' => ['required']
        ]);
        
        $updateLanguage =LanguageModel::find($id);
        
        $updateLanguage->language = $request->language;
        $updateLanguage->status = $request->status;
        
        $updateLanguage->save();
        
        return redirect('/admin/language')->with('success_msg', 'Language Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = LanguageModel::find($id)->delete();
        return redirect('/admin/language')->with('success_msg', 'Language Deleted successfully!');
    }
}
