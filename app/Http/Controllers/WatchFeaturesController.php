<?php

namespace App\Http\Controllers;

use App\WatchFeaturesModel;
use Illuminate\Http\Request;
use App\DialPowerReserveModel;
use App\DialColorModel;
use App\BraceletMaterialModel;
use App\BraceletColorModel;
use App\BraceletClaspTypeModel;
use App\BraceletClaspMaterialModel;
use App\CaseMaterialModel;
use App\CaseMMModel;
use App\CaseWaterRDModel;
use App\CaseGlassTypeModel;
use App\FunctionChronographModel;
use App\FunctionTourbillionModel;
use App\FunctionGMTModel;
use App\FunctionAnnualCalenderModel;
use App\FunctionMinuteRepeaterModel;
use App\FunctionDoubleChronographModel;
use App\FunctionPanormaDateModel;
use App\FunctionJumpingHourModel;
use App\FunctionAlarmModel;
use App\FunctionYearModel;
use App\FunctionDayModel;
use App\WatchBezelModel;


class WatchFeaturesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createFeature(Request $request, $listing_id)
    {
        $power_reserve = DialPowerReserveModel::get();
        $bezel_material = WatchBezelModel::get();
        $dial_color = DialColorModel::get();
        $bracelet_material = BraceletMaterialModel::get();
        $bracelet_color = BraceletColorModel::get();
        $type_of_clasp = BraceletClaspTypeModel::get();
        $clasp_material = BraceletClaspMaterialModel::get();
        $case_material = CaseMaterialModel::get();
        $case_mm = CaseMMModel::get();
        $wrd = CaseWaterRDModel::get();
        $glass_type = CaseGlassTypeModel::get();
        $chronograph = FunctionChronographModel::get();
        $tourbillion = FunctionTourbillionModel::get();
        $gmt = FunctionGMTModel::get();
        $annual_calender = FunctionAnnualCalenderModel::get();
        $minute_repeater = FunctionMinuteRepeaterModel::get();
        $double_chronograph = FunctionDoubleChronographModel::get();
        $panorma_date = FunctionPanormaDateModel::get();
        $jumping_hour = FunctionJumpingHourModel::get();
        $alarm = FunctionAlarmModel::get();
        $year = FunctionYearModel::get();
        $day = FunctionDayModel::get();
        
        
        
        return view('admin.watch.features.create',
            compact('listing_id','power_reserve', 'bezel_material' ,'dial_color', 'bracelet_material', 'bracelet_color','type_of_clasp', 'clasp_material', 'case_material', 
                    'case_mm', 'wrd', 'glass_type','chronograph','tourbillion', 'gmt', 'annual_calender', 'minute_repeater', 'double_chronograph', 'panorma_date',
                    'jumping_hour', 'alarm', 'year', 'day'));
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            
            
            'listing_id' => ['required']
        ]);
        
        $createFeature = new WatchFeaturesModel();
        
        $createFeature->listing_id = $request->input('listing_id');
        $createFeature->bezel_material_id = $request->input('bezelMaterial');
        $createFeature->power_reserve_id = $request->input('powerReserve');
        $createFeature->dial_color_id = $request->input('dialColor');
        $createFeature->bracelet_material_id = $request->input('braceletMaterial');
        $createFeature->bracelet_color_id = $request->input('braceletColor');
        $createFeature->type_of_clasp_id = $request->input('typeOfClasp');
        $createFeature->clasp_material_id = $request->input('claspMaterial');
        $createFeature->case_material_id = $request->input('caseMaterial');
        $createFeature->case_mm_id = $request->input('caseMM');
        $createFeature->water_resisitance_depth_id = $request->input('wrd');
        $createFeature->glass_type_id = $request->input('glassType');
        $createFeature->chronograph_id = $request->input('chronograph');
        $createFeature->tourbillion_id = $request->input('tourbillion');
        $createFeature->gmt_id = $request->input('gmt');
        $createFeature->annual_calender_id = $request->input('annualCalender');
        $createFeature->minute_repeater_id = $request->input('minuteRepeater');
        $createFeature->double_chronograph_id = $request->input('doubleChronograph');
        $createFeature->panorma_date_id = $request->input('panormaDate');
        $createFeature->jumping_hour_id = $request->input('jumpingHour');
        $createFeature->alarm_id = $request->input('alarm');
        $createFeature->year_id = $request->input('year');
        $createFeature->day_id = $request->input('day');
        $createFeature->dial_color = DialColorModel::find($request->input('dialColor'))->dial_color;
        $createFeature->bracelet_material = BraceletMaterialModel::find($request->input('braceletMaterial'))->bracelet_material;
        
        $createFeature->save();
        
        return redirect('/admin/createwatchfeature/'.$request->input('listing_id'))->with('success_msg', 'Feature Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $listing_id = $id;
        $details = WatchFeaturesModel::where('listing_id', '=', $listing_id)
        ->get();
        
        return view('admin.watch.features.index', compact('details','listing_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feature_data =WatchFeaturesModel::find($id);
        
        $power_reserve = DialPowerReserveModel::get();
        $bezel_material = WatchBezelModel::get();
        $dial_color = DialColorModel::get();
        $bracelet_material = BraceletMaterialModel::get();
        $bracelet_color = BraceletColorModel::get();
        $type_of_clasp = BraceletClaspTypeModel::get();
        $clasp_material = BraceletClaspMaterialModel::get();
        $case_material = CaseMaterialModel::get();
        $case_mm = CaseMMModel::get();
        $wrd = CaseWaterRDModel::get();
        $glass_type = CaseGlassTypeModel::get();
        $chronograph = FunctionChronographModel::get();
        $tourbillion = FunctionTourbillionModel::get();
        $gmt = FunctionGMTModel::get();
        $annual_calender = FunctionAnnualCalenderModel::get();
        $minute_repeater = FunctionMinuteRepeaterModel::get();
        $double_chronograph = FunctionDoubleChronographModel::get();
        $panorma_date = FunctionPanormaDateModel::get();
        $jumping_hour = FunctionJumpingHourModel::get();
        $alarm = FunctionAlarmModel::get();
        $year = FunctionYearModel::get();
        $day = FunctionDayModel::get();
        
        $powerReserveSelected = DialPowerReserveModel::find($feature_data->power_reserve_id);
        $bezelMaterialSelected = WatchBezelModel::find($feature_data->bezel_material_id);
        $dialColorSelected = DialColorModel::find($feature_data->dial_color_id);
        $braceletMaterialSelected = BraceletMaterialModel::find($feature_data->bracelet_material_id);
        $braceletColorSelected = BraceletColorModel::find($feature_data->bracelet_color_id);
        $typeOfClaspSelected = BraceletClaspTypeModel::find($feature_data->type_of_clasp_id);
        $claspMaterialSelected = BraceletClaspMaterialModel::find($feature_data->clasp_material_id);
        $caseMaterialSelected = CaseMaterialModel::find($feature_data->case_material_id);
        $caseMMSelected = CaseMMModel::find($feature_data->case_mm_id);
        $wrdSelected = CaseWaterRDModel::find($feature_data->water_resisitance_depth_id);
        $glassTypeSelected = CaseGlassTypeModel::find($feature_data->glass_type_id);
        $chronographSelected = FunctionChronographModel::find($feature_data->chronograph_id);
        $tourbillionSelected = FunctionTourbillionModel::find($feature_data->tourbillion_id);
        $gmtSelected = FunctionGMTModel::find($feature_data->gmt_id);
        $annualCalenderSelected = FunctionAnnualCalenderModel::find($feature_data->annual_calender_id);
        $minuteRepeaterSelected = FunctionMinuteRepeaterModel::find($feature_data->minute_repeater_id);
        $doubleChronographSelected = FunctionDoubleChronographModel::find($feature_data->double_chronograph_id);
        $panormaDateSelected = FunctionPanormaDateModel::find($feature_data->panorma_date_id);
        $jumpingHourSelected = FunctionJumpingHourModel::find($feature_data->jumping_hour_id);
        $alarmSelected = FunctionAlarmModel::find($feature_data->alarm_id);
        $yearSelected = FunctionYearModel::find($feature_data->year_id);
        $daySelected = FunctionDayModel::find($feature_data->day_id);
        
       
        
        return view('admin.watch.features.edit',
            compact('bezel_material','feature_data','power_reserve', 'dial_color', 'bracelet_material', 'bracelet_color','type_of_clasp', 'clasp_material', 'case_material',
                'case_mm', 'wrd', 'glass_type','chronograph','tourbillion', 'gmt', 'annual_calender', 'minute_repeater', 'double_chronograph', 'panorma_date',
                'jumping_hour', 'alarm', 'year', 'day', 'powerReserveSelected', 'dialColorSelected', 'braceletMaterialSelected','braceletColorSelected','typeOfClaspSelected',
                 'claspMaterialSelected','caseMaterialSelected', 'caseMMSelected', 'wrdSelected', 'glassTypeSelected','chronographSelected','tourbillionSelected','gmtSelected'
                  ,'annualCalenderSelected','minuteRepeaterSelected', 'doubleChronographSelected', 'panormaDateSelected','jumpingHourSelected', 'alarmSelected','yearSelected',
                  'daySelected','bezelMaterialSelected'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            
            
            'listing_id' => ['required']
        ]);
        
        $createFeature = WatchFeaturesModel::find($id);
        
        $createFeature->listing_id = $request->input('listing_id');
        $createFeature->bezel_material_id = $request->input('bezelMaterial');
        $createFeature->power_reserve_id = $request->input('powerReserve');
        $createFeature->dial_color_id = $request->input('dialColor');
        $createFeature->bracelet_material_id = $request->input('braceletMaterial');
        $createFeature->bracelet_color_id = $request->input('braceletColor');
        $createFeature->type_of_clasp_id = $request->input('typeOfClasp');
        $createFeature->clasp_material_id = $request->input('claspMaterial');
        $createFeature->case_material_id = $request->input('caseMaterial');
        $createFeature->case_mm_id = $request->input('caseMM');
        $createFeature->water_resisitance_depth_id = $request->input('wrd');
        $createFeature->glass_type_id = $request->input('glassType');
        $createFeature->chronograph_id = $request->input('chronograph');
        $createFeature->tourbillion_id = $request->input('tourbillion');
        $createFeature->gmt_id = $request->input('gmt');
        $createFeature->annual_calender_id = $request->input('annualCalender');
        $createFeature->minute_repeater_id = $request->input('minuteRepeater');
        $createFeature->double_chronograph_id = $request->input('doubleChronograph');
        $createFeature->panorma_date_id = $request->input('panormaDate');
        $createFeature->jumping_hour_id = $request->input('jumpingHour');
        $createFeature->alarm_id = $request->input('alarm');
        $createFeature->year_id = $request->input('year');
        $createFeature->day_id = $request->input('day');
        $createFeature->dial_color = DialColorModel::find($request->input('dialColor'))->dial_color;
        $createFeature->bracelet_material = BraceletMaterialModel::find($request->input('braceletMaterial'))->bracelet_material;
        
        $createFeature->save();
        
        return redirect('/admin/createwatchfeature/'.$request->input('listing_id'))->with('success_msg', 'Feature Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = WatchFeaturesModel::find($id);
        $listing_id = $comment->listing_id;
        
        $deleteRecords = WatchFeaturesModel::find($id)->delete();
        return redirect('/admin/createwatchfeature/'.$listing_id)->with('success_msg', 'Watch Deleted successfully!');
    }
}
