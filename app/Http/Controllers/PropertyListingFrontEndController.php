<?php

namespace App\Http\Controllers;

use App\CityModel;
use App\ContactusModel;
use App\CountryModel;
use App\CurrencyModel;
use App\NoOfBathroomsModel;
use App\NoOfBedroomsModel;
use App\PropertyImageModel;
use App\PropertyTypeModel;
use App\StateModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\PropertyDataModel;
use App\AgenciesImageModel;
use App\PropertySubmitSuggestPrice;
use App\UserDetailsModel;
use App\TopPropertyTypeModel;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Mail\ListingEnquiry;

class PropertyListingFrontEndController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contactUs = ContactusModel::get()->first();
        $images = PropertyImageModel::get();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        $propertyType = PropertyTypeModel::get();
        
        $propertyListing = PropertyDataModel::select('re_property_details.id as id', 'currency_code' ,'ad_title', 'land_size as size','property_price as value', 'number_of_bedrooms as bedrooms',
            'metric', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode','tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id')
            ->join('tbl_currency', 'tbl_currency.id', '=', 're_property_details.currency_id')
            ->join('tbl_city', 'tbl_city.id', '=', 're_property_details.city_id')
            ->join('tbl_country', 'tbl_country.id', '=', 're_property_details.country_id')
            ->join('tbl_agent', 'tbl_agent.id', '=', 're_property_details.agent_id')
            ->join('re_build_year', 're_build_year.id', '=', 're_property_details.year_built_id')
            ->join('re_beds', 're_beds.id', '=', 're_property_details.bed_id')
            ->whereIN('re_property_details.id', function($query){
                $query->select('listing_id')
                ->from('re_property_images');
            })
             ->whereIN('re_property_details.id', function($query){
             $query->select('listing_id')
             ->from('re_property_features');
             }) 
                ->whereIN('re_property_details.id', function($query){
                    $query->select('listing_id')
                    ->from('re_property_comment');
                })->where('re_property_details.status','=', '1')
                ->paginate(2);
                
                
        $agenciesImages = AgenciesImageModel::get();
        $propertyType_data = PropertyTypeModel::get();
        $bedroom_data = NoOfBedroomsModel::get();
        $bathroom_data = NoOfBathroomsModel::get();
        $price_max = currency()->convert(floatval(PropertyDataModel::max('property_price') + 1000), "USD", currency()->getUserCurrency() ,false);
        
        $country_data = CountryModel::orderBy('country_name','ASC')->get();
        
        
        return view('tangiblehtml.property-listing', compact('propertyType','contactUs','propertyListing', 'images', 'propertyType_data', 'bathroom_data','bedroom_data', 'country_data','price_max', 'agenciesImages'));
    }
    
    public function searchFilter(Request $request)
    {
        //print_r($request->all());
        $contactUs = ContactusModel::get()->first();
        $images = PropertyImageModel::get();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        
        $price_max = currency()->convert(floatval(PropertyDataModel::max('property_price') + 1000), "USD", currency()->getUserCurrency() ,false);
        
        $propertyType = PropertyTypeModel::get();
        
        $params = $request->all();
        $homeSelected = [];
        $countrySelected = [];
        $condition ="";
        $sortByParam = "ASC";
        $sortByField = "id";
        
        $inputPurchaseType = $request->purchaseType;
        
        $queryPropertyType = TopPropertyTypeModel::where('property_type', '=' ,$request->propertyType)->first();
        
        foreach($params as $param=>$value){
            
            if(strpos($param, 'home') !== false){
                
                array_push($homeSelected,$value);
            } if(strpos($param, 'country') !== false){
                
                array_push($countrySelected,$value);
            } 
        }
        
        $searchTerm = explode("/",$request->input('search'))[0];
        
        $state = StateModel::select('id')->where('state_name', '=', $searchTerm)->first();
        $state_id = "";
        if (! empty($state))
            $state_id = $state->id;
            
        $city = CityModel::select('id')->where('city_name', '=', $searchTerm)->first();
        $city_id = "";
        if (! empty($city))
             $city_id = $city->id;

        
        if($request->condition1 != null){
            $condition = $request->condition1;
        }
        
        $bedMin = "";
        $bedMax =  "";
        
        if($request->minbeds != null){
            $bedMin = $request->minbeds;
        }if($request->maxbeds != null){
            $bedMax = $request->maxbeds;
        }
        
        $bathMin = "";
        $bathMax =  "";
        
        if($request->minbath != null){
            $bathMin = $request->minbath;
        }if($request->maxbath != null){
            $bathMax = $request->maxbath;
        }
        
        $priceMin = "";
        $priceMax = "";
        
        if($request->minPrice != null){
            $priceMin = currency()->convert(floatval($request->minPrice), currency()->getUserCurrency(), "USD" ,false);
            
            
        }if($request->maxPrice != null){
            $priceMax = currency()->convert(floatval($request->maxPrice), currency()->getUserCurrency(), "USD" ,false);
        }
        
        $sortBy = "";
        
        if($request->sortBy !=null){
            if($request->sortBy == 1){
                $sortByField = 'value';
                $sortByParam = 'DESC';
                $sortBy = $request->sortBy; 
            }
            if($request->sortBy == 2){
                $sortByField = 'value';
                $sortByParam = 'ASC';
                $sortBy = $request->sortBy;
            }
            if($request->sortBy == 3){
                $sortByField = 're_property_details.created_at';
                $sortByParam = 'DESC';
                $sortBy = $request->sortBy;
            }
        }
        
        //DB::enableQueryLog();
        $propertyListing = PropertyDataModel::select('re_property_details.id as id', 'currency_code' ,'ad_title', 'land_size as size','property_price as value', 'number_of_bedrooms as bedrooms',
            'metric', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode','tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id', 're_property_details.status as status', 'tbl_agencies.company_name as agency_name')
            ->join('tbl_currency', 'tbl_currency.id', '=', 're_property_details.currency_id')
            ->join('tbl_city', 'tbl_city.id', '=', 're_property_details.city_id')
            ->join('tbl_country', 'tbl_country.id', '=', 're_property_details.country_id')
            ->join('tbl_agent', 'tbl_agent.id', '=', 're_property_details.agent_id')
            ->leftJoin('tbl_agencies', 'tbl_agencies.id', '=', 'tbl_agent.agency_id')
            ->join('re_build_year', 're_build_year.id', '=', 're_property_details.year_built_id')
            ->join('re_beds', 're_beds.id', '=', 're_property_details.bed_id')
            ->join('re_bathrooms', 're_bathrooms.id', '=', 're_property_details.bathroom_id')
            ->whereIN('re_property_details.id', function($query){
                $query->select('listing_id')
                ->from('re_property_images');
            })
            ->whereIN('re_property_details.id', function($query){
                $query->select('listing_id')
                ->from('re_property_features');
            })
            ->whereIN('re_property_details.id', function($query){
                $query->select('listing_id')
                ->from('re_property_comment');
            })->where(function($query) use($homeSelected) {
                foreach($homeSelected as $term) {
                    $query->orWhere('property_type_id', '=', $term);
                };
            })->where(function($query) use($state_id) {
                if($state_id != null)
                    $query->orWhere('re_property_details.state_id', '=', $state_id);
            })->where(function($query) use($city_id) {
                if($city_id != null)
                    $query->orWhere('re_property_details.city_id', '=', $city_id);
            })->where(function($query) use($condition) {
                if($condition != null)
                    $query->orWhere('establishment_type', '=', $condition);
            })->where(function($query) use($bedMin,$bedMax) {
                if($bedMin != null || $bedMax != null){
                    
                    $query->whereBetween('number_of_bedrooms', [
                        $bedMin,
                        $bedMax+1
                    ]);
                }
            })->where(function($query) use($bathMin,$bathMax) {
                if($bathMin != null || $bathMax != null){
                    
                    $query->whereBetween('number_of_bathrooms', [
                        $bathMin,
                        $bathMax+1
                    ]);
                }
            })->where(function($query) use($priceMin,$priceMax,$price_max) {
                if(($priceMin != null && $priceMin > 0) || ($priceMax != null && $priceMax < $price_max)){
                    
                    $query->whereBetween('property_price', [
                        $priceMin,
                        $priceMax+1
                    ])->orWhere('property_price', NULL);
                }
            })->where(function($query) use($countrySelected) {
                foreach($countrySelected as $term) {
                    $query->orWhere('re_property_details.country_id', '=', $term);
                };
            })->where(function($query) use($queryPropertyType) {
                if($queryPropertyType != null)
                    $query->orWhere('re_property_details.top_property_type_id', '=', $queryPropertyType->id);
            })->where(function($query) use($inputPurchaseType) {
                if($inputPurchaseType != null)
                    $query->orWhere('re_property_details.property_sale_lease', '=', $inputPurchaseType);
            })->where('re_property_details.status','=', '1')
            ->orderBy($sortByField, $sortByParam)
            ->paginate(2);
            
            //dd($propertyListing);
            //echo sizeof($propertyListing);
            //dd(DB::getQueryLog());
            
            $agenciesImages = AgenciesImageModel::get();
            $propertyType_data = PropertyTypeModel::get();
            $bedroom_data = NoOfBedroomsModel::get();
            $bathroom_data = NoOfBathroomsModel::get();
            $country_data = CountryModel::orderBy('country_name','ASC')->get();
            
            $priceMin = $request->minPrice;
            $priceMax = $request->maxPrice;
            
            return view('tangiblehtml.property-listing', compact('sortBy','propertyType','contactUs','propertyListing', 'images', 'propertyType_data', 'bathroom_data','bedroom_data', 'country_data'
                ,'price_max', 'searchTerm', 'homeSelected', 'countrySelected', 'condition','bedMin','bedMax','bathMin', 'bathMax', 'priceMin', 'priceMax', 'agenciesImages'));
    }
    
    public function autocompleteSearch(Request $request)
    {
        $searchTerm = $request->input('search');
        $states = StateModel::select('tbl_state.id as id', 'state_name', 'country_name')
        ->join('tbl_country', 'tbl_country.id', '=', 'tbl_state.country_id')
        ->where('state_name', 'like', '%' . $searchTerm . '%')->get();
        
        $cities = CityModel::select('tbl_state.id as id', 'city_name', 'state_name', 'country_name')
        ->leftjoin('tbl_state', function($join){
            $join->on('tbl_state.id','=','tbl_city.state_id');
            $join->orOn('tbl_state.id','=','tbl_city.state_id', '=', 0);
        })->join('tbl_country', 'tbl_country.id', '=', 'tbl_city.country_id')
        ->where('city_name', 'like', '%' . $searchTerm . '%')->get();
        
        $response = array();
        foreach ($states as $state) {
            $response[] = array(
                "value" => $state->id,
                "label" => $state->state_name,
                "finalLabel" => $state->state_name
            );
        }
        
        foreach ($cities as $city) {
            
            $finalLabel = '';

            $finalLabel = $city->city_name; 
                
                    
                    $response[] = array(
                        "value" => $city->id,
                        "label" => $city->city_name,
                        "finalLabel" => $finalLabel
                    );
        }
        
        return response()->json($response);
    }
    
    public function sortBy(Request $request){
        
        $contactUs = ContactusModel::get()->first();
        $images = PropertyImageModel::get();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        
        $propertyListing = PropertyDataModel::select('re_property_details.id as id', 'currency_code' ,'ad_title', 'land_size as size','property_price as value', 'number_of_bedrooms as bedrooms',
            'metric', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode','tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id')
            ->join('tbl_currency', 'tbl_currency.id', '=', 're_property_details.currency_id')
            ->join('tbl_city', 'tbl_city.id', '=', 're_property_details.city_id')
            ->join('tbl_country', 'tbl_country.id', '=', 're_property_details.country_id')
            ->join('tbl_agent', 'tbl_agent.id', '=', 're_property_details.agent_id')
            ->join('re_build_year', 're_build_year.id', '=', 're_property_details.year_built_id')
            ->join('re_beds', 're_beds.id', '=', 're_property_details.bed_id')
            ->whereIN('re_property_details.id', function($query){
                $query->select('listing_id')
                ->from('re_property_images');
            })
            ->whereIN('re_property_details.id', function($query){
                $query->select('listing_id')
                ->from('re_property_features');
            })
            ->whereIN('re_property_details.id', function($query){
                $query->select('listing_id')
                ->from('re_property_comment');
            })->where('re_property_details.status','=', '1')
            ->get();
            
            //dd($watchListing);
            
            return view('tangiblehtml.property-listing', compact('contactUs','propertyListing', 'images'));
    }
    
    public function suggestPrice($id){
        
        $contactUs = ContactusModel::get()->first();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        currency()->setUserCurrency($selectedCurrency->code);
        $country_data = CountryModel::get();
        $city_data = CityModel::get();
        $category = "property";
        $userDetails = UserDetailsModel::get();
        
        //dd();
        $listing = $property = PropertyDataModel::select('property_type','currency_code','metric','number_of_bathrooms','agent_id','number_of_bedrooms','re_property_details.id as id','ad_title','land_size' ,DB::raw("CONCAT(re_property_details.street_1, ' ', re_property_details.street_2) AS address"),'re_property_details.postal_code as postal_code','property_price','tbl_agent.first_name as agentFirstName','tbl_agent.last_name as agentLastName', 'tbl_agent.mobile as agentMobile',
            'tbl_agent.whatsapp as agentWhatsapp','tbl_agent.office as agentOffice','tbl_agent.fax as agentFax', 'tbl_agent.email as agentEmail', 'comment', 're_price.price as priceType', 'property_price as value', 'agent_id')
            ->join('tbl_currency', 'tbl_currency.id', '=', 're_property_details.currency_id')
            ->join('re_bathrooms', 're_bathrooms.id', '=', 're_property_details.bathroom_id')
            ->join('re_beds', 're_beds.id', '=', 're_property_details.bed_id')
            ->join('tbl_agent','tbl_agent.id', '=', 're_property_details.agent_id')
            ->join('re_property_comment','re_property_comment.listing_id', '=', 're_property_details.id')
            ->join('re_property_type', 're_property_type.id', '=', 're_property_details.property_type_id')
            ->leftJoin('re_price','re_price.id', '=', 're_property_details.price_id')
            ->find($id);
        
            $images = PropertyImageModel::where('listing_id', '=', $property->id)->get();
            
            return view('tangiblehtml.suggest-price', compact('contactUs', 'listing', 'images', 'country_data', 'category', 'userDetails', 'city_data'));
    }
    
    public function submitSuggestPrice(Request $request){
        
        $this->validate($request, [
            'phoneNumber' => "required",
            'address' => "required",
            'country' => "required",
            'city' => "required",
            'postalCode' => "required",
            'suggestedPrice' => "required"
        ]);
        
        $updateUser = UserDetailsModel::where('user_id', '=', $request->userId)->first();
        
        if($updateUser != null){
            $updateUser->user_id = $request->userId;
            $updateUser->user_postalCode = $request->postalCode;
            $updateUser->user_mobile = $request->phoneNumber;
            $updateUser->city_id = $request->city;
            $updateUser->country_id = $request->country;
            $updateUser->user_address = $request->address;
            
            $updateUser->save();
        }else{
            $createUser = new UserDetailsModel();     
            
            $createUser->user_id = $request->userId;
            $createUser->user_postalCode = $request->postalCode;
            $createUser->user_mobile = $request->phoneNumber;
            $createUser->city_id = $request->city;
            $createUser->country_id = $request->country;
            $createUser->user_address = $request->address;
            
            $createUser->save();
        }
        
        $suggestPrice = new PropertySubmitSuggestPrice();
        $suggestPrice->product_id = $request->productId;
        $suggestPrice->user_Id = $request->userId;
        $suggestPrice->agent_Id = $request->agentId;
        $suggestPrice->submit_price = $request->suggestedPrice;
        $suggestPrice->shipping_cost = $request->shippingPrice;
        
        $suggestPrice->save();
        
        return back()->with('success_msg','Make an Offer request send successfully');
    }
    
    public function sendenquery(Request $request){
        $this->validate($request, [
            'name'  => 'required|min:3',
            'mobile'  => 'required|min:10',
            'message'  => 'required'
        ]);
        
        $agent = AgentModel::find($request->agentId);
        
        $enqury = EnquiryModel::where('userID', '=', $request->userId)->where('productId', '=', $request->productid)->where('Category', '=', 'RealEstate')->first();
        
        if($enqury == null){
            $enqury = new EnquiryModel();
            $enqury->name = $request->name;
            $enqury->phone = $request->mobile;
            $enqury->country = $request->country;
            $enqury->queryType = $request->querytype;
            $enqury->message = $request->message;
            $enqury->Category = 'RealEstate';
            $enqury->productId = $request->productid;
            $enqury->agent_id = $request->agentId;
            $enqury->agencies_id = $agent->agency_id;
            $enqury->userID = $request->userId;
            $enqury->agent_userid = $agent->userId;
            
            $enqury->save();
        }
        
        
        $suggestPrice = SubmitSuggestPriceModel::where('user_id', '=', $request->userId)->where('product_id', '=', $request->productid)->where('Category', '=', 'RealEstate')->first();
        
        if($suggestPrice == null){
            $suggestPrice = new SubmitSuggestPriceModel();
            $suggestPrice->id = $enqury->id;
            $suggestPrice->product_id = $request->productid;
            $suggestPrice->user_Id = $request->userId;
            $suggestPrice->agent_Id = $request->agentId;
            $suggestPrice->category = 'RealEstate';
            $suggestPrice->submit_price = $request->suggestedPrice;
            $suggestPrice->shipping_cost = $request->shippingPrice;
            $suggestPrice->agent_userid = $agent->userId;
            
            $suggestPrice->save();
            
        }
        
        $productorder = ProductOrderModel::where('userID', '=', $request->userId)->where('productId', '=', $request->productid)->where('Category', '=', 'RealEstate')->first();
        
        if($productorder == null){
            
            $order = new ProductOrderModel();
            $order->id = $enqury->id;
            $order->name = $request->name;
            $order->user_emailid = $request->email;
            $order->phone = $request->mobile;
            $order->country = $request->country;
            $order->city = $request->city;
            $order->category = 'RealEstate';
            $order->productId = $request->productid;
            $order->agencies_id = $agent->agencies_id;
            $order->agent_id = $request->agentId;
            $order->userId = $request->userId;
            $order->agent_userid = $agent->userId;
            $order->order_status = 'waiting for approval';
            
            $order->save();
        }
        
        
        $seller_category = 'private_seller';
        if($agent->agency_id != null)
            $seller_category = 'agent';
            
            $myMessage = new MyMessageModel();
            $myMessage->message = $request->message;
            $myMessage->message_id = $enqury->id;
            $myMessage->queryType = 'enquiry';
            $myMessage->category = 'RealEstate';
            $myMessage->agent_id = $request->agentId;
            $myMessage->product_id = $request->productid;
            $myMessage->messageFrom = $request->userId;
            $myMessage->messageTo = $agent->userId;
            
            $myMessage->save();
            
            $user = User::find($request->userId);
            
            Mail::to('priyank@optimalvirtualemployee.com')->send(new ListingEnquiry($user, $agent,$enqury ));
            
            /*Mail::send('email.contact-seller', [ 'user' =>$user ,'agent' =>  $agent, 'enqury' =>$enqury], function($message) {
                $message->to('priyank@optimalvirtualemployee.com', 'Priyank Sharma')->subject
                ('Submit Price Request');
                $message->from('pashu.sharma@gmail.com','Tangible Listing');
            });*/
            
            return back()->with('success_msg','Enquiry send Successfully');
        
    }
    
}
