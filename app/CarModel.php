<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//defined car model here
class CarModel extends Model
{
    //defined car model details here
    protected $table = 'au_brands';
}
