<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnquiryModel extends Model
{
    protected $table = 'tbl_enquiry';

    public function customer()
    {
        return $this->belongsTo('App\User', 'userID', 'id');
    }

    public function agent()
    {
        return $this->belongsTo('App\User', 'agent_userid', 'id');
    }

    public function agentTable()
    {
        return $this->belongsTo('App\AgentModel', 'agent_id', 'id');
    }



    public function createBy()
    {
        return $this->belongsTo('App\User', 'listingCreatedBy', 'id');
    }

    public function watchDetail()
    {
        return $this->belongsTo('App\WatchDataListingModel', 'productId', 'id');
    }
}
