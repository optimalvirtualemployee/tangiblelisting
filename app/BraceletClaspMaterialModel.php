<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BraceletClaspMaterialModel extends Model
{
    protected $table = 'wa_factory_features_bracelet_claspMaterial';
    protected $fillable = ['clasp_material', 'status'];
}
