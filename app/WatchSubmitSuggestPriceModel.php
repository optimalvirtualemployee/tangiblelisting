<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchSubmitSuggestPriceModel extends Model
{
    protected $table = 'watch_submitpricerequest';
}
