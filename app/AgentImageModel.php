<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentImageModel extends Model
{
    protected $table = 'tbl_agent_images';
}
