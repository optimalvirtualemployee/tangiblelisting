<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchDataListingModel extends Model
{
    protected $table = 'wa_watch_detail';

    public function country()
    {
        return $this->belongsTo('App\CountryModel', 'country_id', 'id');
    }
}
