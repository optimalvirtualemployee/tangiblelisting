<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FunctionJumpingHourModel extends Model
{
    protected $table = 'wa_factory_features_function_jumpingHour';
    protected $fillable = ['jumping_hour', 'status'];
}
