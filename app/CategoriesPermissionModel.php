<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriesPermissionModel extends Model
{
    protected $table = 'tbl_category_permission';
}
