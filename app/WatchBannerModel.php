<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchBannerModel extends Model
{
    protected $table = 'wa_banner_images';
}
