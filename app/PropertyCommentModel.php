<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyCommentModel extends Model
{
    protected $table = 're_property_comment';
}
