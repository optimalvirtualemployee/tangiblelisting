<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseWaterRDModel extends Model
{
    protected $table = 'wa_factory_features_case_waterRD';
    protected $fillable = ['water_resistant_depth', 'status'];
}
