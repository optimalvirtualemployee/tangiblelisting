<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchContractedDetailsModel extends Model
{
    protected $table = 'wa_customer_product-details';
}
