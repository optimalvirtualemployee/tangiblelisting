<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyContractedDetailsModel extends Model
{
    protected $table = 're_customer_product-details';
}
