<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InclusionModel extends Model
{
    protected $table = 'wa_inclusion';
    protected $fillable = ['inclusion', 'status'];
}
