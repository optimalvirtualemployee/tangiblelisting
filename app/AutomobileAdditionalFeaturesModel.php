<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobileAdditionalFeaturesModel extends Model
{
    protected $table = 'au_automobile_additional_features';
}
