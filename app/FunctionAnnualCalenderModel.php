<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FunctionAnnualCalenderModel extends Model
{
    protected $table = 'wa_factory_features_function_annualCalender';
    protected $fillable = ['annual_calender', 'status'];
}
