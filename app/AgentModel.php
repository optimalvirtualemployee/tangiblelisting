<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentModel extends Model
{
    protected $table = 'tbl_agent';


    public function agency()
    {
        return $this->belongsTo('App\AgenciesModel', 'agency_id', 'id');
    }

    public function img()
    {
        return $this->belongsTo('App\AgentImageModel', 'listing_id', 'id');
    }

}
