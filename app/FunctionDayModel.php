<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FunctionDayModel extends Model
{
    protected $table = 'wa_factory_features_function_day';
    protected $fillable = ['day', 'status'];
}
