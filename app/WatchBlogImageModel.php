<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchBlogImageModel extends Model
{
    protected $table = 'wa_blog_images';
}
