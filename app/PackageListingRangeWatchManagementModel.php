<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageListingRangeWatchManagementModel extends Model
{
    protected $table = 'tbl_package_pricing_range_private_seller_watch';
}
