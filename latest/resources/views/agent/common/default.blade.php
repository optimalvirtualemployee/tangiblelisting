@include('agent.common.header')
<body>
@include('agent.common.sidebar')
  <!--Main Navigation-->
    <!--Main layout-->
    @yield('content')
  <!--Main layout-->
@include('agent.common.footer')