<!DOCTYPE html>
<html>

@include('agent.common.multi-step-header')
<body>
@include('agent.common.sidebar')
  <!--Main Navigation-->
    <!--Main layout-->
    @yield('content')
  <!--Main layout-->
@include('agent.common.multi-step-property')