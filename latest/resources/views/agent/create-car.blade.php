@extends('agent.common.multi-step-default-car')
@section('title', 'Add Auto')
@section('content')

<style>
#loading-image{
  position:fixed;
  top:0px;
  right:0px;
  width:100%;
  height:100%;
  background-color:#666;
  background-image:url('/assets/img/Spinner-loader.gif');
  background-repeat:no-repeat;
  background-position:center;
  z-index:10000000;
  opacity:0.6;
}

 .slick-slide img {
    max-height: 300px;
    width: auto;
  }
  #imgGallery div{
    position: relative;
    width: 150px;
    height: 150px;
    margin: 5px;
    border: 1px solid #e7e7e7;
  }
  #imgGallery div img{
    max-height: 150px;
    width: 100%;
    height: 100%;
    object-fit: cover;
    position: absolute;
   
  }
  .avatar-upload {
  position: relative;
  max-width: 205px;
  border: 2px dotted #bfc8d9;
}
.avatar-upload .avatar-edit {
  position: absolute;
  right: 12px;
  z-index: 1;
  top: 10px;
}
.avatar-upload .avatar-edit input {
  display: none;
}
.avatar-upload .avatar-edit input + label {
  display: inline-block;
  width: 34px;
  height: 34px;
  margin-bottom: 0;
  border-radius: 100%;
  background: #FFFFFF;
  border: 1px solid transparent;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
  cursor: pointer;
  font-weight: normal;
  transition: all 0.2s ease-in-out;
}
.avatar-upload .avatar-edit input + label:hover {
  background: #f1f1f1;
  border-color: #d6d6d6;
}
.avatar-upload .avatar-edit input + label:after {
  content: "\f040";
  font-family: 'FontAwesome';
  color: #757575;
  position: absolute;
  top: 10px;
  left: 0;
  right: 0;
  text-align: center;
  margin: auto;
}
.avatar-upload .avatar-preview {
  width: 192px;
  height: 192px;
  position: relative;
  border-radius: 100%;
  border: 6px solid #F8F8F8;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
  margin: 0 auto;
}
.avatar-upload .avatar-preview > div {
  width: 100%;
  height: 100%;
  border-radius: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
}
.security-photos{
  background-color: #fcfcfc;
    padding: 10px;
}
.previewContainer{
  background-color: #fff;
    box-shadow: 1px 1px 1px 1px #80808040;
    padding: 25px;
}
</style>
<body>
    <section class="become-seller-from overlay-wrapper">
        <div class="container">
            <div class="row">                
                <div class="col-lg-12">
                    <!-- MultiStep Form -->
<div class="container-fluid" id="grad1">
    <div class="row justify-content-center mt-0">
        <div class="col-11  col-lg-12 text-center p-0 mt-3 mb-2">
            <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                <!-- <h2><strong>Sign Up Your User Account</strong></h2>
                <p>Fill all form field to go to next step</p> -->
                <div class="row">
                    <div class="col-md-12 mx-0">
                        <form id="msform" action="{{ url('agent/addAutoData') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="agentId" id="agentId" value="{{Auth::user()->id}}">
                            <!-- progressbar -->
                            <ul id="progressbar">
                                <li class="active" id="account"><strong>Select Car</strong></li>
                                <li id="personal"><strong>Add Details</strong></li>
                                <li id="payment"><strong>Attach Photos</strong></li>
                                <li id="confirm"><strong>Review & Submit</strong></li>
                            </ul> <!-- fieldsets -->
                            <fieldset>
                                <div class="form-card">
                                	<!-- @Auth
                                    <h2 class="fs-title">Welcome, {{ Auth::user()->first_name . ' '. Auth::user()->last_name  }}</h2>
                                    @endauth -->  
                                    <p><b>What type of Car you Selling?</b></p>
                                    <div class="row">
                                        <div class="col-4"> 
                                            <select class="list-car form-control" id="make1" name="make1" >
                                            <option value="">Select Make</option>
                                            @foreach($makes as $make)
                                            <option value="{{$make->id}}">{{$make->automobile_brand_name}}</option>
                                            @endforeach                                       
                                            </select> 
                                        </div>
                                        <div class="col-4"> 
                                            <select class="list-car form-control" id="model" name="model" >
                                            <option value="">Select Model</option>
                                            </select> 
                                        </div>
                                        <div class="col-4"> 
                                            <select class="list-car form-control" id="year" name="year">
                                            <option value="">Build Year</option>
                                            @foreach($buildYear as $year)
                                            <option value="{{$year->id}}">{{$year->build_year}}</option>
                                            @endforeach                                       
                                            </select> 
                                        </div>
                                    </div>
                                    
                                </div> <input type="button" name="next" class="next action-button" id ="next1" value="Next Step" />
                            </fieldset>
                            <fieldset>
                                <div class="form-card">
                                    <div class="">
                                        <h2 class="fs-title">Tell us more about your CAR</h2> 
                                         <div class="row m-0">
                                         <div class="form-group">
                                                    <label class="form-control-label bold_text" >Vehicle Location</label>
                                                  </div>
                                         </div>         
                                         <div class="row m-0">
                                        <div class="col-lg-3">
                                              <div class="form-group">
                                                <label class="form-control-label" for="input-last-name">Select Country*</label>
                                                <select class="form-control" data-trigger name="countryId" id="countryId">
                                                    <option  value="">Select Country</option>
                                                    @foreach($country_data as $country)
                                            <option value="{{$country->id}}">{{$country->country_name}}</option>
                                            @endforeach
                                                  </select>
                                              </div>
                                            </div>
                                            <div class="col-lg-3">
                                              <div class="form-group">
                                                <label class="form-control-label" for="input-last-name">Select State</label>
                                                <select class="form-control" data-trigger name="stateId" id="stateId">
                                                    <option  value="">Select State</option>
                                                  </select>
                                              </div>
                                            </div>
                                            <div class="col-lg-3">
                                              <div class="form-group">
                                                <label class="form-control-label" for="input-last-name">Select City*</label>
                                                <select class="form-control" data-trigger name="cityId" id="cityId">
                                                    <option  value="">Select City</option>
                                                  </select>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="row m-0">
                                          <label class="form-control-label">Price</label>
                                          </div>
                                          <div class="row m-0">
                                          <div class="col-lg-4">
                                                <div class="form-group">
                                                  <label class="form-control-label" for="input-last-name">Select Price Type*</label>
                                                  <select class="form-control" data-trigger name="pricetType" id="pricetType">
                                                      <option value="">Select</option>
                                                      @foreach($prices as $price)
                                                    <option value="{{$price->id}}">{{$price->price}}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                              </div>
                                              <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="input-last-name">Currency*</label>
                                                <select class="form-control" data-trigger name="currency" id="currency">
                                                    <option value=""> Select Currency</option>
                                                    @foreach($currency_data as $currency)
                                                    <option value="{{$currency->id}}">{{$currency->currency_code}}</option>
                                                    @endforeach
                        							</select>
                                                  </div>
                                            </div>
                                          <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="input-last-name">Specify a sale price*</label>
                                                <div class="input-group mb-3">                                                    
                                                    <input type="number" id= "price" name = "price" class="form-control" placeholder= "Car Price" aria-label="Amount (to the nearest dollar)">                                                    
                                                  </div>
                                                  </div>
                                            </div>
											</div>
											<div class="row m-0">
											<label class="form-control-label">Vehicle Specifics</label>
											</div>        
											<div class="row m-0">
                                        
                                              <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="condition">Car Condition*</label> 
                                                <select id="condition" class="form-control" name="condition">
                        						<option value="">Select Condition</option>
                       							 <option value="new" {{ old('new/used') == 'new' && old('new/used') != "" ? 'selected' : ''}}>New</option>
                        						 <option value="used" {{ old('new/used') == 'used' && old('new/used') != "" ? 'selected' : ''}}>Used</option>
                      							</select>
                                                </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-last-name">Registration City</label>
                                                        <div class="input-group mb-3">                                                                                                 
                                                    <input type="text" class="form-control" id= "registrationPlace" name= "registrationPlace" placeholder= "Registration City" aria-label="Amount (to the nearest dollar)">                                                    
                                                  </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="registrationPlate">Registration Plate Number*</label> 
                                                <div class="input-group mb-3">                                                                                                 
                                                    <input type="text" class="form-control" id= "registrationPlate" name= "registrationPlate" placeholder = "Registration Plate Number" aria-label="Amount (to the nearest dollar)">                                                    
                                                  </div>
                                                </div>
                                                </div>
                                              
                                         </div>
											                                  
                                          <div class="row m-0">
                                          <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="input-odometer-metric">Odometer Metric*</label>
                                                <select class="form-control" data-trigger name="odometermetric" id="odometermetric">
                                                    <option value=""> Select metric</option>
                                                    <option value="km">KM</option>
                        							<option value="mi">Miles</option>
                        							</select>
                                                
                                                  </div>
                                              </div>
                                          <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="input-last-name">Odometer*</label>
                                                <div class="input-group mb-3">                                                                                                 
                                                    <input type="number" class="form-control" id = "odometer" name= "odometer" placeholder= "Odometer Value" aria-label="Amount (to the nearest dollar)">
                                                  </div>
                                                  </div>
                                              </div>
                                              <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="registrationPlate">Right/Left Hand Drive*</label> 
                                                <select id="rhdorlhd" class="form-control" name="rhdorlhd">
                        						<option value="">Select RHD/LHD</option>
                       							 <option value="rhd" {{ old('rhd/lhd') == 'rhd' && old('rhd/lhd') != "" ? 'selected' : ''}}>RHD</option>
                        						 <option value="lhd" {{ old('rhd/lhd') == 'lhd' && old('rhd/lhd') != "" ? 'selected' : ''}}>LHD</option>
                      							</select>
                                                </div>
                                                </div>
                                          </div>
                                          <div class="row m-0">
                                          <label class="form-control-label">Vehicle Specification</label>
                                            </div>
                                            <div class="row m-0">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                  <label class="form-control-label" for="input-last-name">Exterior Colour*</label>
                                                  <select class="form-control" data-trigger name="color" id="color">
                                                      <option value="">Select</option>
                                                      @foreach($colours as $colour)
                                                    <option value="{{$colour->id}}">{{$colour->colour}}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                              </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                  <label class="form-control-label" for="input-last-name">Interior Colour*</label>
                                                  <select class="form-control" data-trigger name="interior_color" id="interior_color">
                                                      <option value="">Select</option>
                                                      @foreach($interior_colours as $interior_colour)
                                                    <option value="{{$interior_colour->id}}">{{$interior_colour->interior_colour}}</option>
                                                    @endforeach
                                                    </select>
                                                </div>
                                              </div>
                                              <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="no_of_doors">No Of Doors</label>
                                                        <select class="form-control" data-trigger name="no_of_doors" id="no_of_doors">
                                                            <option value="">Select No Of Doors</option>
                                                            @foreach($no_of_doors as $doors)
                                                            <option value="{{$doors->id}}">{{$doors->no_of_doors}}</option>
                                                            @endforeach
                                                          </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-last-name">Body Types*</label>
                                                        <select class="form-control" data-trigger name="body_type" id="body_type">
                                                            <option value="">Select Body Type</option>
                                                            @foreach($body_types as $body_type)
                                                            <option value="{{$body_type->id}}">{{$body_type->body_type}}</option>
                                                            @endforeach
                                                          </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="transmission">Transmission*</label>
                                                        <select class="form-control" data-trigger name="transmission" id="transmission">
                                                            <option value="">Select Transmission</option>
                                                            @foreach($transmissions as $transmission)
                                                            <option value="{{$transmission->id}}">{{$transmission->transmission}}</option>
                                                            @endforeach
                                                          </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="registrationPlate">Engine*</label> 
                                                <div class="input-group mb-3">                                                                                                 
                                                    <input type="text" class="form-control" id= "engine" name= "engine" placeholder = "Engine" aria-label="Amount (to the nearest dollar)">                                                    
                                                  </div>
                                                </div>
                                                </div>
                                              <div class="col-lg-4">
                                                <div class="form-group">
                                                  <label class="form-control-label" for="fuelType">Fuel Type*</label>
                                                  <select class="form-control" data-trigger name="fuelType" id="fuelType">
                                                      <option value="">Select</option>
                                                      @foreach($fuel_types as $fuel)
                                                    <option value="{{$fuel->id}}">{{$fuel->fuel_type}}</option>
                                                    @endforeach
                                                    </select>
                                                </div>                                                
                                               </div>
                                               <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-last-name">Vehicle Identification Number* <a href="#">What's my VIN?</a></label>
                                                        <input type="text" id="VIN" name="VIN" class="form-control" placeholder="Vehicle Identification Number">
                                                        <label>Provide confidence and peace of mind by including a VIN with your ad so that potential buyers can obtain a CarFacts History Report.</label>
                                                    </div>
                                                </div>
                                               
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-last-name">Timezone</label>
                                                        <select class="form-control" data-trigger name="timezone" id="timezone">
                                                            <option value="">Select Timezone</option>
                                                            @foreach($timezone as $zone)
                                                            <option value="{{$zone->id}}">{{$zone->name .' '. $zone->offset}}</option>
                                                            @endforeach
                                                          </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-last-name">Status Of Listing*</label>
                                                        <select id ="status" name="status" class="form-control" >
                          								<option value="" >Select Status</option>
                           								<option value="1" >Active</option>
                           								<option value="0" >Inactive</option>
                        								</select>
                                                    </div>
                                                </div>
                                                                                                
                                          </div>
                                          <div class="row m-0">
                                            <!-- <div class="col-lg-6">
                                                <h5>How can buyers contact you ?</h5>
                                                <div class="form-group">
                                                    <label class="form-control-label" for="input-last-name">Phone Number </label>
                                                    <input type="text" id="phone-number" class="form-control" placeholder="Phone Number">
                                                    <label>Your number won't be shown on the site and all calls to your virtual number will be diverted to this phone number</label>
                                                </div>
                                            </div> -->
                                            <!-- <div class="col-lg-12">
                                                <div class="inner badges">
                                                    <h2>Add Badges to your Ad</h2>
                                                    <p>To assist you with selling your vehicle during COVID-19 episode time, we're introducting Seller's Badges to tell purchasers what additional items you can offer</p>
                                                    <strong >Select services you can offer</strong>
                                                    <div class="mt-3">
                                                        <div class="custom-control custom-control-alternative custom-checkbox">
                                                            <input class="custom-control-input" id="customCheckRegister" type="checkbox">
                                                            <label class="custom-control-label" for="customCheckRegister">
                                                              <span class="text-muted"> I can video chat with potential buyers </span>
                                                            </label>
                                                        </div>
                                                        <div class="custom-control custom-control-alternative custom-checkbox">
                                                            <input class="custom-control-input" id="customCheckRegister1" type="checkbox">
                                                            <label class="custom-control-label" for="customCheckRegister1">
                                                              <span class="text-muted">  I can bring the vehicle to a potential buyer for an inspection </span>
                                                            </label>
                                                        </div>
                                                        <div class="custom-control custom-control-alternative custom-checkbox">
                                                            <input class="custom-control-input" id="customCheckRegister2" type="checkbox">
                                                            <label class="custom-control-label" for="customCheckRegister2">
                                                              <span class="text-muted"> I'm available on weekdays </span>
                                                            </label>
                                                        </div>
                                                        <div class="custom-control custom-control-alternative custom-checkbox">
                                                            <input class="custom-control-input" id="customCheckRegister3" type="checkbox">
                                                            <label class="custom-control-label" for="customCheckRegister3">
                                                              <span class="text-muted"> I'm available on weekends </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                          </div>
                                      </div>
                                    <h2 class="fs-title mt-5">Select Features</h2> 
                                    <div class="accordion-1">
                                        <div class="container">
                                          <div class="row">
                                            <div class="col-md-12 ml-auto">
                                              <div class="accordion my-3" id="accordionExample">
                                                <div class="card p-0 mb-1">
                                                  <div class="card-header" id="headingOne">
                                                    <h5 class="mb-0">
                                                      <button class="btn btn-link w-100 text-primary text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                       Car Features
                                                        <i class="ni ni-bold-down float-right"></i>
                                      
                                                      </button>
                                                    </h5>
                                                  </div>                                      
                                                  <div id="collapseOne"
                                              class="collapse show"
                                              aria-labelledby="headingOne"
                                              data-parent="#accordionExample">
                                              <div class="mt-3">
                                                <div class="row">
                                                  <div class="col-lg-6">
                                                    <div class="inner">
                                                    <?php echo $html1;?>
                                                    </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                    <div class="inner">
                                                    <?php echo $html2;?>
                                                    </div>
                                                    </div>
                                                    </div>
                                                                            
                                              </div>    
                                            </div>
                                                </div>
                                                <div class="card p-0 mb-1">
                                                  <div class="card-header" id="headingTwo">
                                                    <h5 class="mb-0">
                                                      <button class="btn btn-link w-100 text-primary text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                        Additional Car Features
                                                        <i class="ni ni-bold-down float-right"></i>
                                      
                                                      </button>
                                                    </h5>
                                                  </div>
                                                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">                                                    
                                                    <div class="additionalFeature">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                              <tr>
                                                                <th>Àdd features</th>
                                                                <th>Action</th>                                                              
                                                              </tr>
                                                            </thead>
                                                            <tbody>
                                                              <tr>
                                                                <td><input type="text" id="additional-feature"  name="additional_feature[]" class="form-control"></td>
                                                                <td><button class="btn btn-success" type="button" id="addRow"><i class="fa fa-plus"></i></button></td>                                                               
                                                              </tr>                                                            
                                                            </tbody>
                                                          </table>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                     
                                </div> 
                                <div>
                                </div>
                                <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" id="next2" value="Next Step" />
                            </fieldset>
                            <fieldset>
                        <div class="form-card">
                          <h2 class="fs-title">Attach Photos</h2>
                          <p class="muted">Lorem ipsum is a dummy text, Lorem ipsum is a dummy text</p>
                          <div class="row" >
                            <div class="col-lg-12">
                              <div class="inner">
                              
                                <div class="input-field">      
                                  <div class="input-images-1" id="input-images-1" style="padding-top: .5rem;"></div>
                                </div>
                                
                              </div>
                            </div>
                          </div>
                          <div class="row mt-5">
                            <hr class="my-2">
                            <div class="col-lg-12">
                              <div class="inner">
                                <h2 class="fs-title">Damage Photos</h2>
                                <p class="muted">Lorem ipsum is a dummy text, Lorem ipsum is a dummy text</p>
                                <div class="input-field">
                                  <div class="input-images-2" id="damagePhotos" style="padding-top: .5rem;"></div>
                                </div>
                                
                              </div>
                            </div>
                          </div>
                          <div class="row mt-5">   
                            <div class="col-lg-12">
                              <h2 class="fs-title">VIN Photos</h2>  
                              <p class="muted">Attach photos of your vehicle Identification Number </p>
                            </div>                         
                            <div class="col-lg-6">                             
                              <div class="inner security-photos">
                                <div class="row">                                 
                                  <div class="col-lg-6">
                                    <div class="uploadImage" id= "vin1" name="vin1">
                                      <div class="avatar-upload" >
                                        <div class="avatar-edit" >
                                            <input type='file' id="security1" name="security1[]" class="imageUploadCls" accept=".png, .jpg, .jpeg" />
                                            <label for="security1"></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div class="imagePreview" style="background-image: url(images/Icon-Vin-Identification.png);">
                                            </div>
                                        </div>
                                    </div>  
                                    </div>
                                  </div>
                                  <div class="col-lg-6">
                                    <div class="uploadImage" id= "vin2" name="vin2">
                                      <div class="avatar-upload" >
                                        <div class="avatar-edit">
                                            <input type='file' id="security2" name="security2[]" class="imageUploadCls" accept=".png, .jpg, .jpeg" />
                                            <label for="security2"></label>
                                        </div>
                                        <div class="avatar-preview">
                                          <div class="imagePreview" style="background-image: url(images/Icon-Vin-Identification.png);">
                                            </div>
                                        </div>
                                    </div>  
                                    </div>
                                  </div>
                                </div>                     
                                <hr class="my-2">
                              </div>
                            </div>                            
                          </div>
                          <div class="row mt-5">
                            <hr class="my-2">
                            <div class="col-lg-12">
                              <div class="inner">
                                <h2 class="fs-title">Comments</h2>
                                <hr class="my-2">
                                <label>
                                Setting aside the effort to portray special and
                                recognizing highlights about your vehicle can
                                have the effect in catching a purchaser's
                                consideration.
                                </label>
                                <div class="form-group">
                                  <!-- <div
                                    class="custom-control custom-control-alternative custom-checkbox">
                                    <input class="custom-control-input"
                                      id="inspection" type="checkbox">
                                    <label class="custom-control-label"
                                      for="inspection">
                                    <span class="text-muted"> Allow Buyers
                                    to request Inseption </span>
                                    </label>
                                  </div> -->
                                  <textarea rows="4" class="form-control" id="comment" name="comment"
                                    placeholder="Imagine driving on the rough terrain in this 2012 ACE Cycle-Car. 34 km on the clock only. It is exceptional value at $24. 
                                    Only travelled 34 km. Don't let this go at this price!."></textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <input type="button" name="previous"
                          class="previous action-button-previous" value="Previous" />
                        <input type="button" name="next" class="next action-button" id="next3" value="Next Step" />
                      </fieldset>  
                                    <fieldset>
                                    <div class="form-card">
                                <div class="col-lg-12">
                                    <div class="search-result text-left">                                        
                                        <div class="details-box row mt-4 m-0">
                                            <div class="col-lg-12"><h5>Ads Information</h5></div>
                                            <div class="col-md-12">
                                                <div class="stm-listing-single-price-title heading-font clearfix">
                                                    <div class="price" id="priceTop">$18,000</div>
                            
                                                    <div class="stm-single-title-wrap">
                                                        <h1 class="title" id="title">
                                                            New 2018 Toyota Camry
                                                        </h1>
                                                    </div>
                            
                                                </div>
                                                
                                                <div class="row">                                       
                                        <div class="col-lg-6">
                                            <p>Normal Images</p>
                                            <div id="imgGallery" class="d-flex flex-wrap">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                          <p>Damage Images</p>
                                          <div id="dmgGallery" class="d-flex flex-wrap">
                                          </div>
                                      </div>
                                    </div>
                                                
                                                <!-- End single-product-images -->
                                                <div class="stm-border-top-unit bg-color-border">
                                                    <h5><strong>Car Details</strong></h5>
                                                </div>
                                                <!-- End stm-border-top-unit -->
                                                <div class="stm-single-car-listing-data">
                                                    <table class="stm-table-main">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-car"></i> Body </td>
                                                                                <td class="heading-font" id="bodyTypeShow"> Sedan </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-road"></i> Odometer Reading </td>
                                                                                <td class="heading-font" id ="odometerShow"> 200 </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-oil"></i> Fuel type </td>
                                                                                <td class="heading-font" id="fuelTypeShow"> Fuel </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-engine"></i> Engine </td>
                                                                                <td class="heading-font" id ="engineShow"> 3200 </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-calendar"></i> Year </td>
                                                                                <td class="heading-font" id="buildYearShow"> 2018 </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-tag"></i> Price </td>
                                                                                <td class="heading-font" id="priceShow"> 18000 </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-car"></i> Transmission </td>
                                                                                <td class="heading-font" id="transmissionShow"> Automatic </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-cog"></i> Drive </td>
                                                                                <td class="heading-font"> AWD </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-briefcase" ></i> VIN: </td>
                                                                                <td class="heading-font" id="VINShow"> 3063 </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-dot-circle-o"></i> Exterior
                                                                                    Color </td>
                                                                                <td class="heading-font" id="colorShow"> Silver Metallic </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-dot-circle-o"></i> Interior
                                                                                    Color </td>
                                                                                <td class="heading-font" id="interiorColorShow"> Beige </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-key"></i> Registered </td>
                                                                                <td class="heading-font" id="registrationPlateShow"> N/A </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                            </tr>
                                                                
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!-- End stm-single-car-listing-data -->
                                                <div class="stm-car-listing-data-single stm-border-top-unit bg-color-border">
                                                    <h5>Features</h5>
                                                </div>
                                                <div class="stm-single-listing-car-features">
                                                    <div class="lists-inline" id="featureShow">
                                                        
                                                    </div>
                                                </div>                            
                                            </div>                                           
                                        </div>
                                </div> 
                                </div>
                                </div>   
                                <div class="row form-group">
                    <div class="col-md-12">
                        <div class="text-danger font-italic generic-errors-top"></div>
                    </div>
                </div>    
                                <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                                <input type="submit" name="next" class="action-button-previous"   />
                        		<!-- <input id="postNow" type="button" name="postnow" class="btn btn-success action-button" value="Post ads Now" data-toggle="modal" data-target="#paymentNow" /> -->                        
                            </fieldset>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>      </div>
            </div>
        </div>
    </section>
@stop
