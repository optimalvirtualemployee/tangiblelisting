@extends('agent.common.multi-step-default-watch')
@section('title', 'Add Watch')
@section('content')
  <style>
    .slick-slide img {
    max-height: 300px;
    width: auto;
    }
    #imgGallery div, #dmgGallery div {
    position: relative; 
    width: 120px;
    height: 120px;
    margin: 5px;
    border: 1px solid #e7e7e7;
    }
    #imgGallery div img, #dmgGallery div img {
    max-height: 150px;
    width: 100%;
    height: 100%;
    object-fit: cover;
    position: absolute;
    }
    .avatar-upload {
  position: relative;
  max-width: 205px;
  border: 2px dotted #bfc8d9;
}
.avatar-upload .avatar-edit {
  position: absolute;
  right: 12px;
  z-index: 1;
  top: 10px;
}
.avatar-upload .avatar-edit input {
  display: none;
}
.avatar-upload .avatar-edit input + label {
  display: inline-block;
  width: 34px;
  height: 34px;
  margin-bottom: 0;
  border-radius: 100%;
  background: #FFFFFF;
  border: 1px solid transparent;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
  cursor: pointer;
  font-weight: normal;
  transition: all 0.2s ease-in-out;
}
.avatar-upload .avatar-edit input + label:hover {
  background: #f1f1f1;
  border-color: #d6d6d6;
}
.avatar-upload .avatar-edit input + label:after {
  content: "\f040";
  font-family: 'FontAwesome';
  color: #757575;
  position: absolute;
  top: 10px;
  left: 0;
  right: 0;
  text-align: center;
  margin: auto;
}
.avatar-upload .avatar-preview {
  width: 192px;
  height: 192px;
  position: relative;
  border-radius: 100%;
  border: 6px solid #F8F8F8;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
  margin: 0 auto;
}
.avatar-upload .avatar-preview > div {
  width: 100%;
  height: 100%;
  border-radius: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
}
.security-photos{
  background-color: #fcfcfc;
    padding: 10px;
}
.previewContainer{
  background-color: #fff;
    box-shadow: 1px 1px 1px 1px #80808040;
    padding: 25px;
}
#loading-image{
  position:fixed;
  top:0px;
  right:0px;
  width:100%;
  height:100%;
  background-color:#666;
  background-image:url('/assets/img/Spinner-loader.gif');
  background-repeat:no-repeat;
  background-position:center;
  z-index:10000000;
  opacity:0.6;
}
  </style>
  <body>
    <section class="become-seller-from overlay-wrapper">
    
      <div class="container">
      <div class="row">
      <div class="col-lg-12">
      <div class="">
                        <div id='loading-image' style='display:none'>
				</div>
        <!-- MultiStep Form -->
        <div class="container-fluid" id="grad1">
          <div class="row justify-content-center mt-0">
            <div class="col-11  col-lg-12 text-center p-0 mt-3 mb-2">
              <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                <!-- <h2><strong>Sign Up Your User Account</strong></h2>
                  <p>Fill all form field to go to next step</p> -->
                <div class="row">
                  <div class="col-md-12 mx-0">
                    <form id="msform" action="{{ url('agent/addWatchData') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="agentId" id="agentId" value="{{Auth::user()->id}}">
                      <!-- progressbar -->
                      <ul id="progressbar">
                        <li class="active" id="account"><strong>Select Watch</strong></li>
                        <li id="personal"><strong>Add Details</strong></li>
                        <li id="payment"><strong>Attach Photos</strong></li>
                        <li id="confirm"><strong>Summary</strong></li>
                      </ul>
                      <!-- fieldsets -->
                      <fieldset>
                        <div class="form-card">
                          <p><b>What type of Watch you Selling?</b></p>
                          <div class="row">
                            <div class="col-6">
                              <select class=" form-control" id="brand"
                                name="brand">
                                <option value="">Make</option>
                                @foreach($brands as $brand)
                                       <option value="{{$brand->id}}">{{$brand->watch_brand_name}}</option>
                                            @endforeach
                              </select>
                            </div>
                            <div class="col-6">
                              <select class=" form-control" id="model"
                                name="model">
                                <option value="">Model</option>
                                                             
                              </select>
                            </div>                            
                          </div>
                        </div>
                        <input type="button" name="next" class="next action-button" id ="next1" value="Next Step" />
                      </fieldset>
                      <fieldset>
                        <div class="form-card">
                          <div class="">
                            <h2 class="fs-title">Watch Details</h2>
                            <hr class="my-2">
                            <div class="row">
                              <div class="col-lg-12">
                              <div class="row m-0">
                              <!--<div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label" for="adtitle">Title*</label>
                                                    <input type="text" id="adtitle" name="adtitle"  placeholder="Ad Title" class="form-control">
                                    </div>
                                  </div>-->
                              </div>
                              <div class="row m-0">
                              <label>Watch Location</label>
                              </div>
                              <div class="row m-0">
                              <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Country*</label>
                                      <select class="form-control" data-trigger name="countryId" id="countryId">
                                                    <option  value="">Select Country</option>
                                                    @foreach($country_data as $country)
                                            <option value="{{$country->id}}">{{$country->country_name}}</option>
                                            @endforeach
                                                  </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">State</label>
                                      <select class="form-control" data-trigger name="stateId" id="stateId">
                                                    <option  value="">Select State</option>
                                                  </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">City*</label>
                                      <select class="form-control" data-trigger name="city-id" id="cityId">
                                                    <option  value="">Select City</option>
                                                  </select>
                                    </div>
                                  </div>
                              </div>
                              <div class="row m-0">
                              <label>Price</label>
                              </div>
                              <div class="row m-0">
                              
                              <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Price*</label>
                                      <select class="form-control" data-trigger name="pricetType" id="pricetType">
                                                      <option value="">Select</option>
                                                      @foreach($prices as $price)
                                                    <option value="{{$price->id}}">{{$price->price}}</option>
                                                    @endforeach
                                                    </select>
                                    </div>
                                  </div>
                                  
                                  
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Watch Price*</label>
                                      <input type="number" id="price" name="watch_price" class="form-control" placeholder="Watch Price">                                   
                                    </div>
                                  </div>
                                  
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Currency*</label>
                                      <select id="currency" class="form-control required"  name="currency">
                                      <option value="">Select Currency</option>
                                      @foreach ($currency_data as $currency)
                                        <option value="{{$currency->id}}">{{$currency->currency_code}}</option>
                                      @endforeach
                                    </select>
                                    </div>
                                  </div>
                                  
                              </div>
                              <div class="row m-0">
                              <label>Watch Specifies</label>
                              </div>
                              <div class="row m-0">
                              <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Watch Condition*</label>
                                      <select class="form-control" data-trigger name="watchCondition" id="watchCondition">
                                        <option disabled selected>Select Condition</option>
                                        <option value="New">New</option>
                                        <option value="Used">Used</option>                                        
                                      </select>
                                    </div>
                                  </div>
                                  
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Inclusions*</label>
                                      <select id="inclusions" class="form-control"  name="inclusions">
						<option value="">Select Inclusion</option>
						@foreach ($inclusions_data as $inclusion)
                           <option value="{{$inclusion->id}}">{{$inclusion->inclusion}}</option>
                         @endforeach
                         </select>
                                    </div>
                                  </div>
                                  
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Year of Manufacturer</label>
                                      <select id="yom" class="form-control"  name="yom">
						<option value="">Select Year Of Manufacture</option>
						@foreach ($year_of_manufactures as $year_of_manufacture)
                           <option value="{{$year_of_manufacture->id}}">{{$year_of_manufacture->year_of_manufacture}}</option>
                         @endforeach
                         </select>
                                    </div>
                                  </div>
                                  
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Gender*</label>
                                      <select id="gender" class="form-control required"  name="gender">
						<option value="">Select Gender</option>
						@foreach ($genders as $gender)
                           <option value="{{$gender->id}}">{{$gender->gender}}</option>
                         @endforeach
                         </select>
                                    </div>
                                  </div>
                                  
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Watch Type</label>
                                      <select id="type" class="form-control"  name="type">
						<option value="">Select Watch Type</option>
						@foreach ($types as $type)
                           <option value="{{$type->id}}">{{$type->watch_type}}</option>
                         @endforeach
                         </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Movement</label>
                                      <select id="movement" class="form-control"  name="movement">
						<option value="">Select Movement</option>
						@foreach ($movements as $movement)
                           <option value="{{$movement->id}}">{{$movement->movement}}</option>
                         @endforeach
                         </select>
                                    </div>
                                  </div>
                                  
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <div class="form-group">
                                        <label class="form-control-label"
                                          for="input-last-name">Reference Number*</label>
                                        <div class="input-group mb-3">                                        
                                          <input type="text" class="form-control" id="reference_num" name="referenceNo" placeholder="Reference Number">
                                        </div>
                                      </div>
                                  </div>
                                </div>
                                  
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Case Diameter</label>
                                      <select id="case_diameter" class="form-control"  name="case_diameter">
						<option value="">Select Case Diameter</option>
						@foreach ($case_diameters as $case_diameter)
                           <option value="{{$case_diameter->id}}">{{$case_diameter->case_diameter}}</option>
                         @endforeach
                      </select>
                                    </div>
                                  </div>
                                  
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                     <label class="form-control-label" for="input-last-name">Status Of Listing*</label>
                                                        <select id ="status" name="status" class="form-control" >
                          								<option value="" >Select Status</option>
                           								<option value="1" >Active</option>
                           								<option value="0" >Inactive</option>
                        								</select>
                                    </div>
                                  </div>
                                  
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-last-name">Timezone*</label>
                                                        <select class="form-control" data-trigger name="timezone" id="timezone">
                                                            <option value="">Select Timezone</option>
                                                            @foreach($timezone as $zone)
                                                            <option value="{{$zone->id}}">{{$zone->name .' '. $zone->offset}}</option>
                                                            @endforeach
                                                          </select>
                                                    </div>
                                  </div>
                                  
                              </div>
                                
                                <h2 class="fs-title mt-5">Additional Info</h2>
                                <hr class="my-2">
                                <div class="row m-0">
                                                                 
                                </div>
                                <div class="row m-0">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Power Reserve* </label>
                                      <select id="powerReserve" class="form-control required"  name="powerReserve">
						<option value="">Select Power Reserve</option>
						@foreach ($power_reserve as $power_reserve)
                           <option value="{{$power_reserve->id}}">{{$power_reserve->power_reserve}}</option>
                         @endforeach
                      </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Dial Color*</label>
                                        <select id="dialColor" class="form-control required"  name="dialColor" >
						<option value="">Select Dial Color</option>
						@foreach ($dial_color as $dial_color)
                           <option value="{{$dial_color->id}}">{{$dial_color->dial_color}}</option>
                         @endforeach
                      </select>                                       
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Bracelet Material*</label>
                                      <select id="braceletMaterial" class="form-control required"  name="braceletMaterial">
						<option value="">Select Bracelet Material</option>
						@foreach ($bracelet_material as $bracelet_material)
                           <option value="{{$bracelet_material->id}}">{{$bracelet_material->bracelet_material}}</option>
                         @endforeach
                      </select>
                                    </div>
                                  </div>

                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Bezel Material*</label>
                                      <select id="bezelMaterial" class="form-control required"  name="bezelMaterial" >
						<option value="">Select Bezel Material</option>
						@foreach ($bezel_material as $bezel_material)
                           <option value="{{$bezel_material->id}}">{{$bezel_material->bezel_material}}</option>
                         @endforeach
                      </select>
                                    </div>
                                  </div>                                  
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Bracelet Color*</label>
                                        <select id="braceletColor" class="form-control required"  name="braceletColor">
						<option value="">Select Bracelet Color</option>
						@foreach ($bracelet_color as $bracelet_color)
                           <option value="{{$bracelet_color->id}}">{{$bracelet_color->bracelet_color}}</option>
                         @endforeach
                      </select>                                      
                                    </div>
                                  </div>    
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Type of Clasp*</label>
                                      <select id="typeOfClasp" class="form-control required"  name="typeOfClasp">
						<option value="">Select Type Of Clasp</option>
						@foreach ($type_of_clasp as $type_of_clasp)
                           <option value="{{$type_of_clasp->id}}">{{$type_of_clasp->type_of_clasp}}</option>
                         @endforeach
                      </select>
                                    </div>
                                  </div>                           
                                </div>
                                <div class="row m-0">                                
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Clasp Material*</label>
                                        <select id="claspMaterial" class="form-control required"  name="claspMaterial">
						<option value="">Select Clasp Material</option>
						@foreach ($clasp_material as $clasp_material)
                           <option value="{{$clasp_material->id}}">{{$clasp_material->clasp_material}}</option>
                         @endforeach
                      </select>                                      
                                    </div>
                                  </div>
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Case Material*</label>
                                        <select id="caseMaterial" class="form-control required"  name="caseMaterial">
						<option value="">Select Case Material</option>
						@foreach ($case_material as $case_material)
                           <option value="{{$case_material->id}}">{{$case_material->case_material}}</option>
                         @endforeach
                      </select>                                      
                                    </div>
                                  </div> 

                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Case MM*</label>
                                        <select id="caseMM" class="form-control required"  name="caseMM">
						<option value="">Select Case MM</option>
						@foreach ($case_mm as $case_mm)
                           <option value="{{$case_mm->id}}">{{$case_mm->case_mm}}</option>
                         @endforeach
                      </select>                                       
                                    </div>
                                  </div>   
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Water Resistance Depth*</label>
                                        <select id="wrd" class="form-control required"  name="wrd">
						<option value="">Select Water Resistance Depth</option>
						@foreach ($wrd as $wrd)
                           <option value="{{$wrd->id}}">{{$wrd->water_resistant_depth}}</option>
                         @endforeach
                      </select>                                      
                                    </div>
                                  </div>  
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Glass Type*</label>
                                        <select id="glassType" class="form-control required"  name="glassType">
						<option value="">Select Glass Type</option>
						@foreach ($glass_type as $glass_type)
                           <option value="{{$glass_type->id}}">{{$glass_type->glass_type}}</option>
                         @endforeach
                      </select>                                     
                                    </div>
                                  </div>                         
                                </div>
                                <h2 class="fs-title mt-5">Price Details</h2>
                                <hr class="my-2">
                                <div class="row m-0">
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label" for="input-last-name">Commision</label>
                                      <div class="input-group mb-3">
                                        <input type="text" class="form-control">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="form-control-label" for="input-last-name">Revenue</label>
                                      <div class="input-group mb-3">
                                        <input type="text" class="form-control">
                                      </div>
                                    </div>
                                  </div>
                                </div>  
                                <h2 class="fs-title mt-5"> Additional information (optional) </h2>
                                <hr class="my-2">                                                    
                                <div class="accordion-1">
                                  <div class="container">
                                    <div class="row">
                                      <div class="col-md-12 ml-auto">
                                        <div class="accordion my-3"
                                          id="accordionExample">
                                          <div class="card p-0 mb-1">
                                            <div class="card-header" id="headingOne">
                                              <h5 class="mb-0">
                                                <button class="btn btn-link w-100 text-primary text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" 
                                                aria-controls="collapseOne"> Features <i class="ni ni-bold-down float-right"></i>
                                              </button>
                                              </h5>
                                            </div>
                                            <div id="collapseOne"
                                              class="collapse show"
                                              aria-labelledby="headingOne"
                                              data-parent="#accordionExample">
                                              <div class="mt-3">
                                                <div class="row">
                                                  <div class="col-lg-6">
                                                    <div class="inner">
                                                    <?php echo $html1;?>
                                                    </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                    <div class="inner">
                                                    <?php echo $html2;?>
                                                    </div>
                                                    </div>
                                                    </div>
                                                                            
                                              </div>    
                                            </div>
                                          </div>
                                          <div class="card p-0 mb-1">
                                            <div class="card-header"
                                              id="headingTwo">
                                              <h5 class="mb-0">
                                                <button
                                                  class="btn btn-link w-100 text-primary text-left collapsed"
                                                  type="button"
                                                  data-toggle="collapse"
                                                  data-target="#collapseTwo"
                                                  aria-expanded="false"
                                                  aria-controls="collapseTwo">
                                                Additional Features
                                                <i
                                                  class="ni ni-bold-down float-right"></i>
                                                </button>
                                              </h5>
                                            </div>
                                            <div id="collapseTwo" class="collapse"
                                              aria-labelledby="headingTwo"
                                              data-parent="#accordionExample">
                                            
                                             <div class="row">                                             
                                              <div class="col-lg-12">
                                                <div class="additionalFeature">
                                                  <table class="table table-bordered">
                                                    <thead>
                                                      <tr>
                                                        <th>Àdd features
                                                        </th>
                                                        <th>Action</th>
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                      <tr>
                                                        <td><input type="text" id="additional-feature" name="additional_feature[]" class="form-control">
                                                        </td>
                                                        <td><button class="btn btn-success" type="button" id="addRow"><i class="fa fa-plus"></i></button>
                                                        </td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                </div>
                                              </div>
                                            </div> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            
                          </div>
                         
                        </div>
                        <div>
                        </div>
                        <input type="button" name="previous"
                          class="previous action-button-previous" value="Previous" />
                        <input type="button" name="next" id="next2" class="next action-button"
                          value="Next Step" />
                      </fieldset>
                      <fieldset>
                        <div class="form-card">
                          <h2 class="fs-title">Attach Photos</h2>
                          <p>Attach your watch photos here, you can drag the photos to the main </p>
                          <div class="row" >
                            <div class="col-lg-12">
                              <div class="inner">
                              
                                <div class="input-field">      
                                  <div class="input-images-1" id="input-images-1" style="padding-top: .5rem;"></div>
                                </div>
                                
                              </div>
                            </div>
                          </div>
                          <div class="row mt-5">
                            <hr class="my-2">
                            <div class="col-lg-12">
                              <div class="inner">
                                <h2 class="fs-title">Damage Photos</h2>
                                <p class="muted">Lorem ipsum is a dummy text, Lorem ipsum is a dummy text</p>
                                <div class="input-field">
                                  <div class="input-images-2" id="damagePhotos" style="padding-top: .5rem;"></div>
                                </div>
                                
                              </div>
                            </div>
                          </div>
                          <div class="row mt-5">   
                            <div class="col-lg-12">
                              <h2 class="fs-title">Security Photos</h2>  
                            </div>                         
                            <div class="col-lg-6">                             
                              <div class="inner security-photos">
                                <div class="row">
                                  <div class="col-lg-6">
                                    <div class="secutrity1">
                                      <img src="/assets/img/security-1.png" alt="">
                                    </div>
                                    <input type="hidden" class="digitalTime1" id="security-time1" name="security-time1"/>
                                    <div class="digitalTime digitalTime1" id="security-time1" name="security-time1">
                                      10:32
                                    </div>
                                  </div>
                                  <div class="col-lg-6">
                                    <div class="uploadImage">
                                      <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input type='file' id="security1" name="security1[]" class="imageUploadCls" accept=".png, .jpg, .jpeg" />
                                            <label for="security1"></label>
                                        </div>
                                        <div class="avatar-preview" id="security1-image">
                                            <div class="imagePreview" style="background-image: url(img/watch.png);">
                                            </div>
                                        </div>
                                    </div>  
                                    </div>
                                  </div>
                                </div>                     
                                <hr class="my-2">
                              </div>
                            </div>
                            <div class="col-lg-6">                           
                              <div class="inner security-photos">
                                <div class="row">
                                  <div class="col-lg-6">
                                    <div class="secutrity2">
                                      <img src="/assets/img/security-2.png" alt="">
                                    </div>
                                    <input type="hidden" class="digitalTime2" id="security-time2" name="security-time2"/>
                                    <div class="digitalTime digitalTime2" >
                                      10:32
                                    </div>
                                  </div>
                                  <div class="col-lg-6">
                                    <div class="uploadImage">
                                      <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input type='file' id="security2" name="security2[]" class="imageUploadCls" accept=".png, .jpg, .jpeg" />
                                            <label for="security2"></label>
                                        </div>
                                        <div class="avatar-preview" id="security2-image" >
                                          <div class="imagePreview" style="background-image: url(img/watch.png);">
                                            </div>
                                        </div>
                                    </div>  
                                    </div>
                                  </div>
                                </div>                     
                                <hr class="my-2">
                              </div>
                            </div>
                          </div>
                          <div class="row mt-5">
                            <hr class="my-2">
                            <div class="col-lg-12">
                              <div class="inner">
                                <h2 class="fs-title">Comments</h2>
                                <hr class="my-2">
                                <label>
                                Setting aside the effort to portray special and
                                recognizing highlights about your vehicle can
                                have the effect in catching a purchaser's
                                consideration.
                                </label>
                                <div class="form-group">
                                  <!-- <div
                                    class="custom-control custom-control-alternative custom-checkbox">
                                    <input class="custom-control-input"
                                      id="inspection" type="checkbox">
                                    <label class="custom-control-label"
                                      for="inspection">
                                    <span class="text-muted"> Allow Buyers
                                    to request Inseption </span>
                                    </label>
                                  </div> -->
                                  <textarea rows="4" class="form-control" id="comment" name="comment"
                                    placeholder="Imagine driving on the rough terrain in this 2012 ACE Cycle-Car. 34 km on the clock only. It is exceptional value at $24. 
                                    Only travelled 34 km. Don't let this go at this price!."></textarea>
                                </div>
                              </div>
                            </div>
                          </div>                         
                        </div>
                        <input type="button" name="previous"
                          class="previous action-button-previous" value="Previous" />
                        <input type="button" name="next3" id="next3" class="next action-button"
                          value="Next Step" />
                      </fieldset>   
                      <fieldset>
                        <div class="form-card">
                        <div class="col-lg-12">
                        <div class="search-result">
                            <div class="details-box row mt-4 m-0">
                                <div class="col-lg-12 text-left">
                                    <h5>Ads Information</h5>
                                </div>
                                <div class="col-md-12">
                                    <div
                                        class="stm-listing-single-price-title heading-font clearfix text-left">
                                        <div class="price" id="priceShow">$18,000</div>
                                        <div class="stm-single-title-wrap">
                                            <h1 class="title" id="title">
                                                Rolex GT Matrix 2
                                            </h1>
                                        </div>
                                    </div>
                                    <div class="row">                                       
                                        <div class="col-lg-6">
                                            <p>Normal Images</p>
                                            <div id="imgGallery" class="d-flex flex-wrap">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                          <p>Damage Images</p>
                                          <div id="dmgGallery" class="d-flex flex-wrap">
                                          </div>
                                      </div>
                                    </div>
                                    <!-- End single-product-images -->                                   
                                    <div class="watch-desc">

                                      <div class="stm-border-top-unit bg-color-border" id="commentShow">
                                          <h5><strong>Description</strong></h5>
                                      </div>
          
                                      <div class="stm-border-top-unit bg-color-border">
                                          <h5><strong>Basic Details</strong></h5>
                                      </div>
                                      <div class="stm-single-listing-car-features watch-details">
                                          <div class="lists-inline">
                                              <ul class="list-style-2">
                                                  <li id="brandShow">Brand: 	Rolex</li>
                                                  <li id="modelShow">Model: GMT-Master II</li>
                                              </ul>
                                              <ul class="list-style-2">
                                                  <li id="watchConditionShow">Condition: New</li>
                                                  <li id="movementShow">Movement: </li>
                                              </ul>
                                              <ul class="list-style-2">
                                                  <li id="caseMaterialShow">Case material: Steel</li>
                                                  <li id="braceletMaterialShow">Bracelet material: Stainless-Steel</li>
                                              </ul>
                                              <ul class="list-style-2">
                                                  <li id="yomShow">Year of production: </li>
                                                  <li id="genderShow">Gender: Women's</li>
                                              </ul>
                                              <ul class="list-style-2">
                                                  <li id="locationShow">Location: India,  Delhi</li>
                                                  <li id="dialColorShow">Dial Color: Gold</li>
                                              </ul>
                                          </div>
                                      </div>
                                      <!-- Start of the report section -->          
          
                                  </div>
                                </div>
                            </div>
                            <!-- <div class="row mt-5">
                              <div class="col-lg-12">
                                <div class="choosePaymmentoption">
                                  <div class="form-group"> 
                                    <label class="radio-inline"> 
                                      <input type="radio" name="postAds" checked="" value="free">Post free ads </label> 
                                      <label class="radio-inline"> <input type="radio" name="postAds" value="paid" class="ml-5">Upgrade Ads for 3 months 
                                    </label>
                                </div>
                                </div>
                              </div>
                            </div> -->
                        </div>
                        </div>
                        </div>
                        <div class="row form-group">
                    <div class="col-md-12">
                        <div class="text-danger font-italic generic-errors-top"></div>
                    </div>
                </div>
                        <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                        <input type="submit" name="next" class="action-button-previous"   />
                        <!-- <input id="postNow" type="button" name="postnow" class="btn btn-success action-button" value="Post ads Now" /> -->
                    </fieldset>  
                    
                                                  
                    </form>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  
@stop