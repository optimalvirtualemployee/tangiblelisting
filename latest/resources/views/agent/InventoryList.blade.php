@extends('agent.common.default') 
@section('title', 'Enquiry')
@section('content')

<!-- Main content -->
<div class="main-content" id="panel">
	<!-- Topnav -->

	<!-- Header -->
	<!-- Header -->
	<div class="header pb-6 d-flex align-items-center"
		style="min-height: 100px;"></div>
	<!-- Page content -->
	<div class="container-fluid mt--6">
		<div class="row">
			<div class="col-xl-12">
				<form>
					<div class="bg-white">
						<div class="card-header">
							<div class="row align-items-center">
								<div class="col-2">
									<h3 class="mb-0">Inventory</h3>
								</div>
								<div class="col-3">
									<div class="d-flex align-items-center">
										<div class="statusTitle mr-3">Status :</div>
										<div class="d-flex align-items-center mr-2">
											<span class="badge badge-dot"> <i class="bg-success"></i>
											</span> Live
										</div>
										<div class="d-flex align-items-center mr-2">
											<span class="badge badge-dot"> <i class="bg-warning"></i>
											</span> Pending
										</div>
									</div>
								</div>
								<!-- <div class="col-7 text-right">
									<div id="searchwrap" class="mr-4">
										<form action="" autocomplete="off">
											<input id="search" name="search" type="text"
												class="form-control" placeholder="What're we looking for ?">
											<button type="submit" class="btn btn-icon btn-primary">
												<span class="btn-inner--icon"><i class="fa fa-search"
													aria-hidden="true"></i></span>
											</button>
										</form>
									</div>
								</div> -->
							</div>
						</div>
						<div class="collapse" id="filterCollapse">
							<div class="card-body">Filter section coming soon...</div>
						</div>
						<div class="card-body inventoryCn">
							<div class="row ">
								@foreach($listings_data as $data)
								<div class="col-lg-3 col-md-6 mb-4">
									<div class="border p-2">
										<div class="ads-view position-relative">
											<div class="home-list-item invenView">
											@if($images->firstWhere('listing_id','=', $data->id) != null)
												<a class="home-view" href="#"><img
													src="{{url('uploads/'.$images->firstWhere('listing_id','=', $data->id)->filename)}}" alt=""
													class="img-fluid w-100"></a>
											@endif		
											</div>
											<div class="dropdown dropdownAction">
												<a class="btn btn-sm btn-icon-only text-light" href="#"
													role="button" data-toggle="dropdown" aria-haspopup="true"
													aria-expanded="false"> <i class="fas fa-ellipsis-v"></i>
												</a>
												<div
													class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
													@if($category->website_category == 'Automobiles') <a
														class="dropdown-item"
														href="{{ url('agent/editautoads', $data->id) }}">Edit</a>
													@elseif(($category->website_category == 'Watch')) <a
														class="dropdown-item"
														href="{{ url('agent/editwatchads', $data->id) }}">Edit</a>
													@elseif(($category->website_category == 'Real Estate')) <a
														class="dropdown-item"
														href="{{ url('agent/editpropertyads', $data->id) }}">Edit</a>
													@endif
													 <!-- <a
														class="dropdown-item" href="#">Preview</a> <a
														class="dropdown-item" href="#">Delete</a> -->
												</div>
											</div>
										</div>
										<div class="home-content mt-1">
											<h4
												class="mb-1 d-flex justify-content-between align-items-center  border-top pt-2 mt-3 border-bottom">
												<a href="#">{{$data->brand_name}} {{$data->model_name}}</a><span
													class="badge badge-dot"> <i class="bg-success"></i>
												</span>
											</h4>
											<!-- <div class="invenDesc">Always attractive make every day always attractive with Casio's enticer watches.</div> -->
											<span class="price">{{currency()->convert(floatval($data->price), 'USD', currency()->getUserCurrency())}}</span>
											<div class="ads-details mt-2">
												<table class="table">
													<thead>
														<tr>
															<th>Brand</th>
															<th>Model</th>
														</tr>
														<tr>
															<td>{{$data->brand_name}}</td>
															<td>{{$data->model_name}}</td>
														</tr>
													</thead>
												</table>
												<div class="d-flex justify-content-between">
													<table class="table">
														<thead>
															<tr>
																<th><i class="fa fa-search mr-2"></i></th>
																<th><i class="fa fa-eye mr-2"></i></th>
																<th><i class="fa fa-user mr-2"></i></th>
																<th><i class="fa fa-star mr-2"></i></th>
																<th><i class="fa fa-image mr-2"></i></th>
															</tr>
														</thead>
														<tr>
															<td class="searchAppear">11</td>
															<td class="numberOfView">255</td>
															<td class="users">36</td>
															<td class="fvrts">78</td>
															<td class="photos">22</td>
														</tr>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
								@endforeach
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		@stop