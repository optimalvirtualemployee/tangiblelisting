<!DOCTYPE html>
<html lang="en">
@include('tangiblehtml.headheader')
<style>
    
    .article-para{
text-overflow: ellipsis;
overflow: hidden;
white-space: nowrap;
}
</style>
<body>
	@include('tangiblehtml.innerheader')

	<div class="custom-banner-slider">
		<div class="banner-item">
			<img src="/assets/img/banner-villa.jpg" alt="" class="img-fluid">
		</div>
		<div class="banner-item">
			<img src="/assets/img/banner-villa1.jpg" alt="" class="img-fluid">
		</div>
		<div class="banner-item">
			<img src="/assets/img/banner-villa2.jpg" alt="" class="img-fluid">
		</div>
		<div class="banner-item">
			<img src="/assets/img/slide-3.jpg" alt="" class="img-fluid">
		</div>
		<div class="banner-item">
			<img src="/assets/img/slide-4.jpg" alt="" class="img-fluid">
		</div>
	</div>


<form id="searchForm" method="GET" action="{{url('/propertylisting/search')}}">
	<div class="banner-form-wrapper">
		<div class="container">
			<div class="banner-hero-form-wrapper custom-search-panel">
				<div class="hero-warp-heading-box">
					<h4>Find your dream property </h4>
				</div>
				<ul class="nav nav-tabs" id="myTabParent" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="comm-tab" data-toggle="tab" href="#comm" role="tab"
                                aria-controls="comm" aria-selected="true">Commercial</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Residential-tab" data-toggle="tab" href="#Residential" role="tab"
                                aria-controls="Residential" aria-selected="false">Residential</a>
						</li>
						<li>
							<input type="hidden" name="propertyType" value="Commercial" class="selectedTabValue">
						</li>
                        <!-- <li class="nav-item">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                                aria-controls="contact" aria-selected="false">Property Value</a>
                        </li> -->
                    </ul>
				  <div class="custom-search-panel">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item"><a class="nav-link active" id="home-tab"
							data-toggle="tab" href="#home" role="tab" aria-controls="home"
							aria-selected="true">Buy</a></li>
						<li class="nav-item"><a class="nav-link" id="profile-tab"
							data-toggle="tab" href="#profile" role="tab"
							aria-controls="profile" aria-selected="false">Rent</a></li>
							<li>
							<input type="hidden" name="purchaseType" value="0" class="purchaseType">
						</li>
					</ul>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active" id="home" role="tabpanel"
							aria-labelledby="home-tab">
								<div class="search-input">
									<input type="text" name="search" id="autocomplete1"
										placeholder="search by country, city or region’" value="{{isset($searchTermTOShow) ? $searchTermTOShow : ''}}">
									@if(isset($searchTermTOShow))
									<button type= "submit" id= "searchButton" class="site-btn">Search</button>
									@else
									<button type= "submit" id= "searchButton" class="site-btn">Search</button>
									@endif
								</div>
						</div>
						
					</div>
				</div>
				
				
				
				<div class="search-filter-panel">
					<div class="dropdown">
						<div class="dropdown-list">
							<button class="btn btn-secondary" type="submit" 
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								All Property Type <i class="fas fa-angle-down"></i>
							</button>
							<div class="dropdown-menu" aria-labelledby="allProperty"
								id="propertyCheck">
								<div class="form-check">
								@if(isset($propertyDrop) && strpos($propertyDrop, 'house') !== false && strpos($propertyDrop, 'apartment') !== false && strpos($propertyDrop, 'villa') !== false)
									<input type="checkbox" name="allchecked" class="form-check-input"
										id="exampleCheck1" value="" checked> <label class="form-check-label"
										for="exampleCheck1">All Property Type</label>
										@else
										<input type="checkbox" name="allchecked" class="form-check-input"
										id="exampleCheck1" value=""> <label class="form-check-label"
										for="exampleCheck1">All Property Type</label>
										@endif
								</div>
								@foreach($home_type as $home)
								<div class="form-check">
								@if(isset($propertyDrop) &&  strpos($propertyDrop  , $home->property_type)  !== false)
									<input type="checkbox" class="form-check-input" id="{{$home->property_type}}" name= "home-{{$home->property_type}}" value="{{$home->id}}" checked> <label class="form-check-label" for="{{$home->property_type}}">{{$home->property_type}}</label>
									@else
									<input type="checkbox" class="form-check-input" id="{{$home->property_type}}" name= "home-{{$home->property_type}}" value="{{$home->id}}" > <label class="form-check-label" for="{{$home->property_type}}">{{$home->property_type}}</label>
									@endif
								</div>
								@endforeach
							</div>
						</div>
						<div class="dropdown-list">
							<button class="btn btn-secondary" type="button" id="bed-min"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Price (min) <i class="fas fa-angle-down"></i>
							</button>
							<div class="dropdown-menu" aria-labelledby="bed-min"
								id="priceMin">

								<div class="form-check">
									<input type="radio"  name="minPrice"
										id="anyPrice" value=""> <label 
										for="anyPrice">Any</label>
								</div>
								<div class="form-check">
									@if(isset($priceMin) && $priceMin == "5000000")
									<input type="radio"  name="minPrice"
										id="lakh-min" value="5000000" checked> <label 
										for="lakh-min">50 Lakh</label>
										
										@else
										<input type="radio"  name="minPrice"
										id="lakh-min" value="5000000"> <label 
										for="lakh-min">50 Lakh</label>
										
										@endif
								</div>
								<div class="form-check">
								@if(isset($priceMin) && $priceMin == "10000000")
									<input type="radio"  name="minPrice"
										id="one-crore-min" value="10000000" checked> <label
										 for="one-crore-min">1 crore</label>
								@else
								<input type="radio"  name="minPrice"
										id="one-crore-min" value="10000000" > <label
										 for="one-crore-min">1 crore</label>
										@endif		
								</div>
								<div class="form-check">
								@if(isset($priceMin) && $priceMin == "10000000")
									<input type="radio"  name="minPrice"
										id="two-crore-min" value="20000000" checked> <label
										 for="two-crore-min">2 crore</label>
										@else
										<input type="radio"  name="minPrice"
										id="two-crore-min" value="20000000"> <label
										 for="two-crore-min">2 crore</label>
										@endif
								</div>
							</div>
						</div>
						<div class="dropdown-list">
							<button class="btn btn-secondary" type="button" id="bed-min"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Price (max) <i class="fas fa-angle-down"></i>
							</button>
							<div class="dropdown-menu" aria-labelledby="bed-min"
								id="priceMax">
								<div class="form-check">
									<input type="radio"  name="maxPrice"
										id="anyPrice" value=""> <label 
										for="anyPrice">Any</label>
								</div>
								<div class="form-check">
								@if(isset($priceMax) && $priceMax == "5000000")
									<input type="radio"  name="maxPrice"
										id="lakh" value="5000000" checked> <label 
										for="lakh">50 Lakh</label>
										@else
										<input type="radio" name="maxPrice"
										id="lakh" value="5000000"> <label 
										for="lakh">50 Lakh</label>
										@endif
								</div>
								<div class="form-check">
								@if(isset($priceMax) && $priceMax == "10000000")
									<input type="radio"  name="maxPrice"
										id="one-crore" value="10000000" checked> <label
										 for="one-crore">1 crore</label>
										@else
										<input type="radio"  name="maxPrice"
										id="one-crore" value="10000000"> <label
										 for="one-crore">1 crore</label>
										@endif
								</div>
								<div class="form-check">
								@if(isset($priceMax) && $priceMax == "20000000")
									<input type="radio"  name="maxPrice"
										id="two-crore" value="20000000" checked> <label
										 for="two-crore">2 crore</label>
										@else
										<input type="radio"  name="maxPrice"
										id="two-crore" value="20000000"> <label
										 for="two-crore">2 crore</label>
										@endif
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<div class="perfect-home">
<div class="custom-find-home pt-5 pb-5">
		<div class="container">
			<div class="main-heading">
				<h3>Find Your Featured Listings</h3>
			</div>
			<div class="perfect-home-slider" id="perfectHomes">
			@if($propertyListing){
				@foreach ($propertyListing as $data)
				<div class="home-item">
					<div class="home-detail">
						<div class="home-list-box">
							<a class="home-view"
								href="{{route('tangiblerealestate.show',Crypt::encrypt($data->id))}}"> <img
								src="{{url('uploads/'.$images->firstWhere('listing_id',$data->id)->filename)}}"
								alt="" class="img-fluid">
							</a> 
							<div class="home-content listing-home-content">
								<div class="propertyListing d-flex justify-content-between align-items-center">
									<span class="price order-2">{{currency()->convert(floatval($data->property_price), 'USD', currency()->getUserCurrency())}}</span>
									<h4 class="mb-0">
									<a href="{{route('tangiblerealestate.show',Crypt::encrypt($data->id))}}">{{$data->ad_title}}</a>
									</h4>
								</div>								
								<div class="fasility-item">
								<span class="mr-1"><i class="fas fa-bed"></i>{{$data->number_of_bedrooms}}</span>
								<span class="mr-1"><i class="fas fa-chart-area"></i> {{number_format($data->land_size).' '.$data->metric}} 
								</span >
								 <span class="mr-1"><i class="fas fa-shower"></i>{{$data->number_of_bathrooms}}</span>
								</div>								
								<p>{{$data->property_type}}</p>
							</div>
							<div class="dealerDetails">
								<div class="brand">
								@if($data->agency_id === 0 || $data->agency_id === NULL )
								<img src="https://dummyimage.com/32x32/000/fff" />
								@else
									<img src="{{url('uploads/'.$agenciesImages->firstWhere('listing_id',$data->agency_id)->filename)}}" />
								@endif
								</div>
								<div class="dealerInfo">
								<div class="dealerName">
								<i class="fa fa-check-square-o" aria-hidden="true"></i> 
								@if($data->agency_id === 0 || $data->agency_id === NULL)
								{{ "Private Seller" }}
								@else
								{{$data->agency_name}}
								@endif
								</div>									
									<div class="country-flag">
                                    <img src="{{url('uploads/'.$data->filename)}}" alt="">
                                        <p class="c-code">{{$data->countrycode}}</p>                                   
                                </div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				@endforeach
				}@else{
				<div class="home-item">
					<div class="home-detail">
						<div class="home-list-box">
						<p class = "home-view">No Data Found for this search</p>
						</div>
						</div>
						</div>
				}
				@endif
			</div>
		</div>
	</div>
</div>
	


	<div class="custom-top-home-wrapper pt-5 pb-5">
		<div class="container">
			<div class="main-heading">
				<h3>Latest Listings</h3>
			</div>
			<div class="row">
				@foreach ($latest_properties as $data)
				<div class="col-lg-4 col-md-6 mb-4">
					<div class="home-list-box">
						<div class="home-list-slider">
							@foreach($images->where('listing_id',$data->id) as $img)
						<div class="home-list-item">
							<a class="home-view" href="{{route('tangiblerealestate.show',Crypt::encrypt($data->id))}}"><img src="{{'/uploads/'.$img->filename}}" alt="" class="img-fluid"></a>
						</div>
							@endforeach
						</div>
						<div class="product-detail-btn">
							<ul>
								<li><a href="#"><i class="fas fa-search-plus"></i></a></li>
								<li><a href="#"><i class="far fa-heart"></i></a></li>
								<li><a href="#"><i class="fas fa-plus"></i></a></li>
							</ul>
						</div>
						<div class="home-content">
							<div class="propertyListing d-flex justify-content-between align-items-center">
								<span class="price order-2">{{currency()->convert(floatval($data->property_price), 'USD', currency()->getUserCurrency())}}</span>
								<h4 class="mb-0">
									<a href="{{route('tangiblerealestate.show',Crypt::encrypt($data->id))}}">{{$data->ad_title}}</a>
								</h4>
							</div>
							
							<div class="fasility-item">
							<span class="mr-1"><i class="fas fa-bed"></i>{{$data->number_of_bedrooms}}</span>
							<span class="mr-1"><i class="fas fa-chart-area"></i>{{number_format($data->land_size).''. $data->metric}}</span>
							<span class="mr-1"><i class="fas fa-shower"></i>{{$data->number_of_bathrooms}}</span>
							</div>
							
							<p>{{$data->property_type}}</p>
							
						</div>
						<div class="dealerDetails mt-3">
								<div class="brand">
								    <img src="http://tangiblelisting.developersoptimal.com/uploads/php6gZlNt.png">
								</div>
								<div class="dealerInfo">
								<div class="dealerName">
								<i class="fa fa-check-square-o" aria-hidden="true"></i> 
																real estate 123
																</div>									
									<div class="country-flag">
                                    <img src="http://tangiblelisting.developersoptimal.com/uploads/phpyHEGGE.png" alt="">
                                        <p class="c-code">AU</p>                                   
                                </div>
								</div>
							</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>

    <div class="our-agent-wrapper   pt-5 pb-5">
        <div class="container">
            <div class="main-heading text-center">
                <h3>Our Dealers</h3>
            </div>
            <div class="row">
			@if($dealer_listing)
            @foreach ($dealer_listing as $data)
                <div class="col-lg-3 col-md-6">
                    <div class="agent-detail-box">
                        <div class="agent-photo">
                            <a href="#">
                                <img src="{{url('uploads/'.$data->filename)}}" class="img-fluid">
                            </a>
                        </div>
                        <div class="agent-details">
                            <h4><a href="#">{{$data->first_name}}</a></h4>
                            <h5>400+</h5>
                            <a href="#" class="read-more d-block text-center">Read more</a>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="home-item">
					<div class="home-detail">
						<div class="home-list-box">
						<img src="{{url('/assets/images/NoImageFound.png')}}" alt="" class="img-fluid">
						</div>
						</div>
						</div>
                
                @endif                
                
            </div>
        </div>
    </div>

	<div class="custom-our-blog grey-bg pt-5 pb-5">
		<div class="container">
			<div class="main-heading">
				<h3>Our Articles</h3>
			</div>
			<div class="row">
				@foreach ($blogs as $data)
				<div class="col-lg-3 col-md-4 col-sm-6 mb-4">
					<div class="custom-blog-list">
						<div class="home-list-box">
							<div class="home-list-slider">
								@foreach($blog_images->where('listing_id',$data->id) as $img)
								<div class="home-list-item">
								@if($data->url != null)
								<a class="home-view" href="{{$data->url}}"><img
										src="{{url('uploads/'.$img->filename)}}" alt=""
										class="img-fluid"></a>
								@else
									<a class="home-view" href="{{url('propertyblog/'.$data->id)}}"><img
										src="{{url('uploads/'.$img->filename)}}" alt=""
										class="img-fluid"></a>
										@endif
								</div>
								@endforeach
							</div>
							<div class="blog-detail-content">
								<div class="blog-date d-flex">
									<span><i class="far fa-calendar-alt"></i> <span>{{($data->created_at)->format('M d,Y')}}</span></span>
									<span><i class="fas fa-tag"></i> <a href="#">{{$data->blogType}}</a></span>
								</div>
								<h4>
								@if($data->url != null)
								<a href="{{url($data->url)}}">{{$data->title}}</a>
								@else
									<a href="{{url('propertyblog/'.$data->id)}}">{{$data->title}}</a>
									@endif
								</h4>
								<p class="article-para">{{$data->blogPost}}</p>
								<a class="read-more d-block text-center" href="{{$data->url}}">Continue
									reading</a>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
	
	<section class="become-a-section">
        <div class="container">
            <h3>If you are a seller in automobiles, watches or real estate, sign up below to advertise your inventory globally.!</h3>
            <a href="/becomedealer" class="read-more fill-read-more">Become a Seller</a>
        </div>

    </section>
    
@include('tangiblehtml.innerfooter')


	<script src="/assets/js/jquery.js"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script src="/assets/js/wow.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
	<link
		href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css"
		rel="Stylesheet"></link>
	<script src="/assets/js/local.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
	<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
</body>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
	$(document).ready(function(){

		$('#myTab a').click(function(e) {
			  e.preventDefault();
			  $(this).tab('show');
			  $('#autocomplete1').val("");
			});


		  $('#propertyCheck').change(function () {
			  console.log($('#propertyCheck').val());
			  $('#searchButton').removeAttr('disabled');
			});  
		

		$('.searchproperty').click(function(){
			var list = []
			var proptype = '';
			$('#propertyCheck').find('input[type="checkbox"]').each(function(){	
				if($(this).prop('checked')){
					if($(this).val() !=""){						
						list.push($(this).val());					
					}
				}
			})
			proptype = list.join(',');
			$('input[name="propertyType"]').val(proptype);
		})
		
		$( "#autocomplete1" ).autocomplete({
	        source: function( request, response ) {
	          // Fetch data
	          $.ajax({
	            url:"/tangiblerealestate/autosearch",
	            type: 'post',
	            dataType: "json",
	            data: {
	               _token: '{{csrf_token()}}',
	               search: request.term
	            },
	            success: function( data ) {
	               response( data );
	            }
	          });
	        },
	        select: function (event, ui) {
	           $('#autocomplete1').val(ui.item.finalLabel); // display the selected text
	           console.log('value selected from drop down');

	           $('#searchButton').removeAttr('disabled');
	           console.log('value selected from drop down');
	        		
	           return false;
	        }
	      });
		
		$( "#autocomplete2" ).autocomplete({
	        source: function( request, response ) {
	          // Fetch data
	          $.ajax({
	            url:"/tangiblerealestate/autosearch",
	            type: 'post',
	            dataType: "json",
	            data: {
	               _token: '{{csrf_token()}}',
	               search: request.term
	            },
	            success: function( data ) {
	               response( data );
	            }
	          });
	        },
	        select: function (event, ui) {
	           $('#autocomplete2').val(ui.item.finalLabel); // display the selected text
	           return false;
	        }
	      });
		$( "#autocomplete3" ).autocomplete({
	        source: function( request, response ) {
	          // Fetch data
	          $.ajax({
	            url:"/tangiblerealestate/autosearch",
	            type: 'post',
	            dataType: "json",
	            data: {
	               _token: '{{csrf_token()}}',
	               search: request.term
	            },
	            success: function( data ) {
	               response( data );
	            }
	          });
	        },
	        select: function (event, ui) {
	           $('#autocomplete3').val(ui.item.finalLabel); // display the selected text
	           return false;
	        }
	      });

		
		var	searchTerm = @json($searchTermTOShow ?? '');
					

		if(!!searchTerm){
			$('html, body').animate({
		        scrollTop: $('.perfect-home').offset().top
		    }, 'slow');
			}
		
	});
   
</script>
<script>
    $(document).on('click', '.search-filter-panel .dropdown-menu', function (e) {
      e.stopPropagation();   
    });
	</script>
	<script>
		$('#myTabParent .nav-item').on('click',function(){			
			const selectedTab = $('a',this).attr('id');
			if(selectedTab == 'Residential-tab')
			{
				$('.selectedTabValue').val('Residential');
				
			}
			else{
				$('.selectedTabValue').val('Commercial');
			}
		})

		$('#myTab .nav-item').on('click',function(){			
			const selectedTab = $('a',this).attr('id');
			if(selectedTab == 'home-tab')
			{
				$('.purchaseType').val('0');
				
			}
			else{
				$('.purchaseType').val('1');
			}
		})

		
	</script>
</html>