<!DOCTYPE html>
<html lang="en">
@include('tangiblehtml.headheader')
<body>
    <@include('tangiblehtml.innerheader')
   <div class="custom-top-home-wrapper pt-5 ">
        <div class="container">
            <div class="main-heading mt-5">
                <h3>Sell</h3>                
            </div>
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="btn btn-ouline-dark active" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="true">Requests and Sales</a>
                </li>                
              </ul>
              
              
              <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade active show" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab"> 
                    @if(count($enquiry_property) > 0 && count($enquiry_automobile) > 0 && count($enquiry_watch) > 0)
                    <div class="noproduct text-center p-5 border mb-5   ">
                    <i class="fa-5x fa fa-shopping-cart p-3"></i>
                    <h2>You haven't purchased any items yet</h2>
                    <p>Here you'll find the current status of your price suggestions, purchase requests, and orders on Tangible.</p>
                    <div class="d-flex justify-content-center align-items-center flex-column">Make Secure Purchases with Tangible Buyer Protection
                        <a href="#">Learn more</a>
                     </div>
                    </div>
                    @endif
                    @if(count($enquiry_automobile) > 0)
            @foreach ($enquiry_automobile as $data)
            <div class="row d-flex align-items-center border p-3 mb-3">
                <div class="col-lg-3 col-md-3 mb-4">
                    <div class="border p-2">
                        <div class="ads-view position-relative">                          
                            <div class="home-list-item">
                                <a class="home-view" href="#"><img src="{{'uploads/'.$automobile_images->firstWhere('listing_id','=', $data->productId)->filename}}" alt="" class="img-fluid"></a>
                            </div>                            
                        </div>                        
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="inner">
                        <div class="home-content mt-1 border-right" >  
                            <p class="muted">TD:{{$data->listing_id}}</p>                          
                            <h4 class="mb-1"><a href="#">{{$data->ad_title}}</a></h4>  
                            <p class="font-weight-bold">{{currency()->convert(floatval($data->value), 'USD', currency()->getUserCurrency())}}</p>                        
                            <div class="d-flex mt-2 flex-column">
                                <div class="paymentTrusted mb-2">
                                    <i class="fa fa-payment"></i>Payment via Trusted Checkout
                                </div>    
                                <div class="SellerName mb-2">
                                    <i class="fa fa-payment"></i>@if($data->company_name != null)
                            {{$data->company_name}}
                            @else
                            {{$data->agent_name}}
                            @endif<a href="#">(Contact)</a>
                                </div>    
                                <div class="country mb-2">
                                    <img src="{{'uploads/'.$data->countryflag}}" alt="" width="25px"> {{$data->country_name}}
                                   
                                </div>                 
                            </div>              
                        </div>                       
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="inner">
                        <div class="font-weight-bold">
                            Canceled on Dec 6, 2020                           
                         </div>                         
                    </div>
                </div>
            </div>
            @endforeach
            @endif
            @if($enquiry_watch != null)
            @foreach ($enquiry_watch as $data)
            <div class="row d-flex align-items-center border p-3 mb-3">
                <div class="col-lg-3 col-md-3 mb-4">
                    <div class="border p-2">
                        <div class="ads-view position-relative">                          
                            <div class="home-list-item">
                                <a class="home-view" href="#"><img src="{{'uploads/'.$watch_images->firstWhere('listing_id','=', $data->productId)->filename}}" alt="" class="img-fluid"></a>
                            </div>                            
                        </div>                        
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="inner">
                        <div class="home-content mt-1 border-right" >  
                            <p class="muted">TD:{{$data->listing_id}}</p>                          
                            <h4 class="mb-1"><a href="#">{{$data->ad_title}}</a></h4>  
                            <p class="font-weight-bold">{{currency()->convert(floatval($data->value), 'USD', currency()->getUserCurrency())}}</p>                        
                            <div class="d-flex mt-2 flex-column">
                                <div class="paymentTrusted mb-2">
                                    <i class="fa fa-payment"></i>Payment via Trusted Checkout
                                </div>    
                                <div class="SellerName mb-2">
                                    <i class="fa fa-payment"></i>@if($data->company_name != null)
                            {{$data->company_name}}
                            @else
                            {{$data->agent_name}}
                            @endif<a href="#">(Contact)</a>
                                </div>    
                                <div class="country mb-2">
                                    <img src="{{'uploads/'.$data->countryflag}}" alt="" width="25px"> {{$data->country_name}}
                                   
                                </div>                 
                            </div>              
                        </div>                       
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="inner">
                        <div class="font-weight-bold">
                            Canceled on Dec 6, 2020                           
                         </div>                         
                    </div>
                </div>
            </div>
            @endforeach
            @endif
            @if($enquiry_property != null)
            @foreach ($enquiry_property as $data)
            <div class="row d-flex align-items-center border p-3 mb-3">
                <div class="col-lg-3 col-md-3 mb-4">
                    <div class="border p-2">
                        <div class="ads-view position-relative">                          
                            <div class="home-list-item">
                                <a class="home-view" href="#"><img src="{{'uploads/'.$property_images->firstWhere('listing_id','=', $data->productId)->filename}}" alt="" class="img-fluid"></a>
                            </div>                            
                        </div>                        
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="inner">
                        <div class="home-content mt-1 border-right" >  
                            <p class="muted">TD:{{$data->listing_id}}</p>                          
                            <h4 class="mb-1"><a href="#">{{$data->ad_title}}</a></h4>  
                            <p class="font-weight-bold">{{currency()->convert(floatval($data->value), 'USD', currency()->getUserCurrency())}}</p>                        
                            <div class="d-flex mt-2 flex-column">
                                <div class="paymentTrusted mb-2">
                                    <i class="fa fa-payment"></i>Payment via Trusted Checkout
                                </div>    
                                <div class="SellerName mb-2">
                                    <i class="fa fa-payment"></i>@if($data->company_name != null)
                            {{$data->company_name}}
                            @else
                            {{$data->agent_name}}
                            @endif<a href="#">(Contact)</a>
                                </div>    
                                <div class="country mb-2">
                                    <img src="{{'uploads/'.$data->countryflag}}" alt="" width="25px"> {{$data->country_name}}
                                   
                                </div>                 
                            </div>              
                        </div>                       
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="inner">
                        <div class="font-weight-bold">
                            Canceled on Dec 6, 2020                           
                         </div>                         
                    </div>
                </div>
            </div>
            @endforeach
            @endif
                </div>
              </div>
            <div class="row mb-5">
                <div class="inner w-100">
                    <div class="card-body pt-0 p-1"> 
                        <div class="accordion mt-5" id="accordionExample">
                          <div class="">
                              <div class="card-header collapsed" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                  <strong class="mb-0">How can I delete my listing?</strong>
                              </div>
                              <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample" style="">
                                  <div class="card-body">
                                  <p>Your listing will remain online for 3 months. During this time, you may only offer your item on Tangible. You can delete your listing in your Tangible user area, but only under extraordinary circumstances.</p>
                                  </div>
                              </div>
                        </div>
                          <div class="">
                              <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                  <strong class="mb-0">How long will my listing be online?</strong>
                              </div>
                              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                  <div class="card-body">
                                      <p>Your listing will remain online for 3 months. During this time, you may not offer or sell the item elsewhere. If you don't sell your watch within the initial period of validity, you can choose to either delete your listing or renew it for another 3 months. Before we can renew your listing, we sometimes require proof that the item is still in your possession.</p>
                                  </div>
                              </div>
                          </div>
                          <div class="">
                              <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                  <strong class="mb-0">I can't find my listing. When will it be activated on Tangible?</strong>
                              </div>
                              <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                  <div class="card-body">
                                      <ul class="list-style-2 d-flex flex-column">
                                          <li>The manual review hasn't taken place yet.</li>
                                          <li>We manually review new listings for the safety of all Tangible users. These reviews only take place on business days and not on weekends. Listings are usually activated within 1 to 3 business days.</li>
                                          <li>The security images aren't correct. In this case, you will have received an email requesting that you update the security images in your Tangible area.</li>
                                          <li>Your user account isn't active yet. Please check whether you have confirmed your user account by clicking on the link in the confirmation email.</li>
                                          <li>In some countries, we require private sellers to confirm their identity. Your identity probably hasn't been confirmed yet. Please note that it can take 1 to 3 business days to review your identification documents.</li>
                                      </ul>
                                  </div>
                              </div>
                          </div>
                      </div>
                      </div>
                </div>
            </div>
            
        </div>
    </div>
    @include('tangiblehtml.innerfooter')
    <script src="/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/local.js"></script>

    <script>
    $(document).on('click', '.search-filter-panel .dropdown-menu', function (e) {
      e.stopPropagation();   
    });
    </script>
</body>

</html>