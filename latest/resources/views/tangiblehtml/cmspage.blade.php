@include('tangiblehtml.landing_page_header')

<body>
    @include('tangiblehtml.innerheader')
    <div class="container  custom_cms_page">
        <div class="row">
            <!-- <div class=""> -->
                <div class="main-heading col-12">
                    @isset($page->page_title)
                    <h4 class="mb-4 pb-3" style="text-align: center;border-bottom:1px solid #ccc; font-weight: 800;">
                        {{$page->page_title}}</h4>
                    @endif
                </div>
                
                @if(!empty($page->image))
                <div class="col-md-12">
                    <img style="height: 300px;" src=" {{ URL::to('/') }}/cmspage_images/{{ $page->image }}" alt=""
                        width="100%">
                </div>
                @endif
                <div class="p-3 col-12">
                    @isset($page->description)
                    <div>
                        {!! $page->description !!}
                    </div>
                    @endif
                </div>
            <!-- </div> -->
        </div>
    </div>
</body>

@include('tangiblehtml.innerfooter')