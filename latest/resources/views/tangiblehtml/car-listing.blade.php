<!DOCTYPE html>
<html lang="en">

@include('tangiblehtml.headheader')

<body>

    @include('tangiblehtml.innerheader')

    <div class="custom-home-listing-wrapper ">
        <nav aria-label="breadcrumb ">
            <div class="container-fluid ">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/tangibleautomobiles">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Car</li>
                </ol>
            </div>
        </nav>
        <div class="container-fluid pt-4 ">

            <div class="row">
<div class="col-lg-3 order-1">
                    <div class="product-filter" id="filter-form">
                    <form id="searchForm" method="GET" action="{{url('/carlisting/search')}}">
                        <div class="product-filter-inner">
                            
                            <div class="filter">
                                <p><i class="fa fa-filter" aria-hidden="true"></i> Refine Search</p>
                                <a href="javascript:;"><i class="fa fa-undo" aria-hidden="true" id="clear"> Clear</a></i>
                            </div>
                            <div class="filter-area">

                                <div class="filter-box">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#make-filter"
                                        role="button" aria-expanded="false" aria-controls="make-filter">
                                        Make <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="make-filter">
                                        <div class="inner-text">
                                            <ul id="brands">
                                                @foreach($makes as $make)
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                        @if(isset($makeSelected))
                                                            <input type="checkbox" class="" id="{{$make->automobile_brand_name}}" name="make-{{$make->id}}" value="{{$make->id}}" {{in_array($make->id,$makeSelected) ? "Checked" : ''}}>
                                                         @else
                                                         	<input type="checkbox" class="" id="{{$make->automobile_brand_name}}" name="make-{{$make->id}}" value="{{$make->id}}">
                                                         @endif	   
                                                            <label class="custom-control-label" for="{{$make->automobile_brand_name}}">{{$make->automobile_brand_name}}</label>
                                                        </div>
                                                        <!-- <div class="value">(867)</div> -->
                                                    </a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="filter-box">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#model-filter"
                                        role="button" aria-expanded="false" aria-controls="model-filter">
                                        Model <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="model-filter">
                                        <div class="inner-text">
                                            <ul id="model">
                                             @foreach($models as $model)
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                        @if(isset($modelSelected))
                                                            <input type="checkbox" class="" id="{{$model->automobile_model_name}}" name="model-{{$model->id}}" value ="{{$model->id}}" {{in_array($model->id,$modelSelected) ? "Checked" : ''}}>
                                                        @else
                                                        	<input type="checkbox" class="" id="{{$model->automobile_model_name}}" name="model-{{$model->id}}" value ="{{$model->id}}">    
                                                         @endif
                                                            <label class="custom-control-label" for="{{$model->automobile_model_name}}">{{$model->automobile_model_name}}</label>
                                                        </div>
                                                        <!-- <div class="value">(867)</div> -->
                                                    </a>
                                                </li>
                                               @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-box">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#condition-filter"
                                        role="button" aria-expanded="false" aria-controls="condition-filter">
                                        Condition <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="condition-filter">
                                        <div class="inner-text">
                                            <div class="pt-2 pb-1 pl-3">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                @if(isset($condition))
                                                    <input type="radio" id="customRadioInline1"
                                                        name="customRadioInline1" value= "New" class="custom-control-input" {{$condition == 'New' ? "Checked" : ""}}>
                                                 @else        
                                                 	<input type="radio" id="customRadioInline1"
                                                        name="customRadioInline1" value= "New" class="custom-control-input">
                                                 @endif       
                                                    <label class="custom-control-label"
                                                        for="customRadioInline1">New</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                @if(isset($condition))
                                                    <input type="radio" id="customRadioInline2"
                                                        name="customRadioInline1" value= "Used" class="custom-control-input" {{$condition == 'Used' ? "Checked" : ""}}>
                                                @else        
                                                	<input type="radio" id="customRadioInline2"
                                                        name="customRadioInline1" value= "Used" class="custom-control-input">
                                                @endif        
                                                    <label class="custom-control-label"
                                                        for="customRadioInline2">Used</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="filter-box">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#transmission-filter"
                                        role="button" aria-expanded="false" aria-controls="transmission-filter">
                                        Transmission <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="transmission-filter">
                                        <div class="inner-text">
                                            <ul>
                                            @foreach($transmissions as $transmission)
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                        @if(isset($transmissionSelected))
                                                            <input type="checkbox" class="" id="{{$transmission->transmission}}" name="transmission-{{$transmission->id}}" value="{{$transmission->id}}" {{in_array($transmission->id,$transmissionSelected) ? "Checked" : ''}}>
                                                        @else
                                                        	<input type="checkbox" class="" id="{{$transmission->transmission}}" name="transmission-{{$transmission->id}}" value="{{$transmission->id}}">
                                                        @endif	    
                                                            <label class="custom-control-label" for="{{$transmission->transmission}}">{{$transmission->transmission}}</label>
                                                        </div>
                                                     <!-- <div class="value">(867)</div> -->   
                                                    </a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-box">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#odometer-filter"
                                        role="button" aria-expanded="false" aria-controls="odometer-filter">
                                        <div class="icon-heading"> Odometer </div>
                                        <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="odometer-filter">
                                        <div class="inner-text">
                                            <div class="form-row mb-2 mt-2">
                                                <label for="from">From :</label>
                                                <select class="form-control custom-select"  id="from" name="odoFrom">
                                                    @if(isset($odoFrom))
                                                    <option selected disabled>{{$odoFrom}} km</option>
                                                    <option value = "0" >0 km</option>
                                                    <option value="10000">10,000 km</option>
                                                    <option value="20000">20,000 km</option>
                                                    <option value="30000">30,000 km</option>
                                                    <option value="40000">40,000 km</option>
                                                    <option value="50000">50,000 km</option>
                                                    <option value="60000">60,000 km</option>
                                                    <option value="70000">70,000 km</option>
                                                    <option value="80000">80,000 km</option>
                                                    <option value="90000">90,000 km</option>
                                                    @else
                                                    <option selected disabled>0 km</option>
                                                    <option value="10000">10,000 km</option>
                                                    <option value="20000">20,000 km</option>
                                                    <option value="30000">30,000 km</option>
                                                    <option value="40000">40,000 km</option>
                                                    <option value="50000">50,000 km</option>
                                                    <option value="60000">60,000 km</option>
                                                    <option value="70000">70,000 km</option>
                                                    <option value="80000">80,000 km</option>
                                                    <option value="90000">90,000 km</option>
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="form-row mb-2">
                                                <label for="to">To :</label>
                                                <select class="form-control custom-select"  id="to" name="odoTo">
                                                @if(isset($odoTo))
                                                    <option selected disabled>{{$odoTo}} km</option>
                                                    <option value="10000">10,000 km</option>
                                                    <option value="20000">20,000 km</option>
                                                    <option value="30000">30,000 km</option>
                                                    <option value="40000">40,000 km</option>
                                                    <option value="50000">50,000 km</option>
                                                    <option value="60000">60,000 km</option>
                                                    <option value="70000">70,000 km</option>
                                                    <option value="80000">80,000 km</option>
                                                    <option value="90000">90,000 km</option>
                                                    <option value="100000">1,00,000 km</option>
                                                 @else
                                                 	<option selected disabled>10,000 km</option>
                                                    <option value="20000">20,000 km</option>
                                                    <option value="30000">30,000 km</option>
                                                    <option value="40000">40,000 km</option>
                                                    <option value="50000">50,000 km</option>
                                                    <option value="60000">60,000 km</option>
                                                    <option value="70000">70,000 km</option>
                                                    <option value="80000">80,000 km</option>
                                                    <option value="90000">90,000 km</option>
                                                    <option value="100000">1,00,000 km</option>
                                                  @endif     
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                              <!--   <div class="filter-box">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#cylinders-filter"
                                        role="button" aria-expanded="false" aria-controls="cylinders-filter">
                                        Cylinders <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="cylinders-filter">
                                        <div class="inner-text">
                                            <ul>
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                            <input type="checkbox" class="" id="cylinders 1">
                                                            <label class="custom-control-label" for="cylinders 1">cylinders 1</label>
                                                        </div>
                                                        <div class="value">(867)</div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                            <input type="checkbox" class="" id="cylinders 2">
                                                            <label class="custom-control-label" for="cylinders 2">cylinders 2</label>
                                                        </div>
                                                        <div class="value">(867)</div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                            <input type="checkbox" class="" id="cylinders 3">
                                                            <label class="custom-control-label" for="cylinders 3">cylinders 3</label>
                                                        </div>
                                                        <div class="value">(867)</div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div> -->

                                <div class="filter-box">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#fuletype-filter"
                                        role="button" aria-expanded="false" aria-controls="fuletype-filter">
                                        Fuel Type <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="fuletype-filter">
                                        <div class="inner-text">
                                            <ul>
                                            @foreach($fuel_types as $fuel_type)
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                        @if(isset($fuelSelected))
                                                            <input type="checkbox" class="" id="{{$fuel_type->fuel_type}}" name="fuel_type-{{$fuel_type->id}}" value="{{$fuel_type->id}}" {{in_array($fuel_type->id,$fuelSelected) ? "Checked" : ''}}>
                                                        @else
                                                        	<input type="checkbox" class="" id="{{$fuel_type->fuel_type}}" name="fuel_type-{{$fuel_type->id}}" value="{{$fuel_type->id}}">
                                                        @endif	    
                                                            <label class="custom-control-label" for="{{$fuel_type->fuel_type}}">{{$fuel_type->fuel_type}}</label>
                                                        </div>
                                                      <!-- <div class="value">(867)</div> -->  
                                                    </a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-box">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#style-filter"
                                        role="button" aria-expanded="false" aria-controls="style-filter">
                                        <div>
                                            Style</div> <span><i class="fa fa-caret-right"
                                                aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="style-filter">
                                        <div class="inner-text">
                                            <ul>
                                            @foreach($body_types as $body_type)
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                        @if(isset($bodySelected))
                                                            <input type="checkbox" class="" id="{{$body_type->body_type}}" name="body_type-{{$body_type->id}}" value="{{$body_type->id}}" {{in_array($body_type->id,$bodySelected) ? "Checked" : ''}}>
                                                        @else
                                                        	<input type="checkbox" class="" id="{{$body_type->body_type}}" name="body_type-{{$body_type->id}}" value="{{$body_type->id}}">
                                                        @endif	    
                                                            <label class="custom-control-label" for="{{$body_type->body_type}}">{{$body_type->body_type}}</label>
                                                        </div>
                                                        <!-- <div class="value">(867)</div> -->
                                                    </a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>


                                <div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#drive-filter"
                                        role="button" aria-expanded="false" aria-controls="drive-filter">
                                        <div class="icon-heading"><i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                            Drive</div> <span><i class="fa fa-caret-right"
                                                aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="drive-filter">
                                        <div class="inner-text">
                                            <div class="pt-2 pb-1 pl-3">
                                                <div class="custom-control custom-radio">
                                                @if(isset($driveSelected))
                                                    <input type="radio" id="gender1" name="gender1" value="lhd"
                                                        class="custom-control-input" {{$driveSelected == 'lhd' ? "Checked" : ""}}>
                                                @else
                                                <input type="radio" id="gender1" name="gender1" value="lhd"
                                                        class="custom-control-input">
                                                @endif                
                                                    <label class="custom-control-label" for="gender1">LHD</label>
                                                </div>
                                                <div class="custom-control custom-radio">
                                                @if(isset($driveSelected))
                                                    <input type="radio" id="gender2" name="gender1" value="rhd"
                                                        class="custom-control-input" {{$driveSelected == 'rhd' ? "Checked" : ""}}>
                                                @else
                                                	<input type="radio" id="gender2" name="gender1" value="rhd"
                                                        class="custom-control-input">
                                                @endif                
                                                    <label class="custom-control-label" for="gender2">RHD</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-box icon">
                                    <a class="filter-text" data-toggle="collapse" href="#price-filter" role="button"
                                        aria-expanded="false" aria-controls="price-filter">
                                        <div class="icon-heading"><i class="fa fa-money" aria-hidden="true"></i> Price
                                        </div> <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse show" id="price-filter">
                                        <div class="inner-text">
                                            <div class="values">
                                                <!-- <div class="min-price"></div>
                                    <p class="max-price mt-3"></p> -->
                                                <div id="slider-range" class="price-filter-range" name="rangeInput">
                                                </div>
                                                <div
                                                    style="margin: 20px auto 5px;display: flex;justify-content: space-between;align-items: center;text-align: center;">
                                                    <div class="div">
                            <label style="line-height: 1;" class="d-block" for="min_price">Min</label>
                            @if(isset($priceMin))
                            <input type="number" min=0 max={{$price_max-10}} value = {{$priceMin}} oninput="validity.valid||(value='0');" id="min_price" name="minPrice"
                              class="price-range-field" />
                             @else
                             <input type="number" min=0 max={{$price_max-10}} value = 0 oninput="validity.valid||(value='0');" id="min_price" name="minPrice"
                              class="price-range-field" /> 
                             @endif 
                          </div>
                          <div class="div">
                            <label style="line-height: 1;" class="d-block" for="max_price">Max</label>
                            @if(isset($priceMax) && $priceMax > 0)
                            <input type="number" min=0 max={{$price_max}} value = {{$priceMax}} oninput="validity.valid||(value='{{$priceMax}}');" name="maxPrice"
                              id="max_price" class="price-range-field" />
                              @else
                              <input type="number" min=0 max={{$price_max}} value = {{$price_max}} oninput="validity.valid||(value='{{$price_max}}');" name="maxPrice"
                              id="max_price" class="price-range-field" />
                              @endif
                          </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#year-filter"
                                        role="button" aria-expanded="false" aria-controls="year-filter">
                                        <div class="icon-heading"><i class="fa fa-calendar-check-o"
                                                aria-hidden="true"></i> Year</div>
                                        <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="year-filter">
                                        <div class="inner-text">
                                            <div class="form-row mb-2 mt-2">
                                                <label for="yearfrom">Year From :</label>
                                                <select class="form-control custom-select"  id="yearfrom" name="fromYear">
                                                    <option selected disabled>Select Here</option>
                                                    @foreach($years as $year)
                                                    @if(isset($yearMin))
                                                    <option value="{{$year->build_year}}" {{$yearMin == $year->build_year ? "Selected" : ""}}>{{$year->build_year}}</option>
                                                    @else
                                                    <option value="{{$year->build_year}}">{{$year->build_year}}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-row mb-2">
                                                <label for="yearto">Year to :</label>
                                                <select class="form-control custom-select"  id="yearto" name="toYear">
                                                    <option selected disabled>Select Here</option>
                                                    @foreach($years as $year)
                                                    @if(isset($yearMax))
                                                    <option value="{{$year->build_year}}" {{$yearMax == $year->build_year ? "Selected" : ""}}>{{$year->build_year}}</option>
                                                    @else
                                                    <option value="{{$year->build_year}}">{{$year->build_year}}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

								<div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#dcolor-filter"
                                        role="button" aria-expanded="false" aria-controls="dcolor-filter">
                                        <div class="icon-heading"><i class="fa fa-tachometer" aria-hidden="true"></i>
                                            Color</div>
                                        <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="dcolor-filter">
                                        <div class="inner-text">
                                            <div class="input-group mb-3">
                                                <select class="form-control custom-select mt-3" id="color" name="color">
                                                  <option value="" disabled selected>Select a color...</option>
												  @foreach($colours as $colour)
												  @if(isset($colorSelected))
                                                  <option value="{{$colour->id}}" {{$colorSelected == $colour->id ? "Selected" : ""}}>{{$colour->colour}}</option>
                                                  @else
                                                  <option value="{{$colour->id}}" >{{$colour->colour}}</option>
                                                  @endif
                                                  @endforeach
                                                </select>
                                              </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#country-filter"
                                        role="button" aria-expanded="false" aria-controls="country-filter">
                                        <div class="icon-heading"><i class="fa fa-globe" aria-hidden="true"></i> Country
                                        </div>
                                        <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="country-filter">
                                        <div class="inner-text">
                                            <ul>
                                            @foreach($country_data as $country)
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                        @if(isset($countrySelected))
                                                            <input type="checkbox" class="" id="{{$country-> country_name}}" name="country-{{$country->id}}" value="{{$country->id}}" {{in_array($country-> id,$countrySelected) ? "Checked" : ""}}>
                                                        @else
                                                        	<input type="checkbox" class="" id="{{$country-> country_name}}" name="country-{{$country->id}}" value="{{$country->id}}">
                                                        @endif	    
                                                            <label class="custom-control-label" for="{{$country-> country_name}}">{{$country-> country_name}}</label>
                                                        </div>
                                                      <!-- <div class="value">(867)</div> -->  
                                                    </a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
							    <button type="submit" id= "searchButton" class="site-btn">Search</button>
                            </div>
                        </div>
                        </form>
                    </div>

                </div>
                <div class="col-lg-9 mb-3 order-2">
                    <div class="main-heading mb-0">
                        <h3>Cars for Sale</h3>
                    </div>

                    <div class="sorting-panel">
                        <form action="#" class="needs-validation form-inline justify-content-end mb-3" novalidate>
                            <label for="sortby" class="mr-2">Sort by</label>
                            <select id="sortBy" class="form-control custom-select " name="sortBy">
                                @if(!isset($sortBy))
                                <option value="0" selected>Popularity</option>
                                @else
                                <option value="0">Popularity</option>
                                @endif
                                @if(isset($sortBy) && $sortBy == "1")
                                <option value="1" selected>High to low</option>
                                @else
                                <option value="1">High to low</option>
                                @endif
                                @if(isset($sortBy) && $sortBy == "2")
                                <option value="2" selected>Low to high</option>
                                @else
                                <option value="2">Low to high</option>
                                @endif
                                @if(isset($sortBy) && $sortBy == "3")
                                <option value="3" selected>Recent</option>
                                @else
                                <option value="3">Recent</option>
                                @endif
                            </select>
                        </form>
                    </div>


                    <div class="row  car-listing-row" id="listing">
						@foreach($automobileListing as $automobile)
                        <div class="col-sm-4">
                            <div class="stm-directory-grid-loop">
                                <div class="image">
                                    <div class="car-listing-slider">
                                    @foreach($images->where('listing_id',$automobile->id) as $image)
                                        <a href="{{route('carlisting.show',Crypt::encrypt($automobile->id))}}"><img src="{{url('uploads/'.$image->filename)}}" alt=""></a>
                                        @endforeach
                                    </div>
                                    <div class="stm-badge-directory heading-font Special ">Special</div>
                                </div>
                                <div class="listing-car-item-meta">
                                    <div class="car-meta-top heading-font clearfix">
                                        <div class="price">
                                            <div class="normal-price">{{currency()->convert(floatval($automobile->value), 'USD', currency()->getUserCurrency())}}</div>
                                        </div>
                                        <div class="car-title">
                                            <div class="labels">{{$automobile->neworused .' '. $automobile->build_year}}</div> {{$automobile->ad_title}}
                                        </div>
                                    </div>
                                    <div class="car-meta-bottom">
                                        <ul>
                                            <li>
                                                <i class="fa fa-road"></i>

                                                <span>{{$automobile->odometer}} </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-car"></i>

                                                <span>12/14 </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-gear"></i>
                                                <span>{{$automobile->transmission}}</span>
                                            </li>
                                            <li>
                                                <i class="fa fa-hand-paper-o"></i>
                                                <span>{{$automobile->rhdorlhd}}</span>
                                            </li>
                                            <li>
                                                <i class="fa fa-calendar"></i>
                                                <span>{{$automobile->build_year}}</span>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="dealer-logo">
                                        <div>
                                            <ul>
                                                <li class="active"><i class="fa fa-star"></i></li>
                                                <li class="active"><i class="fa fa-star"></i></li>
                                                <li class="active"><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>
                                            <p><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>
                                                {{ $automobile->agency_id === 0 ? "Private Seller" : "Dealer" }}</p>
                                        </div>
                                        <div class="country-flag">
                                            <img src="{{url('uploads/'.$automobile->filename)}}" alt="">
                                            <p class="c-code">{{$automobile->countrycode}}</p>
                                        </div>
                                    </div>

                                </div>
                                </a>
                            </div>
                        </div>
					@endforeach
                        
                    </div>

                     @if($automobileListing->hasPages())
          <div class="custom-pagination mt-4 d-flex justify-content-end">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
              @if ($automobileListing->onFirstPage())
            <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
        @else
        	<li class="page-item"><a class="page-link" href="{{ $automobileListing->withQueryString()->previousPageUrl() }}">Previous</a></li>
        @endif
        
        @for($i = 1 ; $i <= $automobileListing->lastPage() ; $i++)
        	@if($automobileListing->currentPage() == $i)
			<li class="page-item active"><a class="page-link" href="{{$automobileListing->withQueryString()->url($i)}}">{{$i}}</a></li>
			@else
			<li class="page-item"><a class="page-link" href="{{$automobileListing->withQueryString()->url($i)}}">{{$i}}</a></li>
			@endif
        @endfor        
          @if ($automobileListing->hasMorePages())
            <li class="page-item"><a class="page-link" href="{{ $automobileListing->withQueryString()->nextPageUrl() }}">Next</a></li>
        @else
            <li class="page-item disabled"><a class="page-link" href="#">Next</a></li>
        @endif    
              </ul>
            </nav>
          </div>
          @endif
                </div>
            </div>
        </div>
    </div>
    @include('tangiblehtml.innerfooter')



    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script src="/assets/js/jquery-ui.min.js"></script>
    <script src="/assets/js/local.js"></script>
</body>

<script type="text/javascript">

$(document).ready(function(){ 

	$("#sortBy").change(function(){
		var urlSortBy = window.location.href;
		if(urlSortBy.includes("/carlisting?")){
			var finalUrl = urlSortBy + '&sortBy=' +$(this).val();
			}else  if(urlSortBy.includes("/carlisting")){
		var finalUrl = urlSortBy + '/search?sortBy=' +$(this).val();
		}
		if(urlSortBy.includes("search")){
			if(urlSortBy.includes("sortBy")){
				finalUrl = urlSortBy.replace(/sortBy=\d/,'sortBy=' + +$(this).val());
				}else{
			finalUrl = window.location.href + '&sortBy=' + +$(this).val();
				}
			}
		window.location.assign(finalUrl);
	});
	
	$(function () {
        $("#slider-range").slider({
            range: true,
            orientation: "horizontal",
            min: 0,
            max: {{$price_max}},
            values: [$("#min_price").val(), $("#max_price").val()],
            step: 1000,

            slide: function (event, ui) {
                if (ui.values[0] == ui.values[1]) {
                    return false;
                }

               $("#min_price").val(ui.values[0]);
                $("#max_price").val(ui.values[1]);

                
            }
        });

        $("#min_price").val($("#slider-range").slider("values", 0));
        $("#max_price").val($("#slider-range").slider("values", 1));

    });

	$('#clear').click(function (e) {
		console.log('-----------------------------');
		  $('#filter-form').find(':input').each(function() {
		    if(this.type == 'submit'){
		          //do nothing
		      }
		      else if(this.type == 'checkbox' || this.type == 'radio') {
		        this.checked = false;
		      }
		   })
		   $('#yearfrom')[0].selectedIndex = 0;
		  $('#yearto')[0].selectedIndex = 0;

		  $('#from')[0].selectedIndex = 0;
		  $('#to')[0].selectedIndex = 0;

		  $('#color')[0].selectedIndex = 0;

		  $(function () {
		        $("#slider-range").slider({
		            range: true,
		            orientation: "horizontal",
		            min: 0,
		            max: {{$price_max}},
		            values: [0, {{$price_max}}],
		            step: 100,

		            slide: function (event, ui) {
		                if (ui.values[0] == ui.values[1]) {
		                    return false;
		                }

		               $("#min_price").val(ui.values[0]);
		                $("#max_price").val(ui.values[1]);
		            }
		        });

		        $("#min_price").val($("#slider-range").slider("values", 0));
		        $("#max_price").val($("#slider-range").slider("values", 1));

		    });
		});
	
	$('#brands').click(function(e){
		console.log('------------------------');

		if(e.target.tagName == "INPUT"){
			var checked = []
			var type = "brands"
			$("input[type='checkbox']:checked").each(function ()
			{
			    checked.push($(this).val());
			});
			console.log('checked ',checked);
			$.ajax({
				url: "/carlisting/brand",
				type: "POST",
				data: {brandFilter: checked, _token: '{{csrf_token()}}' },
				dataType: 'json',
				success : function(data){
					console.log(data);
					var listingHtml = "";

					if(data['model'] != null){
						var modelHtml = "";
						var modelData =data['model'];  
						for(var i=0; i < modelData.length; i++){
							modelHtml += `<li>
		                          <a href="#">
		                            <div class="custom-control">
		                              <input type="checkbox" class="" id="${modelData[i].automobile_model_name}" name="model-${modelData[i].id}" value="${modelData[i].id}">
		                              <label class="custom-control-label" for="${modelData[i].automobile_model_name}">${modelData[i].automobile_model_name}</label>
		                            </div>
		                            <!-- <div class="value">(867)</div>  -->
		                          </a>
		                        </li>`;	
							}
						$('#model').html(modelHtml);
							}
					

				}
						
				});
			
			}
		
});

	

});	
</script>

</html>