<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shipping Page Details</title>
    <link href="https://fonts.googleapis.com/css2?family=Muli:wght@200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/hover.css/2.3.1/css/hover-min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" />
    <link rel="stylesheet" href="/assets/css/animate.css">
    <link rel="stylesheet" href="/assets/css/font-style.css">
    <link rel="stylesheet" href="/assets/css/local.css">
    <link rel="stylesheet" href="/assets/css/track-order.css">
    <link rel="stylesheet" href="/assets/css/responsive.css">
</head>

<body class="track-order-status">
    <@include('tangiblehtml.innerheader')
   <div class="container pt-5 ">
       <div class="row">
           <div class="col-lg-9">
               <div class="inner">
                <article class="card p-1 mt-5">
                    <div class="card-body">
                        <div class="track">
                            <div class="step active"> <span class="icon"> <i class="fa fa-shopping-cart"></i> </span> <span class="text">Order Submitted</span> </div>
                            <div class="step active"> <span class="icon"> <i class="fa fa-credit-card"></i> </span> <span class="text">Payment Done</span> </div>
                            <div class="step active"> <span class="icon"> <i class="fa fa-truck"></i> </span> <span class="text">Shipped  </span> </div>
                            <div class="step"> <span class="icon"> <i class="fa fa-home"></i> </span> <span class="text">Delivery Expected </span><strong>21/03/2021 - 27/03/2021 </strong> </div>
                        </div>
                    </div>
                </article>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner">
                            <h3>Your item has been shipped</h3>
                            <p>Seller has shipped your items, below are the Tracking details for the purchase </p>
                           
                        </div>
                    </div>                    
                </div>               
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner border p-2 mb-3">                                                      
                            <div class="cardDetails table-responsive">
                               <table class="table">
                                   <tr><td>Courier Name</td><td>DHL Express</td></tr>
                                   <tr><td>Tracking ID</td><td>20410231570128</td></tr>
                                   <tr><td>Tracking Link</td><td><a href="#">https://somelinkoftrackinggoesher/3423423.</a></td></tr>
                               </table>
                            </div>

                        </div>
                    </div>
                </div>   
               </div>
           </div>
           <div class="col-lg-3">
               <div class="inner">
                <div class="watch-product-item mt-5">
                    <div class="product-thumb">
                      <a href="#"><img src="{{url('uploads/',$images->filename)}}" alt=""></a>
                      <div class="d-none">
                        <ul>
                          <li><a href="#"><i class="fas fa-search-plus"></i></a></li>
                          <li><a href="#"><i class="far fa-heart"></i></a></li>
                          <li><a href="#"><i class="fas fa-plus"></i></a></li>
                        </ul>
                      </div>
                      <div class="inside">
                        <div class="contents">
                          <table class="w-100">
                            <tbody><tr>
                              <td>Model</td>
                              <td><span>TS-2019</span></td>
                            </tr>
                            <tr>
                              <td>Year</td>
                              <td><span>2019</span></td>
                            </tr>
                            <tr>
                              <td>Make</td>
                              <td><span>Something</span></td>
                            </tr>
                            <tr>
                              <td>Case Diameter</td>
                              <td><span>Something</span></td>
                            </tr>
                            <tr>
                              <td>Location</td>
                              <td><span>San Diego, CA</span></td>
                            </tr>
                          </tbody></table>
                        </div>
                      </div>
                    </div>
                    <div class="product-details">
                      <h5><a href="#">Black Dial Leather Strap</a></h5>
                      <div class="price-wrap">
                        <span class="Price-currency"></span>{{$enquiry->ad_title}}
                      </div>
                      <div class="bottom-detais">
                        <div class="dealer">
                          <ul>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                          </ul>
                          <p class="seller-type"><i class="fa fa-check-square-o" aria-hidden="true"></i> {{ $enquiry->agency_id == 'Private Seller' ? "Private Seller" : "Dealer" }}</p>
                        </div>
                        <div class="country-flag">
                          <img src="{{url('uploads/',$enquiry->countryflag)}}" alt="">
                          <p class="c-code">{{$enquiry->countrycode}}</p>
                        </div>
                      </div>
                    </div>
    
                    <a href="javascript:void(0);" title="click here" class="icon"><i class="fa fa-info-circle" aria-hidden="true"></i></a>
    
                  </div>
               </div>
           </div>
       </div>
    </div>
    @include('tangiblehtml.innerfooter')
    <script src="/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
   
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/local.js"></script>

    <script>
    $(document).on('click', '.search-filter-panel .dropdown-menu', function (e) {
      e.stopPropagation();   
    });

    (function() {
        'use strict';
        window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
        }
        form.classList.add('was-validated');
        }, false);
        });
        }, false);
        })();
    </script>
</body>

</html>
