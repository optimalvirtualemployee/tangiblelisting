<!DOCTYPE html>
<html lang="en">

@include('tangiblehtml.headheader')

<body class="watch-lading">
    @include('tangiblehtml.innerheader')
    
    <div class="custom-home-listing-wrapper pb-3">
        <nav aria-label="breadcrumb ">
            <div class="container">
                <ol class="breadcrumb bg-color">
                    <li class="breadcrumb-item"><a href="/tangibleautomobiles">Home</a></li>
                    <li class="breadcrumb-item"><a href="/carlisting">Car</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{$automobile->automobile_brand_name}}</li>
                </ol>
                @if(session()->has('success_msg'))
                <div class="alert alert-success" role="alert">
                {{ session()->get('success_msg') }}
                </div>
                @endif
            </div>
        </nav>
    </div>
    <div class="single-product pb-5 pt-3">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="stm-listing-single-price-title heading-font clearfix">
                        <div class="price">{{currency()->convert(floatval($automobile->value), 'USD', currency()->getUserCurrency())}}</div>

                        <div class="stm-single-title-wrap">
                            <h1 class="title">
                                {{$automobile->ad_title}}
                            </h1>

                        </div>

                    </div>
                    <div class="single-product-images">
                        <div class="slider" id="lightgallery">
 							@if($images)                       
							@foreach($images as $image)
							<div class="item" data-src="{{url('uploads/'.$image->filename)}}">
                                <img src="{{url('uploads/'.$image->filename)}}">
                            </div>
							@endforeach                            
							@else
							<div class="item" data-src="/assets/img/car/d1.jpg">
                                <img src="/assets/img/car/d1.jpg">
                            </div>
							@endif

                            <!--<p class="save-listing" title="Save Listing">
                                <a class="" href="#">
                                    <i class="fa fa-bookmark-o" aria-hidden="true"></i> Save
                                </a>
                            </p>-->

                        </div>
                        <div class="slider-nav-thumbnails">
                            @foreach($images as $image)
                            <div><img src="{{url('uploads/'.$image->filename)}}" alt="One"> </div>
                            @endforeach
                            <!--<div><img src="/assets/img/car/d2.jpg" alt="Two"> </div>
                            <div><img src="/assets/img/car/d3.jpg" alt="Three"> </div>
                            <div><img src="/assets/img/car/d4.jpg" alt="Four"> </div>
                            <div><img src="/assets/img/car/d5.jpg" alt="Five"> </div>-->
                        </div>
                    </div>

                    <!-- End single-product-images -->
                    <div class="stm-border-top-unit bg-color-border">
                        <h5><strong>Car Details</strong></h5>
                    </div>
                    <!-- End stm-border-top-unit -->
                    <div class="stm-single-car-listing-data">
                        <table class="stm-table-main">
                            <tbody>
                                <tr>
                                    <td>
                                        <table class="inner-table">
                                            <tbody>
                                                <tr>
                                                    <td class="label-td"> <i class="fa fa-car"></i> Body </td>
                                                    <td class="heading-font"> {{$automobile->automobile_model_name}} </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class="divider-td"></td>
                                    <td>
                                        <table class="inner-table">
                                            <tbody>
                                                <tr>
                                                    <td class="label-td"> <i class="fa fa-road"></i> Odometer </td>
                                                    <td class="heading-font"> {{$automobile->odometer}} </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class="divider-td"></td>
                                    <td>
                                        <table class="inner-table">
                                            <tbody>
                                                <tr>
                                                    <td class="label-td"> <i class="fas fa-oil-can"></i> Fuel type </td>
                                                    <td class="heading-font"> {{$automobile->fuel_type}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class="divider-td"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="inner-table">
                                            <tbody>
                                                <tr>
                                                    <td class="label-td"> <i class="fas fa-car-battery"></i> Engine </td>
                                                    <td class="heading-font"> {{$automobile->engine}} </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class="divider-td"></td>
                                    <td>
                                        <table class="inner-table">
                                            <tbody>
                                                <tr>
                                                    <td class="label-td"> <i class="fa fa-calendar"></i> Year </td>
                                                    <td class="heading-font"> {{$automobile->year}} </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class="divider-td"></td>
                                    <td>
                                        <table class="inner-table">
                                            <tbody>
                                                <tr>
                                                    <td class="label-td"> <i class="fa fa-tag"></i> Price </td>
                                                    <td class="heading-font"> {{currency()->convert(floatval($automobile->value), 'USD', currency()->getUserCurrency())}} </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class="divider-td"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="inner-table">
                                            <tbody>
                                                <tr>
                                                    <td class="label-td"> <i class="fa fa-car"></i> Transmission </td>
                                                    <td class="heading-font"> {{$automobile->transmission}} </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class="divider-td"></td>
                                    <td>
                                        <table class="inner-table">
                                            <tbody>
                                                <tr>
                                                    <tr>
                                                    <td class="label-td"> <i class="fa fa-dot-circle-o"></i> Exterior
                                                        Color </td>
                                                    <td class="heading-font"> {{$automobile->colour}} </td>
                                                </tr>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class="divider-td"></td>
                                    <td>
                                        <table class="inner-table">
                                            <tbody>
                                                <tr>
                                                    <td class="label-td"> <i class="fa fa-dot-circle-o"></i> Interior
                                                        Color </td>
                                                        @if($interior_colour)
                                                    <td class="heading-font"> {{$interior_colour->interior_colour}} </td>
                                                    @endif
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class="divider-td"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="inner-table">
                                            <tbody>
                                                <tr>
                                                    <td class="label-td"> <i class="fa fa-key"></i> Registered </td>
                                                    <td class="heading-font"> {{$automobile->registration_place}} </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class="divider-td"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- End stm-single-car-listing-data -->
                    <div class="stm-car-listing-data-single stm-border-top-unit bg-color-border">
                        <h5>Features</h5>
                    </div>
                    <div class="stm-single-listing-car-features">
                        <div class="lists-inline">
                            <?php echo $html ?>
                        </div>
                    </div>
                    <!-- End stm-single-listing-car-features -->

                    <!-- Start of the report section -->
                    <div id="report">
                        <br>
                        <div class="report">
                            @auth
                            @if($savedListing && $savedListing->listingId == $automobile->id && $savedListing->userId == Auth::user()->id)
                            <button type="button" class="btn btn-success" >
                                <i class="fa fa-heart"  aria-hidden="true"></i> <span>Saved</span>
                            </button>
                            @else
                            <button type="button" class="btn btn-success" id="save" value="{{$automobile->id}}">
                                <i class="fa fa-heart"  aria-hidden="true"></i> <span>Save</span>
                            </button>
                            @endif
                            @else
                            <button type="button" class="btn btn-success" href="#myModal" data-toggle="modal">
                                <i class="fa fa-heart"  aria-hidden="true"></i> <span>Save</span>
                            </button>
                            @endauth
                        </div>
                    </div>


                    <div class="modal fade" id="report-modal" tabindex="-1" role="dialog" aria-labelledby="report-modal"
                        aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterTitle">Report this listing</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form>
                                    <div class="modal-body">
                                        <p>Please select the appropriate option for your concern regarding this listing. We will review your report and determine whether it violates our Listing Criteria or isn't suitable for us.</p>
                                        <div class="form-row">
                                            <div class="col-md-12 mb-3">
                                                <select class="custom-select" required>
                                                    <option value="" selected disabled>Report this listing</option>
                                                    <option value="Inappropriate content">Inappropriate content</option>
                                                    <option value="Misleading content">Misleading content</option>
                                                    <option value="Terms of use violation">Terms of use violation</option>
                                                    <option value="Copyright infringement">Copyright infringement</option>
                                                    <option value="Exclusive listing rights">Exclusive listing rights</option>
                                                    <option value="Other">Other</option>
                                                </select>
                                            </div>
                                            <div class="col-md-12 mb-3">
                                                <input type="text" class="form-control" id="name" placeholder="name"
                                                    value="" required>
                                            </div>
                                            <div class="col-md-12 mb-3">
                                                <input type="email" class="form-control" id="email" placeholder="email"
                                                    value="" required>
                                            </div>
                                            <div class="col-md-12 mb-3">
                                                <textarea class="form-control" rows="5" id="message" placeholder="comments"
                                                    value="" required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                        <button class="btn btn-danger" type="submit">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- End Col md 9 -->
                <div class="col-md-3">
                    <div class="stm-single-listing-car-sidebar">
                        <div class="dealer-contacts">
                           <!-- <div class="dealer-contact-unit purchase">
                                <a href="#" class="buy-btn">Purchase</a>
                                @if($automobile->priceType == 'Negotiable')
                                <a href="suggestprice/{{$automobile->id}}" class="suggest-btn">Make an Offer</a>
                                @endif
                            </div>-->
                            <!-- <div class="dealer-contact-unit address">
                                <i class="fa fa-map-marker"></i>
                                <div class="address">Démouville, France</div>
                            </div>
                            <div class="dealer-contact-unit phone">
                                <i class="fa fa-phone"></i>
                                <div class="phone heading-font">(88*******</div>
                                <span class="stm-show-number" data-id="628">Show number</span>
                            </div> -->
                        </div>
                        <!-- <div class="map">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30710983.209769644!2d64.45235976587381!3d20.01273993518969!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30635ff06b92b791%3A0xd78c4fa1854213a6!2sIndia!5e0!3m2!1sen!2sin!4v1597646766464!5m2!1sen!2sin"
                                width="100%" height="250" frameborder="0" style="border:0;" allowfullscreen=""
                                aria-hidden="false" tabindex="0"></iframe>
                        </div> -->
                        <div class="dealerDetails border">
                            <div class="brand">
                                @if($brandImg != null)
                                <img src="{{url(('uploads/'.$brandImg->filename))}}" alt="">
                                @endif  
                            </div>
                            <div class="agentsContainer">
                                <div class="agents">
                                    <div class="agentPhoto">
                                    @if($agentImage != null)
                                        <img src="{{url(('uploads/'.$agentImage->filename))}}" alt="">
                                    @endif    
                                    </div>
                                    <div class="agentDetails">
                                        <div class="agentName font-weight-bold">
                                            {{$automobile->agentname}} 
                                        </div>
                                        <div class="agentNumber">
                                        {{$automobile->agentMobile}}
                                        </div>
                                    </div>
                                </div>
                                
                            </div>   
                            <div class="propertyDetails p-3">
                                <div class="propertyAddress">
                                @if($city != null)
                                {{$city->city_name}}
                                @endif
                                @if($state != null)
                                {{$state->state_name}}
                                @endif
                                @if($country != null)
                                {{$country->country_name}}
                                @endif
                                </div>                                
                            </div>
                        </div>
                        <div>
                            <div class="stm-border-bottom-unit bg-color-border">
                                <h5>Contact seller</h5>
                            </div>
                            <div class="contact-seller">
                                <!-- <div class="d-flex">
                                    <div class="seller-profile">
                                        <img src="https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png"
                                            alt="seller" width="40" height="40">
                                    </div>
                                    <div class="seller-info">
                                        <h6>Varified Seller Since 2016</h6>
                                        <ul>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <span><a href="#"> Reviews: <span id="numbers">60</span></a></span>
                                        </ul>
                                    </div>
                                </div>
                                <p class="seller-city">This seller is from Démouville, France</p> -->
                                
                                @if(Route::has('login'))
                                @auth
                                <?php 
                                $checkMessage = null;
                                if($message->firstWhere('messageFrom', '=', Auth::user()->id) != null)
                                $checkMessage = $message->firstWhere('messageFrom', '=', Auth::user()->id)->firstWhere('product_id', '=', $productId);
                                ?>
                                @if($checkMessage != null)
                                <a class="contact-btn" href="/mymessages" role="button"
                                    >Contact Seller  <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                    @else
									 <!--<a class="contact-btn" data-toggle="collapse" href="#contact-seller-form" role="button"-->
          <!--                          aria-expanded="false" aria-controls="contact-seller-form">Contact Seller  <i class="fa fa-angle-down" aria-hidden="true"></i></a>                                   -->
                                    @endif
                                @else
                                <a href="#myModal" class="contact-btn" data-toggle="modal">Contact Seller</a>    
                                @endauth
                                @endif 
                            </div>
                            <div class="collapse stm-single-car-contact bg-color show" id="contact-seller-form">
                                <div role="form">
                                    <form method="POST" id="enquiyFormCar" action="{{ url('/car/enquery') }}">
                                        @csrf
                                            <div class="form-group">
                                                <input type="text" placeholder="Name" id="nameform" name="name" value="{{ old('name') }}" class="form-control" >
                                                    @error('name')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                            </div>
                                            <input type="hidden" name="productid" value="{{$productId}}" class="from-control" value="">
                                            <input type="hidden" name="agentId" value="{{$automobile->agent_id}}" class="from-control" value="">
                                            @if(Route::has('login'))
                                            @auth
                                            <input type="hidden" name="userId" value="{{Auth::user()->id}}" class="from-control" value="">
                                            @endauth
                                            @endif
                                            <div class="form-group">
                                                <input type="number" placeholder="Phone"  id="telephoneForm" name="mobile"  class="from-control" ><span id="phone"></span>
                                                    @error('mobile')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                            </div>
                                            <div class="form-group">
                                                <!-- <input type="email" placeholder="Email" class="from-control" required> -->
                                            </div>
                                            <div class="form-group">
                                                <select class="from-control" name="country" id="countryForm"
                                                    data-role="country-selector" value="default"></select>
                                            </div>

                                            <div class="form-group">
                                                <textarea placeholder="Message"  value="{{ old('message') }}" name="message"  class="form-control"
                                                    >Hi, I am interested in your listing</textarea>
                                                    @error('message')
                                                        <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                            </div>
                                            <div class="form-group">
                                                <input type="submit" value="Send enquiry"
                                                    class="read-more bg-color-border">
                                            </div>
                                        </form>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="boredr-top bg-color-border">
                            <div class="seller-details">
                                <h6 class="seller-status">For Sale by</h6>
                                <h3 class="seller-name">{{$automobile->dealer_name}}</h3>
                                <h6 class="seller-location">{{$automobile->dealer_city .', '.$automobile->dealer_country}}</h6>
                                <div class="member">
                                    <h5><i title="Our Member" class="fa fa-id-badge" aria-hidden="true"></i> <span>: Since {{date('Y', strtotime($automobile->dealer_time))}}</span> </h5>
                                    <p><i class="fa fa-check-square-o" aria-hidden="true"></i> Trusted seller</p>
                                </div>
                            </div>
                        </div> -->
                    </div>

                </div>

                <!-- <div class="col-12">
                    <div id="comment-section">
                        <div class="stm-car-listing-data-single stm-border-top-unit bg-color-border ">
                            <h5>Leave a comment</h5>
                        </div>
                        <div class="comment-box">
                            <div class="comment-container theme--light">
                                <div class="comments">
                                    <div class="card v-card v-sheet theme--light elevation-2">
                                        <div class="sign-in-wrapper">
                                            <form>
                                                <div class="form-row">
                                                    <div class="col-md-6 mb-3">
                                                        <input type="text" class="form-control" id="name"
                                                            placeholder="Name" value="" required>
                                                    </div>
                                                    <div class="col-md-6 mb-3">
                                                        <input type="email" class="form-control" id="email"
                                                            placeholder="Email" value="" required>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-12 mb-3">
                                                        <textarea class="form-control" id="description"
                                                            placeholder="Type Your Comment Here" value=""
                                                            required></textarea>
                                                    </div>
                                                </div>
                                                <button class="btn read-more " type="submit">Post Comment <i class="fa fa-chevron-circle-right" style=" font-size: 16px; margin-left: 5px;" aria-hidden="true"></i></button>
                                            </form>
                                            <p class="error-message"></p>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="card v-card v-sheet theme--light elevation-2">
                                            <div class="header">
                                                <div class="v-avatar avatar" style="height: 50px; width: 50px;"><img
                                                        src="https://images.unsplash.com/photo-1490894641324-cfac2f5cd077?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=100&q=70">
                                                </div>
                                                <span class="displayName title">John Doe</span> <span
                                                    class="displayName caption">2 days ago</span>
                                            </div>
                                            <div class="wrapper comment">
                                                <p>Fusce sodales magna id porta egestas. Nulla massa est, hendrerit nec
                                                    auctor vitae, porta ut est.</p>
                                            </div>
                                            <div class="actions">
                                            </div>
                                            <div class="v-dialog__container" style="display: block;"></div>
                                        </div>
                                        <div class="answers">
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div> -->
            
            </div>
        </div>
    </div>
    <!-- End single-product -->
    @include('tangiblehtml.innerfooter')
    
    <script src="/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/lightgallery@1.7.3/dist/js/lightgallery.js"></script>
    <!-- <script src="js/lightgallery.min.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
        </script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/jquery.countrySelector.js"></script>
    <script src="/assets/js/local.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#buttonsubmit').on('click',function(){
			
		var isError = false;

		var nameForm = $('#nameForm').val();
		var mobile = $("#telephoneForm").val();
		var countryForm = $('#countryForm').val();
        
			
       	if(mobile.length !=10){
               phone.style.color = 'red';
               phone.innerHTML = "Required 10 digits!"
            	isError = true;	
                   
           }else {
           	phone.style.color = 'green';
           	phone.innerHTML = "";
           } 
       	          
        
       if(nameForm == ""){
			$('#nameForm').css('border','1px solid red');
			isError = true;
			}else{
				
				}


       if(countryForm == ""){
			$('#countryForm').css('border','1px solid red');
			isError = true;
			}else{
				
				}

       if(isError == false){
           
    	   document.getElementById("enquiyFormCar").submit();
           }

    	});

$("#telephoneForm").keyup(checkphonenumber);
    
    function checkphonenumber() {

        var mobile = $("#telephoneForm").val();
		
        if(mobile.length !=10){
            
            phone.style.color = 'red';
            phone.innerHTML = "Required 10 digits!"
        }else {
        	phone.style.color = 'green';
        	phone.innerHTML = "";
        }
    }
            
            
            $("#lightgallery").lightGallery({
                selector: '.item'
            });
            
            $("#save").click(function(){
        	  var listingId = $(this).val();
        	  $('#loading-image').show();
        	  $.ajax({
        	    url: "/user/savelisting",
        	    type: "POST",
        	    data: {listingId: listingId, category: 'Automobiles', _token: '{{csrf_token()}}' },
        	    success : function(data){
        	    	var html = `<i class="fa fa-heart"  aria-hidden="true"></i> <span>Saved</span>`;
        	    	$('#save').html(html);
        	      }
        	    });
        	  });
        });
    </script>
</body>

</html>