<!DOCTYPE html>
<html lang="en">
@include('tangiblehtml.landing_page_header')
<body>
    @include('tangiblehtml.innerheader')
    <div class="custom-top-home-wrapper pt-5 ">
    <div class="container my-5">
        <div class="faq_queustion_category">
            <div class="main-heading ">
                <h3>{{$getSubCatName->name}}</h3>
            </div>
            <ul>
                @foreach($FAQListing as $faq)
                <li class="accordian">
                    <h2 class="faq_head">{{$faq->question}}</h2>
                    <div class="faq_content">
                        <p>{{$faq->answer}}</p>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@include('tangiblehtml.innerfooter')

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {

            $('.faq_queustion_category .faq_content').hide();
            $('.accordian').click(function() {
                if ($(this).hasClass("active")) {
                    $(this).removeClass("active").find(".faq_content").slideUp();
                } else {
                    $(".accordian.active .faq_content").slideUp();
                    $(".accordian.active").removeClass("active");
                    $(this).addClass("active").find(".faq_content").slideDown();
                }
                return false;
            });

        })
    </script>
