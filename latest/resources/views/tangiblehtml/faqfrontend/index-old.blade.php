<!DOCTYPE html>
<html>
	@include('tangiblehtml.landing_page_header')
 		<body>
 			 @include('tangiblehtml.innerheader')
			<div class="row custom-top-home-wrapper pt-5">
	 			<div class="col-md-4 container mt-5">
	 				<table class="table border stripped"> 
	 					<tbody>
	 						<?php $i=$j=$k=1; ?>
			 				<tr>
			 					<th>Watch</th>
			 				@if(!empty($faqWatch[0]->categoryId))
			 					@foreach($faqWatch as $faqW)
				 					@if($faqW->categoryId == 1)
				 						<tr><th>Que:{{$i}}-: {{$faqW->question}}</th></tr>
				 						<tr><td><strong>Ans-:</strong> {{$faqW->answer}}</td></tr>
				 						@endif
				 					<?php $i++; ?>
				 				@endforeach
				 			@else
				 				<th>No Records found</th>
				 			@endif
				 			</tr>

				 			<tr>
				 				<th>Automobiles</th>
				 			@if(!empty($faqAutomobiles[0]->categoryId))
				 				@foreach($faqAutomobiles as $faqA)		
				 					@if($faqA->categoryId == 2)
				 						<tr>
				 							<td>
				 								Que:{{$j}}-: {{$faqA->question}}<br>
				 								Ans-: {{$faqA->answer}}
				 							</td>
				 						</tr>
				 					@endif	
				 					<?php $j++; ?>
				 				@endforeach
				 			@else
				 				<th>No Records found</th>
			 				@endif	
			 				</tr>

			 				<tr>
			 					<th>Real Estate</th>
				 			@if(!empty($faqRealEstate[0]->categoryId))
				 				@foreach($faqRealEstate as $faqR)		
				 					@if($faqR->categoryId == 3)
				 						<tr><th>Que:{{$k}}-: {{$faqR->question}}</th></tr>
				 						<tr><td><strong>Ans-:</strong> {{$faqR->answer}}</td></tr>
				 					@endif	
				 					<?php $k++; ?>
				 				@endforeach
				 			@else
				 				<th>No Records found</th>
				 			@endif
				 			</tr>
						</tbody>
		 			</table>
	 			</div>
 			</div>
    	</body>
    @include('tangiblehtml.innerfooter')
</html>