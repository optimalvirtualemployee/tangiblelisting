<!DOCTYPE html>
<html lang="en">
@include('tangiblehtml.landing_page_header')
<body>
    @include('tangiblehtml.innerheader')
    <div class="custom-top-home-wrapper pt-5 ">
    <div class="container my-5">
        <div class="faq_topics_category">
            <div class="mast__cat topic__cat mb-2">
                <div class=" row topic__cat_head">
                    <div class="col-lg-2 col-sm-3 __cat_icon">
                        <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                    </div>
                    <div class="col-lg-10 col-sm-9 __cat_text">
                        <h1 class="display-6">{{strtoupper($CatName)}}</h1>
                        <!-- <p class="lead">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        <div class="row">
                            <div class="col-lg-1 col-sm-2 author_img">
                                <img src="img/dummy-user.jpg" alt="">
                            </div>
                            <div class="col-lg-11 col-sm-10 author_content">
                                <p>25 artivales in this collection</p>
                                <small>Written by <strong>John Doe</strong></small>
                            </div>
                        </div> -->
                    </div>
                </div>

                <!-- Master category Questions -->
                <ul class="master_topics_list">
                    @foreach($getSubCat as $sbcat)
                    <li>
                        <a href="/faq_page/{{$slug}}/{{$sbcat->categoryId}}/{{$sbcat->id}}" class="row">
                            <div class="col-11 __cat_text">
                                <h1 class="display-6">{{$sbcat->name}}</h1>
                                <!-- <p class="lead">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                <div class="row">
                                    <div class="col-lg-1 col-sm-2 author_img">
                                        <img src="img/dummy-user.jpg" alt="">
                                    </div>
                                    <div class="col-lg-11 col-sm-10 author_content">
                                        <p>25 artivales in this collection</p>
                                        <small>Written by <strong>John Doe</strong></small>
                                    </div>
                                </div> -->
                            </div>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
@include('tangiblehtml.innerfooter')