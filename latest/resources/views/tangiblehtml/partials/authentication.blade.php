<script src="https://cdn.rawgit.com/PascaleBeier/bootstrap-validate/v2.2.0/dist/bootstrap-validate.js" ></script>
    <section>               
        <!-- Modal HTML -->
        <div id="myModal" class="modal fade ">
            <div class="modal-dialog modal-login">
                <div class="modal-content">
                    <div id="error"></div>
                    <div id="loginForm">
                        <div class="modal-header">                       			
                            <h4 class="modal-title">Member Login</h4>	
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">                        
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" placeholder="Username or email" required="required" id="usernameEmail"><span id="emailcheck"></span>		
                            </div>
                            <div class="login">
                                <div class="form-group">
                                    <input type="password" class="form-control d-none" name="password" placeholder="Password" required="required" id="rPassword"> 	
                                </div>      
                            </div>
                            <div class="form-group">
                                <button id="userLogin" class="btn btn-primary btn-lg btn-block login-btn">Login</button>
                            </div>
                            <div class="modal-footer">
                                <a href="#forgotPassword" class="forgetPasword">Forgot Password?</a>
                            </div>         
                        </div>
                    </div>
                    @if($errors->any())
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger">
                                {{$error}}
                            </div>
                        @endforeach
                    @endif
                    <form class="needs-validation d-none" novalidate action="user/newUserRegister" method="post" id="registerForm">
                    @csrf
                        <div class="modal-header">                       			
                            <h4 class="modal-title">Register</h4>	
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body"> 
                            <div class="register">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="first_name" placeholder="First Name" required="required">	
                                    <div class="invalid-feedback">This Field is required</div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="last_name" placeholder="Last Name" required="required">	
                                    <div class="invalid-feedback">This Field is required</div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="phone" id="mobileNumber" placeholder="Phone No" minlength="10" maxlength="10" required="required">
                                    <span id="message"></span>	
                                    <div class="invalid-feedback">This Field is required</div>
                                </div> 
                                <div class="form-group">
                                    <input type="email" class="form-control" id="email"  placeholder="email" required="required" disabled>
                                    <input type="hidden" class="form-control" id="useremail" name="email" placeholder="email">	
                                </div> 
                                <div class="form-group">
                                    <input type="password" class="form-control" id="UserPassword" name="password" placeholder="Password" required="required" minlength="8">	
                                    <div class="invalid-feedback">Password should be 8 digits minimum</div>
                                </div>   
                                <div class="form-group">
                                    <input type="password" class="form-control" id="confirmPassword" name="confirmpassword" placeholder="Confirm Password" required="required" minlength="8">
                                    <div class="registrationFormAlert" style="color:red;" id="CheckPasswordMatch">
                                        <div class="invalid-feedback">Password should be 8 digits minimum</div>	
                                    </div>
                                    <div class="form-group mt-4">
                                        <button type="submit" class="btn btn-primary btn-lg btn-block login-btn">Register</button>
                                    </div>
                                </div>
                            </div>                        
                        </form>
                    </div>
                </div>
            </div>  
            <div id="forgotPassword" class="modal fade show">
                <div class="container">
                <!-- Outer Row -->
                    <div class="row justify-content-center">
                      <div class="col-xl-10 col-lg-12 col-md-9">
                        <div class="card o-hidden border-0 shadow-lg my-4">
                            <div class="card-body p-0">
                                <!-- Nested Row within Card Body -->
                                <div class="row">
                                    <i class="fa fa-times closeWindow"></i>
                                    <div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
                                    <div class="offset-lg-2 col-lg-8">
                                        <div class="p-5">
                                            <div class="text-center">
                                                <h1 class="h4 text-gray-900 mb-2">Forgot Your Password?</h1>
                                                <p class="mb-4">We get it, stuff happens. Just enter your email address below and we'll send you a link to reset your password!</p>
                                            </div>
                                            <form method="POST" action="{{ route('password.email') }}" class="user">
                                                @csrf
                                                    <div class="form-group">
                                                        <label for="email" class="col-md-4 col-form-label text-md-right">
                                                            {{ __('E-Mail Address') }}
                                                        </label>
                                                        <input id="email_" type="email" class="form-control  form-control-user @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Enter Email Address..." autofocus>
                                                        @error('email')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <button type="submit" class="btn btn-primary btn-user btn-block">
                                                        {{ __('Send Password Reset Link') }}
                                                    </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </section>