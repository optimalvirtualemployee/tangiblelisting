                                
                                                      <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Terms & Conditions</title>
    <style>
        * {
            font-family: arial,sans-serif;
            color:#444;
        }

        .headingColor {
            color: #084376;
        }

        .noticeViewCn {
            border: 1px solid;
            padding: 15px;
            margin-top: 20px;
        }

        .txtCenter {
            text-align: center;
        }

        .txtItalic {
            font-style: italic;
        }
        table{}
        table tr{}
        table tr td{
            font-size:12px;
        }

        .txtUnderline {
            text-decoration: underline;
        }


        .d-flex {            
            margin: 10px 0;
            display:table;
        }

        .d-flex p {
            margin: 0px;
            display:table-cell;
        }

        .d-flex>span {
            margin-right: 10px;
            display:table-cell;
        }

        .flex-column {
            flex-direction: column;
        }

        .mt-10 {
            margin-top: 10px !important;
        }
        .mt-30 {
            margin-top: 30px;
        }
    </style>
</head>
<body>
    <div class="heding txtCenter">
        <h3 class="headingColor txtCenter">- TrustedCheckout - </h3>
        <h3 class="headingColor txtCenter">- Standard Terms and Conditons of Sale - </h3>
        <h5 class="headingColor txtCenter"> As of March 16th 2018</h5>
    </div>

    <div class="" style="font-size:10px;">
        <p>THIS AGREEMENT CONTAINS AN ARBITRATION CLAUSE. PLEASE READ THIS PROVISION CAREFULLY, AS IT AFFECTS THE
            PARTIES’ LEGAL RIGHTS. IT PROVIDES THAT ANY CLAIM RELATING TO THIS AGREEMENT MUST BE RESOLVED BY 
            <span class="txtUnderline">BINDING ARBITRATION.</span> THE PARTIES ARE ENTITLED TO A FAIR HEARING, BUT 
            THE ARBITRATION PROCEDURES ARE SIMPLER AND MORE LIMITED THAN RULES APPLICABLE IN COURT, AND ARBITRATION 
            DECISIONS ARE SUBJECT TO VERY LIMITED REVIEW.
        </p>
        <p class="mt-10">
            CLAIMS MAY BE ARBITRATED ONLY ON AN INDIVIDUAL BASIS. BOTH PARTIES EXPRESSLY WAIVE ANY RIGHT THEY YOU MAY
            HAVE TO ARBITRATE A CLASS ACTION. IF EITHER PARTY CHOOSES TO ARBITRATE A CLAIM, NEITHER PARTY WILL HAVE THE
            RIGHT TO LITIGATE THAT CLAIM IN COURT OR TO HAVE A JURY TRIAL ON THAT CLAIM, OR TO PARTICIPATE IN A CLASS
            ACTION OR REPRESENTATIVE ACTION WITHRESPECT TO SUCH CLAIM.
        </p>
    </div>

    <div class="scopeOfApplication mt-30">
        <h3 class="headingColor">1 . Scope of Application</h3>
        <table>
            <tr valign="top">
                <td style="width:30px;">1.1</td>
                <td style="width:50px;"></td>
                <td style="">
                    <div class="user" style="font-size:12px;margin:0 0 5px 0;">
                     {{seller_detail}}
                    </div>
                    <address style="font-size:12px; font-style:normal;color:#000;padding-left:30px;margin:0 0 10px 0;">
                        {{address}}
                    </address>
                    <div class="contactInfo" style="font-size:12px;padding-left:30px;">
                        <div>Tel: <strong>{{telephone}}</strong></div>
                        <div>Email: <strong>{{email}}</strong></div>
                    </div>
                    <div class="d-flex">
                        
                        <p>
                            (hereinafter: "Dealer") sells goods to customers (each a “Customer”) on the electronic market places
                            of
                            Chrono24 GmbH, Haid-und-Neu-Str. 18, D-76131 Karlsruhe (hereinafter: "Chrono24") (such market place
                            hereinafter also: "Platform"). The following "Standard Terms and Conditions of Sale" (hereinafter:
                            the
                            “Standard Terms”) shall apply to contracts for the purchase of goods (hereinafter also "Purchased
                            Object(s)") entered into by Customer and Dealer via Chrono24’s Trusted Checkout Service (each a
                            “Purchase Contract”). Within the Trusted Checkout Service, Chrono24 acts exclusively as a commercial
                            agent on behalf of the Dealer. Chrono24 will broker, and enable Dealer and Customer to enter into
                            Purchase Contracts via the Platform. Chrono24 itself will not, however, become a contracting party
                            to
                            the Purchase Contracts.
                        </p>
                    </div>
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">1.2</td>
                <td style="width:50px;"></td>
                <td>The text of the Purchase Contracts will not be stored by the Dealer or Chrono24 after the Purchase 
                Contract has been entered into. </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">1.3</td>
                <td style="width:50px;"></td>
                <td>Dealer and Customer acknowledge that these Standard Terms are the only terms that govern the sale of
                    Purchased Objects by Dealer to Customer via the Trusted Checkout Service. No offer or acceptance
                    shall be effective which varies the terms hereof or proposes additional terms. Any such proposals shall be
                    deemed to be rejected unless expressly and specifically approved in writing by Dealer or Customer, as
                    applicable.
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">1.4</td>
                <td style="width:50px;"></td>
                <td>For the avoidance of doubt, the Dealer does not accept deviating terms and conditions used by the
                    Customer. This shall even apply if the Dealer does not expressly object to the incorporation of such
                    terms and conditions.
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">1.5</td>
                <td style="width:50px;"></td>
                <td>Similarly, whenever Purchase Objects are purchased via the Trusted Checkout service, these Standard
                    Terms shall, in case of doubt, govern, irrespective of any deviating contractual terms and
                    conditions used by the Dealer.
                </td>
            </tr>
        </table>
    </div>
    <div class="conclusionofContract">
        <h3 class="headingColor">2 . Conclusion of the contract</h3>
        <table>
            <tr valign="top">
                <td style="width:30px;">2.1</td>
                <td style="width:50px;"></td>
                <td>The listings displayed on the Platform do not constitute a binding offer to enter into a Purchase Contract.</td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">2.2</td>
                <td style="width:50px;"></td>
                <td>If the Customer makes a binding offer to conclude a purchase contract (“buy now”) Chrono24 will confirm 
                the receipt by a confirmation of receipt via E-Mail. The confirmation of receipt will not be considered as a 
                binding acceptance of the offer. </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">2.3</td>
                <td style="width:50px;"></td>
                <td>A purchase contract for the Purchased Object shall be concluded only when the Vendor accepts the offer of 
                the Customer either by express declaration or by dispatch of the watch.</td>
            </tr>
        </table>
    </div>
    <div class="deliveryretentiontitle">
        <h3 class="headingColor">3 . Delivery / retention of title</h3>
        <table>
            <tr valign="top">
                <td style="width:30px;">3.1</td>
                <td style="width:50px;"></td>
                <td>Except if agreed otherwise, Purchased Objects shall be shipped from the Dealer's facility to the
                    address
                    specified by the Customer. All shipping costs, including shipping insurance, shall be borne by the
                    Customer, except if agreed otherwise.
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">3.2</td>
                <td style="width:50px;"></td>
                <td>The delivery periods specified in the binding offer shall be calculated from the date on which the
                    invoice amount is credited.
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">3.3</td>
                <td style="width:50px;"></td>
                <td>Until the purchase price has been paid in full, the Purchased Object shall remain the property of
                    the Dealer.
                </td>
            </tr>
        </table>
    </div>
    <div class="pricepayementprocessterms">
        <h3 class="headingColor">4 . Prices, payment process and terms</h3>
        <table>
            <tr valign="top">
                <td style="width:30px;">4.1</td>
                <td style="width:50px;"></td>
                <td>The prices specified in the Binding Offer of the Dealer include any required sales, VAT or similar statutory 
                tax and any other price components.</td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">4.2</td>
                <td style="width:50px;"></td>
                <td>As described herein, payment shall be made by Customer in advance of the Dealer shipping the Purchased 
Objects. Within the framework of the Trusted Checkout Service, Chrono24, as the commercial agent of 
Dealer, is entitled to collect the purchase price on behalf of the Dealer. Payments will therefore be made 
into an account held by Chrono24 on behalf of Dealer. Bank details will be provided by Chrono24 via the 
Trusted Checkout Service. As soon as the purchase price has been credited to the Chrono24 bank account, 
Chrono24 will inform the Dealer and request that the Dealer ships the Purchased Objects.</td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">4.3</td>
                <td style="width:50px;"></td>
                <td>The invoiced purchase price shall be paid into the specified account within 5 business days from entry 
into the Purchase Contract. The purchase price shall be deemed “paid” on the date of receipt of payment. 
The Dealer shall have the right to withdraw from the Purchase Contract without having to set a deadline if 
the purchase price is not paid in a timely manner.</td>
            </tr>
        </table>
    </div>
    <div class="warrantyCharacterstickslimitations">
        <h3 class="headingColor">5 . Warranty, Characterstics and limitations</h3>
        <table>
            <tr valign="top">
                <td style="width:30px;">5.1</td>
                <td style="width:50px;"></td>
                <td>Subject to the limitations set forth in Section 5.2, Dealer warrants to Customer that, for a period of (a) one 
                year in the case of used/second hand goods or (b) two years for new goods (each, an applicable “Warranty 
                Period”), the Purchased Object will, to the extent consistent with the description of the Purchased Objects 
                provided by the Dealer, be free from material defects in workmanship and be fit for the purposes for 
                which goods of the same type and in the same condition are normally used. The Warranty Period begins 
                from the date of delivery of the Purchased Object</td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">5.2</td>
                <td style="width:50px;"></td>
                <td>The warranty contained in Section 5.1 is subject to the following limitations:</td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">5.2.1</td>
                <td style="width:50px;"></td>
                <td>
                    <p>If the Purchased Object is not advertised as "new" by the Dealer, Dealer’s Binding Offer in respect of such 
                    Purchased Object relates to used/second hand goods. For used/second hand goods, the actual, individual 
                    state of preservation as described in the description of the article and in the article images shall be 
                    decisive for the agreed characteristics. Such decisive description shall be made in accordance with the 
                    standard condition categories which can be accessed at http://www.chrono24.en/info/conditions.htm </p>
                    <p>Except if the description of the condition states otherwise, traces of usage and age (e.g. scratches on strap 
                    and glass/housing and other evidence of normal wear and tear) as well as other non-material deviations 
                    regarding accuracy of the description shall not constitute a “defect”.</p>
                    <p>Flaws, damages or impairments of functions which reduce the value of the goods and which are 
                    represented in the description of the article or in the article images shall become an integral component of 
                    the agreement on characteristics. The existence of such flaws, damages or impairments shall not 
                    constitute a breach of the warranty set forth in Section 5.1</p>
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">5.2.2</td>
                <td style="width:50px;"></td>
                <td>
                    <p>The Purchased Objects are "original products", as that term is further described below: 
                    Watches are objects of daily use which are subject to wear and tear. It is therefore self-evident that 
                    watches require maintenance. A watch which was sent to the manufacturer for revision still is an original, 
                    authentic watch, even if components have been exchanged. The elements affected by such exchange or 
                    replacement often are the protective glass, the watch-face, certain springs and wear parts in the 
                    mechanism, housing sealing, screws or sets of hands. Even for proven experts it is often impossible to 
                    trace all repairs (e.g. each exchanged component and its originality or the use of an identical component 
                    from another manufacturer, such as screws) in a watch when appraising the clockwork mechanism. 
                    Unless an express statement to the contrary is contained in the description of the Purchased Object, the 
                    exchange or replacement of elements shall not be deemed to impair the originality of the Purchased 
                    Object or otherwise constitute a breach of the warranty set forth in Section 5.1.
                    </p>
                    <p>In as far as information on waterproofness was not provided in the description of the Purchased Object, 
                    the warranty set forth in Section 5.1 excludes any warranty in this respect.</p>
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">5.3</td>
                <td style="width:50px;"></td>
                <td>
                    Disclaimer. EXCEPT FOR THE WARRANTY SET FORTH IN SECTION 5.1 (SUBJECT TO SECTION 5.2), 
                    DEALER MAKES NO WARRANTY WHATSOEVER WITH RESPECT TO THE PURCHASED OBJECTS, 
                    INCLUDING ANY WARRANTY OF MERCHANTABILITY; OR WARRANTY OF FITNESS FOR A PARTICULAR 
                    PURPOSE, WHETHER EXPRESS OR IMPLIED BY LAW, COURSE OF DEALING, COURSE OF PERFORMANCE, 
                    USAGE OF TRADE OR OTHERWISE. <span class="txtUnderline">FOR THE AVOIDANCE OF DOUBT, CHRONO24 IS NOT A PARTY TO 
                    PURCHASE CONTRACTS INITIATED VIA THE TRUSTED CHECKOUT SERVICE, AND CHRONO24 MAKES NO 
                    WARRANTY AT ALL WITH RESPECT TO THE PURCHASED OBJECTS.</span>
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">5.4</td>
                <td style="width:50px;"></td>
                <td>
                    Dealer shall not be liable for a breach of the warranty set forth in Section 5.1 unless: (i) Customer gives 
                    written notice of the defect, reasonably described, to Dealer within 14 days of the time when Customer 
                    discovers or ought to have discovered the defect; (ii) Dealer is given a reasonable opportunity after 
                    receiving the notice to examine such Purchased Objects and Customer (if requested to do so by Dealer) 
                    returns such Purchased Objects to Dealer’s place of business at Dealer’s cost for the examination to take 
                    place there; and (iii) Dealer reasonably verifies Customer’s claim that the Purchase Objects are defective.
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">5.5</td>
                <td style="width:50px;"></td>
                <td>
                    The Dealer shall not be liable for a breach of the warranty set forth in Section 5.1 if: (i) Customer makes 
                    any further use of such Purchased Objects after giving such notice; (ii) the defect arises because Customer 
                    failed to follow Dealer’s oral or written instructions as to the use or maintenance of the Purchased Objects; 
                    or (iii) Customer alters or repairs such Purchased Objects without the prior written consent of Dealer
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">5.6</td>
                <td style="width:50px;"></td>
                <td>
                    Subject to Sections 5.2 and 5.5 above, with respect to any such Purchased Objects during the Warranty 
                    Period, Dealer shall, in its sole discretion, either: (i) repair or replace such Purchased Objects (or the 
                    defective part) or (ii) credit or refund the price of such Purchased Objects provided that, if Dealer so 
                    requests, Customer shall, at Dealer’s expense, return such Purchased Objects to Dealer.
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">5.7</td>
                <td style="width:50px;"></td>
                <td>
                    THE REMEDIES SET FORTH IN SECTION 5.6 SHALL BE THE CUSTOMER’S SOLE AND EXCLUSIVE REMEDY 
                    AND DEALER'S ENTIRE LIABILITY FOR ANY BREACH OF THE LIMITED WARRANTY SET FORTH IN 
                    SECTION 5.1.
                </td>
            </tr>
        </table>
    </div>
    <div class="liability">
        <h3 class="headingColor">6 . Liability</h3>
        <table>
            <tr valign="top">
                <td style="width:30px;">6.1</td>
                <td style="width:50px;"></td>
                <td>TO THE EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL EITHER PARTY BE LIABLE FOR 
                ANY CONSEQUENTIAL, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR PUNITIVE DAMAGES, LOST 
                PROFITS OR REVENUES OR DIMINUTION IN VALUE, ARISING OUT OF OR RELATING TO A BREACH OF 
                THESE STANDARD TERMS OR THE PURCHASE CONTRACT, OR OTHERWISE ARISING OUT OF OR 
                RELATING TO THE PURCHASE OF PURCHASED OBJECTS VIA THE PLATFORM, WHETHER OR NOT THE 
                POSSIBILITY OF SUCH DAMAGES HAS BEEN DISCLOSED IN ADVANCE BY CUSTOMER OR COULD HAVE 
                BEEN REASONABLY FORESEEN BY CUSTOMER, REGARDLESS OF THE LEGAL OR EQUITABLE THEORY 
                (CONTRACT, TORT OR OTHERWISE) UPON WHICH THE CLAIM IS BASED.</td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">6.2</td>
                <td style="width:50px;"></td>
                <td>WITHOUT LIMITATION OF THE FOREGOING, IN NO EVENT SHALL DEALER’s AGGREGATE LIABILITY 
                    ARISING OUT OF OR RELATED TO A BREACH OF THESE STANDARD TERMS OR THE PURCHASE 
                    CONTRACT, OR OTHERWISE ARISING OUT OF OR RELATING TO THE PURCHASE OF PURCHASED 
                    OBJECTS VIA THE PLATFORM, WHETHER ARISING OUT OF OR RELATED TO BREACH OF CONTRACT, 
                    TORT (INCLUDING NEGLIGENCE) OR OTHERWISE, EXCEED THE TOTAL OF THE AMOUNTS PAID TO 
                    DEALER FOR THE PURCHASED OBJECTS SOLD HEREUNDER.
                </td>
            </tr>
        </table>
    </div>
    <div class="consumerRevocationRights">
        <h3 class="headingColor">7 . Consumer's Recovation rights</h3>
        <table>
            <tr valign="top">
                <td style="width:30px;"></td>
                <td style="width:50px;"></td>
                <td>
                    <p>If the Customer is a natural person who is purchasing the Purchased Objects for personal or family
                    use outside his or her trade or profession, the Customer has a revocation right which is subject to the
                    following provisions.</p>
                    <div class="noticeViewCn">
                        <p class="txtCenter">Advice on the right of recovation</p>
                        <h6 class="txtUnderline">Recovation Rights</h6>
                        <p class="txtItalic">You may revoke this Contract within fourteen days, without having to state reasons
                            for this decision.
                            The revocation period shall be fourteen days from the day on which you or a third party named by you
                            who is not the
                            carrier has taken possession of the Purchased Object. </p>
                        <p class="txtItalic">
                            In order to exercise your revocation rights, please inform us
                        </p>
                        <address>
                            {{address}}
                        </address>
                        <div class="contactInfo">
                            <div>Tel: <strong>{{telephone}}</strong></div>
                            <div>Email: <strong>{{email}}</strong></div>
                        </div>
                        <p class="txtItalic">
                            by means of an unambiguous declaration (e.g. a letter sent by post, telefax or e-mail) of your
                            decision to revoke this Contract. You may use the enclosed template form, but this is not mandatory.
                        </p>
                        <p class="txtItalic">
                            In order to meet the deadline, it will be sufficient for you to dispatch the notification informing
                            us that you are exercising your right of revocation prior to expiry of the deadline.
                        </p>
                        <h5 class="txtUnderline">Consequence of the revocation</h5>
                        <p class="txtItalic">
                            If you revoke this Contract, we have to reimburse you for all payments which we have received from
                            you, including
                            delivery costs (with the exception of additional costs incurred because you chose a delivery method
                            other than the
                            cheapest standard delivery method offered by us), without undue delay and no later than within
                            fourteen days from
                            the day on which we receive your notification informing us of your revocation of this Contract. For
                            this
                            reimbursement, we shall use the payment method which you used in the original transaction, except if
                            expressly
                            agreed otherwise; we shall never charge fees for such reimbursement
                        </p>
                        <p class="txtItalic">
                            We shall have the right to refuse reimbursement up until we have received return shipment of all
                            Purchased Objects,
                            or you have provided proof that you have returned the Purchased Objects, depending on which occurs
                            earlier.
                        </p>
                        <p class="txtItalic">
                            You will have to send back or hand over the Purchased Objects without undue delay, in any event no
                            later than within
                            fourteen days from the date on which you inform us of the revocation of this Agreement, to
                        </p>
                        <address>
                            {{address}}
                        </address>
                        <p class="txtItalic">
                            The deadline shall be deemed to have been met if you dispatch Purchased Objects prior to expiry of
                            the fourteen day
                            deadline.
        
                        </p>
                        <p class="txtItalic">
                            You shall bear the direct costs of the return shipment of the Purchased Object. These costs are
                            estimated to amount to
                            a maximum of approx. 250 US $.
                            You will only have to bear any loss in value of the Purchased Object if this loss in value is due to
                            you handling the
                            Purchased Objects in a manner which is not necessary in order to examine its condition,
                            characteristics or
                            functioning.
                        </p>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width:30px;"></td>
                <td style="width:50px;"></td>
                <td>
                    <div class="noticeViewCn">
                        <p class="textCenter">Standard withdrwal form</p>
                        <p>(Complete and return this form only if you wish to withdraw from the contract.)</p>
                        <span>To</span>
                        <address>
                            Sky Diamonds <br>
                            43 West <br>
                            47th Street <br>
                            10036 New York <br>
                            United States of America <br>
                        </address>
                        <ul>
                            <li>I/We (*) hereby give notice that I/We (*) withdraw from my/our (*) contract of sale of the
                                following
                                Purchase items(*)/for the provision of the following service</li>
                            <li>Ordered on (*)/received on (*)</li>
                            <li>Name of consumer(s)</li>
                            <li>Address of consumer(s)</li>
                            <li>Signature of consumer(s) (only if this form is notified on paper)</li>
                            <li>Date</li>
                            <li>(*) Delete as appropriate.</li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
        <table>
            <tr valign="top">
                <td style="width:30px;">7.1</td>
                <td style="width:50px;"></td>
                <td>
                    <p>Chrono24 will support Customer within the framework of the Trusted Checkout Service during the 
                    revocation and return shipment of the Purchased Object. Therefore, Customer should send any revocation 
                    declaration to Chrono24. Customer may declare revocation via their myChrono24 account 
                    (Transaction/Problem Report). As an alternative, Costumer may also send your revocation declaration by 
                    post, telefax or e-mail to:</p>
                    <p>Chrono24 GmbH, Haid-und-Neu-Str. 18, D-76131 Karlsruhe, Germany 
                    Fax: +49 721 - 480 889 88, E-Mail: <a href="#">checkout@chrono24.com</a></p>
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">7.2</td>
                <td style="width:50px;"></td>
                <td>
                    Please note that the Purchased Object usually is a particularly high-priced object which, due to its 
                    characteristics, cannot be returned by regular post. In the event of a revocation, the Purchased Object, in 
                    order to prevent loss, is to be returned as a value parcel, insured at the amount of the purchase price. The 
                    Customer may be liable for damages in the event of loss or damages occurred to the Purchase Objects 
                    during shipping. Chrono24 will be ready to support Customer in organising return shipment of the 
                    Purchased Objects and shipping insurance.
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">7.3</td>
                <td style="width:50px;"></td>
                <td>
                    Sometimes, a delivered Purchased Object is taped with protective foil. The protective foil does not impair 
                    the examination of the condition, characteristics and functioning of the Purchased Object. The protective 
                    foil is to prevent damage to the watch during examination. Please note that the resale value of an unworn 
                    watch may be impaired considerably if the protective foil is removed from the watch. The Customer may 
                    bear a loss in value of the Purchased Object in case of removal of the protective foil before revocation of 
                    the Purchase Contract and return of the Purchased Object.
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">7.4</td>
                <td style="width:50px;"></td>
                <td>
                    Please note that the resale value of a watch may be impaired considerably if the original packaging (box) 
                    or accessories are missing, or if the documentation (warranty card, certificates) is incomplete. The 
                    Customer may bear a loss in value of the Purchased Object in case of revocation of the Purchase Contract if 
                    the returned Purchased Object does not include the original packaging and accessories, or the 
                    documentation (warranty card, certificates) are incomplete. Therefore, the Purchased Object should be 
                    sent back in its original packaging (box) with the complete documentation (warranty card, certificates) 
                    and all accessories. 
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">7.5</td>
                <td style="width:50px;"></td>
                <td>
                    In the event of a revocation, any damage to and soiling of the Purchased Objects is to be avoided. If 
                    necessary, please use protective outer packaging in order to ensure sufficient protection against transport 
                    damage and in order to avoid claims for compensation due to damage caused by insufficient packaging.
                </td>
            </tr>
        </table>
    </div>
    <div class="disputeResolution">
        <h3 class="headingColor">8 . Dispute Resolution</h3>
        <table>
            <tr valign="top">
                <td style="width:30px;"></td>
                <td style="width:50px;"></td>
                <td>
                    Dealer and Customer agree that any claim or dispute at law or equity that has arisen or may arise between 
                    them will be resolved in accordance with the provisions set forth in this Section 8. Please read this Section 
                    carefully. It affects the both parties’ rights and will impact how claims either party has against the other 
                    are resolved. 
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">8.1</td>
                <td style="width:50px;"></td>
                <td>
                    Applicable Law. Dealer and Customer agree that the laws of the State of the principle place of business of 
                    Dealer, without regard to principles of conflict of laws, will govern these Standard Terms and any claim or 
                    dispute that has arisen or may arise between the parties in respect of the purchase of the Purchased 
                    Objects, except as otherwise stated herein (the “Governing Law”). To the extent that (a) Customer is a 
                    natural person who is purchasing the Purchased Objects for personal or family use outside his or her trade 
                    or profession, and (b) application of the Governing Law would have the effect of depriving such Customer 
                    of consumer protection legislation which is mandatory according to the law of Customer’s Country and 
                    State of primary residence, the Governing Law shall be supplemented by such mandatory consumer 
                    protection legislation of Customer’s Country and State of primary residence. Dealer and Customer agree 
                    that the United Nations Convention on Contracts for the International Sale of Goods shall not apply to the 
                    sale of Purchased Objects hereunder. 
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">8.2</td>
                <td style="width:50px;"></td>
                <td>
                    Agreement to Arbitrate. Dealer and Buyer each agree that any and all disputes or claims that have arisen 
                    or may arise between Dealer and Buyer relating in any way to or arising out of these Standard Terms or 
                    related to the purchase of Purchased Objects purchased via the Platform shall be resolved exclusively 
                    through final and binding arbitration, rather than in court, except that Customer may assert claims in 
                    small claims court, if Customer’s claims qualify and so long as the matter remains in such court and 
                    advances only on an individual (non-class, non-representative) basis (the “Agreement to Arbitrate”). The 
                    Federal Arbitration Act governs the interpretation and enforcement of this Agreement to Arbitrate
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">8.3</td>
                <td style="width:50px;"></td>
                <td>
                    Prohibition of Class and Representative Actions and Non-Individualized Relief. 
                    DEALER AND CUSTOMER AGREE THAT EACH MAY BRING CLAIMS AGAINST THE OTHER ONLY ON AN 
                    INDIVIDUAL BASIS AND NOT AS A PLAINTIFF OR CLASS MEMBER IN ANY PURPORTED CLASS OR 
                    REPRESENTATIVE ACTION OR PROCEEDING. UNLESS BOTH DEALER ABD CUSTOMER AGREE 
                    OTHERWISE, THE ARBITRATOR MAY NOT CONSOLIDATE OR JOIN MORE THAN ONE PERSON'S OR 
                    PARTY'S CLAIMS AND MAY NOT OTHERWISE PRESIDE OVER ANY FORM OF A CONSOLIDATED, 
                    REPRESENTATIVE, OR CLASS PROCEEDING. ALSO, THE ARBITRATOR MAY AWARD RELIEF (INCLUDING 
                    MONETARY, INJUNCTIVE, AND DECLARATORY RELIEF) ONLY IN FAVOR OF THE INDIVIDUAL PARTY 
                    SEEKING RELIEF AND ONLY TO THE EXTENT NECESSARY TO PROVIDE RELIEF NECESSITATED BY THAT 
                    PARTY'S INDIVIDUAL CLAIM(S). ANY RELIEF AWARDED CANNOT AFFECT OTHER CUSTOMERS OF 
                    DEALER. 
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">8.4</td>
                <td style="width:50px;"></td>
                <td>
                    Arbitration Procedures
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">8.4.1</td>
                <td style="width:50px;"></td>
                <td>
                    <p>Arbitration is more informal than a lawsuit in court. Arbitration uses a neutral arbitrator instead of a 
                    judge or jury, and court review of an arbitration award is very limited. However, an arbitrator can award 
                    the same damages and relief on an individual basis that a court can award to an individual. An arbitrator 
                    also must follow the terms of these Standard Terms as a court would. All issues are for the arbitrator to 
                    decide, except that issues relating to arbitrability, the scope or enforceability of this agreement to 
                    arbitrate, or the interpretation of Section 8.3 ("Prohibition of Class and Representative Actions 
                    and NonIndividualized Relief"), shall be for a court of competent jurisdiction to decide.</p>
                    <p>The arbitration will be conducted by the American Arbitration Association ("AAA") under its rules and 
                    procedures, including the AAA's Consumer Arbitration Rules (as applicable), as modified by this 
                    Agreement to Arbitrate. The AAA's rules are available at www.adr.org. A form for initiating arbitration 
                    proceedings is available on the AAA's website at http://www.adr.org. The arbitration shall be conducted 
                    before a single arbitrator.
                    </p>
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">8.4.2</td>
                <td style="width:50px;"></td>
                <td>
                    The arbitration shall be held in the county in which Customer has his or her primary place of residence or 
                    at another mutually agreed location. If the value of the relief sought is [$25,000] or less, either Dealer or 
                    Customer may elect to have the arbitration conducted based solely on written submissions, which election 
                    shall be binding on Dealer and Customer, subject to the arbitrator's discretion to require an in-person 
                    hearing, if the circumstances warrant. Attendance at an in-person hearing may be made by telephone by 
                    either Party, unless the arbitrator requires otherwise.
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">8.4.3</td>
                <td style="width:50px;"></td>
                <td>
                    <p>Subject to the last two sentences of Section 8.1, the arbitrator will decide the substance of all claims in 
                    accordance with the laws of the State in which Dealer has a principle place of business and will honor all 
                    claims of privilege recognized by law. The arbitrator's award shall be final and binding, and judgment on 
                    the award rendered by the arbitrator may be entered in any court having jurisdiction thereof. </p>
                    <p>Payment of all filing, administration, and arbitrator fees will be governed by the AAA's rules, unless 
                    otherwise stated in this Agreement to Arbitrate. If Customer is able to demonstrate that the costs of 
                    arbitration will be prohibitive as compared to the costs of litigation, Dealer will pay as much of the filing, 
                    administration, and arbitrator fees as the arbitrator deems necessary to prevent the arbitration from 
                    being cost-prohibitive. In the event the arbitrator determines the claim(s) Customer asserts in the 
                    arbitration to be frivolous, Customer agrees to reimburse Dealer for all fees associated with the 
                    arbitration paid by Dealer on Customer’s behalf that Customer would otherwise would be obligated to pay 
                    under the AAA's rules.
                    </p>
                </td>
            </tr>
            
            <tr valign="top">
                <td style="width:30px;">8.5</td>
                <td style="width:50px;"></td>
                <td>
                    With the exception of any of the provisions in Section 8.3 of this Agreement to Arbitrate ("Prohibition of 
                    Class and Representative Actions and Non-Individualized Relief"), if a court decides that any part of this 
                    Agreement to Arbitrate is invalid or unenforceable, the other parts of this Agreement to Arbitrate shall 
                    still apply. If a court decides that any of the provisions in Section 8.3 of this Agreement to Arbitrate 
                    ("Prohibition of Class and Representative Actions and Non-Individualized Relief") is invalid or 
                    unenforceable, then the entirety of this Agreement to Arbitrate shall be null and void. The remainder of 
                    these Standard Terms including all other provisions of Section 8 (Dispute Resolution), will continue to 
                    apply.
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">8.6</td>
                <td style="width:50px;"></td>
                <td>
                    To the extent that the arbitration provision outlined in Article 8 are not applicable as a result of a decision 
                    by the arbitrator or a court order, the parties agree that any claim or dispute that has arisen or may arise 
                    between them must be resolved exclusively by a state or federal court located in County and State of the 
                    principle place of business of Dealer. Dealer and Customer agree to submit to the personal jurisdiction of 
                    the courts located within the County and State of the principle place of business of Dealer for the purpose 
                    of litigating all such claims or disputes.
                </td>
            </tr>
        </table>
    </div>
    <div class="finalProvision">
        <h3 class="headingColor">9 . Final Provision</h3>
        <table>
            <tr valign="top">
                <td style="width:30px;">9.1</td>
                <td style="width:50px;"></td>
                <td>
                    Should one or several of the provisions in this Agreement be or become invalid, this shall not affect the 
                    validity of the remaining provisions.
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">9.2</td>
                <td style="width:50px;"></td>
                <td>
                    These Standard Terms constitute the sole and entire agreement of the parties to the Purchase Contract 
                    with respect to the subject matter contained herein, and supersedes all prior and contemporaneous 
                    understandings, agreements, representations and warranties, both written and oral, with respect to such 
                    subject matter. In the event of any inconsistency between the statements in the body of these Standard 
                    Terms and other documents related to this Purchase Contract these Standard Terms shall control.
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">9.3</td>
                <td style="width:50px;"></td>
                <td>
                    These Standard Terms may only be amended, modified or supplemented by an agreement in writing 
                    signed by Dealer and Customer.
                </td>
            </tr>
            <tr valign="top">
                <td style="width:30px;">9.4</td>
                <td style="width:50px;"></td>
                <td>
                    No waiver by any party of any of the provisions hereof shall be effective unless explicitly set forth in 
                    writing and signed by the party so waiving. No waiver by any party shall operate or be construed as a 
                    waiver in respect of any failure, breach or default not expressly identified by such written waiver, whether 
                    of a similar or different character, and whether occurring before or after that waiver. No failure to 
                    exercise, or delay in exercising, any right, remedy, power or privilege arising from this Agreement shall 
                    operate or be construed as a waiver thereof; nor shall any single or partial exercise of any right, remedy, 
                    power or privilege hereunder preclude any other or further exercise thereof or the exercise of any other 
                    right, remedy, power or privilege.
                </td>
            </tr>
        </table>
    </div>

</body>

</html>
                                                       