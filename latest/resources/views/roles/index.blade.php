@extends('admin.common.innercommondefault')
@section('title', 'Role Management')
@section('content')
<style type="text/css">.bttn{ border: none;color: #4e73df;background: white;}</style>
<div class="container">
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Role Management</h3>
            </div>
            <div class="pull-right">
            @can('role-create')
                <a class="btn btn-primary" href="{{ route('roles.create') }}"> Create New Role</a>
            @endcan
            </div><br>
        </div>
      </div>
      <div class="card o-hidden border-0 shadow-lg">
        <div class="card-body p-5">
            
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            <table class="table table-bordered">
              <tr>
                 <th>No</th>
                 <th>Name</th>
                 <th width="280px">Action</th>
              </tr>
                @foreach ($roles as $key => $role)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $role->name }}</td>
                    <td>
                        <a href="{{ route('roles.show',$role->id) }}"><i class="fas fa-eye"></i></a>&nbsp;&nbsp;
                        @can('role-edit')
                            <a href="{{ route('roles.edit',$role->id) }}"><i class="fas fa-edit"></i></a>&nbsp;&nbsp;
                        @endcan
                        @can('role-delete')
                          {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                                <!-- {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!} -->
                            {!! Form::button('<i class="fas fa-trash-alt"></i>', ['type' => 'submit','class'=>'bttn']) !!}
                          {!! Form::close() !!}
                        @endcan
                    </td>
                </tr>
                @endforeach
            </table>
            {!! $roles->render() !!}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
