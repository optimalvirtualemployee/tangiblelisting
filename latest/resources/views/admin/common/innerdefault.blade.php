<!DOCTYPE html>
<html>
  <head>
   @include('admin.common.innerheader')
  </head>
  <body>
   
     <!-- Counts Section --> 
     @yield('content')
     @include('admin.common.innerfooter')
    </body>
</html>   