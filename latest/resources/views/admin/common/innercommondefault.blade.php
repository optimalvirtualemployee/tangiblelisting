<!DOCTYPE html>
	<html>
  		<head>
   			@include('admin.common.innerheadercommon')
  		</head>
  		<body>
     		@yield('content')
     		@include('admin.common.innerfooter')
    	</body>
	</html>   