@extends('admin.common.innerdefault')
@section('title', 'Update Bracelet')
@section('content')

<div class="container">
    <!-- Outer Row -->
    <div class="row justify-content-center">
      <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-8">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Update Bracelet</h1>
                  </div>
                  @if($errors->any())
                    @foreach($errors->all() as $error)
                      <div class="alert alert-danger">
                    {{$error}}
                      </div>
                    @endforeach
                  @endif
                  <form class="user" id="create_bracelet" method="POST" action="{{ route('bracelet.update',$bracelet_data->id) }}">
                  @method('PATCH')
                  @csrf
                    <div class="form-group">
                    
                      <input id="bracelet_material" class="form-control form-control-user " type="text"  name="bracelet_material" placeholder="Enter Bracelet Material" maxlength="100" value="{{ $bracelet_data->bracelet_material }}"><br>

                      <input id="bracelet_color" class="form-control form-control-user" type="text" maxlength="100"  name="bracelet_color" placeholder="Enter Bracelet Colour" value="{{ $bracelet_data->bracelet_color }}"><br>
                      
                      <input id="type_of_clasp" class="form-control form-control-user" type="text" maxlength="100"  name="type_of_clasp" placeholder="Enter clasp type" value="{{ $bracelet_data->type_of_clasp }}"><br>
                      
                      <input id="clasp_material" class="form-control form-control-user" type="text" maxlength="100"  name="clasp_material" placeholder="Enter clasp material" value="{{ $bracelet_data->clasp_material }}">
                    
                    </div>
                      <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------------ JS SCRIPT SECTION --------------------------------------->
  
<script type="text/javascript">
    $(document).ready(function(){
     
      $("#create_bracelet").validate({
        
        rules: {
         
          bracelet_material: "required",
          bracelet_color: "required",
          type_of_clasp: "required",
          clasp_material: "required"
        
          },
        
        messages: {
          bracelet_material: "Bracelet Material can\'t be left blank",
          bracelet_color: "Bracelet Colour can\'t be left blank",
          type_of_clasp: "Clasp Type can\'t be left blank",
          clasp_material: "Clasp Material cant\'t be left blank"
        },
        
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>
<!------------------------------------------------ JS SCRIPT SECTION --------------------------------------->
@stop
