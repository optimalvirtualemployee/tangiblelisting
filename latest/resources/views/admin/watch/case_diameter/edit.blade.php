@extends('admin.common.innerdefault')
@section('title', 'Update Case Diameter')
@section('content')
<style>
.error {
      color: red;
   }
</style>
<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update Case Diameter</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="updatecase_diameter" method="POST" action="{{ route('case_diameter.update',$case_diameter_data->id) }}">
                  @method('PATCH')
                  @csrf
                  <div class="form-group">
                    <input id="case_diameter" class="form-control form-control-user " type="number" min="0" max="100" step="0.01"  name="case_diameter" placeholder="Enter Case Diameter" value="{{ $case_diameter_data->case_diameter }}">
                  </div>
                  <div class="form-group">
                    <label>Status</label>
                    <select id ="status" name="status" class="form-control" required>
                      <option value="1" {{$case_diameter_data->status == "1" ? 'selected' : ''}}>Active</option>
                      <option value="0" {{$case_diameter_data->status == "0" ? 'selected' : ''}}>Inactive</option>
                    </select>
                  </div> 
                  <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#updatecase_diameter").validate({
    
        rules: {
          case_diameter: {required:true, number:true}
        },
    
        messages: {
          case_diameter: {required: "Case Diameter can\'t be left blank", number:"Please enter numbers only"}
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop
