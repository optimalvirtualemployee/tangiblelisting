@extends('admin.common.innerdefault')
@section('title', 'Update Function')
@section('content')

<div class="container">
    <!-- Outer Row -->
    <div class="row justify-content-center">
      <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-8">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Update Function</h1>
                  </div>
                  @if($errors->any())
                    @foreach($errors->all() as $error)
                      <div class="alert alert-danger">
                    {{$error}}
                      </div>
                    @endforeach
                  @endif
                  <form class="user" id="update_Function" method="POST" action="{{ route('functions.update',$functions_data->id) }}">
                  @method('PATCH')  
                  @csrf
                    <div class="form-group">
                    
                      <input id="chronograph" class="form-control form-control-user " type="text"  name="chronograph" placeholder="Enter Chronograph" maxlength="100" value="{{ $functions_data->chronograph }}"><br>

                      <input id="tourbillion" class="form-control form-control-user" type="text" maxlength="100"  name="tourbillion" placeholder="Enter Tourbillion" value="{{ $functions_data->tourbillion }}"><br>
                      
                      <input id="GMT" class="form-control form-control-user" type="text" maxlength="100"  name="GMT" placeholder="Enter GMT" value="{{ $functions_data->GMT }}"><br>
                      
                      <div class="row">
                        <div class="col-lg-6">
                          <select id="annual_calendar" class="form-control" name="annual_calendar">
                            <option value="">Annual Calender</option>
                            <option value="1" {{ $functions_data->annual_calender == 1 ? 'selected' : '' }}>Yes</option>
                            <option value="0" {{ $functions_data->annual_calender == 0 ? 'selected' : '' }}>No</option>
                          </select>
                        </div>
                        <div class="col-lg-6">
                          <select id="minute_repeater" class="form-control" name="minute_repeater">
                            <option value="">Minute Repeater</option>
                            <option value="1" {{ $functions_data->minute_repeater == 1 ? 'selected' : '' }}>Yes</option>
                            <option value="0" {{ $functions_data->minute_repeater == 0 ? 'selected' : '' }}>No</option>
                          </select>
                        </div>
                      </div>
                      <br>

                      <input id="double_chronograph" class="form-control form-control-user" type="text" maxlength="100"  name="double_chronograph" placeholder="Enter double chronograph" value="{{ $functions_data->double_chronograph }}"><br>

                      <input id="panorma_date" class="form-control" type="date" maxlength="100"  name="panorma_date" placeholder="Enter double chronograph" value="{{ $functions_data->panorma_date }}"><br>

                      <input id="jumping_hour" class="form-control form-control-user" type="number" min="0" step="any" maxlength="100"  name="jumping_hour" placeholder="Enter Jumping hour" value="{{ $functions_data->jumping_hour }}"><br>

                      <select id="alarm" class="form-control" name="alarm">
                        <option value="">Alarm</option>
                        <option value="1" {{ $functions_data->alarm == 1 ? 'selected' : '' }}>Yes</option>
                        <option value="0" {{ $functions_data->alarm == 0 ? 'selected' : '' }}>No</option>
                      </select><br>

                      <div class="row">
                        <div class="col-lg-6">
                          <input id="year" class="form-control form-control-user" type="number" min="0" step="1" maxlength="100"  name="year" placeholder="Enter Year" value="{{ $functions_data->year }}">
                        </div>

                        <div class="col-lg-6">
                          <input id="day" class="form-control form-control-user" type="number" min="0" step="1" maxlength="100"  name="day" placeholder="Enter Day" value="{{ $functions_data->day }}">
                        </div>
                      </div>
                    
                    </div>
                      <button type="submit" class="btn btn-primary btn-user btn-block">Add New</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------------ JS SCRIPT SECTION --------------------------------------->
  
<script type="text/javascript">
    $(document).ready(function(){
     
      $("#update_Function").validate({
        
        rules: {
         
          chronograph: "required",
          tourbillion: "required",
          GMT: "required",
          annual_calendar: "required",
          minute_repeater: "required",
          double_chronograph: "required",
          panorma_date: "required",
          jumping_hour: "required",
          alarm: "required",
          year: "required",
          day: "required"
        
          },
        
        messages: {
          chronograph: "Chronograph can\'t be left blank",
          tourbillion: "Tourbillion can\'t be left blank",
          GMT: "GMT can\'t be left blank",
          annual_calendar: "Annual Calender can\'t be left blank",
          minute_repeater: "Minute Repeater can\'t be left blank",
          double_chronograph: "double Chronograph can\'t be left blank",
          panorma_date: "Panorama Date can\'t be left blank",
          jumping_hour: "Jumping hour can\'t be left blank",
          alarm: "Alarm can\'t be left blank",
          year: "Year can\'t be left blank",
          day: "Day can\'t be left blank"
        },
        
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>
<!------------------------------------------------ JS SCRIPT SECTION --------------------------------------->
@stop
