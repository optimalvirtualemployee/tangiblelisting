@extends('admin.common.innerdefault')
@section('title', 'Update brands Models')
@section('content')
<style>
.error {
      color: red;
   }
</style>
<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
           
            <div class="row">
              
              <div class="col-lg-8">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Edit Models of Brand of Watches</h1>
                  </div>
                   @if($errors->any())
                    @foreach($errors->all() as $error)
                      <div class="alert alert-danger">
                    {{$error}}
                      </div>
                    @endforeach
                  @endif
                  <form class="user" id="brandsmodel" method="POST" action="{{ route('editbrandsmodel') }}">
                  @csrf
                   @foreach ($brandsmodel as $brandsmodels)
                     <div class="form-group">
                      <label>Brands</label>
                    <input id="brandname" value="{{$brandsmodels->watch_brand_name}}" class="form-control form-control-user " type="text"  name="brandname" required  placeholder="Enter Model Name" disabled="disbaled">
                    </div>
                    <div class="form-group">
                      <input id="brandmodelname" value="{{$brandsmodels->watch_model_name}}" class="form-control form-control-user " type="text"  name="brandmodelname" required  placeholder="Enter Model Name">
                      <input type="hidden" name="modelid" value="{{$brandsmodels->id}}"/>
                    </div>
                      <div class="form-group">
                          <label>Status</label>
                          <select id ="brandstatus" name="brandstatus" class="form-control" required>
                     
                             <option value="1" {{$brandsmodels->status == "1" ? 'selected' : ''}}>Active</option>
                             <option value="0" {{$brandsmodels->status == "0" ? 'selected' : ''}}>Inactive</option>
                          </select>
                        
                       </div> 
                    @endforeach
                      <button type="submit" class="btn btn-primary btn-user btn-block">
                                    Update Brand Models
                      </button>
                      
                    
                    
                  </form>
                 
                 
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
  
<script type="text/javascript">
$(document).ready(function(){
 
  $("#brandsmodel").validate({
    
    rules: {
     
      
      brandmodelname: "required",
    
      },
    
    messages: {
     
      brandmodelname: "Please enter the model name created for brand",
      
    },
    
    submitHandler: function(form) {
      form.submit();
    }
  });
});
</script>
@stop
