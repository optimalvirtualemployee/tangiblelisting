@extends('admin.common.innerdefaultreal')
@section('title', 'Create News')
@section('content')

<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Add News</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="createnews" method="POST" action="{{ route('propertynews.store') }}">
                  @csrf
                  <div class="form-group">
                    <input id="title" class="form-control form-control-user " type="text"  name="title" placeholder="Enter Title" value="">
                   </div>
                   <div class="form-group">
                    <input id="content" class="form-control form-control-user " type="text"  name="content" placeholder="Enter Content" value="">
                   </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">Add New</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#createnews").validate({
    
        rules: {
          title: {required: true},
          newsPost: {required: true},
          newsType: {required: true}
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
      $(".btn-success").click(function(){ 
          var html = $(".clone").html();
          $(".increment").after(html);
      });

      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".control-group").remove();
      });
      
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop
