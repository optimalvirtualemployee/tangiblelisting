@extends('admin.common.innerdefaultauto') @section('title', 'Upload
Automobile Image') @section('content')

<div class="container">
	<!-- Outer Row -->
	<div class="row justify-content-center">
		<div class="col-xl-10 col-lg-12 col-md-9">
			<div class="card o-hidden border-0 shadow-lg my-5">
				<div class="card-body p-0">
					<!-- Nested Row within Card Body -->
					<div class="row">
						<div class="col-lg-8">
							<div class="p-5">
								<div class="text-center">
									<h1 class="h4 text-gray-900 mb-4">Upload Automobile Image</h1>
								</div>
								@if($errors->any()) @foreach($errors->all() as $error)
								<div class="alert alert-danger">{{$error}}</div>
								@endforeach @endif
								<form class="user" id="createimage" method="POST"
									action="{{ route('uploadautomobileimage.store') }}"
									enctype="multipart/form-data">
									@csrf
									<div class="form-group">
										<input type="hidden" id="listing_id"
											class="form-control form-control-user " type="text"
											name="listing_id" value="{{$listing_id}}">
										<div class="input-group control-group increment">
											<input type="file" name="photos[]" class="form-control">
										</div>
										<div class="clone hide" hidden>
											<div class="control-group input-group"
												style="margin-top: 10px">
												<input type="file" name="photos[]" class="form-control">
												<div class="input-group-btn">
												<button class="btn btn-primary  btn-danger" type="button">
														<i class="glyphicon glyphicon-remove"></i> Remove
													</button>
													</div>
											</div>
										</div>
										<div class="input-group-btn">
												<button class="btn btn-primary  btn-success" type="button">
													<i class="glyphicon glyphicon-plus"></i>Add
												</button>
											</div>
									</div>
									<button type="submit"
										class="btn btn-primary btn-user btn-block" value="Upload">Upload
										</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
 
	  $(document).ready(function() {

	      $(".btn-success").click(function(){ 
	          var html = $(".clone").html();
	          $(".increment").after(html);
	      });

	      $("body").on("click",".btn-danger",function(){ 
	          $(this).parents(".control-group").remove();
	      });

	    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop
