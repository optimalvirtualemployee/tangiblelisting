@extends('admin.common.innercommondefault')
@section('title', 'Update Currency')
@section('content')
<style>
.error {
      color: red;
   }
</style>
<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update Currency</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="updateCurrencyData" method="POST" action="{{ route('currencyData.update',$currency->id) }}">
                  @method('PATCH')
                  @csrf
                  <div class="form-group">
                    <div class="form-group">
                    <label>Currency Code:</label><input id="currencyCode" class="form-control form-control-user " type="text"  name="currencyCode" placeholder="Enter Currency Code" value="{{ $currency->currency_code}}">
                    </div>
                    <div class="form-group">
                    <label>Currency:</label><input id="currency" class="form-control form-control-user " type="text"  name="currency" placeholder="Enter Currency" value="{{ $currency->currency}}">
                       </div>
                       <div class="form-group">
                          <label>Status</label>
                          <select id ="currencystatus" name="currencystatus" class="form-control" required>
                           <option value="1" {{$currency->status == "1" ? 'selected' : ''}}>Active</option>
                           <option value="0" {{$currency->status == "0" ? 'selected' : ''}}>Inactive</option>
                        </select>
                        </div>
                      </div> 
                  <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
    	$("#updateCurrencyData").validate({
    	    
            rules: {
            	currencyCode: "required",
            	currency: "required"
            },
        
            messages: {
            	currencyCode: "Currency Code can\'t be left blank",
            	currency: "Currency can\'t be left blank"
            },
        
            submitHandler: function(form) {
              form.submit();
            }
          });
        });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop
