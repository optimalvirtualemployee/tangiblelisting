@extends('admin.common.innercommondefault')
@section('title', 'Update Agent Job Position')
@section('content')

<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update Agent Job Position</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="updateagent" method="POST" action="{{ route('agentjobposition.update',$jobPosition->id) }}">
                  @method('PATCH')
                  @csrf
                  <div class="form-group">
                    <label>Job Position:</label><input id="job_position" class="form-control form-control-user " type="text"  name="job_position" placeholder="Enter First Name" value="{{$jobPosition->position_name}}">
                   </div>
                   <div class="form-group">
                    <label>Status</label>
                    <select id ="status" name="status" class="form-control">
                      <option value="1" {{$jobPosition->status == 1 ? 'selected' : ''}}>Active</option>
                      <option value="0" {{$jobPosition->status == 0 ? 'selected' : ''}}>Inactive</option>
                    </select>
                  </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">Add</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
        
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop
