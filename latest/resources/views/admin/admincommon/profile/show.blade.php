@extends('admin.common.innercommondefault')
@section('title', 'User Management')
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg">
            <div class="card-body p-5">
                <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">
                            <h3> Show Profile</h3>
                        </div>
                    </div>
                </div><hr>

                      <div class="card o-hidden border-0 shadow-lg">
        <div class="card-body p-5">
           @if ($message = Session::get('success'))
            <div class="alert alert-success">
              <p>{{ $message }}</p>
            </div>
          @endif

          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
           <tr>
             <th>First Name</th>
             <th>Last Name</th>
             <th>Email</th>
             <th>Roles</th>
             <th width="280px">Action</th>
             
           </tr>
            <tr>
              <td>{{ $user->first_name}}</td>
              <td>{{$user->last_name}}</td>
              <td>{{ $user->email }}</td>
              <td>
                @if(!empty($user->getRoleNames()))
                  @foreach($user->getRoleNames() as $v)
                     <label class="badge badge-success">{{ $v }}</label>
                  @endforeach
                @endif
              </td>
              
              <td>
                   <a href="{{ route('profile.edit',$user->id) }}"><i class="fas fa-edit"></i></a>&nbsp;&nbsp;
              </td>
            </tr>
          </table>
          </div>
        </div><hr>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection