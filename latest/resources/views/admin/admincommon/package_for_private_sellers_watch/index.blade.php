@extends('admin.common.innercommondefault')
@section('title', 'Display Package for Private Sellers Watch')
@section('content')

  <!-- Page Wrapper -->
  <div id="wrapper">
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- End of Topbar -->
        <!-- Begin Page Content -->
        <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Package for Private Sellers Watch</h1>
        @can('superAdmin-create')
        <a href="{{route('package_for_ps_watch.create')}}" class="btn btn-primary btn-icon-split">
          <span class="text">Add New Package</span>
        </a>
        @endcan
        @if(Session::has('success_msg'))
          <div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">{{ Session::get('success_msg') }}</div>
            @endif
            <div id="main"></div>
            <div class="card shadow mb-4" style="margin-top: 10px;">
            <div class="card-body" >
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Package for Private Sellers</th>
                      <th>Status</th>
                      @can('superAdmin-create')
                      <th>Action</th>
                     @endcan 
                    </tr>
                  </thead>
                  
                  <tbody>
                    @foreach ($package_for_private_sellers as $data)
                      <tr>
                        <td>{{ $data->package_for_private_sellers }}</td>
                        <td> @if($data->status =='1') Active  @else Inactive @endif</td>
                        @can('superAdmin-create')
                        <td>
                          <a href="{{route('package_for_ps_watch.edit', $data->id)}}"><i class="fas fa-edit"></i></a> &nbsp; &nbsp;
                           <form action="{{ route('package_for_ps_watch.destroy', $data->id) }}" method="POST" id="delete-form-{{ $data->id }}" style="display: none;">
                                {{csrf_field()}}
                                {{ method_field('DELETE') }}
                                <input type="hidden" value="{{ $data->id }}" name="id">
                           </form>
                           <a href="" onclick="if (!confirm('Are you sure you want to delete?')) return; 
                            event.preventDefault(); document.getElementById('delete-form-{{ $data->id }}').submit();">
                            <i class="fas fa-trash-alt"></i>
                          </a> &nbsp; &nbsp;

                          <!---------- PACKGE SETTING OPTION STARTS HERE ------->
                          <a href="{{url('admin/package_for_ps_watch/package_setting', $data->id)}}">
                            <i class="fas fa-cog"></i>
                          </a>
                          <!---------- PACKGE SETTING OPTION ENDS HERE --------->

                        </td>
                        @endcan
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->

      <!-------------------------- JS SCRIPT SECTION --------------------------------------->
      <script type="text/javascript">
          $(document).ready(function(){
            setTimeout(function(){
                $("div.alert").remove();
            }, 3000 ); 
          });
      </script>
      <!--------------------------- JS SCRIPT SECTION ------------------------------------->
@stop

