@extends('admin.common.innercommondefault')
@section('title', 'FAQ Sub Category Page')
@section('content')
  <div id="wrapper">
    <div id="content-wrapper" class="d-flex flex-column">
      @if(isset($Categories[0]->id))
      <div id="content">
        <div class="container-fluid">
          <h1 class="h3 mb-2 text-gray-800">FAQ Sub Category</h1>
         	@can('superAdmin-create')
         		<a href="{{route('faq-subcat.create')}}" class="btn btn-primary btn-icon-split">
            		<span class="text">Add New Sub Category</span>
          		</a>
         	@endcan
          @if(Session::has('success_msg'))
          	<div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">{{ Session::get('success_msg') }}</div>
          @endif
          <div id="main"></div>
          	<div class="card shadow mb-4" style="margin-top: 10px;">
            	<div class="card-body" >
             		<div class="table-responsive">
               		<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
               			<thead>
                 			<tr>
                 				<th>S.No.</th>
				                <th>Sub Category</th>
                        <th>Category</th>
                        <th>Status</th>
					  				    @can('superAdmin-create')
                      	  <th>Action</th>
                      	@endcan
                    	</tr>
                  	</thead>
		                <tbody>
                      <?php $i=1;?>
                    	@foreach($FAQSubCategoryListing as $faq)
                    		<tr>
                          <td>{{$i}}</td>
                    			<td>{{$faq->name}}</td>
                          <td>
                            @foreach($Categories as $cat)
                              @if($faq->categoryId == $cat->id)
                                {{$cat->name}}
                              @endif
                            @endforeach
                          </td>
                    			<td>
                   					@if($faq->status =='1') Active @else InActive @endif  
                        	</td>
                          @can('superAdmin-create')
                          <td>
                            {!! Form::open(['method'=>'DELETE', 'url' =>route('faq-subcat.destroy', $faq->id),'style' => 'display:inline']) !!}
                            {!! Form::button('<i class="fas fa-trash-alt"></i>', array('type' => 'submit','class' => 'btn','title' => 'Delete Post','onclick'=>'return confirm("Confirm delete?")')) !!}
                            {!! Form::close() !!}
                              <a  href="{{route('faq-subcat.edit',$faq->id)}}">
                                <i class="fas fa-edit"></i>
                              </a> &nbsp; &nbsp;
                          </td>
                        @endcan
                      	</tr>
                        <?php $i++;?>
                    	@endforeach
                  	</tbody>
                	</table>
              	</div>
            	</div>
          	</div>
          </div>
      </div>
    @else
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
              <div class="card-body p-0 alert-danger text-center">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="p-5">
                      There is <b style="color: red;">No FAQ Category found or Active</b>!!
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    @endif
  @stop