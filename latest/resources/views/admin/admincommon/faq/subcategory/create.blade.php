@extends('admin.common.innercommondefault')
@section('title', 'FAQ Sub Category')
@section('content')
<style> .error { color: red; } </style>
<div class="container">
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                  <h1 class="h4 text-gray-900 mb-4">Add Sub Category</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger" style="color:red">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="add_subcat" method="POST" action="{{ route('faq-subcat.store') }}">
                  @csrf
                    <div class="form-group">
                      <label>Sub Category Name</label>
                      <input id="subcategory" class="form-control form-control-user" type="text" name="subcategory" placeholder="Enter your sub-category here" value="{{old('subcategory')}}">
                    </div>
                    <div class="form-group">
                      <label>Category</label>
                      <select name="category" id="category" class="form-control">
                        <option value="">Select Category</option>
                        @foreach($Categories as $cat)
                          <option value="{{$cat->id}}">{{$cat->name}}</option>
                        @endforeach
                      </select>
                    </div>
                      <div class="form-group">
                          <label>Status</label>
                          <select id ="status" name="status" class="form-control">
                            <option value="">Select Status</option>
                            <option value="1">Active</option>
                            <option value="0">InActive</option>
                          </select>
                      </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block">Create</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $(document).ready(function(){
    $("#add_subcat").validate({
      rules: {
        category: "required",
        subcategory: "required",
        status: "required"
      },
      
      messages: {
        category: "SubCategory can't be empty",
        status: "Status can't be empty",
      },
    
      submitHandler: function(form) {
        form.submit();
      }
    }); 

  </script>
@stop
