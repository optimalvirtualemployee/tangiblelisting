@extends('admin.common.innercommondefault')
@section('title', 'FAQ')
@section('content')
<style>
  .error { color: red; }
</style>
<div class="container">
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                  <h1 class="h4 text-gray-900 mb-4">Update Question</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger" style="color:red">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="add_question" method="POST" action="{{ route('faq.update',$editData->id) }}">
                  @csrf
                  @method('PUT')
                    <div class="form-group">
                      <label>Question</label>
                      <input id="question" class="form-control form-control-user" type="text" name="question" placeholder="Enter your question here" value="{{$editData->question}}">
                    </div>
                    <div class="form-group">
                      <label>Answer</label>
                      <textarea id="answer" class="form-control form-control-user" type="text" name="answer" placeholder="Enter your answer here" maxlength="1000">@if($editData->answer){{$editData->answer}}@endif</textarea>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Category</label>
                          <select name="category" id="category" class="form-control">
                            <option value="">Select Category</option>
                            @foreach($Categories as $cat)
                            <option value="{{$cat->id}}" {{$editData->categoryId == $cat->id ? 'selected="selected"' :''}}>{{$cat->name}}</option>
                            @endforeach
                          </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                          <label>Sub Category</label>
                          <select name="subcategory" id="subcategory" class="form-control">
                            <option value="" id="subcat_optfirst">Select Sub Category</option>
                            <!--@foreach($SubCategories as $cat)-->
                            <!--<option value="{{$cat->id}}" {{$editData->subcategoryId == $cat->id ? 'selected="selected"' :''}}>{{$cat->name}}</option>-->
                            <!--@endforeach-->
                          </select>
                        </div>
                    </div>
                  </div>
                  <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Status</label>
                      <select id ="status" name="status" class="form-control">
                        <option value="">Select Status</option>
                        <option value="1" {{$editData->status == '1' ? 'selected="selected"' :''}}>Active</option>
                        <option value="0" {{$editData->status == '0' ? 'selected="selected"' :''}}>InActive</option>
                      </select>
                    </div>
                    </div>
                  </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $(document).ready(function(){
    $("#add_question").validate({
      rules: {
        question: "required",
        answer: "required",
        categories: "required",
        status: "required"
      },
      
      messages: {
        question: "Question can't be empty",
        answer: "Answer can't be empty",
        categories: "Category can't be empty",
        status: "Status can't be empty",
      },
    
      submitHandler: function(form) {
        form.submit();
      }
    }); 
  
  $('#category').change(function(){
      var catID = $(this).val();
      $.ajax({
        url:'/admin/faqsub-cat',
        type:'POST',
        dataType:'JSON',
        data: {categoryId: catID, _token: '{{csrf_token()}}' },
        success:function(data){
          console.log(data);
          $('.subcat_optsecond').remove();
          var option = '';
          var subcategorycheck = "<?php echo $editData->subcategoryId;?>";
          $.each(data,function(index,value){
            if(subcategorycheck == value['id']){
              var select = 'selected';
            }else{
              var select = '';
            }
            option += '<option class="subcat_optsecond" value="'+value['id']+'" '+select+' >'+value['name']+'</option>';
          })
          $('#subcat_optfirst').after(option);
        }
      });
    });
  });

  $(document).ready(function(){
    var catID = $('#category').val();
      $.ajax({
        url:'/admin/faqsub-cat',
        type:'POST',
        dataType:'JSON',
        data: {categoryId: catID, _token: '{{csrf_token()}}' },
        success:function(data){
          console.log(data);
          $('.subcat_optsecond').remove();
          var option = '';
          var subcategorycheck = "<?php echo $editData->subcategoryId;?>";
          $.each(data,function(index,value){
            if(subcategorycheck == value['id']){
              var select = 'selected';
            }else{
              var select = '';
            }
            option += '<option class="subcat_optsecond" value="'+value['id']+'" '+select+' >'+value['name']+'</option>';
          })
          $('#subcat_optfirst').after(option);
        }
      });
    }); 

  </script>
@stop
