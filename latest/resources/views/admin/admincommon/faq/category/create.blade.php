@extends('admin.common.innercommondefault')
@section('title', 'FAQ Category')
@section('content')
<style>
  .error { color: red; }
</style>
<div class="container">
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                  <h1 class="h4 text-gray-900 mb-4">Add Category</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger" style="color:red">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="add_cat" method="POST" action="{{ route('faq-cat.store') }}">
                  @csrf
                    <div class="form-group">
                      <label>Category Name</label>
                      <input id="category" class="form-control form-control-user" type="text" name="category" placeholder="Enter your category here" value="{{old('category')}}">
                    </div>
                    <div class="form-group">
                      <label>Slug</label>
                      <input id="slug" class="form-control form-control-user" type="text" maxlength="500" name="slug" readonly placeholder="Slug">
                    </div>
                      <div class="form-group">
                          <label>Status</label>
                          <select id ="status" name="status" class="form-control">
                            <option value="">Select Status</option>
                            <option value="1">Active</option>
                            <option value="0">InActive</option>
                          </select>
                      </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block">Create</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $(document).ready(function(){
    $("#add_cat").validate({
      rules: {
        category: "required",
        status: "required"
      },
      
      messages: {
        category: "Question can't be empty",
        status: "Status can't be empty",
      },
    
      submitHandler: function(form) {
        form.submit();
      }
    }); 

    $('#category').keyup(function() {
      var slugValue = $('#category').val();
      var newSlug   = slugValue.replace(/ /g, "-");
      $('#slug').val(newSlug.toLowerCase());
    }); 
  });

  </script>
@stop
