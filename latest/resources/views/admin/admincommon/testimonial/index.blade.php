@extends('admin.common.innercommondefault')
@section('title', 'Testimonials')
@section('content')

  <div id="wrapper">
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <div class="container-fluid">
          <h1 class="h3 mb-2 text-gray-800">Testimonials</h1>
          @can('superAdmin-create')
            <a href="{{route('testimonial.create')}}" class="btn btn-primary btn-icon-split">
              <span class="text">Add New Testimonial</span>
            </a>
          @endcan
          
          @if(Session::has('success_msg'))
            <div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">{{ Session::get('success_msg') }}</div>
          @endif
          <div id="main"></div>
          <div class="card shadow mb-4" style="margin-top: 10px;">
            <div class="card-body" >
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Image</th>
                      <th>Designation</th>
                      <th>Message</th>
                      <th>Status</th>
                      <th>Image Status</th>
                      @can('superAdmin-create')
                        <th>Action</th>
                      @endcan
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($data as $value)
                      <tr>
                        <td>{{$value->name}} </td>
                        <td><img style="height: 50px;width:50px;" src="/testimonial_images/{{ $value->image }}"></td>
                        <td>{{$value->designation}} </td>
                        <td>{{$value->message}} </td>
                        <td>{{$value->status == 1 ? 'Active' : 'Inactive'}} </td>
                        <td>{{$value->image_status == 1 ? 'Active' : 'Inactive'}} </td>
                        @can('superAdmin-create')
                          <td>
                            {!! Form::open(['method'=>'DELETE', 'url' =>route('testimonial.destroy', $value->id),'style' => 'display:inline']) !!}
                            {!! Form::button('<i class="fas fa-trash-alt"></i>', array('type' => 'submit','class' => 'deletecountries btn','title' => 'Delete Post','onclick'=>'return confirm("Confirm delete?")')) !!}
                            {!! Form::close() !!}
                              <a  href="{{route('testimonial.edit',$value->id)}}">
                                <i class="fas fa-edit"></i>
                              </a> &nbsp; &nbsp;
                          </td>
                        @endcan  
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    @stop