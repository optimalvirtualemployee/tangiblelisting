@extends('admin.common.innercommondefault')
@section('title', 'Create States')
@section('content')
<style>
.error {
      color: red;
   }
</style>
<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                  <h1 class="h4 text-gray-900 mb-4">Create States</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                     {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="createstates" method="POST" action="{{ route('savestates') }}">
                  @csrf
                  <div class="form-group">
                      <label>Select Country</label>
                      <select id ="countryid" name="countryid" class="form-control">
                        <option value="">Select Country</option>
                         @foreach ($countries as $country)
                           <option value="{{$country->id}}">{{$country->country_name}}</option>
                         @endforeach
                      </select>
                    </div>
                  <div class="form-group">
                    <input id="statename" class="form-control form-control-user " type="text"  maxlength="50" name="statename" placeholder="Enter State">
                  </div>
                  <div class="form-group">
                      <label>Status</label>
                      <select id ="status" name="status" class="form-control">
                       <option value="">Select Status</option>
                        <option value="1" >Active</option>
                        <option value="0" >Inactive</option>
                      </select>
                    </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">Add</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!----------------------------------- JS SCRIPT SECTION  ---------------------------------------------------->
<script type="text/javascript">
    $(document).ready(function(){
     $("#statename").keypress(function (e) {

    		
        	
            var keyCode = e.keyCode || e.which;

            $("#lblError").html("");

            //Regex for Valid Characters i.e. Alphabets.
            var regex = /^[A-Za-z]+$/;

            //Validate TextBox value against the Regex.
            var isValid = regex.test(String.fromCharCode(keyCode));
            if (!isValid) {
                $("#lblError").html("Only Alphabets allowed.");
            }

            return isValid;
        });    
        
      $("#createstates").validate({
        rules: {
          countryid:"required",
          statename: "required",
          status: "required"
        },
        
        messages: {
          countryid: "Please select the Country under which you want to create state",
          statename: "State cannot be empty",
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>
<!----------------------------------- JS SCRIPT SECTION  ---------------------------------------------------->

@stop
