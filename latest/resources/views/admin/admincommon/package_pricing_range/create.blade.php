@extends('admin.common.innercommondefault')
@section('title', 'Create Pricing Range')
@section('content')

<style>

.column {
  float: left;
  width: 25%;
  padding: 10px;
  height: 100px; /* Should be removed. Only for demonstration */
}

</style>

<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-12">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Create Pricing Range</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="createpricingrange" method="POST" action="{{ route('packagepricingrange.store') }}">
                  @csrf
                  <div class="form-group">
                      <label>Select Package</label>
                      <select id ="package" name="package" style = "width: 400px!important;"class="form-control">
                        <option value="">Select Package</option>
                         @foreach ($packages as $package)
                           <option value="{{$package->id}}">{{$package->package_for_private_sellers}}</option>
                         @endforeach
                      </select>
                    </div>
                    
                  <div class="row increment">
                  <div class="dtbl">
                 <div class="column"><input id="from" class="form-control form-control-user " type="number"  name="from[]" placeholder="Enter From Range" value=""></div>
                 <div class="column"><input id="to" class="form-control form-control-user " type="number"  name="to[]" placeholder="Enter To Range" value=""></div>
                 <div class="column"><input id="pricing" class="form-control form-control-user " type="number"  name="pricing[]" placeholder="Enter Pricing" value=""></div>
                 <div class="column addbutton"><button class="btn btn-primary btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add +</button></div> 
                  </div>
                  </div>  
				                
                 
                  <button type="submit" class="btn btn-primary btn-user btn-block">Submit</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
    	$(".btn-success").click(function(){ 
	          var html = '<div class="dtbl"><div class="column"><input id="from" class="form-control form-control-user " type="number"  name="from[]" placeholder="Enter From Range" value=""></div>';
	          	html += '<div class="column"><input id="to" class="form-control form-control-user " type="number"  name="to[]" placeholder="Enter To Range" value=""></div>';
	          	html += '<div class="column"><input id="pricing" class="form-control form-control-user " type="number"  name="pricing[]" placeholder="Enter Pricing" value=""></div>';
	          	html += '<div class="column addbutton"><button class="btn btn-primary btn-danger" type="button"><i class="glyphicon glyphicon-plus"></i>Delete</button></div></div>';

	          	$(".increment").append(html);
	      });

    	$("body").on("click",".btn-danger",function(){ 
	          $(this).parent().parent().remove();
	      });


    	$("#createpricingrange").submit(function(){
    		$(this).find('input[type="number"],select').each(function(){
    			$(this).css('border','');
        	});
        	var isError = false;
				$(this).find('input[type="number"],select').each(function(){	
					if($(this).val()==""){
						$(this).css('border','1px solid red');
						isError = true;
					}
				})

				if(isError == false){
					return true;
				}else{
					return false;
				}
        	})
        });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop
