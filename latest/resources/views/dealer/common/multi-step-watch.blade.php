<script src="/assets/js/jquery.js"></script>
<script src="/assets/js/image-uploader.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/wow.min.js"></script>
<script src="/assets/js/slick.min.js"></script>
<script src="/assets/js/local.js"></script>
<script src="https://js.stripe.com/v3/"></script>


<script>
    $(window).on('load',function(){
        $('#myModal2').modal({backdrop: 'static', keyboard: false});
    });
      // steps JS
      $(document).ready(function () {

    	  $("#countryId").change(function(){
    	    	//$('#loading-image').show();
    	        var countryId = $(this).val();
    	        
    	         if(!countryId) { 
               $('#stateId').prop("disabled", true); // Element(s) are now disabled.
                $('#cityId').prop("disabled", true); // Element(s) are now disabled.
                return false;
           }
    	        
    	        
    	        $.ajax({
    	          url: "/getState",
    	          type: "POST",
    	          data: {countryId: countryId, _token: '{{csrf_token()}}' },
    	          dataType: 'json',
    	          success : function(data){
    	            var html = `<option value="">Select State</option>`;
    	            for (var i = 0; i < data.length; i++) {
    	                var id = data[i].id;
    	                var name = data[i].state_name;
    	                var option = `<option value="${data[i].id}">${data[i].state_name}</option>`;
    	                html += option;
    	              }
    	              $('#stateId').html(html); 
    	              $('#stateId').prop("disabled", false); // Element(s) are now enabled.
    	            }
    	          });
    	        $.ajax({
    				url: "/getCity",
    				type: "POST",
    				data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
    				dataType: 'json',
    				success : function(data){
    					var html = `<option value="">Select City</option>`;
    					for (var i = 0; i < data.length; i++) {
    						  var id = data[i].id;
    						  var name = data[i].city_name;
    						  var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
    						  html += option;
    						}
    					$('#loading-image').hide();
    						$('#cityId').html(html);
    							$('#cityId').prop("disabled", false); // Element(s) are now enabled.
    					}
    				});
    	      });
    	        $("#stateId").change(function(){
    	        var stateId = $(this).val();
    	       // $('#loading-image').show();
    	        $.ajax({
    	          url: "/getCity",
    	          type: "POST",
    	          data: {stateId: stateId, _token: '{{csrf_token()}}' },
    	          dataType: 'json',
    	          success : function(data){
    	            var html = `<option value="">Select City</option>`;
    	            for (var i = 0; i < data.length; i++) {
    	                var id = data[i].id;
    	                var name = data[i].city_name;
    	                var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
    	                html += option;
    	              }
    	            //$('#loading-image').hide();
    	              $('#cityId').html(html);  
    	            }
    	          });
    	      });

    	        $("#country_pi").change(function(){
    	        	$('#loading-image').show();
    	            var countryId = $(this).val();
    	            $.ajax({
    	              url: "/getState",
    	              type: "POST",
    	              data: {countryId: countryId, _token: '{{csrf_token()}}' },
    	              dataType: 'json',
    	              success : function(data){
    	                var html = `<option value="">Select State</option>`;
    	                for (var i = 0; i < data.length; i++) {
    	                    var id = data[i].id;
    	                    var name = data[i].state_name;
    	                    var option = `<option value="${data[i].id}">${data[i].state_name}</option>`;
    	                    html += option;
    	                  }
    	                  $('#state_pi').html(html); 
    	                }
    	              });
    	            $.ajax({
    	    			url: "/getCity",
    	    			type: "POST",
    	    			data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
    	    			dataType: 'json',
    	    			success : function(data){
    	    				var html = `<option value="">Select City</option>`;
    	    				for (var i = 0; i < data.length; i++) {
    	    					  var id = data[i].id;
    	    					  var name = data[i].city_name;
    	    					  var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
    	    					  html += option;
    	    					}
    	    				$('#loading-image').hide();
    	    					$('#city_pi').html(html);	
    	    				}
    	    			});
    	          });
    	            $("#state_pi").change(function(){
    	            var stateId = $(this).val();
    	            $('#loading-image').show();
    	            $.ajax({
    	              url: "/getCity",
    	              type: "POST",
    	              data: {stateId: stateId, _token: '{{csrf_token()}}' },
    	              dataType: 'json',
    	              success : function(data){
    	                var html = `<option value="">Select City</option>`;
    	                for (var i = 0; i < data.length; i++) {
    	                    var id = data[i].id;
    	                    var name = data[i].city_name;
    	                    var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
    	                    html += option;
    	                  }
    	                $('#loading-image').hide();
    	                  $('#city_pi').html(html);  
    	                }
    	              });
    	          });

    	  $("#brand").change(function(){
    	      console.log('------------------------');
    	  		var brandId = $(this).val();
    	  		
    	  		
    	  		if(!brandId) { 
               $('#model').prop("disabled", true); // Element(s) are now disabled.
               return false;
           }
    	  		
    	  	//	$('#loading-image').show();
    	  		$.ajax({
    	  			url: "/getWatchModel",
    	  			type: "POST",
    	  			data: {brandId: brandId, _token: '{{csrf_token()}}' },
    	  			dataType: 'json',
    	  			success : function(data){
    	  				var html = `<option value="">Select Model</option>`;
    	  				for (var i = 0; i < data.length; i++) {
    	  					  var option = `<option value="${data[i].id}">${data[i].watch_model_name}</option>`;
    	  					  html += option;
    	  					}
    	  			//	$('#loading-image').hide();
    	  					$('#model').html(html);	
    	  					 $('#model').prop("disabled", false); // Element(s) are now enabled.
    	  				}
    	  			});
    	  	});

          
          var current_fs, next_fs, previous_fs; //fieldsets
          var opacity;
          var ImgArray = [];
          var ImgArrayDamage = []; 
          const imageArrayfn = function () {
              ImgArray = [];
              ImgArrayDamage = [];
              $('.uploaded img').each(function (index, value) {
                if($(this).parents('#damagePhotos').length > 0){
                  ImgArrayDamage.push($(this).attr('src'));
                }
                else{
                  ImgArray.push($(this).attr('src'));
                }
              });
              imgUploadedfn();
          }
      
          const imgUploadedfn = function () {
              let imgCount = ImgArray.length;
              let imgCountDmg = ImgArrayDamage.length;
              let html = '';
              let imgGallery = '';
              let dmgGallery= '';
              
              if (imgCount > 0) {
                  for (let j = 0; j < imgCount; j++) {                   
                      // html += ' <div class="item" data-src="' + ImgArray[j] + '"><img src="' + ImgArray[j] + '"></div>';
                      imgGallery += '<div><img src="' + ImgArray[j] + '" /></div>';
                  }
                  $('#imgGallery').empty();   
                  $('#imgGallery').append(imgGallery);      
                 
              }  
              if (imgCountDmg > 0) {
                  for (let i = 0; i < imgCountDmg; i++) { 
                    dmgGallery += '<div><img src="' + ImgArrayDamage[i] + '" /></div>';
                  }   
                  $('#dmgGallery').empty();               
                  $('#dmgGallery').append(dmgGallery);      
                  
              } 
                console.log('Images Array - > ', ImgArray)  ;  
                console.log('Damage Images Array - > ', ImgArrayDamage)  ;  
          }
          /* $(".next").click(function () {
              imageArrayfn();
              current_fs = $(this).parent();
              next_fs = $(this).parent().next();      
              //Add Class Active
              $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");      
              //show the next fieldset
              next_fs.show();
              //hide the current fieldset with style
              current_fs.animate({ opacity: 0 }, {
                  step: function (now) {
                      // for making fielset appear animation
                      opacity = 1 - now;
      
                      current_fs.css({
                          'display': 'none',
                          'position': 'relative'
                      });
                      next_fs.css({ 'opacity': opacity });
                  },
                  duration: 600
              });
          }); */      
          $(".previous").click(function () {
              current_fs = $(this).parent();
              previous_fs = $(this).parent().prev();
              //Remove class active
              $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");      
              //show the previous fieldset
              previous_fs.show();
              //hide the current fieldset with style
              current_fs.animate({ opacity: 0 }, {
                  step: function (now) {
                      // for making fielset appear animation
                      opacity = 1 - now;
      
                      current_fs.css({
                          'display': 'none',
                          'position': 'relative'
                      });
                      previous_fs.css({ 'opacity': opacity });
                  },
                  duration: 600
              });
          });
          $('.radio-group .radio').click(function () {
              $(this).parent().find('.radio').removeClass('selected');
              $(this).addClass('selected');
          });
          $(".submit").click(function () {
              return false;
          })
          $('.input-images-1').imageUploader();
		  $('.input-images-2').imageUploader2();
          $( ".uploaded" ).sortable();
          $( ".uploaded" ).disableSelection();

        //Timer Array
          const timerArray = [
            ['security-1.png','10:10'],
            ['security-2.png','12:16'],
            ['security-3.png','01:22'],
            ['security-4.png','01:30'],
            ['security-5.png','01:45'],
            ['security-6.png','02:34'],
            ['security-7.png','03:42'],
            ['security-8.png','04:50'],
            ['security-9.png','06:08'],
            ['security-10.png','05:14'],
            ['security-11.png','07:14'],
            ['security-12.png','08:24'],
            ['security-13.png','09:36'],
            ['security-14.png','10:42'],
            ['security-15.png','11:52'],
            ['security-16.png','12:46'],
            ['security-17.png','01:51'],
            ['security-18.png','02:50'],
            ['security-19.png','05:04'],
            ['security-20.png','08:15']            
            ]     
            //Setting First clock Time            
            let RandomIndexNumber = Math.floor(Math.random() * 20);            
            let RandomImageSecurity1 = timerArray[RandomIndexNumber][0];
            let RandomTimeSecurity1 = timerArray[RandomIndexNumber][1];
            $('.secutrity1 img').attr('src','/assets/img/security-images/'+RandomImageSecurity1+'');
            $('.digitalTime1').text(RandomTimeSecurity1);
            $('.digitalTime1').val(RandomTimeSecurity1);

            //Setting second clock Time
            let RandomIndexNumber2 = Math.floor(Math.random() * 20);
            let RandomImageSecurity2 = timerArray[RandomIndexNumber2][0];
            let RandomTimeSecurity2 = timerArray[RandomIndexNumber2][1];
            $('.secutrity2 img').attr('src','/assets/img/security-images/'+RandomImageSecurity2+'');
            $('.digitalTime2').text(RandomTimeSecurity2);
            $('.digitalTime2').val(RandomTimeSecurity2);	
          
          var isError = false;
     	 $("#next1").click(function(){
				var make = $('#brand').val();
				var model = $('#model').val();

				// console.log('Make ',$('#brand').val());
				// console.log('Model ',$('#model').val());
				
				  if(make == ""){
					$('#brand').css('border','1px solid red');
					isError = true;
					}else{
            $("#brand").removeAttr("style");
						isError = false;
						}
				if(model == ""){
					$('#model').css('border','1px solid red');
					isError = true;
					}else{
					  $("#model").removeAttr("style");
            isError = false;
						}  

        	 if(isError == false){
        		current_fs = $('#next1').parent();
 			next_fs = $('#next1').parent().next();

 			//Add Class Active
 			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

 			//show the next fieldset
 			next_fs.show();
 			//hide the current fieldset with style
 			current_fs.animate({opacity: 0}, {
 			step: function(now) {
 			// for making fielset appear animation
 			opacity = 1 - now;

 			current_fs.css({
 			'display': 'none',
 			'position': 'relative'
 			});
 			next_fs.css({'opacity': opacity});
 			},
 			duration: 600
 			});
            	 }
     	 });

     	$("#pricetType").change(function(){ 
				var htmlcarPrice = '';
				var htmlPrice = '';
        if($(this).val() == 3){
         	htmlcarPrice += '<input id="price" class="form-control form-control-user " type="number"  min="0" name="price" placeholder="Enter Watch Value" value="" disabled>';
       	  /* htmlPrice = '<select id="pricetType" class="form-control "  name="pricetType" disabled> <option value="">Select Price</option>'; */
				$("#price").replaceWith(htmlcarPrice);
				/* $("#pricetType").replaceWith(htmlPrice); */	
            }else{
         	   htmlcarPrice += '<input id="price" class="form-control form-control-user" type="number"  min="0" name="price" placeholder="Enter Watch Value" value="">';
           	 /* htmlPrice = '<select id="pricetType" class="form-control "  name="pricetType"> <option value="">Select Price</option> @foreach ($prices as $price) <option value="{{$price->id}}">{{$price->price}}</option> @endforeach </select>'; */
					$("#price").replaceWith(htmlcarPrice);
					/* $("#pricetType").replaceWith(htmlPrice); */	
	                 } 
         
     });
     	 
     	$("#next2").click(function(){
			var currency = $('#currency').val();
			var country = $('#countryId :selected').val();
   		 	var city = $('#cityId :selected').val();
   		 	var priceType = $('#pricetType :selected').val();
   		 	var caseDiameter = $('#case_diameter :selected').val();
   		 	var movement = $('#movement :selected').val();
   		 	var type = $('#type :selected').val();
   		 	var gender = $('#gender :selected').val();
   		 	var inclusions = $('#inclusions').val();
   		 	var reference_num = $('#reference_num').val();
   		 	var watchCondition = $('#watchCondition :selected').val();
   		 	var status = $('#status :selected').val();

   		 	var powerReserve = $('#powerReserve :selected').val();
   		 	var dialColor = $('#dialColor :selected').val();
   			var braceletMaterial = $('#braceletMaterial :selected').val();
   			var bezelMaterial = $('#bezelMaterial :selected').val();
   			var braceletColor = $('#braceletColor :selected').val();
   			var typeOfClasp = $('#typeOfClasp :selected').val();
   			var claspMaterial = $('#claspMaterial :selected').val();
   			var caseMaterial = $('#caseMaterial :selected').val();
   			var caseMM = $('#caseMM :selected').val();
   			var wrd = $('#wrd :selected').val();
   			var glassType = $('#glassType :selected').val();
   			var timezone = $('#timezone').val();


			
          if(country == ""){
          $('#countryId').css('border','1px solid red');
          isError = true;
          }else{
          $("#countryId").removeAttr("style");
          isError = false;
          }
          if(city == ""){
          $('#cityId').css('border','1px solid red');
          isError = true;
          }else{
          $("#cityId").removeAttr("style");
          isError = false;
          }
          if(priceType == ""){
          $('#pricetType').css('border','1px solid red');
          isError = true;
          }else{
          $("#pricetType").removeAttr("style");
          isError = false;
          }
          if(currency == ""){
          $('#currency').css('border','1px solid red');
          isError = true;
          }else{
          $("#currency").removeAttr("style");
          isError = false;
          }
          if(inclusions == ""){
          $('#inclusions').css('border','1px solid red');
          isError = true;
          }else{
          $("#inclusions").removeAttr("style");
          isError = false;
          }
          if(gender == ""){
          $('#gender').css('border','1px solid red');
          isError = true;
          }else{
          $("#gender").removeAttr("style");
          isError = false;
          }
          if(reference_num == ""){
          $('#reference_num').css('border','1px solid red');
          isError = true;
          }else{
          $("#reference_num").removeAttr("style");
          isError = false;
          } 
          if(caseDiameter == ""){
          $('#case_diameter').css('border','1px solid red');
          isError = true;
          }else{
          $("#case_diameter").removeAttr("style");
          isError = false;
          }	
          if(status == ""){
          $('#status').css('border','1px solid red');
          isError = true;
          }else{
          $("#status").removeAttr("style");
          isError = false;
          }          if(timezone == ""){
          $('#timezone').css('border','1px solid red');
          isError = true;
          }else{
          $("#timezone").removeAttr("style");
          isError = false;
          }
          // if(watchCondition == ""){
          // $('#watchCondition').css('border','1px solid red');
          // isError = true;
          // }else{
          // $("#watchCondition").removeAttr("style");
          // isError = false;
          // }	
          if(powerReserve == ""){
          $('#powerReserve').css('border','1px solid red');
          isError = true;
          }else{
          $("#powerReserve").removeAttr("style");
          isError = false;
          }
          if(dialColor == ""){
          $('#dialColor').css('border','1px solid red');
          isError = true;
          }else{
          $("#dialColor").removeAttr("style");
          isError = false;
          }
          if(braceletMaterial == ""){
          $('#braceletMaterial').css('border','1px solid red');
          isError = true;
          }else{
          $("#braceletMaterial").removeAttr("style");
          isError = false;
          }
          if(bezelMaterial == ""){
          $('#bezelMaterial').css('border','1px solid red');
          isError = true;
          }else{
          $("#bezelMaterial").removeAttr("style");
          isError = false;
          }
          if(braceletColor == ""){
          $('#braceletColor').css('border','1px solid red');
          isError = true;
          }else{
          $("#braceletColor").removeAttr("style");
          isError = false;
          }


          if(typeOfClasp == ""){
          $('#typeOfClasp').css('border','1px solid red');
          isError = true;
          }else{
          $("#typeOfClasp").removeAttr("style");
          isError = false;
          }
          if(claspMaterial == ""){
          $('#claspMaterial').css('border','1px solid red');
          isError = true;
          }else{
          $("#claspMaterial").removeAttr("style");
          isError = false;
          }
          if(caseMaterial == ""){
          $('#caseMaterial').css('border','1px solid red');
          isError = true;
          }else{
          $("#caseMaterial").removeAttr("style");
          isError = false;
          }
          if(caseMM == ""){
          $('#caseMM').css('border','1px solid red');
          isError = true;
          }else{
          $("#caseMM").removeAttr("style");
          isError = false;
          }
          if(wrd == ""){
          $('#wrd').css('border','1px solid red');
          isError = true;
          }else{
          $("#wrd").removeAttr("style");
          isError = false;
          }
          if(glassType == ""){
          $('#glassType').css('border','1px solid red');
          isError = true;
          }else{
          $("#glassType").removeAttr("style");
          isError = false;
          } 

    	 if(isError == false){
    		current_fs = $('#next2').parent();
			next_fs = $('#next2').parent().next();

			//Add Class Active
			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

			//show the next fieldset
			next_fs.show();
			//hide the current fieldset with style
			current_fs.animate({opacity: 0}, {
			step: function(now) {
			// for making fielset appear animation
			opacity = 1 - now;

			current_fs.css({
			'display': 'none',
			'position': 'relative'
			});
			next_fs.css({'opacity': opacity});
			},
			duration: 600
			});
        	 }
 	 });

     	$("#next3").click(function(){
     		var comment = $('#comment').val();

          var attachPhotos = $('#input-images-1').find('.uploaded-image').length;
          if(attachPhotos == 0){
          $('#img1').html('Please attach watch photos here!');
          isError = true;
          }else{
          $('#img1').html('');
          isError = false;
          }
          var securityImgA = $('#security1').val();
          if(securityImgA.length <= 0){
          $('#img2').html('Please upload security first image');
          isError = true;
          return false;
          }else{
          $('#img2').html('');
          isError = false;
          }

          var securityImgB = $('#security2').val()
          if(securityImgB.length <= 0){
          $('#img3').html('Please upload security second image');
          isError = true;
          return false;
          }else{
          $('#img3').html('');
          isError = false;
          }
          if(comment == ""){
          $('#comment').css('border','1px solid red');
          isError = true;
          }else{
          $("#comment").removeAttr("style");
          isError = false;
          } 

    	 if(isError == false){
    		current_fs = $('#next3').parent();
			next_fs = $('#next3').parent().next();

			//Add Class Active
			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

			//show the next fieldset
			next_fs.show();
			//hide the current fieldset with style
			current_fs.animate({opacity: 0}, {
			step: function(now) {
			// for making fielset appear animation
			opacity = 1 - now;

			current_fs.css({
			'display': 'none',
			'position': 'relative'
			});
			next_fs.css({'opacity': opacity});
			},
			duration: 600
			});
        	 }
 	 });

     	$('#next4').click(function() {

   		 	var title = $('#adtitle').val();
   			var brand = $('#brand :selected').text();
			var model = $('#model :selected').text();
			var price = $('#price').val();
			var comment = $('#comment').val();
			var watchCondition = $('#watchCondition :selected').text();
		 	var movement = $('#movement :selected').text();
		 	var caseMaterial = $('#caseMaterial :selected').text();
		 	var braceletMaterial = $('#braceletMaterial :selected').text();
		 	var yom = $('#yom :selected').text();
		 	var gender = $('#gender :selected').text();
		 	var cityId = $('#cityId :selected').text();
		 	var countryId = $('#countryId :selected').text();
		 	var dialColor = $('#dialColor :selected').text();
			
			
			var titleHtml = title + ' ' + brand + ' ' + model;
			$('#title').html(titleHtml);
			$('#priceShow').html(price);
			$('#commentShow').html(comment);
			$('#brandShow').html('Brand: ' +brand);
			$('#modelShow').html('Model: ' +model);
			$('#conditionShow').html('Condition: ' +watchCondition);
			$('#movementShow').html('Movement: ' +movement);
			$('#braceletMaterialShow').html('Bracelet material: ' +braceletMaterial);
			$('#caseMaterialShow').html('Case material: ' +caseMaterial);
			$('#yomShow').html('Year of production: ' +yom);
			$('#genderShow').html('Gender: ' +gender);
			var location = cityId +' ,'+ countryId;
			$('#locationShow').html('Location: ' +location);
			$('#dialColorShow').html('Dial Color: ' +dialColor);
			
			var firstName = $('#firstName').val();
			var lastName = $('#lastName').val();
			var dob = $('#dob').val();
			var address = $('#address').val();
			var zipcode = $('#zipcode').val();
			var country_pi = $('#country_pi').val();
			var city_pi = $('#city_pi').val();
			var phonenumber = $('#phonenumber').val();
			
  		   if(firstName == ""){
				$('#firstName').css('border','1px solid red');
				isError = true;
				}else{
					
					}
  		if(lastName == ""){
			$('#lastName').css('border','1px solid red');
			isError = true;
			}else{
				
				}
  		if(dob == ""){
			$('#dob').css('border','1px solid red');
			isError = true;
			}else{
				
				}
  		if(address == ""){
			$('#address').css('border','1px solid red');
			isError = true;
			}else{
				
				}
  		if(zipcode == ""){
			$('#zipcode').css('border','1px solid red');
			isError = true;
			}else{
				
				}
  		if(country_pi == ""){
			$('#country_pi').css('border','1px solid red');
			isError = true;
			}else{
				
				}
  		if(city_pi == ""){
			$('#city_pi').css('border','1px solid red');
			isError = true;
			}else{
				
				}
  		if(phonenumber == ""){
			$('#phonenumber').css('border','1px solid red');
			isError = true;
			}else{
				
				}  

      		 
   		 if(isError == false){
					imageArrayfn();
    			 current_fs = $('#next4').parent();
     			next_fs = $('#next4').parent().next();

     			//Add Class Active
     			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

     			//show the next fieldset
     			next_fs.show();
     			//hide the current fieldset with style
     			current_fs.animate({opacity: 0}, {
     			step: function(now) {
     			// for making fielset appear animation
     			opacity = 1 - now;

     			current_fs.css({
     			'display': 'none',
     			'position': 'relative'
     			});
     			next_fs.css({'opacity': opacity});
     			},
     			duration: 600
     			});
        		 }
   		 
       	 });

     	$('#next5').click(function() {
   		 

   		 /*  if(!$('#agb').prop('checked')){
   			 $('.generic-errors-top').html('The terms conditions must be accepted.');
   			 isError = true;
			}else{
				isError = false;
				}  */
   		 
   		 if(isError == false){
					//imageArrayfn();
     			 current_fs = $('#next5').parent();
      			next_fs = $('#next5').parent().next();

      			//Add Class Active
      			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

      			//show the next fieldset
      			next_fs.show();
      			//hide the current fieldset with style
      			current_fs.animate({opacity: 0}, {
      			step: function(now) {
      			// for making fielset appear animation
      			opacity = 1 - now;

      			current_fs.css({
      			'display': 'none',
      			'position': 'relative'
      			});
      			next_fs.css({'opacity': opacity});
      			},
      			duration: 600
      			});

    		 }
   		 
       	 });
      });   
// Security Upload Image Script
$(".imageUploadCls").change(function() {  
  const security = $(this).attr('id');
  if (this.files && this.files[0]) {
        let reader = new FileReader();
        reader.onload = function(e) {                 
            $('#'+security).parents('.avatar-upload').find('.imagePreview').css('background-image', 'url('+e.target.result +')');             
            $('#'+security).parents('.avatar-upload').find('.imagePreview').hide();
            $('#'+security).parents('.avatar-upload').find('.imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(this.files[0]);
      }   
});

//activate the premium ads

var initialPrice = $('.ads-fee').text();
$('.activatethis').on('click',function(){           
  if($(this).parents('.thispackage').hasClass('activatedPackage')){
    $(this).parents('.thispackage').removeClass('activatedPackage');
    $(this).text('Activate');
    $('.packageName').text('Listing Fee');
    $('.ads-fee').text(initialPrice);
  }   
  else{
  $('.thispackage').removeClass('activatedPackage');
  $('.activatethis').text('Activate');
  $(this).text('Deactivate')
  $(this).parents('.thispackage').addClass('activatedPackage');        
  $('.packageName').text($('.activatedPackage h2').text());
  $('.ads-fee').text($('.activatedPackage .amount').text());

  }
})

//Create an instance of the Stripe object
  // Set your publishable API key
  var stripe = Stripe('{{ env("STRIPE_PUBLISH_KEY") }}');

  // Create an instance of elements
  var elements = stripe.elements();

  var style = {
      base: {
          fontWeight: 400,
          fontFamily: '"DM Sans", Roboto, Open Sans, Segoe UI, sans-serif',
          fontSize: '16px',
          lineHeight: '1.4',
          color: '#1b1642',
          padding: '.75rem 1.25rem',
          '::placeholder': {
              color: '#ccc',
          },
      },
      invalid: {
          color: '#dc3545',
      }
  };

  var cardElement = elements.create('cardNumber', {
      style: style
  });
  cardElement.mount('#card_number');

  var exp = elements.create('cardExpiry', {
      'style': style
  });
  exp.mount('#card_expiry');

  var cvc = elements.create('cardCvc', {
      'style': style
  });
  cvc.mount('#card_cvc');

  // Validate input of the card elements
  var resultContainer = document.getElementById('paymentResponse');
  cardElement.addEventListener('change', function (event) {
      if (event.error) {
          resultContainer.innerHTML = '<p>' + event.error.message + '</p>';
      } else {
          resultContainer.innerHTML = '';
      }
  });

  // Get payment form element
  var form = document.getElementById('msform');

  // Create a token when the form is submitted.
  form.addEventListener('submit', function (e) {
      e.preventDefault();
      createToken();
  });

  // Create single-use token to charge the user
  function createToken() {
      stripe.createToken(cardElement).then(function (result) {
          if (result.error) {
              // Inform the user if there was an error
              resultContainer.innerHTML = '<p>' + result.error.message + '</p>';
          } else {
              // Send the token to your server
              stripeTokenHandler(result.token);
          }
      });
  }

  
  // Callback to handle the response from stripe
  function stripeTokenHandler(token) {
      
      // Insert the token ID into the form so it gets submitted to the server
      var hiddenInput = document.createElement('input');
      hiddenInput.setAttribute('type', 'hidden');
      hiddenInput.setAttribute('name', 'stripeToken');
      hiddenInput.setAttribute('value', token.id);
      form.appendChild(hiddenInput);

      // Submit the form
      form.submit();
  }	
  
  $('.pay-via-stripe-btn').on('click', function () {
      var payButton   = $(this);
      var name        = $('#name').val();
      var email       = $('#email').val();

      if (name == '' || name == 'undefined') {
          $('.generic-errors').html('Name field required.');
          return false;
      }
      if (email == '' || email == 'undefined') {
          $('.generic-errors').html('Email field required.');
          return false;
      }

      if(!$('#terms_conditions').prop('checked')){
          $('.generic-errors').html('The terms conditions must be accepted.');
          return false;
      }
  });

    </script>