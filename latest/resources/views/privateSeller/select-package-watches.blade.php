<!DOCTYPE html>
<html lang="en">

@include('tangiblehtml.headheader')

<body>
    @include('tangiblehtml.innerheader')

    <section class="become-seller-from overlay-wrapper">
        <div class="container">
            <div class="row">                
                <div class="col-lg-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                    @foreach($pricing_data as $key=>$data)
                        @if($key == 0)
                        <li class="nav-item">
                            <a class="nav-link active" id="private-tab{{$key}}" data-toggle="tab" href="#private{{$key}}"
                                role="tab" aria-controls="private-tab{{$key}}" aria-selected="true"> {{currency()->convert(floatval($data->rangeFrom), 'USD', 'USD')}} - {{currency()->convert(floatval($data->rangeTo), 'USD', 'USD')}}</a>
                        </li>
                        @else
                        <li class="nav-item">
                            <a class="nav-link" id="private-tab{{$key}}" data-toggle="tab" href="#private{{$key}}"
                                role="tab" aria-controls="private-tab{{$key}}" aria-selected="true"> {{currency()->convert(floatval($data->rangeFrom), 'USD', 'USD')}} - {{currency()->convert(floatval($data->rangeTo), 'USD', 'USD')}}</a>
                        </li>
                        @endif
                     @endforeach   
                        <!-- <li class="nav-item">
                            <a class="nav-link" id="private-tab" data-toggle="tab" href="#private" role="tab"
                                aria-controls="private-tab" aria-selected="false"> $1000 - $1999</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="private-tab2" data-toggle="tab" href="#private2" role="tab"
                                aria-controls="private-tab2" aria-selected="false"> $2000 - $2999</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="private-tab3" data-toggle="tab" href="#private3" role="tab"
                                aria-controls="private-tab3" aria-selected="false"> $3000 - $3999</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="private-tab4" data-toggle="tab" href="#private4" role="tab"
                                aria-controls="private-tab4" aria-selected="false"> $4000 - $4999</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="private-tab5" data-toggle="tab" href="#private5" role="tab"
                                aria-controls="private-tab5" aria-selected="false"> $5000 - $5999</a>
                        </li> -->
                    </ul>

                    <div class="seller__from pt-5 tab-content" id="myTabContent">
                        @foreach($pricing_data as $key=> $pricing)
                        @if($key == 0)
                        <div class="tab-pane fade show active" id="private{{$key}}" role="tabpanel"
                            aria-labelledby="private-tab{{$key}}">  
                            <div class="row">
                                @foreach($value_data as $value)
                                @if($value->rangeFrom == $pricing->rangeFrom)
                                @if($value->package == 'Silver')
                                
                                 <!-- Purple Table -->
                                 <div class="col-lg-4 col-md-6">
                                    <div class="pricing-table purple">
                                       <!-- Table Head -->
                                      <!-- <div class="pricing-label">Most </div>-->
                                       <h2>Silver</h2>
                                       <!--<h5>Made for starters</h5>-->
                                       <!-- Features -->
                                       <div class="pricing-features">
                                        <div class="feature">appear in search results</div>
                                        <div class="feature">receive weekly emails with stats and information</div>
                                        <div class="feature">email will notify you have many views your ad has received each week.</div>
                                        <div class="feature">basic analytics</div>
                                        <div class="feature">limited to only 10 photos per listing</div>
                                        <div class="feature">private message with enquiries</div>
                                        <div class="feature">process payments internationally (stripe)</div>                                           
                                     </div>
                                       <!-- Price -->
                                       <div class="price-tag">
                                          <span class="amount">{{currency()->convert(floatval($value->pricing), 'USD', 'USD')}}</span>
                                          <span class="after">/month</span>
                                       </div>
                                       <!-- Button -->
                                       <a class="price-button" href="/privatesellerwatchadd/Silver/{{$value->rangeFrom}}-{{$value->rangeTo}}/{{$value->pricing}}">Get Started</a>
                                    </div>
                                 </div>
                                 @elseif($value->package == 'Gold')
                                 <!-- Turquoise Table -->
                                 <div class="col-lg-4 col-md-6">
                                    <div class="pricing-table turquoise">
                                       <!-- Table Head -->
                                       <!--<div class="pricing-label">2021</div>-->
                                       <h2>Gold</h2>
                                       <!--<h5>Made for experienced users</h5>-->
                                       <!-- Features -->
                                       <div class="pricing-features">
                                        <div class="feature">appear higher in search results</div>
                                        <div class="feature">receive weekly email with stats and information</div>
                                        <div class="feature">advanced analytics:</div>
                                        <div class="feature">limited to 20 pictures/video per listing</div>
                                        <div class="feature">private message with enquiries</div>
                                        <div class="feature">process payments internationally (stripe)</div>
                                     </div>
                                       <!-- Price -->
                                       <div class="price-tag">
                                          <span class="amount">{{currency()->convert(floatval($value->pricing), 'USD', 'USD')}}</span>
                                          <span class="after">/month</span>
                                       </div>
                                       <!-- Button -->
                                       <a class="price-button" href="/privatesellerwatchadd/Gold/{{$value->rangeFrom}}-{{$value->rangeTo}}/{{$value->pricing}}">Get Started</a>
                                    </div>
                                 </div>
                                 @elseif($value->package == 'Platinum')
                                 <!-- Red Table -->
                                 <div class="col-lg-4 col-md-6">
                                    <div class="pricing-table red">
                                       <!-- Table Head -->
                                       <!--<div class="pricing-label">2020</div>-->
                                       <h2>Platinum</h2>
                                       <!--<h5>Made for professionals/agencies</h5>-->
                                       <!-- Features -->
                                       <div class="pricing-features">
                                        <div class="feature">appear highest in search results</div>
                                        <div class="feature">receive weekly emails with stats and information</div>
                                        <div class="feature">advanced analytics:</div>
                                        <div class="feature">limited to 30 photos/video per listing</div>
                                        <div class="feature">private message with enquiries</div>
                                        <div class="feature">process payments internationally (stripe)</div>
                                        <div class="feature">have your ad appear on the home page (latest listings slideshow)</div>
                                        <div class="feature">have your ad appear on Tangible Listings social media, one time only per ad.</div>
                                     </div>
                                       <!-- Price -->
                                       <div class="price-tag">
                                          <span class="amount">{{currency()->convert(floatval($value->pricing), 'USD', 'USD')}}</span>
                                          <span class="after">/month</span>
                                       </div>
                                       <!-- Button -->
                                       <a class="price-button" href="/privatesellerwatchadd/Platinum/{{$value->rangeFrom}}-{{$value->rangeTo}}/{{$value->pricing}}">Get Started</a>
                                    </div>
                                 </div>
                                 @endif
                                 @endif
                                 @endforeach
                            </div>
                        </div>
                        @else
                        <div class="tab-pane fade" id="private{{$key}}" role="tabpanel"
                            aria-labelledby="private-tab{{$key}}">  
                            <div class="row">
                                @foreach($value_data as $value)
                                @if($value->rangeFrom == $pricing->rangeFrom)
                                @if($value->package == 'Silver')
                                 <!-- Purple Table -->
                                 <div class="col-md-4">
                                    <div class="pricing-table purple">
                                       <!-- Table Head -->
                                       <!--<div class="pricing-label">Most </div>-->
                                       <h2>Silver</h2>
                                       <!--<h5>Made for starters</h5>-->
                                       <!-- Features -->
                                       <div class="pricing-features">
                                        <div class="feature">appear in search results</div>
                                        <div class="feature">receive weekly emails with stats and information</div>
                                        <div class="feature">email will notify you have many views your ad has received each week.</div>
                                        <div class="feature">basic analytics</div>
                                        <div class="feature">limited to only 10 photos per listing</div>
                                        <div class="feature">private message with enquiries</div>
                                        <div class="feature">process payments internationally (stripe)</div>                                          
                                     </div>
                                       <!-- Price -->
                                       <div class="price-tag">
                                          <span class="amount">{{currency()->convert(floatval($value->pricing), 'USD', 'USD')}}</span>
                                          <span class="after">/month</span>
                                       </div>
                                       <!-- Button -->
                                       <a class="price-button" href="/privatesellerwatchadd/Silver/{{$value->rangeFrom}}-{{$value->rangeTo}}/{{$value->pricing}}">Get Started</a>
                                    </div>
                                 </div>
                                 @elseif($value->package == 'Gold')
                                 <!-- Turquoise Table -->
                                 <div class="col-md-4">
                                    <div class="pricing-table turquoise">
                                       <!-- Table Head -->
                                       <!--<div class="pricing-label">2021</div>-->
                                       <h2>Gold</h2>
                                       <!--<h5>Made for experienced users</h5>-->
                                       <!-- Features -->
                                       <div class="pricing-features">
                                        <div class="feature">appear higher in search results</div>
                                        <div class="feature">receive weekly email with stats and information</div>
                                        <div class="feature">advanced analytics:</div>
                                        <div class="feature">limited to 20 pictures/video per listing</div>
                                        <div class="feature">private message with enquiries</div>
                                        <div class="feature">process payments internationally (stripe)</div>
                                     </div>
                                       <!-- Price -->
                                       <div class="price-tag">
                                          <span class="amount">{{currency()->convert(floatval($value->pricing), 'USD', 'USD')}}</span>
                                          <span class="after">/month</span>
                                       </div>
                                       <!-- Button -->
                                       <a class="price-button" href="/privatesellerwatchadd/Gold/{{$value->rangeFrom}}-{{$value->rangeTo}}/{{$value->pricing}}">Get Started</a>
                                    </div>
                                 </div>
                                 @elseif($value->package == 'Platinum')
                                 <!-- Red Table -->
                                 <div class="col-md-4">
                                    <div class="pricing-table red">
                                       <!-- Table Head -->
                                       <!--<div class="pricing-label">2020</div>-->
                                       <h2>Platinum</h2>
                                       <!--<h5>Made for professionals/agencies</h5>-->
                                       <!-- Features -->
                                       <div class="pricing-features">
                                        <div class="feature">appear highest in search results</div>
                                        <div class="feature">receive weekly emails with stats and information</div>
                                        <div class="feature">advanced analytics:</div>
                                        <div class="feature">limited to 30 photos/video per listing</div>
                                        <div class="feature">private message with enquiries</div>
                                        <div class="feature">process payments internationally (stripe)</div>
                                        <div class="feature">have your ad appear on the home page (latest listings slideshow)</div>
                                        <div class="feature">have your ad appear on Tangible Listings social media, one time only per ad.</div>
                                     </div>
                                       <!-- Price -->
                                       <div class="price-tag">
                                          <span class="amount">{{currency()->convert(floatval($value->pricing), 'USD', 'USD')}}</span>
                                          <span class="after">/month</span>
                                       </div>
                                       <!-- Button -->
                                       <a class="price-button" href="/privatesellerwatchadd/Platinum/{{$value->rangeFrom}}-{{$value->rangeTo}}/{{$value->pricing}}">Get Started</a>
                                    </div>
                                 </div>
                                 @endif
                                 @endif
                                 @endforeach
                            </div>
                        </div>
                        @endif
						@endforeach
                        <!-- <div class="tab-pane fade" id="private1" role="tabpanel" aria-labelledby="private-tab1">
                            <div class="row">
                                
                                <div class="col-md-4">
                                   <div class="pricing-table purple">

                                      <div class="pricing-label">Most </div>
                                      <h2>Ultimate Ads</h2>
                                      <h5>Made for starters</h5>

                                      <div class="pricing-features">
                                       <div class="feature">Most Selected for seller</div>
                                       <div class="feature">Weekly Emails Max 500</div>
                                       <div class="feature">Get a free care Commercial</div>
                                       <div class="feature">Share report with buyers</div>                                          
                                    </div>
                                    
                                      <div class="price-tag">
                                         <span class="symbol">$</span>
                                         <span class="amount">7.99</span>
                                         <span class="after">/month</span>
                                      </div>
                                      <a class="price-button" href="#">Get Started</a>
                                   </div>
                                </div>
                            </div>

                        </div> -->
                        <!-- <div class="tab-pane fade" id="private2" role="tabpanel" aria-labelledby="private2-tab">
                            <div class="row">
                                
                                <div class="col-md-4">
                                   <div class="pricing-table purple">

                                      <div class="pricing-label">Most </div>
                                      <h2>Ultimate Ads</h2>
                                      <h5>Made for starters</h5>

                                      <div class="pricing-features">
                                       <div class="feature">Most Selected for seller</div>
                                       <div class="feature">Weekly Emails Max 500</div>
                                       <div class="feature">Get a free care Commercial</div>
                                       <div class="feature">Share report with buyers</div>                                          
                                    </div>

                                      <div class="price-tag">
                                         <span class="symbol">$</span>
                                         <span class="amount">7.99</span>
                                         <span class="after">/month</span>
                                      </div>

                                      <a class="price-button" href="#">Get Started</a>
                                   </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="tab-pane fade" id="private3" role="tabpanel" aria-labelledby="private3-tab">
                            <div class="row">
                                
                                <div class="col-md-4">
                                   <div class="pricing-table purple">

                                      <div class="pricing-label">Most </div>
                                      <h2>Ultimate Ads</h2>
                                      <h5>Made for starters</h5>

                                      <div class="pricing-features">
                                       <div class="feature">Most Selected for seller</div>
                                       <div class="feature">Weekly Emails Max 500</div>
                                       <div class="feature">Get a free care Commercial</div>
                                       <div class="feature">Share report with buyers</div>                                          
                                    </div>

                                      <div class="price-tag">
                                         <span class="symbol">$</span>
                                         <span class="amount">7.99</span>
                                         <span class="after">/month</span>
                                      </div>

                                      <a class="price-button" href="#">Get Started</a>
                                   </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="tab-pane fade" id="private4" role="tabpanel" aria-labelledby="private4-tab">
                            <div class="row">
                                
                                <div class="col-md-4">
                                   <div class="pricing-table purple">

                                      <div class="pricing-label">Most </div>
                                      <h2>Ultimate Ads</h2>
                                      <h5>Made for starters</h5>

                                      <div class="pricing-features">
                                       <div class="feature">Most Selected for seller</div>
                                       <div class="feature">Weekly Emails Max 500</div>
                                       <div class="feature">Get a free care Commercial</div>
                                       <div class="feature">Share report with buyers</div>                                          
                                    </div>

                                      <div class="price-tag">
                                         <span class="symbol">$</span>
                                         <span class="amount">7.99</span>
                                         <span class="after">/month</span>
                                      </div>

                                      <a class="price-button" href="#">Get Started</a>
                                   </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="tab-pane fade" id="private5" role="tabpanel" aria-labelledby="private4-tab">
                            <div class="row">
                                
                                <div class="col-md-4">
                                   <div class="pricing-table purple">

                                      <div class="pricing-label">Most </div>
                                      <h2>Ultimate Ads</h2>
                                      <h5>Made for starters</h5>

                                      <div class="pricing-features">
                                       <div class="feature">Most Selected for seller</div>
                                       <div class="feature">Weekly Emails Max 500</div>
                                       <div class="feature">Get a free care Commercial</div>
                                       <div class="feature">Share report with buyers</div>                                          
                                    </div>

                                      <div class="price-tag">
                                         <span class="symbol">$</span>
                                         <span class="amount">7.99</span>
                                         <span class="after">/month</span>
                                      </div>

                                      <a class="price-button" href="#">Get Started</a>
                                   </div>
                                </div>
                            </div>
                        </div> -->

                    </div>



                </div>
            </div>
        </div>
    </section>
    
    @include('tangiblehtml.innerfooter')
    
     <script src="/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/intlTelInput.min.js"></script>
    <script src="/assets/js/local.js"></script>
    <script>
        // Show the first tab and hide the rest
        // jQuery('.row_choser .tb_link').addClass('active');
        jQuery('.tab-content').hide();
        jQuery('.tab-content:first').show();

        // Click function
        jQuery('.tb_link').click(function () {
            jQuery('.tb_link').removeClass('active');
            jQuery(this).addClass('active');
            jQuery('.tab-content').hide();

            var activeTab = jQuery(this).attr('href');
            jQuery(activeTab).fadeIn();
            return false;
        });
        var input = document.querySelector("#telephone");
        window.intlTelInput(input, ({
            separateDialCode:true
        }));
    </script>
</body>

</html>