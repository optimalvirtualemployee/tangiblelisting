$(document).ready(function () {
    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;
    var ImgArray = [];
    var ImgArrayDamage = []; 
    const imageArrayfn = function () {
        ImgArray = [];
        ImgArrayDamage = [];
        $('.uploaded img').each(function (index, value) {
          if($(this).parents('#damagePhotos').length > 0){
            ImgArrayDamage.push($(this).attr('src'));
          }
          else{
            ImgArray.push($(this).attr('src'));
          }
        });
        imgUploadedfn();
    }

    const imgUploadedfn = function () {
        let imgCount = ImgArray.length;
        let imgCountDmg = ImgArrayDamage.length;
        let html = '';
        let imgGallery = '';
        let dmgGallery= '';
        
        if (imgCount > 0) {
            for (let j = 0; j < imgCount; j++) {                   
                // html += ' <div class="item" data-src="' + ImgArray[j] + '"><img src="' + ImgArray[j] + '"></div>';
                imgGallery += '<div><img src="' + ImgArray[j] + '" /></div>';
            }
            $('#imgGallery').empty();   
            $('#imgGallery').append(imgGallery);      
           
        }  
        if (imgCountDmg > 0) {
            for (let i = 0; i < imgCountDmg; i++) { 
              dmgGallery += '<div><img src="' + ImgArrayDamage[i] + '" /></div>';
            }   
            $('#dmgGallery').empty();               
            $('#dmgGallery').append(dmgGallery);      
            
        } 
          console.log('Images Array - > ', ImgArray)  ;  
          console.log('Damage Images Array - > ', ImgArrayDamage)  ;  
    }
    $(".next").click(function () {
      const thisObject  = $(this);
      if($(this).hasClass('next1')){      
       $(this).parents('fieldset').addClass('was-validated');
      let notvalidNumber = $(this).parents('fieldset').find('.invalid-feedback').filter(function() {
        return $(this).css('display') == 'block';
      }).length;

      if(notvalidNumber == 0){
        nextPage(thisObject);
      }     
           
      }
      if($(this).hasClass('next2')){      
        $(this).parents('fieldset').addClass('was-validated');
         let notvalidNumber = $(this).parents('fieldset').find('.invalid-feedback').filter(function() {
         return $(this).css('display') == 'block';
       }).length;
 
       if(notvalidNumber == 0){
         nextPage(thisObject);
       }    
       }   
       if($(this).hasClass('next3')){      
        $(this).parents('fieldset').addClass('was-validated');
       let notvalidNumber = $(this).parents('fieldset').find('.invalid-feedback').filter(function() {
         return $(this).css('display') == 'block';
       }).length;
 
       if(notvalidNumber == 0){
         nextPage(thisObject);
       }   
            
       }    
     
      
    }); 
    
    function nextPage(thisObject){
      imageArrayfn();
      current_fs = $(thisObject).parent();
      next_fs = $(thisObject).parent().next();      
      //Add Class Active
      $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");      
      //show the next fieldset
      next_fs.show();
      //hide the current fieldset with style
      current_fs.animate({ opacity: 0 }, {
          step: function (now) {
              // for making fielset appear animation
              opacity = 1 - now;

              current_fs.css({
                  'display': 'none',
                  'position': 'relative'
              });
              next_fs.css({ 'opacity': opacity });
          },
          duration: 600
      });
    }
    $(".previous").click(function () {
        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();
        //Remove class active
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");      
        //show the previous fieldset
        previous_fs.show();
        //hide the current fieldset with style
        current_fs.animate({ opacity: 0 }, {
            step: function (now) {
                // for making fielset appear animation
                opacity = 1 - now;

                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                previous_fs.css({ 'opacity': opacity });
            },
            duration: 600
        });
    });
    $('.radio-group .radio').click(function () {
        $(this).parent().find('.radio').removeClass('selected');
        $(this).addClass('selected');
    });
    $(".submit").click(function () {
        return false;
    })
    /*let preloadImages = [
    {id: 1, src: 'https://picsum.photos/500/500?random=1'},
    {id: 2, src: 'https://picsum.photos/500/500?random=2'},
    {id: 3, src: 'https://picsum.photos/500/500?random=3'},
    {id: 4, src: 'https://picsum.photos/500/500?random=4'},
    {id: 5, src: 'https://picsum.photos/500/500?random=5'},
    {id: 6, src: 'https://picsum.photos/500/500?random=6'},
    ];*/
    //$('.input-images-1').imageUploader();
    $('.input-images-1').imageUploader({
      //preloaded: preloadImages,
      imagesInputName: 'normalImages',
      preloadedInputName: 'preloadedNormal',
      // maxSize: 2 * 1024 * 1024,
      // maxFiles: 10
    });
    $( ".uploaded" ).sortable();
    $( ".uploaded" ).disableSelection();   

});   

// Security Upload Image Script
$(".imageUploadCls").change(function() {  
const security = $(this).attr('id');
if (this.files && this.files[0]) {
  let reader = new FileReader();
  reader.onload = function(e) {                 
      $('#'+security).parents('.avatar-upload').find('.imagePreview').css('background-image', 'url('+e.target.result +')');             
      $('#'+security).parents('.avatar-upload').find('.imagePreview').hide();
      $('#'+security).parents('.avatar-upload').find('.imagePreview').fadeIn(650);
  }
  reader.readAsDataURL(this.files[0]);
}   
});

//activate the premium ads
$('.activatethis').on('click',function(){           
  if($(this).parents('.thispackage').hasClass('activatedPackage')){
    $(this).parents('.thispackage').removeClass('activatedPackage');
    $(this).text('Activate');
    $('.packageName').text('Listing Fee');
    $('.ads-fee').text('00.00');
  }   
  else{
  $('.thispackage').removeClass('activatedPackage');
  $('.activatethis').text('Activate');
  $(this).text('Deactivate')
  $(this).parents('.thispackage').addClass('activatedPackage');        
  $('.packageName').text($('.activatedPackage h2').text());
  $('.ads-fee').text($('.activatedPackage .amount').text());
  } 
})   


