<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Listing Page</title>
    <link href="https://fonts.googleapis.com/css2?family=Muli:wght@200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/hover.css/2.3.1/css/hover-min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" />
    <link rel="stylesheet" href="/assets/css/animate.css">
    <link rel="stylesheet" href="/assets/css/jquery-ui.min.css" />
    <link rel="stylesheet" href="/assets/css/font-style.css">
    <link rel="stylesheet" href="/assets/css/local.css">
    <link rel="stylesheet" href="/assets/css/filters.css">
    <link rel="stylesheet" href="/assets/css/responsive.css">
</head>

<body>
    <header class="custom-header-wrapper">
        <div class="custom-top-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="header-social-icon">
                            <ul>
                                <li><a class="hvr-buzz-out" href="https://www.facebook.com/tangiblelistings"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a class="hvr-buzz-out" href="https://www.instagram.com/tangiblelistings/"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-6 col-sm-6 col-12">
                        <div class="header-phone">
                        <div class="dropdown">
                                <button class="btn float-right text-white dropdown-toggle" type="button" data-toggle="dropdown">Account
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                <!-- <li><a class="hvr-buzz-out" href="tel:(123)987654">(123) 987654</a></li>
                                <li><a class="hvr-buzz-out" href="mailto:info@tangiblelistings.com">info@tangiblelistings.com</a></li> -->
                                <li>
                                @if (Route::has('login'))
                                @auth
                                <a class="hvr-buzz-out text-dark" href="{{ url('/admin/home') }}">Home</a>
                                @else
                                <a class="hvr-buzz-out text-dark" href="{{ route('login') }}">Login</a> </br>

                                @if (Route::has('register'))
                                <a class="hvr-buzz-out text-dark" href="{{ route('register') }}">Register</a>
                                @endif 
                                @endauth
                                @endif</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="custom-bottom-header about-header">
            <nav class="navbar navbar-expand-lg">
                <div class="container">
                    <a href="/" class="navbar-brand">
                        <img src="/assets/img/logo-header.png" class="img-fluid">
                    </a>
                    <button type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbars"
                        aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler">
                        <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
                    </button>
                    <div id="navbarContent" class="collapse navbar-collapse">
                        <ul class="navbar-nav">
                        	<!--<li class="nav-item"><a href="/" class="nav-link">Home </a></li>-->
                            <li class="nav-item"><a href="/tangibleautomobiles" class="nav-link">Automobiles </a></li>
                            <li class="nav-item active"><a href="/tangiblerealestate" class="nav-link">Real Estate</a></li>
                            <li class="nav-item "><a href="/tangiblewatches" class="nav-link">Watches</a></li>
                            <!-- <li class="nav-item"><a href="#" class="nav-link"> Articles </a></li>
                            <li class="nav-item"><a href="#" class="nav-link"> Details </a></li> -->
                            <!-- Level one dropdown -->
                            <!-- <li class="nav-item dropdown">
                              <a id="dropdownMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">About us</a>
                              <ul aria-labelledby="dropdownMenu1" class="dropdown-menu border-0 shadow">
                                  <li><a href="become-a-seller" class="dropdown-item">Become A Seller</a></li>
                                  <li><a href="#" class="dropdown-item">Team</a></li>
                              </ul>
                          </li> -->
                            <!-- End Level one -->
                            <!-- <li class="nav-item"><a href="#" class="nav-link">Contact</a></li> -->
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>


    <div class="custom-home-listing-wrapper ">
        <nav aria-label="breadcrumb ">
            <div class="container-fluid">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/tangiblerealestate">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Realstate</li>
                </ol>
            </div>
        </nav>
        <div class="container-fluid pt-4">
            <div class="row">
                <div class="col-lg-3 order-1">
                    <div class="product-filter" id="filter-form">
                    <form id="searchForm" method="GET" action="{{url('/propertylisting/search')}}">
                        <div class="product-filter-inner">
                            <div class="filter">
                                <p><i class="fa fa-filter" aria-hidden="true"></i> Refine Search</p>
                                <a href="javascript:;"><i class="fa fa-undo" aria-hidden="true" id="clear"> Clear</a></i>
                            </div>
                            <div class="filter-area">

                                     
                                   <!-- <div class="filter-box">
                                    <div class="search-box mb-2 mt-2">
                                            <div class="finder">
                                                <div class="finder__outer">
                                                    <div class="finder__inner">
                                                        <div class="finder__icon" ref="icon"></div>
                                                        <input class="finder__input" type="text" name=""
                                                            placeholder="City, Region or Country" />
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>  -->
								
								 <div class="search-box mb-2 mt-2">
						            <div class="finder">
                                        <div class="finder__outer">
                                            <div class="finder__inner">
                                                <div class="finder__icon" ref="icon"></div>
                                                @if(isset($searchTerm))
                                                <input class="finder__input" id="autocomplete2" type="text" name="search" placeholder="City, Region or Country" value="{{$searchTerm}}" />
                                                @else
                                                <input class="finder__input" id= "autocomplete2" type="text" name="" placeholder="City, Region or Country" value=""/>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#hometype-filter"
                                        role="button" aria-expanded="false" aria-controls="hometype-filter">
                                        <div class="icon-heading"><i class="fa fa-home" aria-hidden="true"></i> Home
                                            Type</div> <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="hometype-filter">
                                        <div class="inner-text">
                                            <div class="pt-2 pb-1">
                                                @foreach($propertyType_data as $propertyType)
                                                <div class="custom-control">
                                                   @if(isset($homeSelected))
                                                    <input type="checkbox" class="" id="{{$propertyType->property_type}}" name="home-{{$propertyType->property_type}}" value="{{$propertyType->id}}" {{in_array($propertyType->id,$homeSelected) ? "Checked" : ""}}>
                                                   @else 
                                                   <input type="checkbox" class="" id="{{$propertyType->property_type}}" name="home-{{$propertyType->property_type}}" value="{{$propertyType->id}}">
                                                   @endif
                                                    <label class="custom-control-label" for="{{$propertyType->property_type}}">{{$propertyType->property_type}}</label>
                                                   <!-- <div class="value">(867)</div> --> 
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#condition-filter"
                                        role="button" aria-expanded="false" aria-controls="condition-filter">
                                        <div class="icon-heading"><i class="fa fa-flag" aria-hidden="true"></i>
                                            Condition</div> <span><i class="fa fa-caret-right"
                                                aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="condition-filter">
                                        <div class="inner-text">
                                            <div class="pt-2 pb-1">
                                                <div class="custom-control custom-radio">
                                                   @if(isset($condition))
                                                    <input type="radio" id="condition1" name="condition1"
                                                        class="custom-control-input" value="0" {{$condition == '0' ? "Checked" : ""}}>
                                                    @else
                                                    <input type="radio" id="condition1" name="condition1"
                                                        class="custom-control-input" value="0">
                                                    @endif        
                                                    <label class="custom-control-label" for="condition1">New</label>
                                                    <!-- <div class="value">(867)</div> --> 
                                                </div>
                                                <div class="custom-control custom-radio">
                                                    @if(isset($condition))
                                                    <input type="radio" id="condition2" name="condition1"
                                                        class="custom-control-input" value="1" {{$condition == '1' ? "Checked" : ""}}>
                                                    @else
                                                    <input type="radio" id="condition2" name="condition1"
                                                        class="custom-control-input" value="1">
                                                     @endif       
                                                    <label class="custom-control-label"
                                                        for="condition2">Established</label>
                                                   <!-- <div class="value">(867)</div> --> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#bed-filter"
                                        role="button" aria-expanded="false" aria-controls="bed-filter">
                                        <div class="icon-heading"><i class="fa fa-bed" aria-hidden="true"></i>
                                            Bedrooms</div> <span><i class="fa fa-caret-right"
                                                aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="bed-filter">
                                        <div class="inner-text mt-2">
                                            <div class="select-box">
                                                <div class="title">Min :</div>
                                                <select class="form-control custom-select" name="minbeds"
                                                    id="min-beds">
                                                    <option value="" selected disabled>Choose...</option>
                                                    @foreach($bedroom_data as $bedroom)
                                                    @if(isset($bedMin))
                                                    <option value="{{$bedroom->number_of_bedrooms}}" {{$bedMin == $bedroom->number_of_bedrooms ? "Selected" : ""}}>{{$bedroom->number_of_bedrooms}}</option>
                                                    @else
                                                    <option value="{{$bedroom->number_of_bedrooms}}">{{$bedroom->number_of_bedrooms}}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="select-box">
                                                <div class="title">Max :</div>
                                                <select class="form-control custom-select" name="maxbeds"
                                                    id="max-beds">
                                                    <option value="" selected disabled>Choose...</option>
                                                    @foreach($bedroom_data as $bedroom)
                                                    @if(isset($bedMax))
                                                    <option value="{{$bedroom->number_of_bedrooms}}" {{$bedMax == $bedroom->number_of_bedrooms ? "Selected" : ""}}>{{$bedroom->number_of_bedrooms}}</option>
                                                    @else
                                                    <option value="{{$bedroom->number_of_bedrooms}}">{{$bedroom->number_of_bedrooms}}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#bath-filter"
                                        role="button" aria-expanded="false" aria-controls="bath-filter">
                                        <div class="icon-heading"><i class="fa fa-bath" aria-hidden="true"></i>
                                            Bathrooms</div> <span><i class="fa fa-caret-right"
                                                aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="bath-filter">
                                        <div class="inner-text mt-2">
                                            <div class="select-box">
                                                <div class="title">Min :</div>
                                                <select class="form-control custom-select" name="minbath"
                                                    id="min-bath">
                                                    <option value="" selected disabled>Choose...</option>
                                                    @foreach($bathroom_data as $bathroom)
                                                    @if(isset($bathMin))
                                                    <option value="{{$bathroom->number_of_bathrooms}}" {{$bathMin == $bathroom->number_of_bathrooms ? "Selected" : ""}}>{{$bathroom->number_of_bathrooms}}</option>
                                                    @else
                                                    <option value="{{$bathroom->number_of_bathrooms}}">{{$bathroom->number_of_bathrooms}}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="select-box">
                                                <div class="title">Max :</div>
                                                <select class="form-control custom-select" name="maxbath"
                                                    id="max-bath">
                                                    <option value="" selected disabled>Choose...</option>
                                                    @foreach($bathroom_data as $bathroom)
                                                    @if(isset($bathMax))
                                                    <option value="{{$bathroom->number_of_bathrooms}}" {{$bathMax == $bathroom->number_of_bathrooms ? "Selected" : ""}}>{{$bathroom->number_of_bathrooms}}</option>
                                                    @else
                                                    <option value="{{$bathroom->number_of_bathrooms}}">{{$bathroom->number_of_bathrooms}}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="filter-box icon">
                                    <a class="filter-text" data-toggle="collapse" href="#price-filter" role="button"
                                        aria-expanded="false" aria-controls="price-filter">
                                        <div class="icon-heading"><i class="fa fa-money" aria-hidden="true"></i> Price
                                        </div> <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse show" id="price-filter">
                                        <div class="inner-text">
                                            <div class="values">
                                                <!-- <div class="min-price"></div>
                                    <p class="max-price mt-3"></p> -->
                                                <div id="slider-range" class="price-filter-range" name="rangeInput">
                                                </div>
                                                <div
                                                    style="margin: 20px auto 5px;display: flex;justify-content: space-between;align-items: center;text-align: center;">
                                                    <div class="div">
                                                        <label style="line-height: 1;" class="d-block"
                                                            for="min_price">Min</label>
                                                        <input type="number" min=0 max="{{$price_max-10}}"
                                                            oninput="validity.valid||(value='0');" id="min_price" name="minPrice"
                                                            class="price-range-field" />
                                                    </div>
                                                    <div class="div">
                                                        <label style="line-height: 1;" class="d-block"
                                                            for="max_price">Max</label>
                                                        <input type="number" min=0 max="{{$price_max}}"
                                                            oninput="validity.valid||(value='100000');" id="max_price" name="maxPrice"
                                                            class="price-range-field" />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#landsize-filter"
                                        role="button" aria-expanded="false" aria-controls="landsize-filter">
                                        <div class="icon-heading"><i class="fa fa-map-o" aria-hidden="true"></i> Land
                                            Size</div>
                                        <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="landsize-filter">
                                        <div class="inner-text">
                                            <div class="form-row mb-2 mt-2">
                                                <label for="landsize">Min Land Size :</label>
                                                <!-- <input class="form-control" list="browsers" name="landsize" id="landsize"> -->
                                                <input list="browsers" class="form-control" id="yearfrom" value=""
                                                     placeholder="any">
                                                <datalist id="browsers">
                                                    <option value="200 sq/ft">
                                                    <option value="300 sq/ft">
                                                    <option value="400 sq/ft">
                                                    <option value="500 sq/ft">
                                                    <option value="600 sq/ft">
                                                    <option value="700 sq/ft">
                                                    <option value="800 sq/ft">
                                                    <option value="900 sq/ft">
                                                    <option value="1000 sq/ft">
                                                    <option value="1100 sq/ft">
                                                    <option value="1200 sq/ft">
                                                </datalist>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#country-filter"
                                        role="button" aria-expanded="false" aria-controls="country-filter">
                                        <div class="icon-heading"><i class="fa fa-globe" aria-hidden="true"></i> Country
                                        </div>
                                        <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="country-filter">
                                        <div class="inner-text">
                                            <ul>
                                            @foreach($country_data as $country)
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                        @if(isset($countrySelected))
                                                            <input type="checkbox" class="" id="{{$country->id}}" name="country-{{$country-> country_name}}" value="{{$country-> id}}" {{in_array($country-> id,$countrySelected) ? "Checked" : ""}}>
                                                        @else
                                                        <input type="checkbox" class="" id="{{$country->id}}" name="country-{{$country-> country_name}}" value="{{$country-> id}}">
                                                        @endif    
                                                            <label class="custom-control-label" for="{{$country->id}}">{{$country-> country_name}}</label>
                                                        </div>
                                                        <!-- <div class="value">(867)</div> -->
                                                    </a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
								<button type= "submit" id= "searchButton" class="site-btn">Search</button>
                            </div>
                        </div>
                        </form>
                    </div>

                </div>
                
                
                <div class="col-lg-9 mb-3 order-2">
                    <div class="main-heading mb-0">
                        <h3>Luxury Homes for Sale Worldwide</h3>
                        <p>Classic and contemporary design</p>
                    </div>
                    <div class="sorting-panel">
                        <form action="#" class="needs-validation form-inline justify-content-end mb-3" novalidate>
                            <label for="sortby" class="mr-2">Sort by</label>
                            <select id="sortBy" class="form-control custom-select " name="sortBy">
                                 @if(!isset($sortBy))
                                <option value="0" selected>Popularity</option>
                                @else
                                <option value="0">Popularity</option>
                                @endif
                                @if(isset($sortBy) && $sortBy == "1")
                                <option value="1" selected>High to low</option>
                                @else
                                <option value="1">High to low</option>
                                @endif
                                @if(isset($sortBy) && $sortBy == "2")
                                <option value="2" selected>Low to high</option>
                                @else
                                <option value="2">Low to high</option>
                                @endif
                                @if(isset($sortBy) && $sortBy == "3")
                                <option value="3" selected>Recent</option>
                                @else
                                <option value="3">Recent</option>
                                @endif
                            </select>
                        </form>
                    </div>

                    <div class="row">
                    @foreach($propertyListing as $property)
                        <div class="col-lg-6 col-md-6 mb-4">
                            <div class="home-list-box listing-page-slider-wrapper">
                                <div class="home-list-slider">
									@foreach($images->where('listing_id',$property->id) as $image)
                                    <div class="home-list-item">
                                        <a class="home-view" href="{{route('tangiblerealestate.show',$property->id)}}"><img src="{{url('uploads/'.$image->filename)}}" alt=""
                                                class="img-fluid"></a>
                                    </div>
                                    @endforeach
                                </div>

                                <div class="product-detail-btn">
                                    <ul>
                                        <li><a href="#"><i class="fas fa-search-plus"></i></a></li>
                                        <li><a href="#"><i class="far fa-heart"></i></a></li>
                                        <li><a href="#"><i class="fas fa-plus"></i></a></li>
                                    </ul>
                                </div>
                                <div class="home-content">
                                    <span class="price">{{currency()->convert(floatval($property->value), $property->currency_code, currency()->getUserCurrency())}}</span>
                                    <h4><a href="#">{{$property->ad_title}}</a></h4>
                                    <span class="icon"><i class="fas fa-bed"></i>{{$property->bedrooms}} </span>
                                    <span class="icon"><i class="fas fa-chart-area"></i> {{$property->size  .' '. ($property->metric == 'sqft' ? 'Sq Ft' : 'Sq Mtr')}}</span>
                                </div>
                                <div class="bottom-detais">
                                    <div class="dealer">
                                        <ul>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>
                                        <p class="seller-type">
                                            <img src="https://dummyimage.com/32x32/000/fff" />
                                            {{ $property->agency_id === 0 ? "Private Seller" : "Dealer Name" }}</p>
                                    </div>
                                    <div class="country-flag">
                                        <img src="{{url('uploads/'.$property->filename)}}" alt="">
                                        <p class="c-code">{{$property->countrycode}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @if($propertyListing->hasPages())
          <div class="custom-pagination mt-4 d-flex justify-content-end">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
              @if ($propertyListing->onFirstPage())
            <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
        @else
        	<li class="page-item"><a class="page-link" href="{{ $propertyListing->withQueryString()->previousPageUrl() }}">Previous</a></li>
        @endif
        
        @for($i = 1 ; $i <= $propertyListing->lastPage() ; $i++)
        	@if($propertyListing->currentPage() == $i)
			<li class="page-item active"><a class="page-link" href="{{$propertyListing->withQueryString()->url($i)}}">{{$i}}</a></li>
			@else
			<li class="page-item"><a class="page-link" href="{{$propertyListing->withQueryString()->url($i)}}">{{$i}}</a></li>
			@endif
        @endfor        
          @if ($propertyListing->hasMorePages())
            <li class="page-item"><a class="page-link" href="{{ $propertyListing->withQueryString()->nextPageUrl() }}">Next</a></li>
        @else
            <li class="page-item disabled"><a class="page-link" href="#">Next</a></li>
        @endif    
              </ul>
            </nav>
          </div>
          @endif
                </div>
            </div>
        </div>
    </div>
    
    <footer>
        <div class="custom-top-footer pt-5 pb-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 mr-auto col-md-4 mb-4">
                        <div class="custom-footer-logo">
                            <a href="#"><img src="/assets/img/logo-header.png" class="img-fluid"></a>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make
                                a type specimen book.</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 mb-4">
                        <div class="custom-footer-links footer-social">
                            <h3>Contact Us</h3>
                            <ul>
                                <li>
                                    <i class="fas fa-map-marker-alt"></i> {{$contactUs->address}}
                                </li>
                                <li>
                                     Call us FREE<a href="#"><i class="fas fa-mobile-alt"></i>  {{$contactUs->mobile}}</a>
                                </li>
                                <li>
                                    <a href="mailto:{{$contactUs->email}}"><i class="far fa-envelope"></i> {{$contactUs->email}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="custom-footer-links footer-nav">
                            <h3>Important Links</h3>
                            <ul>
                                @if($propertyType->firstWhere('property_type', 'Residential'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Residential='. $propertyType->firstWhere('property_type', 'Residential')->id)}}">Residential</a>
                                </li>
                                @endif
                                @if($propertyType->firstWhere('property_type', 'Apartment'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Apartment='. $propertyType->firstWhere('property_type', 'Apartment')->id)}}">Apartment</a>
                                </li>
                                @endif
                                @if($propertyType->firstWhere('property_type', 'Single Family Home'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Single Family Home='. $propertyType->firstWhere('property_type', 'Single Family Home')->id)}}">Single Family Home</a>
                                </li>
                                @endif
                                @if($propertyType->firstWhere('property_type', 'Villa'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Villa='. $propertyType->firstWhere('property_type', 'Villa')->id)}}">Villa</a>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="custom-bottom-footer pt-3 pb-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-12">
                        <div class="copyright-wrapper">
                            <p>© Tangible - All rights reserved</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-12">
                        <div class="footer-social-links">
                            <ul>
                                <li><a class="hvr-buzz-out" href="https://www.facebook.com/tangiblelistings"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a class="hvr-buzz-out" href="https://www.instagram.com/tangiblelistings/"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>



    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script src="/assets/js/jquery-ui.min.js"></script>
    <script src="/assets/js/local.js"></script>
</body>

<script type="text/javascript">

$(document).ready(function(){ 

	$("#sortBy").change(function(){
		var urlSortBy = window.location.href;
		
		if(urlSortBy.includes("/propertylisting?")){
			var finalUrl = urlSortBy + '&sortBy=' +$(this).val();
			}else  if(urlSortBy.includes("/propertylisting")){
				var finalUrl = urlSortBy + '/search?sortBy=' +$(this).val();
		}
		if(urlSortBy.includes("search")){
			if(urlSortBy.includes("sortBy")){
				finalUrl = urlSortBy.replace(/sortBy=\d/,'sortBy=' + +$(this).val());
				}else{
			finalUrl = window.location.href + '&sortBy=' + +$(this).val();
				}
			}
		window.location.assign(finalUrl);
	});
	
	$( "#autocomplete2" ).autocomplete({
        source: function( request, response ) {
          // Fetch data
          $.ajax({
            url:"/propertylisting/autosearch",
            type: 'post',
            dataType: "json",
            data: {
               _token: '{{csrf_token()}}',
               search: request.term
            },
            success: function( data ) {
               response( data );
            }
          });
        },
        select: function (event, ui) {
           $('#autocomplete2').val(ui.item.finalLabel); // display the selected text
           return false;
        }
      });	

	
	$(function () {
        $("#slider-range").slider({
            range: true,
            orientation: "horizontal",
            min: 0,
            max: {{$price_max}},
            values: [0, {{$price_max}}],
            

            slide: function (event, ui) {
                if (ui.values[0] == ui.values[1]) {
                    return false;
                }

               $("#min_price").val(ui.values[0]);
                $("#max_price").val(ui.values[1]);
            }
        });

        $("#min_price").val($("#slider-range").slider("values", 0));
        $("#max_price").val($("#slider-range").slider("values", 1));

    });

	$('#clear').click(function (e) {
	  $('#filter-form').find(':input').each(function() {
	    if(this.type == 'submit'){
	          //do nothing
	      }
	      else if(this.type == 'checkbox' || this.type == 'radio') {
	        this.checked = false;
	      }else if( this.type == 'text'){
	    	  $(this). val("") 
		      }
	   })
	   $('#min-bath')[0].selectedIndex = 0;
	  $('#max-bath')[0].selectedIndex = 0;

	  $('#min-beds')[0].selectedIndex = 0;
	  $('#max-beds')[0].selectedIndex = 0;
	  $(function () {
	        $("#slider-range").slider({
	            range: true,
	            orientation: "horizontal",
	            min: 0,
	            max: {{$price_max}},
	            values: [0, {{$price_max}}],
	            step: 100,

	            slide: function (event, ui) {
	                if (ui.values[0] == ui.values[1]) {
	                    return false;
	                }

	               $("#min_price").val(ui.values[0]);
	                $("#max_price").val(ui.values[1]);
	            }
	        });

	        $("#min_price").val($("#slider-range").slider("values", 0));
	        $("#max_price").val($("#slider-range").slider("values", 1));

	    });
	});
	
});
</script>
</html>