<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Landing page</title>
    <link href="https://fonts.googleapis.com/css2?family=Muli:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/hover.css/2.3.1/css/hover-min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" />
    <link rel="stylesheet" href="/assets/css/animate.css">
    <link rel="stylesheet" href="/assets/css/font-style.css">
    <link rel="stylesheet" href="/assets/css/local.css">
    <link rel="stylesheet" href="/assets/css/responsive.css">
</head>

<body>
    <header class="custom-header-wrapper">
        <div class="custom-top-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="header-social-icon">
                            <ul>
                                <li><a class="hvr-buzz-out" href="https://www.facebook.com/tangiblelistings"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a class="hvr-buzz-out" href="https://www.instagram.com/tangiblelistings/"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-6 col-sm-6 col-12">
                        <div class="header-phone">
                            
                            <div class="dropdown">
                                <button class="btn float-right text-white dropdown-toggle" type="button" data-toggle="dropdown">Account
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                <!-- <li><a class="hvr-buzz-out" href="tel:(123)987654">(123) 987654</a></li>
                                <li><a class="hvr-buzz-out" href="mailto:info@tangiblelistings.com">info@tangiblelistings.com</a></li> -->
                                <li>
                                @if (Route::has('login'))
                                @auth
                                <a class="hvr-buzz-out text-dark" href="{{ url('/admin/home') }}">Home</a>
                                @else
                                <a class="hvr-buzz-out text-dark" href="{{ route('login') }}">Login</a> </br>

                                @if (Route::has('register'))
                                <a class="hvr-buzz-out text-dark" href="{{ route('register') }}">Register</a>
                                @endif 
                                @endauth
                                @endif</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="custom-bottom-header">
            <nav class="navbar navbar-expand-lg">
                <div class="container">
                    <a href="/" class="navbar-brand">
                        <img src="/assets/img/logo-header.png" class="img-fluid">
                    </a>
                    <button type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbars" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler">
                <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
            </button>
                    <div id="navbarContent" class="collapse navbar-collapse">
                        <ul class="navbar-nav">
                            <li class="nav-item"><a href="/tangibleautomobiles" class="nav-link">Automobiles</a></li>
                            <li class="nav-item"><a href="/tangiblerealestate" class="nav-link"> Real Estate </a></li>
                            <li class="nav-item"><a href="/tangiblewatches" class="nav-link">Watches</a></li>
                            
                            <!-- <li class="nav-item"><a href="single-detail.html" class="nav-link">Articles</a></li> -->
                            <!-- Level one dropdown -->
<!--                             <li class="nav-item dropdown"> -->
<!--                                 <a id="dropdownMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">About</a> -->
<!--                                 <ul aria-labelledby="dropdownMenu1" class="dropdown-menu border-0 shadow"> -->
<!--                                     <li><a href="#" class="dropdown-item">Company</a></li> -->
<!--                                     <li><a href="#" class="dropdown-item">Team</a></li> -->
<!--                                 </ul> -->
<!--                             </li> -->
                            <!-- End Level one -->
                          <!--  <li class="nav-item"><a href="#" class="nav-link">Contact</a></li> -->
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>

    <!--banner area-->
    <div class="custom-landing-banner-wrapper main-landing">
        <div class="custom-landing-banner-slider">
            <div class="landing-banner-item ">
                <div class="container">
                    <div class="caption">
                        <h2>The World's </h2>
                        <h3>Luxury Marketplace </h3>
                        <p>Get measured by your phone in under one <br/> minute for perfect fitting custom clothing</p>
                        <a href="/carlisting" class="read-more fill-read-more"> Explore More</a>
                    </div>
                </div>
                <img src="/assets/img/car-image3.jpg" alt="" class="img-fluid">
            </div>
            <div class="landing-banner-item">
                <div class="container">
                    <div class="caption">
                        <h2>The World's </h2>
                        <h3>Luxury Marketplace </h3>
                        <p>Get measured by your phone in under one <br/> minute for perfect fitting custom clothing</p>
                        <a href="/watchlisting" class="read-more fill-read-more"> Explore More</a>
                    </div>
                </div>
                <img src="/assets/img/watch3.jpg" alt="" class="img-fluid">
            </div>
            <div class="landing-banner-item">
                <div class="container">
                    <div class="caption">
                        <h2>The World's </h2>
                        <h3>Luxury Marketplace </h3>
                        <p>Get measured by your phone in under one <br/> minute for perfect fitting custom clothing</p>
                        <a href="/propertylisting" class="read-more fill-read-more"> Explore More</a>
                    </div>
                </div>
                <img src="/assets/img/banner-villa2.jpg" alt="" class="img-fluid">
            </div>
        </div>
    </div>
    <!--banner area end-->


    <div class="custom-find-home grey-bg pt-5 pb-5">
        <div class="container">
            <div class="main-heading">
                <h3>New & Trending
                </h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
            </div>
            <div class="perfect-home-slider">
            @if($propertyListing)
                @foreach ($propertyListing as $data)
                <div class="home-item">
                    <div class="home-detail">
                        <div class="home-list-box">
                            <a class="home-view" href="{{route('tangiblerealestate.show',Crypt::encrypt($data->id))}}">
                                <img src="{{url('uploads/'.$images->firstWhere('listing_id',$data->id)->filename)}}" alt="" class="img-fluid">

                            </a>

                            <div class="home-content listing-home-content"><span class="price">{{currency()->convert(floatval($data->property_price), $data->currency_code, currency()->getUserCurrency())}}</span>
                                <h4><a href="{{route('tangiblerealestate.show',$data->id)}}">{{$data->ad_title}}</a></h4>

                                <div class="fasility-item">
                                    <span><i class="fas fa-bed"></i>{{$data->number_of_bedrooms}}</span>
                                    <span><i class="fas fa-chart-area"></i> {{number_format($data->land_size).' '.$data->metric}}</span>
                                    <span><i class="fas fa-shower"></i> {{$data->number_of_bathrooms}}</span>
                                </div>
                                <a class="" href="{{url('/propertylisting/search?home-'.$data->property_type. '=' .$data->propertyId)}}">
                                <div class="apartment_wrap">{{$data->property_type}}</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="home-item">
					<div class="home-detail">
						<div class="home-list-box">
						<img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
						</div>
						</div>
						</div>
                
                @endif
            </div>
        </div>
    </div>

    <div class="custom-latestoffer-wrapper pt-5 pb-5">
        <div class="container">
            <div class="main-heading">
                <h3>latest listings </h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
            </div>
            <div class="row">
                <div class="col-lg-7 col-md-12">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="offer-category-list">
                            @if($latest_automobile)
                                <div class="offer-category-bg-box cat-1-bg">
                                <a class="home-view" href="#">
                                <img src="{{url('uploads/'.$latest_automobile->filename)}}" alt="" class="img-fluid">
                            </a>
                                    <div class="offer-category-overlay">
                                        <div class="offer-category-content">
                                            <div class="new-offer">New</div>
                                            <div class="lo-text clearfix">
                                                <div class="price-box-2"><sup>{{currency()->convert(floatval($latest_automobile->value), $latest_automobile->currency_code, currency()->getUserCurrency())}}</div>
                                                <h3 class="category-title">
                                                    <a href="{{url('/carlisting/'.$latest_automobile->id)}}">{{$latest_automobile->automobile_brand_name .' '. $latest_automobile->automobile_model_name}}</a>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @else
                <div class="home-item">
					<div class="home-detail">
						<div class="home-list-box">
						<img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
						</div>
						</div>
						</div>
                
                @endif
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <div class="offer-category-list">
                            @if(isset($latest_watches[0]))
                                <div class="offer-category-bg-box cat-2-bg">
                                <a class="home-view" href="#">
                                <img src="{{url('uploads/'.$latest_watches[0]->filename)}}" alt="" class="img-fluid">

                            </a>
                                    <div class="offer-category-overlay">
                                        <div class="offer-category-content">
                                            <div class="new-offer">New</div>
                                            <div class="lo-text clearfix">
                                                <div class="price-box-2"><sup>{{currency()->convert(floatval($latest_watches[0]->watch_price), $latest_watches[0]->currency_code, currency()->getUserCurrency())}}</div>
                                                <h3 class="category-title">
                                                    <a href="{{url('/watchlisting/'.$latest_watches[0]->id)}}">{{$latest_watches[0]->brand_name}}</a>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @else
                <div class="home-item">
					<div class="home-detail">
						<div class="home-list-box">
					<img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
						</div>
						</div>
						</div>
                
                @endif
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <div class="offer-category-list">
                            @if(isset($latest_watches[1])) 
                                <div class="offer-category-bg-box cat-3-bg">
                           
                                <a class="home-view" href="#">
                                <img src="{{url('uploads/'.$latest_watches[1]->filename)}}" alt="" class="img-fluid">

                            </a>
                                    <div class="offer-category-overlay">
                                        <div class="offer-category-content">
                                            <div class="new-offer">New</div>
                                            <div class="lo-text clearfix">
                                                <div class="price-box-2"><sup>{{currency()->convert(floatval($latest_watches[1]->watch_price), $latest_watches[1]->currency_code, currency()->getUserCurrency())}}</div>
                                                <h3 class="category-title">
                                                    <a href="{{url('/watchlisting/'.$latest_watches[1]->id)}}">{{$latest_watches[1]->brand_name}}</a>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
								@else
                <div class="home-item">
					<div class="home-detail">
						<div class="home-list-box">
						<img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
						</div>
						</div>
						</div>
                
                @endif                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12">
                    <div class="offer-category-list">
                    @if($latest_property)
                        <div class="offer-category-bg-box offer-category_long_bg">
                        <a class="home-view" href="{{route('tangiblerealestate.show',$data->id)}}">
                                <img src="{{url('uploads/'.$images->firstWhere('listing_id',$latest_property->id)->filename)}}" alt="" class="img-fluid  offer-category_long_bg">
                            </a>
                            <div class="offer-category-overlay">
                                <div class="offer-category-content">
                                    <div class="new-offer">New</div>
                                    <div class="lo-text clearfix">
                                        <div class="price-box-2"><sup>{{currency()->convert(floatval($latest_property->property_price), $latest_property->currency_code, currency()->getUserCurrency())}}</div>
                                        <h3 class="category-title">
                                            <a href="{{url('/tangiblerealestate/'.$latest_property->id)}}">{{$latest_property->property_type}}</a>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @else
                <div class="home-item">
					<div class="home-detail">
						<div class="home-list-box">
						<img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
						</div>
						</div>
						</div>
                
                @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="custom-service-section pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 align-self-center mb-4 mb-lg-auto">
                    <div class="main-title">
                        <h1>We Are The Best</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
                        <a href="#" class="read-more">Read more</a>
                    </div>
                </div>
                <div class="col-lg-7 offset-lg-1">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="custom-service-info">
                                <div class="service-icon">
                                    <i class="fas fa-shield-alt"></i>
                                </div>
                                <div class="our-service-detail">
                                    <h3>Highly Secured</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="custom-service-info">
                                <div class="service-icon">
                                    <i class="far fa-handshake"></i>
                                </div>
                                <div class="our-service-detail">
                                    <h3>Trusted Agents</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="custom-service-info">
                                <div class="service-icon">
                                    <i class="fas fa-dollar-sign"></i>
                                </div>
                                <div class="our-service-detail">
                                    <h3>Get an Offer</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="custom-service-info">
                                <div class="service-icon">
                                    <i class="fas fa-headset"></i>
                                </div>
                                <div class="our-service-detail">
                                    <h3>Free Support</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="our-agent-wrapper   pt-5 pb-5">
        <div class="container">
            <div class="main-heading text-center">
                <h3>Our Dealers</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
            </div>
            <div class="row">
            @if($dealer_listing)
            @foreach ($dealer_listing as $data)
                <div class="col-lg-3 col-md-6">
                    <div class="agent-detail-box">
                        <div class="agent-photo">
                            <a href="#">
                                <img src="{{url('uploads/'.$data->filename)}}" class="img-fluid">
                            </a>
                        </div>
                        <div class="agent-details">
                            <h4><a href="#">{{$data->first_name}}</a></h4>
                            <h5>400+</h5>
                            <a href="#" class="read-more d-block text-center">Read more</a>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="home-item">
					<div class="home-detail">
						<div class="home-list-box">
					<img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
						</div>
						</div>
						</div>
                
                @endif
            </div>
        </div>
    </div>

    <div class="custom-testimonial-wrapper grey-bg">
        <div class="container">
            <div class="main-heading text-center">
                <h3 class="cl-white">Our Testimonial</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
            </div>

            <div class="testimonial-slider">
                <div class="testimonial-item">
                    <div class="testimonial-info">
                        <div class="testimonial-detail">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                                a type specimen book. It has survived not</p>
                            <h3>Anne Brady</h3>
                            <h4>Villa Owner</h4>
                        </div>
                    </div>
                </div>
                <div class="testimonial-item">
                    <div class="testimonial-info">
                        <div class="testimonial-detail">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                                a type specimen book. It has survived not</p>
                            <h3>Anne Brady</h3>
                            <h4>Villa Owner</h4>
                        </div>
                    </div>
                </div>
                <div class="testimonial-item">
                    <div class="testimonial-info">
                        <div class="testimonial-detail">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                                a type specimen book. It has survived not</p>
                            <h3>Anne Brady</h3>
                            <h4>Villa Owner</h4>
                        </div>
                    </div>
                </div>
                <div class="testimonial-item">
                    <div class="testimonial-info">
                        <div class="testimonial-detail">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                                a type specimen book. It has survived not</p>
                            <h3>Anne Brady</h3>
                            <h4>Villa Owner</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="custom-our-blog  pt-5 pb-5">
        <div class="container">
            <div class="main-heading text-center">
                <h3>From Our Articles</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
            </div>
            <div class="row">
                <div class="col-sm-7">
                    <div class="blog-left">
                    @if($property_blog)
                        <div class="blog__item">
                            <a class="home-view" href="#"><img src="{{'uploads/'.$property_blog->filename}}" alt="" class="img-fluid"></a>
                            <div class="blog-detail-content">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i> <span>{{date_format($property_blog->created_at, 'F d, Y')}}</span></span>
                                    <span><i class="fas fa-tag"></i> <a href="#"> {{$property_blog->blogType}}</a></span>
                                </div>
                                <h4><a href="#">{{$property_blog->title}}</a></h4>
                                <p>{{$property_blog->blogPost}}</p>

                            </div>
                        </div>
                        @else
                <div class="home-item">
					<div class="home-detail">
						<div class="home-list-box">
						<img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
						</div>
						</div>
						</div>
                
                @endif
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="blog-right">
						@if($automobile_blog)                    
                        <div class="blog__right-item">
                            <div class="blog__right-item__left">
                                <a class="home-view" href="#"><img src="{{'uploads/'.$automobile_blog->filename}}" alt="" class="img-fluid"></a>
                            </div>
                            <div class="blog__right-item__right">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i> <span>{{date_format($automobile_blog->created_at, 'F d, Y')}}</span></span>

                                </div>
                                <h4><a href="#">{{$automobile_blog->title}}</a></h4>
                                <p>{{$automobile_blog->blogPost}}</p>


                            </div>
                            @else
                <div class="home-item">
					<div class="home-detail">
						<div class="home-list-box">
						<img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
						</div>
						</div>
						</div>
                
                @endif
                        </div>
                        @if($watch_blog)
                        @foreach($watch_blog as $data)
                        <div class="blog__right-item">
                            <div class="blog__right-item__left">
                                <a class="home-view" href="#"><img src="{{'uploads/'.$data->filename}}" alt="" class="img-fluid"></a>
                            </div>
                            <div class="blog__right-item__right">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i> <span>{{date_format($data->created_at, 'F d, Y')}}</span></span>
                                </div>
                                <h4><a href="#">{{$data->title}}</a></h4>
                                <p>{{$data->blogPost}}</p>


                            </div>
                        </div>
						@endforeach
						@else
                <div class="home-item">
					<div class="home-detail">
						<div class="home-list-box">
					<img src="{{url('/assets/img/NoImageFound.png')}}" alt="" class="img-fluid">
						</div>
						</div>
						</div>
                
                @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="become-a-section">
        <div class="container">
            <h3>Join more than 2,000 sellers to reach a global high-net-worth individual audience of over 400,000 per month!</h3>
            <a href="#" class="read-more fill-read-more">Become a Seller</a>
        </div>

    </section>
    <footer>
        <div class="custom-top-footer pt-5 pb-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 mr-auto col-md-4 mb-4">
                        <div class="custom-footer-logo">
                            <a href="#"><img src="/assets/img/logo-header.png" class="img-fluid"></a>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                                a type specimen book.</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 mb-4">
                        <div class="custom-footer-links footer-social">
                            <h3>Contact Us</h3>
                            <ul>
                                <li>
                                    <i class="fas fa-map-marker-alt"></i>{{$contactUs->address}}
                                </li>
                                <li>
                                    Call us FREE<a href="#"><i class="fas fa-mobile-alt"></i> {{$contactUs->mobile}}</a>
                                </li>
                                <li>
                                    <a href="mailto:email@email.com"><i class="far fa-envelope"></i> {{$contactUs->email}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="custom-footer-links footer-nav">
                            <h3>Important Links</h3>
                            <ul>
                                @if($propertyType->firstWhere('property_type', 'Residential'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Residential='. $propertyType->firstWhere('property_type', 'Residential')->id)}}">Residential</a>
                                </li>
                                @endif
                                @if($propertyType->firstWhere('property_type', 'Apartment'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Apartment='. $propertyType->firstWhere('property_type', 'Apartment')->id)}}">Apartment</a>
                                </li>
                                @endif
                                @if($propertyType->firstWhere('property_type', 'Single Family Home'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Single Family Home='. $propertyType->firstWhere('property_type', 'Single Family Home')->id)}}">Single Family Home</a>
                                </li>
                                @endif
                                @if($propertyType->firstWhere('property_type', 'Villa'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Villa='. $propertyType->firstWhere('property_type', 'Villa')->id)}}">Villa</a>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="custom-bottom-footer pt-3 pb-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-12">
                        <div class="copyright-wrapper">
                            <p>© Tangible - All rights reserved</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-12">
                        <div class="footer-social-links">
                            <ul>
                                <li><a class="hvr-buzz-out" href="https://www.facebook.com/tangiblelistings"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a class="hvr-buzz-out" href="https://www.instagram.com/tangiblelistings/"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>


    <script src="/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/local.js"></script>
</body>

</html>