<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoOfDoorsModel extends Model
{
    protected $table = 'au_no_of_doors';
    protected $fillable = ['no_of_doors', 'status'];
}
