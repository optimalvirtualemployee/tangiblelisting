<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchDetailsFeaturesModel extends Model
{
    protected $table = 'wa_watch_detail_features';
}
