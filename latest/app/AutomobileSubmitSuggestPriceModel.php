<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobileSubmitSuggestPriceModel extends Model
{
    protected $table = 'automobile_submitpricerequest';
}
