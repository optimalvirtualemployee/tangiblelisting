<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FunctionPanormaDateModel extends Model
{
    protected $table = 'wa_factory_features_function_panormaDate';
    protected $fillable = ['panorma_date', 'status'];
}
