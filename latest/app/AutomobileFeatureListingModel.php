<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobileFeatureListingModel extends Model
{
    protected $table = 'au_automobiles_features_listing';
}
