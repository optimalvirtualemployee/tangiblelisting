<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseModel extends Model
{
    protected $table = 'wa_factorty_features_case';
    protected $fillable = ['case_matrial','case_mm','water_resistant_depth','glass_type'];
}
