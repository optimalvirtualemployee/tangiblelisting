<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnquiryAboutModel extends Model
{
    protected $table = 're_enquiry_about';
    protected $fillable = ['enquiry_type', 'status'];
}
