<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDeliveryDetailModel extends Model
{
    protected $table = 'tbl_order_delivered_status';
}
