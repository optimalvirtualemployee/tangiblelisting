<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchAdditionalFeaturesModel extends Model
{
    protected $table = 'wa_watch_additional_features';
}
