<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaqCategoryModel extends Model
{
    protected $table = 'tbl_faqcategory';
}
