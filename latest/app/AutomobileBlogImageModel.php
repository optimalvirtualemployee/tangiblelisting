<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobileBlogImageModel extends Model
{
    protected $table = 'au_blog_images';
}
