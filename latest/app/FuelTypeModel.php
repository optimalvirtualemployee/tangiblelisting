<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FuelTypeModel extends Model
{
    protected $table = 'au_fuel_type';
    protected $fillable = ['fuel_type', 'status'];
}
