<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyNewsImageModel extends Model
{
    protected $table = 're_property_news_images';
}
