<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyNewsModel extends Model
{
    protected $table = 're_property_news';
}
