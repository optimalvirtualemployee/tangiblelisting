<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobileBodyTypeModel extends Model
{
    protected $table = 'au_body_type';
    protected $fillable = ['body_type', 'status'];
}
