<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobileCylindersModel extends Model
{
    protected $table = 'au_cylinders';
    protected $fillable = ['cylinders', 'status'];
}
