<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//defined city model here
class CityModel extends Model
{
    //defined city model details here
    protected $table = 'tbl_city';
}
