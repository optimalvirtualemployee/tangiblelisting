<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobileSecurityImageModel extends Model
{
    protected $table = 'au_automobile_security_images';
}
