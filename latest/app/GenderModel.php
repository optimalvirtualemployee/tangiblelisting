<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenderModel extends Model
{
    protected $table = 'wa_gender';
    protected $fillable = ['gender', 'status'];
}
