<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeModel extends Model
{
    protected $table = 'wa_type';
    protected $fillable = ['watch_type', 'status'];
}
