<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopPropertyTypeModel extends Model
{
    protected $table = 're_top_property_type';
    protected $fillable = ['property_type', 'status'];
}
