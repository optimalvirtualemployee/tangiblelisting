<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageSettingsModelForDealers extends Model
{
    protected $table = 'tbl_package_settings_for_dealers';
    protected $fillable = ['package_id','no_of_active_listing','no_of_images_per_listing','normal_search_result','weekly_statistics','basic_analytics','prioritized_search_result','escrow_platform','personalized_logo','advanced_analytics','top_search_result','choice_listing_on_home_page','tangible_listing_on_social_media','professional_analytics'];
}
