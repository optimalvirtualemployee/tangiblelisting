<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BraceletColorModel extends Model
{
    protected $table = 'wa_factory_features_bracelet_color';
    protected $fillable = ['bracelet_color', 'status'];
}
