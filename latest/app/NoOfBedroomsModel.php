<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoOfBedroomsModel extends Model
{
    protected $table = 're_beds';
    protected $fillable = ['number_of_bedrooms', 'status'];
}
