<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrencyConverterModel extends Model
{
    protected $table = 'currencies';
}
