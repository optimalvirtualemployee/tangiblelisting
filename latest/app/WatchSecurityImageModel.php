<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchSecurityImageModel extends Model
{
    protected $table = 'wa_watch_security_images';
}
