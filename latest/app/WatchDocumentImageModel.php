<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchDocumentImageModel extends Model
{
    protected $table = 'wa_watch_document_images';
}
