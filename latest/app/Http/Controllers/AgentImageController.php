<?php

namespace App\Http\Controllers;

use App\AgentImageModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class AgentImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function createImage(Request $request, $listing_id)
    {
        return view('admin.admincommon.agentimages.create',
            compact('listing_id'));
    }
    
    public function create()
    {
        
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'photos'  => 'required',
            'photos.*' => 'mimes:jpg,jpeg,png,gif'
        ]);
        
        
        foreach ($request->file('photos') as $image){
            $uploadImage = new AgentImageModel();
            
            $extension = $image->getClientOriginalExtension();
            Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
            
            $uploadImage->images = $image->getClientMimeType();
            $uploadImage->filename = $image->getFilename().'.'.$extension;
            $uploadImage->listing_id = $request->input('listing_id');
            $uploadImage->save();
        }
        
        return redirect('/admin/uploadagentimage/'.$request->input('listing_id'))->with('success_msg', 'Agent image Created successfully!');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $listing_id = $id;
        $details = AgentImageModel::where('listing_id', '=', $listing_id)
        ->get();
        
        return view('admin.admincommon.agentimages.index', compact('details','listing_id'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = AgentImageModel::find($id);
        $listing_id = $image->listing_id;
        
        $deleteRecords = AgentImageModel::find($id)->delete();
        return redirect('/admin/uploadagentimage/'.$listing_id)->with('success_msg', 'Agent Image Deleted successfully!');
    }
}
