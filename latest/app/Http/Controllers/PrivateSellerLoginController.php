<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Session;
use App\User;
class PrivateSellerLoginController extends Controller
{
    public function login() {
        return view('privateSeller.login');
    }
    public function register(){
        return view('privateSeller.register');
    }

    function checklogin(Request $request) {
			$this->validate($request, [
				'email'   => 'required|email',
				'password'  => 'required|min:3'
			]);
			$user_data = array(
				'email'  => $request->post('email'),
				'password' => $request->post('password'),
                'usertype' =>'4'
			);
		if(Auth::attempt($user_data)){

            if(Auth::user()->status=='1'){
                session(['url.intended' => url()->previous()]);
                return redirect(session()->get('url.intended'));
                //return redirect('/privateSeller/dashboard');
            }
            else{
                return back()->withErrors(['error' => ['User is not active.']]);
            }

		} else {
            return back()->withErrors(['error' => ['Invalid user name or password.']]);
		}
    }
    
    public function newprivateSellerRegister(Request $request){
        $this->validate($request, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'min:10', 'max:12'],
            'password' => ['required', 'string', 'min:8'],
        ]);

        $regUser = new User;
        $regUser->usertype  =  4;
        $regUser->first_name  =  $request->first_name;
        $regUser->last_name  =   $request->last_name;
        $regUser->email  =   $request->email;
        $regUser->phone =   $request->phone;
        $regUser->password  =   Hash::make($request->password);
        $regUser->save();
            $user_data = array(
                'email'  => $request->post('email'),
                'password' => $request->post('password'),
                'usertype' =>'4'
            );
        Auth::attempt($user_data);
        if(Auth::user()->status=='1'){
            session(['url.intended' => url()->previous()]);
            return redirect(session()->get('url.intended'));
            //return redirect('/privateSeller/dashboard');
        }
        else{
            return back()->withErrors(['error' => ['User is not active.']]);
        }
    }

    function successlogin()  {
     	return view('dashbord');
    }

    function logout()  {
	    Auth::logout();
	    return redirect('main');
    }
}
