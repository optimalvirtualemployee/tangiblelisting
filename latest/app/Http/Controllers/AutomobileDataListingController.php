<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\AgentModel;
use App\AutomobileDataListingModel;
use App\BuildYearModel;
use App\CityModel;
use App\CountryModel;
use App\StateModel;
use App\CarModel;
use App\CarPriceModel;
use App\FuelTypeModel;
use App\ColourModel;
use App\NoOfDoorsModel;
use App\AutomobileBodyTypeModel;
use App\InteriorColourModel;
use App\CarBrandModel;
use App\CurrencyModel;
use App\AutomobileCommentModel;
use App\AutomobileImageModel;
use App\Timezone;
use App\TransmissionModel;
use App\AutomobileFeatureListingModel;
use App\AutomobileFeaturesModel;
use App\AutomobileAdditionalFeaturesModel;
use App\AutomobileSecurityImageModel;
use App\AutomobileDamageImageModel;

class AutomobileDataListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = AutomobileDataListingModel::select('au_automobile_detail.id as id','ad_title', 'au_automobile_detail.status as status' ,'automobile_model_name','automobile_brand_name', 'odometer','approval_status')
        ->join('au_brands', 'au_brands.id', '=', 'au_automobile_detail.make_id')
        ->join('au_model','au_model.id', '=', 'au_automobile_detail.model_id')
        ->get();
        
        return view('admin.automobile.listing_datamsf.index')->with(array(
            'details' => $details
            
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $state_data = StateModel::get();
        $currency_data = CurrencyModel::get();
        $country_data = CountryModel::get();
        $makes = CarModel::get();
        $models = CarBrandModel::get();
        $prices = CarPriceModel::get();
        $fuel_types = FuelTypeModel::get();
        $colours = ColourModel::get();
        $no_of_doors = NoOfDoorsModel::where('status','1')->get();
        $body_types = AutomobileBodyTypeModel::get();
        $interior_colours = InteriorColourModel::get();
        $transmissions = TransmissionModel::get();
        $agents = AgentModel::get();
        $year = BuildYearModel::get();
        $timezone = Timezone::Orderby('offset')->get();
        
        $feature_listing = AutomobileFeatureListingModel::get();
        
        $html ='';
        foreach ($feature_listing as $values){
            $id = $values->id;
            $propertyName = $values->property_name;
            
            $html .= '<div class="form-group">
                    <tr>
                   <td><label style= margin-right:10px!important; ><Strong>'.$propertyName.':</Strong></label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$propertyName.'" name="'.$id.'" value="1"><label style= margin-right:5px!important; for="yes"> Yes </label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$propertyName.'" name="'.$id.'" value="0" checked><label style= margin-right:5px!important; for="no"> No</label></td>
                    </tr>
                      </div>';
            
        }
        
        return view('admin.automobile.listing_datamsf.create', 
                    compact('timezone','html','year','agents','currency_data','state_data','country_data','makes','models','prices','fuel_types','colours','no_of_doors','body_types','interior_colours', 'transmissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'currency' => 'required',
            'cityId' => 'required',
            'countryId' => 'required',
            'make' => 'required',
            'model' => 'required',
            'fuel_type' => 'required',
            'colour' => 'required',
            'body_type' => 'required',
            'transmission' => 'required',
            'rhdorlhd' => 'required',
            'year' => 'required',
            'neworused' => 'required',
            'odometer' => 'required',
            'metric' => 'required',
            'vin' => 'required',
            'comment' => 'required',
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
            
        ]);
        
        $converted_amount=NULL;
        if($request->input('value') !=''){
        $currency_code = CurrencyModel::find($request->input('currency'));
        
        $converted_amount = currency()->convert(floatval($request->input('value')), $currency_code->currency_code, "USD",false);
        }
        $agent = AgentModel::find($request->input('agent'));
        $dealer_id = $agent->agency_id;
        
        $createAutomobile = new AutomobileDataListingModel();
        
        $createAutomobile->ad_title = $request->input('adTitle');
        $createAutomobile->currency_id = $request->input('currency');
        if($request->input('stateId') != '')
        $createAutomobile->state_id = $request->input('stateId');
        $createAutomobile->country_id = $request->input('countryId');
        $createAutomobile->city_id = $request->input('cityId');
        $createAutomobile->make_id = $request->input('make');
        $createAutomobile->model_id = $request->input('model');
        $createAutomobile->price_on_request = $request->input('price_on_request');
        $createAutomobile->value = $converted_amount;
        $createAutomobile->price_id = $request->input('price');
        $createAutomobile->fuel_type_id = $request->input('fuel_type');
        $createAutomobile->colour_id = $request->input('colour');
        if($request->input('no_of_doors') != '')
        $createAutomobile->no_of_doors_id = $request->input('no_of_doors');
        $createAutomobile->body_type_id = $request->input('body_type');
        $createAutomobile->transmission = $request->input('transmission');
        if($request->input('registrationPlace') != '')
        $createAutomobile->registration_place = $request->input('registrationPlace');
        $createAutomobile->rhdorlhd  = $request->input('rhdorlhd');
        $createAutomobile->year_id = $request->input('year');
        $createAutomobile->neworused = $request->input('neworused');
        $createAutomobile->odometer = $request->input('odometer');
        $createAutomobile->metric = $request->input('metric');
        if($request->input('engine') != '')
        $createAutomobile->engine = $request->input('engine');
        if($request->input('interior_colour') != '')
        $createAutomobile->interior_colour_id = $request->input('interior_colour');
        $createAutomobile->transmission_id = $request->input('transmission');
        $createAutomobile->agent_id = $request->input('agent');
        $createAutomobile->agencies_id = $dealer_id;
        $createAutomobile->vin_number = $request->input('vin');
        $createAutomobile->registration_number = $request->input('regNum');
        $createAutomobile->timezone_id = $request->input('timezone');
        
        $createAutomobile->save();
        
        $listing_id = $createAutomobile->id;
        
        if($request->input('additional_feature') ?? ''){
            
            foreach ($request->input('additional_feature') as $features){
                
                if($features != null){
                    $uploadFeature = new AutomobileAdditionalFeaturesModel();
                    
                    $uploadFeature->feature = $features;
                    $uploadFeature->listing_id = $listing_id;
                    $uploadFeature->save();
                }
            }
        }
        
        foreach ($request->request as $key => $value){
            if($key != '_token' && $key != 'listing_id' && gettype($key) != 'string'){
                $createFeature = new AutomobileFeaturesModel();
                $createFeature->listing_id = $listing_id;
                $createFeature->feature_id = $key;
                $createFeature->feature_available = $value;
                $createFeature->save();
            }
            
        }
        
        if($request->comment ?? ''){
            $createComment = new AutomobileCommentModel();
            $createComment->comment = $request->input('comment');
            $createComment->listing_id = $listing_id;
            $createComment->save();
        }
        if($request->file('photos') ?? ''){
            
            foreach ($request->file('photos') as $image){
                $uploadImage = new AutomobileImageModel();
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                
                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename().'.'.$extension;
                $uploadImage->listing_id = $listing_id;
                $uploadImage->save();
            }
        }
        
        return redirect('/admin/createautomobiledata')->with('success_msg', 'Automobile Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $automobile = AutomobileDataListingModel::find($id);
        $currency_data = CurrencyModel::get();
        $state_data = StateModel::get();
        $country_data = CountryModel::get();
        $city_data = CityModel::get();
        $makes = CarModel::get();
        $prices = CarPriceModel::get();
        $fuel_types = FuelTypeModel::get();
        $colours = ColourModel::get();
        $no_of_doors = NoOfDoorsModel::where('status','1')->get();
        $body_types = AutomobileBodyTypeModel::get();
        $interior_colours = InteriorColourModel::get();
        $transmissions = TransmissionModel::get();
        $agents = AgentModel::get();
        $year = BuildYearModel::get();
        $timezone = Timezone::Orderby('offset')->get();
        
        $currencySelected = CurrencyModel::find($automobile->currency_id);
        $citySelected = CityModel::find($automobile->city_id);
        $stateSelected = StateModel::find($automobile->state_id);
        $countrySelected = CountryModel::find($automobile->country_id);
        $makeSelected = CarModel::find($automobile->make_id);
        $modelSelected = CarBrandModel::find($automobile->model_id);
        $priceSelected = CarPriceModel::find($automobile->price_id);
        $fuelTypeSelected = FuelTypeModel::find($automobile->fuel_type_id);
        $colourSelected = ColourModel::find($automobile->colour_id);
        $noOfDoorsSelected = NoOfDoorsModel::find($automobile->no_of_doors_id);
        $bodyTypeSelected = AutomobileBodyTypeModel::find($automobile->body_type_id);
        $interiorColourSelected = InteriorColourModel::find($automobile->interior_colour_id);
        $transmissionSelected = TransmissionModel::find($automobile->transmission_id);
        $agentSelected = AgentModel::find($automobile->agent_id);
        $yearSelected = BuildYearModel::find($automobile->year_id);
        $timezoneSelected = Timezone::find($automobile->timezone_id);
        
        $currency_code = $currencySelected->currency_code;
        
        $converted_amount = currency()->convert(floatval($automobile->value), "USD", $currency_code ,false);
        
        $automobile->value = $converted_amount;
        
        $models = CarBrandModel::select('id','automobile_model_name')->where('automobile_brand_id', '=',$makeSelected->id)->get();
        
        $additionlFeatures = AutomobileAdditionalFeaturesModel::select('au_automobile_additional_features.id as id', 'listing_id' , 'feature')
        ->where('listing_id', '=', $id)
        ->get();
        
        $additionalHtml = '';
        foreach($additionlFeatures as $features){
            
            $additionalHtml .= '<tr class = "control-group-feature">
                    <td><label  >Additional Feature</label>
                    <input  class="form-control form-control-user " type="text" maxLength="100"  name="additional_feature[]" value="'.$features->feature.'"></td>
                    <td><button class="btn btn-primary  btn-danger btn-danger-feature" type="button">
				<i class="glyphicon glyphicon-remove"></i> Remove
			</button></td>
                    </tr>';
        }
        
        $feature_list = AutomobileFeatureListingModel::where('status', '=', '1')->get();
        
        $html = '';
        foreach ($feature_list as $values){
            
            $feature_data = AutomobileFeaturesModel::where('feature_id', '=', $values->id)->where('listing_id', '=', $id)
            ->where('feature_available', '=', '1')->first();
            
            
            if($feature_data != null){

                $html .= '<div class="form-group">
                    <tr>
                    <td><label style= margin-right:10px!important; ><Strong>'.$values->property_name.':</Strong></label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$values->propertyName.'" name="'.$values->id.'" value="1" checked><label style= margin-right:5px!important; for="yes"> Yes </label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$values->propertyName.'" name="'.$values->id.'" value="0" ><label style= margin-right:5px!important; for="no"> No</label></td>
                    </tr>
                    </div>';
            }else{
                
                $html .= '<div class="form-group">
                    <tr>
                    <td><label style= margin-right:10px!important; ><Strong>'.$values->property_name.':</Strong></label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$values->propertyName.'" name="'.$values->id.'" value="1" ><label style= margin-right:5px!important; for="yes"> Yes </label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$values->propertyName.'" name="'.$values->id.'" value="0" checked><label style= margin-right:5px!important; for="no"> No</label></td>
                    </tr>
                    </div>';
            }
        }
        
        $comment_data = AutomobileCommentModel::where('listing_id',$id)->first();
        
        $images_data = AutomobileImageModel::where('listing_id', '=', $id)->get();
        
        $images_damage = AutomobileDamageImageModel::where('listing_id', '=', $id)->get();
        
        $security_data = AutomobileSecurityImageModel::where('listing_id', '=', $id)->get();
        
        return view('admin.automobile.listing_datamsf.edit',
            compact('timezoneSelected','timezone','additionalHtml','html','year','yearSelected','agents','agentSelected','automobile','currency_data','citySelected','city_data','state_data','country_data','makes','models','prices','fuel_types','colours','no_of_doors','body_types'
                    ,'interior_colours','stateSelected','countrySelected','makeSelected','modelSelected','priceSelected','fuelTypeSelected'
                    ,'colourSelected','noOfDoorsSelected','bodyTypeSelected','interiorColourSelected', 'currencySelected', 'images_damage','comment_data', 'images_data', 'transmissions','transmissionSelected', 'security_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'currency' => 'required',
            'cityId' => 'required',
            'countryId' => 'required',
            'make' => 'required',
            'model' => 'required',
            'fuel_type' => 'required',
            'colour' => 'required',
            'body_type' => 'required',
            'transmission' => 'required',
            'rhdorlhd' => 'required',
            'year' => 'required',
            'neworused' => 'required',
            'odometer' => 'required',
            'metric' => 'required',
            'vin' => 'required',
            'comment' => 'required',
            'listingApproval' => 'required',
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
            
        ]);
        
        
        
        $value = NULL;
        if($request->input('value') !=''){
            $currency_code = CurrencyModel::find($request->input('currency'));
            
            $value = currency()->convert(floatval($request->input('value')), $currency_code->currency_code, "USD",false);
            
        }
        
        $price = NULL;
        if($request->input('price') !=''){
            $price = $request->input('price');
            
        }
        
        $no_of_door = NULL;
        if($request->input('no_of_doors') != ''){
            $no_of_door = $request->input('no_of_doors');
        }
        
        $registration = NULL;
        if($request->input('registrationPlace') != ''){
            $registration = $request->input('registrationPlace');
        }
        
        $engine = NULL;
        if($request->input('engine') != ''){
            $engine = $request->input('engine');
        }
        
        $interior_colour = NULL;
        if($request->input('interior_colour') != ''){
            $interior_colour = $request->input('interior_colour');
        }
        
        $agent = AgentModel::find($request->input('agent'));
        $dealer_id = $agent->agency_id;
        
        $updateAutomobile = AutomobileDataListingModel::find($id);
        
        $updateAutomobile->ad_title = $request->input('adTitle');
        $updateAutomobile->currency_id = $request->input('currency');
        if($request->input('stateId') != '')
        $updateAutomobile->state_id = $request->input('stateId');
        $updateAutomobile->country_id = $request->input('countryId');
        $updateAutomobile->city_id = $request->input('cityId');
        $updateAutomobile->make_id = $request->input('make');
        $updateAutomobile->model_id = $request->input('model');
        $updateAutomobile->price_on_request = $request->input('price_on_request');
        $updateAutomobile->value = $value;
        $updateAutomobile->price_id = $price;
        $updateAutomobile->fuel_type_id = $request->input('fuel_type');
        $updateAutomobile->colour_id = $request->input('colour');
        $updateAutomobile->no_of_doors_id = $no_of_door;
        $updateAutomobile->body_type_id = $request->input('body_type');
        $updateAutomobile->transmission = $request->input('transmission');
        $updateAutomobile->registration_place = $registration;
        $updateAutomobile->rhdorlhd  = $request->input('rhdorlhd');
        $updateAutomobile->year_id = $request->input('year');
        $updateAutomobile->neworused = $request->input('neworused');
        $updateAutomobile->odometer = $request->input('odometer');
        $updateAutomobile->metric = $request->input('metric');
        $updateAutomobile->engine = $engine;
        $updateAutomobile->interior_colour_id = $interior_colour;
        $updateAutomobile->agent_id = $request->input('agent');
        $updateAutomobile->agencies_id = $dealer_id;
        $updateAutomobile->vin_number = $request->input('vin');
        $updateAutomobile->registration_number = $request->input('regNum');
        $updateAutomobile->timezone_id = $request->input('timezone');
        $updateAutomobile->approval_status = $request->input('listingApproval');
        
        $updateAutomobile->save();
        
        $deleteRecords = AutomobileAdditionalFeaturesModel::where('listing_id', '=', $id)->delete();
        
        if($request->input('additional_feature') ?? ''){
            
            foreach ($request->input('additional_feature') as $features){
                
                if($features != null){
                    $uploadFeature = new AutomobileAdditionalFeaturesModel();
                    
                    $uploadFeature->feature = $features;
                    $uploadFeature->listing_id = $id;
                    $uploadFeature->save();
                }
            }
        }
        
        $deleteRecords = AutomobileFeaturesModel::where('listing_id', '=', $id)->delete();
        
        foreach ($request->request as $key => $value){
            
            if($key != '_token' && $key != 'listing_id' && gettype($key) != 'string'){
                $createFeature = new AutomobileFeaturesModel();
                $createFeature->listing_id = $id;
                $createFeature->feature_id = $key;
                $createFeature->feature_available = $value;
                $createFeature->save();
            }
            
        }
        
        if($request->comment ?? ''){
            AutomobileCommentModel::where('listing_id',$id)->delete();
            $updateComment = new AutomobileCommentModel();
            $updateComment->comment = $request->comment;
            $updateComment->listing_id = $id;
            $updateComment->save();
        }
        if($request->file('photos') ?? ''){
            foreach ($request->file('photos') as $image){
                $uploadImage = new AutomobileImageModel();
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                
                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename().'.'.$extension;
                $uploadImage->listing_id = $id;
                $uploadImage->save();
            }
        }
        
        return redirect('/admin/createautomobiledata')->with('success_msg', 'Automobile Updated successfully!');
    }
    
    //this function is used to get the models based on selected brand
    public function getModelByMake(Request $request){
        
        $brandId = $request->input('brandId');
        $city = CarBrandModel::select('id', 'automobile_model_name')->where('automobile_brand_id', $brandId)->get();
        return $city;
    }
    
    public function updateLatestOffer($id){
        
        $automobile = AutomobileDataListingModel::find($id);
        
        if($automobile->latestOffer == '0'){
            
            $automobile->latestOffer = '1';
            
        }else{
            $automobile->latestOffer = '0';
        }
        
        $automobile->save();
        
        return redirect('/admin/createautomobiledata')->with('success_msg', 'Latest Offer Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = AutomobileDataListingModel::find($id)->delete();
        return redirect('/admin/createautomobiledata')->with('success_msg', 'Automobile Deleted successfully!');
    }
}
