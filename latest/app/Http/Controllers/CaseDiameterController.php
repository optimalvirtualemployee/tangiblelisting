<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CaseDiameterModel;

class CaseDiameterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $case_diameter = CaseDiameterModel::get();
        return view('admin.watch.case_diameter.index',compact('case_diameter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.case_diameter.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'case_diameter' => 'required|regex:/^\d+(\.\d{1,2})?$/|unique:wa_case_diameter',
            'status'=> 'required'
        ]);

        $case_diameter = CaseDiameterModel::create($validate);
        $case_diameter->save();
        return redirect('/admin/case_diameter')->with('success_msg','Case Diameter Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $case_diameter_data = CaseDiameterModel::find($id);
        return view('admin.watch.case_diameter.edit', compact('case_diameter_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'case_diameter' => 'required|regex:/^\d+(\.\d{1,2})?$/'
        ]);

        $case_diameterUpdate = CaseDiameterModel::find($id);

        if ($case_diameterUpdate->case_diameter == $validate['case_diameter']) {
            $case_diameterUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['case_diameter' => 'unique:wa_case_diameter']);
            $case_diameterUpdate->case_diameter = $validate['case_diameter'];
            $case_diameterUpdate->status = $request->input('status');
        }

        $case_diameterUpdate->save();

        return redirect('/admin/case_diameter')->with('success_msg','Case Diameter Updated successfully!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = CaseDiameterModel::find($id)->delete();
        return redirect('/admin/case_diameter')->with('success_msg','Case Diameter deleted successfully!');
    }
}
