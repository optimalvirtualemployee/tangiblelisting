<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CurrencyModel;

class CurrencyDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Responsere
     */
    public function index()
    {
        $currencies = CurrencyModel::get();
        
        return view('admin.admincommon.package_for_currencies.display')->with(array('currencies' => $currencies));
    }
    // function to display all currency to the view
    public function selectCurrency(){
        
        $currencies = CurrencyModel::get();
        
        return view('admin.admincommon.selectcurrency.index')->with(array('currencies' => $currencies));
    }
    
    // Function to update the selected currency to active and other to deactive status
    public function saveSelectCurrency(Request $request, $id){
        
        CurrencyModel::where('status', '=', '1')->update(['status' => '0']);
        
        $updateSelectedCurrency = CurrencyModel::find($id);
        
        $updateSelectedCurrency->status = '1';
        
        $updateSelectedCurrency->save();
        
        return redirect('/admin/currencySelect/select')->with('success_msg', 'Currency Selected successfully!');
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admincommon.package_for_currencies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'currencyCode' => ['required', 'unique:tbl_currency,currency_code'],
            'currency' => ['required'],
            'status' => ['required']
        ]);
        
        $createCurrency = new CurrencyModel();
        
        $createCurrency->currency_code = $request->input('currencyCode');
        $createCurrency->currency = $request->input('currency');
        $createCurrency->status = $request->input('status');
        
        $createCurrency->save();
        return redirect('/admin/currencyData')->with('success_msg', 'Currency Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $currency = CurrencyModel::find($id);
        
        return view('admin.admincommon.package_for_currencies.edit')->with(array('currency' => $currency));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'currencyCode' => ['required'],
            'currency' => ['required'],
            'currencystatus' => ['required']
        ]);
        
        $updateCurrency = CurrencyModel::find($id);
        
        $updateCurrency->currency_code = $request->input('currencyCode');
        $updateCurrency->currency = $request->input('currency');
        $updateCurrency->status = $request->input('currencystatus');
        
        $updateCurrency->save();
        
        return redirect('/admin/currencyData')->with('success_msg', 'Currency Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = CurrencyModel::find($id)->delete();
        return redirect('/admin/currencyData')->with('success_msg', 'Currency Deleted successfully!');
    }
}
