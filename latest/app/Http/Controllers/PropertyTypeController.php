<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PropertyTypeModel;
use App\TopPropertyTypeModel;

class PropertyTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $property_type = PropertyTypeModel::Select('re_property_type.id as id', 're_property_type.property_type as property_type', 're_property_type.status as status')
        ->get();
        
        return view('admin.realestate.property_type.index',compact('property_type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $top_property_type = TopPropertyTypeModel::get();
        return view('admin.realestate.property_type.create', compact('top_property_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'property_type' => 'required|unique:re_property_type',
            'status' => 'required'
        ]);

        $property_type = PropertyTypeModel::create($validate);
        $property_type->save();
        return redirect('/admin/property_type')->with('success_msg','Property Type Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $property_type_data = PropertyTypeModel::find($id);
        
        return view('admin.realestate.property_type.edit', compact('property_type_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'property_type' => 'required|unique:re_property_type,property_type, '.$id
        ]);

        $property_typeUpdate = PropertyTypeModel::find($id);

        if ($property_typeUpdate->property_type == $validate['property_type']) {
            $property_typeUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['property_type' => 'unique:re_property_type']);
            $property_typeUpdate->property_type = $validate['property_type'];
            $property_typeUpdate->status = $request->input('status');
        }

        $property_typeUpdate->save();

        return redirect('/admin/property_type')->with('success_msg','Property Type Updated successfully!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $deleteRecords = PropertyTypeModel::find($id)->delete();
        return redirect('/admin/property_type')->with('success_msg','Property Type deleted successfully!');
    }
}
