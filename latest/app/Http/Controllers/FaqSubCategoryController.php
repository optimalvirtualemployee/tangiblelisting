<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\FaqSubCategoryModel;
use App\FaqCategoryModel;

class FaqSubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Categories = FaqCategoryModel::where('status','1')->get();
        $FAQSubCategoryListing = FaqSubCategoryModel::get();
        return view('admin.admincommon.faq.subcategory.index', compact('FAQSubCategoryListing','Categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Categories = FaqCategoryModel::where('status','1')->get();
        return view('admin.admincommon.faq.subcategory.create',compact('Categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'category'   => 'required',
            'subcategory'=> 'required',
            'status'     => 'required',
        ]);

        $faqSubCat            = new FaqSubCategoryModel;
        $faqSubCat->name      = $request->subcategory;
        $faqSubCat->categoryId= $request->category;
        $faqSubCat->status    = $request->status;
        
        $faqSubCat->save();
        return redirect('/admin/faq-subcat')->with('success_msg','FAQ sub-category added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $Categories = FaqCategoryModel::where('status','1')->get();
        $editData = FaqSubCategoryModel::find($id);
        return view('admin.admincommon.faq.subcategory.edit',compact('editData','Categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $faqSubCat = FaqSubCategoryModel::find($id);

        $validate = $this->validate($request, [
            'category'   => 'required',
            'subcategory'=> 'required',
            'status'     => 'required',
        ]);

        $faqSubCat->name      = $request->subcategory;
        $faqSubCat->categoryId= $request->category;
        $faqSubCat->status    = $request->status;
        
        $faqSubCat->save();
        return redirect('/admin/faq-subcat')->with('success_msg','FAQ sub-category updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = FaqSubCategoryModel::find($id);    
        $post->delete();
        return redirect()->back()->with('success_msg','FAQ Sub Category deleted successfully!');
    }
    
    public function getSubCategory(Request $request){
        $catID = $request->categoryId;
        $getsubCat = FaqSubCategoryModel::where('categoryId',$catID)->where('status','1')->get();
        echo json_encode($getsubCat);
    }
}
