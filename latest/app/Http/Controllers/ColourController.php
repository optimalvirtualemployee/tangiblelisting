<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ColourModel;

class ColourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colour = ColourModel::get();
        return view('admin.automobile.colour.index',compact('colour'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.automobile.colour.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'colour' => 'required|regex:/^([a-zA-Z_]+)(\s[a-zA-Z_]+)*$/|unique:au_colour',
            'status' => 'required'
        ]);

        $colour = ColourModel::create($validate);
        $colour->save();
        return redirect('/admin/colour')->with('success_msg','Colour Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $colour_data = ColourModel::find($id);
        return view('admin.automobile.colour.edit', compact('colour_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'colour' => 'required|regex:/^([a-zA-Z_]+)(\s[a-zA-Z_]+)*$/'
        ]);

        $colourUpdate = ColourModel::find($id);

        if ($colourUpdate->colour == $validate['colour']) {
            $colourUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['colour' => 'unique:au_colour']);
            $colourUpdate->colour = $validate['colour'];
            $colourUpdate->status = $request->input('status');
        }

        $colourUpdate->save();

        return redirect('/admin/colour')->with('success_msg','Colour Updated successfully!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = ColourModel::find($id)->delete();
        return redirect('/admin/colour')->with('success_msg','Colour deleted successfully!');
    }
}
