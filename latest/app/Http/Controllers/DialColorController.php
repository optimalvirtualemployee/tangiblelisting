<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DialColorModel;

class DialColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dial = DialColorModel::get();
        return view('admin.watch.dialColor.index',compact('dial'));
    }

    /**Power Reserve
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.dialColor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validate($request, [
            "dial_color" => 'required|unique:wa_factory_features_dial_color',
            'status' => 'required'
            
        ]);
        
        $dial = DialColorModel::create($validator);
        $dial->save();
        
        return redirect('/admin/dialColor')->with('success_msg','Dial Color Created successfully!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dial_data = DialColorModel::find($id);
        return view('admin.watch.dialColor.edit', compact('dial_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            "dial_color" => 'required',
            "status" => 'required'
            
        ]);
            
            $dial = DialColorModel::find($id);
            $dial->dial_color = $request->input('dial_color');
            $dial->status = $request->input('status');
            $dial->save();
        
            return redirect('/admin/dialColor')->with('success_msg','Dial Color Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = DialColorModel::find($id)->delete();
        return redirect('/admin/dialColor')->with('success_msg','Dial Color deleted successfully!');
    }
}
