<?php

namespace App\Http\Controllers;

use App\BraceletClaspMaterialModel;
use Illuminate\Http\Request;

class BraceletClaspMaterialController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:watch-list|watch-create|watch-edit|watch-delete', ['only' => ['index','store']]);
        $this->middleware('permission:watch-create', ['only' => ['create','store']]);
        $this->middleware('permission:watch-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:watch-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bracelet = BraceletClaspMaterialModel::get();
        return view('admin.watch.braceletClaspMaterial.index',compact('bracelet'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.braceletClaspMaterial.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = $this->validate($request, [
            "clasp_material" => 'required|unique:wa_factory_features_bracelet_claspMaterial',
            "status" => 'required'
        ]);
        
        $bracelet = BraceletClaspMaterialModel::create($validator);
        $bracelet->save();
        
        return redirect('/admin/braceletClaspMaterial')->with('success_msg','Bracelet Clasp Material Created successfully!');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bracelet_data = BraceletClaspMaterialModel::find($id);
        return view('admin.watch.braceletClaspMaterial.edit', compact('bracelet_data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            "clasp_material" => 'required',
            "status" => 'required'
        ]);
            
            $bracelet = BraceletClaspMaterialModel::find($id);
            $bracelet->clasp_material = $request->input('clasp_material');
            $bracelet->status = $request->input('status');
            $bracelet->save();
        
        return redirect('/admin/braceletClaspMaterial')->with('success_msg','Bracelet Clasp Material Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = BraceletClaspMaterialModel::find($id)->delete();
        return redirect('/admin/braceletClaspMaterial')->with('success_msg','Bracelet Clasp Material deleted successfully!');
    }
}
