<?php

namespace App\Http\Controllers;

use App\PropertyDataModel;
use Illuminate\Http\Request;

class PropertyDreamLocationController extends Controller
{
    public function showDreamLocationProperties(Request $request){
        
        $properties = PropertyDataModel::select('re_property_details.id', 'ad_title', 'land_size', 're_property_details.state_id', 're_property_details.city_id', 'city_name', 'state_name')->join('tbl_city', 'tbl_city.id', '=', 're_property_details.city_id')
        ->join('tbl_state', 'tbl_state.id', '=', 're_property_details.state_id')
        ->where('dream_location', '=','1')
        ->get();
        
        return view('admin.realestate.dreamlocation.dreamlocation')->with(array(
            'properties' => $properties
        ));
        
    }
    
    public function editDreamLocation($id){
        
        $featured = PropertyDataModel::find($id);
        
        return view('admin.realestate.dreamlocation.editdreamlocation', compact('featured'));
    }
    
    public function updateDreamLocation(Request $request, $id){
        
        $data = $request->validate([
            'dreamlocation' => ['required']
        ]);
        
        $updateFeatured = PropertyDataModel::find($id);
        
        $updateFeatured->dream_location = $request->input('dreamlocation');
        
        $updateFeatured->save();
        
        return redirect('/admin/dreamlocationproperty')->with('success_msg', 'Property Dream Location Updated successfully!');
    }
}
