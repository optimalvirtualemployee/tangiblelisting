<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CurrencyConverterModel;

class CurrencyConverterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currencies = CurrencyConverterModel::get();
        
        return view('admin.admincommon.currencyconverter.display', compact('currencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admincommon.currencyconverter.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            
            
            'currency' => ['required'],
            'code' => ['required'],
            'symbol' => ['required'],
            'exchange_rate' => ['required'],
            'format' => ['required']
        ]);
        
        $createCurrency = new CurrencyConverterModel();
        
        $createCurrency->name = $request->currency;
        $createCurrency->code = $request->code;
        $createCurrency->symbol = $request->symbol;
        $createCurrency->exchange_rate = $request->exchange_rate;
        $createCurrency->format = $request->format;
        
        $createCurrency->save();
        
        return redirect('/admin/currencyconverter')->with('success_msg', 'Currency Conversion Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $currency = CurrencyConverterModel::find($id);
        
        return view('admin.admincommon.currencyconverter.edit',compact('currency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            
            
            'currency' => ['required'],
            'code' => ['required'],
            'symbol' => ['required'],
            'exchange_rate' => ['required'],
            'format' => ['required']
        ]);
        
        $updateCurrency =CurrencyConverterModel::find($id);
        
        $updateCurrency->name = $request->currency;
        $updateCurrency->code = $request->code;
        $updateCurrency->symbol = $request->symbol;
        $updateCurrency->exchange_rate = $request->exchange_rate;
        $updateCurrency->format = $request->format;
        
        $updateCurrency->save();
        
        return redirect('/admin/currencyconverter')->with('success_msg', 'Currency Conversion Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = CurrencyConverterModel::find($id)->delete();
        return redirect('/admin/currencyconverter')->with('success_msg', 'Currency Conversion Deleted successfully!');
    }
}
