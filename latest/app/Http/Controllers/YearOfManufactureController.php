<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\YearOfManufactureModel;

class YearOfManufactureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $year_of_manufacture = YearOfManufactureModel::get();
        return view('admin.watch.year_of_manufacture.index',compact('year_of_manufacture'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.year_of_manufacture.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'year_of_manufacture' => 'required|digits:4|integer|min:1900|max:'.(date('Y')+1).'|unique:wa_year_of_manufacture',
            'status' => 'required'
        ]);

        $year_of_manufacture = YearOfManufactureModel::create($validate);
        $year_of_manufacture->save();
        return redirect('/admin/year_of_manufacture')->with('success_msg','Year of Manufacture Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $year_of_manufacture_data = YearOfManufactureModel::find($id);
        return view('admin.watch.year_of_manufacture.edit', compact('year_of_manufacture_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'year_of_manufacture' => 'required|digits:4|integer|min:1900|max:'.(date('Y')+1)
        ]);

        $year_of_manufactureUpdate = YearOfManufactureModel::find($id);

        if ($year_of_manufactureUpdate->year_of_manufacture == $validate['year_of_manufacture']) {
            $year_of_manufactureUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['year_of_manufacture' => 'unique:wa_year_of_manufacture']);
            $year_of_manufactureUpdate->year_of_manufacture = $validate['year_of_manufacture'];
            $year_of_manufactureUpdate->status = $request->input('status');
        }

        $year_of_manufactureUpdate->save();

        return redirect('/admin/year_of_manufacture')->with('success_msg','Year of Manufacture Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = YearOfManufactureModel::find($id)->delete();
        return redirect('/admin/year_of_manufacture')->with('success_msg','Year of Manufacture deleted successfully!');
    }
}
