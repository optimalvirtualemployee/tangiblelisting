<?php

namespace App\Http\Controllers;

use App\FunctionJumpingHourModel;
use Illuminate\Http\Request;

class FunctionJumpingHourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $functions = FunctionJumpingHourModel::get();
        return view('admin.watch.functionJumpingHour.index',compact('functions'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.functionJumpingHour.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = $this->validate($request, [
            "jumping_hour" => 'required|unique:wa_factory_features_function_jumpingHour,jumping_hour',
            "status" => 'required'
        ]);
        
        $case = FunctionJumpingHourModel::create($validator);
        $case->save();
        
        return redirect('/admin/functionJumpingHour')->with('success_msg','Jumping Hour Created successfully!');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $functions_data = FunctionJumpingHourModel::find($id);
        return view('admin.watch.functionJumpingHour.edit', compact('functions_data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            "jumping_hour" => 'required|unique:wa_factory_features_function_jumpingHour,jumping_hour, ' . $id,
            "status" => 'required'
        ]);
            
            $case = FunctionJumpingHourModel::find($id);
            $case->jumping_hour = $request->input('jumping_hour');
            $case->status = $request->input('status');
            $case->save();
        
        return redirect('/admin/functionJumpingHour')->with('success_msg','Jumping Hour Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = FunctionJumpingHourModel::find($id)->delete();
        return redirect('/admin/functionJumpingHour')->with('success_msg','Jumping Hour Deleted successfully!');
    }
}
