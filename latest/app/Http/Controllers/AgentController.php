<?php

namespace App\Http\Controllers;

use App\AgentModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\AgenciesModel;
use App\AgentImageModel;
use App\CityModel;
use App\CountryModel;
use App\StateModel;
use App\Timezone;
use App\User;
use App\UserPermissionModel;
use Faker\Provider\DateTime;

class AgentController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:agent-list|agent-create|agent-edit|agent-delete', ['only' => ['index','store']]);
        $this->middleware('permission:agent-create', ['only' => ['create','store']]);
        $this->middleware('permission:agent-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:agent-delete', ['only' => ['destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return 
     */
    public function index()
    {
        
        $agents = AgentModel::select('tbl_agent.id as id', 'tbl_agent.status as status','tbl_agent.first_name as first_name','tbl_agent.last_name as last_name','tbl_agent.mobile as mobile','tbl_agent.whatsapp as whatsapp', 'tbl_agencies.first_name as dealer_fname','tbl_agencies.last_name as dealer_lname')
        ->leftJoin('tbl_agencies', 'tbl_agencies.id','=', 'tbl_agent.agency_id')
        ->get();
        
        $agent_images = AgentImageModel::get();
        
        //dd($agent_images);
        return view('admin.admincommon.agent_msf.display')->with(array('agents' => $agents, 'agent_images' => $agent_images));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $agencies = AgenciesModel::get();
        $country_data = CountryModel::get();
        $timezone = Timezone::Orderby('offset')->get();
        
        return view('admin.admincommon.agent_msf.create', compact('agencies', 'country_data', 'timezone'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'countryId' => 'required',
            'cityId' => 'required',
            'street_1' => 'required',
            'street_2' => 'required',
            'postal_code' => 'required',
            'fax' => 'required',
            'office' => 'required',
            'mobile' => 'required',
            'whatsapp' => 'required',
            'status' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirmpassword',
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        
        $createAgent = new AgentModel();
        
        if($request->input('agencyId') != '')
        $createAgent->agency_id = $request->input('agencyId');
        
        $createAgent->first_name = $request->input('first_name');
        $createAgent->last_name = $request->input('last_name');
        $createAgent->country_id = $request->input('countryId');
        if($request->input('stateId') != '')
          $createAgent->state_id = $request->input('stateId');
        $createAgent->city_id = $request->input('cityId');
        $createAgent->street_1 = $request->input('street_1');
        $createAgent->street_2 = $request->input('street_2');
        $createAgent->postal_code = $request->input('postal_code');
        $createAgent->whatsapp = $request->input('whatsapp');
        $createAgent->mobile = $request->input('mobile');
        $createAgent->fax = $request->input('fax');
        $createAgent->office = $request->input('office');
        $createAgent->email = $request->input('email');
        $createAgent->timezone_id = $request->input('timezone');
        $createAgent->status = $request->input('status');
        
        $input['first_name'] = $request->input('first_name');
        $input['last_name'] = $request->input('last_name');
        $input['email'] = $request->input('email');
        $input['password'] = Hash::make($request->input('password'));
        $input['usertype'] = 3;
        $input['role_id'] = 3;
        
        $user = User::create($input);
        $user->assignRole('Agent');
        
        $role = Role::findByName('Agent');
        $permission = Permission::get();
        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
        ->where("role_has_permissions.role_id",$role->id)
        ->get();
        
        if($rolePermissions !=null){
            foreach ($rolePermissions as $permission){
                
                $userPermission = new UserPermissionModel();
                
                $userPermission->userId = $user->id;
                $userPermission->permissionId = $permission->id;
                
                $userPermission->save();
            }
        }
        
        $createAgent->userId = $user->id;
        
        $createAgent->save();
        
        $listing_id = $createAgent->id;
        
        if($request->file('photos') ?? ''){
        foreach ($request->file('photos') as $image){
            $uploadImage = new AgentImageModel();
            
            $extension = $image->getClientOriginalExtension();
            Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
            
            $uploadImage->images = $image->getClientMimeType();
            $uploadImage->filename = $image->getFilename().'.'.$extension;
            $uploadImage->listing_id = $listing_id;
            $uploadImage->save();
        }
        }
        
        return redirect('/admin/agentdata')->with('success_msg', 'Agent Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agent = AgentModel::find($id);
        
        $agencies = AgenciesModel::get();
        $agency_data = AgenciesModel::find($agent->agency_id);
        $country_data = CountryModel::get();
        $state_data = StateModel::get();
        $city_data = CityModel::get();
        $timezone_data = Timezone::Orderby('offset')->get();
        
        $citySelected = CityModel::find($agent->city_id);
        $stateSelected = StateModel::find($agent->state_id);
        $countrySelected = CountryModel::find($agent->country_id);
        $timezoneSelected = Timezone::find($agent->timezone_id);
        
        $images_data = AgentImageModel::where('listing_id', '=', $id)->get();
        
        return view('admin.admincommon.agent_msf.edit', compact('agent', 'agencies', 'agency_data','images_data','country_data', 'citySelected', 'stateSelected',
            'countrySelected', 'state_data', 'city_data', 'timezone_data', 'timezoneSelected'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateAgent = AgentModel::find($id);
        
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'countryId' => 'required',
            'cityId' => 'required',
            'street_1' => 'required',
            'street_2' => 'required',
            'fax' => 'required',
            'office' => 'required',
            'mobile' => 'required',
            'whatsapp' => 'required',
            'status' => 'required',
            'email' => 'required|email|unique:users,email,'.$updateAgent->userId,
            'password' => 'same:confirmpassword',
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        
        
        if($request->input('agencyId') != '')
        $updateAgent->agency_id = $request->input('agencyId');
        
        $updateAgent->first_name = $request->input('first_name');
        $updateAgent->last_name = $request->input('last_name');
        $updateAgent->whatsapp = $request->input('whatsapp');
        $updateAgent->mobile = $request->input('mobile');
        $updateAgent->country_id = $request->input('countryId');
        if($request->input('stateId') != '')
          $updateAgent->state_id = $request->input('stateId');
        $updateAgent->city_id = $request->input('cityId');
        $updateAgent->street_1 = $request->input('street_1');
        $updateAgent->street_2 = $request->input('street_2');
        $updateAgent->postal_code = $request->input('postal_code');
        $updateAgent->fax = $request->input('fax');
        $updateAgent->office = $request->input('office');
        $updateAgent->email = $request->input('email');
        $updateAgent->timezone_id = $request->input('timezone');
        $updateAgent->status = $request->input('status');
        
        $input['first_name'] = $request->input('first_name');
        $input['last_name'] = $request->input('last_name');
        $input['email'] = $request->input('email');
        
        if(!empty($request->input('password'))){
            $input['password'] = Hash::make($request->input('password'));
        }
        
        
        $user = User::find($updateAgent->userId);
        $user->update($input);
        
        
        $updateAgent->save();
        
        if($request->file('photos') ?? ''){
            foreach ($request->file('photos') as $image){
                $uploadImage = new AgentImageModel();
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                
                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename().'.'.$extension;
                $uploadImage->listing_id = $id;
                $uploadImage->save();
            }
        }
        
        return redirect('/admin/agentdata')->with('success_msg', 'Agent Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = AgentModel::find($id)->delete();
        return redirect('/admin/agentdata')->with('success_msg', 'Agent Deleted successfully!');
    }
}
