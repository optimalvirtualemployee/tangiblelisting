<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\CarModel;
use App\CarBrandModel;
use App\CarModelImage;


class AutomobileadminController extends Controller
{
	// this function redirects to dashboard page of automobile
    public function home() {
        return view('admin.automobile.automobiledashboard');
    }

//----------------------------------- CARS BRAND STARTS HERE ---------------------------------------------------//    

    // this function displayes the list of automobile brands which are created in system
    public function displayautomobilebrands(){
    	 $brands = CarModel::select('id','automobile_brand_name','status', 'show_under_brand')
        ->get();
    	 $images = CarModelImage::get();
    	 return view('admin.automobile.displayautomobilebrands')->with(array('brands'=>$brands, 'images'=>$images));
    }

    // this function takes us to the page from where brands can be created
    public function create_car_brands(){
        //return view('admin.automobile.create_car_brands');
        return view('admin.automobile.create_car_brand_msf');
    }

    // this function updates the car brands in the system
    public function updatebrands($id){
      $brands = CarModel::select('id','automobile_brand_name','status', 'show_under_brand')->where('id','=',$id)
        ->get();
        $images_data = CarModelImage::where('listing_id', '=', $id)->get();
        return view('admin.automobile.updatebrandsmsf')->with(array('brands'=>$brands, 'images_data' =>$images_data));
    }

    // this function saves the brands in the system
    public function savebrands(Request $request){
        $brandname = $request->input('brandname');
        $status = $request->input('status');
        $showunderbrand = $request->input('show_under_brand');

        $data = $request->validate(['brandname'   => ['required','unique:au_brands,automobile_brand_name'],
                                    'status' => ['required']
            
        ]);
        
        if($brandname == ''){
            return back()->withErrors(['error' => ['Any of the field cannot be left blanck']]);
        } else {  

            $data = new CarModel;
            $data->automobile_brand_name =  $brandname;
            $data->status =  $status;
            $data->show_under_brand = $showunderbrand;
            $data->save();
            
            $listing_id = $data->id;
            
            if($request->file('photos') ?? ''){
                foreach ($request->file('photos') as $image){
                    $uploadImage = new CarModelImage();
                    
                    $extension = $image->getClientOriginalExtension();
                    Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                    
                    $uploadImage->images = $image->getClientMimeType();
                    $uploadImage->filename = $image->getFilename().'.'.$extension;
                    $uploadImage->listing_id = $listing_id;
                    $uploadImage->save();
                }
            }
            
            $brands = CarModel::select('id','automobile_brand_name','status')->get();
            
          return redirect('admin/automobile/displayautomobilebrands')->with('success_msg','Car brand created Successfully..!!');
        }
    }


    // this function edits the car brands in the system
    public function editbrands(Request $request){

        $brandname   = $request->input('brandname');
        $id 	     = $request->input('car_id');
        $brandstatus = $request->input('brandstatus');
        $showunderbrand = $request->input('show_under_brand');
        
        $data        = $request->validate([ 'brandname'   => ['required','unique:au_brands,automobile_brand_name,' . $id], ]);
        
        $data 		          = CarModel::find($id);
        $data->automobile_brand_name = $brandname;
        $data->status         = $brandstatus;
        $data->show_under_brand = $showunderbrand;
        $data->save();

        $brands =CarModel::select('id','automobile_brand_name','status')->get();
        
        if($request->file('photos') ?? ''){
            foreach ($request->file('photos') as $image){
                $uploadImage = new CarModelImage();
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                
                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename().'.'.$extension;
                $uploadImage->listing_id = $id;
                $uploadImage->save();
            }
        }
         
       return redirect('admin/automobile/displayautomobilebrands')->with('success_msg','Car brand updated Successfully..!!');

    }


    // this function deletes the car brands from the system

    public function deletebrands(Request $request){
        $id = $request->input('id');
        $deletewatch = CarModel::find($id);
        $deletewatch->delete(); 
        return response()->json([ 'success_msg'=>'Car brand deleted Successfully..!!']); 
    }

    
    public function deletebrandsimages($id){
        $deletewatch = CarModelImage::find($id);
        $deletewatch->delete();
        return response()->json([ 'success_msg'=>'Car brand Image deleted Successfully..!!']);
    }
 //----------------------------------- CARS BRAND ENDS HERE ---------------------------------------------------//

 //----------------------------------- BRAND MODEL STARTS HERE -----------------------------------------------//

    // this function displayes the list of models created for the watch brands
    public function displaybrandsmodel(){
        
        $brands = CarBrandModel::select('au_model.id','automobile_model_name','au_model.status','automobile_brand_name')
        ->join('au_brands','au_brands.id','=','au_model.automobile_brand_id')
        ->get();
       
       return view('admin.automobile.displaybrandsmodel')->with(array('brands'=>$brands));
    }

    // this function takes us to the page from where models of the brands can be created
    public function createbrandsmodel(){
        $brands = CarModel::select('id','automobile_brand_name','status')->where('status','=','1')
        ->get();
        return view('admin.automobile.createbrandsmodel')->with(array('brands'=>$brands));

    }

    // this function saves the car's brands model in the system
    public function savebrandsmodel(Request $request){
        $brandid = $request->brandid;
        $brandmodelname = $request->brandmodelname;
        $status = $request->status;

         $data = $request->validate([
                'brandid'   => ['required'],
                'brandmodelname' => ['required',
                    Rule::unique('au_model','automobile_model_name')->where(function ($query) use($brandid) {
                        return $query->where('automobile_brand_id', $brandid)
                        /* ->where('state_id',$stateid) */;
                    })
                ],
                'status' => ['required']
        ]);

        if($brandid == '' || $brandmodelname == ''){
            return back()->withErrors(['error' => ['Any of the field cannot be left blanck']]);
       }else{
            $data = new CarBrandModel;
            $data->automobile_brand_id =  $brandid;
            $data->automobile_model_name = $brandmodelname;
            $data->status = $status;
            $data->save();
            return redirect('admin/automobile/displaybrandsmodel')->with('success_msg','Car brand Model created Successfully..!!');
       }
    }

    // this function is created for displaying the update page of brandsmodel

    public function updatebrandsmodel($id){
        $brandsmodel = CarBrandModel::select('au_model.id','automobile_model_name','au_model.status','automobile_brand_name')
        ->join('au_brands','au_brands.id','=','au_model.automobile_brand_id')
        ->where('au_model.id','=',$id)
        ->get();
        

        return view('admin.automobile.updatebrandsmodel')->with(array('brandsmodel'=>$brandsmodel));
    }

    // this function is to edit the model of the brands

    public function editbrandsmodel(Request $request){
        
        $brandmodelname = $request->brandmodelname;
        $modelid        = $request->modelid;
        $brandstatus    = $request->input('brandstatus');
        $data           = $request->validate([ 'brandmodelname' => ['required','unique:au_model,automobile_model_name,' . $modelid],]);
        
        if($brandmodelname == '' || $modelid == ''){
            return back()->withErrors(['error' => ['Any of the field cannot be left blanck']]);
        }else{
            $data = CarBrandModel::find($modelid);
            $data->automobile_model_name = $brandmodelname;
            $data->status = $brandstatus;

            $data->save();
          
            return redirect('admin/automobile/displaybrandsmodel')->with('success_msg','Car brand model updated Successfully..!!');
       }
    }

    // this function is to delete brands models
    public function deletebrandsmodel(Request $request){
        $id         = $request->input('id');
        $delete_car = CarBrandModel::find($id);
        $delete_car->delete(); 
        return response()->json([ 'success_msg'=>'Car brand deleted Successfully..!!' ]); 
    }

 //----------------------------------- BRAND MODEL ENDS HERE -------------------------------------------------//   
    
}
