<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use App\CountryModel;
use App\StateModel;
use App\CityModel;
use Session;

class CommonadminController extends Controller
{
    // this function redirects to dashboard page from where all common entries can be entered
    public function home() {
        return view('admin.admincommon.dashboard');
    }

//------------------------------------COUNTRIES-----------------------------------------------------//
    // this function will display all countries listed in the system
    public function displaycountries(){
    	$countries = CountryModel::select('id','country_name','status', 'filename', 'countrycode', 'phonecode')
        ->get();
    	return view('admin.admincommon.displaycountries')->with(array('countries'=> $countries));
	}

	// this function will display the form from wher countries can be created

	public function createcountries(){

        return view('admin.admincommon.createcountries');
    }

    //this function is to save country

    public function savecountries(Request $request){
    	$countryname = $request->input('countryname');

        $data = $request->validate([
            'status' => ['required'],
            'countryname'   => ['required','unique:tbl_country,country_name'],
            'countrycode'   => ['required','unique:tbl_country,countrycode'],
            'phonecode'   => ['required','unique:tbl_country,phonecode']
        ],
            ['countryname.required' =>  'Country Cannot be Empty',
                'countrycode.required' =>  'Country Code Cannot be Empty'
            ]);
        
        if($countryname == ''){
            return back()->withErrors(['error' => ['Any of the field cannot be left blank']]);
        } else {  

            $data = new CountryModel;
            $data->country_name =  $countryname;
            $data->countrycode =  $request->input('countrycode');
            $data->phonecode =  $request->input('phonecode');
            $data->status =  $request->input('status');
            
            
            
            if($request->file('photos') ?? ''){
                foreach ($request->file('photos') as $image){
                    
                    $extension = $image->getClientOriginalExtension();
                    Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                    
                    $data->filename = $image->getFilename().'.'.$extension;
                }
            }
            
            $data->save();
            
          return redirect('admin/admincommon/displaycountries')->with('success_msg','Country  created Successfully..!!');
        }
    }
    // this  function is to display for for updateing countries
    public function updatecountries($id){
    	 $countries = CountryModel::select('id','country_name','status', 'filename', 'countrycode', 'phonecode')->where('id','=',$id)
        ->get();
        return view('admin.admincommon.updatecountries')->with(array('countries'=>$countries));
    }

    // this function is to edit the countries
    public function editcountries(Request $request){
    	$countryname = $request->input('countryname');
        $id=$request->input('countryid');
        $countrystatus=$request->input('countrystatus');
        $countrycode=$request->input('countrycode');
        $phonecode=$request->input('phonecode');
        $data = $request->validate([
                'countryname'   => ['required', 'unique:tbl_country,country_name,' . $id],
            'countrycode'   => ['required', 'unique:tbl_country,countrycode,' . $id],
            'phonecode'   => ['required', 'unique:tbl_country,phonecode,' . $id],
                'countryid'   => ['required'],
                'countrystatus'   => ['required'],
        ]);
        $data = CountryModel::find($id);
        $data->country_name =  $countryname;
        $data->countrycode =  $countrycode;
        $data->phonecode =  $phonecode;
        $data->status = $countrystatus;
        
        if($request->file('photos') ?? ''){
            foreach ($request->file('photos') as $image){
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                
                $data->filename = $image->getFilename().'.'.$extension;
            }
        }
        
        $data->save();
        
       return redirect('admin/admincommon/displaycountries')->with('success_msg','Country updated Successfully..!!');
    }

    // this function is to delete the countries
    public function deletecountries(Request $request){
    	$id=$request->input('id');
        $deletecountries = CountryModel::find($id);
        $deletecountries->delete(); 
        return response()->json([
                    'success_msg'=>'Country deleted Successfully..!!']); 
    }

//------------------------------------COUNTRIES-----------------------------------------------------//

//------------------------------------STATES--------------------------------------------------------//
    
    // this function is to display the listings of state
    public function displaystates(){
    	 $states = StateModel::select('tbl_state.id','state_name','tbl_state.status','country_name')
        ->join('tbl_country','tbl_country.id','=','tbl_state.country_id')
        ->get();
       
       return view('admin.admincommon.displaystates')->with(array('states'=>$states));
    }

    // this function displays the form page of creation of states
    public function createstates(){
        $countries = CountryModel::select('id','country_name','status')->orderBy('country_name', 'asc')->where('status','=','1')
        ->get();
        return view('admin.admincommon.createstates')->with(array('countries'=>$countries));
    }

    //this function is to save country
    public function savestates(Request $request){
        $countryid = $request->input('countryid');
        $statename = $request->input('statename');
        $data = $request->validate([
            'status' => ['required'],
            'countryid'   => ['required'],
            'statename'   => ['required','unique:tbl_state,state_name'],
        ],['statename.required' =>  'State Cannot be Empty'
        ]);
        if($countryid == '' || $statename == ''){
            return back()->withErrors(['error' => ['Any of the field cannot be left blanck']]);
        } else {  

            $data = new stateModel;
            $data->country_id = $countryid;
            $data->state_name =  $statename;
            $data->status = $request->input('status');
            $data->save();
            
          return redirect('admin/admincommon/displaystates')->with('success_msg','State created Successfully..!!');
        }
    }


    // this function is created for displaying the update page of state
    public function updatestates($id){
        $states = StateModel::select('tbl_state.id','state_name','tbl_state.status','country_name')
        ->join('tbl_country','tbl_country.id','=','tbl_state.country_id')
        ->where('tbl_state.id','=',$id)
        ->get();
        

        return view('admin.admincommon.updatestates')->with(array('states'=>$states));
    }


   // this function is to edit the state of the country
    public function editstates(Request $request){
        
        $statename   = $request->statename;
        $countryid   = $request->countryid;
        $statestatus = $request->input('statestatus');
        $data        = $request->validate([ 'statename' => ['required']]);
        $data        = $request->validate([ 'statename' => ['required'],['statename.required' =>  'State Cannot be Empty'
        ]]);
        if($statename == '' || $countryid == ''){
            return back()->withErrors(['error' => ['Any of the field cannot be left blank']]);
        }else{
            $data = StateModel::find($countryid);
            $data->state_name = $statename;
            $data->status = $statestatus;

            $data->save();
          
            return redirect('admin/admincommon/displaystates')->with('success_msg','State updated Successfully..!!');
       }
    }

    // this function is to delete the state
    public function deletestates(Request $request){
        $id=$request->input('id');
        $deletestate = StateModel::find($id);
        $deletestate->delete(); 
        return response()->json([ 'success_msg'=>'State deleted Successfully..!!']); 
    } 

//------------------------------------STATES-----------------------------------------------------//

//------------------------------------CITY--------------------------------------------------------//
    
    // this function is to display the listings of city
    public function displaycity(){
         $cities = CityModel::select('tbl_city.id','city_name','tbl_city.status','state_name', 'country_name')
         ->leftjoin('tbl_state', function($join){
             $join->on('tbl_state.id','=','tbl_city.state_id'); 
             $join->orOn('tbl_state.id','=','tbl_city.state_id', '=', 0);
         })
        ->join('tbl_country', 'tbl_country.id', '=', 'tbl_city.country_id')
        ->get();
        
       return view('admin.admincommon.displaycity')->with(array('cities'=>$cities));
    }
    //this function displays the states based on the selected country
    public function getStateByCountry(Request $request){
    
        $countryId = $request->input('countryId');
        $state = StateModel::select('id', 'state_name')->where('country_id', $countryId)
        ->get(); 
        return $state;
    }
    
    //this function is used to get the cities based on selected state
    public function getCityByState(Request $request){
        
        $stateId = $request->input('stateId');
        $countryId = $request->input('countryId');
        
        if($stateId == '')
            $city = CityModel::select('id', 'city_name')->where('country_id', $countryId)->get();
        else            
        $city = CityModel::select('id', 'city_name')->where('state_id', $stateId)->get();
        
        
        return $city;
    }
    
    // this function displays the form page of creation of city
    public function createcity(){
        $countries = CountryModel::select('id', 'country_name', 'status')->where('status','=','1')
        ->get();
        
        return view('admin.admincommon.createcity')->with(array('countries' =>$countries));
    }

    //this function is to save city
    public function savecity(Request $request){
        $countryid = $request->input('countryid');
        $stateid = $request->input('stateid');
        $cityname = $request->input('cityname');
        $data = $request->validate([
            'status' => ['required'],
            'countryid' => ['required'],
           // 'stateid'   => ['required'],
            'cityname'   => ['required',
                Rule::unique('tbl_city','city_name')->where(function ($query) use($countryid,$stateid) {
                    return $query->where('country_id', $countryid)
                    /* ->where('state_id',$stateid) */;
                })
            ],
            ],['cityname.required' =>  'City Cannot be Empty'
            ]);
        if($countryid =='' || $cityname == ''){
            return back()->withErrors(['error' => ['Any of the field cannot be left blank']]);
        } else {  

            $data = new CityModel;
            $data->country_id = $countryid;
            if($stateid != '')
            $data->state_id = $stateid;
            $data->status = $request->input('status');
            
            $data->city_name =  $cityname;
            $data->save();
            
          return redirect('admin/admincommon/displaycity')->with('success_msg','City created Successfully..!!');
        }
    }


    // this function is created for displaying the update page of city
    public function updatecity($id){
        $cities = CityModel::select('tbl_city.id','city_name','tbl_city.status','state_name','country_name')
        ->leftjoin('tbl_state', function($join){
            $join->on('tbl_state.id','=','tbl_city.state_id');
            $join->orOn('tbl_state.id','=','tbl_city.state_id', '=', 0);
        })
        ->join('tbl_country', 'tbl_country.id', '=', 'tbl_city.country_id')
        ->where('tbl_city.id','=',$id)
        ->get();
        

        return view('admin.admincommon.updatecity')->with(array('cities'=>$cities));
    }


   // this function is to edit the city of the state
    public function editcity(Request $request){
        
        $cityname   = $request->cityname;
        $stateid   = $request->stateid;
        $citystatus = $request->input('citystatus');
        $data        = $request->validate([ 'cityname' => ['required','unique:tbl_city,city_name']]);
        
        if($cityname == '' || $stateid == ''){
            return back()->withErrors(['error' => ['Any of the field cannot be left blank']]);
        }else{
            $data = CityModel::find($stateid);
            $data->city_name = $cityname;
            $data->status = $citystatus;

            $data->save();
          
            return redirect('admin/admincommon/displaycity')->with('success_msg','City updated Successfully..!!');
       }
    }

    // this function is to delete the city
    public function deletecity(Request $request){
        $id=$request->input('id');
        $deletecity = CityModel::find($id);
        $deletecity->delete(); 
        return response()->json([ 'success_msg'=>'City deleted Successfully..!!']); 
    } 

//------------------------------------ CITY -----------------------------------------------------//     
}
