<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GenderModel;

class GenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gender = GenderModel::get();
        return view('admin.watch.gender.index',compact('gender'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.gender.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validate = $this->validate($request, [
            'gender' => 'required|unique:wa_gender',
            'status' => 'required'
        ]);

        $gender = GenderModel::create($validate);
        $gender->save();
        return redirect('/admin/gender')->with('success_msg','Gender Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gender_data = GenderModel::find($id);
        return view('admin.watch.gender.edit', compact('gender_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $validate = $this->validate($request, [
            'gender' => 'required|unique:wa_gender'
        ]);

        $genderUpdate = GenderModel::find($id);

        if ($genderUpdate->gender == $validate['gender']) {
            $genderUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['gender' => 'unique:wa_gender']);
            $genderUpdate->gender = $validate['gender'];
            $genderUpdate->status = $request->input('status');
        }

        $genderUpdate->save();

        return redirect('/admin/gender')->with('success_msg','Gender Updated successfully!');          
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = GenderModel::find($id)->delete();
        return redirect('/admin/gender')->with('success_msg','Gender deleted successfully!');
    }
}
