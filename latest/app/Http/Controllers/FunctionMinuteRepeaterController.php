<?php

namespace App\Http\Controllers;

use App\FunctionMinuteRepeaterModel;
use Illuminate\Http\Request;

class FunctionMinuteRepeaterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $functions = FunctionMinuteRepeaterModel::get();
        return view('admin.watch.functionMinuteRepeater.index',compact('functions'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.functionMinuteRepeater.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = $this->validate($request, [
            "minute_repeater" => 'required|unique:wa_factory_features_function_minuteRepeater,minute_repeater',
            "status" => 'required'
        ]);
        
        $case = new FunctionMinuteRepeaterModel;
        $case->minute_repeater = $request->input('minute_repeater');
        $case->status = $request->input('status');
        $case->save();
        
        return redirect('/admin/functionMinuteRepeater')->with('success_msg','Minute Repeater Created successfully!');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $functions_data = FunctionMinuteRepeaterModel::find($id);
        return view('admin.watch.functionMinuteRepeater.edit', compact('functions_data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            "minute_repeater" => 'required|unique:wa_factory_features_function_minuteRepeater,minute_repeater,' . $id,
            "status" => 'required'
            
        ]);
        $case = FunctionMinuteRepeaterModel::find($id);
        $case->minute_repeater = $request->input('minute_repeater');
        $case->status = $request->input('status');
        $case->save();
        
        return redirect('/admin/functionMinuteRepeater')->with('success_msg','Minute Repeater Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = FunctionMinuteRepeaterModel::find($id)->delete();
        return redirect('/admin/functionMinuteRepeater')->with('success_msg','Minute Repeater Deleted successfully!');
    }
}
