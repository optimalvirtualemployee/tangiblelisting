<?php

namespace App\Http\Controllers;

use App\AutomobileBlogModel;
use App\ContactusModel;
use App\CurrencyModel;
use App\PropertyBlogModel;
use App\PropertyTypeModel;
use Illuminate\Http\Request;
use App\WatchBlogModel;
use App\WatchBlogImageModel;

class SingleBlogFrontEndController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contactUs = ContactusModel::get()->first();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        $propertyType = PropertyTypeModel::get();
        
        $blog = WatchBlogModel::find($id);
        
        $blog_images = WatchBlogImageModel::get();
        
        
        $type_blog = WatchBlogModel::select('wa_blog.id as id','filename','title', 'blogPost', 'wa_blog.created_at as created_at', 'blogType','url')
        ->join('wa_blog_images', 'wa_blog_images.listing_id', '=', 'wa_blog.id')
        ->where('status', '=', '1')->where('wa_blog.id', '!=', $id)->orderBy('wa_blog.created_at','desc')->limit('4')->get();
        
        //dd($type_blog);
        
        return view('tangiblehtml.watch-blog', compact('contactUs', 'propertyType', 'blog', 'blog_images', 'type_blog'));
    }
}
