<?php

namespace App\Http\Controllers;

use App\PropertyFeaturesModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\PropertyFeaturesListingModel;

class PropertyFeaturesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createFeature(Request $request, $listing_id)
    {
        $feature_listing = PropertyFeaturesListingModel::get();
        
        
        $html ='';
        foreach ($feature_listing as $values){
            $id = $values->id;
            $propertyName = $values->property_name;
            
            $html .= '<div class="form-group">
                    <tr>
                   <td><label style= margin-right:10px!important; ><Strong>'.$propertyName.':</Strong></label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$propertyName.'" name="'.$id.'" value="1"><label style= margin-right:5px!important; for="yes"> Yes </label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$propertyName.'" name="'.$id.'" value="0" checked><label style= margin-right:5px!important; for="no"> No</label></td>
                    </tr>
                      </div>';
            
        }
        
        return view('admin.realestate.features.create', compact('listing_id', 'html'));
    }
    public function create()
    {
        //
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            
            
            'listing_id' => ['required']
        ]);
        
        
        $param = $request->request;
        $listing_id = $request->input('listing_id');
        
        foreach ($param as $key => $value){
            if($key != '_token' && $key != 'listing_id'){
                $createFeature = new PropertyFeaturesModel();
                $createFeature->listing_id = $listing_id;
                $createFeature->feature_id = $key;
                $createFeature->feature_available = $value;
                $createFeature->save();
            }
            
        }
        
        return redirect('/admin/createpropertyfeature/'.$listing_id)->with('success_msg', 'Property Features Added successfully!');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $listing_id = $id;
        $details = PropertyFeaturesModel::select('re_property_features.id as id', 'property_name', 'feature_available')
        ->join('re_properties_features_listing', 're_properties_features_listing.id', '=', 're_property_features.feature_id')
        ->where('listing_id', '=', $listing_id)
        ->where('feature_available', '=', '1')
        ->get();
        
        return view('admin.realestate.features.index', compact('details','listing_id'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feature_data =PropertyFeaturesModel::select('re_property_features.id as id', 'listing_id' ,'property_name', 'feature_available', 'feature_id')
        ->join('re_properties_features_listing', 're_properties_features_listing.id', '=', 're_property_features.feature_id')
        ->where('listing_id', '=', $id)
        ->get();
        
        $html = '';
        foreach ($feature_data as $values){
        
            $yesChecked = '';
            $noChecked = '';
            if($values->feature_available == '1'){
                $yesChecked = 'checked';
            }else{
                $noChecked = 'checked';
            }
            
      $html .= '<div class="form-group">
                    <tr>
                    <td><label style= margin-right:10px!important; ><Strong>'.$values->property_name.':</Strong></label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$values->propertyName.'" name="'.$values->feature_id.'" value="1"'.$yesChecked.'><label style= margin-right:5px!important; for="yes"> Yes </label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$values->propertyName.'" name="'.$values->feature_id.'" value="0" '.$noChecked.'><label style= margin-right:5px!important; for="no"> No</label></td>
                    </tr>  
                    </div>';
        }
        
        return view('admin.realestate.features.edit',
            compact('html', 'id'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $data = $request->validate([
            
            
            'listing_id' => ['required']
        ]);
        
        $deleteRecords = PropertyFeaturesModel::where('listing_id', '=', $id)->delete();
        
        foreach ($request->request as $key => $value){
            if($key != '_token' && $key != 'listing_id' && $key !='_method'){
                $createFeature = new PropertyFeaturesModel();

                $createFeature->listing_id = $id;
                $createFeature->feature_id = $key;
                $createFeature->feature_available = $value;
                $createFeature->save();
            }
            
        }
        
        return redirect('/admin/createpropertyfeature/'.$request->input('listing_id'))->with('success_msg', 'Feature Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = PropertyFeaturesModel::find($id);
        $listing_id = $comment->listing_id;
        
        $deleteRecords = PropertyFeaturesModel::find($id)->delete();
        return redirect('/admin/createpropertyfeature/'.$listing_id)->with('success_msg', 'Property Deleted successfully!');
    }
}
