<?php

namespace App\Http\Controllers;

use App\PropertyNewsModel;
use Illuminate\Http\Request;

class PropertyNewsController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = PropertyNewsModel::get();

        return view('admin.realestate.news.index')->with(array(
            'news' => $news
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.realestate.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = $request->validate([
            
            
            'title' => ['required'],
            'content' => ['required']
            
        ]);
        
        
        $createPropertynews = new PropertyNewsModel();

        $createPropertynews->title = $request->input('title');
        $createPropertynews->content = $request->input('content');

        $createPropertynews->save();

        return redirect('/admin/propertynews')->with('success_msg', 'Property news Created successfully!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $news = PropertyNewsModel::find($id);
        
        
        return view('admin.realestate.news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            
            
            'title' => ['required'],
            'content' => ['required']
        ]);
        
        $updatePropertynews = PropertyNewsModel::find($id);

        $updatePropertynews->title = $request->input('title');
        $updatePropertynews->content = $request->input('content');

        $updatePropertynews->save();

        return redirect('/admin/propertynews')->with('success_msg', 'Property news Updated successfully!');
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = PropertyNewsModel::find($id)->delete();
        return redirect('/admin/propertynews')->with('success_msg', 'Property news Deleted successfully!');
        
    }
}
