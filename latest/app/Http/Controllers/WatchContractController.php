<?php

namespace App\Http\Controllers\admin;

use App\WatchBlogImageModel;
use App\WatchBlogModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class WatchContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createContract()
    { dd("d");
        $blogs = WatchBlogModel::get();
        
        return view('admin.watch.blog_msf.index')->with(array(
            'blogs' => $blogs
        ));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.watch.blog_msf.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = $request->validate([
            
            
            'title' => ['required','unique:wa_blog,title'],
            'blogPost' => ['required'],
            'blogType' => ['required'],
            'status' => ['required'],
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        
        
        $createWatchBlog = new WatchBlogModel();
        
        $createWatchBlog->title = $request->input('title');
        $createWatchBlog->blogPost = $request->input('blogPost');
        $createWatchBlog->blogType = $request->input('blogType');
        $createWatchBlog->status = $request->input('status');
        $createWatchBlog->url = $request->input('url');
        
        $createWatchBlog->save();
        
        $listing_id = $createWatchBlog->id;
        
        if($request->file('photos') ?? ''){
            foreach ($request->file('photos') as $image){
                $uploadImage = new WatchBlogImageModel();
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                
                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename().'.'.$extension;
                $uploadImage->listing_id = $listing_id;
                $uploadImage->save();
            }
        }
        
        return redirect('/admin/watchblog')->with('success_msg', 'Watch Blog Created successfully!');
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $blog = WatchBlogModel::find($id);
        
        $images_data = WatchBlogImageModel::where('listing_id', '=', $id)->get();
        
        return view('admin.watch.blog_msf.edit', compact('blog', 'images_data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $data = $request->validate([
            
            
            'title' => ['required','unique:wa_blog,title,' . $id],
            'blogPost' => ['required'],
            'blogType' => ['required'],
            'status' => ['required'],
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        
        $updateWatchBlog = WatchBlogModel::find($id);
        
        $updateWatchBlog->title = $request->input('title');
        $updateWatchBlog->blogPost = $request->input('blogPost');
        $updateWatchBlog->blogType = $request->input('blogType');
        $updateWatchBlog->status = $request->input('status');
        $updateWatchBlog->url = $request->input('url');
        
        $updateWatchBlog->save();
        
        if($request->file('photos') ?? ''){
            foreach ($request->file('photos') as $image){
                $uploadImage = new WatchBlogImageModel();
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                
                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename().'.'.$extension;
                $uploadImage->listing_id = $id;
                $uploadImage->save();
            }
        }
        
        return redirect('/admin/watchblog')->with('success_msg', 'Watch Blog Updated successfully!');
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = WatchBlogModel::find($id)->delete();
        return redirect('/admin/watchblog')->with('success_msg', 'Watch Blog Deleted successfully!');
        
    }
}
