<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WatchDataListingModel;

class WatchFeaturedController extends Controller
{
    public function showFeaturedProperties(Request $request){
        
        $watches = WatchDataListingModel::where('featured', '=','1')->get();
        
        return view('admin.watch.featured.featured')->with(array(
            'watches' => $watches
        ));
        
    }
    
    public function editFeatured($id){
        
        $featured = WatchDataListingModel::find($id);
        
        return view('admin.watch.featured.editfeatured', compact('featured'));
    }
    
    public function updateFeatured(Request $request, $id){
        
        $data = $request->validate([
            'featured' => ['required']
        ]);
        
        $updateFeatured = WatchDataListingModel::find($id);
        
        $updateFeatured->featured = $request->input('featured');
        
        $updateFeatured->save();
        
        return redirect('/admin/featuredwatch')->with('success_msg', 'Watch Feature Updated successfully!');
    }
}
