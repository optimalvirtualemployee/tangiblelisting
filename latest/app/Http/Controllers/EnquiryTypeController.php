<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EnquiryAboutModel;

class EnquiryTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $enquiry_type = EnquiryAboutModel::get();
        return view('admin.realestate.enquiry_type.index',compact('enquiry_type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.realestate.enquiry_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'enquiry_type' => 'required|regex:/^[\pL\s_]+$/u|unique:re_enquiry_about',
            'status' => 'required'
        ]);

        $enquiry_type = EnquiryAboutModel::create($validate);
        $enquiry_type->save();
        return redirect('/admin/enquiry_type')->with('success_msg','Enquiry Type Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $enquiry_type_data = EnquiryAboutModel::find($id);
        return view('admin.realestate.enquiry_type.edit', compact('enquiry_type_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'enquiry_type' => 'required|regex:/^[\pL\s_]+$/u'
        ]);

        $enquiry_typeUpdate = EnquiryAboutModel::find($id);

        if ($enquiry_typeUpdate->enquiry_type == $validate['enquiry_type']) {
            $enquiry_typeUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['enquiry_type' => 'unique:re_enquiry_about']);
            $enquiry_typeUpdate->enquiry_type = $validate['enquiry_type'];
            $enquiry_typeUpdate->status = $request->input('status');
        }

        $enquiry_typeUpdate->save();

        return redirect('/admin/enquiry_type')->with('success_msg','Enquiry Type Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = EnquiryAboutModel::find($id)->delete();
        return redirect('/admin/enquiry_type')->with('success_msg','Enquiry Type deleted successfully!');
    }
}
