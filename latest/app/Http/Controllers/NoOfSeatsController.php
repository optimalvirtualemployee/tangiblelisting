<?php

namespace App\Http\Controllers;

use App\NoOfSeatsModel;
use Illuminate\Http\Request;

class NoOfSeatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $no_of_seats = NoOfSeatsModel::get();
        return view('admin.automobile.no_of_seats.index',compact('no_of_seats'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.automobile.no_of_seats.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'no_of_seats' => 'required|numeric|unique:au_no_of_seats',
            'status' => 'required'
        ]);
        
        $no_of_seats = NoOfSeatsModel::create($validate);
        $no_of_seats->save();
        return redirect('/admin/no_of_seats')->with('success_msg','No. of Seats Created successfully!');
    }
    
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $no_of_seats_data = NoOfSeatsModel::find($id);
        return view('admin.automobile.no_of_seats.edit', compact('no_of_seats_data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'no_of_seats' => 'required'
        ]);
        
        $no_of_seatsUpdate = NoOfSeatsModel::find($id);
        
        
        $no_of_seatsUpdate->no_of_seats = $validate['no_of_seats'];
        $no_of_seatsUpdate->status = $request->input('status');
        
        $no_of_seatsUpdate->save();
        
        return redirect('/admin/no_of_seats')->with('success_msg','No. of Seats Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = NoOfSeatsModel::find($id)->delete();
        return redirect('/admin/no_of_seats')->with('success_msg','No. of Seats deleted successfully!');
    }
}
