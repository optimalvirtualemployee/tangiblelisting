<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DialPowerReserveModel;

class DialPowerReserveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    //get dial list
    public function index()
    {
        $dial = DialPowerReserveModel::get();
        return view('admin.watch.dialPowerReserve.index',compact('dial'));
    }
    public function show(){
        
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.dialPowerReserve.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = $this->validate($request, [
            "power_reserve" => 'required|unique:wa_factory_features_dial_powerreserve',
            'status' => 'required'
            
        ]);

          $dial = DialPowerReserveModel::create($validator);
          $dial->save();
       
      return redirect('/admin/dialPowerReserve')->with('success_msg','Dial Power Reserve Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dial_data = DialPowerReserveModel::find($id);
        return view('admin.watch.dialPowerReserve.edit', compact('dial_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $validator = $this->validate($request, [
            "power_reserve" => 'required',
            'status' => 'required'
            
        ]);
                
              $dial = DialPowerReserveModel::find($id);
              $dial->power_reserve = $request->input('power_reserve');
              $dial->status = $request->input('status');
              $dial->save();
         
      return redirect('/admin/dialPowerReserve')->with('success_msg','Dial Power Reserve Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = DialPowerReserveModel::find($id)->delete();
        return redirect('/admin/dialPowerReserve')->with('success_msg','Dial Power Reserve deleted successfully!');
    }
}
