<?php

namespace App\Http\Controllers;

use App\CityModel;
use App\ContactusModel;
use App\CurrencyModel;
use App\PropertyImageModel;
use App\UserDetailsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use App\CountryModel;
use App\UserDetailsImageModel;
use App\User;

class UserProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
         if (Auth::user() == null){
            return redirect('/');
        }
        
        $id = Auth::user()->id;

        $contactUs = ContactusModel::get()->first();
        $images = PropertyImageModel::get();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        
        $userDetails = UserDetailsModel::where('user_id', '=',$id)->first();
        $country_data = CountryModel::get();
        $city_data = CityModel::get();
        
        
        /* Mail::send(['text'=>'mail'], $data, function($message) {
            $message->to('priyank@optimalvirtualemployee.com', 'Tutorials Point')->subject
            ('Laravel Basic Testing Mail');
            $message->from('xyz@mail.com','Virat Gandhi');
        }); */
        
        //dd($userDetails);
        
        return view('tangiblehtml.myprofile', compact('contactUs', 'userDetails', 'country_data', 'city_data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'postalCode' => 'required',
            'phoneNumber' => 'required',
            'city' => 'required',
            'country' => 'required',
            'address' => 'required',
            'date_of_birth' => 'required',
            'occupation' => 'required',
            'aboutMe' => 'required'
        ]);
        
        $input['first_name'] = $request->input('first_name');
        $input['last_name'] = $request->input('last_name');
        $input['email'] = $request->input('email');
        $input['phone'] = $request->input('phoneNumber');
        
        if(!empty($request->input('password'))){
            $input['password'] = Hash::make($request->input('password'));
        }
        
        $user = User::find($id);
        $user->update($input);
        
        $listing_id = "";
        $updateUser = UserDetailsModel::where('user_id', '=', $id)->first();
        
        if ($updateUser != null) {
            $updateUser->user_id = $id;
            $updateUser->user_postalCode = $request->postalCode;
            $updateUser->user_mobile = $request->phoneNumber;
            $updateUser->city_id = $request->city;
            $updateUser->country_id = $request->country;
            $updateUser->user_address = $request->address;
            $updateUser->dob = $request->date_of_birth;
            $updateUser->occupation = $request->occupation;
            $updateUser->about_me = $request->aboutMe;
            
            $updateUser->save();
            $listing_id = $updateUser->id;
        } else {
            
            $createUser = new UserDetailsModel();
            
            $createUser->user_id = $id;
            $createUser->user_postalCode = $request->postalCode;
            $createUser->user_mobile = $request->phoneNumber;
            $createUser->city_id = $request->city;
            $createUser->country_id = $request->country;
            $createUser->user_address = $request->address;
            $createUser->dob = $request->date_of_birth;
            $createUser->occupation = $request->occupation;
            $createUser->about_me = $request->aboutMe;
            
            $createUser->save();
            $listing_id = $createUser->id;
        }
        
        if ($request->file('photo') ?? '') {
            foreach ($request->file('photo') as $image) {
                
                UserDetailsImageModel::where('listing_id', '=', $listing_id)->delete();
                
                $uploadImage = new UserDetailsImageModel();
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));
                
                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename() . '.' . $extension;
                $uploadImage->listing_id = $listing_id;
                
                $uploadImage->save();
            }
        }
        
        return back()->with('success_msg','Profile Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
