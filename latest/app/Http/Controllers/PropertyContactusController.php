<?php

namespace App\Http\Controllers;

use App\PropertyBannerModel;
use App\PropertyContactusModel;
use Illuminate\Http\Request;

class PropertyContactusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contactuss = PropertyContactusModel::get();
        
        return view('admin.realestate.contactus.index')->with(array(
            'contactus' => $contactuss
        ));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.realestate.contactus.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = $request->validate([
            
            
            'address' => ['required'],
            'mobile' => ['required'],
            'whatsapp' => ['required'],
            'email' => ['required']
        ]);
        
        $createPropertycontactus = new PropertyContactusModel();
        
        $createPropertycontactus->address = $request->input('address');
        $createPropertycontactus->mobile = $request->input('mobile');
        $createPropertycontactus->whatsapp = $request->input('whatsapp');
        $createPropertycontactus->email = $request->input('email');
        
        $createPropertycontactus->save();
        
        return redirect('/admin/propertycontactus')->with('success_msg', 'Property contactus Created successfully!');
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $contactus = PropertyContactusModel::find($id);
        
        
        return view('admin.realestate.contactus.edit', compact('contactus'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            
            
            'address' => ['required'],
            'mobile' => ['required'],
            'whatsapp' => ['required'],
            'email' => ['required']
        ]);
        
        $updatePropertycontactus = PropertyContactusModel::find($id);
        
        $updatePropertycontactus->address = $request->input('address');
        $updatePropertycontactus->mobile = $request->input('mobile');
        $updatePropertycontactus->whatsapp = $request->input('whatsapp');
        $updatePropertycontactus->email = $request->input('email');
        
        $updatePropertycontactus->save();
        
        return redirect('/admin/propertycontactus')->with('success_msg', 'Property contactus Updated successfully!');
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = PropertyContactusModel::find($id)->delete();
        return redirect('/admin/propertycontactus')->with('success_msg', 'Property contactus Deleted successfully!');
        
    }
}
