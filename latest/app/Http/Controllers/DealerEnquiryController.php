<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Session;
use App\EnquiryModel;
use App\AgenciesModel;

class DealerEnquiryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function enquiryList(Request $request){
        $dealerId = Auth::user()->id;
        $agencyId = DB::table('tbl_agencies')->where('userId',$dealerId)->first();
        
        $dealer = AgenciesModel::select('tbl_category.website_category as category')
        ->where('userId', '=', Auth::user()->id)
        ->join('tbl_category', 'tbl_category.id', 'tbl_agencies.category_id')
        ->first();
        
        if (!empty($request->search)) {
            $search = $request->search;
            //watch category enquery data search
            $watchEnquary = DB::table('tbl_enquiry')->select('tbl_enquiry.id','tbl_enquiry.name','tbl_enquiry.message','tbl_enquiry.phone','tbl_enquiry.country','tbl_enquiry.productId','tbl_enquiry.agencies_id','wa_watch_detail.ad_title','wa_watch_detail.brand_name','wa_watch_detail.model_name','wa_watch_detail.watch_price','tbl_enquiry.created_at','tbl_agent.first_name','tbl_agent.last_name')
            ->join('wa_watch_detail','wa_watch_detail.id','=','tbl_enquiry.productId')
            ->join('tbl_agent','tbl_agent.id','=','wa_watch_detail.agent_id')
            ->where('tbl_enquiry.agencies_id','=',$agencyId->id)
           ->where('tbl_enquiry.category','watch')
           /* ->where('tbl_enquiry.name','LIKE','%'.$search.'%') */
           ->where(function ($query) use($search) {
               
               $query->where('tbl_enquiry.name','LIKE','%'.$search.'%')
               ->orWhere('wa_watch_detail.ad_title','LIKE','%'.$search.'%')
               ->orWhere('tbl_agent.first_name','LIKE','%'.$search.'%')
               ->orWhere('tbl_agent.last_name','LIKE','%'.$search.'%');
           })
           ->paginate(15);
           //automobile category enquery data search
           $carEnquary = DB::table('tbl_enquiry')->select('tbl_enquiry.id','tbl_enquiry.name','tbl_enquiry.message','tbl_enquiry.phone','tbl_enquiry.country','tbl_enquiry.productId','tbl_enquiry.agencies_id','au_automobile_detail.value as value' ,'au_automobile_detail.ad_title','tbl_enquiry.created_at','tbl_agent.first_name','tbl_agent.last_name')
            ->join('au_automobile_detail','au_automobile_detail.id','=','tbl_enquiry.productId')
            ->join('tbl_agent','tbl_agent.id','=','au_automobile_detail.agent_id')
            ->where('tbl_enquiry.agencies_id',$agencyId->id)
            ->where('tbl_enquiry.category','Automobile')
            /* ->where('tbl_enquiry.name','LIKE','%'.$search.'%') */
            ->where(function ($query) use($search) {
                
                $query->where('tbl_enquiry.name','LIKE','%'.$search.'%')
                ->orWhere('au_automobile_detail.ad_title','LIKE','%'.$search.'%')
                ->orWhere('tbl_agent.first_name','LIKE','%'.$search.'%')
                ->orWhere('tbl_agent.last_name','LIKE','%'.$search.'%');
            })
            ->paginate(15);

            //realEstate enquery data search
            $realEstate = DB::table('tbl_enquiry')->select('tbl_enquiry.id','tbl_enquiry.name','tbl_enquiry.message','tbl_enquiry.phone','tbl_enquiry.country','tbl_enquiry.productId','tbl_enquiry.agencies_id','re_property_details.property_price as value','re_property_details.ad_title','tbl_enquiry.created_at','tbl_agent.first_name','tbl_agent.last_name')
            ->join('re_property_details','re_property_details.id','=','tbl_enquiry.productId')
            ->join('tbl_agent','tbl_agent.id','=','re_property_details.agent_id')
             ->where('tbl_enquiry.agencies_id',$agencyId->id)
            ->where('tbl_enquiry.category','RealEstate')
            /* ->where('tbl_enquiry.name','LIKE','%'.$search.'%') */
            ->where(function ($query) use($search) {
                
                $query->where('tbl_enquiry.name','LIKE','%'.$search.'%')
                ->orWhere('re_property_details.ad_title','LIKE','%'.$search.'%')
                ->orWhere('tbl_agent.first_name','LIKE','%'.$search.'%')
                ->orWhere('tbl_agent.last_name','LIKE','%'.$search.'%');
            })
           ->paginate(15);
        }
        else{
            //watch category enquery data
            $watchEnquary = DB::table('tbl_enquiry')->select('tbl_enquiry.id','tbl_enquiry.name','tbl_enquiry.message','tbl_enquiry.phone','tbl_enquiry.country','tbl_enquiry.productId','tbl_enquiry.agencies_id', 'wa_watch_detail.ad_title','wa_watch_detail.brand_name','wa_watch_detail.model_name','wa_watch_detail.watch_price','tbl_enquiry.created_at','tbl_agent.first_name','tbl_agent.last_name')
            ->join('wa_watch_detail','wa_watch_detail.id','=','tbl_enquiry.productId')
            ->join('tbl_agent','tbl_agent.id','=','wa_watch_detail.agent_id')
            ->where('tbl_enquiry.agencies_id',$agencyId->id)
            ->where('tbl_enquiry.category','watch')->paginate(15);
            //automobile category enquery data
            $carEnquary = DB::table('tbl_enquiry')->select('tbl_enquiry.id','tbl_enquiry.name','tbl_enquiry.message','tbl_enquiry.phone','tbl_enquiry.country','tbl_enquiry.productId','tbl_enquiry.agencies_id','au_automobile_detail.value as value' ,'au_automobile_detail.ad_title','tbl_enquiry.created_at','tbl_agent.first_name','tbl_agent.last_name')
            ->join('au_automobile_detail','au_automobile_detail.id','=','tbl_enquiry.productId')
            ->join('tbl_agent','tbl_agent.id','=','au_automobile_detail.agent_id')
            ->where('tbl_enquiry.agencies_id',$agencyId->id)
            ->where('tbl_enquiry.category','Automobile')->paginate(15);

            //realEstate enquery data
            $realEstate = DB::table('tbl_enquiry')->select('tbl_enquiry.id','tbl_enquiry.name','tbl_enquiry.message','tbl_enquiry.phone','tbl_enquiry.country','tbl_enquiry.productId','tbl_enquiry.agencies_id','re_property_details.property_price as value','re_property_details.ad_title','tbl_enquiry.created_at','tbl_agent.first_name','tbl_agent.last_name')
            ->join('re_property_details','re_property_details.id','=','tbl_enquiry.productId')
            ->join('tbl_agent','tbl_agent.id','=','re_property_details.agent_id')
            ->where('tbl_enquiry.agencies_id',$agencyId->id)
            ->where('tbl_enquiry.category','RealEstate')->paginate(15);
        }

        if(!empty($watchEnquary)){
            $pages = $watchEnquary;
        }
        else if (!empty($carEnquary)){
            $pages = $carEnquary;
        }
        else{
            $pages = $realEstate;
        }
        
    	 return view('dealer.enquiry',compact('watchEnquary','carEnquary','realEstate','pages', 'dealer'));  
    }




}

