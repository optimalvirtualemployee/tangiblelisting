<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CaseModel;

class CaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $case = CaseModel::get();
        return view('admin.watch.case.index',compact('case'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.case.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validate($request, [
            "case_matrial" => 'required|unique:wa_factorty_features_case',
            "case_mm"  => "required|regex:/^\d+(\.\d{1,2})?$/",
            'water_resistant_depth' => "required",
            'glass_type' => "required"
        ]);

          $case = CaseModel::create($validator);
          $case->save();
       
      return redirect('/admin/case')->with('success_msg','Case Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $case_data = CaseModel::find($id);
        return view('admin.watch.case.edit', compact('case_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            "case_matrial" => 'required',
            "case_mm"  => "required|regex:/^\d+(\.\d{1,2})?$/",
            'water_resistant_depth' => "required",
            'glass_type' => "required"
        ]);

        $case = CaseModel::find($id);

        if ($case->case_matrial == $validator['case_matrial']) {
            $case->case_mm = $validator['case_mm'];
            $case->water_resistant_depth = $validator['water_resistant_depth'];
            $case->glass_type = $validator['glass_type'];
        } else {
            $validate = $this->validate($request, ['case_matrial' => 'unique:wa_factorty_features_case']);
            $case->case_matrial = $validator['case_matrial'];
            $case->case_mm = $validator['case_mm'];
            $case->water_resistant_depth = $validator['water_resistant_depth'];
            $case->glass_type = $validator['glass_type'];
        }

        $case->save();
        return redirect('/admin/case')->with('success_msg','Case Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = CaseModel::find($id)->delete();
        return redirect('/admin/case')->with('success_msg','Case Deleted successfully!');
    }
}
