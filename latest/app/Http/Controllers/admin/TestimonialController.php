<?php

namespace App\Http\Controllers\admin;

use App\Testimonial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //test
        $data = Testimonial::get();
        // dd($data);
        return view('admin.admincommon.testimonial.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admincommon.testimonial.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        try {        
        $validate = $this->validate($request, [
            'name'        => 'required',
            'designation' => 'required',
            'message'     => 'required',
            'image_status'=> 'required',
            'image'       => 'required',
            'status'      => 'required'
        ]);

        
        if ($request->image) {
            $image = $request->file('image');
            $name  = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/testimonial_images');
            $image->move($destinationPath, $name);
        }
        
        $conditions              = new Testimonial;
        $conditions->name        = $request->name;
        $conditions->designation = $request->designation;
        $conditions->message     = $request->message;
        $conditions->image_status= $request->image_status;
        $conditions->status      = $request->status;
        $conditions->image       = $name;

        $conditions->save();
        return redirect('/admin/testimonial')->with('success_msg','One new testimonial added successfully!');
    } catch (ModelNotFoundException $exception) {
        return back()->withError($exception->getMessage())->withInput();
    }
    return view('users.search', compact('user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //return view('users.search');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editData = Testimonial::where('id',$id)->first();
        return view('admin.admincommon.testimonial.edit',compact('editData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     $updateErr = Testimonial::where('id',$id)->first();

     try {        
        $validate = $this->validate($request, [
            'name'        => 'required',
            'designation' => 'required',
            'message'     => 'required',
            'image_status'=> 'required',
            'status'      => 'required'
        ]);

        $updateErr->image;
        if ($request->image) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/testimonial_images');
            $image->move($destinationPath, $name);
        }else{
            $name = $updateErr->image;
        }
        
        $updateErr->name = $request->name;
        $updateErr->designation = $request->designation;
        $updateErr->message = $request->message;
        $updateErr->image_status = $request->image_status;
        $updateErr->status = $request->status;
        $updateErr->image = @$name;
        $updateErr->save();
        return redirect('/admin/testimonial')->with('success_msg','Testimonial updated successfully!');
    } catch (ModelNotFoundException $exception) {
        return back()->withError($exception->getMessage())->withInput();
    }
    return view('users.search', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Testimonial::find($id);    
        $image_path = public_path()."/testimonial_images/".$post->image;

        if (File::exists($image_path)) {
            unlink($image_path);
        }
        
        $post->delete();
        return redirect()->back();
    }
}
