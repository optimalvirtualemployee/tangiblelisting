<?php

namespace App\Http\Controllers;

use App\AutomobileContactusModel;
use Illuminate\Http\Request;

class AutomobileContactusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contactuss = AutomobileContactusModel::get();
        
        return view('admin.automobile.contactus.index')->with(array(
            'contactus' => $contactuss
        ));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.automobile.contactus.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = $request->validate([
            
            
            'address' => ['required'],
            'mobile' => ['required'],
            'whatsapp' => ['required'],
            'email' => ['required']
        ]);
        
        $createautomobilecontactus = new AutomobileContactusModel();
        
        $createautomobilecontactus->address = $request->input('address');
        $createautomobilecontactus->mobile = $request->input('mobile');
        $createautomobilecontactus->whatsapp = $request->input('whatsapp');
        $createautomobilecontactus->email = $request->input('email');
        
        $createautomobilecontactus->save();
        
        return redirect('/admin/automobilecontactus')->with('success_msg', 'Automobile contactus Created successfully!');
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $contactus = AutomobileContactusModel::find($id);
        
        
        return view('admin.automobile.contactus.edit', compact('contactus'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            
            
            'address' => ['required'],
            'mobile' => ['required'],
            'whatsapp' => ['required'],
            'email' => ['required']
        ]);
        
        $updateautomobilecontactus = AutomobileContactusModel::find($id);
        
        $updateautomobilecontactus->address = $request->input('address');
        $updateautomobilecontactus->mobile = $request->input('mobile');
        $updateautomobilecontactus->whatsapp = $request->input('whatsapp');
        $updateautomobilecontactus->email = $request->input('email');
        
        $updateautomobilecontactus->save();
        
        return redirect('/admin/automobilecontactus')->with('success_msg', 'Automobile contactus Updated successfully!');
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = AutomobileContactusModel::find($id)->delete();
        return redirect('/admin/automobilecontactus')->with('success_msg', 'Automobile contactus Deleted successfully!');
        
    }
}
