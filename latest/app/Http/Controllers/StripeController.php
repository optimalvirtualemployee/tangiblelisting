<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Stripe;
use Session;
use App\ContactusModel;
use URL;

class StripeController extends Controller
{
    /**
     * payment view
     */
    public function handleGet()
    {   
        $contactUs = ContactusModel::get()->first();
        return view('home', compact('contactUs'));
       
    }
  
    /**
     * handling payment with POST
     */
    public function handlePost(Request $request)
    {
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        // $stripe = new \Stripe\StripeClient(
        // 'sk_test_51IbIAtSEBtRtnlVS8c1S888PJelnbBwGoJmiWYPxdfqb0NAqGyNERnUqn3FeufMpo7FwfsOVZ9ee7ik1FcXP6J9900A8JcvYcL'
        // );
        // $customer =  $stripe->customers->create([
        // 'description' => 'My First Test Customer (created for API docs)',
        // 'name' => "Rajat",
        // 'email' => "rajat@optimalvirtualemployee.com",
        // ]);


    //    $customer = Stripe\Customer::create([
    //         'name' => "Rajat",
    //         'email' => "rajat@optimalvirtualemployee.com",
    //         'source' => $request->stripeToken
    //     ]);

       $paymentData = session('paymentData');

        $charge = Stripe\Charge::create ([
                "amount" => 100 * $paymentData['amount'],
                "currency" => "inr",
                "source" => $request->stripeToken,
                "description" => "Making test payment.",
               // "customer" => $customer->id
        ]);

        $paymentDetails = $charge->jsonSerialize();

       // dd($paymentDetails);
  
        Session::flash('success', 'Payment has been successfully processed.');
          
        return back();
    }

     /**
     * handling payment with POST
     */
    public function reauth($acId)
    {
         // Generating Onboarding Process Account Link
         Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

         $baseUrl = URL::to('/');

         $account_links = \Stripe\AccountLink::create([
         'account' => $acId,
         'refresh_url' => $baseUrl.'/reauth',
         'return_url' => $baseUrl,
         'type' => 'account_onboarding',
         ]);

         $accountLinkDetails = $account_links->jsonSerialize();
         $onboardUrl = $accountLinkDetails['url'];

         return redirect($onboardUrl);
    }
}