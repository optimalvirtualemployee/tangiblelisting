<?php

namespace App\Http\Controllers;

use App\FunctionDayModel;
use Illuminate\Http\Request;

class FunctionDayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $functions = FunctionDayModel::get();
        return view('admin.watch.functionDay.index',compact('functions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.watch.functionDay.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
         $validator = $this->validate($request, [
            "day" => 'required|unique:wa_factory_features_function_day,day',
             "status" => 'required'
        ]);

          $case = FunctionDayModel::create($validator);
          $case->save();
       
      return redirect('/admin/functionDay')->with('success_msg','Day Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $functions_data = FunctionDayModel::find($id);
        return view('admin.watch.functionDay.edit', compact('functions_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            "day" => 'required|unique:wa_factory_features_function_day,day,'.$id,
            "status" => 'required'
        ]);

          $case = FunctionDayModel::find($id);
          $case->day = $request->input('day');
          $case->status = $request->input('status');
          $case->save();
        
        return redirect('/admin/functionDay')->with('success_msg','Day Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = FunctionDayModel::find($id)->delete();
        return redirect('/admin/functionDay')->with('success_msg','Day Deleted successfully!');
    }
}
