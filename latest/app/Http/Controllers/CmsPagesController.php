<?php

namespace App\Http\Controllers;

use App\CmsPagesModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class CmsPagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $CMSPageListing = CmsPagesModel::get();
        return view('admin.admincommon.cmspages.list', compact('CMSPageListing'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admincommon.cmspages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'page_title'  => 'required',
            'slug'        => 'required',
            'meta_title'  => 'required',
            'meta_keyword'=> 'required',
            'description' => 'required',
            'status'      => 'required'
        ]);

        
        if ($request->image) {
            $image = $request->file('image');
            $name  = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/cmspage_images');
            $image->move($destinationPath, $name);
        }
        
        $cmspages              = new CmsPagesModel;
        $cmspages->page_title  = $request->page_title;
        $cmspages->slug        = $request->slug;
        $cmspages->meta_title  = $request->meta_title;
        $cmspages->meta_keyword= $request->meta_keyword;
        $cmspages->image       = (isset($name)?$name:'');
        $cmspages->description = $request->description;
        $cmspages->status      = $request->status;

        $cmspages->save();
        return redirect('/admin/cmspages')->with('success_msg','One new page created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editData = CmsPagesModel::where('id',$id)->first();
        return view('admin.admincommon.cmspages.edit',compact('editData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateErr = CmsPagesModel::where('id',$id)->first();

        $validate = $this->validate($request, [
            'page_title'  => 'required',
            'meta_title'  => 'required',
            'meta_keyword'=> 'required',
            'description' => 'required',
            'status'      => 'required'
        ]);

        if ($request->image) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/cmspage_images');
            $image->move($destinationPath, $name);
        }else{
            $name = $updateErr->image;
        }
        
        $updateErr->page_title   = $request->page_title;
        $updateErr->slug         = $request->slug;
        $updateErr->meta_title   = $request->meta_title;
        $updateErr->meta_keyword = $request->meta_keyword;
        $updateErr->image        = @$name;
        $updateErr->description  = $request->description;
        $updateErr->status       = $request->status;
        $updateErr->save();
        return redirect('/admin/cmspages')->with('success_msg','Cms page updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = CmsPagesModel::find($id);    
        $image_path = public_path()."/cmspage_images/".$post->image;

        if (File::exists($image_path)) {
            unlink($image_path);
        }
        
        $post->delete();
        return redirect()->back()->with('success_msg','Cms page deleted successfully!');
    }

    //Front end controller for rendering CMS page from fronend URL directly.
    public function page($any){
        $data['page'] = CmsPagesModel::where('slug',$any)->first();
        if(!$data['page']){
            return abort(404);
        }else if($data['page']->status==0){
            return abort(403);
        }
        $data['title'] = $data['page']->page_title;
        return view('tangiblehtml.cmspage')->with($data);
    }
}
