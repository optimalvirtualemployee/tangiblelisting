<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BraceletClaspTypeModel;

class BraceletClaspTypeController extends Controller
{
    
    function __construct()
    {
        $this->middleware('permission:watch-list|watch-create|watch-edit|watch-delete', ['only' => ['index','store']]);
        $this->middleware('permission:watch-create', ['only' => ['create','store']]);
        $this->middleware('permission:watch-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:watch-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bracelet = BraceletClaspTypeModel::get();
        return view('admin.watch.braceletClaspType.index',compact('bracelet'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.watch.braceletClaspType.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = $this->validate($request, [
            "type_of_clasp" => 'required|unique:wa_factory_features_bracelet_claspType',
            "status" => 'required'
        ]);
        
        $bracelet = BraceletClaspTypeModel::create($validator);
        $bracelet->save();
        
        return redirect('/admin/braceletClaspType')->with('success_msg','Bracelet Clasp Type Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bracelet_data = BraceletClaspTypeModel::find($id);
        return view('admin.watch.braceletClaspType.edit', compact('bracelet_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            "type_of_clasp" => 'required',
            "status" => 'required'
        ]);
            
            $bracelet = BraceletClaspTypeModel::find($id);
            $bracelet->type_of_clasp = $request->input('type_of_clasp');
            $bracelet->status = $request->input('status');
            $bracelet->save();
        
        
        return redirect('/admin/braceletClaspType')->with('success_msg','Bracelet Clasp Type Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = BraceletClaspTypeModel::find($id)->delete();
        return redirect('/admin/braceletClaspType')->with('success_msg','Bracelet Clasp Type deleted successfully!');
    }
}
