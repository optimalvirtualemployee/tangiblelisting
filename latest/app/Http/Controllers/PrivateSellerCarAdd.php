<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AutomobileBodyTypeModel;
use App\AutomobileFeatureListingModel;
use App\BuildYearModel;
use App\CarModel;
use App\CarBrandModel;
use App\CarPriceModel;
use App\ColourModel;
use App\ContactusModel;
use App\FuelTypeModel;
use App\InteriorColourModel;
use App\CountryModel;
use App\NoOfDoorsModel;
use App\PackageListingRangeManagementModel;
use App\PropertyTypeModel;
use App\StateModel;
use App\CityModel;
use App\CurrencyModel;
use App\Timezone;
use App\TransmissionModel;
use App\PackageForPrivateSellersModel;
use App\UserDetailsModel;

class PrivateSellerCarAdd extends Controller
{
    public function index(){
        
        $contactUs = ContactusModel::get()->first();
        $propertyType = PropertyTypeModel::get();
        
        $makes = CarModel::get();
        $models = CarBrandModel::get();
        $colours = ColourModel::get();
        $interior_colours = InteriorColourModel::get();
        $country_data = CountryModel::get();
        $state_data = StateModel::get();
        $city_data = CityModel::get();
        $currency_data = CurrencyModel::get();
        $fuel_types = FuelTypeModel::get();
        $no_of_doors = NoOfDoorsModel::get();
        $body_types = AutomobileBodyTypeModel::get();
        $buildYear = BuildYearModel::orderBy('build_year', 'DESC')->get();
        $transmissions = TransmissionModel::get();
        $prices = CarPriceModel::get();
        $timezone = Timezone::Orderby('offset')->get();
        $userDetails = UserDetailsModel::get();
        
        $feature_listing = AutomobileFeatureListingModel::get();
        
        $html ='';
        $i = 1;
        foreach ($feature_listing as $values){
            $id = $values->id;
            $propertyName = $values->property_name;
            
            if($i == 1)
                $html .= '<div class="row">';
            
            $html .= '<div class="col-lg-4">
                      <div class="card-body opacity-8">
                    <tr>
                   <td><label  ><Strong>'.$propertyName.':</Strong></label></td>
                    <td><input   type="radio" id="'.$propertyName.'" name="'.$id.'" value="1"><label  for="yes"> Yes </label></td>
                    <td><input   type="radio" id="'.$propertyName.'" name="'.$id.'" value="0" checked><label  for="no"> No</label></td>
                    </tr>
                      </div>
                      </div>';
            
            if($i == 3)
                $html .= '</div>';
                
                $i++;
                if($i == 4)
                    $i = 1;
            
        }
        if($i != 1){
            $html .= '</div>';
        }
        
            //dd($html);
        
        return view('privateSeller.create-car', compact('makes', 'models', 'buildYear', 'colours','interior_colours', 'country_data', 
                'state_data', 'city_data', 'currency_data', 'fuel_types', 'no_of_doors', 'body_types', 'transmissions', 'prices', 'timezone',
                'html', 'contactUs', 'propertyType', 'userDetails'));
    }
    
    //this function is used to get the models based on selected brand
    public function getCarModel(Request $request){
        
        $brandId = $request->input('brandId');
        $city = CarBrandModel::select('id', 'automobile_model_name')->where('automobile_brand_id', $brandId)->where('status', "1")->get();
        return $city;
    }
    
    //this function is used to get the models based on selected brand
    public function getCarBrandName(Request $request){
        
        $brandId = $request->input('brandId');
        $name = CarModel::find($brandId);
        return $name;
    }
    
    public function getCarModelName(Request $request){
        
        $brandId = $request->input('brandId');
        $name = CarBrandModel::find($brandId);
        return $name;
    }
    
    //this function displays the states based on the selected country
    public function getStateByCountry(Request $request){
        
        $countryId = $request->input('countryId');
        $state = StateModel::select('id', 'state_name')->where('country_id', $countryId)
        ->orderBy('state_name','asc')->get();
        return $state;
    }
    
    //this function is used to get the cities based on selected state
    public function getCityByState(Request $request){
        
        $stateId = $request->input('stateId');
        $countryId = $request->input('countryId');
        
        if($stateId == '')
            $city = CityModel::select('id', 'city_name')->where('country_id', $countryId)->orderBy('city_name','asc')->get();
            else
                $city = CityModel::select('id', 'city_name')->where('state_id', $stateId)->orderBy('city_name','asc')->get();
                
                
                return $city;
    }
    
    public function price($package, $pricerange, $packageprice){
        
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        currency()->setUserCurrency($selectedCurrency->code);
        
        $priceRangeArray = explode("-",$pricerange);
        
        $packageId = PackageForPrivateSellersModel::select('id')->where('package_for_private_sellers','=' , $package)->get()->first();

        $packageList = PackageForPrivateSellersModel::get();
        
        $upgradepackage = PackageListingRangeManagementModel::where('rangeFrom', '=', $priceRangeArray[0])->
        where('rangeTo', '=', $priceRangeArray[1])->where('package_id', '!=', $packageId->id)->get();
        
        $contactUs = ContactusModel::get()->first();
        $propertyType = PropertyTypeModel::get();
        
        $makes = CarModel::get();
        $models = CarBrandModel::get();
        $colours = ColourModel::get();
        $interior_colours = InteriorColourModel::get();
        $country_data = CountryModel::get();
        $state_data = StateModel::get();
        $city_data = CityModel::get();
        $currency_data = CurrencyModel::get();
        $fuel_types = FuelTypeModel::get();
        $no_of_doors = NoOfDoorsModel::get();
        $body_types = AutomobileBodyTypeModel::get();
        $buildYear = BuildYearModel::orderBy('build_year', 'DESC')->get();
        $transmissions = TransmissionModel::get();
        $prices = CarPriceModel::get();
        $timezone = Timezone::Orderby('offset')->get();
        $userDetails = UserDetailsModel::get();
        
        $feature_listing = AutomobileFeatureListingModel::get();
        
        $i =0;
        $html1 = '';
        $html2 = '';
        foreach ($feature_listing as $values){
            $id = $values->id;
            $propertyName = $values->property_name;
            
            if($i % 2 == 0){
                
                
                
                $html1 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="'.$propertyName.'"></label>
                    <input   type="checkbox" class="form-check-input" id="'.$propertyName.'" name="'.$id.'" value="1">'.$propertyName.'
                      </div>';
            }else {
                
                $html2 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="'.$propertyName.'"></label>
                    <input   type="checkbox" class="form-check-input" id="'.$propertyName.'" name="'.$id.'" value="1">'.$propertyName.'
                      </div>';
                
                
            }
            
            $i++;
                        
        }
        
        
        //dd($html);
        
        return view('privateSeller.create-car', compact('makes', 'models', 'buildYear', 'colours','interior_colours', 'country_data',
            'state_data', 'city_data', 'currency_data', 'fuel_types', 'no_of_doors', 'body_types', 'transmissions', 'prices', 'timezone',
            'html1','html2', 'contactUs', 'propertyType', 'packageprice', 'upgradepackage', 'packageList', 'userDetails'));
        
    }
}
