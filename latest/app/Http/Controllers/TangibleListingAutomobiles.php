<?php
namespace App\Http\Controllers;

use App\AgenciesModel;
use App\AutomobileDataListingModel;
use App\AutomobileImageModel;
use App\BuildYearModel;
use App\CarBrandModel;
use App\CarModel;
use App\ContactusModel;
use App\CurrencyModel;
use App\PropertyTypeModel;
use Illuminate\Http\Request;
use App\AutomobileBlogModel;
use App\AutomobileBlogImageModel;
use App\AutomobileBodyTypeModel;
use App\CarModelImage;
use App\AutomobileBodyTypeImageModel;

class TangibleListingAutomobiles extends Controller
{

    public function index()
    {
        $contactUs = ContactusModel::get()->first();
        $images = AutomobileImageModel::get();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        $propertyType = PropertyTypeModel::get();
            
        $total_listing = AutomobileDataListingModel::where('status','=', '1')->get();
        
        $latest_listing = AutomobileDataListingModel::select('currency_code', 'au_automobile_detail.id as id', 'ad_title', 'build_year', 'neworused', 'value', 'odometer', 'au_transmission.transmission as transmission', 'rhdorlhd', 'au_automobile_detail.created_at as automobilecreationtime', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode', 'tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id')->join('tbl_currency', 'tbl_currency.id', '=', 'au_automobile_detail.currency_id')
        ->join('tbl_state', 'tbl_state.id', '=', 'au_automobile_detail.state_id')
        ->join('tbl_city', 'tbl_city.id', '=', 'au_automobile_detail.city_id')
        ->join('tbl_country', 'tbl_country.id', '=', 'au_automobile_detail.country_id')
        ->join('tbl_agent', 'tbl_agent.id', '=', 'au_automobile_detail.agent_id')
        ->join('re_build_year', 're_build_year.id', '=', 'au_automobile_detail.year_id')
        ->join('au_transmission', 'au_transmission.id', '=', 'au_automobile_detail.transmission_id')
        ->join('au_body_type', 'au_body_type.id', '=', 'au_automobile_detail.body_type_id')
        ->whereIN('au_automobile_detail.id', function ($query) {
            $query->select('listing_id')
            ->from('au_automobile_images');
        })
        ->whereIN('au_automobile_detail.id', function($query){
         $query->select('listing_id')
         ->from('au_automobile_features');
         })
            ->whereIN('au_automobile_detail.id', function ($query) {
                $query->select('listing_id')
                ->from('au_automobile_comments');
            })->where('au_automobile_detail.status', '=', '1')
            ->orderBy('au_automobile_detail.created_at', 'DESC')
            ->get();
            
         //dd($latest_listing);   
         
         $latest_listing_top = AutomobileDataListingModel::select('currency_code', 'au_automobile_detail.id as id', 'ad_title', 'build_year', 'neworused', 'value', 'odometer', 'au_transmission.transmission as transmission', 'rhdorlhd', 'au_automobile_detail.created_at as automobilecreationtime', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode', 'tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id')->join('tbl_currency', 'tbl_currency.id', '=', 'au_automobile_detail.currency_id')
         ->join('tbl_state', 'tbl_state.id', '=', 'au_automobile_detail.state_id')
         ->join('tbl_city', 'tbl_city.id', '=', 'au_automobile_detail.city_id')
         ->join('tbl_country', 'tbl_country.id', '=', 'au_automobile_detail.country_id')
         ->join('tbl_agent', 'tbl_agent.id', '=', 'au_automobile_detail.agent_id')
         ->join('re_build_year', 're_build_year.id', '=', 'au_automobile_detail.year_id')
         ->join('au_transmission', 'au_transmission.id', '=', 'au_automobile_detail.transmission_id')
         ->join('au_body_type', 'au_body_type.id', '=', 'au_automobile_detail.body_type_id')
         ->whereIN('au_automobile_detail.id', function ($query) {
             $query->select('listing_id')
             ->from('au_automobile_images');
         })
         ->whereIN('au_automobile_detail.id', function($query){
             $query->select('listing_id')
             ->from('au_automobile_features');
         })
         ->whereIN('au_automobile_detail.id', function ($query) {
             $query->select('listing_id')
             ->from('au_automobile_comments');
         })->where('au_automobile_detail.status', '=', '1')
         ->orderBy('au_automobile_detail.created_at', 'DESC')
         ->get();
            
        $suv_listing = AutomobileDataListingModel::select('currency_code', 'au_automobile_detail.id as id', 'ad_title', 'build_year', 'neworused', 'value', 'odometer', 'au_transmission.transmission as transmission', 'rhdorlhd', 'au_automobile_detail.created_at as automobilecreationtime', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode', 'tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id')->join('tbl_currency', 'tbl_currency.id', '=', 'au_automobile_detail.currency_id')
            ->join('tbl_state', 'tbl_state.id', '=', 'au_automobile_detail.state_id')
            ->join('tbl_city', 'tbl_city.id', '=', 'au_automobile_detail.city_id')
            ->join('tbl_country', 'tbl_country.id', '=', 'au_automobile_detail.country_id')
            ->join('tbl_agent', 'tbl_agent.id', '=', 'au_automobile_detail.agent_id')
            ->join('re_build_year', 're_build_year.id', '=', 'au_automobile_detail.year_id')
            ->join('au_transmission', 'au_transmission.id', '=', 'au_automobile_detail.transmission_id')
            ->join('au_body_type', 'au_body_type.id', '=', 'au_automobile_detail.body_type_id')
            ->whereIN('au_automobile_detail.id', function ($query) {
            $query->select('listing_id')
                ->from('au_automobile_images');
        })
        ->whereIN('au_automobile_detail.id', function($query){
         $query->select('listing_id')
         ->from('au_automobile_features');
         })
            ->whereIN('au_automobile_detail.id', function ($query) {
            $query->select('listing_id')
                ->from('au_automobile_comments');
        })
            ->where('au_body_type.body_type', '=', 'SUV')
            ->where('au_automobile_detail.status', '=', '1')
            ->get();

            
            
        $sedan_listing = AutomobileDataListingModel::select('currency_code', 'au_automobile_detail.id as id', 'ad_title', 'build_year', 'neworused', 'value', 'odometer', 'au_transmission.transmission as transmission', 'rhdorlhd', 'au_automobile_detail.created_at as automobilecreationtime', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode', 'tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id')->join('tbl_currency', 'tbl_currency.id', '=', 'au_automobile_detail.currency_id')
            ->join('tbl_state', 'tbl_state.id', '=', 'au_automobile_detail.state_id')
            ->join('tbl_city', 'tbl_city.id', '=', 'au_automobile_detail.city_id')
            ->join('tbl_country', 'tbl_country.id', '=', 'au_automobile_detail.country_id')
            ->join('tbl_agent', 'tbl_agent.id', '=', 'au_automobile_detail.agent_id')
            ->join('re_build_year', 're_build_year.id', '=', 'au_automobile_detail.year_id')
            ->join('au_transmission', 'au_transmission.id', '=', 'au_automobile_detail.transmission_id')
            ->join('au_body_type', 'au_body_type.id', '=', 'au_automobile_detail.body_type_id')
            ->whereIN('au_automobile_detail.id', function ($query) {
            $query->select('listing_id')
                ->from('au_automobile_images');
        })
            ->whereIN('au_automobile_detail.id', function($query){
             $query->select('listing_id')
             ->from('au_automobile_features');
             })
                ->whereIN('au_automobile_detail.id', function ($query) {
            $query->select('listing_id')
                ->from('au_automobile_comments');
        })
            ->where('au_body_type.body_type', '=', 'Sedan')
            ->where('au_automobile_detail.status', '=', '1')
            ->get();
            
        $electric_listing = AutomobileDataListingModel::select('currency_code', 'au_automobile_detail.id as id', 'ad_title', 'build_year', 'neworused', 'value', 'odometer', 'au_transmission.transmission as transmission', 'rhdorlhd', 'au_automobile_detail.created_at as automobilecreationtime', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode', 'tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id')->join('tbl_currency', 'tbl_currency.id', '=', 'au_automobile_detail.currency_id')
            ->join('tbl_state', 'tbl_state.id', '=', 'au_automobile_detail.state_id')
            ->join('tbl_city', 'tbl_city.id', '=', 'au_automobile_detail.city_id')
            ->join('tbl_country', 'tbl_country.id', '=', 'au_automobile_detail.country_id')
            ->join('tbl_agent', 'tbl_agent.id', '=', 'au_automobile_detail.agent_id')
            ->join('re_build_year', 're_build_year.id', '=', 'au_automobile_detail.year_id')
            ->join('au_transmission', 'au_transmission.id', '=', 'au_automobile_detail.transmission_id')
            ->join('au_body_type', 'au_body_type.id', '=', 'au_automobile_detail.body_type_id')
            ->whereIN('au_automobile_detail.id', function ($query) {
            $query->select('listing_id')
                ->from('au_automobile_images');
        })->whereIN('au_automobile_detail.id', function($query){
                 $query->select('listing_id')
                 ->from('au_automobile_features');
        })->whereIN('au_automobile_detail.id', function ($query) {
            $query->select('listing_id')
                ->from('au_automobile_comments');
        })->where('au_body_type.body_type', '=', 'Electric')
          ->where('au_automobile_detail.status', '=', '1')
          ->get();

        $hybrid_listing = AutomobileDataListingModel::select('currency_code', 'au_automobile_detail.id as id', 'ad_title', 'build_year', 'neworused', 'value', 'odometer', 'au_transmission.transmission as transmission', 'rhdorlhd', 'au_automobile_detail.created_at as automobilecreationtime', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode', 'tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id')->join('tbl_currency', 'tbl_currency.id', '=', 'au_automobile_detail.currency_id')
            ->join('tbl_state', 'tbl_state.id', '=', 'au_automobile_detail.state_id')
            ->join('tbl_city', 'tbl_city.id', '=', 'au_automobile_detail.city_id')
            ->join('tbl_country', 'tbl_country.id', '=', 'au_automobile_detail.country_id')
            ->join('tbl_agent', 'tbl_agent.id', '=', 'au_automobile_detail.agent_id')
            ->join('re_build_year', 're_build_year.id', '=', 'au_automobile_detail.year_id')
            ->join('au_transmission', 'au_transmission.id', '=', 'au_automobile_detail.transmission_id')
            ->join('au_body_type', 'au_body_type.id', '=', 'au_automobile_detail.body_type_id')
            ->whereIN('au_automobile_detail.id', function ($query) {
            $query->select('listing_id')
                ->from('au_automobile_images');
        })->whereIN('au_automobile_detail.id', function($query){
                     $query->select('listing_id')->from('au_automobile_features');
        })->whereIN('au_automobile_detail.id', function ($query) {
            $query->select('listing_id')
                ->from('au_automobile_comments');
        })->where('au_body_type.body_type', '=', 'Hybrid')
          ->where('au_automobile_detail.status', '=', '1')
          ->get();
        
        $luxury_listing = AutomobileDataListingModel::select('currency_code', 'au_automobile_detail.id as id', 'ad_title', 'build_year', 'neworused', 'value', 'odometer', 'au_transmission.transmission as transmission', 'rhdorlhd', 'au_automobile_detail.created_at as automobilecreationtime', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode', 'tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id')->join('tbl_currency', 'tbl_currency.id', '=', 'au_automobile_detail.currency_id')
            ->join('tbl_state', 'tbl_state.id', '=', 'au_automobile_detail.state_id')
            ->join('tbl_city', 'tbl_city.id', '=', 'au_automobile_detail.city_id')
            ->join('tbl_country', 'tbl_country.id', '=', 'au_automobile_detail.country_id')
            ->join('tbl_agent', 'tbl_agent.id', '=', 'au_automobile_detail.agent_id')
            ->join('re_build_year', 're_build_year.id', '=', 'au_automobile_detail.year_id')
            ->join('au_transmission', 'au_transmission.id', '=', 'au_automobile_detail.transmission_id')
            ->join('au_body_type', 'au_body_type.id', '=', 'au_automobile_detail.body_type_id')
            ->whereIN('au_automobile_detail.id', function ($query) {
                $query->select('listing_id')
                ->from('au_automobile_images');
            })
            ->whereIN('au_automobile_detail.id', function($query){
             $query->select('listing_id')
             ->from('au_automobile_features');
             })->whereIN('au_automobile_detail.id', function ($query) {
                    $query->select('listing_id')
                    ->from('au_automobile_comments');
             })->where('au_body_type.body_type', '=', 'Luxury')
               ->where('au_automobile_detail.status', '=', '1')
               ->get();
        // dd($suv_listing);

        $dealer_listing = AgenciesModel::select('tbl_agencies.id as id', 'first_name', 'filename')->join('tbl_agencies_images', 'tbl_agencies_images.listing_id', '=', 'tbl_agencies.id')
            ->orderBy('tbl_agencies.created_at', 'desc')
            ->limit('4')
            ->get();

        $auto_blogs = AutomobileBlogModel::select('au_blog.id as id', 'title', 'blogPost', 'au_blog.created_at as created_at', 'blogType','url')->where('status', '=', '1')
            ->orderBy('au_blog.created_at', 'asc')
            ->limit('4')
            ->get();

        $blog_images = AutomobileBlogImageModel::get();
        
        
        $body_types = AutomobileBodyTypeModel::where('status','=', '1')->get();
        
        //dd($body_types);
        $body_types_images = AutomobileBodyTypeImageModel::get();
        $makes = CarModel::where('status','=', '1')->get();
        $makes_images = CarModelImage::get();
        $models = CarBrandModel::whereIN('au_model.automobile_brand_id', function ($query) {
            $query->select('id')->from('au_brands');
        })->where('status','=', '1')->get();
        $from_year = BuildYearModel::orderBy('build_year', 'ASC')->get();
        $to_year = BuildYearModel::orderBy('build_year', 'DESC')->get();

        $make_brand = CarModel::where('show_under_brand', '=', '1')->get();
    
        // dd($from_year);
        return view('tangiblehtml.car-landing', compact('latest_listing_top','total_listing','propertyType','contactUs', 'dealer_listing', 'auto_blogs', 'blog_images', 'makes', 'models', 'from_year', 'to_year', 'suv_listing', 
                    'images', 'sedan_listing', 'electric_listing', 'hybrid_listing', 'body_types', 'luxury_listing','latest_listing', 'makes_images', 'body_types_images', 'make_brand'));
    }
}
