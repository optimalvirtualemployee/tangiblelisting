<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Session;
use App\User;
use App\AgentModel;

class AgentLoginController extends Controller
{
     public function index(){
        
         $agent = AgentModel::select('tbl_category.website_category as category')
         ->where('tbl_agent.userId', '=', Auth::User()->id)
         ->join('tbl_agencies', 'tbl_agencies.id','tbl_agent.agency_id')
         ->join('tbl_category', 'tbl_category.id', 'tbl_agencies.category_id')
         ->first();

         return view('agent.dashboard.dashboard', compact('agent'));
    }
    public function login() {
        return view('agent.login');
    }
    public function register(){
        return view('agent.register');
    }

    function checklogin(Request $request) {
			$this->validate($request, [
				'email'   => 'required|email',
				'password'  => 'required|min:3'
			]);
			$user_data = array(
				'email'  => $request->post('email'),
				'password' => $request->post('password'),
                'usertype' =>'5'
			);
		if(Auth::attempt($user_data)){

            if(Auth::user()->status=='1'){
                return redirect('/agent/dashboard');
            }
            else{
                return back()->withErrors(['error' => ['User is not active.']]);
            }

		} else {
            return back()->withErrors(['error' => ['Invalid user name or password.']]);
		}
    }
    
    public function newagentRegister(Request $request){
        $this->validate($request, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'min:10', 'max:12'],
            'password' => ['required', 'string', 'min:8'],
            'i_agree'   =>['required'],
        ]);

        $regUser = new User;
        $regUser->usertype  =  5;
        $regUser->first_name  =  $request->first_name;
        $regUser->last_name  =   $request->last_name;
        $regUser->email  =   $request->email;
        $regUser->phone =   $request->phone;
        $regUser->password  =   Hash::make($request->password);
        $regUser->save();
            $user_data = array(
                'email'  => $request->post('email'),
                'password' => $request->post('password'),
                'usertype' =>'5'
            );
        Auth::attempt($user_data);
        return redirect('/agent/dashboard');
    }

    function successlogin()  {
     	return view('dashbord');
    }

    function logout()  {
	    Auth::logout();
	    return redirect('main');
    }
}
