<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FunctionGMTModel extends Model
{
    protected $table = 'wa_factory_features_function_gmt';
    protected $fillable = ['GMT', 'status'];
}
