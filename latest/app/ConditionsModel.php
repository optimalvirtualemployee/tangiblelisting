<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConditionsModel extends Model
{
    protected $table = 'wa_conditions';
    protected $fillable = ['conditions', 'status'];
}
