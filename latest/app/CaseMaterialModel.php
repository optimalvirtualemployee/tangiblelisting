<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseMaterialModel extends Model
{
    protected $table = 'wa_factory_features_case_material';
    protected $fillable = ['case_material', 'status'];
}
