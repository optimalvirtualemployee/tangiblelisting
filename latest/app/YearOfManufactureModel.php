<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class YearOfManufactureModel extends Model
{
    protected $table = 'wa_year_of_manufacture';
    protected $fillable = ['year_of_manufacture', 'status'];
}
