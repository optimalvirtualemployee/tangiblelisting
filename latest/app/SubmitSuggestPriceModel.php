<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubmitSuggestPriceModel extends Model
{
    protected $table = 'submitpricerequest';
}
