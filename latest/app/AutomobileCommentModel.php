<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobileCommentModel extends Model
{
    protected $table = 'au_automobile_comments';
}
