<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobileBlogModel extends Model
{
    protected $table = 'au_blog';
}
