<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoOfBalconiesModel extends Model
{
    protected $table = 're_balcony';
    protected $fillable = ['number_of_balconies', 'status'];
}
