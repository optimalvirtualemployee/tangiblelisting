<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseDiameterModel extends Model
{
    protected $table = 'wa_case_diameter';
    protected $fillable = ['case_diameter', 'status'];
}
