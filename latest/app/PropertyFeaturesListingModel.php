<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyFeaturesListingModel extends Model
{
    protected $table = 're_properties_features_listing';
}
