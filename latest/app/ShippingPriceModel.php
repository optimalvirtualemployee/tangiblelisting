<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingPriceModel extends Model
{
    protected $table = 'wa_shipping_price';

}