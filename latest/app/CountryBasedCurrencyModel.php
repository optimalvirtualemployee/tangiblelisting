<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryBasedCurrencyModel extends Model
{
    protected $table = 'tbl_country_currency';
}
